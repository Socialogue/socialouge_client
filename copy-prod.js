const fs = require('fs');

async function copyFiles() {
  fs.copyFile('./web/.prod', './web/.env.production', (err) => {
    if (err) throw err;
  });
}

copyFiles();
