import {
  ERROR_SUFFIX,
  SUCCESS_SUFFIX,
  getRequestOptions,
  isAxiosRequestAction
} from './helpers';

const axiosSagaMiddleware = () => {
  const requestMap = new Map();

  return next => (action) => {
    const { type } = action;

    if (!isAxiosRequestAction(action) &&
        !type.includes(SUCCESS_SUFFIX) &&
        !type.includes(ERROR_SUFFIX)) {
      return next(action);
    }

    /**
     * If action contains request object, we'll return a promise
     * so that we can resolve/reject it later when SUCCESS/FAIL
     * action is dispatched.
     */
    if (isAxiosRequestAction(action)) {
      const requestOptions = getRequestOptions(action);
      const shouldRejectOnError = requestOptions &&
        requestOptions.returnRejectedPromiseOnError;
      const requestActionPromise = requestMap.get(action.type);

      /**
       * If current action is already in the requestMap,
       * fulfill the pending promise with empty value
       * and delete it from the requestMap.
       */
      if (requestActionPromise) {
        requestActionPromise(null, false);
        requestMap.delete(action);
      }

      return new Promise((resolve, reject) => {
        requestMap.set(
          type,
          (response, error) => (
            error && shouldRejectOnError ? reject(response) : resolve(response)
          )
        );

        next(action);
      });
    }

    /**
     * Get the original action which triggered the SUCCESS/FAIL
     * action and check if we have any promise pending for that
     * action. Then fulfill the promise and remove the action
     * from map object.
     */
    const errorResponse = type.includes(ERROR_SUFFIX);
    const originalAction = type.substring(
      0, errorResponse ? type.indexOf(ERROR_SUFFIX) : type.indexOf(SUCCESS_SUFFIX)
    );
    const requestActionPromise = requestMap.get(originalAction);

    if (requestActionPromise) {
      requestActionPromise(action, errorResponse);
      requestMap.delete(originalAction);
    }

    return next(action);
  };
};

export default axiosSagaMiddleware;
