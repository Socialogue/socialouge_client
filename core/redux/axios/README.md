# Saga & Middleware for Axios

## Saga
Saga will listen for any action that has request object and trigger API call. If request succeeds it'll dispatch `${action}_SUCCESS` action, otherwise `${action}_FAIL` will be dispatched.

Saga will only take the latest action for any particular action and cancel the previous action if any.

### Saga Usage
```js
function* rootSaga() {
  yield all([
    axiosSaga(axiosClient, axiosConfig),
    ......
  ]);
}
```

Where `axiosClient` is an axios instance created by `axios.create({})`. `axiosConfig` only supports interceptors for now.

```js
axiosConfig = {
  interceptors: {
    request: [
      ({ getState, action }, request) => {},
      ({ getState, action }, error) => {}
    ],
    response: [
      ({ getState, action }, response) => {},
      ({ getState, action }, error) => {}
    ]
  }
};
```

## Middleware
Middleware will wrap the response of the request with a promise and return the result, so that the caller can chain the call with `.then()` or `.catch()` method. When the request succeeds and `SUCCESS/FAIL` action is dispatched, it'll `resolve/reject` the promise with the whole action object as data.

>NOTE: Register it before the redux-saga middleware.

## TODO
Make saga & middleware more generic & configurable.
