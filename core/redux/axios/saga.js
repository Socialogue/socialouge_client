import {
  ERROR_SUFFIX,
  SUCCESS_SUFFIX,
  getRequestConfig,
  isAxiosRequestAction
} from "./helpers";
import {
  call,
  cancel,
  cancelled,
  fork,
  put,
  select,
  take
} from "redux-saga/effects";

function applyInterceptorsIfExists(axiosClient, axiosConfig, injectParams) {
  if (!axiosConfig.interceptors) return;

  const requestInterceptor = axiosConfig.interceptors.request;
  const responseInterceptor = axiosConfig.interceptors.response;

  if (requestInterceptor && requestInterceptor.length) {
    axiosClient.interceptors.request.use(
      requestInterceptor[0].bind(null, injectParams),
      requestInterceptor[1]
        ? requestInterceptor[1].bind(null, injectParams)
        : null
    );
  }

  if (responseInterceptor && responseInterceptor.length) {
    axiosClient.interceptors.response.use(
      responseInterceptor[0].bind(null, injectParams),
      responseInterceptor[1]
        ? responseInterceptor[1].bind(null, injectParams)
        : null
    );
  }
}

function makeRequest(axiosClient, config) {
  return axiosClient
    .request(config)
    .then(response => ({ response }))
    .catch(error => ({ error }));
}

function* dispatchFailureAction(action, error) {
  let errorObject;

  if (!error.response) {
    errorObject = {
      data: error.message,
      status: 0
    };

    if (process.env.NODE_ENV !== "production") {
      // eslint-disable-next-line no-console
      console.log("HTTP Failure in Axios", error);
    }
  } else {
    errorObject = error;
  }

  yield put({
    type: `${action.type}${ERROR_SUFFIX}`,
    error: errorObject,
    meta: {
      previousAction: action
    }
  });
}

function* dispatchSuccessAction(action, response) {
  const newAction = {
    type: `${action.type}${SUCCESS_SUFFIX}`,
    payload: response,
    meta: {
      previousAction: action
    }
  };
  if (action.fileUploadKey) {
    newAction.fileUploadKey = action.fileUploadKey;
  }
  yield put(newAction);
}

function* handleAxiosAction(axiosClient, axiosConfig, action) {
  try {
    const config = yield call(getRequestConfig, action);
    const state = yield select();
    const injectParams = {
      getState: () => state,
      action
    };

    applyInterceptorsIfExists(axiosClient, axiosConfig, injectParams);

    const { response, error } = yield call(makeRequest, axiosClient, config);

    if (response) {
      yield call(dispatchSuccessAction, action, response);
    } else {
      yield call(dispatchFailureAction, action, error);
    }
  } finally {
    if (yield cancelled()) {
      // Cleanup task, maybe call _CANCEL action ?
    }
  }
}

export default function* axiosSaga(axiosClient, axiosConfig) {
  const taskList = {};

  while (true) {
    const action = yield take(isAxiosRequestAction);
    const activeTask = taskList[action.type];

    if (activeTask && activeTask.isRunning()) {
      // Cancel the previous task
      yield cancel(activeTask);
    }

    const newTask = yield fork(
      handleAxiosAction,
      axiosClient,
      axiosConfig,
      action
    );
    taskList[action.type] = newTask;
  }
}
