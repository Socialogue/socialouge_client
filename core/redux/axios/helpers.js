export const SUCCESS_SUFFIX = '_SUCCESS';
export const ERROR_SUFFIX = '_FAIL';

export const isAxiosRequestAction = action =>
  action.payload &&
  action.payload.request &&
  !action.type.includes(SUCCESS_SUFFIX) &&
  !action.type.includes(ERROR_SUFFIX);

export const getRequestConfig = action => action.payload.request;

export const getRequestOptions = action => action.payload.options;
