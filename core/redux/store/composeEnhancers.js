import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import * as authActionCreators from '../../actions/authActionCreators';


const actionCreators = {
  ...authActionCreators
};

const composeEnhancers = composeWithDevTools({
  // Specify here name, actionsBlacklist, actionsCreators and other options if needed
  actionCreators
});

export default composeEnhancers;
