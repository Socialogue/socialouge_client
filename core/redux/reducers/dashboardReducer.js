import {
  ACTION_DELETE_PRODUCT,
  ACTION_DELETE_PRODUCT_FAIL,
  ACTION_DELETE_PRODUCT_SUCCESS,
  ACTION_GET_ALL_PRODUCTS,
  ACTION_GET_ALL_PRODUCTS_FAIL,
  ACTION_GET_ALL_PRODUCTS_SUCCESS,
  ACTION_GET_ALL_ORDERS,
  ACTION_GET_ALL_ORDERS_FAIL,
  ACTION_GET_ALL_ORDERS_SUCCESS,
  ACTION_ORDERS_STATUS_CHANGE_FAIL,
  ACTION_ORDERS_STATUS_CHANGE_SUCCESS,
  ACTION_ORDERS_STATUS_CHANGE
} from "../../constants/actionTypes";

export const initialState = {
  productDelete: false,
  products: [],
  orders: [],
  page: null,
  hasNextPage: null,
  hasPrevPage: null,
  limit: null,
  nextPage: null,
  page: null,
  pagingCounter: null,
  prevPage: null,
  totalDocs: null,
  totalPages: null,
  orderStatusChange: false
};

const dashboardReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    //get all produtcs...
    case ACTION_GET_ALL_PRODUCTS: {
      return {
        ...state
      };
    }

    case ACTION_GET_ALL_PRODUCTS_SUCCESS: {
      return {
        ...state,
        products: payload.data.data.docs,
        page: payload.data.data.page,
        hasNextPage: payload.data.data.hasNextPage,
        hasPrevPage: payload.data.data.hasPrevPage,
        limit: payload.data.data.limit,
        nextPage: payload.data.data.nextPage,
        pagingCounter: payload.data.data.pagingCounter,
        prevPage: payload.data.data.prevPage,
        totalDocs: payload.data.data.totalDocs,
        totalPages: payload.data.data.totalPages
      };
    }

    case ACTION_GET_ALL_PRODUCTS_FAIL: {
      return {
        ...state
      };
    }

    //delete product...

    case ACTION_DELETE_PRODUCT: {
      // let ids = payload.request.data.listingItems;
      // const products = state.products;
      //  let filter = ids.map(id => )
      // console.log("ids: ", payload.request.data.listingItems, state.products);

      return {
        ...state
      };
    }

    case ACTION_DELETE_PRODUCT_SUCCESS: {
      return {
        ...state,
        productDelete: true
      };
    }

    case ACTION_DELETE_PRODUCT_FAIL: {
      return {
        ...state,
        productDelete: false
      };
    }

    //get all produtcs...
    case ACTION_GET_ALL_ORDERS: {
      return {
        ...state
      };
    }

    case ACTION_GET_ALL_ORDERS_SUCCESS: {
      return {
        ...state,
        orders: payload.data.data.docs,
        page: payload.data.data.page,
        hasNextPage: payload.data.data.hasNextPage,
        hasPrevPage: payload.data.data.hasPrevPage,
        limit: payload.data.data.limit,
        nextPage: payload.data.data.nextPage,
        pagingCounter: payload.data.data.pagingCounter,
        prevPage: payload.data.data.prevPage,
        totalDocs: payload.data.data.totalDocs,
        totalPages: payload.data.data.totalPages
      };
    }

    case ACTION_GET_ALL_ORDERS_FAIL: {
      return {
        ...state
      };
    }

    // order status change...
    case ACTION_ORDERS_STATUS_CHANGE: {
      return {
        ...state
      };
    }

    case ACTION_ORDERS_STATUS_CHANGE_SUCCESS: {
      return {
        ...state,
        orderStatusChange: true
      };
    }

    case ACTION_ORDERS_STATUS_CHANGE_FAIL: {
      return {
        ...state,
        orderStatusChange: false
      };
    }

    default:
      return state;
  }
};

export default dashboardReducer;
