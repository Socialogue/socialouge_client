import {
  ACTION_ADD_POST_SUCCESS,
  ACTION_ADD_POST_TO_REDUX,
  ACTION_GET_DELIVERY_ADDRESS,
  ACTION_GET_DELIVERY_ADDRESS_FAIL,
  ACTION_GET_DELIVERY_ADDRESS_SUCCESS,
  ACTION_LIKE_POST_SOCKET,
  ACTION_LOAD_MORE_COMMENT,
  ACTION_LOAD_MORE_COMMENT_FAIL,
  ACTION_LOAD_MORE_COMMENT_REPLY,
  ACTION_LOAD_MORE_COMMENT_REPLY_FAIL,
  ACTION_LOAD_MORE_COMMENT_REPLY_SUCCESS,
  ACTION_LOAD_MORE_COMMENT_SUCCESS,
  ACTION_POST_PRODUCT,
  ACTION_POST_PRODUCT_FAIL,
  ACTION_POST_PRODUCT_SUCCESS,
  ACTION_SET_SORT_VIEW,
  ACTION_SET_SORT_VIEW_FEED,
} from '../../constants/actionTypes'
import { API_LOAD_MORE_COMMENT } from '../../constants/apiEndpoints'
import {
  SOCKET_ACTION_ADD_COMMENT_EDIT_TO_POST,
  SOCKET_ACTION_ADD_COMMENT_EDIT_TO_POST_RES,
  SOCKET_ACTION_ADD_COMMENT_REMOVE_TO_POST,
  SOCKET_ACTION_ADD_COMMENT_REMOVE_TO_POST_RES,
  SOCKET_ACTION_ADD_COMMENT_REPLY_TO_POST,
  SOCKET_ACTION_ADD_COMMENT_REPLY_TO_POST_RES,
  SOCKET_ACTION_ADD_COMMENT_TO_POST,
  SOCKET_ACTION_ADD_COMMENT_TO_POST_RES,
  SOCKET_ACTION_ADD_LIKE_TO_POST,
  SOCKET_ACTION_ADD_LIKE_TO_POST_RES,
} from '../../constants/socketActionTypes'

export const initialState = {
  viewType: 'mid',
  viewTypeFeed: 'top',
  feed: {},
  post: null,
  postChangeDetect: false,
  deliveryAddressMsg: '',
  errorMsg: '',
  replyAdded: false,
  commentDeleted: false,
  commentEdited: false,
}

const homeReducer = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    /*case REHYDRATE: {
      const { posts } = payload;
      return {
        ...state,
        posts: posts
      };
    }*/

    case ACTION_SET_SORT_VIEW: {
      return {
        ...state,
        viewType: payload.data,
      }
    }

    case ACTION_SET_SORT_VIEW_FEED: {
      return {
        ...state,
        viewTypeFeed: payload.data,
      }
    }

    case ACTION_POST_PRODUCT: {
      return {
        ...state,
      }
    }

    case ACTION_POST_PRODUCT_SUCCESS: {
      const posts = payload.data.posts
      posts.map((post) => {
        const comments = []
        post.comments.map((cm) => {
          if (cm.replay.length > 2) {
            cm.replay = cm.replay.splice(0, 2)
            comments.push(cm)
          }
        })
      })
      return {
        ...state,
        feed: payload.data,
      }
    }

    case ACTION_POST_PRODUCT_FAIL: {
      return {
        ...state,
      }
    }

    // adding post for details...
    case ACTION_ADD_POST_TO_REDUX: {
      return {
        ...state,
        post: payload.post,
      }
    }

    // after adding a new post...
    case ACTION_ADD_POST_SUCCESS: {
      const posts = state.feed.posts
      posts.unshift(payload.data.data)
      return {
        ...state,
        feed: { ...state.feed, posts },
        postAddStatus: false,
      }
    }

    case ACTION_GET_DELIVERY_ADDRESS: {
      return {
        ...state,
        deliveryAddressMsg: '',
      }
    }

    case ACTION_GET_DELIVERY_ADDRESS_SUCCESS: {
      return {
        ...state,
        deliveryAddress: payload.data,
        deliveryAddressMsg: 'success',
      }
    }

    case ACTION_GET_DELIVERY_ADDRESS_FAIL: {
      return {
        ...state,
        deliveryAddressMsg: 'Not found any address',
      }
    }

    // load more comment of a post...

    case ACTION_LOAD_MORE_COMMENT: {
      return {
        ...state,
        errorMsg: '',
        postChangeDetect: false,
      }
    }
    case ACTION_LOAD_MORE_COMMENT_SUCCESS: {
      const posts = [...state.feed.posts]
      let detailsPost = { ...state.post }
      if (payload.data.data.length) {
        const postIndex = posts.findIndex(
          (post) => post._id === payload.data.data[0].post,
        )
        if (postIndex !== -1) {
          posts[postIndex].comments = [
            ...posts[postIndex].comments,
            ...payload.data.data,
          ]
          detailsPost = posts[postIndex]
          return {
            ...state,
            feed: { ...state.feed, posts },
            postChangeDetect: true,
            errorMsg: '',
          }
        }
      }
    }
    case ACTION_LOAD_MORE_COMMENT_FAIL: {
      return {
        ...state,
        errorMsg: "couldn't load comments",
        postChangeDetect: false,
      }
    }

    // load more reply of a post...

    case ACTION_LOAD_MORE_COMMENT_REPLY: {
      return {
        ...state,
        errorMsg: '',
        postChangeDetect: false,
      }
    }
    case ACTION_LOAD_MORE_COMMENT_REPLY_SUCCESS: {
      const posts = [...state.feed.posts]
      const postIndex = posts.findIndex(
        (post) => post._id === payload.data.data.post,
      )
      if (postIndex !== -1) {
        const commentIndex = posts[postIndex].comments.findIndex(
          (comment) => comment._id === payload.data.data._id,
        )
        if (commentIndex !== -1) {
          const reply = [...posts[postIndex].comments[commentIndex].replay]
          posts[postIndex].comments[commentIndex].replay = reply.concat(
            payload.data.data.replay,
          )
          return {
            ...state,
            feed: { ...state.feed, posts },
            postChangeDetect: true,
            errorMsg: '',
          }
        }
      }
    }
    case ACTION_LOAD_MORE_COMMENT_REPLY_FAIL: {
      return {
        ...state,
        errorMsg: "couldn't load reply",
        postChangeDetect: false,
      }
    }

    // socket action in home/feed...

    case SOCKET_ACTION_ADD_LIKE_TO_POST: {
      return {
        ...state,
        errorMsg: '',
        postChangeDetect: false,
      }
    }

    case SOCKET_ACTION_ADD_LIKE_TO_POST_RES: {
      const posts = [...state.feed.posts]
      let detailsPost
      posts.map((post) => {
        if (post._id === action.data.postId) {
          post.totalLike = action.data.totalLike
          if (action.data.liked) {
            post.postLikes.push({ userId: action.data.userId })
            detailsPost = post
          } else {
            post.postLikes = post.postLikes.filter(
              (postLike) => postLike.userId !== action.data.userId,
            )
            detailsPost = post
          }
        }
      })
      return {
        ...state,
        feed: { ...state.feed, posts },
        postChangeDetect: true,
      }
    }

    //adding comment for post...
    case SOCKET_ACTION_ADD_COMMENT_TO_POST: {
      return {
        ...state,
        errorMsg: '',
        postChangeDetect: null,
      }
    }

    case SOCKET_ACTION_ADD_COMMENT_TO_POST_RES: {
      const posts = [...state.feed.posts]
      let detailsPost
      posts.map((post) => {
        if (post._id === action.data.post) {
          post.totalComment = action.data.totalComment
          post.grandTotalComment = action.data.grandTotalComment
          post.comments.unshift(action.data)
          detailsPost = post
        }
      })
      return {
        ...state,
        feed: { ...state.feed, posts },
        postChangeDetect: true,
      }
    }

    //delete comment for post...
    case SOCKET_ACTION_ADD_COMMENT_REMOVE_TO_POST: {
      return {
        ...state,
        commentDeleted: false,
        postChangeDetect: null,
      }
    }
    case SOCKET_ACTION_ADD_COMMENT_REMOVE_TO_POST_RES: {
      const posts = [...state.feed.posts]
      let detailsPost = { ...state.post }
      posts.map((post) => {
        if (post._id === action.data.postId) {
          if (action.data.status) {
            const comments = post.comments.filter(
              (comment) => comment._id !== action.data.comment,
            )
            post.comments = comments
            post.totalComment = action.data.totalComment
            post.grandTotalComment = action.data.grandTotalComment
            detailsPost = post
          } else {
            const comments = [...post.comments]
            post.comments.map((comment) => {
              if (comment._id === action.data.comment) {
                const reply = comment.replay.filter(
                  (reply) => reply._id !== action.data.reply,
                )
                return (comment.replay = reply)
              }
            })
            post.comments = comments
            post.grandTotalComment = post.grandTotalComment - 1
            detailsPost = post
          }
        }
      })
      return {
        ...state,
        feed: { ...state.feed, posts },
        commentDeleted: true,
        postChangeDetect: true,
      }
    }

    // edit comment or reply with socket...
    case SOCKET_ACTION_ADD_COMMENT_EDIT_TO_POST: {
      return {
        ...state,
        commentEdited: false,
        postChangeDetect: null,
      }
    }

    case SOCKET_ACTION_ADD_COMMENT_EDIT_TO_POST_RES: {
      const posts = [...state.feed.posts]
      let detailsPost
      const postIndex = posts.findIndex(
        (post) => post._id === action.data.postId,
      )

      if (postIndex !== -1) {
        if (action.data.status) {
          const comments = posts[postIndex].comments
          const commentIndex = comments.findIndex(
            (comment) => comment._id === action.data.comment,
          )
          if (commentIndex !== -1) {
            posts[postIndex].comments[commentIndex].text = action.data.text
            detailsPost = posts[postIndex]
          }
        } else {
          const comments = posts[postIndex].comments
          const commentIndex = comments.findIndex(
            (comment) => comment._id === action.data.comment,
          )
          if (commentIndex !== -1) {
            const replies = comments[commentIndex].replay

            const replyIndex = replies.findIndex(
              (reply) => reply._id === action.data.reply,
            )

            if (replyIndex !== -1) {
              posts[postIndex].comments[commentIndex].replay[replyIndex].text =
                action.data.text
              detailsPost = posts[postIndex]
            }
          }
        }
      }

      return {
        ...state,
        feed: { ...state.feed, posts },
        commentEdited: true,
        postChangeDetect: true,
      }
    }

    //adding comment-reply for post...
    case SOCKET_ACTION_ADD_COMMENT_REPLY_TO_POST: {
      return {
        ...state,
        replyAdded: false,
        postChangeDetect: null,
      }
    }

    case SOCKET_ACTION_ADD_COMMENT_REPLY_TO_POST_RES: {
      const posts = [...state.feed.posts]
      let detailsPost
      posts.map((post) => {
        if (post._id === action.data.postId) {
          const comments = [...post.comments]
          post.grandTotalComment = action.data.grandTotalComment
          comments.map((comment) => {
            if (comment._id === action.data.commentId) {
              const reply = action.data

              comment.replay.push({
                createdAt: reply.createdAt,
                mention: reply.mention,
                text: reply.text,
                user: reply.user,
                _id: reply._id,
              })
              detailsPost = post
            }
          })
        }
      })
      return {
        ...state,
        feed: { ...state.feed, posts },
        replyAdded: true,
        postChangeDetect: true,
      }
    }

    default:
      return state
  }
}

export default homeReducer
