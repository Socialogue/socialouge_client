import { REHYDRATE } from 'redux-persist/constants'

import {
  ACTION_ADD_POST,
  ACTION_ADD_POST_FAIL,
  ACTION_ADD_POST_SUCCESS,
  ACTION_FAVORITE_POST,
  ACTION_FAVORITE_POST_FAIL,
  ACTION_FAVORITE_POST_SUCCESS,
  ACTION_FETCH_POST,
  ACTION_FETCH_POST_FAIL,
  ACTION_FETCH_POST_SUCCESS,
  ACTION_LIKE_POST,
  ACTION_LIKE_POST_FAIL,
  ACTION_LIKE_POST_SUCCESS,
  ACTION_LIKE_POST_SOCKET,
  ACTION_LIKE_POST_SOCKET_FAIL,
  ACTION_LIKE_POST_SOCKET_SUCCESS,
} from '../../constants/actionTypes'

export const initialState = {
  posts: [],
  postLoading: false,
  loaded: false,
  total: null,
  likedPosts: [],
}

const postReducer = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    /*case REHYDRATE: {
      const { posts } = payload;
      return {
        ...state,
        posts: posts
      };
    }*/

    case ACTION_FETCH_POST: {
      return {
        ...state,
        postLoading: true,
      }
    }

    case ACTION_FETCH_POST_SUCCESS: {
      return {
        ...state,
        posts: payload.data.data,
        postLoading: false,
        total: payload.data.totalItem,
      }
    }
    case ACTION_FETCH_POST_FAIL: {
      return {
        ...state,
        postLoading: false,
      }
    }

    case ACTION_ADD_POST: {
      return {
        ...state,
        postAddStatus: true,
      }
    }
    case ACTION_ADD_POST_SUCCESS: {
      return {
        ...state,
        posts: [...state.posts, payload.data.data],
        postAddStatus: false,
      }
    }
    case ACTION_ADD_POST_FAIL: {
      return {
        ...state,
        postAddStatus: false,
      }
    }
    case ACTION_LIKE_POST: {
      return {
        ...state,
        postLoading: true,
      }
    }

    case ACTION_LIKE_POST_SUCCESS: {
      return {
        ...state,
        likedPosts: payload.data.data,
        total: payload.data.totalItem,
        postLoading: false,
      }
    }

    case ACTION_LIKE_POST_FAIL: {
      return {
        ...state,
        postLoading: false,
      }
    }

    case ACTION_LIKE_POST_SOCKET: {
      return {
        ...state,
        postLoading: true,
      }
    }

    case ACTION_FAVORITE_POST: {
      return {
        ...state,
        postLoading: true,
      }
    }

    case ACTION_FAVORITE_POST_SUCCESS: {
      return {
        ...state,
        favoritePosts: payload.data.data.favoritePosts,
        postLoading: false,
        loaded: true,
      }
    }

    case ACTION_FAVORITE_POST_FAIL: {
      return {
        ...state,
        postLoading: false,
        loaded: true,
      }
    }

    default:
      return state
  }
}

export default postReducer
