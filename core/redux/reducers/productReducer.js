import { REHYDRATE } from 'redux-persist/constants'

import {
  ACTION_FETCH_FAVORITE_PRODUCTS,
  ACTION_FETCH_FAVORITE_PRODUCTS_FAIL,
  ACTION_FETCH_FAVORITE_PRODUCTS_SUCCESS,
  ACTION_FETCH_PRODUCT,
  ACTION_FETCH_PRODUCT_FAIL,
  ACTION_FETCH_PRODUCT_SUCCESS,
  ACTION_ADD_FAVORITE_PRODUCT,
  ACTION_ADD_FAVORITE_PRODUCT_FAIL,
  ACTION_ADD_FAVORITE_PRODUCT_SUCCESS,
  ACTION_FETCH_PROFILE_PUBLIC_PRODUCTS_FAIL,
  ACTION_FETCH_PROFILE_PUBLIC_PRODUCTS_SUCCESS,
  ACTION_FETCH_PROFILE_PUBLIC_PRODUCTS,
  ACTION_GET_CATEGORIES_FAIL,
  ACTION_GET_CATEGORIES_SUCCESS,
  ACTION_GET_CATEGORIES,
  ACTION_SET_CATEGORIES,
  ACTION_GET_ADDITIONAL_DATA,
  ACTION_GET_ADDITIONAL_DATA_SUCCESS,
  ACTION_GET_ADDITIONAL_DATA_FAIL,
  ACTION_GET_PRODUCT_DETAILS,
  ACTION_GET_PRODUCT_DETAILS_SUCCESS,
  ACTION_GET_PRODUCT_DETAILS_FAIL,
  ACTION_ADD_PRODUCT_TO_CART,
} from '../../constants/actionTypes'

export const initialState = {
  products: [],
  favoriteProducts: [],
  favoritePublicProducts: [],
  loadingProducts: false,
  errorMsg: '',
  categories: [],
  additionalData: {},
  categoryData: {},
  subCategoryData: {},
  innerCategoryData: {},
  productDetails: {},
}

const productReducer = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    // case REHYDRATE: {
    //   const { product } = payload;

    //   if (product) {
    //     return {
    //       ...state,
    //       cart: product
    //     };
    //   }

    //   return state;
    // }

    case ACTION_FETCH_PRODUCT: {
      return {
        ...state,
        loadingProducts: true,
      }
    }

    case ACTION_FETCH_PRODUCT_SUCCESS: {
      return {
        ...state,
        loadingProducts: false,
        shops: payload.data,
      }
    }

    case ACTION_FETCH_PRODUCT_FAIL: {
      return {
        ...state,
        loadingProducts: false,
        productLoadingFail: true,
      }
    }

    case ACTION_FETCH_FAVORITE_PRODUCTS: {
      return {
        ...state,
        loadingProducts: true,
      }
    }

    case ACTION_FETCH_FAVORITE_PRODUCTS_SUCCESS: {
      return {
        ...state,
        favoriteProducts: payload.data.data.favoriteProduct,
        total: payload.data.totalItem,
        loadingProducts: false,
      }
    }

    case ACTION_FETCH_FAVORITE_PRODUCTS_FAIL: {
      return {
        ...state,
        loadingProducts: false,
      }
    }

    case ACTION_FETCH_PROFILE_PUBLIC_PRODUCTS: {
      return {
        ...state,
        loadingProducts: true,
      }
    }

    case ACTION_FETCH_PROFILE_PUBLIC_PRODUCTS_SUCCESS: {
      return {
        ...state,
        favoritePublicProducts: payload.data.data.favoriteProduct,
        loadingProducts: false,
        total: payload.data.totalItem,
      }
    }

    case ACTION_FETCH_PROFILE_PUBLIC_PRODUCTS_FAIL: {
      return {
        ...state,
        loadingProducts: false,
      }
    }

    case ACTION_ADD_FAVORITE_PRODUCT: {
      return {
        ...state,
      }
    }

    case ACTION_ADD_FAVORITE_PRODUCT_SUCCESS: {
      return {
        ...state,
      }
    }

    case ACTION_ADD_FAVORITE_PRODUCT_FAIL: {
      return {
        ...state,
      }
    }

    case ACTION_GET_CATEGORIES: {
      return {
        ...state,
        errorMsg: '',
      }
    }

    case ACTION_GET_CATEGORIES_SUCCESS: {
      return {
        ...state,
        categories: payload.data.categories,
        errorMsg: '',
      }
    }

    case ACTION_GET_CATEGORIES_FAIL: {
      return {
        ...state,
        errorMsg: action.error.response.data.message,
      }
    }

    case ACTION_GET_ADDITIONAL_DATA: {
      return {
        ...state,
        errorMsg: '',
      }
    }

    case ACTION_GET_ADDITIONAL_DATA_SUCCESS: {
      return {
        ...state,
        additionalData: payload.data,
        errorMsg: '',
      }
    }

    case ACTION_GET_ADDITIONAL_DATA_FAIL: {
      return {
        ...state,
        errorMsg: 'Network error!',
      }
    }

    case ACTION_SET_CATEGORIES: {
      return {
        ...state,
        errorMsg: '',
        categoryData: payload.categoryData,
        subCategoryData: payload.subCategoryData,
        innerCategoryData: payload.innerCategoryData,
      }
    }

    case ACTION_GET_PRODUCT_DETAILS: {
      return {
        ...state,
        errorMsg: '',
      }
    }

    case ACTION_GET_PRODUCT_DETAILS_SUCCESS: {
      return {
        ...state,
        productDetails: payload.data.data,
        errorMsg: '',
      }
    }

    case ACTION_GET_PRODUCT_DETAILS_FAIL: {
      return {
        ...state,
        errorMsg: 'Something went wrong!',
      }
    }

    default:
      return state
  }
}

export default productReducer
