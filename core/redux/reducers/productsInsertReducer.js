import {
  ACTION_INSERT_PRODUCT_FAIL,
  ACTION_INSERT_PRODUCT_RESET_STATE,
  ACTION_INSERT_PRODUCT_SUCCESS
} from "../../constants/actionTypes";

export const initialState = {
  mainImageUrl: '',
  extraImageUrls: [],
  successMsg: '',
  errorMsg: ''
};

const productInsertReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {

    case ACTION_INSERT_PRODUCT_SUCCESS: {
      return {
        ...state,
        successMsg: 'Product Inserted Successfully'
      };
    }

    case ACTION_INSERT_PRODUCT_FAIL: {
      return {
        ...state,
        errorMsg: 'Product Insert failed'
      };
    }

    case ACTION_INSERT_PRODUCT_RESET_STATE: {
      return {
        ...state,
        successMsg: '',
        errorMsg: '',
      };
    }

    default:
      return state;
  }
};

export default productInsertReducer;
