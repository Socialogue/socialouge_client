import {
  ACTION_ADD_TO_FOLLOW,
  ACTION_ADD_TO_FOLLOW_FAIL,
  ACTION_ADD_TO_FOLLOW_SUCCESS,
  ACTION_AFTER_FOLLOW,
  ACTION_AFTER_UN_FOLLOW,
  ACTION_FETCH_PROFILE_OWNER,
  ACTION_FETCH_PROFILE_OWNER_FAIL,
  ACTION_FETCH_PROFILE_OWNER_SUCCESS,
  ACTION_FETCH_PROFILE_POSTS,
  ACTION_FETCH_PROFILE_POSTS_FAIL,
  ACTION_FETCH_PROFILE_POSTS_SUCCESS,
  ACTION_SET_SHOP_TEMP_COVER_IMAGE_SUCCESS,
  ACTION_SET_SHOP_TEMP_PROFILE_IMAGE_SUCCESS,
  ACTION_SET_TEMP_COVER_IMAGE_SUCCESS,
  ACTION_SET_TEMP_PROFILE_IMAGE_SUCCESS,
} from '../../constants/actionTypes'

export const initialState = {
  profileOwner: {},
  profilePosts: [],
  tmpProfileImage: '',
  tmpCoverImage: '',
  tmpShopProfileImage: '',
  tmpShopCoverImage: '',
  total: null,
}

const shopReducer = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case ACTION_FETCH_PROFILE_OWNER: {
      return {
        ...state,
      }
    }

    case ACTION_FETCH_PROFILE_OWNER_SUCCESS: {
      return {
        ...state,
        profileOwner: payload.data.data,
      }
    }

    case ACTION_FETCH_PROFILE_OWNER_FAIL: {
      return {
        ...state,
      }
    }

    case ACTION_FETCH_PROFILE_POSTS: {
      return {
        ...state,
      }
    }

    case ACTION_FETCH_PROFILE_POSTS_SUCCESS: {
      return {
        ...state,
        profilePosts: payload.data.data,
        total: payload.data.totalItem,
      }
    }

    case ACTION_FETCH_PROFILE_POSTS_FAIL: {
      return {
        ...state,
      }
    }

    // after profile image upload successfully...
    case ACTION_SET_TEMP_PROFILE_IMAGE_SUCCESS: {
      const profileImage = payload.data.profileImage

      return {
        ...state,
        tmpProfileImage: profileImage,
      }
    }

    // after cover image upload successfully...
    case ACTION_SET_TEMP_COVER_IMAGE_SUCCESS: {
      const coverImage = payload.data.coverImage

      return {
        ...state,
        tmpCoverImage: coverImage,
      }
    }

    // after shop profile image upload successfully...
    case ACTION_SET_SHOP_TEMP_PROFILE_IMAGE_SUCCESS: {
      const profileImage = payload.data.profileImage

      return {
        ...state,
        tmpShopProfileImage: profileImage,
      }
    }

    // after shop cover image upload successfully...
    case ACTION_SET_SHOP_TEMP_COVER_IMAGE_SUCCESS: {
      const coverImage = payload.data.coverImage

      return {
        ...state,
        tmpShopCoverImage: coverImage,
      }
    }

    case ACTION_AFTER_FOLLOW: {
      const profileOwner = { ...state.profileOwner }
      profileOwner.totalFollowers += 1
      return {
        ...state,
        profileOwner: profileOwner,
      }
    }

    case ACTION_AFTER_UN_FOLLOW: {
      const profileOwner = { ...state.profileOwner }
      profileOwner.totalFollowers -= 1
      return {
        ...state,
        profileOwner: profileOwner,
      }
    }

    default:
      return state
  }
}

export default shopReducer
