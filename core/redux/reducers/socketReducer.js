import {GOT_CHAT_MESSAGE} from "../../constants/actionTypes";

export const initialState = {
  chats: [],
  postLoading: false
};

const socketReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    /*case REHYDRATE: {
      const { posts } = payload;
      return {
        ...state,
        posts: posts
      };
    }*/

    case GOT_CHAT_MESSAGE: {
      console.log("GOT_CHAT_MESSAGE res ", { payload });
      return {
        ...state
      };
    }

    default:
      return state;
  }
};

export default socketReducer;
