import {
  ACTION_FILE_UPLOAD_FAIL,
  ACTION_FILE_UPLOAD_SUCCESS,
  ACTION_MULTIPLE_FILE_UPLOAD_FAIL,
  ACTION_MULTIPLE_FILE_UPLOAD_SUCCESS,
} from '../../constants/actionTypes'

export const initialState = {
  files: [],
  uploadkey: '',
  converted_paths: [],
  original_path: '',
  uploadUrls: [],
  file: '',
}

const fileUploadReducer = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case ACTION_FILE_UPLOAD_SUCCESS: {
      return {
        ...state,
        uploadkey: action.fileUploadKey,
        uploadUrls: [payload.data.original_path],
        original_path: payload.data.original_path,
        converted_paths: payload.data.converted_paths,
        file: payload.data.file,
      }
    }

    case ACTION_FILE_UPLOAD_FAIL: {
      return {
        ...state,
        uploadkey: '',
        uploadUrls: [],
        original_path: '',
        converted_paths: [],
      }
    }

    // multiple file upload...
    case ACTION_MULTIPLE_FILE_UPLOAD_SUCCESS: {
      console.log('ACTION_MULTIPLE_FILE_UPLOAD_SUCCESS res: ', payload.data)
      return {
        ...state,
        uploadkey: action.fileUploadKey,
        uploadUrls: [payload.data.original_path],
        original_path: payload.data.original_path,
        converted_paths: payload.data.converted_paths,
        file: payload.data.file,
      }
    }

    case ACTION_MULTIPLE_FILE_UPLOAD_FAIL: {
      return {
        ...state,
        uploadkey: '',
        uploadUrls: [],
        original_path: '',
        converted_paths: [],
      }
    }

    default:
      return state
  }
}

export default fileUploadReducer
