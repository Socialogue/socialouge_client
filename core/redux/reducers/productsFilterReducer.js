import {
  ACTION_GET_FILTERED_PRODUCTS,
  ACTION_GET_FILTERED_PRODUCTS_FAIL,
  ACTION_GET_FILTERED_PRODUCTS_SUCCESS,
  ACTION_SEARCH_PRODUCTS,
  ACTION_SEARCH_PRODUCTS_FAIL,
  ACTION_SEARCH_PRODUCTS_SUCCESS,
} from '../../constants/actionTypes'

export const initialState = {
  products: [],
  searchProducts: [],
  allColors: [],
  allSizes: [],
  totalProducts: null,
  loadingProducts: false,
  loaded: false,
  errorMsg: '',
  categories: [],
  additionalData: {},
  // categoryData: {},
  productDetails: {},
}

const productsFilterReducer = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    // case REHYDRATE: {
    //   const { product } = payload;

    //   if (product) {
    //     return {
    //       ...state,
    //       cart: product
    //     };
    //   }

    //   return state;
    // }

    case ACTION_GET_FILTERED_PRODUCTS: {
      return {
        ...state,
        loadingProducts: true,
      }
    }

    case ACTION_GET_FILTERED_PRODUCTS_SUCCESS: {
      let sizes = payload.data.allSizes.slice(1, 20)
      let colors = payload.data.allColors
      const key = 'lt'
      const colorkey = 'name'

      const uniqueSizes = [
        ...new Map(sizes.map((item) => [item[key], item])).values(),
      ]

      const uniqueColors = [
        ...new Map(colors.map((item) => [item[colorkey], item])).values(),
      ]

      return {
        ...state,
        loadingProducts: false,
        products: payload.data.data,
        allColors: uniqueColors,
        allSizes: uniqueSizes,
        totalProducts: payload.data.totalProduct,
        loaded: true,
      }
    }

    case ACTION_GET_FILTERED_PRODUCTS_FAIL: {
      return {
        ...state,
        loadingProducts: false,
        productLoadingFail: true,
        loaded: true,
      }
    }

    ////////////////////////////////////////////////////////////////////////////
    // products search for tag to a post...
    case ACTION_SEARCH_PRODUCTS: {
      return {
        ...state,
        loadingProducts: true,
      }
    }

    case ACTION_SEARCH_PRODUCTS_SUCCESS: {
      return {
        ...state,
        loadingProducts: false,
        searchProducts: payload.data.data,
      }
    }

    case ACTION_SEARCH_PRODUCTS_FAIL: {
      return {
        ...state,
        loadingProducts: false,
        productLoadingFail: true,
      }
    }

    default:
      return state
  }
}

export default productsFilterReducer
