import { REHYDRATE } from "redux-persist/constants";
import {
  ACTION_ADD_PRODUCT_TO_CART,
  ACTION_ADD_PRODUCT_TO_CART_DB,
  ACTION_ADD_PRODUCT_TO_CART_DB_FAIL,
  ACTION_ADD_PRODUCT_TO_CART_DB_SUCCESS,
  ACTION_DELETE_PRODUCT_FROM_CART_DB,
  ACTION_DELETE_PRODUCT_FROM_CART_DB_FAIL,
  ACTION_DELETE_PRODUCT_FROM_CART_DB_SUCCESS,
  ACTION_GET_PRODUCT_FROM_CART_DB,
  ACTION_GET_PRODUCT_FROM_CART_DB_FAIL,
  ACTION_GET_PRODUCT_FROM_CART_DB_SUCCESS,
  ACTION_UPDATE_PRODUCT_FROM_CART_DB,
  ACTION_UPDATE_PRODUCT_FROM_CART_DB_FAIL,
  ACTION_UPDATE_PRODUCT_FROM_CART_DB_SUCCESS,
  ACTION_UPDATE_PRODUCT_TO_CART
} from "../../constants/actionTypes";

export const initialState = {
  cart: [],
  finalPrice: 0
};

const cartReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case REHYDRATE: {
      const {
        product,
        shopId,
        shopOwnerId,
        clientId,
        productId,
        price,
        color,
        size,
        quantity,
        vat,
        totalPrice,
        vatAmmount,
        grandTotal
      } = payload;

      if (product) {
        return {
          ...state,
          cart: [
            ...state.cart,
            {
              product,
              shopId,
              shopOwnerId,
              clientId,
              productId,
              price,
              color,
              size,
              quantity,
              vat,
              totalPrice,
              vatAmmount,
              grandTotal
            }
          ],
          finalPrice: finalPrice
        };
      }
      return state;
    }

    case ACTION_ADD_PRODUCT_TO_CART: {
      const {
        product,
        shopId,
        shopOwnerId,
        clientId,
        productId,
        price,
        color,
        size,
        quantity,
        vat,
        totalPrice,
        vatAmmount,
        grandTotal
      } = payload;

      return {
        ...state,
        cart: [
          ...state.cart,
          {
            product,
            shopId,
            shopOwnerId,
            clientId,
            productId,
            price,
            color,
            size,
            quantity,
            vat,
            totalPrice,
            vatAmmount,
            grandTotal
          }
        ],
        finalPrice: payload.finalPrice
      };
    }

    case ACTION_UPDATE_PRODUCT_TO_CART: {
      return {
        ...state,
        cart: payload.updatedCart,
        finalPrice: payload.totalPrice
      };
    }

    case ACTION_GET_PRODUCT_FROM_CART_DB: {
      return {
        ...state
      };
    }
    case ACTION_GET_PRODUCT_FROM_CART_DB_SUCCESS: {
      return {
        ...state,
        cart: payload.data.cart,
        finalPrice: payload.data.totalPrice
      };
    }
    case ACTION_GET_PRODUCT_FROM_CART_DB_FAIL: {
      return {
        ...state,
        msg: "no data"
      };
    }

    case ACTION_ADD_PRODUCT_TO_CART_DB: {
      return {
        ...state
      };
    }
    case ACTION_ADD_PRODUCT_TO_CART_DB_SUCCESS: {
      return {
        ...state
      };
    }
    case ACTION_ADD_PRODUCT_TO_CART_DB_FAIL: {
      return {
        ...state,
        msg: "Please login first"
      };
    }

    case ACTION_UPDATE_PRODUCT_FROM_CART_DB: {
      return {
        ...state
      };
    }
    case ACTION_UPDATE_PRODUCT_FROM_CART_DB_SUCCESS: {
      return {
        ...state
      };
    }
    case ACTION_UPDATE_PRODUCT_FROM_CART_DB_FAIL: {
      return {
        ...state,
        msg: "Please login first"
      };
    }

    case ACTION_DELETE_PRODUCT_FROM_CART_DB: {
      return {
        ...state
      };
    }
    case ACTION_DELETE_PRODUCT_FROM_CART_DB_SUCCESS: {
      return {
        ...state,
        cart: payload.data.cart,
        finalPrice: payload.data.totalPrice
      };
    }
    case ACTION_DELETE_PRODUCT_FROM_CART_DB_FAIL: {
      return {
        ...state,
        msg: "Please login first"
      };
    }

    default:
      return state;
  }
};

export default cartReducer;
