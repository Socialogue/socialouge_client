import { REHYDRATE } from 'redux-persist/constants'

import {
  ACTION_UPDATE_PERSONAL_DATA,
  ACTION_UPDATE_PERSONAL_DATA_FAIL,
  ACTION_UPDATE_PERSONAL_DATA_SUCCESS,
  ACTION_UPDATE_EMAIL_DATA,
  ACTION_UPDATE_EMAIL_DATA_FAIL,
  ACTION_UPDATE_EMAIL_DATA_SUCCESS,
  ACTION_UPDATE_PASSWORD_DATA,
  ACTION_UPDATE_PASSWORD_DATA_FAIL,
  ACTION_UPDATE_PASSWORD_DATA_SUCCESS,
  ACTION_DELETE_USER_ACCOUNT,
  ACTION_DELETE_USER_ACCOUNT_FAIL,
  ACTION_DELETE_USER_ACCOUNT_SUCCESS,
  ACTION_UPDATE_DELIVERY_INFO,
  ACTION_UPDATE_DELIVERY_INFO_FAIL,
  ACTION_UPDATE_DELIVERY_INFO_SUCCESS,
  ACTION_GET_USER_INFO,
  ACTION_GET_USER_INFO_FAIL,
  ACTION_GET_USER_INFO_SUCCESS,
  ACTION_VERIFY_OTP_FOR_EMAIL_CHAGNE,
  ACTION_VERIFY_OTP_FOR_EMAIL_CHAGNE_FAIL,
  ACTION_VERIFY_OTP_FOR_EMAIL_CHAGNE_SUCCESS,
  ACTION_ADD_DELIVERY_INFO,
  ACTION_ADD_DELIVERY_INFO_SUCCESS,
  ACTION_ADD_DELIVERY_INFO_FAIL,
  ACTION_GET_DELIVERY_INFO,
  ACTION_GET_DELIVERY_INFO_SUCCESS,
  ACTION_GET_DELIVERY_INFO_FAIL,
  ACTION_GET_DELIVERY_TYPES_FAIL,
  ACTION_GET_DELIVERY_TYPES,
  ACTION_GET_DELIVERY_TYPES_SUCCESS,
} from '../../constants/actionTypes'

export const initialState = {
  personalData: {},
  deliveryData: [],
  deliveryTypes: [],
  emailData: {},
  passwordData: {},
  userData: {},
  successMsg: '',
  errorMsg: '',
  mailOrPass: '',
}

const shopReducer = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    /*case REHYDRATE: {
      const { shops } = payload;
      return {
        ...state,
        shops: shops
      };
    }*/

    case ACTION_UPDATE_PERSONAL_DATA: {
      return {
        ...state,
        successMsg: '',
      }
    }

    case ACTION_UPDATE_PERSONAL_DATA_SUCCESS: {
      return {
        ...state,
        personalData: payload.data,
        successMsg: payload.data.message,
        mailOrPass: '',
      }
    }

    case ACTION_UPDATE_PERSONAL_DATA_FAIL: {
      console.log({ action })
      return {
        ...state,
        mailOrPass: '',
        successMsg: '',
      }
    }

    case ACTION_UPDATE_EMAIL_DATA: {
      return {
        ...state,
        successMsg: '',
        errorMsg: '',
        mailOrPass: '',
      }
    }

    case ACTION_UPDATE_EMAIL_DATA_SUCCESS: {
      return {
        ...state,
        emailData: payload.data.data,
        successMsg: payload.data.message,
        mailOrPass: 'email',
      }
    }

    case ACTION_UPDATE_EMAIL_DATA_FAIL: {
      return {
        ...state,
        errorMsg: action.error.response.data.message,
        successMsg: '',
        mailOrPass: '',
      }
    }

    case ACTION_UPDATE_PASSWORD_DATA: {
      return {
        ...state,
        successMsg: '',
      }
    }

    case ACTION_UPDATE_PASSWORD_DATA_SUCCESS: {
      return {
        ...state,
        passwordData: payload.data,
        successMsg: payload.data.message,
      }
    }

    case ACTION_UPDATE_PASSWORD_DATA_FAIL: {
      return {
        ...state,
        successMsg: '',
      }
    }

    case ACTION_DELETE_USER_ACCOUNT: {
      return {
        ...state,
        successMsg: '',
      }
    }

    case ACTION_DELETE_USER_ACCOUNT_SUCCESS: {
      return {
        ...state,
        successMsg: payload.data.message,
      }
    }

    case ACTION_DELETE_USER_ACCOUNT_FAIL: {
      return {
        ...state,
        successMsg: '',
      }
    }

    case ACTION_GET_DELIVERY_INFO: {
      return {
        ...state,
        errorMsg: '',
      }
    }

    case ACTION_GET_DELIVERY_INFO_SUCCESS: {
      return {
        ...state,
        deliveryData: payload.data.data[0].delivery,
        // successMsg: payload.data.message
      }
    }

    case ACTION_GET_DELIVERY_INFO_FAIL: {
      return {
        ...state,
        errorMsg: 'something went wrong!',
      }
    }

    case ACTION_ADD_DELIVERY_INFO: {
      return {
        ...state,
        successMsg: '',
        errorMsg: '',
      }
    }

    case ACTION_ADD_DELIVERY_INFO_SUCCESS: {
      return {
        ...state,
        deliveryData: payload.data.data[0].delivery,
        successMsg: payload.data.message,
      }
    }

    case ACTION_ADD_DELIVERY_INFO_FAIL: {
      return {
        ...state,
        errorMsg: 'something went wrong!',
      }
    }

    case ACTION_UPDATE_DELIVERY_INFO: {
      return {
        ...state,
        successMsg: '',
      }
    }

    case ACTION_UPDATE_DELIVERY_INFO_SUCCESS: {
      return {
        ...state,
        deliveryData: payload.data.data[0].delivery,
        successMsg: payload.data.message,
      }
    }

    case ACTION_UPDATE_DELIVERY_INFO_FAIL: {
      return {
        ...state,
        successMsg: '',
      }
    }

    case ACTION_GET_USER_INFO: {
      return {
        ...state,
      }
    }

    case ACTION_GET_USER_INFO_SUCCESS: {
      return {
        ...state,
        userData: payload.data.data,
      }
    }

    case ACTION_GET_USER_INFO_FAIL: {
      return {
        ...state,
      }
    }

    case ACTION_VERIFY_OTP_FOR_EMAIL_CHAGNE: {
      return {
        ...state,
        successMsg: '',
        errorMsg: '',
      }
    }

    case ACTION_VERIFY_OTP_FOR_EMAIL_CHAGNE_SUCCESS: {
      return {
        ...state,
        successMsg: payload.data.message,
        errorMsg: '',
      }
    }

    case ACTION_VERIFY_OTP_FOR_EMAIL_CHAGNE_FAIL: {
      return {
        ...state,
        successMsg: '',
        errorMsg: action.error.response.data.message,
      }
    }

    case ACTION_GET_DELIVERY_TYPES: {
      return {
        ...state,
        successMsg: '',
        errorMsg: '',
      }
    }

    case ACTION_GET_DELIVERY_TYPES_SUCCESS: {
      return {
        ...state,
        deliveryTypes: payload.data.data,
        successMsg: payload.data.message,
        errorMsg: '',
      }
    }

    case ACTION_GET_DELIVERY_TYPES_FAIL: {
      return {
        ...state,
        successMsg: '',
        errorMsg: 'Network error',
      }
    }

    default:
      return state
  }
}

export default shopReducer
