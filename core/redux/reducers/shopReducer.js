import { REHYDRATE } from 'redux-persist/constants'

import {
  ACTION_CREATE_SHOP,
  ACTION_CREATE_SHOP_FAIL,
  ACTION_CREATE_SHOP_SUCCESS,
  ACTION_FETCH_FOLLOWINGS,
  ACTION_FETCH_FOLLOWINGS_FAIL,
  ACTION_FETCH_FOLLOWINGS_SUCCESS,
  ACTION_FETCH_SHOP,
  ACTION_FETCH_SHOP_FAIL,
  ACTION_FETCH_SHOP_SUCCESS,
  ACTION_GET_ALL_SHOPS,
  ACTION_GET_ALL_SHOPS_FAIL,
  ACTION_GET_ALL_SHOPS_SUCCESS,
  ACTION_GET_SHOP_DATA,
  ACTION_GET_SHOP_DATA_FAIL,
  ACTION_GET_SHOP_DATA_SUCCESS,
  ACTION_GET_SHOP_POST,
  ACTION_GET_SHOP_POST_FAIL,
  ACTION_GET_SHOP_POST_SUCCESS,
  ACTION_GET_SHOP_PRODUCT,
  ACTION_GET_SHOP_PRODUCT_FAIL,
  ACTION_GET_SHOP_PRODUCT_SUCCESS,
} from '../../constants/actionTypes'

export const initialState = {
  allShops: [],
  allShopsCount: null,
  shops: [],
  loadingShops: false,
  followings: {},
  follwingsCount: null,
  profileOwner: {},
  successMsg: '',
  errorMsg: '',
  shopPosts: [],
  shopProducts: [],
  shopData: null,
}

const shopReducer = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    /*case REHYDRATE: {
      const { shops } = payload;
      return {
        ...state,
        shops: shops
      };
    }*/

    case ACTION_CREATE_SHOP: {
      return {
        ...state,
        loadingShops: true,
        successMsg: '',
        errorMsg: '',
      }
    }

    case ACTION_CREATE_SHOP_SUCCESS: {
      console.log('SHOP RES: ', payload.data)
      return {
        ...state,
        loadingShops: false,
        successMsg: payload.data.message,
        errorMsg: '',
        shops: payload.data.data,
      }
    }

    case ACTION_CREATE_SHOP_FAIL: {
      return {
        ...state,
        loadingShops: false,
        shopLoadingFail: true,
        successMsg: '',
        errorMsg: action.error.response.data.message,
      }
    }

    case ACTION_FETCH_FOLLOWINGS: {
      return {
        ...state,
      }
    }

    case ACTION_FETCH_FOLLOWINGS_SUCCESS: {
      return {
        ...state,
        followings: payload.data.data.following,
        profileOwner: {
          fullName: payload.data.data.fullName,
          id: payload.data.data._id,
        },
        follwingsCount: payload.data.data.totalFollowing,
      }
    }

    case ACTION_FETCH_FOLLOWINGS_FAIL: {
      return {
        ...state,
      }
    }

    // Fetch all shops
    case ACTION_FETCH_SHOP: {
      return {
        ...state,
      }
    }

    case ACTION_FETCH_SHOP_SUCCESS: {
      return {
        ...state,
        shops: payload.data.data,
      }
    }

    case ACTION_FETCH_SHOP_FAIL: {
      return {
        ...state,
      }
    }

    case ACTION_GET_ALL_SHOPS: {
      return {
        ...state,
        successMsg: '',
        errorMsg: '',
      }
    }

    case ACTION_GET_ALL_SHOPS_SUCCESS: {
      return {
        ...state,
        allShops: payload.data.data,
        successMsg: payload.data.successMsg,
        errorMsg: '',
        allShopsCount: payload.data.totalItem,
      }
    }

    case ACTION_GET_ALL_SHOPS_FAIL: {
      return {
        ...state,
        successMsg: '',
        errorMsg: 'Network error',
      }
    }

    // get posts of shop...
    case ACTION_GET_SHOP_POST: {
      return {
        ...state,
        successMsg: '',
        errorMsg: '',
      }
    }

    case ACTION_GET_SHOP_POST_SUCCESS: {
      return {
        ...state,
        shopPosts: payload.data.data,
      }
    }

    case ACTION_GET_SHOP_POST_FAIL: {
      return {
        ...state,
        successMsg: '',
        errorMsg: 'Network error',
      }
    }

    // get products of shop...
    case ACTION_GET_SHOP_PRODUCT: {
      return {
        ...state,
        successMsg: '',
        errorMsg: '',
      }
    }

    case ACTION_GET_SHOP_PRODUCT_SUCCESS: {
      return {
        ...state,
        shopProducts: payload.data.data,
      }
    }

    case ACTION_GET_SHOP_PRODUCT_FAIL: {
      return {
        ...state,
        successMsg: '',
        errorMsg: 'Network error',
      }
    }

    // get shop DATA...
    case ACTION_GET_SHOP_DATA: {
      return {
        ...state,
        successMsg: '',
        errorMsg: '',
      }
    }

    case ACTION_GET_SHOP_DATA_SUCCESS: {
      return {
        ...state,
        shopData: payload.data.data,
      }
    }

    case ACTION_GET_SHOP_DATA_FAIL: {
      return {
        ...state,
        successMsg: '',
        errorMsg: 'Network error',
      }
    }

    default:
      return state
  }
}

export default shopReducer
