import {
  ACTION_FETCH_OUTFITS,
  ACTION_FETCH_OUTFITS_COLLECTION,
  ACTION_FETCH_OUTFITS_COLLECTION_FAIL,
  ACTION_FETCH_OUTFITS_COLLECTION_SUCCESS,
  ACTION_FETCH_OUTFITS_FAIL,
  ACTION_FETCH_OUTFITS_SUCCESS,
  ACTION_ADD_OUTFITS_COLLECTION,
  ACTION_ADD_OUTFITS_COLLECTION_SUCCESS,
  ACTION_ADD_OUTFITS_COLLECTION_FAIL,
  ACTION_CREATE_OUTFITS_COLLECTION,
  ACTION_CREATE_OUTFITS_COLLECTION_SUCCESS,
  ACTION_CREATE_OUTFITS_COLLECTION_FAIL
} from "../../constants/actionTypes";

export const initialState = {
  outfits: [],
  outfitsCollection: [],
  errorMsg:'',
  successMsg:'',
  createCollectionMsg:''
};

const outfitsReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {

    case ACTION_FETCH_OUTFITS: {
      console.log("ACTION_FETCH_OUTFITS", payload);
      return {
        ...state
      };
    }

    case ACTION_FETCH_OUTFITS_SUCCESS: {
      console.log("ACTION_FETCH_OUTFITS_SUCCESS res: ", payload);
      return {
        ...state,
        outfits: payload.data.data
      };
    }

    case ACTION_FETCH_OUTFITS_FAIL: {
      console.log("ACTION_FETCH_OUTFITS_FAIL", action);
      return {
        ...state,
        errorMsg:action.error.response.data.message
      };
    }

    case ACTION_FETCH_OUTFITS_COLLECTION: {
      console.log("ACTION_FETCH_OUTFITS_COLLECTION", payload);
      return {
        ...state
      };
    }

    case ACTION_FETCH_OUTFITS_COLLECTION_SUCCESS: {
      console.log("ACTION_FETCH_OUTFITS_COLLECTION_SUCCESS res: ", payload);
      return {
        ...state,
        outfitsCollection: payload.data.data.collections
      };
    }

    case ACTION_FETCH_OUTFITS_COLLECTION_FAIL: {
      console.log("ACTION_FETCH_OUTFITS_COLLECTION_FAIL", action);
      return {
        ...state,
        errorMsg:action.error.response.data.message
      };
    }

    case ACTION_ADD_OUTFITS_COLLECTION: {
      console.log("ACTION_ADD_OUTFITS_COLLECTION", payload);
      return {
        ...state,
        successMsg:''
      };
    }

    case ACTION_ADD_OUTFITS_COLLECTION_SUCCESS: {
      console.log("ACTION_ADD_OUTFITS_COLLECTION_SUCCESS res: ", payload);
      return {
        ...state,
        successMsg:payload.data.message
      };
    }

    case ACTION_ADD_OUTFITS_COLLECTION_FAIL: {
      console.log("ACTION_ADD_OUTFITS_COLLECTION_FAIL", action);
      return {
        ...state,
        errorMsg:action.error.response.data.message,
        successMsg:''
      };
    }

    case ACTION_CREATE_OUTFITS_COLLECTION: {
      console.log("ACTION_CREATE_OUTFITS_COLLECTION", payload);
      return {
        ...state,
        createCollectionMsg:''
      };
    }

    case ACTION_CREATE_OUTFITS_COLLECTION_SUCCESS: {
      console.log("ACTION_CREATE_OUTFITS_COLLECTION_SUCCESS res: ", payload);
      return {
        ...state,
        createCollectionMsg:payload.data.message,
        outfitsCollection: [...state.outfitsCollection, payload.data.data]
      };
    }

    case ACTION_CREATE_OUTFITS_COLLECTION_FAIL: {
      console.log("ACTION_CREATE_OUTFITS_COLLECTION_FAIL", action);
      return {
        ...state,
        errorMsg:action.error.response.data.message,
        createCollectionMsg:''
      };
    }




    default:
      return state;
  }
};

export default outfitsReducer;
