import {
  ACTION_ADD_REPLY_TO_COMMENT_TO_POST,
  ACTION_ADD_REPLY_TO_COMMENT_TO_POST_FAIL,
  ACTION_ADD_REPLY_TO_COMMENT_TO_POST_SUCCESS,
  ACTION_GET_COMMENTS_FOR_POST,
  ACTION_GET_COMMENTS_FOR_POST_FAIL,
  ACTION_GET_COMMENTS_FOR_POST_SUCCESS,
} from '../../constants/actionTypes'
import { SOCKET_ACTION_ADD_COMMENT_TO_POST } from '../../constants/socketActionTypes'

export const initialState = {
  comments: [],
  commentAdded: false,
}

const commentReducer = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    //get all comments...
    // case ACTION_GET_COMMENTS_FOR_POST: {
    //   return {
    //     ...state,
    //   }
    // }

    // case ACTION_GET_COMMENTS_FOR_POST_SUCCESS: {
    //   return {
    //     ...state,
    //     comments: payload.data.data,
    //   }
    // }

    // case ACTION_GET_COMMENTS_FOR_POST_FAIL: {
    //   return {
    //     ...state,
    //   }
    // }

    // //adding comment for post...
    // case SOCKET_ACTION_ADD_COMMENT_TO_POST: {
    //   console.log('SOCKET_ACTION_ADD_COMMENT_TO_POST: ', action)
    //   return {
    //     ...state,
    //   }
    // }

    // //adding reply to a comment for post...
    // case ACTION_ADD_REPLY_TO_COMMENT_TO_POST: {
    //   return {
    //     ...state,
    //   }
    // }

    // case ACTION_ADD_REPLY_TO_COMMENT_TO_POST_SUCCESS: {
    //   return {
    //     ...state,
    //   }
    // }

    // case ACTION_ADD_REPLY_TO_COMMENT_TO_POST_FAIL: {
    //   return {
    //     ...state,
    //   }
    // }

    default:
      return state
  }
}

export default commentReducer
