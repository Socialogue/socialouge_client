import { REHYDRATE } from 'redux-persist/constants'

import {
  OTP_VALIDATE_SUCCESS,
  LOGOUT,
  ACTION_LOGIN_SUCCESS,
  ACTION_LOGIN_FAIL,
  ACTION_REGISTER_SUCCESS,
  ACTION_REGISTER_FAIL,
  ACTION_REGISTER,
  ACTION_FACEBOOK_LOGIN,
  ACTION_FACEBOOK_LOGIN_SUCCESS,
  ACTION_FACEBOOK_LOGIN_FAIL,
  ACTION_OTP_VERIFY_SUCCESS,
  ACTION_OTP_VERIFY,
  ACTION_GOOGLE_LOGIN,
  ACTION_GOOGLE_LOGIN_SUCCESS,
  ACTION_GOOGLE_LOGIN_FAIL,
  ACTION_OTP_VERIFY_FAIL,
  ACTION_FORGOT_PASSWORD,
  ACTION_FORGOT_PASSWORD_SUCCESS,
  ACTION_FORGOT_PASSWORD_FAIL,
  ACTION_FORGOT_PASSWORD_OTP_VERIFY,
  ACTION_FORGOT_PASSWORD_OTP_VERIFY_SUCCESS,
  ACTION_FORGOT_PASSWORD_OTP_VERIFY_FAIL,
  ACTION_RESET_PASSWORD,
  ACTION_RESET_PASSWORD_SUCCESS,
  ACTION_RESET_PASSWORD_FAIL,
  ACTION_OTP_RESEND,
  ACTION_OTP_RESEND_SUCCESS,
  ACTION_OTP_RESEND_FAIL,
  ACTION_LOGIN,
  ACTION_PROFILE_IMAGE_UPLOAD_SUCCESS,
  ACTION_COVER_IMAGE_UPLOAD_SUCCESS,
  ACTION_ADD_TO_FOLLOW,
  ACTION_ADD_TO_FOLLOW_SUCCESS,
  ACTION_ADD_TO_FOLLOW_FAIL,
  ACTION_UN_FOLLOW,
  ACTION_UN_FOLLOW_SUCCESS,
  ACTION_UN_FOLLOW_FAIL,
} from '../../constants/actionTypes'

export const initialState = {
  expires_in: 0,
  isProfileComplete: false,
  loginProcessing: false,
  loginSuccess: false,
  registrationProcessing: false,
  registrationSuccess: false,
  token: '',
  user: {},
  isLoggedIn: false,
  networkError: '',
  registerErrorMessage: '',
  otpSendMsg: '',
  otpErrorMsg: '',
  needToVerify: false,
  myFollowings: [],
  followingStatus: null,
}

const authReducer = (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case REHYDRATE: {
      const { auth } = payload

      if (auth) {
        return {
          ...state,
          expires_in: auth.expires_in,
          isProfileComplete: auth.isProfileComplete,
          token: auth.token,
          user: auth.user,
          userToVerify: auth.userToVerify,
          verifyType: auth.verifyType,
          myFollowings: auth.myFollowings,
        }
      }

      return state
    }

    // when otp is validated, just do the login
    // TODO for rashid vai: first time register auto-login not getting users all infor, check localstorage please
    case OTP_VALIDATE_SUCCESS: {
      const { user } = payload.data
      return {
        ...z.data,
        isProfileComplete: !!user.name,
        loginProcessing: false,
      }
    }

    case ACTION_LOGIN: {
      return {
        ...state,
        loginErrorMsg: '',
      }
    }
    case ACTION_LOGIN_SUCCESS: {
      const needToVerify = payload.data.needToVerify

      if (needToVerify) {
        return {
          ...state,
          needToVerify: needToVerify,
          otpSendMsg: payload.data.message,
          loginErrorMsg: '',
        }
      }

      const followingIds = payload.data.data.following.reduce((acc, obj) => {
        return [...acc, obj.userId]
      }, [])
      return {
        token: payload.data.token,
        isLoggedIn: true,
        user: payload.data.data,
        totalFollowing: payload.data.data,
        loginErrorMsg: '',
        myFollowings: followingIds,
      }
    }

    case ACTION_LOGIN_FAIL: {
      const err = action.error.data ? action.error.data : ''
      return {
        ...state,
        networkError: err,
        needToVerify: false,
        loginErrorMsg: action.error.response.data.message,
      }
    }

    case ACTION_REGISTER: {
      return {
        ...state,
        registrationProcessing: true,
        registrationSuccess: false,
        registerErrorMessage: '',
      }
    }

    case ACTION_REGISTER_SUCCESS: {
      return {
        ...state,
        registrationProcessing: false,
        registrationSuccess: true,
        userToVerify: payload.data.data,
      }
    }

    case ACTION_REGISTER_FAIL: {
      return {
        ...state,
        registrationProcessing: false,
        registrationSuccess: false,
        registerErrorMessage: action.error.response.data.message,
      }
    }

    case ACTION_OTP_VERIFY: {
      return {
        ...state,
        otpErrorMsg: '',
      }
    }

    case ACTION_OTP_VERIFY_SUCCESS: {
      return {
        ...state,
        token: payload.data.token,
        loginSuccess: true,
        user: payload.data.data,
        otpErrorMsg: '',
      }
    }

    case ACTION_OTP_VERIFY_FAIL: {
      return {
        ...state,
        otpErrorMsg: action.error.response.data.message,
      }
    }

    case ACTION_OTP_RESEND: {
      return {
        ...state,
        otpSendMsg: '',
      }
    }

    case ACTION_OTP_RESEND_SUCCESS: {
      return {
        ...state,
        otpSendMsg: payload.data.message,
      }
    }

    case ACTION_OTP_RESEND_FAIL: {
      return {
        ...state,
      }
    }

    case ACTION_FACEBOOK_LOGIN: {
      return {
        ...state,
        loginProcessing: true,
      }
    }

    case ACTION_FACEBOOK_LOGIN_SUCCESS: {
      return {
        ...state,
        loginProcessing: false,
        loginSuccess: true,
        token: payload.data.token,
        user: payload.data.data,
      }
    }

    case ACTION_FACEBOOK_LOGIN_FAIL: {
      return {
        ...state,
        loginProcessing: false,
        loginSuccess: false,
      }
    }

    case ACTION_GOOGLE_LOGIN: {
      return {
        ...state,
        loginProcessing: true,
      }
    }

    case ACTION_GOOGLE_LOGIN_SUCCESS: {
      return {
        ...state,
        loginProcessing: false,
        loginSuccess: true,
        token: payload.data.token,
        user: payload.data.data,
      }
    }

    case ACTION_GOOGLE_LOGIN_FAIL: {
      return {
        ...state,
        loginProcessing: false,
        loginSuccess: false,
      }
    }

    case ACTION_FORGOT_PASSWORD: {
      return {
        ...state,
      }
    }

    case ACTION_FORGOT_PASSWORD_SUCCESS: {
      return {
        ...state,
        userToVerify: payload.data.data,
        verifyType: true,
      }
    }

    case ACTION_FORGOT_PASSWORD_FAIL: {
      return {
        ...state,
        verifyType: false,
      }
    }

    case ACTION_FORGOT_PASSWORD_OTP_VERIFY: {
      return {
        ...state,
        otpErrorMsg: '',
      }
    }

    case ACTION_FORGOT_PASSWORD_OTP_VERIFY_SUCCESS: {
      return {
        ...state,
        forgotOtpVerify: true,
        otpErrorMsg: '',
      }
    }

    case ACTION_FORGOT_PASSWORD_OTP_VERIFY_FAIL: {
      return {
        ...state,
        forgotOtpVerify: false,
        otpErrorMsg: action.error.response.data.message,
      }
    }

    case ACTION_RESET_PASSWORD: {
      return {
        ...state,
      }
    }

    case ACTION_RESET_PASSWORD_SUCCESS: {
      return {
        loginProcessing: false,
        loginSuccess: true,
        token: payload.data.token,
        user: payload.data.data,
        userToVerify: {},
      }
    }

    case ACTION_RESET_PASSWORD_FAIL: {
      return {
        ...state,
      }
    }

    // after profile image upload successfully...
    case ACTION_PROFILE_IMAGE_UPLOAD_SUCCESS: {
      const profileImage = payload.data.profileImage
      let user = { ...state.user, profileImage }

      return {
        ...state,
        user: user,
      }
    }

    // after cover image upload successfully...
    case ACTION_COVER_IMAGE_UPLOAD_SUCCESS: {
      const coverImage = payload.data.coverImage
      let user = { ...state.user, coverImage }

      return {
        ...state,
        user: user,
      }
    }

    case LOGOUT:
      return initialState

    case ACTION_ADD_TO_FOLLOW: {
      return {
        ...state,
        followingStatus: 'adding',
      }
    }

    case ACTION_ADD_TO_FOLLOW_SUCCESS: {
      const followingIds = [...state.myFollowings, payload.data.data.followerId]
      return {
        ...state,
        followingStatus: 'followed',
        myFollowings: followingIds,
      }
    }

    case ACTION_ADD_TO_FOLLOW_FAIL: {
      return {
        ...state,
        followingStatus: 'failed',
      }
    }

    case ACTION_UN_FOLLOW: {
      return {
        ...state,
        followingStatus: 'removing',
      }
    }

    case ACTION_UN_FOLLOW_SUCCESS: {
      const followingIds = [...state.myFollowings].filter(
        (id) => id !== payload.data.data.unFollowId,
      )

      return {
        ...state,
        followingStatus: 'unFollowed',
        myFollowings: followingIds,
      }
    }

    case ACTION_UN_FOLLOW_FAIL: {
      return {
        ...state,
        followingStatus: 'failed',
      }
    }

    default:
      return state
  }
}

export default authReducer
