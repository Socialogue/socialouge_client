import { REHYDRATE } from "redux-persist/constants";
import {
  ACTION_SET_ACTIVE_SHOP,
  ACTION_SET_SHOP_TEMP_COVER_IMAGE_SUCCESS,
  ACTION_SET_SHOP_TEMP_PROFILE_IMAGE_SUCCESS,
  ACTION_UPDATE_SHOP,
  ACTION_UPDATE_SHOP_FAIL,
  ACTION_UPDATE_SHOP_SUCCESS
} from "../../constants/actionTypes";

export const initialState = {
  activeProfile: "",
  activeShop: {},
  // products: [],
  updateShopError: "",
  updateShopSuccess: ""
};

const accountReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case REHYDRATE: {
      const { activeProfile, activeShop } = payload;

      if (activeProfile && activeShop) {
        return {
          ...state,
          activeProfile: payload.activeProfile,
          activeShop: payload.activeShop
        };
      }

      return state;
    }

    case ACTION_SET_ACTIVE_SHOP: {
      return {
        ...state,
        activeProfile: payload.activeProfile,
        activeShop: payload.activeShop
      };
    }

    //SHOP UPDATE
    case ACTION_UPDATE_SHOP: {
      return {
        ...state,
        updateShopSuccess: "",
        updateShopError: ""
      };
    }

    case ACTION_UPDATE_SHOP_SUCCESS: {
      return {
        ...state,
        updateShopSuccess: payload.data.message,
        updateShopError: "",
        activeShop: payload.data.data,
        activeProfile: payload.data.data.shopName
      };
    }

    case ACTION_UPDATE_SHOP_FAIL: {
      return {
        ...state,
        updateShopError: action.error.response.data.message,
        updateShopSuccess: ""
      };
    }

    //================================  after shop image upload    ========================================

    // after shop profile image upload successfully...
    case ACTION_SET_SHOP_TEMP_PROFILE_IMAGE_SUCCESS: {
      const profileImage = payload.data.profileImage;
      let activeShop = { ...state.activeShop, profileImage };

      return {
        ...state,
        activeShop: activeShop
      };
    }

    case ACTION_SET_SHOP_TEMP_COVER_IMAGE_SUCCESS: {
      const coverImage = payload.data.coverImage;
      console.log("ACTION_SET_SHOP_TEMP_COVER_IMAGE_SUCCESS res", coverImage);
      let activeShop = { ...state.activeShop, coverImage };

      return {
        ...state,
        activeShop: activeShop
      };
    }

    default:
      return state;
  }
};

export default accountReducer;
