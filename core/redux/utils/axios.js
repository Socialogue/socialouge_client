import get from 'lodash/get';

export function getAxiosRequest(action) {
  try {
    return action.meta.previousAction.payload.request;
  } catch (e) {
    throw new Error('Not an axios<API> response action');
  }
}

export function getAxiosResponseStatusCode(action) {
  return get(action, 'error.response.status');
}

export function isAxiosErrorResponse(action) {
  return !!(action.error && action.error.response && action.error.response.data);
}
