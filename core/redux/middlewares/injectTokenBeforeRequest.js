import { isAxiosRequestAction } from '../axios/helpers';
import { selectToken } from '../../selectors/user';

const injectTokenBeforeRequest = guestToken => store => next => (action) => {
  if (!isAxiosRequestAction(action)) {
    return next(action);
  }

  const token = selectToken(store.getState());
  const guestAllowed = action.payload.guestAllowed || false;

  if (guestAllowed) {
    // eslint-disable-next-line no-param-reassign
    action.payload.request.headers = {
      ...(action.payload.request.headers || {}),
      Authorization: `Bearer ${guestToken}`
    };
  }

  if (token) {
    // eslint-disable-next-line no-param-reassign
    action.payload.request.headers = {
      ...(action.payload.request.headers || {}),
      Authorization: `Bearer ${token}`
    };
  }

  return next(action);
};

export default injectTokenBeforeRequest;
