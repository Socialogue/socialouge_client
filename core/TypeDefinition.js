/**
 * @flow
 */

export type UserData = {
  address: ?string,
  facebook: boolean,
  is_complete: boolean,
  lat: ?number,
  lng: ?number,
  name: ?string,
  phone_number: string,
  profile_picture: ?{
    url: string,
    thumbnail: string,
    thumbnail_small: null | string
  },
  username: string
};


export type IntlShippingOption = {
  countries: Array<String>,
  oneItemCost: number,
  additionalItemCost: number,
  deliveryTimeFrom: number,
  deliveryTimeTo: number
}
