/**
 * @flow
 */
import {
  ACTION_POST_PRODUCT,
  ACTION_SET_SORT_VIEW,
  ACTION_GET_DELIVERY_ADDRESS,
  ACTION_SET_SORT_VIEW_FEED,
  ACTION_ADD_POST_TO_REDUX,
} from '../constants/actionTypes'
import { FEED, API_GET_DELIVERY_ADDRESS } from '../constants/apiEndpoints'

export const sortView = (viewType) => ({
  type: ACTION_SET_SORT_VIEW,
  payload: {
    data: viewType,
  },
})

export const sortViewFeed = (viewType) => ({
  type: ACTION_SET_SORT_VIEW_FEED,
  payload: {
    data: viewType,
  },
})

export const getPostProducts = () => ({
  type: ACTION_POST_PRODUCT,
  payload: {
    request: {
      url: FEED,
      method: 'GET',
    },
  },
})

export const getDeliverAddress = () => ({
  type: ACTION_GET_DELIVERY_ADDRESS,
  payload: {
    request: {
      url: API_GET_DELIVERY_ADDRESS,
      method: 'GET',
    },
  },
})

export const openPostInDetails = (post) => ({
  type: ACTION_ADD_POST_TO_REDUX,
  payload: {
    post: post,
  },
})
