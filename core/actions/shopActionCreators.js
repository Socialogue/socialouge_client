/**
 * @flow
 */
import {
  ACTION_CREATE_SHOP,
  ACTION_FETCH_FOLLOWINGS,
  ACTION_FETCH_SHOP,
  ACTION_GET_ALL_SHOPS,
  ACTION_GET_SHOP_DATA,
  ACTION_GET_SHOP_POST,
  ACTION_GET_SHOP_PRODUCT,
} from '../constants/actionTypes'
import {
  API_CREATE_SHOP,
  API_GET_ALL_SHOPS,
  API_GET_FOLLOWINGS,
  API_GET_SHOP,
  API_GET_SHOP_DATA,
  API_GET_SHOP_POSTS,
  API_GET_SHOP_PRODUCTS,
} from '../constants/apiEndpoints'

export const fetchShop = () => ({
  type: ACTION_FETCH_SHOP,
  payload: {
    request: {
      url: API_GET_SHOP,
      method: 'GET',
    },
  },
})

// old create shop method...

export const createShop = (
  {
    shopName,
    companyName,
    companyId,
    country,
    companyAddress,
    zipCode,
    companyPhone,
    supportPhone,
    email,
    supportEmail,
  },
  userId,
) => ({
  type: ACTION_CREATE_SHOP,
  payload: {
    request: {
      url: API_CREATE_SHOP,
      method: 'POST',
      data: {
        userId,
        shopName,
        companyName,
        companyId,
        country,
        companyAddress,
        zipCode,
        companyPhone,
        supportPhone,
        email,
        supportEmail,
      },
    },
  },
})

// modified one for create shop...
export const applyForShop = (
  {
    shopName,
    itemForSell,
    countryCode,
    phone,
    companyName,
    companyId,
    companyVat,
    companyEmail,
    registeredCompanyAddress,
    socialMedia1,
    socialMedia2,
    whereDidYouHere,
  },
  userId,
) => ({
  type: ACTION_CREATE_SHOP,
  payload: {
    request: {
      url: API_CREATE_SHOP,
      method: 'POST',
      data: {
        userId,
        shopName,
        itemForSell,
        countryCode,
        phone,
        companyName,
        companyId,
        companyVat,
        companyEmail,
        registeredCompanyAddress,
        socialMedia1,
        socialMedia2,
        whereDidYouHere,
      },
    },
  },
})

export const fetchFollowings = (userId, skip, limit) => ({
  type: ACTION_FETCH_FOLLOWINGS,
  payload: {
    request: {
      url: API_GET_FOLLOWINGS + `?userId=${userId}&skip=${skip}&limit=${limit}`,
      method: 'GET',
    },
  },
})

export const getAllShops = (limit = 10, offset = 0) => ({
  type: ACTION_GET_ALL_SHOPS,
  payload: {
    request: {
      url: `${API_GET_ALL_SHOPS}?skip=${offset}&limit=${limit}`,
      method: 'GET',
    },
  },
})

export const getShopPosts = (shopId, limit = 10, offset = 0) => ({
  type: ACTION_GET_SHOP_POST,
  payload: {
    request: {
      url: `${API_GET_SHOP_POSTS + shopId}?skip=${offset}&limit=${limit}`,
      method: 'GET',
    },
  },
})

export const getShopProducts = (shopId, limit = 10, offset = 0) => ({
  type: ACTION_GET_SHOP_PRODUCT,
  payload: {
    request: {
      url: `${API_GET_SHOP_PRODUCTS + shopId}?skip=${offset}&limit=${limit}`,
      method: 'GET',
    },
  },
})

export const getShopData = (shopId, limit = 10, offset = 0) => ({
  type: ACTION_GET_SHOP_DATA,
  payload: {
    request: {
      url: API_GET_SHOP_DATA + shopId,
      method: 'GET',
    },
  },
})
