/**
 * @flow
 */
import {
  ACTION_GET_ALL_PRODUCTS,
  ACTION_DELETE_PRODUCT,
  ACTION_GET_ALL_ORDERS,
  ACTION_ORDERS_STATUS_CHANGE
} from "../constants/actionTypes";
import {
  API_GET_ALL_PRODUCTS,
  API_DELETE_PRODUCT,
  API_GET_ALL_ORDERS,
  API_ORDER_STATUS_CHANGE
} from "../constants/apiEndpoints";

export const fetchAllProducts = (shopId, page = 0, limit = 5) => ({
  type: ACTION_GET_ALL_PRODUCTS,
  payload: {
    request: {
      url: API_GET_ALL_PRODUCTS + shopId + `?page=${page}&limit=${limit}`,
      method: "GET"
    }
  }
});

export const deleteProduct = listingItems => ({
  type: ACTION_DELETE_PRODUCT,
  payload: {
    request: {
      url: API_DELETE_PRODUCT,
      method: "DELETE",
      data: { listingItems: listingItems }
    }
  }
});

export const fetchAllOrders = (shopId, page = 0, limit = 5) => ({
  type: ACTION_GET_ALL_ORDERS,
  payload: {
    request: {
      url: API_GET_ALL_ORDERS + shopId + `?page=${page}&limit=${limit}`,
      method: "GET"
    }
  }
});

export const orderStatusChange = (orderIds, orderStatus) => ({
  type: ACTION_ORDERS_STATUS_CHANGE,
  payload: {
    request: {
      url: API_ORDER_STATUS_CHANGE,
      method: "POST",
      data: {
        orderIds: orderIds,
        orderStatus: orderStatus
      }
    }
  }
});
