/**
 * @flow
 */
import {
  ACTION_UPDATE_PERSONAL_DATA,
  ACTION_UPDATE_EMAIL_DATA,
  ACTION_FETCH_SHOP,
  ACTION_UPDATE_PASSWORD_DATA,
  ACTION_DELETE_USER_ACCOUNT,
  ACTION_UPDATE_DELIVERY_INFO,
  ACTION_GET_USER_INFO,
  ACTION_VERIFY_OTP_FOR_EMAIL_CHAGNE,
  ACTION_ADD_DELIVERY_INFO,
  ACTION_GET_DELIVERY_INFO,
  ACTION_GET_DELIVERY_TYPES
} from "../constants/actionTypes";
import {
  API_UPDATE_USER_PERSONAL_DATA,
  API_UPDATE_USER_EMAIL_DATA,
  API_GET_SHOP,
  API_UPDATE_USER_PASSWORD_DATA,
  API_DELETE_USER_ACCOUNT,
  API_UPDATE_DELIVERY_INFO,
  API_GET_USER_INFO,
  API_VERIFY_OTP_EMAIL_CHANGE,
  API_ADD_DELIVERY_INFO,
  API_GET_DELIVERY_INFO,
  API_GET_DELIVERY_TYPES
} from "../constants/apiEndpoints";

export const fetchPersonalData = () => ({
  type: ACTION_FETCH_SHOP,
  payload: {
    request: {
      url: API_GET_SHOP,
      method: "GET"
    }
  }
});

export const updatePersonalData = personalData => ({
  type: ACTION_UPDATE_PERSONAL_DATA,
  payload: {
    request: {
      url: API_UPDATE_USER_PERSONAL_DATA,
      method: "PATCH",
      data: personalData
    }
  }
});

export const updateEmailData = emailData => ({
  type: ACTION_UPDATE_EMAIL_DATA,
  payload: {
    request: {
      url: API_UPDATE_USER_EMAIL_DATA,
      method: "POST",
      data: emailData
    }
  }
});

export const updatePasswordData = passwordData => ({
  type: ACTION_UPDATE_PASSWORD_DATA,
  payload: {
    request: {
      url: API_UPDATE_USER_PASSWORD_DATA,
      method: "POST",
      data: passwordData
    }
  }
});

export const deleteAccount = () => ({
  type: ACTION_DELETE_USER_ACCOUNT,
  payload: {
    request: {
      url: API_DELETE_USER_ACCOUNT,
      method: "POST"
    }
  }
});

export const getDeliveryData = () => ({
  type: ACTION_GET_DELIVERY_INFO,
  payload: {
    request: {
      url: API_GET_DELIVERY_INFO,
      method: "GET"
    }
  }
});

export const getDeliveryTypes = () => ({
  type: ACTION_GET_DELIVERY_TYPES,
  payload: {
    request: {
      url: API_GET_DELIVERY_TYPES,
      method: "GET"
    }
  }
});

export const addDeliveryData = deliveryData => ({
  type: ACTION_ADD_DELIVERY_INFO,
  payload: {
    request: {
      url: API_ADD_DELIVERY_INFO,
      method: "POST",
      data: deliveryData
    }
  }
});

export const updateDeliveryData = deliveryData => ({
  type: ACTION_UPDATE_DELIVERY_INFO,
  payload: {
    request: {
      url: API_UPDATE_DELIVERY_INFO,
      method: "PATCH",
      data: deliveryData
    }
  }
});

export const getUserData = () => ({
  type: ACTION_GET_USER_INFO,
  payload: {
    request: {
      url: API_GET_USER_INFO,
      method: "GET"
    }
  }
});

export const verifyOtpForEmailChange = (otp, username, newEmail) => ({
  type: ACTION_VERIFY_OTP_FOR_EMAIL_CHAGNE,
  payload: {
    request: {
      url: API_VERIFY_OTP_EMAIL_CHANGE,
      method: "POST",
      data: {
        otp,
        username,
        newEmail
      }
    }
  }
});
