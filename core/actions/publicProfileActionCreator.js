/**
 * @flow
 */
import {
  ACTION_FETCH_FAVORITE_PRODUCTS,
  ACTION_FETCH_PRODUCT,
  ACTION_FETCH_PROFILE_OWNER,
  ACTION_FETCH_PROFILE_POSTS,
  ACTION_ADD_TO_FOLLOW,
  ACTION_UN_FOLLOW,
  ACTION_AFTER_FOLLOW,
  ACTION_AFTER_UN_FOLLOW,
} from '../constants/actionTypes'
import {
  API_GET_PROFILE_OWNER,
  API_GET_PROFILE_POSTS,
  API_ADD_TO_FOLLOW,
  API_UN_FOLLOW,
} from '../constants/apiEndpoints'

export const fetchProfileOwner = (userId) => ({
  type: ACTION_FETCH_PROFILE_OWNER,
  payload: {
    request: {
      url: API_GET_PROFILE_OWNER + userId,
      method: 'GET',
    },
  },
})

export const fetchProfilePosts = (userId, limit, skip) => ({
  type: ACTION_FETCH_PROFILE_POSTS,
  payload: {
    request: {
      url: API_GET_PROFILE_POSTS + userId + `?skip=${skip}&limit=${limit}`,
      method: 'GET',
    },
  },
})

export const addToFollow = (followingId, followerId, status) => ({
  type: ACTION_ADD_TO_FOLLOW,
  payload: {
    request: {
      url: API_ADD_TO_FOLLOW,
      method: 'POST',
      data: {
        followingId,
        followerId,
        status,
      },
    },
  },
})

export const unFollowUser = (status, userId, unFollowId, shopId) => ({
  type: ACTION_UN_FOLLOW,
  payload: {
    request: {
      url: API_UN_FOLLOW,
      method: 'DELETE',
      data: {
        status,
        userId,
        unFollowId,
        shopId,
      },
    },
  },
})

// only for shop following...
export const addToFollowShop = (followingId, shopId, status) => ({
  type: ACTION_ADD_TO_FOLLOW,
  payload: {
    request: {
      url: API_ADD_TO_FOLLOW,
      method: 'POST',
      data: {
        followingId,
        shopId,
        status,
      },
    },
  },
})

export const unFollowShop = (followingId, shopId, status) => ({
  type: ACTION_ADD_TO_FOLLOW,
  payload: {
    request: {
      url: API_UN_FOLLOW,
      method: 'DELETE',
      data: {
        followingId,
        shopId,
        status,
      },
    },
  },
})

export const favoriteProduct = (userId, productId) => ({
  type: API_ADD_FAVORITE_PRODUCT,
  payload: {
    request: {
      url: API_ADD_FAVORITE_PRODUCT,
      method: 'POST',
      data: {
        userId,
        productId,
      },
    },
  },
})

export const addFollower = () => ({
  type: ACTION_AFTER_FOLLOW,
  payload: {
    data: 1,
  },
})

export const removeFollower = () => ({
  type: ACTION_AFTER_UN_FOLLOW,
  payload: {
    data: -1,
  },
})
