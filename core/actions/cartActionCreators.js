/**
 * @flow
 */
import {
  ACTION_ADD_PRODUCT_TO_CART_DB,
  ACTION_GET_PRODUCT_FROM_CART_DB,
  ACTION_UPDATE_PRODUCT_FROM_CART_DB,
  ACTION_DELETE_PRODUCT_FROM_CART_DB
} from "../constants/actionTypes";
import {
  API_ADD_PRODUCT_TO_CART_DB,
  API_GET_PRODUCT_FROM_CART_DB,
  API_UPDATE_PRODUCT_FROM_CART_DB,
  API_DELETE_PRODUCT_FROM_CART_DB
} from "../constants/apiEndpoints";

export const getCartData = () => ({
  type: ACTION_GET_PRODUCT_FROM_CART_DB,
  payload: {
    request: {
      url: API_GET_PRODUCT_FROM_CART_DB,
      method: "GET"
    }
  }
});

export const addToCartDb = (cartData, cartPrice, clientId) => ({
  type: ACTION_ADD_PRODUCT_TO_CART_DB,
  payload: {
    request: {
      url: API_ADD_PRODUCT_TO_CART_DB,
      method: "POST",
      data: {
        clientId: clientId,
        finalPrice: cartPrice,
        products: cartData
      }
    }
  }
});

export const updateCartDb = (cartData, cartPrice, clientId) => ({
  type: ACTION_UPDATE_PRODUCT_FROM_CART_DB,
  payload: {
    request: {
      url: API_UPDATE_PRODUCT_FROM_CART_DB,
      method: "PATCH",
      data: {
        clientId: clientId,
        finalPrice: cartPrice,
        products: cartData
      }
    }
  }
});

export const deleteProductFromCart = clientId => ({
  type: ACTION_DELETE_PRODUCT_FROM_CART_DB,
  payload: {
    request: {
      url: API_DELETE_PRODUCT_FROM_CART_DB,
      method: "DELETE",
      data: { clientId }
    }
  }
});
