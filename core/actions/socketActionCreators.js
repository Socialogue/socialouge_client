/**
 * @flow
 */
import {
  ACTION_LOGIN,
  ACTION_REGISTER,
  ACTION_FACEBOOK_LOGIN,
  GET_USER_INFO,
  LOGOUT,
  SAVE_AUTH_CREDENTIALS,
  ACTION_GOOGLE_LOGIN,
  ACTION_OTP_VERIFY,
  ACTION_FORGOT_PASSWORD,
  ACTION_FORGOT_PASSWORD_OTP_VERIFY,
  ACTION_RESET_PASSWORD
} from '../constants/actionTypes';

import {
  API_LOGIN,
  API_REGISTER,
  USER_RESOURCE_API,
  VALIDATE_OTP,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_OTP_VERIFY_API,
  RESET_PASSWORD_API
} from '../constants/apiEndpoints';

export const sendChatMessage = (chat) =>{
  return {
    type: "SEND_CHAT_MESSAGE",
    meta: {remote: true},
    chat
  }
}

export const authenticateByGoogle = (
  name,
  email,
  profileImage,
  accessToken,
  accountType
) => ({
  type: ACTION_GOOGLE_LOGIN,
  payload: {
    request: {
      url: API_LOGIN,
      method: 'POST',
      data: {
        name,
        email,
        profileImage,
        accessToken,
        accountType
      }
    }
  }
});

export const register = (name, username, password, accountType) => ({
  type: ACTION_REGISTER,
  payload: {
    request: {
      url: API_REGISTER,
      method: 'POST',
      data: {
        name,
        username,
        password,
        accountType
      }
    }
  }
});

export const login = (username, password, accountType) => ({
  type: ACTION_LOGIN,
  payload: {
    request: {
      url: API_LOGIN,
      method: 'POST',
      data: {
        username,
        password,
        accountType
      }
    }
  }
});

export const logOut = () => ({
  type: LOGOUT
});

export const getUserInfo = (returnRejectedPromiseOnError: boolean = false) => ({
  type: GET_USER_INFO,
  payload: {
    options: {
      returnRejectedPromiseOnError
    },
    request: {
      url: USER_RESOURCE_API,
      method: 'GET'
    }
  }
});

export const saveAuthCredentials = (data: {
  token: string,
  expiresIn: number
}) => ({
  type: SAVE_AUTH_CREDENTIALS,
  payload: {
    token: data.token,
    expiresIn: data.expiresIn
  }
});

export const verifyOtp = (otp, username, accountType) => ({
  type: ACTION_OTP_VERIFY,
  payload: {
    request: {
      url: VALIDATE_OTP,
      method: 'POST',
      data: {
        otp,
        username,
        accountType
      }
    }
  }
});

export const forgotPassword = (username, accountType) => ({
  type: ACTION_FORGOT_PASSWORD,
  payload: {
    request: {
      url: FORGOT_PASSWORD,
      method: 'POST',
      data: {
        username,
        accountType
      }
    }
  }
});

export const forgotPasswordOtpVerify = (username, accountType, otp) => ({
  type: ACTION_FORGOT_PASSWORD_OTP_VERIFY,
  payload: {
    request: {
      url: FORGOT_PASSWORD_OTP_VERIFY_API,
      method: 'POST',
      data: {
        username,
        accountType,
        otp
      }
    }
  }
});

export const resetPassword = (username, accountType, password, repeatPassword) => ({
  type: ACTION_RESET_PASSWORD,
  payload: {
    request: {
      url: RESET_PASSWORD_API,
      method: 'POST',
      data: {
        username,
        accountType,
        password,
        repeatPassword
      }
    }
  }
});
