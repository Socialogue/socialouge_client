/**
 * @flow
 */
import { OTP_LOGIN, OTP_SET_PHONENUMBER, OTP_VALIDATE, RESEND_OTP } from '../constants/actionTypes';
import { REQUEST_OTP_API, RESEND_OTP_API, VALIDATE_OTP_API } from '../constants/apiEndpoints';

export const requestOtp = (phoneNumber: string) => {
  const API_ENDPOINT = `${REQUEST_OTP_API.replace(':phone_number', number)}`;
  return {
    type: OTP_LOGIN,
    payload: {
      request: {
        url: API_ENDPOINT,
        method: 'GET'
      }
    }
  };
};

export const resendOtp = (phoneNumber: string) => {
  const API_ENDPOINT = `${RESEND_OTP_API.replace(':phone_number', number)}`;
  return {
    type: RESEND_OTP,
    payload: {
      request: {
        url: API_ENDPOINT,
        method: 'GET'
      }
    }
  };
};

export const validateOtp = (otp: string, phone_number: string, token: string) => ({
  type: OTP_VALIDATE,
  payload: {
    request: {
      url: VALIDATE_OTP_API,
      method: 'POST',
      data: {
        otp,
        phone_number,
        token
      }
    }
  }
});

export const setPhoneNumber = (phoneNumber: string) => ({
  type: OTP_SET_PHONENUMBER,
  payload: {
    phoneNumber: number
  }
});
