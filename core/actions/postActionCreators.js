/**
 * @flow
 */
import {
  ACTION_FETCH_POST,
  ACTION_ADD_POST,
  ACTION_LIKE_POST,
  ACTION_LIKE_POST_SOCKET,
  ACTION_FETCH_LIKED_POST,
  ACTION_FAVORITE_POST,
} from '../constants/actionTypes'
import {
  API_POSTS,
  API_ADD_POST,
  API_MY_POSTS,
  API_REACT_TO_POST,
  API_MY_LIKED_POSTS,
  API_MY_FAVORITE_POSTS,
} from '../constants/apiEndpoints'
import { SOCKET_ACTION_ADD_LIKE_TO_POST } from '../constants/socketActionTypes'

export const fetchPosts = (userId, limit, skip) => ({
  type: ACTION_FETCH_POST,
  payload: {
    request: {
      url: `${API_MY_POSTS}${userId}?skip=${skip}&limit=${limit}`,
      method: 'GET',
    },
  },
})

export const fetchLikedPosts = (userId, limit, skip) => ({
  type: ACTION_LIKE_POST,
  payload: {
    request: {
      url: `${API_MY_LIKED_POSTS}?userId=${userId}&limit=${limit}&skip=${skip}`,
      method: 'GET',
      data: {
        userId,
      },
    },
  },
})

export const fetchFavoritePosts = (userId) => ({
  type: ACTION_FAVORITE_POST,
  payload: {
    request: {
      url: API_MY_FAVORITE_POSTS + '?userId=' + userId,
      method: 'GET',
      data: {
        userId,
      },
    },
  },
})

export const addPost = (
  content,
  url,
  taggedProductIds,
  userId,
  shopId = null,
) => ({
  type: ACTION_ADD_POST,
  payload: {
    request: {
      url: API_ADD_POST,
      method: 'POST',
      data: {
        userId: userId,
        shopId: shopId,
        content: content,
        images: url,
        tags: taggedProductIds,
      },
    },
  },
})

export const reactToPost = (userId, postId) => ({
  type: ACTION_LIKE_POST,
  payload: {
    request: {
      url: API_REACT_TO_POST,
      method: 'POST',
      data: {
        userId,
        postId,
      },
    },
  },
})

export const reactToPostSocket = (userId, postId) => ({
  type: SOCKET_ACTION_ADD_LIKE_TO_POST,
  meta: { remote: true },
  userId: userId,
  postId: postId,
})
