/**
 * @flow
 */
import {
  ACTION_FETCH_OUTFITS,
  ACTION_FETCH_OUTFITS_COLLECTION,
  ACTION_ADD_OUTFITS_COLLECTION,
  ACTION_CREATE_OUTFITS_COLLECTION
} from "../constants/actionTypes";
import {
  API_GET_OUTFIT,
  API_GET_OUTFIT_COLLECTION,
  API_ADD_OUTFIT_COLLECTION,
  API_CREATE_OUTFIT_COLLECTION
} from "../constants/apiEndpoints";

export const fetchOutfits = () => ({
  type: ACTION_FETCH_OUTFITS,
  payload: {
    request: {
      url: API_GET_OUTFIT,
      method: "GET"
    }
  }
});

export const fetchOutfitCollection = () => ({
  type: ACTION_FETCH_OUTFITS_COLLECTION,
  payload: {
    request: {
      url: API_GET_OUTFIT_COLLECTION,
      method: "GET"
    }
  }
});

export const addOutfitsCollection = (collectionId, productId) => ({
  type: ACTION_ADD_OUTFITS_COLLECTION,
  payload: {
    request: {
      url: API_ADD_OUTFIT_COLLECTION,
      method: "POST",
      data: {
        collectionId: collectionId,
        productId: productId
      }
    }
  }
});

export const createCollection = (collectionName) => ({
  type: ACTION_CREATE_OUTFITS_COLLECTION,
  payload: {
    request: {
      url: API_CREATE_OUTFIT_COLLECTION,
      method: "POST",
      data: {
        name: collectionName
      }
    }
  }
});
