/**
 * @flow
 */
import {
  ACTION_ALL_FOR_TAG_PRODUCTS,
  ACTION_GET_FILTERED_PRODUCTS,
  ACTION_SEARCH_PRODUCTS,
} from '../constants/actionTypes'
import {
  API_ALL_PRODUCTS_FOR_TAG,
  API_GET_FILTERED_PRODUCTS,
  API_SEARCH_PRODUCTS,
} from '../constants/apiEndpoints'

export const fetchFilterdProducts = (key, id) => ({
  type: ACTION_GET_FILTERED_PRODUCTS,
  payload: {
    request: {
      url: API_GET_FILTERED_PRODUCTS,
      method: 'POST',
      data: { [key]: id },
    },
  },
})

export const fetchProductsWithPriceFilter = (
  key,
  query,
  minPrice,
  maxPrice,
  colors,
  sizes,
) => ({
  type: ACTION_GET_FILTERED_PRODUCTS,
  payload: {
    request: {
      url: API_GET_FILTERED_PRODUCTS,
      method: 'POST',
      data: { [key]: query, minPrice, maxPrice, colors, sizes },
    },
  },
})

// products search action creator for adding product to post/tag product to post..
export const searchProducts = (userId, search, status) => ({
  type: ACTION_SEARCH_PRODUCTS,
  payload: {
    request: {
      url: API_SEARCH_PRODUCTS,
      method: 'POST',
      data: { userId, search, status },
    },
  },
})

// products action creator for products for authenticated user..
export const getAllProducts = (userId, status) => ({
  type: ACTION_SEARCH_PRODUCTS,
  payload: {
    request: {
      url: API_SEARCH_PRODUCTS,
      method: 'POST',
      data: { userId, status },
    },
  },
})
