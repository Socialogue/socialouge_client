/**
 * @flow
 */
import {
  ACTION_FACEBOOK_LOGIN,
  ACTION_FORGOT_PASSWORD,
  ACTION_FORGOT_PASSWORD_OTP_VERIFY,
  ACTION_GOOGLE_LOGIN,
  ACTION_LOGIN,
  ACTION_OTP_RESEND,
  ACTION_OTP_VERIFY,
  ACTION_REGISTER,
  ACTION_RESET_PASSWORD,
  GET_USER_INFO,
  LOGOUT,
  SAVE_AUTH_CREDENTIALS,
} from '../constants/actionTypes'
import {
  API_LOGIN,
  API_REGISTER,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_OTP_VERIFY_API,
  RESEND_OTP,
  RESET_PASSWORD_API,
  USER_RESOURCE_API,
  VALIDATE_OTP,
} from '../constants/apiEndpoints'

export const authenticateByFacebook = (
  name,
  email,
  profileImage,
  accessToken,
  accountType,
) => ({
  type: ACTION_FACEBOOK_LOGIN,
  payload: {
    request: {
      url: API_LOGIN,
      method: 'POST',
      data: {
        name,
        email,
        profileImage,
        accessToken,
        accountType,
      },
    },
  },
})

export const authenticateByGoogle = (
  name,
  email,
  profileImage,
  accessToken,
  accountType,
) => ({
  type: ACTION_GOOGLE_LOGIN,
  payload: {
    request: {
      url: API_LOGIN,
      method: 'POST',
      data: {
        name,
        email,
        profileImage,
        accessToken,
        accountType,
      },
    },
  },
})

export const register = (fullName, username, password, accountType) => ({
  type: ACTION_REGISTER,
  payload: {
    request: {
      url: API_REGISTER,
      method: 'POST',
      data: {
        fullName,
        username,
        password,
        accountType,
      },
    },
  },
})

export const login = (username, password, accountType) => ({
  type: ACTION_LOGIN,
  payload: {
    request: {
      url: API_LOGIN,
      method: 'POST',
      data: {
        username,
        password,
        accountType,
      },
    },
  },
})

export const logOut = () => ({
  type: LOGOUT,
})

export const getUserInfo = (returnRejectedPromiseOnError = false) => ({
  type: GET_USER_INFO,
  payload: {
    options: {
      returnRejectedPromiseOnError,
    },
    request: {
      url: USER_RESOURCE_API,
      method: 'GET',
    },
  },
})

export const saveAuthCredentials = (data) => ({
  type: SAVE_AUTH_CREDENTIALS,
  payload: {
    token: data.token,
    expiresIn: data.expiresIn,
  },
})

export const verifyOtp = (otp, username, accountType) => ({
  type: ACTION_OTP_VERIFY,
  payload: {
    request: {
      url: VALIDATE_OTP,
      method: 'POST',
      data: {
        otp,
        username,
        accountType,
      },
    },
  },
})

export const resendOtp = (username, accountType) => ({
  type: ACTION_OTP_RESEND,
  payload: {
    request: {
      url: RESEND_OTP,
      method: 'POST',
      data: {
        username,
        accountType,
      },
    },
  },
})

export const forgotPassword = (username, accountType) => ({
  type: ACTION_FORGOT_PASSWORD,
  payload: {
    request: {
      url: FORGOT_PASSWORD,
      method: 'POST',
      data: {
        username,
        accountType,
      },
    },
  },
})

export const forgotPasswordOtpVerify = (username, accountType, otp) => ({
  type: ACTION_FORGOT_PASSWORD_OTP_VERIFY,
  payload: {
    request: {
      url: FORGOT_PASSWORD_OTP_VERIFY_API,
      method: 'POST',
      data: {
        username,
        accountType,
        otp,
      },
    },
  },
})

export const resetPassword = (
  username,
  accountType,
  password,
  repeatPassword,
) => ({
  type: ACTION_RESET_PASSWORD,
  payload: {
    request: {
      url: RESET_PASSWORD_API,
      method: 'POST',
      data: {
        username,
        accountType,
        password,
        repeatPassword,
      },
    },
  },
})
