/**
 * @flow
 */
import {
  ACTION_ADD_FAVORITE_PRODUCT,
  ACTION_FETCH_FAVORITE_PRODUCTS,
  ACTION_FETCH_PRODUCT,
  ACTION_FETCH_PROFILE_PUBLIC_PRODUCTS,
  ACTION_GET_CATEGORIES,
  ACTION_SET_CATEGORIES,
  ACTION_GET_ADDITIONAL_DATA,
  ACTION_GET_PRODUCT_DETAILS,
  ACTION_ADD_PRODUCT_TO_CART,
  ACTION_UPDATE_PRODUCT_TO_CART,
  ACTION_REACT_TO_PRODUCT,
} from '../constants/actionTypes'
import {
  API_ADD_FAVORITE_PRODUCT,
  API_GET_FAVORITE_PRODUCTS,
  API_GET_PRODUCT,
  API_GET_PUBLIC_FAVORITE_PRODUCTS,
  API_GET_CATEGORIES,
  API_GET_ADDITIONAL_DATA,
  API_GET_PRODUCT_DETAILS,
  API_REACT_TO_PRODUCT,
} from '../constants/apiEndpoints'

export const fetchProduct = () => ({
  type: ACTION_FETCH_PRODUCT,
  payload: {
    request: {
      url: API_GET_PRODUCT,
      method: 'GET',
    },
  },
})

export const fetchFavoriteProducts = (userId, limit, skip) => ({
  type: ACTION_FETCH_FAVORITE_PRODUCTS,
  payload: {
    request: {
      url: `${API_GET_FAVORITE_PRODUCTS}?userId=${userId}&skip=${skip}&limit=${limit}`,
      method: 'GET',
    },
  },
})

export const fetchPublicFavoriteProducts = (userId, limit, skip) => ({
  type: ACTION_FETCH_PROFILE_PUBLIC_PRODUCTS,
  payload: {
    request: {
      url:
        API_GET_PUBLIC_FAVORITE_PRODUCTS +
        userId +
        `?skip=${skip}&limit=${limit}`,
      method: 'GET',
    },
  },
})

export const favoriteProduct = (userId, productId) => ({
  type: ACTION_ADD_FAVORITE_PRODUCT,
  payload: {
    request: {
      url: API_ADD_FAVORITE_PRODUCT,
      method: 'POST',
      data: {
        userId,
        productId,
      },
    },
  },
})

export const getCategories = () => ({
  type: ACTION_GET_CATEGORIES,
  payload: {
    request: {
      url: API_GET_CATEGORIES,
      method: 'GET',
    },
  },
})

export const getSubCategories = (catId) => ({
  type: ACTION_GET_CATEGORIES,
  payload: {
    request: {
      url: API_GET_CATEGORIES,
      method: 'GET',
    },
  },
})

export const setCatDataForProduct = (cat, sub, inner) => ({
  type: ACTION_SET_CATEGORIES,
  payload: {
    categoryData: cat,
    subCategoryData: sub,
    innerCategoryData: inner,
  },
})

export const getAdditionalDataForProduct = (shopId) => ({
  type: ACTION_GET_ADDITIONAL_DATA,
  payload: {
    request: {
      url: API_GET_ADDITIONAL_DATA + shopId,
      method: 'GET',
    },
  },
})

export const getProductDetails = (productId) => ({
  type: ACTION_GET_PRODUCT_DETAILS,
  payload: {
    request: {
      url: API_GET_PRODUCT_DETAILS + productId,
      method: 'GET',
    },
  },
})

export const addToCart = (
  {
    product,
    shopId,
    shopOwnerId,
    clientId,
    productId,
    price,
    color,
    size,
    quantity,
    vat,
    totalPrice,
    vatAmmount,
    grandTotal,
  },
  finalPrice,
) => ({
  type: ACTION_ADD_PRODUCT_TO_CART,
  payload: {
    product,
    shopId,
    shopOwnerId,
    clientId,
    productId,
    price,
    color,
    size,
    quantity,
    vat,
    totalPrice,
    vatAmmount,
    grandTotal,
    finalPrice,
  },
})

export const updateCart = (product, totalPrice) => ({
  type: ACTION_UPDATE_PRODUCT_TO_CART,
  payload: {
    updatedCart: product,
    totalPrice,
  },
})

export const reactToProduct = (userId, productId) => ({
  type: ACTION_REACT_TO_PRODUCT,
  payload: {
    request: {
      url: API_REACT_TO_PRODUCT,
      method: 'POST',
      data: {
        userId,
        productId,
      },
    },
  },
})
