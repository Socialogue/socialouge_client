/**
 * @flow
 */
import {
  ACTION_INSERT_PRODUCT,
  ACTION_INSERT_PRODUCT_RESET_STATE, ACTION_INSERT_SHIPPING_FROM_TEMPLATE,
  ACTION_INSERT_SHIPPING_TO_TEMPLATE
} from "../constants/actionTypes";
import {API_INSERT_PRODUCT, API_INSERT_SHIPPING_TEMPLATE} from "../constants/apiEndpoints";

export const insertProduct = (payload) => ({
  type: ACTION_INSERT_PRODUCT,
  payload: {
    request: {
      url: API_INSERT_PRODUCT,
      method: "POST",
      data: {
        ...payload
      }
    }
  }
});

export const insertShippingToTemplate = (payload) => ({
  type: ACTION_INSERT_SHIPPING_TO_TEMPLATE,
  payload: {
    request: {
      url: API_INSERT_SHIPPING_TEMPLATE,
      method: "POST",
      data: {
        ...payload
      }
    }
  }
});

export const insertShippingFromTemplate = (payload) => ({
  type: ACTION_INSERT_SHIPPING_FROM_TEMPLATE,
  payload: {
    request: {
      url: API_INSERT_SHIPPING_TEMPLATE,
      method: "POST",
      data: {
        ...payload
      }
    }
  }
});

export const resetState = () => ({
  type: ACTION_INSERT_PRODUCT_RESET_STATE,
});
