/**
 * @flow
 */
import {
  ACTION_LOAD_MORE_COMMENT,
  ACTION_LOAD_MORE_COMMENT_REPLY,
} from '../constants/actionTypes'
import {
  API_ADD_REPLY_TO_COMMENT_TO_POST,
  API_LOAD_MORE_COMMENT,
  API_LOAD_MORE_COMMENT_REPLY,
} from '../constants/apiEndpoints'
import {
  SOCKET_ACTION_ADD_COMMENT_REPLY_TO_POST,
  SOCKET_ACTION_ADD_COMMENT_TO_POST,
  SOCKET_ACTION_ADD_REPLY_TO_COMMENT_TO_POST,
  SOCKET_ACTION_ADD_COMMENT_REMOVE_TO_POST,
  SOCKET_ACTION_ADD_COMMENT_EDIT_TO_POST,
} from '../constants/socketActionTypes'

// socket call for adding comment to a post...
export const addComment = (commentText, postId, postOwner, userId) => ({
  type: SOCKET_ACTION_ADD_COMMENT_TO_POST,
  meta: { remote: true },
  user: userId,
  post: postId,
  postOwner: postOwner,
  text: commentText,
})

// socket call for loading comment with pagination of a post...
export const loadMoreComment = (postId, skip, limit) => {
  return {
    type: ACTION_LOAD_MORE_COMMENT,
    payload: {
      request: {
        url:
          API_LOAD_MORE_COMMENT +
          `?postId=${postId}&skip=${skip}&limit=${limit}`,
        method: 'GET',
      },
    },
  }
}

// socket call for edit comment to a post...
export const editComment = (
  commentId,
  postId,
  replyId,
  isComment,
  commentText,
) => ({
  type: SOCKET_ACTION_ADD_COMMENT_EDIT_TO_POST,
  meta: { remote: true },
  comment: commentId,
  postId: postId,
  reply: replyId,
  status: isComment,
  text: commentText,
})

// socket call for delete comment to a post...
export const deleteComment = (commentId, postId, replyId, isComment) => ({
  type: SOCKET_ACTION_ADD_COMMENT_REMOVE_TO_POST,
  meta: { remote: true },
  comment: commentId,
  postId: postId,
  reply: replyId,
  status: isComment,
})

// adding a reply to a comment...
export const addCommentReply = (
  commentId,
  mention,
  user,
  userName,
  text,
  postId,
  postOwnerName,
  commentOwnerName,
) => ({
  type: SOCKET_ACTION_ADD_COMMENT_REPLY_TO_POST,
  meta: { remote: true },
  commentId: commentId,
  text: text,
  mention: mention,
  user: user,
  commenterName: userName,
  postId: postId,
  postOwner: postOwnerName,
  commentOwner: commentOwnerName,
})

// socket call for loading reply with pagination of a post...
export const loadMoreReply = (postId, commentId, skip, limit) => {
  return {
    type: ACTION_LOAD_MORE_COMMENT_REPLY,
    payload: {
      request: {
        url:
          API_LOAD_MORE_COMMENT_REPLY +
          `?commentId=${commentId}&skip=${skip}&limit=${limit}`,
        method: 'GET',
      },
    },
  }
}
