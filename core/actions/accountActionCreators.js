/**
 * @flow
 */
import {
  ACTION_SET_ACTIVE_SHOP,
  ACTION_UPDATE_SHOP
} from "../constants/actionTypes";
import { API_UPDATE_SHOP } from "../constants/apiEndpoints";

export const setActiveShop = (activeProfile, activeShop ) => ({
  type: ACTION_SET_ACTIVE_SHOP,
  payload: {
    activeShop,
    activeProfile
  }
});


export const updateShop = (activeShop) => ({
  type: ACTION_UPDATE_SHOP,
  payload: {
    request: {
      url: API_UPDATE_SHOP+activeShop.shopId,
      method: "PATCH",
      data: {
        companyEmail:activeShop.companyEmail,
        companyId:activeShop.companyId,
        companyName:activeShop.companyName,
        companyVat:activeShop.companyVat,
        countryCode:activeShop.countrycode,
        itemForSell:activeShop.itemforsell,
        phone:activeShop.phone,
        registeredCompanyAddress:activeShop.registeredCompanyAddress,
        shopName:activeShop.shopName,
        socialMedia1: activeShop.socialMedia1,
        socialMedia2: activeShop.socialMedia2,
        userId: activeShop.userId,
        whereDidYouHere: activeShop.wheredidyouhere
      }
    }
  }
});
