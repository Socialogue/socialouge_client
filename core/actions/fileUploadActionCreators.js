/**
 * @flow
 */
import config from 'web/src/Config'
import {
  ACTION_COVER_IMAGE_UPLOAD_SUCCESS,
  ACTION_FILE_UPLOAD,
  ACTION_MULTIPLE_FILE_UPLOAD,
  ACTION_PROFILE_IMAGE_UPLOAD_SUCCESS,
  ACTION_PROFILE_IMAGE_URL,
  ACTION_SET_SHOP_TEMP_COVER_IMAGE_SUCCESS,
  ACTION_SET_SHOP_TEMP_PROFILE_IMAGE_SUCCESS,
  ACTION_SET_TEMP_COVER_IMAGE_SUCCESS,
  ACTION_SET_TEMP_PROFILE_IMAGE_SUCCESS,
  ACTION_SHOP_PROFILE_IMAGE_URL,
} from '../constants/actionTypes'
import {
  API_ADD_PROFILE_IMAGE_URLS,
  FILE_SERVER_SINGLE_IMAGE,
} from '../constants/apiEndpoints'

// upload single image..
export const uploadSingleImage = (key, file) => {
  const formData = new FormData()
  formData.append('image', file)
  formData.append('key', config.FILE_SERVER_KEY)
  formData.append('type', config.FILE_SERVER_KEY)

  return {
    type: ACTION_FILE_UPLOAD,
    fileUploadKey: key,
    payload: {
      options: {
        returnRejectedPromiseOnError: true,
      },
      request: {
        baseURL: config.FILE_SERVER_URL,
        headers: {
          // "Content-Type": "multipart/form-data",
          Accept: 'Application/json',
          allowed_headers: '*',
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
        },
        url: FILE_SERVER_SINGLE_IMAGE,
        method: 'POST',
        data: formData,
        timeout: 0,
      },
    },
  }
}

// upload multiple images..
export const uploadMultipleImages = (key, files) => {
  const formData = new FormData()
  formData.append('image', JSON.stringify(files))
  formData.append('key', config.FILE_SERVER_KEY)
  formData.append('type', config.FILE_SERVER_KEY)

  return {
    type: ACTION_MULTIPLE_FILE_UPLOAD,
    fileUploadKey: key,
    payload: {
      options: {
        returnRejectedPromiseOnError: true,
      },
      request: {
        baseURL: config.FILE_SERVER_URL,
        headers: {
          // "Content-Type": "multipart/form-data",
          Accept: 'Application/json',
          allowed_headers: '*',
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
        },
        url: FILE_SERVER_SINGLE_IMAGE,
        method: 'POST',
        data: formData,
        timeout: 0,
      },
    },
  }
}

// api call for profile/cover image url to mongodb...
export const storeImageUrl = (userId, images, type, status) => {
  return {
    type: ACTION_PROFILE_IMAGE_URL,
    payload: {
      request: {
        data: {
          userId: userId,
          image: images,
          type: type,
          status: status,
        },
        url: API_ADD_PROFILE_IMAGE_URLS,
        method: 'POST',
      },
    },
  }
}

// api call for shop profile/cover image url to mongodb...
export const storeShopImageUrl = (shopId, images, type, status) => {
  return {
    type: ACTION_SHOP_PROFILE_IMAGE_URL,
    payload: {
      request: {
        data: {
          shopId: shopId,
          image: images,
          type: type,
          status: status,
        },
        url: API_ADD_PROFILE_IMAGE_URLS,
        method: 'POST',
      },
    },
  }
}

// call after profile photo uploaded successfully... for change profile image url in auth reducer...
// check auth reducer if needed...
export const afterProfileImageUpload = (data) => {
  return {
    type: ACTION_PROFILE_IMAGE_UPLOAD_SUCCESS,
    fileUploadKey: data.key,
    payload: {
      data: data,
    },
  }
}

// call after cover photo uploaded successfully... for change cover image url in auth reducer...
// check auth reducer if needed...
export const afterCoverImageUpload = (data) => {
  return {
    type: ACTION_COVER_IMAGE_UPLOAD_SUCCESS,
    fileUploadKey: data.key,
    payload: {
      data: data,
    },
  }
}

// handler for set temp profile image path ...
export const setTmpProfileImage = (data) => {
  return {
    type: ACTION_SET_TEMP_PROFILE_IMAGE_SUCCESS,
    fileUploadKey: data.key,
    payload: {
      data: data,
    },
  }
}

// handler for set temp cover image path ...
export const setTmpCoverImage = (data) => {
  return {
    type: ACTION_SET_TEMP_COVER_IMAGE_SUCCESS,
    fileUploadKey: data.key,
    payload: {
      data: data,
    },
  }
}

// call after SHOP profile photo uploaded successfully... for change profile image url in account reducer...
// check account reducer if needed...
export const afterShopProfileImageUpload = (data) => {
  return {
    type: ACTION_SET_SHOP_TEMP_PROFILE_IMAGE_SUCCESS,
    fileUploadKey: data.key,
    payload: {
      data: data,
    },
  }
}
export const afterShopCoverImageUpload = (data) => {
  return {
    type: ACTION_SET_SHOP_TEMP_COVER_IMAGE_SUCCESS,
    fileUploadKey: data.key,
    payload: {
      data: data,
    },
  }
}

// handler for set temp shop profile image path ...
export const setTmpShopProfileImage = (data) => {
  return {
    type: ACTION_SET_SHOP_TEMP_PROFILE_IMAGE_SUCCESS,
    fileUploadKey: data.key,
    payload: {
      data: data,
    },
  }
}

// handler for set temp shop cover image path ...
export const setTmpShopCoverImage = (data) => {
  return {
    type: ACTION_SET_SHOP_TEMP_COVER_IMAGE_SUCCESS,
    fileUploadKey: data.key,
    payload: {
      data: data,
    },
  }
}
