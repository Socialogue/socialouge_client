export const selectActiveProfile = state => state.account.activeProfile;
export const selectActiveShop = state => state.account.activeShop;
// export const selectProducts = state => state.account.products;

export const updateShopError = state => state.account.updateShopError;
export const updateShopSuccess = state => state.account.updateShopSuccess;
