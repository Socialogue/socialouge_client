export const selectUploadkey = state => state.file.uploadkey;
export const selectOriginalPath = state => state.file.original_path;
export const selectConvertedPaths = state => state.file.converted_paths;
export const selectFile = state => state.file.file;
