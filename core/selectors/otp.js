export const selectOtpToken = state => state.otp.otpToken;
export const selectPhoneNumber = state => state.otp.phoneNumber;
export const selectOtpFailed = state => state.otp.otpValidationFailed;
