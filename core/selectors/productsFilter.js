export const selectSearchProducts = (state) =>
  state.productsFilter.searchProducts
export const selectFilteredProducts = (state) => state.productsFilter.products
export const selectLoadingStatus = (state) => state.productsFilter.loaded
export const selectTotalFilteredProducts = (state) =>
  state.productsFilter.totalProducts
export const selectAllColors = (state) => state.productsFilter.allColors
export const selectAllSizes = (state) => state.productsFilter.allSizes
