export const selectToken = (state) => state.auth.token
export const selectUser = (state) => state.auth.user
export const selectNetworkError = (state) => state.auth.networkError
export const selectUserToVerifyOtp = (state) => state.auth.userToVerify
export const selectForgotPasswordVerify = (state) => state.auth.forgotOtpVerify
export const selectOtpSendMsg = (state) => state.auth.otpSendMsg
export const selectLoginErrorMsg = (state) => state.auth.loginErrorMsg
export const selectOtpVerifyStatus = (state) => state.auth.needToVerify
export const selectOtpErrorMsg = (state) => state.auth.otpErrorMsg
export const selectRegisterProccessing = (state) =>
  state.auth.registrationProcessing

export const selectRegistrationStatus = (state) =>
  state.auth.registrationSuccess
export const selectLoginStatus = (state) => state.auth.loginSuccess
export const selectRegisterErrorMessage = (state) =>
  state.auth.registerErrorMessage

export const selectPicture = (state) => {
  if (state.auth.user.profile_picture) {
    return state.auth.user.profile_picture.thumbnail
  }

  return ''
}
export const selectMyFollowings = (state) => state.auth.myFollowings

export const selectIsLoggedIn = (state) => {
  const token = selectToken(state)

  return !!token
}

// Logged In but profile may or may not be complete.
export const selectIsPartiallyLoggedIn = (state) => !!selectToken(state)

export const isProfileComplete = (user) => !!user.name

// home reducer...
export const selectViewType = (state) => state.home.viewType
export const selectViewTypeFeed = (state) => state.home.viewTypeFeed
export const selectFeed = (state) => state.home.feed
export const selectReplyStatus = (state) => state.home.replyAdded
export const selectCmtDelStatus = (state) => state.home.commentDeleted
export const selectCmtEditStatus = (state) => state.home.commentEdited
export const selectDeliveryAddressMsg = (state) => state.home.deliveryAddressMsg
export const selectPost = (state) => state.home.post
export const selectPostChange = (state) => state.home.postChangeDetect
