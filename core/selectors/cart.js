export const selectCartData = state => state.cart.cart;
export const selectCartPrice = state => state.cart.finalPrice;
export const selectCartVat = state => {
  let vat = 0;
  state.cart.cart.map(item => {
    vat += item.vatAmmount;
    // console.log("vat: ", vat, item.vatAmmount);
  });
  return vat;
};
