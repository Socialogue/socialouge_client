export const selectPosts = (state) => state.posts.posts
export const selectLikedPosts = (state) => state.posts.likedPosts
export const selectCount = (state) => state.posts.total
export const selectFavoritePosts = (state) => state.posts.favoriteProducts
export const selectPostsLoading = (state) => state.posts.postLoading
export const selectLoadedStatus = (state) => state.posts.loaded
export const selectPostAddStatus = (state) => state.posts.postAddStatus
