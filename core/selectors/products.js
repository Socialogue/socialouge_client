export const selectProducts = (state) => state.products.products
export const selectProduct = (state) => state.products.productDetails
export const selectFavoriteProducts = (state) => state.products.favoriteProducts
export const selectTotalFavorites = (state) => state.products.total
export const selectPublicFavoriteProducts = (state) =>
  state.products.favoritePublicProducts
export const selectCategories = (state) => state.products.categories
export const selectErrorMsg = (state) => state.products.errorMsg
export const selectCatData = (state) => state.products.categoryData
export const selectSubCatData = (state) => state.products.subCategoryData
export const selectInnerCatData = (state) => state.products.innerCategoryData
export const selectAdditionalData = (state) => state.products.additionalData
