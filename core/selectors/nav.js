import {selectToken } from './user';
import {selectIsSwiperShown} from "app/src/redux/selectors";

// eslint-disable-next-line import/prefer-default-export
export const selectInitialRoute = (state) => {
  const isSwiperShown = selectIsSwiperShown(state);
  if (!isSwiperShown) {
    return 'Swiper';
  }
  const token = selectToken(state);
  if (!token) {
    return 'Login';
  }
  return 'App';
};
