export const selectProductDeleteStatus = state => state.dashboard.productDelete;
export const selectProducts = state => state.dashboard.products;
export const selectPage = state => state.dashboard.page;
export const selectHasNextPage = state => state.dashboard.hasNextPage;
export const selecthasPrevPage = state => state.dashboard.hasPrevPage;
export const selectLimit = state => state.dashboard.limit;
export const selectNextPage = state => state.dashboard.nextPage;
export const selectPagingCounter = state => state.dashboard.pagingCounter;
export const selectPrevPage = state => state.dashboard.prevPage;
export const selectTotalDocs = state => state.dashboard.totalDocs;
export const selectTotalPages = state => {
  // let totalPages = [];
  // for(let i = 1; i <= state.dashboard.totalPages; i++){
  //   totalPages.push(i);
  // }
  return state.dashboard.totalPages;
};

//orders...
export const selectOrders = state => state.dashboard.orders;
export const selectOrderStatusChange = state =>
  state.dashboard.orderStatusChange;
