export const getProfileOwnerData = (state) => state.publicProfile.profileOwner
export const getProfileOwnerPosts = (state) => state.publicProfile.profilePosts
export const selectTotalCount = (state) => state.publicProfile.total
export const getFollowedStatus = (state) => state.auth.followingStatus
export const getTmpProfileImage = (state) => state.publicProfile.tmpProfileImage
export const getTmpCoverImage = (state) => state.publicProfile.tmpCoverImage
export const getTmpShopProfileImage = (state) =>
  state.publicProfile.tmpShopProfileImage
export const getTmpShopCoverImage = (state) =>
  state.publicProfile.tmpShopCoverImage
export const selectFollowStatus = (state) => state.auth.followingStatus
