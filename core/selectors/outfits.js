export const selectOutfitsData = state => state.outfits.outfits;
export const selectOutfitsMsg = state => state.outfits.errorMsg;
export const selectOutfitsCollection = state => state.outfits.outfitsCollection;
export const selectAddOutfitsSuccessMsg = state => state.outfits.successMsg;
export const selectCreateCollectionMsg = state => state.outfits.createCollectionMsg;
