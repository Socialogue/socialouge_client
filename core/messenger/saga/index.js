import { FETCH_ALL_CONVERSATION, FETCH_ALL_MESSAGES, MSG_PROMPT_USER_ID } from "../actionTypes";
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { fetchConversations, fetchMessages } from '../api/MessengerApiClient';
import {
  fetchConversationsError,
  fetchConversationsSuccess,
  fetchMessagesError,
  fetchMessagesSuccess
} from "../actionCreators/conversationsActionCreators";

import { ACTION_LOGIN_SUCCESS } from '../../constants/actionTypes';
import { delay } from 'redux-saga';
import { msgAddUserIDToSocket } from "../socketActionCreators";
import { selectUserID } from "../selectors";

function* fetchConversationsAsync(action) {
  try {
    const data = yield call(fetchConversations, { authUserId: action.authUserId || action.payload.data.data._id });
    yield put(fetchConversationsSuccess(data));
  } catch (e) {
    yield put(fetchConversationsError(e));
  }
}

function* fetchMessagesAsync(action) {
  try {
    const data = yield call(fetchMessages, {conversationId: action.conversationId})
    yield put(fetchMessagesSuccess(data));
  } catch (e) {
    yield put(fetchMessagesError(e));
  }
}

function* msgPromptUserID(action) {
  for (let i = 0; i < 5; i++) {
    const userID = yield select(selectUserID);
    if (userID) {
      yield put(msgAddUserIDToSocket(userID));
      return;
    }
    yield delay(2000);
  }
}

export default function* messengerSagas() {
  yield takeLatest(ACTION_LOGIN_SUCCESS, fetchConversationsAsync);

  yield takeLatest(FETCH_ALL_MESSAGES, fetchMessagesAsync);
  yield takeLatest(FETCH_ALL_CONVERSATION, fetchConversationsAsync);

  yield takeLatest(ACTION_LOGIN_SUCCESS, msgPromptUserID);
  yield takeLatest(MSG_PROMPT_USER_ID, msgPromptUserID);
}
