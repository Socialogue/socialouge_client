import {
  FETCH_ALL_CONVERSATION,
  FETCH_ALL_CONVERSATION_ERROR,
  FETCH_ALL_CONVERSATION_SUCCESS,
  FETCH_ALL_MESSAGES, FETCH_ALL_MESSAGES_ERROR, FETCH_ALL_MESSAGES_SUCCESS
} from "../actionTypes";

export const fetchConversationsSuccess = (response) => {
  return {
    type: FETCH_ALL_CONVERSATION_SUCCESS,
    response
  };
}

export const fetchConversations = (authUserId) => {
  return {
    type: FETCH_ALL_CONVERSATION,
    authUserId
  }
}

export const fetchConversationsError = (data) => {
  return {
    type: FETCH_ALL_CONVERSATION_ERROR,
    payload: data
  }
}

export const fetchMessagesSuccess = (response) => {
  return {
    type: FETCH_ALL_MESSAGES_SUCCESS,
    response
  };
}

export const fetchMessages = (conversationId) => {
  return {
    type: FETCH_ALL_MESSAGES,
    conversationId
  }
}

export const fetchMessagesError = (data) => {
  return {
    type: FETCH_ALL_MESSAGES_ERROR,
    payload: data
  }
}
