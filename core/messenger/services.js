// @flow
import { v4 as uuidv4 } from 'uuid';

export const openConversationWindow = (action, oldConversations, loggedInUser) => {
  // if convo exists for the userId, just change winodw open flag
  const find = oldConversations.find(i => i.members.includes(action.userId));
  if (find) {
    const newConversations = oldConversations.map((i) => {
      if (i.members.includes(action.userId)) {
        i.isWindowOpen = true;
      }
      return i;
    });

    return newConversations;
  }

  // other create new convo, give  temporary uuid and add to oldCon
  return [
    ...oldConversations,
    {
      tempConversationId: uuidv4(),
      members: [action.userId, loggedInUser._id],
      isWindowOpen: true,
      messages: [],
      memberUsers: [{
        userId: action.userId,
        userName: action.userName,
        userAvatar: action.userAvatar
      },
      {
        userId: loggedInUser._id,
        userName: `${loggedInUser.firstName} ${loggedInUser.lastName}`,
        userAvatar: loggedInUser.profileImage
      }
      ]
    }
  ];
};

export const closeConversationWindow = (action, oldConversations) => {
  const idKey = action.conversationId ? 'conversationId' : 'tempConversationId';

  return oldConversations.map((i) => {
    if (i[idKey] === action[idKey]) {
      i.isWindowOpen = false;
    }
    return i;
  });
};

export const addNewConversation = (action, oldConversations) => {
  // two situations:

  // 1. I initiated the convo & I got the socket event 'MSG_NEW_CONVERSATION'
  //then we should have a conversation with tempConversationId in our system
  // need to replace the conversation with actual conversationId
  const find = oldConversations.find(i => i.tempConversationId === action.tempConversationId);
  if (find) {
    const newConversations = oldConversations.filter(i => i.tempConversationId !== action.tempConversationId);
    return [
      ...newConversations,
      {
        _id: action.conversationId,
        conversationId: action.conversationId,
        tempConversationId: action.tempConversationId, // we need to keep the tempConversationId in order to properly
        //handle incoming messages for a newly created conversation(when we first click on a user profile's message button)
        members: action.members,
        memberUsers: action.memberUsers,
        lastMessage: action.lastMessage,
        messages: [action.message],
        isWindowOpen: find.isWindowOpen,
        updatedAt: action.updatedAt
      }
    ];
  }

  // 2. someone messaged me for the first time so I got this socket event
  // we can safely add a new conversation, also to be sure make sure we dont already
  // have a conversation by that conversationId in our system
  // if convo exists for the conversationId, just change winodw open flag
  const check = oldConversations.find(i => i.conversationId === action.conversationId);
  if (check) {
    return oldConversations.map((i) => {
      if (i.conversationId === action.conversationId) {
        i.lastMessage = action.lastMessage;
      }
      return i;
    });
  }

  return [
    ...oldConversations,
    {
      _id: action.conversationId,
      members: action.members,
      lastMessage: action.lastMessage,
      messages: [action.message],
      memberUsers: action.memberUsers,
      updatedAt: action.updatedAt
    }
  ];
};

export const addMessageToConversation = (action, oldConversations) => oldConversations.map((convo) => {
  if (convo._id === action.conversationId) {
    convo.messages = convo.messages || [];
    convo.messages.push({
      text: action.text,
      userId: action.userId
    });
  }
  return convo;

});

export const addBulkMessagesToConversation = (conversationId, oldConversations, messages) => oldConversations.map((convo) => {
  if (convo._id === conversationId) {
    console.log('matched', messages);
    convo.messages = convo.messages || [];
    convo.messages = messages;
  }
  return convo;
});


/*
* get the other user's object, means the user who is not the logged in user
* */
export const getOpponentUser = (conversation, authUserId) => {
  let user = {};
  if (conversation.memberUsers) {
    user = conversation.memberUsers.find( i => i.userId !== authUserId);
  }
  return user;
}
