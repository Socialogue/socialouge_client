import Config from 'web/src/Config';
import {API_ALL_CONVERSATIONS, API_ALL_MESSAGES} from "../apiEndpoints";

export const fetchConversations = (args) => {
  return new Promise(function(resolve,reject) {
    const url = Config.MESSAGING_API + API_ALL_CONVERSATIONS.replace(':userId', args.authUserId);
    const options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    }
    fetch(url, options)
      .then((res) => {return res.json()})
      .then((response) => {
        resolve(response)
      })
      .catch(error => console.log(error))
  })
}

export const fetchConversationsfromDataBase=(args)=>{
  return new Promise(function(resolve,reject) {
    const url = Config.MESSAGING_API + API_ALL_CONVERSATIONS.replace(':userId', args.authUserId)
    const options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    }
    fetch(url, options)
      .then((res) => {return res.json()})
      .then((response) => {
        resolve(response)
      })
      .catch(error => console.log(error))
  })

}

export const fetchMessages = (args) => {
  return new Promise(function(resolve,reject) {
    const url = Config.MESSAGING_API + API_ALL_MESSAGES.replace(':conversationId', args.conversationId);
    const options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    }
    fetch(url, options)
      .then((res) => {return res.json()})
      .then((response) => {
        resolve(response)
      })
      .catch(error => console.log(error))
  })
}

export const fetchMessagesFromDatabase=(args)=>{

  return new Promise(function(resolve,reject) {
    const url = Config.MESSAGING_API + API_ALL_MESSAGES.replace(':conversationId', args.conversationId)
    const options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    }
    fetch(url, options)
      .then((res) => {return res.json()})
      .then((response) => {
        resolve(response)
      })
      .catch(error => console.log(error))
  })

}
