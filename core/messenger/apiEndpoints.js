export const API_ALL_CONVERSATIONS = 'conversations/:userId';
export const API_ALL_MESSAGES = 'messages/:conversationId';
