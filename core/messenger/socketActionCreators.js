/**
 * @flow
 */

import {
  ACTIVATE_CONVERSATION,
  FETCH_ALL_CONVERSATION,
  MSG_ADD_USER_ID_TO_SOCKET,
  MSG_SEND_CHAT_MESSAGE,
  MSG_SEND_INITIAL_CHAT_MESSAGE,
} from "./actionTypes";

export const sendChatMessage = (text, userId, conversationId) =>{
  return {
    type: MSG_SEND_CHAT_MESSAGE,
    meta: {remote: true},
    text,
    userId,
    conversationId,
  }
}

// this also sends a chat message, but also starts a conversation, when
//someone sends message to someone for the first time
export const sendInitialChatMessage = (text, members, memberUsers, loggedInUser, tempConversationId) =>{
  return {
    type: MSG_SEND_INITIAL_CHAT_MESSAGE,
    meta: {remote: true},
    text,
    fromUserId: loggedInUser._id,
    tempConversationId,
    memberUsers,
    members
  }
}

//get all of user's conversations from server. TODO move to api call
export const fetchAllConversations = (userId) => {
  return {
    type: FETCH_ALL_CONVERSATION,
    meta: {remote: true},
    userId,
  }
};

export const msgAddUserIDToSocket = (userId) => {
  return {
    type: MSG_ADD_USER_ID_TO_SOCKET,
    meta: { remote: true },
    userId,
  }
}
