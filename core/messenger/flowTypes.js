/**
 * @flow
 */

export type Conversation = {
  conversationId: string,
};

