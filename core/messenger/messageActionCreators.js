import {
  ACTIVATE_CONVERSATION,
  CLOSE_CHAT_WINDOW,
  OPEN_CHAT_WINDOW
} from './actionTypes';

export const openChatWindow = (
  userId,
  userName,
  userAvatar
) => ({
  type: OPEN_CHAT_WINDOW,
  userId,
  userName,
  userAvatar
});

export const closeChatWindow = (conversationId = null, tempConversationId) => ({
  type: CLOSE_CHAT_WINDOW,
  conversationId,
  tempConversationId
});


//on messenger page, clicking on left sidebar opens the convo on right
export const activateConversation = (
  conversationId = null
) => ({
  type: ACTIVATE_CONVERSATION,
  conversationId
});
