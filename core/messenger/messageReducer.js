import {
  ACTIVATE_CONVERSATION,
  CLOSE_CHAT_WINDOW,
  FETCH_ALL_CONVERSATION_SUCCESS,
  FETCH_ALL_MESSAGES_SUCCESS,
  MSG_NEW_CHAT_MESSAGE,
  MSG_NEW_CONVERSATION,
  OPEN_CHAT_WINDOW
} from "./actionTypes";
import {
  MSG_ACTIVE_USERS_IN_CONVERSATIONS_SCOPE,
  MSG_ADD_ACTIVE_USERS_IN_CONVERSATIONS_SCOPE,
  MSG_INACTIVATE_USERS
} from "../messenger/actionTypes";
import {
  addBulkMessagesToConversation,
  addMessageToConversation,
  addNewConversation,
  closeConversationWindow,
  openConversationWindow
} from "./services";

import { REHYDRATE } from "redux-persist/constants";

export const initialState = {
  conversations: [], //conversationId, members, lastMessage,userId, userName, userAvatar, isWindowOpen, messages, tempConversationId
  windowsMinimized: [],
  windowsOpened: [], //userId, userName, userAvatar, conversationId
  messages: [], //conversationId, romUserId, text
  loggedInUser: {},
  activeConversationId: '',
  activeMembersIds: [],
};

const messageReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case REHYDRATE: {
      const { auth } = payload;
      if (auth) {
        return {
          ...state,
          loggedInUser: auth.user
        };
      }
      return {
        ...state
      }
    }

    case OPEN_CHAT_WINDOW: {
      const conversations = openConversationWindow(action, state.conversations, state.loggedInUser);
      return {
        ...state,
        conversations
      }
    }

    case ACTIVATE_CONVERSATION: {
      return {
        ...state,
        activeConversationId: action.conversationId
      }
    }

    case FETCH_ALL_CONVERSATION_SUCCESS: {
      return {
        ...state,
        conversations: action.response.data || []
      }
    }

    case FETCH_ALL_MESSAGES_SUCCESS: {
      const conversations = addBulkMessagesToConversation(action.response.conversationId, state.conversations, action.response.data);
      return {
        ...state,
        conversations
      }
    }

    case CLOSE_CHAT_WINDOW: {
      const conversations = closeConversationWindow(action, state.conversations);
      return {
        ...state,
        conversations
      }
    }

    case MSG_NEW_CHAT_MESSAGE: {
      const conversations = addMessageToConversation(action, state.conversations);
      return {
        ...state,
        conversations
      }
    }

    case MSG_NEW_CONVERSATION: {
      // if initiatedBy or initiatedFor is this userId, add it to list
      if (state.loggedInUser) {
        if (action.members.includes(state.loggedInUser._id)) {
          //TODO check if already exists in store
          const conversations = addNewConversation(action, state.conversations);
          return {
            ...state,
            conversations
          }
        }
      }
      return {
        ...state
      };
    }

    case MSG_ACTIVE_USERS_IN_CONVERSATIONS_SCOPE: {
      return { ...state, activeMembersIds: action.activeMembersIds };
    }
    case MSG_ADD_ACTIVE_USERS_IN_CONVERSATIONS_SCOPE: {
      return { ...state, activeMembersIds: Array.from(new Set([...state.activeMembersIds, ...action.activeMembersIds])) };
    }

    case MSG_INACTIVATE_USERS: {
      const activeMembersIdFiltered = state.activeMembersIds.filter((id) => !action.ids.includes(id));
      return { ...state, activeMembersIds: activeMembersIdFiltered || [] };
    }

    default:
      return state;
  }
};

export default messageReducer;
