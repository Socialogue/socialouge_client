export const selectOpenChatWindows = state => state.messenger.conversations.filter( i => i.isWindowOpen );
export const selectAllConversations = state => state.messenger.conversations;
export const selectActiveConversation = state => state.messenger.conversations.find(
    i => i._id === state.messenger.activeConversationId);
export const selectUsersLoggedIn = state => state.messenger.loggedInUser;
export const selectUserID = state => state.auth.user._id;
export const selectActiveMembers = state => state.messenger.activeMembersIds;
