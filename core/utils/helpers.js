import qs from 'qs'

export const cropString = (mainString, cropLength) => {
  let data = mainString
  if (data && data.length > cropLength) {
    data = data.substring(0, cropLength)
    data = data.concat('...')
  }
  return data
}

export const parseQuery = (queryParam) => {
  // made an object based on query stirng, for filtering products...

  const queryObject = qs.parse(queryParam, {
    ignoreQueryPrefix: true,
  })

  // build key and id...
  // const key = this.getKey(queryObject.t);

  return queryObject
}

// helper function for get original image path...
const mainImage = 'http://app.freedomofword.com/storage/images/' //1623131188_TyzfOAGaUsh7hwJHP0it.jpg";
const convertedImage = 'http://app.freedomofword.com/storage/converted/'
//40_40_1623131188_TyzfOAGaUsh7hwJHP0it.jpg";
export const getImageOriginalUrl = (imageName, size = undefined) => {
  // if size not defined return original path...
  if (!size) {
    return mainImage + imageName
  }

  // TODO for nadim: till now have some issue with converted image path
  // that's why now using original path.. need to change in the future
  return mainImage + imageName

  // if size have some value then return converted path based on size...

  switch (size) {
    case '40_40_': {
      return convertedImage + size + imageName
    }

    case '80_80_': {
      return convertedImage + size + imageName
    }

    case '150_150_': {
      return convertedImage + size + imageName
    }
    case '191_136_': {
      return convertedImage + size + imageName
    }
    case '270_224_': {
      return convertedImage + size + imageName
    }
    case '306_214_': {
      return convertedImage + size + imageName
    }
    case '570_400_': {
      return convertedImage + size + imageName
    }

    default: {
      return convertedImage + '150_150_' + imageName
    }
  }
}
