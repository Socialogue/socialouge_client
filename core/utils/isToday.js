/**
 * @flow
 */
import moment from 'moment';

const isToday = (date: string) => {
  const today = moment();
  return moment(date).isSame(today, 'day');
};

export default isToday;
