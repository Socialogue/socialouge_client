export const getProducts = (arrays) => {
  if (arrays.length === 0) {
    return [[]];
  }

  let results = [];

  getProducts(arrays.slice(1)).forEach((product) => {
    arrays[0].forEach((value) => {
      results.push([value].concat(product));
    });
  });

  return results;
};

export const getAllVariations = (attributes) => {
  let attributeNames = Object.keys(attributes);

  let attributeValues = attributeNames.map((name) => attributes[name]);

  return getProducts(attributeValues).map((product) => {
    const obj = {};
    attributeNames.forEach((name, i) => {
      obj[name] = product[i];
    });
    return obj;
  });
};

