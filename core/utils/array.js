/*
* If an object exists in arr, remove it first, then add
* the updated item
* */
export const addOrUpdate = (arr, item, key) => {
  const index = arr.findIndex( i => i[key] === item[key]);
  if (index > -1) {
    arr.splice(index, 1);
  }
  arr.push(item);
  return arr;
}

/*
* If item exists in array then remove it
* otherwise add it (item is simple string)
* TODO add support for object type item
* */
export const addOrRemove = (arr, item) => {
  const index = arr.findIndex( i => i === item);
  if (index > -1) {
    arr.splice(index, 1);
  } else {
    arr.push(item);
  }
  return arr;
}

