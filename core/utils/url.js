export const getFilename = (url) => {
  return url.split('/').pop();
}
