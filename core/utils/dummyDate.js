export const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];
export const getMonthIndex = month => {
  if (month == "January") {
    return 0;
  }
  if (month == "February") {
    return 1;
  }
  if (month == "March") {
    return 2;
  }
  if (month == "April") {
    return 3;
  }
  if (month == "May") {
    return 4;
  }
  if (month == "June") {
    return 5;
  }
  if (month == "July") {
    return 6;
  }
  if (month == "August") {
    return 7;
  }
  if (month == "September") {
    return 8;
  }
  if (month == "October") {
    return 9;
  }
  if (month == "November") {
    return 10;
  }
  if (month == "December") {
    return 11;
  }
};
export const days = function(month, year) {
  if (month && year) {
    return new Date(year, month, 0).getDate();
  }
  let days = [];
  for (let i = 1; i < 30; i++) {
    days.push({ day: i });
  }
  return days;
};

export const generateArrayOfYears = () => {
  var max = new Date().getFullYear();
  var min = max - 60;
  var years = [];

  for (var i = max; i >= min; i--) {
    years.push(i);
  }
  return years;
};
