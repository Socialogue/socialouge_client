import axios from 'axios';

export const axiosClient = ({ baseUrl, apiVersion }) => {
  const client = axios.create({
    baseURL: baseUrl,
    responseType: 'json',
    timeout: 60000,
    headers: {
      Accept: `application/json; version=${apiVersion}`
    }
  });

  return client;
};

// eslint-disable-next-line no-unused-vars
export const axiosClientConfig = ({ guestToken, getTokenFromState }) => ({
  // interceptors: {
  //   request: [
  //     ({ getState, action }, req) => {
  //       const token = getTokenFromState(getState());
  //       const guestAllowed = action.payload.guestAllowed;

  //       if (guestAllowed) {
  //         req.headers.Authorization = `Bearer ${guestToken}`;
  //       }

  //       if (token) {
  //         req.headers.Authorization = `Bearer ${token}`;
  //       }

  //       return req;
  //     }
  //   ],
  //   response: [
  //     (params, response) => Promise.resolve(response),
  //     (params, error) => Promise.reject(error)
  //   ]
  // }
});
