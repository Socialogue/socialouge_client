//file server
export const FILE_SERVER_SINGLE_IMAGE = '/images/upload'
export const FILE_SERVER_SINGLE_VIDEO = '/videos/upload'

// Otp
export const REQUEST_OTP_API = '/auth/otp?phone_number=:phone_number'
export const RESEND_OTP_API = '/auth/otp?phone_number=:phone_number&retry=True'
export const VALIDATE_OTP_API = '/otp/login'
export const FACEBOOK_AUTH_API = '/user/login'
export const USER_RESOURCE_API = '/otp/login'
export const VALIDATE_OTP = '/user/verify'
export const RESEND_OTP = '/user/resend-code'
export const FORGOT_PASSWORD = '/user/forget-password'
export const FORGOT_PASSWORD_OTP_VERIFY_API = '/user/forget-password/otp'
export const RESET_PASSWORD_API = '/user/reset-password'

//auth
export const API_LOGIN = '/user/login'
export const API_REGISTER = '/user/signup'

//POST
export const API_POSTS = '/user/posts'
export const API_MY_POSTS = '/user/posts/user/'
export const API_MY_LIKED_POSTS = '/user/posts/like/get'
export const API_MY_FAVORITE_POSTS = '/favorites/get'
export const API_ADD_POST = '/user/posts/create'
export const API_REACT_TO_POST = '/user/posts/like'

//shop
export const API_GET_SHOP = '/user/shops/user/get-shops'
export const API_GET_SHOPS = '/'
export const API_CREATE_SHOP = '/user/shops/create'
export const API_UPDATE_SHOP = '/user/shops/'
export const API_GET_SHOP_DATA = '/user/shops/'

// all shops...
export const API_GET_ALL_SHOPS = '/user/shops/public/shops'

// shop posts...
export const API_GET_SHOP_POSTS = '/user/posts/get/by-shop/'

// shop posts...
export const API_GET_SHOP_PRODUCTS = 'products/product-by-shop/'

//product
export const API_GET_PRODUCT = '/'
export const API_ADD_FAVORITE_PRODUCT = '/favorites/add'
export const API_GET_FAVORITE_PRODUCTS = '/favorites/get'
export const API_GET_PRODUCTS = '/'
export const API_INSERT_PRODUCT = '/products/add'
export const API_INSERT_SHIPPING_TEMPLATE = '/user/shops/add-templete'
export const API_REACT_TO_PRODUCT = '/favorites/add/'

//outfits
export const API_GET_OUTFIT = '/collections/get-collections'
export const API_GET_OUTFIT_COLLECTION = '/collections/all-collections'
export const API_ADD_OUTFIT_COLLECTION = '/collections/add-product'
export const API_CREATE_OUTFIT_COLLECTION = '/collections/add'

// home
export const FEED = '/feed'

//followings
export const API_GET_FOLLOWINGS = '/followers/following-list/'

// public profile
export const API_GET_PROFILE_OWNER = '/public-profile/'
export const API_GET_PROFILE_POSTS = '/public-profile/post/'
export const API_GET_PUBLIC_FAVORITE_PRODUCTS =
  '/public-profile/favorite-product/'

export const API_ADD_TO_FOLLOW = '/followers/add/'
export const API_UN_FOLLOW = '/followers/remove/'

// user pesonal data
export const API_FETCH_USER_PERSONAL_DATA = '/user/profile/'
export const API_UPDATE_USER_PERSONAL_DATA = '/user/profile/update/'
export const API_UPDATE_USER_EMAIL_DATA = '/user/profile/change-email/otp/'
export const API_UPDATE_USER_PASSWORD_DATA = '/user/profile/change-password/'
export const API_DELETE_USER_ACCOUNT = '/user/profile/delete-account/'
export const API_GET_DELIVERY_INFO = '/user/profile/get-delivery-info/'
export const API_GET_DELIVERY_TYPES = '/delivery-method/get/'
export const API_ADD_DELIVERY_INFO = '/user/profile/add-delivery-info/'
export const API_UPDATE_DELIVERY_INFO = '/user/profile/update-delivery-info/'
export const API_GET_USER_INFO = '/user/profile/get/'
export const API_VERIFY_OTP_EMAIL_CHANGE = '/user/profile/change-email/'

// create product- category
export const API_GET_CATEGORIES = '/admin/categories/'

// create product- get additional data for product...
export const API_GET_ADDITIONAL_DATA = '/products/get/all-items/'

// get product details...
export const API_GET_PRODUCT_DETAILS = '/products/details/'

// get delivery address...
export const API_GET_DELIVERY_ADDRESS = '/user/profile/delivery-address'

// add product to cart db...
export const API_GET_PRODUCT_FROM_CART_DB = '/cart/get/'
export const API_ADD_PRODUCT_TO_CART_DB = '/cart/add/'
export const API_UPDATE_PRODUCT_FROM_CART_DB = '/cart/update/'
export const API_DELETE_PRODUCT_FROM_CART_DB = '/cart/delete/'

// dashboard api endpoints...
export const API_GET_ALL_PRODUCTS = '/seller/listing/get/'
export const API_DELETE_PRODUCT = '/seller/listing/delete/'
export const API_GET_ALL_ORDERS = '/seller/orders/get/'

// order status change...
export const API_ORDER_STATUS_CHANGE = '/seller/orders/change-order-status/'

// products filtering api endpoints...
export const API_GET_FILTERED_PRODUCTS = '/product/filter/'

// products searching api endpoints...
export const API_SEARCH_PRODUCTS = '/products/search/'

//all products tag to post api endpoints...
export const API_ALL_PRODUCTS_FOR_TAG = '/products/search/'

// comments for post api endpoints...
export const API_LOAD_MORE_COMMENT = '/comments/get/'
export const API_ADD_REPLY_TO_COMMENT_TO_POST = '/comments/replay/'
export const API_LOAD_MORE_COMMENT_REPLY = '/comments/reply/'

//store profile image url api endpoint...
export const API_ADD_PROFILE_IMAGE_URLS = '/user/profile/image-upload/'
