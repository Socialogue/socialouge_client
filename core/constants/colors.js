// Global colors
export const COLOR_WHITE = '#FFFFFF';
export const COLOR_BLACK = '#030303';
export const COLOR_BRAND = '#df350d';
export const COLOR_GREEN = '#df350d';
export const TEXT_GREEN = '#df350d';
export const COLOR_GREEN_DISABLED = 'rgba(236, 100, 75, 0.5)';
export const COLOR_RED = '#FE3824';
export const COLOR_GREY = '#707070';
export const COLOR_RED_LIGHT = '#EC644B';
export const COLOR_RED_LIGHT_DISABLED = 'rgba(236, 100, 75, 0.5)';
