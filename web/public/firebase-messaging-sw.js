/* eslint-disable */

// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
  messagingSenderId: '112233'
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
var messaging = firebase.messaging();

self.addEventListener('push', function (event) {
  event.waitUntil(async function () {
    clients
      .matchAll({ type: 'window' })
      .then(function(clientList) {
        return clientList.filter(function(client) {
          return client.visibilityState === 'hidden';
        });
      })
      .then(function(invisibleClients) {
        if (!invisibleClients.length) return;

        // create a firebase push object
        var push = {
          'firebase-messaging-msg-data': event.data.json(),
          'firebase-messaging-msg-type': 'push-msg-received'
        };

        //invisibleClients.forEach(client => console.log(client));
        invisibleClients.forEach(function(client) {
          client.postMessage(push)
        });
      });
  }());
});

