const token = 'simpleguesttoken';
const name = 'John Doe';


/**
 * Do all sorts of cool stuff with store here for easier development
 * @param store
 */
const bootstrapDevelopment = (store) => {
  // store.dispatch(loginAction);
  // store.dispatch(getUserInfo());
};

export default {
  REDUX_LOGGER: true,
  CONFIG_LOGGER: true,
  PERSIST_STORE_KEYS: ['auth', 'cart'],
  API_URL: 'http://localhost:5000/',
  MESSAGING_API: 'http://127.0.0.1:3001/',
  FILE_SERVER_URL: '',
  FILE_SERVER_KEY: '',
  bootstrapDevelopment
};
