export const getThirtyBusinessDays = () => {
  const days = [];
  for (let i =1; i<= 30; i++) {
    days.push({
      _id: i,
      name: `${i} business days`
    });
  }
  return days;
}

export const getMessengerActiveClassOrEmpty = (activeMembers=[], userId) => {
  const activeClass = 'connection-active-bubble-color';

  if (activeMembers.includes(userId)) {
    return activeClass;
  }
  return '';
}
