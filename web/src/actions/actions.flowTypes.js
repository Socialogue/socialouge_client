/** @flow **/

export type ApiActionType = {
  type: string,
  payload: {
    request: {
      url: string,
      data?: Object,
      method?: string
    }
  }
};

export type ActionType = {
  type: string,
  payload?: Object
};
