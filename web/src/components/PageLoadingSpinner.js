/**
 * @flow
 */
import React from 'react';

import PageErrorView from './ListErrorView';
import LoadingIndicator from './LoadingIndicator';

type Props = {
  error: ?Object,
  pastDelay: boolean,
  retry: Function
};

const PageLoadingSpinner = (props: Props) => {
  if (props.error) {
    return (
      <PageErrorView
        containerClass='cu-absolute-center-vertical container px-3'
        message='An error occurred while loading the page'
        onRetry={props.retry}
        error={props.error}
      />
    );
  } else if (props.pastDelay) {
    return (
      <div className='cu-absolute-center'>
        <LoadingIndicator />
      </div>
    );
  }

  return null;
};

export default PageLoadingSpinner;
