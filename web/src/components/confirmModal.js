import React, { Component } from "react";

class ConfirmModal extends Component {
  render() {
    const { actionType, actionMsg, additionalMsg } = this.props;
    if (!actionMsg || !actionType || !additionalMsg) {
      return null;
    }
    return (
      <div
        className="modal modal-1 d-block show"
        id="deleteAcModal"
        tabindex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-body">
              <div className="sm-text-fw6 text-uppercase mb-3">
                Are you sure you want to{" "}
                <span className="color-orange"> {actionType}</span> {actionMsg}?
              </div>
              <div className="sm-text-fw5-g mb-5">{additionalMsg}</div>
              <div className="d-flex align-items-center">
                <button
                  className="btn border-btn btn-w-115"
                  onClick={() => this.props.sure(false)}
                >
                  No, I am not
                </button>

                <div className="w-100 text-right x-sm-text-fw6">
                  <button
                    className="btn border-btn btn-w-115 border-0"
                    onClick={() => {
                      this.props.sure(true);
                    }}
                  >
                    Yes, I am Sure
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ConfirmModal;
