import React, { Component } from 'react'
import Star from '../images/icons/star'

export default class Favorite extends Component {
  state = {
    favorite: false,
  }

  onChange = () => {
    this.setState({
      favorite: !this.state.favorite,
    })
    this.props.favorite(this.props.productId)
  }

  render() {
    const { fill } = this.props
    if (!fill) {
      return (
        <span className="com-icon" onClick={() => this.onChange()}>
          <Star fill={this.state.favorite ? 'black' : 'none'} />
        </span>
      )
    }

    return (
      <span className="com-icon" onClick={() => this.onChange()}>
        <Star fill={fill ? 'black' : 'none'} />
      </span>
    )
  }
}
