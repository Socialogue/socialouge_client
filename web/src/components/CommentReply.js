import React, { Component } from 'react'
import Img from '../images/register-img.webp'
import Dot from '../images/icons/dot'
import ArrowRight from '../images/icons/arrowRight'
import { getImageOriginalUrl } from 'core/utils/helpers'
import { connect } from 'react-redux'
import {
  selectCmtDelStatus,
  selectCmtEditStatus,
  selectReplyStatus,
  selectUser,
} from 'core/selectors/user'
import {
  addCommentReply,
  deleteComment,
  editComment,
} from 'core/actions/commentActionCreators'
import moment from 'moment'
import ReadMore from './readMore/readMore'

class CommentReply extends Component {
  constructor(props) {
    super(props)
    this.state = {
      editBtn: false,
      addReply: false,
      replyId: '',
      replyUserName: '',
      replyValue: '',
      replyShow: '',
      mention: false,
      setStatus: false,
      replyEdit: false,
      replyToEdit: '',
    }
    this.replyText = React.createRef()
    this.editReply = React.createRef()
  }

  componentDidUpdate = (prevProps) => {
    if (prevProps.cmtEditStatus !== this.props.cmtEditStatus) {
      this.setState({
        editBtn: false,
      })
    }
  }

  // handler for on-click reply...
  // when anyone click on reply of a comment-reply then automatically mention
  /// comment-reply owner name in the reply input box...
  onReplyClick = (reply) => {
    const mention = `${reply.user.fullName.toUpperCase()} `
    this.setState(
      {
        addReply: !this.state.addReply,
        replyId: reply._id,
        replyUserName: reply.user.fullName,
        replyShow: mention,
      },
      () => {
        this.replyText.current.focus()
      },
    )
  }

  // handler for on-change reply text...
  onChangeReply = (e) => {
    const value = e.target.value
    const name = this.state.replyUserName + ' '
    let text = value
    // if mention have name, filter out the mention name from reply text...
    if (value.includes(name.toUpperCase(), 0)) {
      text = value.slice(name.length)
    }
    this.setState({ replyShow: value, replyValue: text })
    // if try to remove mention name, it will be done on one go...
    if (name !== value && name.length > value.length) {
      this.setState({
        replyUserName: '',
        replyValue: '',
        mention: false,
      })
    }
  }

  // handler for comment-reply submit...
  onSubmitMentionReply = () => {
    const replyId = this.props.comment._id
    const mention = this.props.reply.user._id
    const text = this.state.replyValue
    const commentId = this.props.comment._id
    const user = this.props.user._id
    const userName = this.props.user.fullName
    const postId = this.props.comment.post
    const postOwnerName = ''
    const commentOwnerName = this.props.comment.user.fullName

    // api call for adding reply to a comment...
    // TODO for nadim: need to move this api call to home/index page...
    this.props.addCommentReply(
      commentId,
      mention,
      user,
      userName,
      text,
      postId,
      postOwnerName,
      commentOwnerName,
    )
    this.setState({
      addReply: false,
    })
  }

  deleteCommentReply = () => {
    const comment = this.props.comment
    const commentId = comment._id
    const postId = comment.post
    const replyId = this.props.reply._id
    const isComment = false
    this.props.deleteComment(commentId, postId, replyId, isComment)
    this.setState({ editBtn: false })
  }

  onEditComment = () => {
    this.setState({
      editBtn: false,
      replyEdit: true,
      replyToEdit: this.props.reply.text,
    })
    this.editReply.current.focus()
  }

  onChangeComment = (e) => {
    const text = e.target.value
    this.setState({ replyToEdit: text })
  }

  onUpdateReply = () => {
    const comment = this.props.comment
    const commentId = comment._id
    const postId = comment.post
    const replyId = this.props.reply._id
    const isComment = false
    const text = this.state.replyToEdit
    this.props.editComment(commentId, postId, replyId, isComment, text)
    this.setState({ replyEdit: false })
  }

  render() {
    const { reply, index, comment } = this.props
    const {
      addReply,
      replyId,
      replyValue,
      replyShow,
      editBtn,
      replyEdit,
    } = this.state

    if (!reply) {
      return null
    }

    return (
      <div>
        <div className="reply-box pl-3">
          <div className="reply mb-3">
            <div className="pro-cnt mb-2 align-items-center">
              <div className="pro-img">
                <img
                  className="cover"
                  src={
                    reply.user && reply.user.profileImage
                      ? getImageOriginalUrl(reply.user.profileImage, '40_40_')
                      : Img
                  }
                  alt="img"
                />
              </div>
              <div className="pro-txt-cnt">
                <span className="font-weight-bold text-uppercase">
                  {reply.user && reply.user.fullName}
                </span>
              </div>
            </div>
            <div className="sm-text-fw4-g mb-2">
              <ReadMore
                className=""
                charLimit={100}
                readMoreText=" Read more"
                readLessText="Read less"
              >
                {reply.text}
              </ReadMore>
            </div>
            <div className="">
              <span className="com-time sm-text-fw4-g">
                {' '}
                {moment(reply.createdAt).fromNow()}{' '}
              </span>
              <button
                className="btn border-btn border-0 px-2 py-0 text-uppercase mr-1"
                onClick={() => this.onReplyClick(reply)}
              >
                Reply
              </button>
              <span
                className={
                  this.state.editBtn
                    ? 'd-inline-flex edit-btn active'
                    : 'd-inline-flex edit-btn'
                }
              >
                {this.props.user._id === this.props.reply.user._id && (
                  <span
                    onClick={() => {
                      if (this.props.user._id === this.props.reply.user._id) {
                        this.setState({
                          editBtn: !this.state.editBtn,
                        })
                      }
                    }}
                    className="cursor-pointer d-inline-flex"
                  >
                    <Dot />
                  </span>
                )}
                <span className="tolpit-c">Edit or delete comment</span>

                <span className="edit-box">
                  <span
                    className="sm-text-fw6-g mb-2 cursor-pointer text-decoration-underline d-block text-uppercase"
                    onClick={this.onEditComment}
                  >
                    Edit
                  </span>
                  <span
                    className="sm-text-fw6-g cursor-pointer color-orange text-decoration-underline d-block text-uppercase"
                    onClick={() => this.deleteCommentReply()}
                  >
                    Delete
                  </span>
                </span>
              </span>
            </div>
          </div>
          {/* //////////////////// reply /////////////////*/}
          <div
            className={
              addReply && replyId === reply._id
                ? 'form-group '
                : 'form-group d-none'
            }
          >
            <div className="form-input position-relative form-input-append">
              <input
                type="text"
                className="form-control custom-input"
                placeholder="Add reply"
                name="reply"
                value={replyShow}
                ref={this.replyText}
                onChange={this.onChangeReply}
              />
              <span className="append-input d-block">
                <button
                  className="btn border-btn border-0 p-0"
                  onClick={this.onSubmitMentionReply}
                >
                  Reply{' '}
                  <span className="ml-1">
                    <ArrowRight />
                  </span>
                </button>
              </span>
            </div>
          </div>
          {/* //////////// */}

          {/* //////////////////// edit reply /////////////////*/}
          <div
            className={
              replyEdit && !editBtn ? 'form-group ' : 'form-group d-none'
            }
          >
            <div className="form-input position-relative form-input-append">
              <input
                type="text"
                className="form-control custom-input"
                name="reply"
                value={this.state.replyToEdit}
                ref={this.editReply}
                onChange={this.onChangeComment}
              />
              <span className="append-input d-block">
                <button
                  className="btn border-btn border-0 p-0"
                  onClick={this.onUpdateReply}
                >
                  Update{' '}
                  <span className="ml-1">
                    <ArrowRight />
                  </span>
                </button>
              </span>
            </div>
          </div>
          {/* //////////// */}
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    user: selectUser(state),
    replyStatus: selectReplyStatus(state),
    cmtEditStatus: selectCmtEditStatus(state),
    posts: state.home.feed.posts,
  }),
  { addCommentReply, deleteComment, editComment },
)(CommentReply)
