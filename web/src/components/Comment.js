import React, { Component } from 'react'
import Img from '../images/register-img.webp'
import Dot from '../images/icons/dot'
import ArrowRight from '../images/icons/arrowRight'
import CommentReply from './CommentReply'
import { getImageOriginalUrl } from 'core/utils/helpers'
import { connect } from 'react-redux'
import {
  selectCmtDelStatus,
  selectCmtEditStatus,
  selectPost,
  selectReplyStatus,
  selectUser,
} from 'core/selectors/user'
import {
  addCommentReply,
  deleteComment,
  editComment,
  loadMoreReply,
} from 'core/actions/commentActionCreators'
import moment from 'moment'
import ReadMore from './readMore/readMore'

class Comment extends Component {
  constructor(props) {
    super(props)
    this.state = {
      editBtn: false,
      comment: this.props.comment,
      reply: [],
      replyLength: null,
      replySkip: 0,
      replyLimit: 2,
      mention: false,
      addReply: false,
      commentId: '',
      commentUserName: '',
      commentUserId: '',
      replyShow: '',
      replyValue: '',
      setStatus: false,
      commentEdit: false,
      commentToEdit: '',
    }
    this.replyText = React.createRef()
    this.editBtn = React.createRef()
    this.editCmt = React.createRef()
  }

  componentDidMount = () => {
    document.addEventListener('click', this.handleEditBtn)
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleEditBtn)
  }

  handleEditBtn = (event) => {
    if (
      this.editBtn &&
      !this.editBtn.current.contains(event.target) &&
      this.state.editBtn
    ) {
      this.setState({
        editBtn: false,
      })
    }
  }

  //handler for load more comment reply...
  loadMoreReply = (newStart) => {
    this.setState(
      (prevState) => ({
        replySkip: prevState.replySkip + prevState.replyLimit,
      }),
      () => {
        // call action for load more comment using current limit and offset...
        const comment = this.props.comment
        const commentId = comment._id
        const postId = comment.post
        this.props.loadMoreReply(
          postId,
          commentId,
          this.state.replySkip,
          this.state.replyLimit,
        )
      },
    )
  }

  // handler for on-click reply...
  // when anyone click on reply of a comment then automatically mention
  /// comment owner name in the reply input box...
  onReplyClick = (comment) => {
    const mention = `${comment.user.fullName.toUpperCase()} `
    this.setState(
      {
        addReply: !this.state.addReply,
        commentId: comment._id,
        commentUserName: comment.user.fullName,
        commentUserId: comment.user._id,
        replyShow: mention,
        mention: true,
      },
      () => {
        this.replyText.current.focus()
      },
    )
  }

  // handler for on-change reply text...
  onChangeReply = (e) => {
    const value = e.target.value
    const name = this.state.commentUserName + ' '
    let text = value
    // if mention have name, filter out the mention name from reply text...
    if (value.includes(name.toUpperCase(), 0)) {
      text = value.slice(name.length)
    }
    this.setState({ replyShow: value, replyValue: text })
    // if try to remove mention name, it will be done on one go...
    if (name !== value && name.length > value.length) {
      this.setState({
        commentUserName: '',
        replyShow: '',
        mention: false,
      })
    }
  }

  // handler for comment-reply submit...
  onSubmitCommentReply = () => {
    const commentId = this.props.comment._id
    const mention = this.props.comment.user._id
    const user = this.props.user._id
    const text = this.state.replyValue
    const userName = this.props.user.fullName
    const postId = this.props.comment.post
    const postOwnerName = ''
    const commentOwnerName = this.props.comment.user.fullName

    // api call for adding reply to a comment...
    // TODO for nadim: need to move this api call to home/index page...
    this.props.addCommentReply(
      commentId,
      mention,
      user,
      userName,
      text,
      postId,
      postOwnerName,
      commentOwnerName,
    )
    this.setState({
      replyAdded: this.props.replyStatus,
      setStatus: true,
      addReply: false,
      replyShow: '',
      replyValue: '',
      commentId: '',
    })
  }

  deleteComment = () => {
    const comment = this.props.comment
    const commentId = comment._id
    const postId = comment.post
    const replyId = null
    const isComment = true
    this.props.deleteComment(commentId, postId, replyId, isComment)
  }

  onEditComment = () => {
    this.setState({
      editBtn: false,
      commentEdit: true,
      commentToEdit: this.state.comment.text,
    })
    this.editCmt.current.focus()
  }

  onChangeComment = (e) => {
    const text = e.target.value
    this.setState({ commentToEdit: text })
  }

  onUpdateComment = () => {
    const comment = this.props.comment
    const commentId = comment._id
    const postId = comment.post
    const replyId = null
    const isComment = true
    const text = this.state.commentToEdit
    this.props.editComment(commentId, postId, replyId, isComment, text)
    this.setState({ commentEdit: false })
  }
  render() {
    const { comment } = this.props
    const {
      start,
      end,
      addReply,
      commentEdit,
      editBtn,
      commentId,
      replyValue,
      replyShow,
    } = this.state
    if (!comment) {
      return null
    }
    return (
      <div>
        <div className="comment">
          <div className="comment-cnt mb-3">
            <div className="pro-cnt mb-2 align-items-center">
              <div className="pro-img">
                <img
                  className="cover"
                  src={
                    comment.user && comment.user.profileImage
                      ? getImageOriginalUrl(comment.user.profileImage, '40_40_')
                      : Img
                  }
                  alt="img"
                />
              </div>
              <div className="pro-txt-cnt">
                <span className="font-weight-bold text-uppercase">
                  {comment.user && comment.user.fullName}
                </span>
              </div>
            </div>
            <div className="sm-text-fw4-g mb-2">
              <ReadMore
                className=""
                charLimit={100}
                readMoreText=" Read more"
                readLessText="Read less"
              >
                {comment.text}
              </ReadMore>
            </div>
            <div className="">
              <span className="com-time sm-text-fw4-g">
                {moment(comment.createdAt).fromNow()}{' '}
              </span>
              <button
                className="btn border-btn border-0 px-2 py-0 text-uppercase mr-1"
                onClick={() => this.onReplyClick(comment)}
              >
                Reply
              </button>
              <span
                className={
                  this.state.editBtn
                    ? 'd-inline-flex edit-btn active'
                    : 'd-inline-flex edit-btn'
                }
                ref={this.editBtn}
              >
                {comment.user && this.props.user._id === comment.user._id && (
                  <span
                    onClick={() => {
                      if (
                        comment.user &&
                        this.props.user._id === comment.user._id
                      ) {
                        this.setState({
                          editBtn: !this.state.editBtn,
                        })
                      }
                    }}
                    className="cursor-pointer d-inline-flex"
                  >
                    <Dot />
                  </span>
                )}
                <span className="tolpit-c">Edit or delete comment</span>

                <span className="edit-box">
                  <span
                    className="sm-text-fw6-g mb-2 cursor-pointer text-decoration-underline d-block text-uppercase"
                    onClick={() => this.onEditComment()}
                  >
                    Edit
                  </span>
                  <span
                    className="sm-text-fw6-g cursor-pointer color-orange text-decoration-underline d-block text-uppercase"
                    onClick={() => this.deleteComment()}
                  >
                    Delete
                  </span>
                </span>
              </span>
            </div>
          </div>
          {/* //////////////////// comment reply //////////////////////// */}
          <div
            className={
              addReply && commentId == comment._id
                ? 'form-group '
                : 'form-group d-none'
            }
          >
            <div className="form-input position-relative form-input-append">
              <input
                type="text"
                className="form-control custom-input"
                placeholder="Add reply"
                name="reply"
                value={replyShow}
                ref={this.replyText}
                onChange={this.onChangeReply}
              />
              <span className="append-input d-block">
                <button
                  className="btn border-btn border-0 p-0"
                  onClick={this.onSubmitCommentReply}
                >
                  Reply{' '}
                  <span className="ml-1">
                    <ArrowRight />
                  </span>
                </button>
              </span>
            </div>
          </div>
          {/* //////////// */}

          {/* //////////////////// comment edit //////////////////////// */}
          <div
            className={
              commentEdit && !editBtn ? 'form-group ' : 'form-group d-none'
            }
          >
            <div className="form-input position-relative form-input-append">
              <input
                type="text"
                className="form-control custom-input"
                name="comment"
                value={this.state.commentToEdit}
                ref={this.editCmt}
                onChange={this.onChangeComment}
              />
              <span className="append-input d-block">
                <button
                  className="btn border-btn border-0 p-0"
                  onClick={this.onUpdateComment}
                >
                  Update{' '}
                  <span className="ml-1">
                    <ArrowRight />
                  </span>
                </button>
              </span>
            </div>
          </div>
          {/* //////////// */}
          {this.props.comment.replay.map((reply) => {
            return (
              <CommentReply
                key={reply._id}
                index={end}
                reply={reply}
                comment={this.props.comment}
              />
            )
          })}

          {/* only show load more button when have more reply  */}

          {this.props.comment.totalReply > this.props.comment.replay.length && (
            <div onClick={() => this.loadMoreReply()}>
              <button className="btn border-btn border-0 p-0">
                Load more reply...
              </button>
            </div>
          )}
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    user: selectUser(state),
    replyStatus: selectReplyStatus(state),
    cmtDelStatus: selectCmtDelStatus(state),
    cmtEditStatus: selectCmtEditStatus(state),
    post: selectPost(state),
    posts: state.home.feed.posts,
  }),
  {
    addCommentReply,
    deleteComment,
    editComment,
    loadMoreReply,
  },
)(Comment)
