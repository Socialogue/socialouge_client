/**
 * @flow
 */
import React from "react";
import parsePhoneNumber from "libphonenumber-js";

class SingleLineInput extends React.Component {
  constructor() {
    super();
    this.state = {
      touched: false,
      value: "",
      errorText: ""
    };
  }

  onChange = e => {
    const name = e.target.name;
    const value = e.target.value;

    this.setState({
      value
    });
    // validations
    if (this.isValid(value)) {
      this.props.onChange(name, value);
    }
  };

  isValid = value => {
    const { validations } = this.props;
    if (validations === undefined) return true;
    if (validations && Array.isArray(validations) && validations.length === 0)
      return true;

    // email
    if (validations.includes("email")) {
      if (!this.validateEmail(value)) {
        this.setState({
          errorText: "Please add a valid email address"
        });
        return false;
      }
    }

    // email or phone
    if (validations.includes("email-phone")) {
      if (!this.validateEmail(value) && !parsePhoneNumber(value)) {
        this.setState({
          errorText: "Please enter a valid email or phone"
        });
        return false;
      }
    }

    //password
    if (validations.includes("password")) {
      if (value.length < 6) {
        this.setState({
          errorText: "Password must be at least 6 characters"
        });
        return false;
      }
    }

    // if code reach this point, that means all valdiation check has passed
    this.setState({
      errorText: ""
    });
    return true;
  };

  validateEmail = email => {
    const re = /\S+@\S+\.\S+/;
    return re.test(email);
  };

  render() {
    const { touched, value, errorText } = this.state;
    const { required } = this.props;
    const error =
      touched && value === "" && required
        ? "This field is required"
        : errorText;

    return (
      <div className={error !== "" ? "form-group error-input" : "form-group"}>
        <label className="x-sm-text-fw6 text-uppercase">
          {this.props.label}
        </label>
        <input
          type={this.props.type}
          className="form-control custom-input"
          placeholder={this.props.placeholder ? this.props.placeholder : ""}
          name={this.props.name}
          onChange={this.onChange}
          onBlur={() => this.setState({ touched: true })}
        />
        {error !== "" && <span className="color-orange">{error}</span>}
      </div>
    );
  }
}

export default SingleLineInput;
