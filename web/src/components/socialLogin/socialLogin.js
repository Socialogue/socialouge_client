import React, { Component } from "react";
import { connect } from "react-redux";

import Google from "../../images/icons/google.svg";
import FacebookLogin from "react-facebook-login";
import config from "../../Config";
import { ROUTE_HOME } from "../../routes/constants";
import {
  selectLoginStatus,
  selectRegisterProccessing,
  selectRegistrationStatus,
  selectToken
} from "core/selectors/user";
import {
  register,
  authenticateByFacebook,
  authenticateByGoogle
} from "core/actions/authActionCreators";

class SocialLogin extends Component {
  constructor(props) {
    super();
  }

  componentDidMount() {
    window.gapi.load("auth2", () => {
      window.gapi.auth2
        .init({ client_id: config.GOOGLE_CLIENT_ID })
        .then(() => {
          // DO NOT ATTEMPT TO RENDER BUTTON UNTIL THE 'Init' PROMISE RETURNS
          window.gapi.signin2.render("g-signin22", {
            scope: "https://www.googleapis.com/auth/plus.login",
            width: 200,
            height: 56,
            longtitle: true,
            theme: "light",
            onsuccess: this.responseGoogle,
            client_id: config.GOOGLE_CLIENT_ID,
            cookiepolicy: "single_host_origin"
          });
        });
    });
  }

  responseFacebook = async response => {
    console.log("facebook res: ", this.props);
    const accountType = 3;
    try {
      await this.props.authenticateByFacebook(
        response.name,
        response.email,
        response.picture.data.url,
        response.accessToken,
        accountType
      );
      if (this.props.loginStatus) {
        this.props.history.push(ROUTE_HOME);
      }
    } catch (err) {
      console.log("login with facebook error: ", err);
    }
  };

  componentClicked = e => {
    console.log("clicked", e);
  };

  responseGoogle = async googleUser => {
    var profile = googleUser.getBasicProfile();
    // console.log("ID: " + profile.getId()); // Do not send to your backend! Use an ID token instead.
    // console.log("Name: " + profile.getName());
    // console.log("Image URL: " + profile.getImageUrl());
    // console.log("Email: " + profile.getEmail()); // T
    // // The ID token you need to pass to your backend:
    // var id_token = googleUser.getAuthResponse().id_token;
    // console.log("ID Token: " + id_token);
    const name = profile.getName();
    const email = profile.getEmail();
    const image = profile.getImageUrl();
    const token = googleUser.getAuthResponse().id_token;
    const accountType = 4;

    try {
      await this.props.authenticateByGoogle(
        name,
        email,
        image,
        token,
        accountType
      );
      if (this.props.loginStatus) {
        this.props.history.push(ROUTE_HOME);
      }
    } catch (err) {
      console.log("login with google error: ", err);
    }
  };
  render() {
    return (
      <div className="form-group row mr-0">
        <div className="col-6 pr-0">
          <div id="g-signin22">
            <img className="mr-4" src={Google} alt="img" />Google
          </div>
          {/* <GoogleLogin
                      clientId="1074659719210-1uiu9dbmg2ssuglmrpq6pq13q2260um5.apps.googleusercontent.com"
                      buttonText="Login"
                      onSuccess={this.responseGoogle}
                      onFailure={this.responseGoogle}
                      cookiePolicy={"single_host_origin"}
                      redirectUri="https://localhost:3000"
                    /> */}
        </div>
        <div className="col-6 pr-0">
          <FacebookLogin
            appId={config.FACEBOOK_APP_ID}
            fields="name,email,picture"
            onClick={this.componentClicked}
            callback={this.responseFacebook}
            icon="fa-facebook mr-3"
            cssClass="btn fb-bg-btn w-100 ml-2"
            textButton=" Facebook"
          />
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: !!selectToken(state),
    loading: selectRegisterProccessing(state),
    registerStatus: selectRegistrationStatus(state),
    loginStatus: selectLoginStatus(state)
  }),
  {
    register,
    authenticateByFacebook,
    authenticateByGoogle
  }
)(SocialLogin);
