/**
 * @flow
 */
import React from "react";
import parsePhoneNumber from "libphonenumber-js";

class InputWithoutLabel extends React.Component {
  constructor() {
    super();
    this.state = {
      value: ""
    };
  }

  onChange = e => {
    const name = e.target.name;
    const value = e.target.value;

    this.setState({
      value
    });
    this.props.onChange(value, name);
  };

  render() {
    return (
      <div className="form-group">
        <input
          type={this.props.type}
          className="form-control custom-input"
          placeholder={this.props.placeholder ? this.props.placeholder : ""}
          name={this.props.name}
          onChange={this.onChange}
        />
      </div>
    );
  }
}

export default InputWithoutLabel;
