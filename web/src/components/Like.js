import React, { Component } from 'react'
import Love from '../images/icons/love'
import Arrow from '../images/icons/arrowLeft'

export default class Like extends Component {
  state = {
    liked: false,
  }

  onChange = () => {
    this.setState({
      liked: !this.state.liked,
    })
    this.props.react(this.props.postId)
  }

  render() {
    const { fill } = this.props
    // if (!fill) {
    //   return (
    //     <span className="com-icon" onClick={() => this.onChange()}>
    //       <Love fill={this.state.liked ? 'black' : 'none'} />
    //     </span>
    //   )
    // }

    return (
      <span className="com-icon" onClick={() => this.onChange()}>
        <Love fill={fill ? 'black' : 'none'} />
      </span>
    )
  }
}
