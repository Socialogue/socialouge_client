import React from 'react';

import { storiesOf } from '@storybook/react';
import '@storybook/addon-backgrounds/register';
import { withInfo } from '@storybook/addon-info';
import centered from '@storybook/addon-centered';
import { checkA11y } from '@storybook/addon-a11y';

import LoadingIndicator from '../LoadingIndicator';

const loadingIndicator = storiesOf('Components|LoadingIndicator', module);

loadingIndicator
  .addDecorator(centered)
  .addDecorator(checkA11y);

loadingIndicator.add(
  'Basic',
  withInfo({
    text: 'Component for rendering a spinner'
  })(() => (
    <LoadingIndicator />
  ))
);

loadingIndicator.add(
  'Different sizes',
  withInfo({
    text: 'Component for rendering a spinner'
  })(() => (
    <div>
      <LoadingIndicator />
      <LoadingIndicator size={75} />
      <LoadingIndicator size={100} />
    </div>
  ))
);
