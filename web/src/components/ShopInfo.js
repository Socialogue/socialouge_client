import React from 'react'
import ProImg from './../images/login-img1.webp'
import Comments from './../images/icons/comments.svg'
import { connect } from 'react-redux'
import { selectIsLoggedIn, selectUser } from 'core/selectors/user'
import {
  getFollowedStatus,
  getTmpShopProfileImage,
} from 'core/selectors/profile'
import {
  addToFollow,
  addToFollowShop,
} from 'core/actions/publicProfileActionCreator'
import {
  uploadSingleImage,
  storeShopImageUrl,
  afterShopProfileImageUpload,
  setTmpShopProfileImage,
} from 'core/actions/fileUploadActionCreators'
import { withRouter } from 'react-router'
import { errorAlert, successAlert } from '../utils/alerts'
import { Component } from 'react'
import { openChatWindow } from 'core/messenger/messageActionCreators'
import { fetchConversations } from 'core/messenger/actionCreators/conversationsActionCreators'
import { ROUTE_LOGIN } from '../routes/constants'
import PhotoUpload from '../images/icons/photoUpload'
import { Link } from 'react-router-dom'
import {
  selectConvertedPaths,
  selectFile,
  selectUploadkey,
} from 'core/selectors/fileUpload'
import { getImageOriginalUrl } from 'core/utils/helpers'
import { selectActiveShop } from 'core/selectors/account'
import { isEmpty } from 'lodash'
import { selectShops } from 'core/selectors/shops'

class UserInfo extends Component {
  // constants...
  shopProfile = 'shop_profile'
  shopCover = 'shop_cover'

  componentDidMount = () => {}
  componentDidUpdate = (prevProps) => {
    if (prevProps.isFollowed !== this.props.isFollowed) {
      if (this.props.isFollowed) {
        successAlert('Followed')
      }
    }

    // checking if shop image uploaded or not...
    if (
      JSON.stringify(prevProps.convertedPaths) !==
      JSON.stringify(this.props.convertedPaths)
    ) {
      //check for shopProfile
      if (this.props.uploadKey == this.shopProfile) {
        // api call for cover photo url save to database...

        let image = this.props.file
        console.log('shop profile: ', image)
        const shopId = this.props.shop._id
        const type = 'profileImage'
        const status = 'shop'
        this.props.storeShopImageUrl(shopId, image, type, status)
        this.props.afterShopProfileImageUpload({
          key: type,
          profileImage: image,
        })
        this.props.setTmpShopProfileImage({
          key: type,
          profileImage: image,
        })
      }

      // check for shopCover photo...
      if (this.props.uploadedKey == this.shopCover) {
        // api call for cover photo url save to database...

        let image = this.props.file
        console.log('shop cover: ', image)
        const shopId = this.props.shop._id
        const type = 'coverImage'
        const status = 'shop'
        this.props.storeShopImageUrl(shopId, image, type, status)
        this.props.afterShopProfileImageUpload({
          key: type,
          profileImage: image,
        })
        this.props.setTmpShopProfileImage({
          key: type,
          profileImage: image,
        })
      }
    }
  }

  follow = () => {
    const followingId = this.props.loggedInUser._id
    const followerId = this.props.match.params.id
    this.props.fetchConversations(followingId)
    // return;

    try {
      // set status = 1; if you try to follow a user... if follow shop then set it to 2...
      this.props.addToFollowShop(followingId, followerId, (status = 2))
    } catch (err) {
      console.log(err)
      errorAlert('Something went wrong.')
    }
  }

  // handle shop image uploading...
  // key means a flag to determine what type of image we uploading

  onSelectImage = (e, key) => {
    // const key = this.profileImage;
    e.stopPropagation()
    e.preventDefault()
    const file = e.target.files[0]

    // call action creator that responsible for image upload
    this.props.uploadSingleImage(key, file)
  }

  onClickMessage = (userId) => {
    if (!this.props.isLoggedIn) {
      this.props.history.push(ROUTE_LOGIN)
    } else {
      console.log('error', this.props.shop)
      this.props.openChatWindow(
        this.props.match.params.id,
        this.props.shop.shopName,
        this.props.shop.profileImage,
      )
    }
  }

  render() {
    return (
      <div className="pro-cnt d-block d-md-flex cw-user-info user-info align-items-center mb-0 mb-md-5">
        <div className="pro-img pro-img-xxl position-relative overflow-hidden upload-ov-show mb-1 mb-md-0">
          <img
            className="cover"
            src={
              this.props.tmpShopProfile && this.props.tmpShopProfile
                ? getImageOriginalUrl(this.props.tmpShopProfile)
                : this.props.shop && this.props.shop.profileImage
                ? getImageOriginalUrl(this.props.shop.profileImage, '150_150_')
                : ProImg
            }
            alt="img"
          />
          {this.props.myShops.some(
            (item) => item._id === this.props.match.params.id,
          ) && (
            <div className="upload-overlay cover upload-input">
              {/* <span className="sm-text-fw5">Edit picture</span> */}
              <input
                type="file"
                className="ctm-hidden-upload-input cover"
                onChange={(e) => this.onSelectImage(e, this.shopProfile)}
              />
            </div>
          )}
        </div>
        <div className="pro-txt-cnt pro-txt-cnt-xxl md-w-100 pl-0 pl-md-3 d-flex pt-0 pt-md-2">
          <div className="w-100">
            <div className="md-title md-title-res25 text-uppercase mb-1">
              <span className="pr-3 cursor-pointer">
                {this.props.shop && this.props.shop.shopName}{' '}
              </span>
            </div>
            <div className="sm-text-fw6 text-uppercase mb-1">
              {this.props.user
                ? this.props.user.totalFollowers
                : this.props.shop
                ? this.props.shop.totalFollowers
                : 0}{' '}
              Followers
            </div>
            <div>
              {/* <Link className="color-gray7 text-decoration-underline" to="/">
                Edit Profile
              </Link> */}
            </div>
          </div>
          {this.props.match.params.id && (
            <span className="d-block d-md-flex align-items-start">
              <button
                className="btn border-btn mr-0 mr-md-3 mb-2 mb-md-0"
                onClick={() => this.follow()}
              >
                Follow
              </button>
              <br className="d-block d-md-none" />
              <button className="btn border-btn" onClick={this.onClickMessage}>
                Message
                <img className="ml-2" src={Comments} alt="img" />
              </button>
            </span>
          )}
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    loggedInUser: selectUser(state),
    isLoggedIn: selectIsLoggedIn(state),
    isFollowed: getFollowedStatus(state),
    uploadKey: selectUploadkey(state),
    convertedPaths: selectConvertedPaths(state),
    file: selectFile(state),
    tmpShopProfile: getTmpShopProfileImage(state),
    myShops: selectShops(state),
  }),
  {
    addToFollow,
    addToFollowShop,
    openChatWindow,
    fetchConversations,
    uploadSingleImage,
    storeShopImageUrl,
    afterShopProfileImageUpload,
    setTmpShopProfileImage,
  },
)(withRouter(UserInfo))
