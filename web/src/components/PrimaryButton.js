/**
 * @flow
 */
import React from "react";

type Props = {
  loading: boolean,
  onClick: Function,
  label: string
};

const PrimaryButton = (props: Props) => {
  if (props.loading) {
    return (
      <div
        style={{ cursor: "default" }}
        className={props.className} //"btn black-bg-btn text-uppercase w-100"
      >
        <div className="loader" />
      </div>
    );
  }

  return (
    <button onClick={props.onClick} className={props.className}>
      {props.label}
    </button>
  );
};

export default PrimaryButton;
