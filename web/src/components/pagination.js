import React, { Component } from "react";

class Pagination extends Component {
  render() {
    const {pages, hasPrevPage, hasNextPage} = this.props;
    console.log("pages: ", pages);
    if(!pages){
      return null;
    }
    return (
      <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
          <li class={hasPrevPage ? "page-item" : "page-item disabled"}>
            <a class="page-link" href="#" tabindex="-1">
              Previous
            </a>
          </li>

         { pages.map((page, i) => {
           return (<li class="page-item" key={i}>
            <a class="page-link" href="#">
              {page}
            </a>
          </li>);
         }) }

          <li class={hasNextPage ? "page-item" : "page-item disabled"}>
            <a class="page-link" href="#">
              Next
            </a>
          </li>
        </ul>
      </nav>
    );
  }
}

export default Pagination;
