import React, { Component } from "react";
import Down from "../../images/icons/down";
import ExcB from "./../../images/icons/excIcon";
import SingleSelect from "../SingleSelect";

export default class Color extends Component {
  render() {
    const { colors } = this.props;
    if (!colors) {
      return null;
    }
    return (
      <span>
        <div className="row align-items-center">
          <div className="sm-box-p mb-5">
            <div className="sm-box-p mb-5">
              {colors.map(color => {
                const active = this.props.selectedColors.findIndex( i => i._id === color._id) === -1 ? false : true
                return (
                  <span
                    className={
                      active
                        ? "sm-box sm-text-fw6 text-uppercase active"
                        : "sm-box sm-text-fw6 text-uppercase "
                    }
                    onClick={() => this.props.selectColor(color)}
                  >
                    {color.name}
                  </span>
                );
              })}
            </div>
          </div>
        </div>
      </span>
    );
  }
}
