import React, { Component } from "react";
import Down from "../../images/icons/down";
import ExcB from "./../../images/icons/excIcon";
import SingleSelect from "../SingleSelect";
import Info from "../../images/icons/info";

export default class Size extends Component {
  state = {
    multiBox: false
  };
  render() {
    const { categoryData } = this.props;
    if (!categoryData) {
      return null;
    }
    return (
      <span>
        <div className="row align-items-center mb-5">
          <div className="col-12 col-md-5">
            {/* <SingleSelect
              placeholder="Select measuring scale"
              options={[
                { scale: "Lithuania" },
                { scale: "United states" },
                { scale: "United kingdom" },
                { scale: "European union" }
              ]}
              name="scale"
              onChange={this.props.onChangeSize}
            /> */}
            <div onClick={() =>
                this.setState({
                  multiBox: !this.state.multiBox
                })
              } className="ctm-select py-1 ctm-select-main">
              <div className="ctm-select-txt mr-2">
                <span className="select-img d-inline-flex pr-2">
                  <Info />
                </span>
                <span className="slct-txt">DE women’s</span>
                <span className="select-arr">
                  <Down />
                </span>
              </div>
              <div className={
              this.state.multiBox
                ? "ctm-select-box multi-select-b d-block"
                : "ctm-select-box multi-select-b"
            }>
                <div className="box-cnt">
                  <div className="ctm-select-option p-0">
                    <label className="ctm-container d-block">
                      option
                      <input type="checkbox" name="atr[]" />
                      <span className="checkmark" />
                    </label>
                  </div>
                  <div className="ctm-select-option p-0">
                    <label className="ctm-container d-block">
                      option
                      <input type="checkbox" name="atr[]" />
                      <span className="checkmark" />
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-7 x-sm-text-fw4">
            There are many measuring scales in the world, select yours
          </div>
        </div>

        <div className="sm-box-p mb-5">
          {this.props.allSizes.map(sz => {
            return (
              <span
                className={
                  this.props.selectedSizes.includes(sz)
                    ? "sm-box sm-text-fw6 text-uppercase active"
                    : "sm-box sm-text-fw6 text-uppercase "
                }
                onClick={() => this.props.selectSize(sz)}
              >
                {sz[this.props.selectedScaleValue]}
              </span>
            );
          })}
        </div>
      </span>
    );
  }
}
