import React, { Component } from "react";
import Close from "./../images/icons/close";

class CustomSelect extends Component {
  state = {
    clickOnSelect: false,
    selected: this.props.selectedText,
    data: ""
  };

  changeValue = e => {
    const valueLower = e.target.value.toLowerCase();
    const name = e.target.name;
    this.setState({ data: valueLower });
    this.props.onChange(valueLower, this.props.id);
  };

  render() {
    const { options, placeholder, additionalData, name } = this.props;

    if (!options) {
      return null;
    }
    return (
      <span>
        <div className="form-group">
          <select
            className="form-control custom-input"
            name={this.props.name}
            onChange={this.changeValue}
          >
            <option disabled>{placeholder}</option>
            {options.map((option, index) => {
              return (
                <option value={option[name]} key={index}>
                  {additionalData
                    ? option[name] + " " + additionalData
                    : option[name]}
                </option>
              );
            })}
          </select>
        </div>
      </span>
    );
  }
}

export default CustomSelect;
