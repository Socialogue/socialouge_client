/**
 * @flow
 */
import React from 'react';

type Props = {
  containerClass: string,
  message: string,
  onRetry: Function
};

const ListErrorView = ({ containerClass, message, onRetry, error }: Props) => {
  console.error(error);
  return (
    <section className={containerClass}>
      <section className='row mx-0'>
        <div className='col-6 px-0'>
          <h1 className='mb-0'>Whoops!</h1>
          <p className='my-3'>{message}</p>
          <p className='my-3'>Check console for detailed error message</p>
          <button className='btn btn-link pl-0' onClick={onRetry}>Retry</button>
        </div>
        <div className='col-6 text-right px-0'>
          <img src='/img/warning_sign.png' alt='Warning' />
        </div>
      </section>
    </section>
  )
};

export default ListErrorView;
