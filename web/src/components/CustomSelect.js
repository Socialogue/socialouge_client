import React, { Component } from "react";
import Close from "./../images/icons/close";

class CustomSelect extends Component {
  state = {
    clickOnSelect: false,
    selected: this.props.selectedText,
    data: []
  };

  remove = val => {
    let data = [...this.state.data];
    if (data.includes(val)) {
      const pos = data.indexOf(val);
      data.splice(pos, 1);
      this.setState({
        data: data
      });
      this.props.onChange(this.state.data, this.props.id);
    }
  };

  changeValue = e => {
    const id = e.target.value;
    let data = [...this.state.data];
    let values = [...this.props.options];
    let dataToPush = values.filter(val => val._id == id);

    if (!data.includes(dataToPush[0])) {
      data.push(dataToPush[0]);
      this.setState({
        data: data
      }, () => {
        this.props.onChange(this.state.data, this.props.id);
      });
    }
  };

  render() {
    const { options, placeholder, name } = this.props;
    if (!options) {
      return null;
    }
    return (
      <span>
        <div className="form-group">
          <select
            className="form-control custom-input"
            name="brand"
            onChange={this.changeValue}
          >
            <option disabled>{placeholder}</option>
            {options.map((option, index) => {
              return (
                <option value={option["_id"]} key={index}>
                  {option[name]}
                </option>
              );
            })}
          </select>
        </div>

        <div className="form-group row mr-0">
          {this.state.data &&
            this.state.data.map((dt, index) => {
              return (
                <div className="col-12 col-md-4 pr-0 mb-1" key={index}>
                  <button className="btn black-bg-btn sm-black-btn bg-gray-btn w-100">
                    {dt[name]}{" "}
                    <span className="close-b" onClick={() => this.remove(dt)}>
                      <Close />
                    </span>
                  </button>
                </div>
              );
            })}
        </div>
      </span>
    );
  }
}

export default CustomSelect;
