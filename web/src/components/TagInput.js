import React, { Component } from "react";
import Close from "../images/icons/close";

export default class TagInput extends Component {
  state = {
    tags: []
  };

  addTags = event => {
    if (
      (event.key === "Enter" || event.key === "Tab") &&
      event.target.value !== ""
    ) {
      console.log("tags: ", event.target.value);
      this.setState({ tags: [...this.state.tags, event.target.value] });
      event.target.value = "";
      this.props.onChange(this.state.tags, this.props.id);
    }
  };

  removeTags = index => {
    let data = [...this.state.tags];
    data.splice(index, 1);
    this.setState({
      tags: data
    });
  };

  render() {
    const { tags } = this.state;
    return (
      <div>
        <div className="tags-input">
          <input
            type={this.props.type}
            className="form-control custom-input"
            placeholder={this.props.placeholder ? this.props.placeholder : ""}
            onKeyUp={event => this.addTags(event)}
            name={this.props.name}
          />
          <div className="row mt-2 ">
            {tags.map((tag, index) => (
              <div className="col-12 col-md-4 pr-0 mb-1" key={index}>
                <button className="btn black-bg-btn sm-black-btn w-100 bg-gray-btn">
                  {tag}{" "}
                  <span
                    className="close-b"
                    onClick={() => this.removeTags(index)}
                  >
                    <Close />
                  </span>
                </button>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}
