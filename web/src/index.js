import "./styles/index.scss";
import "bootstrap/dist/css/bootstrap.css";
import "./theme/global.scss";
import "./theme/form-module.scss";
import "./theme/Tab.scss";

import configureStore, { history } from "./redux/store/configureStore";

import App from "./App";
import Config from "./Config";
import Loadable from "react-loadable";
import { Provider } from "react-redux";
import React from "react";
import ReactDOM from "react-dom";
import { ConnectedRouter as Router } from "react-router-redux";
import { persistStore } from "redux-persist";
import sagas from "./redux/sagas/index";

// Initialize store with server side data if exists.
const store = configureStore({});
store.runSaga(sagas);

if (window.__SL_REDUX_STATE__) {
  delete window.__SL_REDUX_STATE__;
}
/** setup dev environment */
if (process.env.NODE_ENV === "development") {
  const { bootstrapDevelopment, logConfig } = require("./devSetup");
  logConfig();
  bootstrapDevelopment(store);
}

window.onload = () => {
  Loadable.preloadReady().then(() => {
    persistStore(store, { whitelist: Config.PERSIST_STORE_KEYS }, renderApp);
  });
};

function renderApp() {
  const renderMethod = module.hot ? ReactDOM.render : ReactDOM.hydrate;

  renderMethod(
    <Provider store={store}>
      <Router history={history}>
        <App />
      </Router>
    </Provider>,
    document.getElementById("root")
  );
}
