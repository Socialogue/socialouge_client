/**
 * @flow
 */
// $FlowFixMe
import React, { Fragment } from "react";
import { Switch, withRouter } from "react-router-dom";
import ReactNotification from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import routes from "./routes";
import PageRoute from "./routes/PageRoute";
import ScrollToTop from "./routes/ScrollToTop";
import FixedMessenger from "./pages/messenger/FixedMessenger";

const App = () => (
  <Fragment>
    <ReactNotification />
    <section>
      <ScrollToTop>
        <Switch>
          {routes.map((route, index) => (
            // $FlowFixMe

            <PageRoute
              // eslint-disable-next-line react/no-array-index-key
              key={`route-${index}`}
              path={route.path}
              exact={route.exact || false}
              component={route.component}
            />
          ))}
        </Switch>
      </ScrollToTop>
    </section>
    <FixedMessenger />
  </Fragment>
);

export default withRouter(App);
