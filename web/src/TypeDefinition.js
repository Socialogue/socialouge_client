/**
 * @flow
 */
export type RouteStaticContext = ?{
  baseUrl: string,
  fullUrl: string
};
