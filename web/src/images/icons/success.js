import React from "react";

const Success = () => {
  return (
    <svg
      width="69"
      height="69"
      viewBox="0 0 69 69"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M66 31.62V34.518C65.9961 41.3108 63.7966 47.9203 59.7294 53.3608C55.6622 58.8013 49.9454 62.7813 43.4314 64.7073C36.9174 66.6333 29.9554 66.402 23.5836 64.048C17.2118 61.6939 11.7717 57.3432 8.07453 51.6448C4.37739 45.9463 2.62134 39.2054 3.06828 32.4274C3.51522 25.6494 6.1412 19.1975 10.5546 14.0338C14.968 8.87016 20.9323 5.27148 27.558 3.77449C34.1837 2.2775 41.1158 2.96239 47.3205 5.72703"
        stroke="#5ACA3E"
        strokeWidth="5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M65.9959 9.31934L34.4959 40.8508L25.0459 31.4008"
        stroke="#5ACA3E"
        strokeWidth="5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default Success;
