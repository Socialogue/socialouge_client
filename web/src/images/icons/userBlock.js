import React from "react";
const UserBlock = () => (
  <svg
    width="20"
    height="16"
    viewBox="0 0 20 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M13.5 15.5V13.8333C13.5 12.9493 13.1488 12.1014 12.5237 11.4763C11.8986 10.8512 11.0507 10.5 10.1667 10.5H4.33333C3.44928 10.5 2.60143 10.8512 1.97631 11.4763C1.35119 12.1014 1 12.9493 1 13.8333V15.5"
      stroke="#111111"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7.25002 7.16667C9.09097 7.16667 10.5834 5.67428 10.5834 3.83333C10.5834 1.99238 9.09097 0.5 7.25002 0.5C5.40907 0.5 3.91669 1.99238 3.91669 3.83333C3.91669 5.67428 5.40907 7.16667 7.25002 7.16667Z"
      stroke="#111111"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M15.1667 4.66602L19.3334 8.83268"
      stroke="#111111"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M19.3334 4.66602L15.1667 8.83268"
      stroke="#111111"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
export default UserBlock;
