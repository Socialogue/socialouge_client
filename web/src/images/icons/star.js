import React from "react";

const Star = ({ fill }) => {
  return (
    <svg
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill={fill ? fill : "none"}
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M10 2L12.781 7.59516L19 8.4979L14.5 12.8507L15.562 19L10 16.0952L4.438 19L5.5 12.8507L1 8.4979L7.219 7.59516L10 2Z"
        stroke="black"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default Star;
