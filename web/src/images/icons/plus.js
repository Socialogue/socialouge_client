import React from "react";

const Plus = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <line
      x1="12"
      y1="-1.47963e-08"
      x2="12"
      y2="23.7679"
      stroke="#868686"
      strokeWidth="2"
    />
    <line
      x1="-2.87146e-09"
      y1="11"
      x2="23.7679"
      y2="11"
      stroke="#868686"
      strokeWidth="2"
    />
  </svg>
);
export default Plus;
