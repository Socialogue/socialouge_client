import React from 'react';

const Icon = () => (
    <svg width="18" height="24" viewBox="0 0 18 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <line x1="9" y1="-1.47963e-08" x2="9" y2="23.7679" stroke="#868686" strokeWidth="2"/>
    <path d="M9 0C9 6.39872 3.66667 8.86644 1 9.30046" stroke="#868686" strokeWidth="2"/>
    <path d="M9 0C9 6.39872 14.3333 8.86644 17 9.30046" stroke="#868686" strokeWidth="2"/>
    </svg>
);
export default Icon;