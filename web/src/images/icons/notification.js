import React from "react";

const Notification = () => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M11.6083 17.666C11.4618 17.9186 11.2515 18.1282 10.9985 18.274C10.7455 18.4197 10.4586 18.4964 10.1666 18.4964C9.87466 18.4964 9.5878 18.4197 9.33479 18.274C9.08177 18.1282 8.87148 17.9186 8.72498 17.666"
      stroke="#111111"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M15.6917 11.0007C15.321 9.64349 15.1442 8.24069 15.1667 6.83398"
      stroke="#111111"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M5.38329 5.38281C5.23851 5.85249 5.16547 6.34133 5.16663 6.83281C5.16663 12.6661 2.66663 14.3328 2.66663 14.3328H14.3333"
      stroke="#111111"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M15.1667 6.83359C15.1681 5.92781 14.9233 5.03868 14.4587 4.26117C13.994 3.48366 13.3269 2.84697 12.5285 2.41912C11.7301 1.99128 10.8305 1.78834 9.92581 1.83198C9.02108 1.87563 8.1452 2.16422 7.39172 2.66692"
      stroke="#111111"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M1 1L19.3333 19.3333"
      stroke="#111111"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
export default Notification;
