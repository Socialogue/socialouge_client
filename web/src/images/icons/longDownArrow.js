import React from "react";

const LongDownArrow = () => {
  return (
    <svg
      width="10"
      height="47"
      viewBox="0 0 10 47"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <line
        x1="4.84766"
        y1="46.5996"
        x2="4.84766"
        y2="0.399613"
        stroke="black"
      />
      <path
        d="M4.82617 46.5996C4.82617 34.1618 7.60878 29.365 9.00009 28.5213"
        stroke="black"
      />
      <path
        d="M4.82617 46.5996C4.82617 34.1618 2.04356 29.365 0.65226 28.5213"
        stroke="black"
      />
    </svg>
  );
};

export default LongDownArrow;
