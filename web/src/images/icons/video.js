import React from 'react';

const Icon = () => (
    <svg width="18" height="12" viewBox="0 0 18 12" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M17 2L12 5.5L17 9V2Z" stroke="white" strokeLinecap="round" strokeLinejoin="round"/>
    <path d="M10.5333 1H2.46667C1.65665 1 1 1.63959 1 2.42857V9.57143C1 10.3604 1.65665 11 2.46667 11H10.5333C11.3434 11 12 10.3604 12 9.57143V2.42857C12 1.63959 11.3434 1 10.5333 1Z" stroke="white" strokeLinecap="round" strokeLinejoin="round"/>
    </svg>
);
export default Icon;
