import React from "react";

const PhotoUpload = () => {
  return (
    <svg
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M15.8333 2.5H4.16667C3.24619 2.5 2.5 3.24619 2.5 4.16667V15.8333C2.5 16.7538 3.24619 17.5 4.16667 17.5H15.8333C16.7538 17.5 17.5 16.7538 17.5 15.8333V4.16667C17.5 3.24619 16.7538 2.5 15.8333 2.5Z"
        stroke="black"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M7.08398 8.33301C7.77434 8.33301 8.33398 7.77336 8.33398 7.08301C8.33398 6.39265 7.77434 5.83301 7.08398 5.83301C6.39363 5.83301 5.83398 6.39265 5.83398 7.08301C5.83398 7.77336 6.39363 8.33301 7.08398 8.33301Z"
        stroke="black"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M17.4993 12.4997L13.3327 8.33301L4.16602 17.4997"
        stroke="black"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};
export default PhotoUpload;
