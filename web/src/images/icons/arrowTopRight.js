import React from "react";

const ArrowTopRight = () => {
  return (
    <svg
      width="6"
      height="6"
      viewBox="0 0 6 6"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M0.64978 5.16667L5.23887 1"
        stroke="#29D425"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M0.64978 1H5.23887V5.16667"
        stroke="#29D425"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default ArrowTopRight;
