import React from "react";
const User = () => (
  <svg
    width="15"
    height="16"
    viewBox="0 0 15 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M13.8333 15.5V13.8333C13.8333 12.9493 13.4821 12.1014 12.857 11.4763C12.2319 10.8512 11.3841 10.5 10.5 10.5H3.83333C2.94928 10.5 2.10143 10.8512 1.47631 11.4763C0.851189 12.1014 0.5 12.9493 0.5 13.8333V15.5"
      stroke="#111111"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7.16665 7.16667C9.0076 7.16667 10.5 5.67428 10.5 3.83333C10.5 1.99238 9.0076 0.5 7.16665 0.5C5.3257 0.5 3.83331 1.99238 3.83331 3.83333C3.83331 5.67428 5.3257 7.16667 7.16665 7.16667Z"
      stroke="#111111"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
export default User;
