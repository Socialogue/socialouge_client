import React from "react";
const ArrowRight = () => (
  <svg
    width="12"
    height="10"
    viewBox="0 0 12 10"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <line x1="12" y1="5.15234" x2="9.97386e-07" y2="5.15234" stroke="black" />
    <path
      d="M12 5.17383C8.76939 5.17383 7.52348 2.39122 7.30435 0.999915"
      stroke="black"
    />
    <path
      d="M12 5.17383C8.76939 5.17383 7.52348 7.95644 7.30435 9.34774"
      stroke="black"
    />
  </svg>
);
export default ArrowRight;
