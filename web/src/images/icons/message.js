import React from "react";

const Message = () => {
  return (
    <svg
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M3.33073 3.33301H16.6641C17.5807 3.33301 18.3307 4.08301 18.3307 4.99967V14.9997C18.3307 15.9163 17.5807 16.6663 16.6641 16.6663H3.33073C2.41406 16.6663 1.66406 15.9163 1.66406 14.9997V4.99967C1.66406 4.08301 2.41406 3.33301 3.33073 3.33301Z"
        stroke="black"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M18.3307 5L9.9974 10.8333L1.66406 5"
        stroke="black"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default Message;
