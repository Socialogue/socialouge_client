import React from "react";

const Screen = () => (
  <svg
    width="15"
    height="15"
    viewBox="0 0 15 15"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M3.125 5.625L1.25 7.5L3.125 9.375"
      stroke="black"
      strokeWidth="0.833333"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M5.625 3.125L7.5 1.25L9.375 3.125"
      stroke="black"
      strokeWidth="0.833333"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M9.375 11.875L7.5 13.75L5.625 11.875"
      stroke="black"
      strokeWidth="0.833333"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M11.875 5.625L13.75 7.5L11.875 9.375"
      stroke="black"
      strokeWidth="0.833333"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M1.25 7.5H13.75"
      stroke="black"
      strokeWidth="0.833333"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M7.5 1.25V13.75"
      stroke="black"
      strokeWidth="0.833333"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
export default Screen;
