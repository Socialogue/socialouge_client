import React from "react";

const Dot = () => (
  <svg
    width="16"
    height="4"
    viewBox="0 0 16 4"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M7.99999 2.83268C8.46023 2.83268 8.83332 2.45959 8.83332 1.99935C8.83332 1.53911 8.46023 1.16602 7.99999 1.16602C7.53975 1.16602 7.16666 1.53911 7.16666 1.99935C7.16666 2.45959 7.53975 2.83268 7.99999 2.83268Z"
      stroke="black"
      strokeWidth="0.833333"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M13.8333 2.83268C14.2936 2.83268 14.6667 2.45959 14.6667 1.99935C14.6667 1.53911 14.2936 1.16602 13.8333 1.16602C13.3731 1.16602 13 1.53911 13 1.99935C13 2.45959 13.3731 2.83268 13.8333 2.83268Z"
      stroke="black"
      strokeWidth="0.833333"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M2.16668 2.83268C2.62691 2.83268 3.00001 2.45959 3.00001 1.99935C3.00001 1.53911 2.62691 1.16602 2.16668 1.16602C1.70644 1.16602 1.33334 1.53911 1.33334 1.99935C1.33334 2.45959 1.70644 2.83268 2.16668 2.83268Z"
      stroke="black"
      strokeWidth="0.833333"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
export default Dot;
