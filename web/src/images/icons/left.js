import React from "react";

const Left = () => {
  return (
    <svg
      width="4"
      height="7"
      viewBox="0 0 4 7"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M0 3.57227C2.064 3.57227 2.86 5.28655 3 6.14369"
        stroke="black"
      />
      <path
        d="M0 3.57227C2.064 3.57227 2.86 1.85798 3 1.00084"
        stroke="black"
      />
    </svg>
  );
};

export default Left;
