import React from "react";

const Down = () => (
  <svg
    width="10"
    height="6"
    viewBox="0 0 10 6"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M5.07031 5.75195C5.07031 2.48266 7.78276 1.22182 9.13898 1.00007"
      stroke="black"
    />
    <path
      d="M5.07031 5.75195C5.07031 2.48266 2.35786 1.22182 1.00164 1.00007"
      stroke="black"
    />
  </svg>
);
export default Down;
