import React from "react";

const Up = () => {
  return (
    <svg
      width="16"
      height="10"
      viewBox="0 0 16 10"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M8 0C8 6.192 3.33334 8.58 1.00001 9"
        stroke="black"
        strokeWidth="2"
      />
      <path
        d="M8 0C8 6.192 12.6667 8.58 15 9"
        stroke="black"
        strokeWidth="2"
      />
    </svg>
  );
};

export default Up;
