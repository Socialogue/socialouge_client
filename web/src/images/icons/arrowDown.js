import React from "react";

const ArrowDown = () => {
  return (
    <svg
      width="18"
      height="25"
      viewBox="0 0 18 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <line
        x1="9"
        y1="24.125"
        x2="9"
        y2="0.357147"
        stroke="black"
        stroke-width="2"
      />
      <path
        d="M9 24.125C9 17.7263 14.3333 15.2586 17 14.8245"
        stroke="black"
        strokeWidth="2"
      />
      <path
        d="M9 24.125C9 17.7263 3.66667 15.2586 0.999999 14.8245"
        stroke="black"
        strokeWidth="2"
      />
    </svg>
  );
};

export default ArrowDown;
