import React from "react";

const Icon = () => (
  <svg
    width="31"
    height="31"
    viewBox="0 0 31 31"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M20.6666 20.6667L15.4999 15.5L10.3333 20.6667"
      stroke="#868686"
      strokeWidth="0.833333"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M15.5 15.5V27.125"
      stroke="#868686"
      strokeWidth="0.833333"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M26.337 23.7545C27.5968 23.0677 28.592 21.9809 29.1656 20.6656C29.7391 19.3504 29.8583 17.8816 29.5044 16.491C29.1505 15.1005 28.3436 13.8674 27.211 12.9864C26.0784 12.1054 24.6848 11.6267 23.2499 11.6257H21.6224C21.2314 10.1135 20.5027 8.70958 19.4911 7.51951C18.4794 6.32945 17.2111 5.38421 15.7816 4.75485C14.3521 4.12549 12.7984 3.8284 11.2375 3.88591C9.67665 3.94342 8.1491 4.35404 6.76974 5.08688C5.39038 5.81973 4.19511 6.85574 3.27377 8.11702C2.35244 9.37831 1.72903 10.832 1.4504 12.3689C1.17178 13.9058 1.24519 15.4859 1.66512 16.9904C2.08505 18.4948 2.84058 19.8845 3.87489 21.0549"
      stroke="#868686"
      strokeWidth="0.833333"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M20.6666 20.6667L15.4999 15.5L10.3333 20.6667"
      stroke="#868686"
      strokeWidth="0.833333"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);
export default Icon;
