import React from "react";
const Right = () => (
  <svg
    width="4"
    height="7"
    viewBox="0 0 4 7"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path d="M4 3.57031C1.936 3.57031 1.14 1.85603 1 0.998884" stroke="black" />
    <path d="M4 3.57031C1.936 3.57031 1.14 5.2846 1 6.14174" stroke="black" />
  </svg>
);
export default Right;
