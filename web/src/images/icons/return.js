import React from "react";

const Return = () => {
  return (
    <svg
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M7.49998 8.3335L3.33331 12.5002L7.49998 16.6668"
        stroke="#868686"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M16.6666 3.3335V9.16683C16.6666 10.0509 16.3155 10.8987 15.6903 11.5239C15.0652 12.149 14.2174 12.5002 13.3333 12.5002H3.33331"
        stroke="#868686"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default Return;
