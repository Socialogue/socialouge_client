import React from "react";

const Dashbord = () => {
  return (
    <svg
      width="15"
      height="15"
      viewBox="0 0 15 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M12.5556 1H2.44444C1.6467 1 1 1.6467 1 2.44444V12.5556C1 13.3533 1.6467 14 2.44444 14H12.5556C13.3533 14 14 13.3533 14 12.5556V2.44444C14 1.6467 13.3533 1 12.5556 1Z"
        stroke="#111111"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M1 5.33301H14"
        stroke="#111111"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M5.33203 13.9997V5.33301"
        stroke="#111111"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default Dashbord;
