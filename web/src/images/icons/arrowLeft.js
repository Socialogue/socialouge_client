import React from "react";

const ArrowLeft = () => (
  <svg
    width="12"
    height="10"
    viewBox="0 0 12 10"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <line y1="5.19531" x2="12" y2="5.19531" stroke="black" />
    <path
      d="M0 5.17383C3.23061 5.17383 4.47652 7.95644 4.69565 9.34774"
      stroke="black"
    />
    <path
      d="M0 5.17383C3.23061 5.17383 4.47652 2.39122 4.69565 0.999915"
      stroke="black"
    />
  </svg>
);
export default ArrowLeft;
