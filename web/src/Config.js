/**
 * @flow
 */
import invariant from "invariant";

type Config = {
  API_URL: ?string,
  API_VERSION: ?string,
  FIREBASE_API_KEY: ?string,
  FIREBASE_MESSAGING_ID: ?string,
  GOOGLE_MAP_API_KEY: ?string,
  GUEST_SECRET: ?string,
  REDUX_LOGGER: boolean,
  SITE_NAME: ?string,
  PERSIST_STORE_KEYS: Array<string>,
  FACEBOOK_APP_ID: ?string
};

// PRODUCTION CONFIG
// eslint-disable-next-line import/no-mutable-exports
let config: Config = {
  API_URL: process.env.SL_API_URL,
  FILE_SERVER_URL: process.env.FILE_SERVER_URL,
  FILE_SERVER_KEY: process.env.FILE_SERVER_KEY,
  API_VERSION: process.env.SL_API_VERSION,
  FIREBASE_API_KEY: process.env.SL_FIREBASE_API_KEY,
  FIREBASE_MESSAGING_ID: process.env.SL_FIREBASE_MESSAGING_ID,
  GOOGLE_MAP_API_KEY: process.env.SL_GOOGLE_MAP_API_KEY,
  GUEST_SECRET: process.env.SL_GUEST_SECRET,
  REDUX_LOGGER: false,
  SITE_NAME: process.env.SL_SITE_NAME,
  PERSIST_STORE_KEYS: ["auth", "cart", "account"],
  FACEBOOK_APP_ID: process.env.SL_FACEBOOK_APP_ID,
  GOOGLE_CLIENT_ID: process.env.SL_GOOGLE_CLIENT_ID,
  MESSAGING_API: process.env.SL_MESSAGING_API
};

if (process.env.NODE_ENV === "development") {
  const devConfig = require("./config.development.js").default;
  config = {
    ...config,
    ...devConfig
  };
}

invariant(config.API_URL, "API_URL is not set in .env");
invariant(config.API_VERSION, "API_VERSION is not set in .env");
invariant(config.FIREBASE_API_KEY, "FIREBASE_API_KEY is not set in .env");
invariant(
  config.FIREBASE_MESSAGING_ID,
  "FIREBASE_MESSAGING_ID is not set in .env"
);
invariant(config.GOOGLE_MAP_API_KEY, "GOOGLE_MAP_API_KEY is not set in .env");
invariant(config.SITE_NAME, "SITE_NAME is not set in .env");

export default config;
