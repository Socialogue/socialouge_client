import isFunction from 'lodash/isFunction';
import config from './Config';


export function logConfig() {
  if (!config.CONFIG_LOGGER) return;

  /* eslint-disable no-console */
  console.log('--------------App Config--------------');
  Object.keys(config).forEach(key => console.log(`${key}: ${String(config[key])}`));
  console.log('--------------App Config--------------');
  console.log('--------------ENV Config--------------');
  Object.keys(process.env).forEach(key => console.log(`${key}: ${String(process.env[key])}`));
  console.log('--------------ENV Config--------------');
  /* eslint-enable no-console */
}

export function bootstrapDevelopment(store) {
  if (!isFunction(config.bootstrapDevelopment)) return;

  config.bootstrapDevelopment(store);
}
