/**
 * @flow
 */
import * as React from 'react'
import { connect } from 'react-redux'
import { Redirect, Route } from 'react-router-dom'
import { selectIsLoggedIn, selectUser } from 'core/selectors/user'
import { fetchConversations } from 'core/messenger/actionCreators/conversationsActionCreators'

import * as Routes from './constants'

type Props = {
  component: React.ComponentType<any>,
  dispatch: Function,
  IsLoggedIn: boolean,
  path: string,
}
//TODO for protect route after logout....
const redirectRouteMap = {
  loggedOut: {
    from: [
      Routes.ROUTE_ABOUT,
      Routes.ROUTE_USER_PROFILE_POST,
      Routes.ROUTE_USER_OWN_PROFILE_POST,
      Routes.ROUTE_USER_OWN_PROFILE_FAVORITE,
      Routes.ROUTE_USER_OWN_PROFILE_FOLLOWING,
      Routes.ROUTE_USER_OWN_PROFILE_LIKE,
      Routes.ROUTE_USER_OWN_PROFILE_ORDER,
      Routes.ROUTE_USER_OWN_PROFILE_REVIEW,
      Routes.ROUTE_USER_SETTINGS,
      Routes.ROUTE_SELLING_START,
      Routes.ROUTE_SELLING_APPLY_SHOP,
      Routes.ROUTE_SELLER_FINANCE,
      Routes.ROUTE_SELLER_ORDERS,
      Routes.ROUTE_SELLER_PRODUCTS_LIST,
      Routes.ROUTE_SELLER_RETURNS,
      Routes.ROUTE_CREATE_PRODUCT,
    ],
    to: Routes.ROUTE_LOGIN,
  },
  loggedIn: {
    from: [Routes.ROUTE_LOGIN],
    to: Routes.ROUTE_HOME,
  },
}

class PageRoute extends React.Component<Props> {
  _generateRoutePath = (route) => route.pathname + route.search + route.hash

  _renderRedirectComponent = (props, url) => (
    <Redirect
      to={{
        pathname: url,
        state: { from: props.location },
      }}
    />
  )

  _renderRoute = (routeProps) => {
    /**
     * 1) If not logged In, redirect if it matches the redirectionMap. Otherwise, render.
     * 2) If logged In but profile is not complete -
     *    - Redirect routes to /profile if they match the redirectionMap
     *    - Else render
     * 3) If logged In and profile is complete -
     *    - If index route, redirect to /feed
     *    - render all routes normally
     */
    const { component: Component, IsLoggedIn, path } = this.props
    let redirectKey = ''

    if (IsLoggedIn) {
      redirectKey = 'loggedIn'
      // this.props.fetchConversations(this.props.user._id)
      console.log({ redirectKey })
    } else {
      redirectKey = 'loggedOut'
    }

    if (redirectRouteMap[redirectKey].from.includes(path)) {
      return this._renderRedirectComponent(
        routeProps,
        redirectRouteMap[redirectKey].to,
      )
    }

    return <Component {...routeProps} />
  }

  render() {
    const {
      component: Component,
      dispatch,
      isPartiallyLoggedIn,
      isProfileComplete,
      ...rest
    } = this.props

    return <Route {...rest} render={this._renderRoute} />
  }
}

export default connect(
  (state) => ({
    IsLoggedIn: selectIsLoggedIn(state),
    user: selectUser(state),
  }),
  {
    fetchConversations,
  },
)(PageRoute)
