import Loadable from "react-loadable";

import * as ROUTES from "./constants";
import PageLoadingSpinner from "../components/PageLoadingSpinner";

const About = Loadable({
  loader: () => import("../pages/About" /* webpackChunkName: "about" */),
  loading: PageLoadingSpinner,
  modules: ["about"]
});

const Faq = Loadable({
  loader: () => import("../pages/faq/index.js" /* webpackChunkName: "faq" */),
  loading: PageLoadingSpinner,
  modules: ["Faq"]
});

const Home = Loadable({
  loader: () => import("../pages/home" /* webpackChunkName: "faq" */),
  loading: PageLoadingSpinner,
  modules: ["home"]
});

const Login = Loadable({
  loader: () => import("../pages/login" /* webpackChunkName: "faq" */),
  loading: PageLoadingSpinner,
  modules: ["login"]
});

const Register = Loadable({
  loader: () => import("../pages/register" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["register"]
});
const ForgotPassword = Loadable({
  loader: () =>
    import("../pages/forgotPassword" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["forgotPassword"]
});
const ForgotPasswordOtpVefify = Loadable({
  loader: () =>
    import("../pages/forgotPassword/otpVerify" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["forgotPasswordOtpVerify"]
});

const ResetPassword = Loadable({
  loader: () =>
    import("../pages/resetPassword" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["resetPassword"]
});

const Otp = Loadable({
  loader: () => import("../pages/otp" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["otp"]
});
const CreateShop = Loadable({
  loader: () =>
    import("../pages/createShop" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["createShop"]
});

// user profile
const UserProfilePost = Loadable({
  loader: () =>
    import("../pages/publicProfile/userProfilePost" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["userProfilePost"]
});

const UserProfileFavorite = Loadable({
  loader: () =>
    import("../pages/publicProfile/userProfileFavorite" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["userProfileFavorite"]
});

const UserProfileFollowing = Loadable({
  loader: () =>
    import("../pages/publicProfile/userProfileFollowing" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["userProfileFollowing"]
});
const UserProfileReview = Loadable({
  loader: () =>
    import("../pages/publicProfile/userProfileReview" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["userProfileReview"]
});

// user own profile

const UserOwnProfilePost = Loadable({
  loader: () =>
    import("../pages/profile/userOwnProfile/userOwnProfilePost" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["UserOwnProfilePost"]
});

const UserOwnProfileFavorite = Loadable({
  loader: () =>
    import("../pages/profile/userOwnProfile/userOwnProfileFavorite" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["userOwnProfileFavorite"]
});

const UserOwnProfileFollowing = Loadable({
  loader: () =>
    import("../pages/profile/userOwnProfile/userOwnProfileFollowing" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["userOwnProfileFollowing"]
});
const UserOwnProfileReview = Loadable({
  loader: () =>
    import("../pages/profile/userOwnProfile/userOwnProfileReview" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["userOwnProfileReview"]
});

const UserOwnProfileLike = Loadable({
  loader: () =>
    import("../pages/profile/userOwnProfile/userOwnProfileLike" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["userOwnProfileLike"]
});

const UserOwnProfileOrder = Loadable({
  loader: () =>
    import("../pages/profile/userOwnProfile/userOwnProfileOrder" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["userOwnProfileOrder"]
});

const CreateProduct = Loadable({
  loader: () =>
    import("../pages/createProduct" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["createProduct"]
});
const Messenger = Loadable({
  loader: () => import("../pages/messenger" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["messenger"]
});
const ProductListing = Loadable({
  loader: () =>
    import("../pages/productListing" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["ProductListing"]
});
const UserSettings = Loadable({
  loader: () =>
    import("../pages/userSettings" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["UserSettings"]
});
const SuccessLogin = Loadable({
  loader: () =>
    import("../pages/successLogin" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["SuccessLogin"]
});
const Verification = Loadable({
  loader: () =>
    import("../pages/verification" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["Verification"]
});
const ShopProfileTermsConditions = Loadable({
  loader: () =>
    import("../pages/shopProfile/termsConditions" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["ShopProfileTermsConditions"]
});
const ShopProfileAbout = Loadable({
  loader: () =>
    import("../pages/shopProfile/about" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["ShopProfileAbout"]
});
const ShopProfileProducts = Loadable({
  loader: () =>
    import("../pages/shopProfile/products" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["ShopProfileProducts"]
});
const ShopProfilePosts = Loadable({
  loader: () =>
    import("../pages/shopProfile/posts" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["ShopProfilePosts"]
});
const ShopProfileReviews = Loadable({
  loader: () =>
    import("../pages/shopProfile/reviews" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["ShopProfileReviews"]
});
const Products = Loadable({
  loader: () => import("../pages/products" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["Products"]
});
const ProductDetails = Loadable({
  loader: () => import("../pages/productDetails" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["ProductDetails"]
});
const ShopList = Loadable({
  loader: () => import("../pages/shopList" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["ShopList"]
});
const StartSelling = Loadable({
  loader: () => import("../pages/selling/startSelling" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["StartSelling"]
});
const ApplyShop = Loadable({
  loader: () => import("../pages/selling/applyShop" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["ApplyShop"]
});
const PrivacyPolicy = Loadable({
  loader: () => import("../pages/privacyPolicy" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["PrivacyPolicy"]
});
const AboutUs = Loadable({
  loader: () => import("../pages/aboutUs" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["AboutUs"]
});
const TermsService = Loadable({
  loader: () => import("../pages/termsService" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["TermsService"]
});
const SellerProducstList = Loadable({
  loader: () => import("../pages/seller/productsList" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["SellerProducstList"]
});
const SellerOrders = Loadable({
  loader: () => import("../pages/seller/orders" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["SellerOrders"]
});
const SellerReturns = Loadable({
  loader: () => import("../pages/seller/returns" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["SellerReturns"]
});
const SellerFinance = Loadable({
  loader: () => import("../pages/seller/finance" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["SellerFinance"]
});
const Cart = Loadable({
  loader: () => import("../pages/cart" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["Cart"]
});
const Checkout = Loadable({
  loader: () => import("../pages/checkout" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["Checkout"]
});

const BuyerEmail = Loadable({
  loader: () => import("../pages/buyerEmail" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["BuyerEmail"]
});
const LeaveReview = Loadable({
  loader: () => import("../pages/leaveReview" /* webpackChunkName: "register" */),
  loading: PageLoadingSpinner,
  modules: ["LeaveReview"]
});
const routes = [
  {
    component: LeaveReview,
    exact: true,
    path: ROUTES.ROUTE_LEAVE_REVIEW
  },
  {
    component: BuyerEmail,
    exact: true,
    path: ROUTES.ROUTE_BUYER_EMAIL
  },
  {
    component: Checkout,
    exact: true,
    path: ROUTES.ROUTE_CHECKOUT
  },
  {
    component: Cart,
    exact: true,
    path: ROUTES.ROUTE_CART
  },
  {
    component: SellerFinance,
    exact: true,
    path: ROUTES.ROUTE_SELLER_FINANCE
  },
  {
    component: SellerReturns,
    exact: true,
    path: ROUTES.ROUTE_SELLER_RETURNS
  },
  {
    component: SellerOrders,
    exact: true,
    path: ROUTES.ROUTE_SELLER_ORDERS
  },
  {
    component: SellerProducstList,
    exact: true,
    path: ROUTES.ROUTE_SELLER_PRODUCTS_LIST
  },
  {
    component: ApplyShop,
    exact: true,
    path: ROUTES.ROUTE_SELLING_APPLY_SHOP
  },
  {
    component: TermsService,
    exact: true,
    path: ROUTES.ROUTE_TERMS_SERVICE
  },
  {
    component: AboutUs,
    exact: true,
    path: ROUTES.ROUTE_ABOUT_US
  },
  {
    component: PrivacyPolicy,
    exact: true,
    path: ROUTES.ROUTE_PRIVACY_POLICY
  },
  {
    component: StartSelling,
    exact: true,
    path: ROUTES.ROUTE_SELLING_START
  },
  {
    component: ShopList,
    exact: true,
    path: ROUTES.ROUTE_SHOP_LIST
  },
  {
    component: Products,
    exact: true,
    path: ROUTES.ROUTE_PRODUCTS
  },
  {
    component: ProductDetails,
    path: ROUTES.ROUTE_PRODUCT_DETAILS
  },
  {
    component: ShopProfileProducts,
    exact: true,
    path: ROUTES.ROUTE_SHOP_PROFILE_PRODUCTS
  },
  {
    component: ShopProfilePosts,
    exact: true,
    path: ROUTES.ROUTE_SHOP_PROFILE_POSTS
  },
  {
    component: ShopProfileReviews,
    exact: true,
    path: ROUTES.ROUTE_SHOP_PROFILE_REVIEWS
  },
  {
    component: ShopProfileAbout,
    exact: true,
    path: ROUTES.ROUTE_SHOP_PROFILE_ABOUT
  },
  {
    component: ShopProfileTermsConditions,
    exact: true,
    path: ROUTES.ROUTE_SHOP_PROFILE_TERMS_CONDITIONS
  },
  {
    component: Verification,
    exact: true,
    path: ROUTES.ROUTE_VERIFICATION
  },
  {
    component: SuccessLogin,
    exact: true,
    path: ROUTES.ROUTE_SUCCESS_LOGIN
  },
  {
    component: UserSettings,
    exact: true,
    path: ROUTES.ROUTE_USER_SETTINGS
  },
  {
    component: ProductListing,
    exact: true,
    path: ROUTES.ROUTE_PRODUCT_LISTING
  },
  {
    component: Messenger,
    exact: true,
    path: ROUTES.ROUTE_MESSENGER
  },
  {
    component: CreateProduct,
    exact: true,
    path: ROUTES.ROUTE_CREATE_PRODUCT
  },
  {
    component: UserOwnProfileLike,
    exact: true,
    path: ROUTES.ROUTE_USER_OWN_PROFILE_LIKE
  },
  {
    component: UserOwnProfileOrder,
    exact: true,
    path: ROUTES.ROUTE_USER_OWN_PROFILE_ORDER
  },
  {
    component: UserOwnProfileReview,
    exact: true,
    path: ROUTES.ROUTE_USER_OWN_PROFILE_REVIEW
  },
  {
    component: UserOwnProfileFollowing,
    exact: true,
    path: ROUTES.ROUTE_USER_OWN_PROFILE_FOLLOWING
  },
  {
    component: UserOwnProfileFavorite,
    exact: true,
    path: ROUTES.ROUTE_USER_OWN_PROFILE_FAVORITE
  },
  {
    component: UserOwnProfilePost,
    exact: true,
    path: ROUTES.ROUTE_USER_OWN_PROFILE_POST
  },
  {
    component: UserProfileReview,
    exact: true,
    path: ROUTES.ROUTE_USER_PROFILE_REVIEW
  },
  {
    component: UserProfileFollowing,
    exact: true,
    path: ROUTES.ROUTE_USER_PROFILE_FOLLOWING
  },
  {
    component: UserProfileFavorite,
    exact: true,
    path: ROUTES.ROUTE_USER_PROFILE_FAVORITE
  },
  {
    component: UserProfilePost,
    exact: true,
    path: ROUTES.ROUTE_USER_PROFILE_POST
  },
  {
    component: CreateShop,
    exact: true,
    path: ROUTES.ROUTE_CREATE_SHOP
  },
  {
    component: About,
    exact: true,
    path: ROUTES.ROUTE_ABOUT
  },
  {
    component: Faq,
    exact: true,
    path: ROUTES.ROUTE_FAQ
  },
  {
    component: Home,
    exact: true,
    path: ROUTES.ROUTE_HOME
  },
  {
    component: Login,
    exact: true,
    path: ROUTES.ROUTE_LOGIN
  },
  {
    component: Register,
    exact: true,
    path: ROUTES.ROUTE_REGISTER
  },
  {
    component: ForgotPassword,
    exact: true,
    path: ROUTES.ROUTE_FORGOT_PASSWORD
  },
  {
    component: ForgotPasswordOtpVefify,
    exact: true,
    path: ROUTES.ROUTE_FORGOT_PASSWORD_OTP_VERIFY
  },

  {
    component: ResetPassword,
    exact: true,
    path: ROUTES.ROUTE_RESET_PASSWORD
  },
  {
    component: Otp,
    exact: true,
    path: ROUTES.ROUTE_VERIFY_OTP
  }
];

export default routes;
