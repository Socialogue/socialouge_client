/**
 * @flow
 */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { selectCurrentRoute } from '../selectors/routeSelectors';

type Props = {
  dispatch: Function,
  onClick?: Function,
  route: {
    pathname: string,
    search: string,
    hash: string
  },
  to: string | {
    pathname: string,
    search: string,
    hash: string
  }
};

class PageLink extends Component<Props> {
  _handleClick = (event) => {
    const { onClick, route, to } = this.props;

    if (onClick) {
      onClick(event);
    }

    const currentUrl = route.pathname + route.search + route.hash;
    let targetUrl = '';

    if (typeof to === 'string') {
      targetUrl = to;
    } else {
      targetUrl = to.pathname + to.search + to.hash;
    }

    if (currentUrl === targetUrl) {
      event.preventDefault();
    }
  }

  render() {
    const { dispatch, onClick, route, ...rest } = this.props;

    return (
      <Link onClick={this._handleClick} {...rest} />
    );
  }
}

export default connect(
  state => ({
    route: selectCurrentRoute(state)
  })
)(PageLink);
