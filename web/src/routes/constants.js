export const ROUTE_PRODUCT_DETAILS = '/product/:slug/:productId/'
export const ROUTE_HOME = '/'
export const ROUTE_ABOUT = '/about'
export const ROUTE_FAQ = '/faq'
export const ROUTE_LOGIN = '/login'
export const ROUTE_REGISTER = '/register'
export const ROUTE_FORGOT_PASSWORD = '/forgot-password'
export const ROUTE_FORGOT_PASSWORD_OTP_VERIFY = '/forgot-password/otp'
export const ROUTE_RESET_PASSWORD = '/reset-password'
export const ROUTE_VERIFY_OTP = '/verify-otp'
export const ROUTE_CREATE_SHOP = '/create-shop'
export const ROUTE_CREATE_PRODUCT = '/create-product'

export const ROUTE_USER_PROFILE_POST = '/user/:id/profile'
export const ROUTE_USER_PROFILE_FAVORITE = '/user/:id/favorite'
export const ROUTE_USER_PROFILE_FOLLOWING = '/user/:id/following'
export const ROUTE_USER_PROFILE_REVIEW = '/user/:id/review'

export const ROUTE_USER_OWN_PROFILE_POST = '/profile'
export const ROUTE_USER_OWN_PROFILE_FAVORITE = '/profile/favorite'
export const ROUTE_USER_OWN_PROFILE_FOLLOWING = '/profile/following'
export const ROUTE_USER_OWN_PROFILE_REVIEW = '/profile/review'
export const ROUTE_USER_OWN_PROFILE_LIKE = '/profile/like'
export const ROUTE_USER_OWN_PROFILE_ORDER = '/profile/order'
export const ROUTE_MESSENGER = '/messenger'
export const ROUTE_PRODUCT_LISTING = '/product-listing'
export const ROUTE_USER_SETTINGS = '/user-settings'
export const ROUTE_SUCCESS_LOGIN = '/success-login'
export const ROUTE_VERIFICATION = '/verification'

export const ROUTE_SHOP_PROFILE_TERMS_CONDITIONS = '/shop/terms-conditions/:id'
export const ROUTE_SHOP_PROFILE_ABOUT = '/shop/about/:id'
export const ROUTE_SHOP_PROFILE_PRODUCTS = '/shop/products/:id'
export const ROUTE_SHOP_PROFILE_POSTS = '/shop/posts/:id'
export const ROUTE_SHOP_PROFILE_REVIEWS = '/shop/reviews/:id'

export const ROUTE_PRODUCTS =
  '/products/:category/:subCategory?/:innerCategory?'
export const ROUTE_SHOP_LIST = '/shop-list'

export const ROUTE_SELLING_START = '/selling/start'
export const ROUTE_SELLING_APPLY_SHOP = '/selling/apply-shop'
export const ROUTE_PRIVACY_POLICY = '/privacy-policy'
export const ROUTE_ABOUT_US = '/about-us'
export const ROUTE_TERMS_SERVICE = '/terms-service'

export const ROUTE_SELLER_PRODUCTS_LIST = '/seller/products-list'
export const ROUTE_SELLER_ORDERS = '/seller/orders'
export const ROUTE_SELLER_RETURNS = '/seller/returns'
export const ROUTE_SELLER_FINANCE = '/seller/finance'

export const ROUTE_CART = '/cart'
export const ROUTE_CHECKOUT = '/checkout'
export const ROUTE_BUYER_EMAIL = '/buyer-email'
export const ROUTE_LEAVE_REVIEW = '/leave-review'
