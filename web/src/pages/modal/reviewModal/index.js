import React, { Component } from "react";
import StarF from "../../../images/icons/starF";
import ProImg from "./../../../images/eco-img1.webp";

class ReviewModal extends Component {
  setStar(star, index, e) {
    document.getElementById("star").value = star;
    console.log(index);
    document.querySelectorAll(".r-star").forEach(function (userItem, i) {
      userItem.classList.remove("active");
    });
    document.querySelectorAll(".r-star").forEach(function (userItem, i) {
      if (i >= index - 1) userItem.classList.add("active");
    });
  }
  render() {
    return (
      <div
        className="modal d-block show"
        id="reviewModal"
        tabindex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-lg modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-body">
              <div className="font-weight-bold text-uppercase mb-3">
                SHOP REVIEWS (33)
              </div>
              <div className="">
                <div className="review py-4 border-bottom-1">
                  <div className="pro-cnt">
                    <div className="pro-img pro-img-md">
                      <img className="cover" src={ProImg} alt="img" />
                    </div>
                    <div className="pro-txt-cnt pro-txt-cnt-md pl-3">
                      <div className="mb-2 pb-1">
                        <span className="sm-text-fw6">
                          <span className="text-uppercase">UNiq butiq</span>
                          <span className="sm-text-fw5-s2 ml-2">
                            at 19 Jan 2018
                          </span>
                        </span>
                        <span className="float-right">
                          <span className="review-star pr-1 active">
                            <StarF />
                          </span>
                          <span className="review-star pr-1 active">
                            <StarF />
                          </span>
                          <span className="review-star pr-1 active">
                            <StarF />
                          </span>
                          <span className="review-star pr-1 active">
                            <StarF />
                          </span>
                          <span className="review-star pr-1">
                            <StarF />
                          </span>
                        </span>
                      </div>
                      <div className="sm-text-fw5-g mb-3">
                        It is a long established fact that a reader will be
                        distracted by the readable content of a page when
                        looking at its layout. The point of using Lorem Ipsum is
                        that it has a more-or-less normal distribution of
                        letters, as opposed to using
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="text-center pt-3 mb-5">
                <button className="btn border-btn border-0 p-0">
                  Show more...
                </button>
              </div>
              <form>
                <div className="form-group mb-4">
                  <div className="font-weight-bold text-uppercase mb-3">
                    Your review <sup>*</sup>
                  </div>
                  <textarea
                    className="form-control textarea-msg mb-3"
                    placeholder="Write review..."
                  />
                  <div className="color-gray7 font-weight-5">
                    Tell other people more about the product. What about the
                    quality? Or the comfort?
                  </div>
                </div>
                <div className="form-group mb-4">
                  <div className="font-weight-bold text-uppercase">
                    Your overall rating <sup>*</sup>
                  </div>
                  <div className="color-gray7 font-weight-5">Please select</div>
                  <div class="rating rating2">
                    <input type="hidden" name="star" id="star" />
                    <span
                      className="r-star cursor-pointer"
                      onClick={e => this.setStar(5, 1, e)}
                      title="Give 5 stars"
                    >
                      ★
                    </span>
                    <span
                      className="r-star cursor-pointer"
                      onClick={e => this.setStar(4, 2, e)}
                      title="Give 4 stars"
                    >
                      ★
                    </span>
                    <span
                      className="r-star cursor-pointer"
                      onClick={e => this.setStar(3, 3, e)}
                      title="Give 3 stars"
                    >
                      ★
                    </span>
                    <span
                      className="r-star cursor-pointer"
                      onClick={e => this.setStar(2, 4, e)}
                      title="Give 2 stars"
                    >
                      ★
                    </span>
                    <span
                      className="r-star cursor-pointer"
                      onClick={e => this.setStar(1, 5, e)}
                      title="Give 1 star"
                    >
                      ★
                    </span>
                  </div>
                </div>
                <div className="form-group mb-4  pb-2">
                  <div className="font-weight-bold text-uppercase mb-2">
                    Upload photo
                  </div>
                  <div className="d-flex upload-box">
                    <div className="upload-div1 position-relative">
                      Select photo +
                      <input
                        type="file"
                        className="ctm-hidden-upload-input cover"
                      />
                    </div>
                    <div className="upload-div2">
                      Upload your .PNG or .JPG file
                    </div>
                  </div>
                </div>
                <div className="form-group text-right mb-0">
                  <button className="btn border-btn px-5"> Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ReviewModal;
