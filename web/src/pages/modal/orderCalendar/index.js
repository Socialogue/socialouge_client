import React, { Component } from "react";
import ArrowTopRight from "../../../images/icons/arrowTopRight";

class OrderCalendar extends Component {
  render() {
    return (
      <div>
        <div
          className="modal d-block show"
          id="orderCalendar"
          tabindex="-1"
          role="dialog"
        >
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div className="modal-body">
                <div className="font-weight-bold text-uppercase mb-2">
                  Total sales
                </div>
                <div className="text-uppercase row align-items-center">
                  <div className="col-12 col-md-8">
                    <span className="sm-text-fw6 mr-3">Sales over time</span>
                    <span className="sm-text-fw6-g mr-3">
                      April 1, 2020 (today)
                    </span>
                    <span className="sm-text-fw6 color-orange mr-3">
                      12 sold
                    </span>
                  </div>
                  <div className="col-12 col-md-4 d-flex align-items-center justify-content-end">
                    <span className="md-title font-weight-6 mr-3">
                      € 143.43
                    </span>
                    <span className="sm-text-fw6 color-green">
                      43,2%
                      <sup className="c-sup">
                        <ArrowTopRight />
                      </sup>
                    </span>
                  </div>
                </div>
                <div className="my-3">
                  <span className="text-uppercase font-weight-bold">
                    Here will be Carve Calendar
                  </span>
                </div>
                <div className="text-uppercase font-weight-6 text-right">
                  <span className="all-date all-from-date mr-4">
                    April 1, 2020
                  </span>
                  <span className="all-date all-to-date">April 1, 2020</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default OrderCalendar;
