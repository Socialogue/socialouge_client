import React, { Component } from 'react'
import Love from '../../../images/icons/love'
import Comment from '../../../images/icons/comment'
import ArrowRight from '../../../images/icons/arrowRight'
import Img from '../../../images/register-img.webp'
import Dot from '../../../images/icons/dot'
import Close from '../../../images/icons/close'
import ReadMoreAndLess from '../../../components/readMore/readMore'
import moment from 'moment'
import { errorAlert } from '../../../utils/alerts'
import { trim } from 'lodash'
import Comments from '../../../components/Comment'
import { getImageOriginalUrl } from 'core/utils/helpers'
import socketIOClient from 'socket.io-client'
import LessThan from '../../../images/icons/lessThan'
import GreaterThan from '../../../images/icons/greaterThan'
import { connect } from 'react-redux'
import { selectPost, selectPostChange, selectUser } from 'core/selectors/user'
import { reactToPostSocket } from 'core/actions/postActionCreators'
import { loadMoreComment } from 'core/actions/commentActionCreators'
import Like from '../../../components/Like'

// import 'viewerjs/dist/viewer.css'
// import Viewer from 'viewerjs'

const ENDPOINT = 'http://localhost:5000/'

class PostModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      editBtn: false,
      commentText: '',
      postId: props.post._id,
      activeCommentSubmit: false,
      post: this.props.post,
      imageUrls: [],
      currentImage: '',
      alreadyLiked: !!props.post.postLikes.filter(
        (item) => item.userId === this.props.user._id,
      ).length,
      commentLimit: 2,
      commentSkip: 0,
      setComment: false,
    }
  }

  componentDidMount = () => {
    // making post images urls for slider...
    const urls = []
    this.state.post.images.map((image) => {
      urls.push(getImageOriginalUrl(image.url))
    })
    this.setState({ imageUrls: urls, currentImage: urls[0] })
  }

  componentDidUpdate = (prevProps) => {
    if (prevProps.postChangeStatus !== this.props.postChangeStatus) {
      if (this.props.postChangeStatus) {
        this.setState({ post: this.props.post })
      }
    }
  }

  componentWillUnmount = () => {
    this.props.close()
  }

  //handler for onChange on add comment box...
  onCommentChange = (e, postId) => {
    const commentText = trim(e.target.value)
    this.setState({ commentText: commentText, postId: postId }, () => {
      if (
        this.state.commentText &&
        this.state.commentText !== ' ' &&
        this.state.postId &&
        this.state.postId !== ''
      ) {
        this.setState({ activeCommentSubmit: true })
      } else {
        this.setState({ activeCommentSubmit: false })
      }
    })
  }

  // handler for comment submiting with api...
  onSubmitComment = () => {
    const commentText = this.state.commentText
    const postId = this.state.postId
    if (commentText === '' || postId === '') {
      return
    }

    // props coming from home/index...
    this.props.addingComment(commentText, postId)
  }

  // change slider image...
  changeImage = (num) => {
    const allImage = [...this.state.imageUrls]
    const currentImage = this.state.currentImage
    const currentIndex = allImage.indexOf(currentImage)

    // if number is -ve
    if (num == -1) {
      if (currentIndex + num < 0) {
        this.setState({ currentImage: allImage[allImage.length + num] })
      } else {
        this.setState({ currentImage: allImage[currentIndex + num] })
      }
    }

    // if number is +ve
    if (num == 1) {
      if (currentIndex + num < allImage.length) {
        this.setState({ currentImage: allImage[currentIndex + num] })
      } else {
        this.setState({ currentImage: allImage[0] })
      }
    }
  }

  // handler click on image
  showImage = () => {
    // View an image.
    const image = document.getElementById('image')
    const viewer = new Viewer(image, {
      inline: true,
      zoomable: false,
      button: true,
      view() {
        this.viewer.full()
        // this.viewer.destroy()
      },
    })

    // Then, show the image by clicking it, or call `viewer.show()`.

    // View a list of images.
    // Note: All images within the container will be found by calling `element.querySelectorAll('img')`.
    // const gallery = new Viewer(document.getElementById('images'))
    // Then, show one image by click it, or call `gallery.show()`.
  }

  reactToPost = () => {
    if (!this.props.user._id) {
      return
    }
    this.setState((prevState) => ({
      alreadyLiked: !prevState.alreadyLiked,
    }))
    this.props.reactToPostSocket(this.props.user._id, this.state.post._id)
  }

  // handler for loadmore comments...
  loadMoreComments = () => {
    this.setState(
      (prevState) => ({
        commentSkip: prevState.commentSkip + prevState.commentLimit,
      }),
      () => {
        // call action for load more comment using current limit and offset...
        this.props.loadMoreComment(
          this.state.postId,
          this.state.commentSkip,
          this.state.commentLimit,
        )
      },
    )
  }

  render() {
    const { activeCommentSubmit, imageUrls, currentImage } = this.state
    const { post } = this.props
    const { userId } = post
    if ((!post && userId.name == '') || !userId) {
      return null
    }
    return (
      <div
        className="modal d-block show"
        id="postDeModal"
        tabindex="-1"
        role="dialog"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-body">
              <span
                className="close-m btn border-btn p-0 border-0"
                onClick={this.props.close}
              >
                Close <Close />
              </span>
              <div className="product-post">
                <div className="row h-100">
                  <div className="col-12 col-md-6 mb-4 mb-md-0">
                    <div className="show-img-pp show-img-pp-pb-140">
                      {/* for slider control */}
                      <span
                        className="prev-btn scale-hover"
                        onClick={() => this.changeImage(-1)}
                      >
                        <LessThan />
                      </span>
                      <span
                        className="next-btn scale-hover"
                        onClick={() => this.changeImage(1)}
                      >
                        <GreaterThan />
                      </span>
                      {/* for slider control */}
                      <div className="show-img-p">
                        <img
                          className="cover"
                          src={currentImage ? currentImage : Img}
                          // onClick={this.showImage}
                          alt="img"
                        />
                      </div>
                      <div className="show-img-p d-none" id="image">
                        {imageUrls.map((url) => {
                          return (
                            <img
                              className="cover"
                              src={url ? url : Img}
                              // onClick={this.showImage}
                              alt="img"
                            />
                          )
                        })}
                      </div>
                      <img
                        src={post.images[0] ? post.images[0].url : Img}
                        alt="img"
                      />
                    </div>
                  </div>
                  <div className="col-12 col-md-6">
                    <div className="post-info ml-0 ml-md-3">
                      <div className="pro-cnt mb-3">
                        <div
                          className="pro-img cursor-pointer"
                          onClick={() => this.props.gotoProfile(userId._id)}
                        >
                          {' '}
                          <img
                            className="cover"
                            src={
                              userId
                                ? getImageOriginalUrl(userId.profileImage)
                                : Img
                            }
                            alt="img"
                          />
                        </div>
                        <div className="pro-txt-cnt">
                          <div>
                            <span
                              className="font-weight-bold text-uppercase cursor-pointer"
                              onClick={() => this.props.gotoProfile(userId._id)}
                            >
                              {userId.fullName}
                            </span>
                            {userId && userId._id !== this.props.user._id && (
                              <button className="btn border-btn btn-w-115 float-right">
                                Follow
                              </button>
                            )}
                          </div>
                          <div className="sm-text-fw4-g">
                            {' '}
                            {post.createdAt && moment(post.createdAt).fromNow()}
                          </div>
                        </div>
                      </div>
                      <div className="sm-text-fw4-g mb-3">
                        <ReadMoreAndLess
                          // ref={this.ReadMore}
                          className="btn border-btn"
                          charLimit={250}
                          readMoreText=" Read more"
                          readLessText="Read less"
                        >
                          {post.content}
                        </ReadMoreAndLess>
                      </div>
                      {/* <div className="mb-4">

                        <button className="btn border-btn border-0 p-0 text-uppercase">
                          Read more...
                        </button>
                      </div> */}
                      <div className="like-com-d sm-text-fw6 text-uppercase mb-3">
                        <span className="d-inline-flex mr-2">
                          <span className="com-icon">
                            <Like
                              fill={this.state.alreadyLiked}
                              react={this.reactToPost}
                              postId={post._id}
                            />
                          </span>
                          {post.postLikes.length | 0} likes
                        </span>
                        <span className="d-inline-flex">
                          <span className="com-icon">
                            <Comment />
                          </span>
                          {post.grandTotalComment | 0} comments
                        </span>
                      </div>

                      {/* comments component and comment reply is inside comment component */}
                      <div className="comment-box pl-4">
                        {this.props.post.comments.map((comment, i) => {
                          return (
                            <Comments comment={comment} key={comment._id} />
                          )
                        })}
                        <div
                        // onClick={() => this.loadMoreReply(end)}
                        >
                          {post.comments.length < post.totalComment && (
                            <button
                              className="btn border-btn border-0 p-0"
                              onClick={() => this.loadMoreComments()}
                            >
                              Load more comment...
                            </button>
                          )}
                        </div>
                      </div>
                      <div className="pro-bottom-part">
                        <div className="form-group ">
                          <div className="form-input position-relative form-input-append">
                            <input
                              type="text"
                              className="form-control custom-input"
                              placeholder="Add Comment..."
                              name="comment"
                              onChange={(e) =>
                                this.onCommentChange(e, post._id)
                              }
                            />
                            <span className="append-input d-block">
                              <button
                                className={
                                  activeCommentSubmit
                                    ? 'btn border-btn border-0 p-0'
                                    : 'btn border-btn border-0 p-0 disabled'
                                }
                                onClick={this.onSubmitComment}
                              >
                                Post{' '}
                                <span className="ml-1">
                                  <ArrowRight />
                                </span>
                              </button>
                            </span>
                          </div>
                        </div>{' '}
                        <div className="like-com-d sm-text-fw6 text-uppercase">
                          <span className="d-inline-flex mr-2">
                            <span className="com-icon">
                              <Love />
                            </span>
                            like
                          </span>
                          <span className="d-inline-flex">
                            <span className="com-icon">
                              <Comment />
                            </span>
                            comment
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    user: selectUser(state),
    post: selectPost(state),
    postChangeStatus: selectPostChange(state),
  }),
  {
    reactToPostSocket,
    loadMoreComment,
  },
)(PostModal)
