import React from 'react'
import { connect } from 'react-redux'
import { selectPosts } from 'core/selectors/posts'
import { fetchPosts } from 'core/actions/postActionCreators'
import { getProfileOwnerData } from 'core/selectors/profile'
import { errorAlert } from '../../../utils/alerts'
import { fetchProfileOwner } from 'core/actions/publicProfileActionCreator'
import { fetchFollowings } from 'core/actions/shopActionCreators'
import { selectFollowings, selectFollowingsCount } from 'core/selectors/shops'

import Header from './../../home/Header/Header'
import Footer from './../../home/Footer/Footer'

// user profile
import UserInfo from './../../profile/common/userInfo/userInfo'
import Tab from './../../profile/common/tab/tab'
import Following from './../../profile/common/following/following'
import './../../profile/index.scss'
import CoverPhoto from '../../profile/common/coverPhoto'
import Img from './../../../images/eco-img.webp'
import FixedFooter from '../../Common/fixedFooter'
import { ROUTE_PRODUCT_DETAILS } from '../../../routes/constants'
class UserProfileFollowing extends React.Component {
  constructor() {
    super()
    this.state = {
      loading: false,
      followingsCount: null,
      followings: [],
      count: 10,
      start: 0,
    }
  }

  componentDidMount = () => {
    try {
      this.props.fetchProfileOwner(this.props.match.params.id)
      this.props.fetchFollowings(
        this.props.match.params.id,
        this.state.start,
        this.state.count,
      )
    } catch (err) {
      console.log(err)
      errorAlert('Something went wrong.')
    }
  }

  componentDidUpdate = (prevProps) => {
    if (prevProps.followingsCount !== this.props.followingsCount) {
      if (this.props.followingsCount) {
        this.setState({
          followingsCount: this.props.followingsCount,
        })
      }
    }
    if (
      JSON.stringify(prevProps.followings) !==
      JSON.stringify(this.props.followings)
    ) {
      if (this.props.followings.length > 0) {
        this.setState({
          followings: this.state.followings.concat(this.props.followings),
        })
      }
    }
  }

  // handler for infinite scroll ...

  fetchMoreFollowings = () => {
    this.setState({ start: this.state.start + this.state.count }, () => {
      console.log('skip:', this.state.start)
      this.props.fetchFollowings(
        this.props.match.params.id,
        this.state.start,
        this.state.count,
      )
    })
  }

  onSubmit = async (e) => {
    try {
      await this.props.fetchPosts()
      this.setState({
        loading: true,
      })
    } catch (e) {
      console.log('catch', e)
    } finally {
      console.log('finally')
    }
    return true
  }

  // handler for go to product details page...
  viewProduct = (slug, productId) => {
    this.props.history.push(
      ROUTE_PRODUCT_DETAILS.replace(':slug', slug).replace(
        ':productId',
        productId,
      ),
    )
  }

  render() {
    return (
      <div>
        <Header />
        <CoverPhoto src={Img} status="user" />
        <div className="user-profile-cnt pt-0 pt-md-3 mt-0 mt-md-4">
          <UserInfo isOwn={false} user={this.props.profileOwner} />
          <Tab />
          <Following
            data={this.state.followings}
            fetchMoreFollowings={this.fetchMoreFollowings}
            followingsCount={this.props.followingsCount}
            viewProduct={this.viewProduct}
          />
        </div>
        <Footer />
        <div className="ab-mf px-4 mb-4 d-block d-md-none">
          <FixedFooter />
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    followings: selectFollowings(state),
    followingsCount: selectFollowingsCount(state),
    profileOwner: getProfileOwnerData(state),
  }),
  {
    fetchProfileOwner,
    fetchFollowings,
  },
)(UserProfileFollowing)
