import React from "react";
import { connect } from "react-redux";
import { selectPosts } from "core/selectors/posts";
import { fetchPosts } from "core/actions/postActionCreators";
import { fetchProfileOwner } from "core/actions/publicProfileActionCreator";
import Header from "./../../home/Header/Header";
import Footer from "./../../home/Footer/Footer";

// user profile
import UserInfo from "./../../profile/common/userInfo/userInfo";
import Tab from "./../../profile/common/tab/tab";
import Review from "./../../profile/common/review/review";
import "./../../profile/index.scss";
import CoverPhoto from "../../profile/common/coverPhoto";
import Img from "./../../../images/eco-img.webp";
import { getProfileOwnerData } from "core/selectors/profile";
import FixedFooter from "../../Common/fixedFooter";
class UserProfileReview extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: false,
    };
  }

  componentDidMount = () => {
    this.props.fetchPosts();
    this.props.fetchProfileOwner(this.props.match.params.id);
  };

  onSubmit = async (e) => {
    try {
      this.setState({
        loading: true,
      });
    } catch (e) {
      console.log("catch", e);
    } finally {
      console.log("finally");
    }
    return true;
  };

  render() {
    console.log("lkd: ", this.props.user);
    return (
      <div>
        <Header />
        <CoverPhoto src={Img} status="user" />
        <div className="user-profile-cnt pt-0 pt-md-3 mt-0 mt-md-4">
          <UserInfo isOwn={false} user={this.props.profileOwner} />
          <Tab />
          <Review />
        </div>
        <Footer />
        <div className="ab-mf px-4 mb-4 d-block d-md-none">
          <FixedFooter />
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    posts: selectPosts(state),
    profileOwner: getProfileOwnerData(state),
  }),
  {
    fetchPosts,
    fetchProfileOwner,
  }
)(UserProfileReview);
