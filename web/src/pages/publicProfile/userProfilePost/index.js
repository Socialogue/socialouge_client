import React from 'react'
import { connect } from 'react-redux'
import {
  getProfileOwnerData,
  getProfileOwnerPosts,
} from 'core/selectors/profile'
import {
  fetchProfileOwner,
  fetchProfilePosts,
} from 'core/actions/publicProfileActionCreator'
import { addComment } from 'core/actions/commentActionCreators'
import Header from './../../home/Header/Header'

// user profile
import UserInfo from './../../profile/common/userInfo/userInfo'
import Tab from './../../profile/common/tab/tab'
import Post from './../../profile/common/post/post'
import './../../profile/index.scss'
import { errorAlert } from '../../../utils/alerts'
import CoverPhoto from '../../profile/common/coverPhoto'
import Img from './../../../images/eco-img.webp'
import SortView from '../../home/sortView'
import FixedFooter from '../../Common/fixedFooter'
import {
  ROUTE_USER_OWN_PROFILE_POST,
  ROUTE_USER_PROFILE_POST,
} from '../../../routes/constants'
import { selectUser } from 'core/selectors/user'
import PostModal from '../../modal/postModal'

class UserProfilePost extends React.Component {
  constructor() {
    super()
    this.state = {
      loading: false,
      allPosts: [],
      count: 10,
      start: 0,
      setData: false,
      postModalOpen: false,
      post: {},
    }
  }

  componentDidMount = () => {
    try {
      this.props.fetchProfileOwner(this.props.match.params.id)
      this.props.fetchProfilePosts(
        this.props.match.params.id,
        this.state.count,
        this.state.start,
      )
    } catch (err) {
      console.log('error: ', err)
      errorAlert('Something went wrong.')
    }
  }

  componentDidUpdate = (prevProps) => {
    if (
      JSON.stringify(prevProps.profilePosts) !==
        JSON.stringify(this.props.profilePosts) ||
      !this.state.setData
    ) {
      if (this.props.profilePosts) {
        this.setState({
          allPosts: this.state.allPosts.concat(this.props.profilePosts),
          setData: true,
        })
      }
    }
  }

  fetchMoreData = () => {
    this.setState({ start: this.state.start + this.state.count }, () => {
      this.props.fetchProfilePosts(
        this.props.match.params.id,
        this.state.count,
        this.state.start,
      )
    })
  }

  // handler for goto post details as popup...
  openPostDetailsPopup = (postId) => {
    //TODO have some issue, need test and fix
    // let posts = [...this.state.allPosts]
    // const post = posts.filter((item) => item._id === postId)
    // document.body.classList.add('modal-open')
    // this.setState({ postModalOpen: true, post: post[0] })
  }

  // handler for closing post details popup...
  closePostDetailsPopup = () => {
    document.body.classList.remove('modal-open')
    this.setState({ postModalOpen: false, post: {} })
  }

  // handler for adding a comment to post... passing as props
  addingComment = (commentText, postId) => {
    const userId = this.props.user._id
    // if not have userId then show some notification ...
    if (!userId) {
      return errorAlert('Login first please!')
    }
    this.props.addComment(commentText, postId, userId)
  }

  // handler for goto user profile...
  gotoProfile = (userId) => {
    if (userId == this.props.user._id) {
      this.props.history.push(ROUTE_USER_OWN_PROFILE_POST)
    } else {
      const route = ROUTE_USER_PROFILE_POST.replace(':id', userId)
      this.props.history.push(route)
    }
  }

  render() {
    const { postModalOpen, post } = this.state
    return (
      <div>
        <Header />
        <CoverPhoto
          src={Img}
          status="user"
          coverImage={this.props.profileOwner}
        />
        <div className="user-profile-cnt pt-0 pt-md-3 mt-0 mt-md-4">
          <UserInfo isOwn={false} user={this.props.profileOwner} />
          <Tab url={this.props.location.pathname} match={this.props.match} />
          <Post
            data={this.state.allPosts}
            fetchMoreData={this.fetchMoreData}
            isOwn={false}
            openPostDetails={this.openPostDetailsPopup}
          />
          <SortView />
          {postModalOpen === true && (
            <PostModal
              post={post}
              close={this.closePostDetailsPopup}
              gotoProfile={this.gotoProfile}
              addingComment={this.addingComment}
            />
          )}
        </div>
        <FixedFooter />
      </div>
    )
  }
}

export default connect(
  (state) => ({
    profileOwner: getProfileOwnerData(state),
    profilePosts: getProfileOwnerPosts(state),
    user: selectUser(state),
  }),
  {
    fetchProfileOwner,
    fetchProfilePosts,
    addComment,
  },
)(UserProfilePost)
