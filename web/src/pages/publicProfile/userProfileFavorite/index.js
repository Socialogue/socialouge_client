import React from 'react'
import { connect } from 'react-redux'
import {
  fetchPublicFavoriteProducts,
  reactToProduct,
} from 'core/actions/productActionCreators'
import { fetchProfileOwner } from 'core/actions/publicProfileActionCreator'
import {
  selectPublicFavoriteProducts,
  selectTotalFavorites,
} from 'core/selectors/products'
import { getProfileOwnerData } from 'core/selectors/profile'
import { errorAlert } from '../../../utils/alerts'
import Header from './../../home/Header/Header'
import Footer from './../../home/Footer/Footer'
import Favorite from './../../profile/common/favorite/favorite'
import Tab from './../../profile/common/tab/tab'
// user profile
import UserInfo from './../../profile/common/userInfo/userInfo'
import './../../profile/index.scss'
import CoverPhoto from '../../profile/common/coverPhoto'
import Img from './../../../images/eco-img.webp'
import FixedFooter from '../../Common/fixedFooter'
import { selectLoadedStatus } from 'core/selectors/posts'
import {
  ROUTE_PRODUCT_DETAILS,
  ROUTE_USER_OWN_PROFILE_POST,
  ROUTE_USER_PROFILE_POST,
} from '../../../routes/constants'
import { selectUser } from 'core/selectors/user'

class UserProfileFavorite extends React.Component {
  constructor() {
    super()
    this.state = {
      loading: false,
      publicFavoriteProducts: [],
      count: 10,
      start: 0,
      total: null,
      setData: false,
    }
  }

  componentDidMount = () => {
    try {
      this.props.fetchProfileOwner(this.props.match.params.id)
      this.props.fetchPublicFavoriteProducts(
        this.props.match.params.id,
        this.state.count,
        this.state.start,
      )
    } catch (err) {
      console.log(err)
      errorAlert('Something went wrong.')
    }
  }

  componentDidUpdate = (prevProps) => {
    if (
      JSON.stringify(prevProps.publicFavoriteProducts) !==
        JSON.stringify(this.props.publicFavoriteProducts) ||
      !this.state.setData
    ) {
      if (this.props.publicFavoriteProducts) {
        this.setState({
          publicFavoriteProducts: this.state.publicFavoriteProducts.concat(
            this.props.publicFavoriteProducts,
          ),
          total: this.props.favoriteCount,
          setData: true,
        })
      }
    }
  }

  fetchMoreData = () => {
    this.setState({ start: this.state.start + this.state.count }, () => {
      this.props.fetchPublicFavoriteProducts(
        this.props.match.params.id,
        this.state.count,
        this.state.start,
      )
    })
  }

  reactToProduct = (productId) => {
    let favProducts = [...this.state.publicFavoriteProducts]
    let filteredProducts = favProducts.filter(
      (product) => product.productId._id !== productId,
    )
    this.setState({ publicFavoriteProducts: filteredProducts }, () => {})
    this.props.reactToProduct(this.props.match.params.id, productId)
  }

  // handler for goto user profile...
  gotoProfile = (userId) => {
    if (userId == this.props.user._id) {
      this.props.history.push(ROUTE_USER_OWN_PROFILE_POST)
    } else {
      const route = ROUTE_USER_PROFILE_POST.replace(':id', userId)
      this.props.history.push(route)
    }
  }

  // open product in details page...
  viewProduct = (slug, productId) => {
    this.props.history.push(
      ROUTE_PRODUCT_DETAILS.replace(':slug', slug).replace(
        ':productId',
        productId,
      ),
    )
  }

  render() {
    return (
      <div>
        <Header />
        <CoverPhoto src={Img} status="user" />
        <div className="user-profile-cnt  pt-0 pt-md-3 mt-0 mt-md-4">
          <UserInfo isOwn={false} user={this.props.profileOwner} />
          <Tab />
          <Favorite
            data={this.state.publicFavoriteProducts}
            fetchMoreData={this.fetchMoreData}
            favorite={this.reactToProduct}
            total={this.state.total}
            isOwn={true}
            loadedStatus={this.props.loadedStatus}
            gotoProfile={this.gotoProfile}
            viewProduct={this.viewProduct}
          />
        </div>
        <Footer />
        <div className="ab-mf px-4 mb-4 d-block d-md-none">
          <FixedFooter />
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    profileOwner: getProfileOwnerData(state),
    publicFavoriteProducts: selectPublicFavoriteProducts(state),
    favoriteCount: selectTotalFavorites(state),
    loadedStatus: selectLoadedStatus(state),
    user: selectUser(state),
  }),
  {
    fetchProfileOwner,
    fetchPublicFavoriteProducts,
    reactToProduct,
  },
)(UserProfileFavorite)
