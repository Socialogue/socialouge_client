import React from 'react'
import { connect } from 'react-redux'
import {
  selectLoginErrorMsg,
  selectNetworkError,
  selectOtpSendMsg,
  selectOtpVerifyStatus,
  selectToken,
  selectUser,
} from 'core/selectors/user'
import { login } from 'core/actions/authActionCreators'
import { Link } from 'react-router-dom'

import SmLogo from '../../images/sm-logo.svg'
import Back from '../../images/icons/back.svg'
import LoginImgLg from '../../images/login-img.webp'
import LoginImgSm1 from '../../images/log-sm.webp'
import LoginImgSm2 from '../../images/login-img2.webp'
import {
  ROUTE_FORGOT_PASSWORD,
  ROUTE_HOME,
  ROUTE_VERIFY_OTP,
} from '../../routes/constants'
import SocialLogin from '../../components/socialLogin/socialLogin'
import PrimaryButton from '../../components/PrimaryButton'
import SingleLineInput from '../../components/SingleLineInput'
import { errorAlert, successAlert } from '../../utils/alerts'
import { ACCOUNTYPE_EMAIL } from 'core/constants/terms'
import LogoPre from '../../images/icons/logoPre'
import socketIOClient from 'socket.io-client'
const ENDPOINT = 'http://localhost:5000'

class Login extends React.Component {
  constructor() {
    super()
    this.state = {
      loading: false,
      username: '',
      password: '',
      accountType: ACCOUNTYPE_EMAIL,
    }
  }

  componentDidMount = () => {
    const socket = socketIOClient(ENDPOINT, {
      transports: ['websocket', 'polling', 'flashsocket'],
    })

    socket.on('hello', (arg) => {
      console.log(arg) // world
    })
  }

  componentDidUpdate(prevProps) {
    // if (this.props.networkError !== prevProps.networkError) {
    //   if (this.props.networkError !== "") {
    //     errorAlert(this.props.networkError);
    //   }
    // }
    if (this.props.isLoggedIn !== prevProps.isLoggedIn) {
      if (this.props.isLoggedIn) {
        successAlert('Logged in successfully!')
        this.props.history.push(ROUTE_HOME)
      }
    }
    if (this.props.otpVerifyStatus !== prevProps.otpVerifyStatus) {
      if (this.props.otpVerifyStatus) {
        successAlert(this.props.otpSendMsg)
        this.props.history.push(ROUTE_VERIFY_OTP)
      }
    }
    if (this.props.loginErrorMsg !== prevProps.loginErrorMsg) {
      if (this.props.loginErrorMsg !== '') {
        errorAlert(this.props.loginErrorMsg)
      }
    }
  }

  inputHandler = (name, value) => {
    this.setState({
      [name]: value,
    })
  }

  onSubmit = async (e) => {
    if (this.state.username === '' || this.state.password === '') {
      errorAlert('You should provide some credential!')
      return
    }

    this.setState({
      loading: true,
    })
    try {
      await this.props.login(
        this.state.username,
        this.state.password,
        this.state.accountType,
      )
    } catch (e) {
      errorAlert('Something went wrong!')
    }
    this.setState({
      loading: false,
    })
    return true
  }

  render() {
    return (
      <div className="form-module-cnt">
        <div className="row m-0 wh-100">
          <div className="col-12 col-md-7 p-0 h-100">
            <div className="form-info-cnt d-flex-box wh-100">
              <div className="back-link">
                <Link to={ROUTE_HOME}>
                  <img className="" src={Back} alt="img" />
                  Back to Main
                </Link>
              </div>
              <div className="form-info">
                <div className="form-logo d-none d-md-block">
                  {' '}
                  <img className="" src={SmLogo} alt="img" />
                </div>
                <div className="lg-title">
                  <span className="d-inline-block d-md-none">
                    <LogoPre />
                  </span>{' '}
                  login
                  <span className="dot-r" />
                </div>
                <div className="form">
                  <SingleLineInput
                    label="Email / phone number"
                    name="username"
                    placeholder="Your Email or phone number"
                    required={true}
                    validations={['email-phone']}
                    onChange={this.inputHandler}
                    type="text"
                  />
                  <SingleLineInput
                    label="Your Password"
                    name="password"
                    placeholder="Your Password"
                    required={true}
                    onChange={this.inputHandler}
                    type="password"
                  />
                  <div className="form-group">
                    <label className="ctm-container">
                      Remember me
                      <input type="checkbox" />
                      <span className="checkmark" />
                    </label>
                    <Link
                      to={ROUTE_FORGOT_PASSWORD}
                      className="x-sm-text-fw6 float-right color-black"
                    >
                      Forgot password?
                    </Link>
                  </div>
                </div>
                <div className="form-group">
                  <PrimaryButton
                    loading={this.state.loading}
                    onClick={this.onSubmit}
                    label="Login"
                    className="btn black-bg-btn text-uppercase w-100"
                  />
                </div>
                <div className="form-group text-center">
                  <span className="md-text-fw6">Don’t have an account?</span>
                  <Link to="/register" className="md-text-fw6">
                    {' '}
                    Register
                  </Link>
                </div>
                {/* <div className="line-txt-c d-flex-box">
                  <div className="line-l" />
                  <div className="line-txt sm-text-fw4-g">or</div>
                  <div className="line-l" />
                </div> */}
                {/*<SocialLogin history={this.props.history} />*/}
              </div>
            </div>
          </div>
          <div className="col-12 col-md-5 p-0 h-100 d-none d-md-block">
            <div className="form-img-cnt wh-100">
              <img className="cover" src={LoginImgLg} alt="img" />
              <div className="form-img-1">
                <img className="cover" src={LoginImgSm1} alt="img" />
              </div>
              <div className="form-img-2">
                <img className="cover" src={LoginImgSm2} alt="img" />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    user: selectUser(state),
    isLoggedIn: !!selectToken(state),
    auth: state.auth,
    networkError: selectNetworkError(state),
    otpSendMsg: selectOtpSendMsg(state),
    otpVerifyStatus: selectOtpVerifyStatus(state),
    loginErrorMsg: selectLoginErrorMsg(state),
  }),
  {
    login,
  },
)(Login)
