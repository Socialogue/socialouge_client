import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SmLogo from '../../images/sm-logo.svg'
import Back from '../../images/icons/back.svg'
import LoginImgLg from '../../images/reset-img.webp'
import { ROUTE_HOME } from '../../routes/constants'
class SuccessLogin extends Component {
  render() {
    return (
      <div className="form-module-cnt">
        <div className="row m-0 wh-100">
          <div className="col-12 col-md-7 p-0 h-100">
            <div className="form-info-cnt d-flex-box wh-100">
              <div className="back-link">
                <Link to={ROUTE_HOME}>
                  <img className="" src={Back} alt="img" />
                  Back to Main
                </Link>
              </div>
              <div className="form-info">
                <div className="form-logo">
                  {' '}
                  <img className="" src={SmLogo} alt="img" />
                </div>
                <div className="lg-title">
                  success
                  <span className="dot-r" />
                </div>
                <div className="form-group">
                  <span className="x-sm-text-fw4-g">
                    Your new password has been successfully saved.
                  </span>
                </div>
                <div className="form-group">
                  <button className="btn black-bg-btn text-uppercase w-100">
                    Login
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-5 p-0  h-100 d-none d-md-block">
            <div className="form-img-cnt wh-100">
              <img className="cover" src={LoginImgLg} alt="img" />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default SuccessLogin
