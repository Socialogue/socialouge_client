import {
  authenticateByFacebook,
  authenticateByGoogle,
  register,
} from "core/actions/authActionCreators";
import {
  selectLoginStatus,
  selectRegisterErrorMessage,
  selectRegisterProccessing,
  selectRegistrationStatus,
  selectToken,
} from "core/selectors/user";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import PrimaryButton from "../../components/PrimaryButton";
import SingleLineInput from "../../components/SingleLineInput";
import SocialLogin from "../../components/socialLogin/socialLogin";
import { ROUTE_HOME, ROUTE_LOGIN } from "../../routes/constants";
import { errorAlert, successAlert } from "../../utils/alerts";
import Helmet from "../Common/Helmet";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import CustomSelectOption from "../userSettings/components/horizontalSelectOption/customSelectOption";
import Back from "./../../images/icons/back.svg";
import RegiterImgLg from "./../../images/lr.webp";
import SmLogo from "./../../images/sm-logo.svg";
import LoginImgSm1 from "../../images/log-sm.webp";
import LoginImgSm2 from "../../images/login-img2.webp";
import parsePhoneNumber from "libphonenumber-js";
import LogoPre from "../../images/icons/logoPre";

class RegisterPage extends Component {
  constructor() {
    super();
    this.state = {
      activeTab: "email",
      loading: false,
      username: "",
      accountType: 1,
      email: "",
      phone: "",
      password: "",
      privacy: false,
      error: "",
      options: ["+1", "+44", "+370", "+39"],
      countryCode: "+44",
      errorText: "",
      touched: false,
    };
  }

  componentDidUpdate(prevProps) {
    // console.log("props: ", this.props.registerStatus);
    // console.log("PrevProps: ", prevProps);
    // console.log("error msg: ", this.props.registerErrorMsg);
    // if (this.props.registerStatus !== prevProps.registerStatus) {
    //   this.props.history.push(ROUTE_VERIFY_OTP);
    // }

    if (prevProps.registerErrorMsg !== this.props.registerErrorMsg) {
      if (this.props.registerErrorMsg != "") {
        errorAlert(this.props.registerErrorMsg);
      }
    }
    if (prevProps.registerStatus !== this.props.registerStatus) {
      if (this.props.registerStatus) {
        successAlert("Otp is sent");
        this.props.history.push("/verify-otp");
      }
    }
  }

  onInputChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    this.setState({
      [name]: !this.state.privacy,
    });
  };

  inputHandler = (name, value) => {
    this.setState({
      [name]: value,
    });
  };

  onSubmit = async (e) => {
    if (!this.state.privacy) {
      errorAlert("Please accept privacy policy");
      return;
    }

    if (this.state.activeTab === "email") {
      if (
        this.state.username == "" ||
        this.state.email == "" ||
        this.state.password == "" ||
        this.state.password.length < 5
      ) {
        errorAlert("Please fill up the email");
        return;
      }
    }

    if (this.state.activeTab === "phone") {
      if (
        this.state.username == "" ||
        this.state.phone == "" ||
        this.state.password == "" ||
        this.state.password.length < 5
      ) {
        errorAlert("Please fill up the form phone");
        return;
      }
    }

    this.setState({ loading: true });

    try {
      if (this.state.activeTab === "email") {
        this.props.register(
          this.state.username,
          this.state.email,
          this.state.password,
          1
        );
        // TODO "NEED TO EXPORT CONST FOR ACCOUTN TYPE"
      }
      if (this.state.activeTab === "phone") {
        this.props.register(
          this.state.username,
          this.state.phone,
          this.state.password,
          2
        );
      }
    } catch (err) {
      console.log("register form submit error: ", err);
    }
    this.setState({ loading: false });
    return true;
  };

  handleCountryCode = (name, value, index) => {
    this.setState({
      countryCode: value,
    });
    console.log("country; code: ", name, value);
    // const countryCode = get();
  };

  handleTab = (value) => {
    this.setState({
      activeTab: value,
    });
  };

  handlePhoneNumber = (e) => {
    const phoneNumber = this.state.countryCode + e.target.value;
    // console.log("test phone: ", phoneNumber);
    console.log("phone: ", parsePhoneNumber(phoneNumber));
    if (!parsePhoneNumber(phoneNumber)) {
      this.setState({
        errorText: "Please enter a valid phone number",
      });
    }
    if (parsePhoneNumber(phoneNumber)) {
      if (parsePhoneNumber(phoneNumber).number.length != 13) {
        this.setState({
          errorText: "Please enter a valid phone number",
        });
        return;
      }

      this.setState({
        errorText: "",
        phone: parsePhoneNumber(phoneNumber).number,
      });
    }
  };

  render() {
    return (
      <div className="form-module-cnt">
        <Helmet title="Register" />
        <div className="row m-0 wh-100">
          <div className="col-12 col-md-7 p-0 h-100">
            <div className="form-info-cnt d-flex-box wh-100">
              <div className="back-link">
                <Link to={ROUTE_HOME}>
                  <img className="" src={Back} alt="img" />
                  Back to Main
                </Link>
              </div>
              <div className="form-info">
                <div className="form-logo d-none d-md-block">
                  {" "}
                  <img className="" src={SmLogo} alt="img" />
                </div>
                <div className="lg-title">
                  <span className="d-inline-block d-md-none">
                    <LogoPre />
                  </span>{" "}
                  register
                  <span className="dot-r" />
                </div>
                <ul className="nav nav-fill tab-nav">
                  <li
                    className="nav-item"
                    onClick={() => this.handleTab("email")}
                  >
                    <span
                      className={
                        this.state.activeTab === "email"
                          ? "nav-link cursor-pointer sm-text-fw6-g text-uppercase active"
                          : "nav-link cursor-pointer sm-text-fw6-g text-uppercase"
                      }
                    >
                      <span className="d-none d-md-inline-block">With</span>{" "}
                      email address
                    </span>
                  </li>
                  <li
                    className="nav-item"
                    onClick={() => this.handleTab("phone")}
                  >
                    <span
                      className={
                        this.state.activeTab === "phone"
                          ? "nav-link cursor-pointer sm-text-fw6-g text-uppercase active"
                          : "nav-link cursor-pointer sm-text-fw6-g text-uppercase"
                      }
                    >
                      <span className="d-none d-md-inline-block">With</span>{" "}
                      Phone Number
                    </span>
                  </li>
                </ul>
                <div className="form">
                  <SingleLineInput
                    label="User Name"
                    name="username"
                    placeholder="Your username"
                    required={true}
                    validations={[]}
                    onChange={this.inputHandler}
                    type="text"
                  />
                  <div
                    className={
                      this.state.activeTab === "email" ? " " : "d-none"
                    }
                  >
                    <SingleLineInput
                      label="Email Address"
                      name="email"
                      placeholder="Your Email address"
                      required={true}
                      validations={["email"]}
                      onChange={this.inputHandler}
                      type="text"
                    />
                  </div>
                  <div
                    className={
                      this.state.activeTab === "phone"
                        ? "form-group "
                        : "form-group d-none"
                    }
                  >
                    <label className="x-sm-text-fw6 text-uppercase">
                      phone number
                    </label>
                    <div className="input-group mb-3 sc-input apply-sc">
                      <PhoneInput
                        country={"us"}
                        inputStyle={{
                          border: "1px solid black",
                          borderTop: "none",
                          borderRight: "none",
                          width: "420px",
                          marginLeft: "5px",
                          borderRadius: "0",
                        }}
                        className="form-control custom-input"
                        buttonStyle={{
                          border: "none",
                          backgroundColor: "white",
                          borderRight: "1px solid black",
                          borderBottom: "1px solid black",
                          borderRadius: "0",
                        }}
                        value={this.state.phone}
                        placeholder="Enter your phone number"
                        required={true}
                        onChange={(phone) => this.setState({ phone })}
                      />
                    </div>
                    {this.state.errorText !== "" && this.state.touched && (
                      <span className="color-orange">
                        {this.state.errorText}
                      </span>
                    )}
                  </div>
                  <SingleLineInput
                    label="Your Password"
                    name="password"
                    placeholder="Your Password"
                    required={true}
                    validations={["password"]}
                    onChange={this.inputHandler}
                    type="password"
                  />

                  <div className="form-group">
                    <label className="ctm-container">
                      Agree with Socialogue Privacy Policy
                      <input
                        type="checkbox"
                        name="privacy"
                        onChange={this.onInputChange}
                      />
                      <span className="checkmark" />
                    </label>
                  </div>
                </div>
                <div className="form-group">
                  <PrimaryButton
                    loading={this.state.loading}
                    onClick={this.onSubmit}
                    label="Register"
                    className="btn black-bg-btn text-uppercase w-100"
                  />
                </div>

                <div className="form-group text-center">
                  <span className="md-text-fw6">Already have an account?</span>
                  <Link to={ROUTE_LOGIN} className="md-text-fw6">
                    {" "}
                    Login
                  </Link>
                </div>
                <div className="line-txt-c d-flex-box">
                  <div className="line-l" />
                  <div className="line-txt sm-text-fw4-g">or</div>
                  <div className="line-l" />
                </div>
                <SocialLogin history={this.props.history} />
                {/* // TODO need to chagne personal email with company's email */}
              </div>
            </div>
          </div>
          <div className="col-12 col-md-5 p-0 h-100 d-none d-md-block">
            <div className="form-img-cnt wh-100">
              <img className="cover" src={RegiterImgLg} alt="img" />
              <div className="form-img-1">
                <img className="cover" src={LoginImgSm1} alt="img" />
              </div>
              <div className="form-img-2">
                <img className="cover" src={LoginImgSm2} alt="img" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    isLoggedIn: !!selectToken(state),
    loading: selectRegisterProccessing(state),
    registerStatus: selectRegistrationStatus(state),
    registerErrorMsg: selectRegisterErrorMessage(state),
    loginStatus: selectLoginStatus(state),
  }),
  {
    register,
    authenticateByFacebook,
    authenticateByGoogle,
  }
)(RegisterPage);
