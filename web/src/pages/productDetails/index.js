import React, { Component } from "react";
import { Link } from "react-router-dom";
import Header from "../home/Header/Header";
import Img from "./../../images/eco-img.webp";
import Description from "./components/description";
import "./product-details.scss";
import Shipping from "./components/shipping";
import CompleteLook from "./components/completeLook";
import MatchStyle from "./components/matchStyle";
import Posts from "./components/posts";
import ProductInfo from "./components/productInfo";
import FixedFooter from "../Common/fixedFooter";
import SingleSlider from "./components/singleSlider";
import SliderNav from "./components/sliderNav";
import { connect } from "react-redux";
import {
  getProductDetails,
  addToCart
} from "core/actions/productActionCreators";
import { addToCartDb } from "core/actions/cartActionCreators";
import { selectProduct } from "core/selectors/products";
import { selectCartData, selectCartPrice } from "core/selectors/cart";
import { selectUser } from "core/selectors/user";
import { successAlert } from "../../utils/alerts";
import ScrollTopZero from "../Common/ScrollTopZero";
import {
  ROUTE_USER_OWN_PROFILE_POST,
  ROUTE_USER_PROFILE_POST
} from "../../routes/constants";
class ProductDetails extends Component {
  state = {
    variation: null,
    mainImage: "",
    size: ""
  };

  scrollToNode(val, e) {
    var tbHt = 0,
      tbHt1 = 0,
      tbHt2 = 0,
      tbHt3 = 0,
      tbHt4 = 0;
    const tb = document.querySelectorAll(".tb");
    tb.forEach(function(userItem, i) {
      if (i < 1) tbHt += userItem.offsetHeight + 48;
    });
    tb.forEach(function(userItem, i) {
      if (i < 2) tbHt1 += userItem.offsetHeight + 48;
    });
    tb.forEach(function(userItem, i) {
      if (i < 3) tbHt2 += userItem.offsetHeight + 48;
    });
    tb.forEach(function(userItem, i) {
      if (i < 4) tbHt3 += userItem.offsetHeight + 48;
    });
    tb.forEach(function(userItem, i) {
      if (i < 5) tbHt4 += userItem.offsetHeight + 48;
    });
    
    if (val ==1) {
      document
        .querySelector(".sticky-srl")
        .scroll({ top: 0, behavior: 'smooth' });
      
    } else if (val == 2) {
      document
      .querySelector(".sticky-srl")
      .scroll({ top: tbHt + 18, behavior: 'smooth' });
    }else if (val == 3) {
      document
      .querySelector(".sticky-srl")
      .scroll({ top: tbHt1 + 18, behavior: 'smooth' });
    }else if (val == 4) {
      document
      .querySelector(".sticky-srl")
      .scroll({ top: tbHt2 + 18, behavior: 'smooth' });
    } else {
      document
      .querySelector(".sticky-srl")
      .scroll({ top: tbHt3 + 18, behavior: 'smooth' });
    }
    const tbl = e.currentTarget
      .closest(".sticky-d-nav")
      .querySelectorAll(".nav-link");
    tbl.forEach(function(userItem, i) {
      userItem.classList.remove("active");
    });
    e.currentTarget.classList.add("active");
  }
  componentDidMount = () => {
    const productId = this.props.match.params.productId;
    this.props.getProductDetails(productId);
    document
      .querySelector(".sticky-srl")
      .addEventListener("scroll", this.handleScroll, true);
  };
  
  componentWillUnmount() {
     document
    .querySelector(".sticky-srl")
    .removeEventListener("scroll", this.handleScroll);
  }
  handleScroll = () => {
    var scTop = document.querySelector(".sticky-srl").scrollTop;
    var tbHt = 0,
      tbHt1 = 0,
      tbHt2 = 0,
      tbHt3 = 0,
      tbHt4 = 0;
    const tb = document.querySelectorAll(".tb");
    tb.forEach(function(userItem, i) {
      if (i < 1) tbHt += userItem.offsetHeight + 48;
    });
    tb.forEach(function(userItem, i) {
      if (i < 2) tbHt1 += userItem.offsetHeight + 48;
    });
    tb.forEach(function(userItem, i) {
      if (i < 3) tbHt2 += userItem.offsetHeight + 48;
    });
    tb.forEach(function(userItem, i) {
      if (i < 4) tbHt3 += userItem.offsetHeight + 48;
    });
    tb.forEach(function(userItem, i) {
      if (i < 5) tbHt4 += userItem.offsetHeight + 48;
    });
    const tbl = document
      .querySelector(".sticky-d-nav")
      .querySelectorAll(".nav-link");
    tbl.forEach(function(userItem, i) {
      userItem.classList.remove("active");
    });
    
    if (scTop >= 0 && scTop <= tbHt) {
      document
        .querySelector(".sticky-d-nav")
        .children[0].querySelector(".nav-link")
        .classList.add("active");
    } else if (scTop >= tbHt && scTop <= tbHt1) {
      document
        .querySelector(".sticky-d-nav")
        .children[1].querySelector(".nav-link")
        .classList.add("active");
    } else if (scTop >= tbHt1 && scTop <= tbHt2) {
      document
        .querySelector(".sticky-d-nav")
        .children[2].querySelector(".nav-link")
        .classList.add("active");
    } else if (scTop >= tbHt2 && scTop <= tbHt3) {
      document
        .querySelector(".sticky-d-nav")
        .children[3].querySelector(".nav-link")
        .classList.add("active");
    } else {
      document
        .querySelector(".sticky-d-nav")
        .children[4].querySelector(".nav-link")
        .classList.add("active");
    }
  };
  componentDidUpdate = prevProps => {
    if (prevProps.product !== this.props.product) {
      this.setState({
        mainImage: this.props.product.variations[0].colorImage,
        variation: this.props.product.variations[0],
        size: this.props.product.variations[0].sizes.lt
      });
    }
    if (
      JSON.stringify(prevProps.cartData) !== JSON.stringify(this.props.cartData)
    ) {
      if (this.props.cartData) {
        successAlert("Added to cart!");
        if (this.props.user._id) {
          this.props.addToCartDb(
            this.props.cartData,
            this.props.cartPrice,
            this.props.user._id
          );
        }
      }
    }
  };

  changeColor = variation => {
    this.setState({
      variation: variation,
      mainImage: variation.colorImage,
      size: variation.sizes.lt
    });
  };

  addToCart = p => {
    // return console.log("p: ", p);
    const shopId = p.shopId._id;
    const shopOwnerId = p.userId._id;
    const clientId = this.props.user._id || "";
    const productId = p._id;
    const price = this.state.variation.price;
    const color = this.state.variation;
    const size = this.state.size;
    const quantity = 1;
    const vat = 20; //percent...
    const totalPrice = +quantity * +price; // quantity*price...
    const vatAmmount = (+totalPrice * +vat) / 100; // (totalPrice*vat)/100
    const grandTotal = +totalPrice + +vatAmmount; // totalPrice+vatAmmount;
    const product = p;
    let finalPrice = this.props.cartPrice;
    finalPrice = +finalPrice + +grandTotal;
    this.props.addToCart(
      {
        product,
        shopId,
        shopOwnerId,
        clientId,
        productId,
        price,
        color,
        size,
        quantity,
        vat,
        totalPrice,
        vatAmmount,
        grandTotal
      },
      finalPrice
    );
  };

  changeCentreImage = image => {
    this.setState({ mainImage: image });
  };

  selectSize = size => {
    this.setState({
      size: size
    });
  };

  // handler for goto user profile...
  gotoProfile = userId => {
    if (userId == this.props.user._id) {
      this.props.history.push(ROUTE_USER_OWN_PROFILE_POST);
    } else {
      const route = ROUTE_USER_PROFILE_POST.replace(":id", userId);
      this.props.history.push(route);
    }
  };

  render() {
    const { product } = this.props;
    if (!product) {
      return null;
    }
    return (
      <div>
        <Header />
        <div className="product-content max-content-width-1 px-0 px-md-4 mb-0">
          <nav aria-label="breadcrumb" className="mb-3 mb-md-5 px-4 px-md-0">
            <ol className="breadcrumb custom-breadcrumb">
              <li className="breadcrumb-item">
                <Link to="#">Home</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to="#">Man</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to="#">Jeans</Link>
              </li>
              <li className="breadcrumb-item active">Capri pants</li>
            </ol>
          </nav>

          <div className="row mb-3">
            <div className="col-12 col-md-6">
              <SingleSlider
                mainImage={this.state.mainImage}
                variation={this.state.variation}
              />
            </div>
            <div className="col-12 col-md-6">
              <ProductInfo
                product={this.props.product}
                changeColor={this.changeColor}
                variation={this.state.variation}
                addToCart={this.addToCart}
                selectSize={this.selectSize}
                size={this.state.size}
                gotoProfile={this.gotoProfile}
              />
            </div>
          </div>

          <SliderNav
            additionalImages={this.props.product.additionalImage}
            changeCentreImage={this.changeCentreImage}
          />
        </div>

        <div className="sticky-srl">
          <div
            className="product-content max-content-width-1 px-0 px-md-4 mt-3"
            style={{ overflowX: "unset" }}
          >
            <div className="row">
              <div className="col-12 col-md-6">
                <div className="px-4 px-md-0 position-relative">
                  <ul className="nav nav-fill tab-nav sticky-d-nav mb-5 d-none d-md-flex pt-3">
                    <li className="nav-item">
                      <span
                        className="nav-link sm-text-fw6-g text-uppercase cursor-pointer active"
                        onClick={e => this.scrollToNode(1, e)}
                      >
                        DESCRIPTION
                      </span>
                    </li>
                    <li className="nav-item">
                      <span
                        className="nav-link sm-text-fw6-g text-uppercase cursor-pointer"
                        onClick={e => this.scrollToNode(2, e)}
                      >
                        SHIPPING
                      </span>
                    </li>
                    <li className="nav-item">
                      <span
                        className="nav-link sm-text-fw6-g text-uppercase cursor-pointer"
                        onClick={e => this.scrollToNode(3, e)}
                      >
                        COMPLETE LOOK
                      </span>
                    </li>
                    <li className="nav-item">
                      <span
                        className="nav-link sm-text-fw6-g text-uppercase cursor-pointer"
                        onClick={e => this.scrollToNode(4, e)}
                      >
                        MAtCH STYLE
                      </span>
                    </li>
                    <li className="nav-item">
                      <span
                        className="nav-link sm-text-fw6-g text-uppercase cursor-pointer"
                        onClick={e => this.scrollToNode(5, e)}
                      >
                        POSTS
                      </span>
                    </li>
                  </ul>

                  <div className="tb">
                    <Description description={this.props.product.description} />
                  </div>
                  <div className="tb">
                    <Shipping shipping={this.props.product.shippingTo} />
                  </div>
                  <div className="tb">
                    <CompleteLook />
                  </div>
                  <div className="tb">
                    <MatchStyle />
                  </div>
                  <div className="tb">
                    <Posts />
                  </div>
                  <div className="sm-text-fw6-g text-uppercase my-5 d-block d-md-none text-right">
                    Product code: 1946487
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <ScrollTopZero Display="d-md-flex d-none" />
        <div className="ab-mf px-4 mb-4">
          <FixedFooter />
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    user: selectUser(state),
    product: selectProduct(state),
    cartData: selectCartData(state),
    cartPrice: selectCartPrice(state)
  }),
  {
    getProductDetails,
    addToCart,
    addToCartDb
  }
)(ProductDetails);
