import React, { Component } from "react";
import Img from "./../../../../images/eco-img.webp";
class ProductCart extends Component {
  render() {
    return (
      <div className="col-6 col-md-4 pr-0 mb-3 pro-c-img">
        <div className="show-img-pp show-img-pp-pb-140 mb-2">
          <div className="show-img-p">
            <img className="cover" src={Img} />
          </div>
        </div>
        <div className="sm-text-fw6 text-uppercase mb-2">
          limited edition coat
        </div>
        <div className="sm-text-fw4 mb-2">110 €</div>
        <div>
          <button className="btn border-btn btn-w-115">Cart</button>
        </div>
      </div>
    );
  }
}

export default ProductCart;
