import React, { Component } from 'react'
import Img from './../../../../images/eco-img.webp'
import { Link } from 'react-router-dom'
import CustomSelectOption from '../../../userSettings/components/horizontalSelectOption/customSelectOption'
import Cart from '../../../../images/icons/cart'
import Love from '../../../../images/icons/love'
import Comment from '../../../../images/icons/comment'
import Star from '../../../../images/icons/star'
import Share from '../../../../images/icons/share'
import CustomSelect from '../../../../components/SingleSelect'
import HorizontalSelectOption from '../../../userSettings/components/horizontalSelectOption'
import { getImageOriginalUrl } from 'core/utils/helpers'

class ProductInfo extends Component {
  state = {
    colors: [],
  }

  componentDidMount = () => {
    if (this.props.product) {
      // const colors = this.props.product.variations.map((variation) => {
      //   let color = variation.color;
      // });
      // this.setState({ colors: [...this.state.colors], color });
    }
  }
  // handleInputChange = (value, name) => {
  //   const changedVariation = this.props.product.variations.filter(
  //     variation => variation.color == value
  //   );
  //   this.props.changeColor(changedVariation[0]);
  // };
  handleInputChange = (name, value) => {
    const changedVariation = this.props.product.variations.filter(
      (variation) => variation.color.name == value,
    )
    this.props.changeColor(changedVariation[0])
  }
  render() {
    const { shopId } = this.props.product
    if (!shopId) {
      return null
    }

    const colors = []
    this.props.product.variations.map((variation) => {
      let color = variation.color.name
      colors.push(color)
    })

    return (
      <div className="pl-4 pr-4 pr-md-0 pt-4 pt-md-0 pb-5 position-relative h-100">
        <div className="sm-text-fw6-g text-uppercase mb-5 d-none d-md-block">
          Product code: 1946487
        </div>
        <div className="md-title md-title-res25 mb-1 mb-md-3">
          {this.props.product.title}
        </div>
        <div className="pro-cnt align-items-center mb-4 mb-md-5">
          <div className="pro-img d-none d-md-block">
            <img
              className="cover"
              src={
                this.props.product.shopId.profileImage
                  ? getImageOriginalUrl(this.props.product.shopId.profileImage)
                  : Img
              }
            />
          </div>
          <div className="pro-txt-cnt pl-0 pl-md-2">
            by
            <Link
              className="text-uppercase ml-1 font-weight-6 text-decoration-underline"
              to={
                this.props.product.shopId
                  ? `/shop/products/${this.props.product.shopId._id}`
                  : ''
              }
            >
              {this.props.product.shopId.shopName}
            </Link>
            {' | '}
            <span
              className="text-uppercase ml-1 font-weight-6 text-decoration-underline cursor-pointer"
              onClick={() =>
                this.props.gotoProfile(this.props.product.userId._id)
              }
            >
              {this.props.product.userId.fullName}
            </span>
            <button className="btn border-btn ml-3 px-3 d-none d-md-inline-block">
              Contact seller
            </button>
          </div>
        </div>
        <div className="mb-4 discount-price">
          {/* when have discound that time need to add discount-price class */}
          <div className="md-title md-title-res25">
            {' '}
            €{' '}
            {this.props.variation ? this.props.variation.price : this.props.product.variations[0].price}
          </div>
          <div className="">
            <span className="del-price"> € 290.00</span>
          </div>
        </div>
        <div className="form-group single-group color-group mb-3 pb-1">
          {/* <div className="mb-2">
            <span className="sm-text-fw6 text-uppercase">Color list:</span>
          </div> */}
          <HorizontalSelectOption
            formInputClass="form-input position-relative"
            label="Color list:"
            errorText=""
            isRequired={false}
            imageRequired={false}
            selectedText={colors[0]}
            name="color"
            options={colors}
            onChange={this.handleInputChange}
          />
          {/* <CustomSelect
            label="Black"
            name="color"
            id="color"
            options={this.props.product.variations}
            onChange={this.handleInputChange}
          /> */}
        </div>
        <div className="form-group">
          <div className="mb-2">
            <span className="sm-text-fw6 text-uppercase">Size</span>
            <span className="x-sm-text-fw4-g float-right">Size guides</span>
          </div>
          <div className="sm-box-p">
            <span className="sm-box sm-text-fw6 active">
              {this.props.variation
                ? this.props.variation.sizes.lt
                : this.props.product.variations[0].sizes.lt}
            </span>
          </div>
        </div>
        <div className="form-group">
          <button
            className="btn border-btn black-bg-btn w-100 d-flex-box py-3 text-uppercase"
            onClick={() => this.props.addToCart(this.props.product)}
          >
            <Cart />
            <span className="ml-1"> Add to Cart</span>
          </button>
        </div>
        <div className="bt-lsr sm-text-fw6 text-uppercase">
          <span className="">
            <span className="like mr-2 d-inline-flex align-items-center">
              <span className="com-icon">
                <Love />
              </span>
              <span className="like-txt">46 Likes</span>
            </span>
            <span className="review mr-2 com-icon">
              <Star />
            </span>
            <span className="share com-icon">
              <Share />
            </span>
          </span>
        </div>
      </div>
    )
  }
}

export default ProductInfo
