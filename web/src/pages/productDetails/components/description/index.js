import React, { Component } from "react";
import Down from "../../../../images/icons/down";

class Description extends Component {
  state = {
    showDetails: false
  };
  render() {
    const { description } = this.props;
    // if (!description) {
    //   return null;
    // }
    return (
      <div className="tab-cnt mb-0 mb-md-5 pb-3" id="description">
        <div className={
            this.state.showDetails
              ? "font-weight-bold text-uppercase de-tlt active"
              : "font-weight-bold text-uppercase de-tlt"
          }>
          <span
            onClick={() =>
              this.setState({
                showDetails: !this.state.showDetails
              })
            }
          >
            <span> DESCRIPTION </span>{" "}
            <span className="d-md-none">
              <Down />
            </span>
          </span>
        </div>
        <div
          className={
            this.state.showDetails
              ? "detail-cnt mt-3 pt-3"
              : "detail-cnt mt-3 pt-3 d-none d-md-block"
          }
        >
          <div className="mb-5">
            {/* <div className="md-title mb-5">
            Topman slim oxford shirt in light blue
          </div> */}
            <div className="x-sm-text-fw4-g">{description}</div>
          </div>
          <div className="mb-4">
            <div className="font-weight-bold mb-4 text-uppercase">care</div>
            <ul className="d-flex wrap-ul">
              <li className="x-sm-text-fw4-g mb-4">Do not bleach</li>
              <li className="x-sm-text-fw4-g mb-4">Tumble dry low heat</li>
              <li className="x-sm-text-fw4-g mb-4">Do not dry clean</li>
              <li className="x-sm-text-fw4-g mb-4">Touch up with cool iron</li>
              <li className="x-sm-text-fw4-g mb-4">Machine wash warm</li>
              <li className="x-sm-text-fw4-g mb-4">Machine wash warm</li>
            </ul>
          </div>
          <div className="">
            <div className="font-weight-bold mb-4 text-uppercase">
              specifications
            </div>
            <ul className="d-flex wrap-ul">
              <li className="x-sm-text-fw4-g mb-4">Slim fit</li>
              <li className="x-sm-text-fw4-g mb-4">Side zip pockets</li>
              <li className="x-sm-text-fw4-g mb-4">
                Drawcord on elastic waist
              </li>
              <li className="x-sm-text-fw4-g mb-4">Interlock</li>
              <li className="x-sm-text-fw4-g mb-4">
                50% cotton, 45% recycled polyester
              </li>
              <li className="x-sm-text-fw4-g mb-4">
                Product colour: Black / White
              </li>
              <li className="x-sm-text-fw4-g mb-4">3-Stripes track pants</li>
              <li className="x-sm-text-fw4-g mb-4">Product code: GD2361</li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default Description;
