import React, { Component } from "react";
import Down from "../../../../images/icons/down";
import ProductCart from "../productCart";

class MatchStyle extends Component {
  state = {
    showDetails: false
  };
  render() {
    return (
      <div className="tab-cnt  mb-0 mb-md-5 pb-3" id="completeLook">
      <div
        className={
          this.state.showDetails
            ? "font-weight-bold text-uppercase de-tlt active"
            : "font-weight-bold text-uppercase de-tlt"
        }
      >
        <span
          onClick={() =>
            this.setState({
              showDetails: !this.state.showDetails
            })
          }
        >
          <span>  Match Style </span>{" "}
          <span className="d-md-none">
            <Down />
          </span>
        </span>
      </div>
      <div
          className={
            this.state.showDetails
              ? "detail-cnt mt-4"
              : "detail-cnt mt-4 d-none d-md-block"
          }
        >
          <div className="row mr-0">
            <ProductCart />
            <ProductCart />
            <ProductCart />
          </div>
        </div>
      </div>
    );
  }
}

export default MatchStyle;
