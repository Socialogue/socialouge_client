import React, { Component } from "react";
import ArrowDown from "../../../../images/icons/arrowDown";
import Down from "../../../../images/icons/down";
import Post from "../../../home/Home/ProductPostContentBottom/post";

class Posts extends Component {
  state = {
    showDetails: false
  };
  render() {
    return (
      <div className="tab-cnt mb-0 mb-md-5 border-0" id="posts">
        <div
          className={
            this.state.showDetails
              ? "font-weight-bold text-uppercase de-tlt active"
              : "font-weight-bold text-uppercase de-tlt"
          }
        >
          <span
            onClick={() =>
              this.setState({
                showDetails: !this.state.showDetails
              })
            }
          >
            <span>posts </span>{" "}
            <span className="d-md-none">
              <Down />
            </span>
          </span>
        </div>
        <div
          className={
            this.state.showDetails
              ? "detail-cnt mt-4"
              : "detail-cnt mt-4 d-none d-md-block"
          }
        >
          <div className="row">
            <Post class="col-12 mb-5" />
            <Post class="col-12 mb-5" />
          </div>
          <div className="text-center pb-5">
            <button className="btn border-btn border-0 p-0">
              Show more.. <br />
              <ArrowDown />
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Posts;
