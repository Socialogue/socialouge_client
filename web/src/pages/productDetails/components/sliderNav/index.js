import { getImageOriginalUrl } from 'core/utils/helpers'
import React, { Component } from 'react'
import Img from './../../../../images/eco-img.webp'
class SliderNav extends Component {
  render() {
    const { additionalImages } = this.props
    if (!additionalImages) {
      return null
    }
    return (
      <div className="slider-nav row mr-0 mb-5 d-none d-md-flex">
        <div className="col-12 col-md-6">
          <div className="row mr-0">
            {additionalImages.map((additionalImage) => {
              return (
                <div className="col-12 col-md-2 pr-0">
                  <div className="show-img-pp show-img-pp-pb-140">
                    <div
                      className="show-img-p"
                      onClick={() =>
                        this.props.changeCentreImage(additionalImage)
                      }
                    >
                      <img
                        className="cover"
                        src={getImageOriginalUrl(additionalImage)}
                      />
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
        </div>
      </div>
    )
  }
}

export default SliderNav
