import React, { Component } from "react";
import Down from "../../../../images/icons/down";

class Shipping extends Component {
  state = {
    showDetails: false
  };
  render() {
    const { shipping } = this.props;
    if (!shipping) {
      return (
        <div className="tab-cnt  mb-0 mb-md-5 pb-3" id="shipping">
          <div
            className={
              this.state.showDetails
                ? "font-weight-bold text-uppercase de-tlt active"
                : "font-weight-bold text-uppercase de-tlt"
            }
          >
            <span
              onClick={() =>
                this.setState({
                  showDetails: !this.state.showDetails
                })
              }
            >
              <span> SHIPPING </span>{" "}
              <span className="d-md-none">
                <Down />
              </span>
            </span>
          </div>
          <div
            className={
              this.state.showDetails
                ? "detail-cnt mt-4"
                : "detail-cnt mt-4 d-none d-md-block"
            }
          >
            <div className="x-sm-text-fw4-g mb-5">
              This item is shipped from UK <br />
              You can select shipping options at the checkout.
            </div>
          </div>
        </div>
      );
    }
    return (
      <div className="tab-cnt  mb-0 mb-md-5 pb-3" id="shipping">
        <div
          className={
            this.state.showDetails
              ? "font-weight-bold text-uppercase de-tlt active"
              : "font-weight-bold text-uppercase de-tlt"
          }
        >
          <span
            onClick={() =>
              this.setState({
                showDetails: !this.state.showDetails
              })
            }
          >
            <span> SHIPPING </span>{" "}
            <span className="d-md-none">
              <Down />
            </span>
          </span>
        </div>
        <div
          className={
            this.state.showDetails
              ? "detail-cnt mt-4"
              : "detail-cnt mt-4 d-none d-md-block"
          }
        >
          <div className="x-sm-text-fw4-g mb-5">
            This item is shipped from UK <br />
            You can select shipping options at the checkout.
          </div>
          <table class="table">
            <tbody>
              <tr>
                <td className="sm-text-fw6-g text-uppercase text-left">
                  Method
                </td>
                <td className="sm-text-fw6-g text-uppercase">Price</td>
                <td className="sm-text-fw6-g text-uppercase">Multi item</td>
                <td className="sm-text-fw6-g text-uppercase text-right">
                  Days
                </td>
              </tr>
              {shipping.map(ship => {
                return (
                  <tr>
                    <td className="text-left">UK Standard</td>
                    <td>2.00 €</td>
                    <td>1.00 €</td>
                    <td className="text-right">3 day</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
          <div className="x-sm-text-fw4-g text-md-right">
            If your region isn’t listed, the seller may still ship to you. Ask
            them.
          </div>
        </div>
      </div>
    );
  }
}

export default Shipping;
