import React, { Component } from "react";

import { Link } from "react-router-dom";
class Tab extends Component {
  render() {
    return (
      <ul className="nav nav-fill tab-nav mb-5">
        <li className="nav-item">
          <Link className="nav-link sm-text-fw6-g text-uppercase active" to="/">
            DESCRIPTION
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link sm-text-fw6-g text-uppercase" to="/">
            SHIPPING
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link sm-text-fw6-g text-uppercase" to="/">
            COMPLETE LOOK
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link sm-text-fw6-g text-uppercase" to="/">
            MAtCH STYLE
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link sm-text-fw6-g text-uppercase" to="/">
            POSTS
          </Link>
        </li>
      </ul>
    );
  }
}

export default Tab;
