import { getImageOriginalUrl } from 'core/utils/helpers'
import React, { Component } from 'react'
import Img from './../../../../images/eco-img.webp'

class SingleSlider extends Component {
  render() {
    const { mainImage } = this.props
    if (!mainImage) {
      return null
    }
    return (
      <div className="slider-cnt position-relative">
        <div className="discount">-20%</div>
        {/* for lopping */}
        <div className="slider">
          <div className="show-img-pp show-img-pp-pb-140">
            <div className="show-img-p">
              <img className="cover" src={getImageOriginalUrl(mainImage)} />
            </div>
          </div>
        </div>
        {/* for lopping */}
      </div>
    )
  }
}

export default SingleSlider
