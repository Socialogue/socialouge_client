import React, { Component } from "react";
import Header from "../home/Header/Header";
import ArrowDown from "../../images/icons/arrowDown";
import Img from "./../../images/eco-img.webp";
import Img1 from "./../../images/images.webp";
import SocialSymbol from "../../images/icons/socialSymbol";
import HorizontalInput from "../userSettings/components/horizontalInput";
import ArrowRight from "../../images/icons/arrowRight";
import Comment from "../../images/icons/comment";
import LongDownArrow from "../../images/icons/longDownArrow";
import Footer from "../home/Footer/Footer";
import FixedFooter from "../Common/fixedFooter";

class AboutUs extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="about-cnt">
          <div className="max-content-width-1 py-3 py-xl-5 my-3 my-xl-5 px-3">
            <div className="x-md-title border-bottom-1 pb-2 pb-xl-4 mb-4 mb-md-5 about-m-tlt">
              <div className="position-relative ml-5 ml-xl-0 pl-3 pl-xl-0">
                <span className="about-sym">
                  <SocialSymbol />
                </span>{" "}
                about
              </div>
              <div className="ml-5 pl-3 pl-xl-0">
                socialogue
                <span className="dot-r" />
              </div>
            </div>
            <div className="row">
              {/* for desktop start */}
              <div className="col-12 col-md-7 d-none d-md-block">
                <div className="mr-0 mr-md-5">
                  <div className="mt-5 pt-5 x-md-text-fw6 text-uppercase font-weight-bold">
                    MORE THAN 100 + <br /> SHOPS CHOOSE US
                  </div>
                </div>
              </div>
              {/* for desktop end */}
              <div className="col-12 col-md-5">
                <div className="d-flex align-items-md-center mb-0 mb-md-5 pb-0 pb-md-3">
                  <span className="sm-text-fw6 text-uppercase">
                    brand story
                  </span>
                  <span className="ml-2 sel-svg d-md-inline-block d-none">
                    <ArrowDown />
                  </span>
                  <span className="ml-2 d-inline-block d-md-none">
                    <LongDownArrow />
                  </span>
                </div>
                <div className="sel-img mb-3 d-block d-md-none">
                  <img className="cover" src={Img1} />
                </div>
              </div>
            </div>

            <div className="row">
              {/* for desktop start */}
              <div className="col-12  col-md-6 col-xl-7 d-none d-md-block">
                <div className="my-5 py-5 " />
                <div className="my-5 py-5" />
                <div className="row">
                  <div className="col-12 col-md-6">
                    <div className="sel-img mb-3">
                      <img className="cover" src={Img} />
                    </div>
                    <div className="font-weight-bold text-uppercase mb-3">
                      our values
                    </div>
                    <div className="x-sm-text-fw4-g mb-4">
                      It is a long established fact that a reader will be
                      distracted by the readable content of a page when looking
                      at its layout.
                    </div>
                  </div>
                </div>
              </div>
              {/* for desktop end*/}

              <div className="col-12 col-md-6 col-xl-5">
                <div className="mb-5 pb-3">
                  <div className="md-title mb-3">who we are?</div>
                  <div className="x-sm-text-fw4-g mb-4">
                    Simply dummy text of the printing and typesetting
                    industry.Lorem Ipsum has been the industry 's standard dummy
                    text ever since the 1500 s, when an unknown printer took a
                    galley of type and scrambled it to make a type specimen
                    book.
                  </div>
                  <div className="x-sm-text-fw4-g mb-4">
                    It has survived not only five centuries, but also the leap
                    into electronic typesetting, remaining essentially
                    unchanged.It was popularised in the 1960 s with the release
                    of Letraset sheets containing Lorem Ipsum passages, and more
                    recently with desktop publishing software like Aldus
                    PageMaker including versions of Lorem Ipsum.
                  </div>
                  {/* for mobile start */}
                  <div className="d-block d-md-none mb-5">
                    <div className="mr-0 mr-md-5">
                      <div className="x-md-text-fw6 text-uppercase font-weight-bold">
                        MORE THAN 100 + <br /> SHOPS CHOOSE US
                      </div>
                    </div>
                  </div>
                  {/* for mobile end */}
                  <div className="">
                    <div className="position-relative">
                      <div className="show-img-pp show-img-pp-pb-140 ab-lf-img">
                        <div className="show-img-p">
                          <img className="cover" src={Img} />
                        </div>
                      </div>
                      <div className="about-side-img h-100">
                        <img className="cover" src={Img} />
                      </div>
                    </div>
                    <div className="sm-text-fw6 text-uppercase text-right mt-2">
                      brands that collaborate with us
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12 col-md-6 col-xl-7">
                <div className="row">
                  {/* for mobile start */}
                  <div className="col-12 d-block d-md-none">
                    <div className="row">
                      <div className="col-12 col-md-6 mb-5">
                        <div className="font-weight-bold text-uppercase mb-3">
                          our values
                        </div>
                        <div className="x-sm-text-fw4-g mb-3">
                          It is a long established fact that a reader will be
                          distracted by the readable content of a page when
                          looking at its layout.
                        </div>
                        <div className="sel-img">
                          <img className="cover" src={Img} />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* for mobile end*/}

                  <div className="col-12 col-md-10 col-xl-9">
                    <div className="contact">
                      <div className="md-title mb-2">let’s collaborate!</div>
                      <div className="sm-text-fw6 text-uppercase">
                        <nav aria-label="breadcrumb" className="">
                          <ol className="breadcrumb custom-breadcrumb mb-0">
                            <li className="breadcrumb-item">
                              contact@socialogue.com{" "}
                            </li>
                            <li className="breadcrumb-item">0037068023456</li>
                            <li className="breadcrumb-item">@socialogue</li>
                          </ol>
                        </nav>
                      </div>
                      <div className="contact-form mt-5">
                        <div className="input-hide-icon mb-5">
                          <HorizontalInput
                            formInputClass="form-input position-relative"
                            label="your name"
                            errorText="Please enter Your name"
                            isRequired={false}
                            type="text"
                            className="form-control custom-input"
                            placeholder="Your Shop name"
                            name="name"
                          />
                        </div>
                        <div className="input-hide-icon mb-5">
                          <HorizontalInput
                            formInputClass="form-input position-relative"
                            label="company name"
                            errorText="Please enter Your company name"
                            isRequired={false}
                            type="text"
                            className="form-control custom-input"
                            placeholder="Your company name"
                            name="companyName"
                          />
                        </div>
                        <div className="input-hide-icon mb-5">
                          <HorizontalInput
                            formInputClass="form-input position-relative"
                            label="Email"
                            errorText="Please enter Your Email"
                            isRequired={true}
                            type="text"
                            className="form-control custom-input"
                            placeholder="Bershka@gmail.com"
                            name="email"
                          />
                        </div>
                        <div className="input-hide-icon mb-0 mb-md-4">
                          <HorizontalInput
                            formInputClass="form-input position-relative"
                            label="message"
                            errorText="Please enter Your message"
                            isRequired={true}
                            type="text"
                            className="form-control custom-input"
                            placeholder="Start typing..."
                            name="message"
                          />
                        </div>
                        <div className="text-right pt-0 pt-md-2">
                          <button className="btn border-btn border-0 p-0">
                            Send <ArrowRight />
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-12 col-md-6 col-xl-5" />
              <div className="col-12 col-md-6 col-xl-7" />
              <div className="col-12 col-md-6 col-xl-5 mt-5">
                <button className="btn border-btn chat-ab-btn d-flex-box p-0">
                  <Comment />
                </button>
                <table className="con-table">
                  <tr>
                    <td className="sm-text-fw6 text-uppercase pr-3 pr-md-4 pb-4 text-nowrap">
                      follow us
                    </td>
                    <td className="sm-text-fw6 text-uppercase pl-4 pl-xl-5 pb-4">
                      address
                    </td>
                  </tr>
                  <tr>
                    <td className="sm-text-fw6 text-uppercase pr-3 pr-md-4">
                      <span className="pr-4">Ig</span>
                      <span>fb</span>
                    </td>
                    <td className="x-sm-text-fw4-g pl-4 pl-xl-5 ">
                      65 Airport Street Morrisville, PA 19067
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
        <Footer />
        <div className="ab-mf px-4 mb-4 d-block d-md-none">
          <FixedFooter />
        </div>
      </div>
    );
  }
}

export default AboutUs;
