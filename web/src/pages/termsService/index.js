import React, { Component } from "react";
import { Link } from "react-router-dom";
import ArrowLeft from "../../images/icons/arrowLeft";
import Footer from "../home/Footer/Footer";
import Header from "../home/Header/Header";

class TermsService extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="max-content-width-1 py-3 py-xl-5 my-3 my-xl-5 px-4">
          <div className="x-md-title border-bottom-1 pb-3 pb-md-4 mb-4 mb-md-5">
            terms of service<span className="dot-r" />
          </div>
          <div className="pvf-cnt">
            <div className="mb-5">
              <div className="md-title mb-3">Socialogue Terms of Service</div>

              <div className="x-sm-text-fw4-g mb-3">
                Here you can find Socialogue Terms of Service.
              </div>
            </div>
            <div className="mb-4">
              <div className="x-md-text-fw6 font-weight-bold mb-3 text-uppercase">
                Your Account and Responsibilities
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                You’re responsible for your use of the Services and any content
                you provide, including compliance with applicable laws. Content
                on the Services may be protected by others’ intellectual
                property rights. Please don’t copy, upload, download, or share
                content unless you have the right to do so. Your use of the
                Services must comply with our Rules. You may need to register
                for an account to access some or all of our Services. Help us
                keep your account protected. Safeguard your password to the
                account, and keep your account information current. We recommend
                that you do not share your password with others. If you’re
                accepting these Terms and using the Services on behalf of
                someone else (such as another person or entity), you represent
                that you’re authorized to do so, and in that case the words
                “you” or “your” in these Terms include that other person or
                entity. To use our Services, you must be at least 13 years old.
                If you use the Services to access, collect, or use personal
                information about other Medium users (“Personal Information”),
                you agree to do so in compliance with applicable laws. You
                further agree not to sell any Personal Information, where the
                term “sell” has the meaning given to it under applicable laws.
              </div>
            </div>
            <div className="mb-4">
              <div className="x-md-text-fw6 font-weight-bold mb-3 text-uppercase">
                User Content on the Services
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                Medium may review your conduct and content for compliance with
                these Terms and our Rules, and reserves the right to remove any
                violating content. Medium reserves the right to delete or
                disable content alleged to be infringing the intellectual
                property rights of others, and to terminate accounts of repeat
                infringers. We respond to notices of alleged copyright
                infringement if they comply with the law; please report such
                notices using our Copyright Policy.
              </div>
            </div>
            <div className="mb-4">
              <div className="x-md-text-fw6 font-weight-bold mb-3 text-uppercase">
                Transfer and Processing Data
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                Search engines may index your Medium user profile page, public
                interactions (such as claps or highlights), and post pages, such
                that people may find these pages when searching against your
                name on services like Google, DuckDuckGo, or Bing. Users may
                also share links to your content on social media platforms such
                as Facebook or Twitter.
              </div>
            </div>
            <div className="mb-4">
              <div className="x-md-text-fw6 font-weight-bold mb-3 text-uppercase">
                Rights and Ownership
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                Medium uses third-party vendors and hosting partners, such as
                Amazon, for hardware, software, networking, storage, and related
                technology we need to run Medium. We maintain two types of logs:
                server logs and event logs. By using the Services, you authorize
                Medium to transfer, store, and use your information in the
                United States and any other country where we operate.
              </div>
            </div>
            <div className="mb-4">
              <div className="x-md-text-fw6 font-weight-bold mb-3 text-uppercase">
                Indemnification
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                Some of the content that you see displayed on Medium is not
                hosted by Medium. These “embeds” are hosted by a third-party and
                embedded in a Medium page, so that it appears to be part of that
                page. For example: YouTube or Vimeo videos, Imgur or Giphy gifs,
                SoundCloud audio files, Twitter tweets, GitHub code snippets, or
                Scribd documents that appear within a Medium post. These files
                send data to the hosted site just as if you were visiting that
                site directly (for example, when you load a Medium post page
                with a YouTube video embedded in it, that video appears because
                of a pointer to files hosted by YouTube, and in turn YouTube
                receives data about your activity, such as your IP address and
                how much of the video you watch).
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                Medium doesn’t control what data third parties collect in cases
                like this, or what they ultimately do with it. So, third-party
                embeds on Medium are not covered by this Privacy Policy. They
                are covered by the privacy policy of the third-party service
                (so, when you watch a YouTube video embedded in a Medium post,
                the use of data about your interactions with the video would be
                covered by YouTube’s privacy policy).
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                Some embeds may ask you for personal information, such as
                submitting your email address, through a form linked to from a
                Medium post. We do our best to keep bad actors off of Medium.
                However, if you choose to submit your information to a third
                party this way, we don’t know what they may do with it. As
                explained above, their actions are not covered by this Privacy
                Policy. So, please be careful when you see embedded forms on
                Medium asking for your email address or any other personal
                information. Make sure you understand who you are submitting
                your information to and what they say they plan to do with it.
                We suggest that you do not submit your email address or other
                personal information to any third-party through an embedded
                form.
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                When posting on Medium, you may not embed a form that allows
                submission of personal information by users. You must link
                offsite to a page that allows such submissions by users, and
                that page’s appearance must be distinct enough from Medium to
                ensure it does not cause confusion among users over to whom they
                are submitting personal information. Failure to do so may lead
                Medium to disable the post or take other action to limit or
                disable your account.
              </div>
            </div>
            <div className="mb-4 pt-4">
              <div className="x-md-text-fw6 font-weight-bold mb-3 text-uppercase">
                Contact Us
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                You may contact us by emailing us at contact@socialogue.com
              </div>
            </div>
            <div>
              <Link className="x-sm-text-fw6" to="/">
                <ArrowLeft /> Back to Main
              </Link>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default TermsService;
