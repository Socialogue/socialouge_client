import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import SmLogo from './../../images/sm-logo.svg'
import Back from './../../images/icons/back.svg'
import ResetImgLg from './../../images/reset-img.webp'
import { selectToken, selectUserToVerifyOtp } from 'core/selectors/user'
import { resetPassword } from 'core/actions/authActionCreators'
import { ROUTE_HOME } from '../../routes/constants'

class ResetPassword extends React.Component {
  constructor() {
    super()
    this.state = {
      loading: false,
      password: '',
      repeatPassword: '',
      error: '',
    }
  }
  inputHandler = (e) => {
    const name = e.target.name
    const value = e.target.value
    this.setState({
      [name]: value,
    })
  }

  onSubmit = async (e) => {
    if (this.state.password == '' || this.state.repeatPassword == '') {
      return
    }
    if (this.state.password != this.state.repeatPassword) {
      this.setState({
        error: 'Password not match with confirm password',
      })
      return
    }
    try {
      await this.props.resetPassword(
        this.props.user.username,
        this.props.user.accountType,
        this.state.password,
        this.state.repeatPassword,
      )
      if (this.props.isLoggedIn) {
        this.props.history.push(ROUTE_HOME)
      }
    } catch (e) {
      console.log('catch', e)
    } finally {
      console.log('finally')
    }
    return true
  }

  render() {
    return (
      <div className="form-module-cnt">
        <div className="row m-0 wh-100">
          <div className="col-12 col-md-7 p-0 h-100">
            <div className="form-info-cnt d-flex-box wh-100">
              <div className="back-link">
                <Link to={ROUTE_HOME}>
                  <img className="" src={Back} alt="img" />
                  Back to Main
                </Link>
              </div>
              <div className="form-info">
                <div className="form-logo">
                  {' '}
                  <img className="" src={SmLogo} alt="img" />
                </div>
                <div className="lg-title">
                  resset password
                  <span className="dot-r" />
                </div>
                <div className="form">
                  <div className="form-group">
                    <label className="x-sm-text-fw6 text-uppercase">
                      New Password
                    </label>
                    <input
                      type="password"
                      className="form-control custom-input"
                      name="password"
                      placeholder="New Password"
                      onChange={this.inputHandler}
                    />
                  </div>
                  <div className="form-group">
                    <label className="x-sm-text-fw6 text-uppercase">
                      Repeat new Password
                    </label>
                    <input
                      type="password"
                      className="form-control custom-input"
                      name="repeatPassword"
                      placeholder="Repeat New Password"
                      onChange={this.inputHandler}
                    />
                    <span className="text-danger">{this.state.error}</span>
                  </div>
                </div>
                <div className="form-group">
                  <button
                    className="btn black-bg-btn text-uppercase w-100"
                    onClick={this.onSubmit}
                  >
                    Reset
                  </button>
                </div>
                <div className="form-group text-center">
                  <Link to="#" className="md-text-fw6">
                    {' '}
                    Back to Login
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-5 p-0 h-100 d-none d-md-block">
            <div className="form-img-cnt wh-100">
              <img className="cover" src={ResetImgLg} alt="img" />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    user: selectUserToVerifyOtp(state),
    isLoggedIn: !!selectToken(state),
    auth: state.auth,
  }),
  {
    resetPassword,
  },
)(ResetPassword)
