import React, { Component } from "react";
import Down from "../../../../images/icons/down";
import "./productFilter.scss";
class ProductFilter extends Component {
  handleInputChange = () => {
    console.log("hgfkhgs");
  };
  render() {
    return (
      <div className="product-filter d-none d-md-block">
        <div className="filter-ul">
          {/* li need active class */}
          <div className="filter-li pb-2 sm-text-fw6 text-uppercase">
            <div className="mb-2 cursor-pointer">
              Price <Down />
            </div>
            <div className="li-cnt d-none mb-3">
              need to range
            </div>
          </div>
          <div className="filter-li pb-2 sm-text-fw6 text-uppercase">
            <div className="mb-2 cursor-pointer">
              size <Down />
            </div>
            <div className="li-cnt d-none mb-3">
              <label className="ctm-container d-block  pb-2">
                small
                <input type="checkbox" />
                <span className="checkmark" />
                <span className="sm-text-fw6-g float-right">(12)</span>
              </label>
              <label className="ctm-container d-block pb-2">
                larg
                <input type="checkbox" />
                <span className="checkmark" />
                <span className="sm-text-fw6-g float-right">(12)</span>
              </label>
            </div>
          </div>
          <div className="filter-li pb-2 sm-text-fw6 text-uppercase">
            <div className="mb-2 cursor-pointer">
              Colour <Down />
            </div>
            <div className="li-cnt d-none mb-3">
              <label className="ctm-container d-block  pb-2">
                Blue
                <input type="checkbox" />
                <span className="checkmark" />
                <span className="sm-text-fw6-g float-right">(12)</span>
              </label>
              <label className="ctm-container d-block pb-2">
                red
                <input type="checkbox" />
                <span className="checkmark" />
                <span className="sm-text-fw6-g float-right">(12)</span>
              </label>
            </div>
          </div>
          <div className="filter-li pb-2 sm-text-fw6 text-uppercase active">
            <div className="mb-2 cursor-pointer">
              Gender <Down />
            </div>
            <div className="li-cnt d-none mb-3">
              <label className="ctm-container d-block  pb-2">
                Male
                <input type="checkbox" />
                <span className="checkmark" />
                <span className="sm-text-fw6-g float-right">(12)</span>
              </label>
              <label className="ctm-container d-block pb-2">
                FeMale
                <input type="checkbox" />
                <span className="checkmark" />
                <span className="sm-text-fw6-g float-right">(12)</span>
              </label>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductFilter;
