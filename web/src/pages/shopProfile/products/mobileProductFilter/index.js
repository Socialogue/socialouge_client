import React, { Component } from "react";
import Close from "../../../../images/icons/close";
import Down from "../../../../images/icons/down";

class MobileProductFilter extends Component {
  state = {
    showMobileF: false
  };
  render() {
    return (
      <div className="mobile-sort-filter mobile-pro-filter d-inline-block d-md-none">
        <div
          onClick={() =>
            this.setState({
              showMobileF: !this.state.showMobileF
            })
          }
          className="sm-text-fw6 text-uppercase ff-txt cursor-pointer"
        >
          Filter <Down />
        </div>
        <div
          className={
            this.state.showMobileF
              ? "mobile-filter-box d-block"
              : "mobile-filter-box d-none"
          }
        >
          <div className="mb-3">
            <span className="sm-text-fw6 text-uppercase ff-txt">
              Filter <Down />
            </span>
            <span  onClick={() =>
            this.setState({
              showMobileF: !this.state.showMobileF
            })
          } className="float-right cursor-pointer">
              <Close />
            </span>
          </div>
          <div className="filter-ul">
            {/* li need active class */}
            <div className="filter-li pb-2 sm-text-fw6 text-uppercase active">
              <div className="mb-2 pb-1 cursor-pointer">
                Gender <Down />
              </div>
              <div className="li-cnt mb-3">
                <label className="ctm-container d-block  pb-2">
                  Male
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
                <label className="ctm-container d-block pb-2">
                  FeMale
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
                <label className="ctm-container d-block pb-2">
                  Unisex
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
              </div>
            </div>
            <div className="filter-li pb-2 sm-text-fw6 text-uppercase">
              <div className="mb-2 pb-1 cursor-pointer">
                Price <Down />
              </div>
              <div className="li-cnt mb-3">need to range</div>
            </div>
            <div className="filter-li pb-2 sm-text-fw6 text-uppercase">
              <div className="mb-2 pb-1 cursor-pointer">
                size <Down />
              </div>
              <div className="li-cnt mb-3">
                <label className="ctm-container d-block  pb-2">
                  s
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
                <label className="ctm-container d-block pb-2">
                  m
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
                <label className="ctm-container d-block pb-2">
                  l
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
                <label className="ctm-container d-block pb-2">
                  xl
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
                <label className="ctm-container d-block pb-2">
                  xs
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
                <label className="ctm-container d-block pb-2">
                  xxl
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
              </div>
            </div>
            <div className="filter-li pb-2 sm-text-fw6 text-uppercase">
              <div className="mb-2 pb-1 cursor-pointer">
                Colour <Down />
              </div>
              <div className="li-cnt mb-3">
                <label className="ctm-container d-block  pb-2">
                  Black
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
                <label className="ctm-container d-block pb-2">
                  Blue
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
                <label className="ctm-container d-block pb-2">
                  Brown
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
                <label className="ctm-container d-block pb-2">
                  Ecru
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
                <label className="ctm-container d-block pb-2">
                  Green
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
                <label className="ctm-container d-block pb-2">
                  Grey
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MobileProductFilter;
