import React, { Component } from 'react'
import Header from '../../home/Header/Header'
import Img from './../../../images/eco-img.webp'
import CoverPhoto from '../../profile/common/coverPhoto'
import './../../profile/index.scss'
import UserInfo from '../../profile/common/userInfo/userInfo'
import Tab from '../tab/tab'
import ProductSm from '../../Common/productSm'
import FixedFooter from '../../Common/fixedFooter'
import SortView from '../../home/sortView'
import ProductFilter from './productFilter'
import ShopInfo from '../../../components/ShopInfo'
import { connect } from 'react-redux'
import { selectActiveShop } from 'core/selectors/account'
import { selectViewType } from 'core/selectors/user'
import { SORT_VIEW_BOTTOM, SORT_VIEW_MID, SORT_VIEW_TOP } from 'core/constants'
import ProductLg from '../../Common/productLg'
import { Fragment } from 'react'
import MobileProductFilter from './mobileProductFilter'
import MobileSort from './mobileSort'
import { getShopProducts, getShopData } from 'core/actions/shopActionCreators'
import { selectShop, selectShopProducts } from 'core/selectors/shops'
import {
  ROUTE_PRODUCT_DETAILS,
  ROUTE_SHOP_PROFILE_PRODUCTS,
} from '../../../routes/constants'

class Products extends Component {
  state = {
    allProducts: [],
    limit: 100,
    offset: 0,
    setData: false,
  }

  componentDidMount = () => {
    const shopId = this.props.match.params.id
    const limit = this.state.limit
    const offset = this.state.offset
    this.props.getShopData(shopId)
    this.props.getShopProducts(shopId, limit, offset)
  }

  componentDidUpdate = (prevProps) => {
    if (
      JSON.stringify(prevProps.shopProducts) !==
        JSON.stringify(this.props.shopProducts) ||
      !this.state.setData
    ) {
      if (this.props.shopProducts) {
        this.setState({ allProducts: this.props.shopProducts, setData: true })
      }
    }
  }

  viewProduct = (slug, productId) => {
    this.props.history.push(
      ROUTE_PRODUCT_DETAILS.replace(':slug', slug).replace(
        ':productId',
        productId,
      ),
    )
  }

  shopProfile = (shopId) => {
    const route = ROUTE_SHOP_PROFILE_PRODUCTS.replace(':id', shopId)
    this.props.history.push(route)
  }

  render() {
    const { allProducts } = this.state
    let cls = 'col-12 col-md-6 col-lg-4 post mb-5'
    if (this.props.sortView == SORT_VIEW_BOTTOM) {
      cls = 'col-12 col-md-6 col-lg-4 post mb-5'
    }
    if (this.props.sortView == SORT_VIEW_MID) {
      cls = 'col-12 col-md-6 post mb-5'
    }
    if (this.props.sortView == SORT_VIEW_TOP) {
      cls = 'col-12 post mb-5'
    }

    return (
      <div>
        <Header />
        <CoverPhoto src={Img} status="shop" />
        <div className="user-profile-cnt shop-profile-cnt mx-auto mb-0 mb-md-5 pb-0 pb-md-4">
          <ShopInfo shop={this.props.shopData} />
          <Tab shop={this.props.shopData} />
          <div className="product-cnt mt-4 mt-md-5 pt-2 pb-5">
            {/* <div className="text-right font-weight-5 mb-3">
              Showing 66 products
            </div> */}
            <div className="row">
              {this.props.sortView == SORT_VIEW_TOP ? (
                <Fragment>
                  {allProducts.map((product) => {
                    return (
                      <ProductLg
                        // colClass={cls}
                        proImg={Img}
                        title="wide brim bucket hat "
                        userName="BUTIQ K"
                        price="30"
                        product={product}
                        viewProduct={this.viewProduct}
                        shopProfile={this.shopProfile}
                      />
                    )
                  })}
                </Fragment>
              ) : (
                <Fragment>
                  {allProducts.map((product) => {
                    return (
                      <ProductSm
                        colClass={cls}
                        proImg={Img}
                        title="wide brim bucket hat "
                        userName="BUTIQ K"
                        price="30"
                        product={product}
                        viewProduct={this.viewProduct}
                        shopProfile={this.shopProfile}
                      />
                    )
                  })}
                </Fragment>
              )}
            </div>
          </div>
        </div>
        <SortView />
        <ProductFilter />
        <MobileProductFilter />
        <MobileSort />
        <div className="ab-mf px-4 mb-4 d-block">
          <FixedFooter />
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    shop: selectActiveShop(state),
    sortView: selectViewType(state),
    shopProducts: selectShopProducts(state),
    shopData: selectShop(state),
  }),
  {
    getShopProducts,
    getShopData,
  },
)(Products)
