import React, { Component } from "react";

class AboutMe extends Component {
  render() {
    return (
      <div className="row mx-0 mb-5">
        <div className="col-12 col-md-6 px-0">
          <div className="show-img-pp show-img-pp-pb-80">
            <div className="show-img-p">
              <img className="cover" src={this.props.aboutImg} />
            </div>
          </div>
        </div>
        <div className="col-12 col-md-6 px-0 d-flex-box about-bg">
          <div className="about-tx m-4">
            <div className="sm-text-fw6 text-uppercase mb-3 mb-md-4 pr-4">
              {this.props.title}
            </div>
            <div className="sm-text-fw6-g mb-3 mb-md-4">{this.props.text}</div>
            <button className="btn border-btn">View More</button>
          </div>
        </div>
      </div>
    );
  }
}

export default AboutMe;
