import React, { Component } from 'react'
import Footer from '../../home/Footer/Footer'
import Header from '../../home/Header/Header'
import Img from './../../../images/eco-img.webp'
import CoverPhoto from '../../profile/common/coverPhoto'
import './../../profile/index.scss'
import UserInfo from '../../profile/common/userInfo/userInfo'
import Tab from '../tab/tab'
import ProductSm from '../../Common/productSm'
import AboutMe from './aboutMe'
import ShopInfo from '../../../components/ShopInfo'
import { connect } from 'react-redux'
import { selectActiveShop } from 'core/selectors/account'
import FixedFooter from '../../Common/fixedFooter'
import { getShopPosts, getShopData } from 'core/actions/shopActionCreators'
import { selectShop, selectShopPosts } from 'core/selectors/shops'
import { selectUser } from 'core/selectors/user'

class About extends Component {
  state = {
    allPosts: [],
    limit: 100,
    offset: 0,
    setData: false,
  }

  componentDidMount = () => {
    // TODO need to make decision whether shop have post or not...
    const shopId = this.props.match.params.id
    const limit = this.state.limit
    const offset = this.state.offset
    this.props.getShopData(shopId)
    // this.props.getShopPosts(shopId, limit, offset)
  }

  render() {
    return (
      <div>
        <Header />
        <CoverPhoto src={Img} status="shop" />
        <div className="user-profile-cnt shop-profile-cnt mx-auto mb-5 pb-5">
          <ShopInfo shop={this.props.shopData} />
          <Tab />
          <div className="about-cnt mt-5 pt-0 pt-md-3 px-0">
            <AboutMe
              aboutImg={Img}
              title=" Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
            do eiusmod."
              text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
            do eiusmod."
            />

            <div className="row">
              <ProductSm
                colClass="col-12 col-md-6 col-lg-4 post mb-5"
                proImg={Img}
                title="wide brim bucket hat "
                userName="BUTIQ K"
                price="30"
              />
              <ProductSm
                colClass="col-12 col-md-6 col-lg-4 post mb-5"
                proImg={Img}
                title="wide brim bucket hat "
                userName="BUTIQ K"
                price="30"
              />
              <ProductSm
                colClass="col-12 col-md-6 col-lg-4 post mb-5"
                proImg={Img}
                title="wide brim bucket hat "
                userName="BUTIQ K"
                price="30"
              />
            </div>

            <div className="mb-5">
              <div className="pro-cover position-relative upload-ov-show">
                <img src={Img} className="cover" />
              </div>
            </div>

            <AboutMe
              aboutImg={Img}
              title=" Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
            do eiusmod."
              text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
            do eiusmod."
            />

            <div>
              <div className="md-title mb-3">Who are you?</div>
              <div className="x-sm-text-fw4-g">
                vero eos et accusamus et iusto odio dignissimos ducimus qui
                blanditiis praesentium voluptatum deleniti atque corrupti quos
                dolores et quas molestias excepturi sint occaecati cupiditate
                non provident, similique sunt in culpa qui officia deserunt
                mollitia animi, id est laborum et dolorum fuga. Et harum quidem
                rerum facilis est et expedita distinctio. Nam libero tempore,
                cum soluta nobis est eligendi optio cumque nihil impedit quo
                minus id quod maxime placeat facere possimus, omnis voluptas
                assumenda est, omnis dolor repellendus. Temporibus autem
                quibusdam et aut officiis debitis aut rerum necessitatibus saepe
                eveniet ut et voluptates repudiandae sint et molestiae non
                recusandae. Itaque earum rerum hic tenetur a sapiente delectus,
                ut aut reiciendis voluptatibus maiores alias consequatur aut
                perferendis doloribus asperiores repellat."
              </div>
            </div>
          </div>
        </div>
        <Footer />
        <div className="ab-mf px-4 mb-4 d-block d-md-none">
          <FixedFooter />
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    shop: selectActiveShop(state),
    user: selectUser(state),
    shopData: selectShop(state),
  }),
  {
    getShopData,
  },
)(About)
