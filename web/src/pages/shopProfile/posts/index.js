import React, { Component } from 'react'
import Header from '../../home/Header/Header'
import Img from './../../../images/eco-img.webp'
import CoverPhoto from '../../profile/common/coverPhoto'
import './../../profile/index.scss'
import UserInfo from '../../profile/common/userInfo/userInfo'
import Tab from '../tab/tab'
import ProductSm from '../../Common/productSm'
import FixedFooter from '../../Common/fixedFooter'
import SortView from '../../home/sortView'
import FilterMenu from '../../home/FilterMenu/FilterMenu'
import { connect } from 'react-redux'
import { selectActiveShop } from 'core/selectors/account'
import ShopInfo from '../../../components/ShopInfo'
import { selectUser, selectViewType } from 'core/selectors/user'
import { SORT_VIEW_BOTTOM, SORT_VIEW_MID, SORT_VIEW_TOP } from 'core/constants'
import { Fragment } from 'react'
import ProductLg from '../../Common/productLg'
import { getShopPosts, getShopData } from 'core/actions/shopActionCreators'
import { selectShop, selectShopPosts } from 'core/selectors/shops'

class Posts extends Component {
  state = {
    allPosts: [],
    limit: 100,
    offset: 0,
    setData: false,
  }

  componentDidMount = () => {
    // TODO need to make decision whether shop have post or not...
    const shopId = this.props.match.params.id
    const limit = this.state.limit
    const offset = this.state.offset
    this.props.getShopData(shopId)
    // this.props.getShopPosts(shopId, limit, offset)
  }

  componentDidUpdate = (prevProps) => {
    if (
      JSON.stringify(prevProps.shopPosts) !==
        JSON.stringify(this.props.shopPosts) ||
      !this.state.setData
    ) {
      if (this.props.shopPosts) {
        this.setState({ allPosts: this.props.shopPosts, setData: true })
      }
    }
  }
  render() {
    const { allPosts } = this.state

    let cls = 'col-12 col-md-6 col-lg-4 post mb-5'
    if (this.props.sortView == SORT_VIEW_BOTTOM) {
      cls = 'col-12 col-md-6 col-lg-4 post mb-5'
    }
    if (this.props.sortView == SORT_VIEW_MID) {
      cls = 'col-12 col-md-6 post mb-5'
    }
    if (this.props.sortView == SORT_VIEW_TOP) {
      cls = 'col-12 post mb-5'
    }
    return (
      <div>
        <Header />
        <CoverPhoto src={Img} status="shop" />
        <div className="user-profile-cnt shop-profile-cnt mx-auto mb-0 mb-md-5 pb-3 pb-md-4">
          <ShopInfo shop={this.props.shopData} />
          <Tab />
          <div className="product-cnt mt-4 mt-md-5 pt-2 pb-5">
            {/* <div className="text-right font-weight-5 mb-3">
              Showing 66 products
            </div> */}
            <div className="row">
              {this.props.sortView == SORT_VIEW_TOP ? (
                <Fragment>
                  {allPosts.map((post) => {
                    return (
                      <ProductLg
                        // colClass={cls}
                        proImg={Img}
                        title="wide brim bucket hat "
                        userName="BUTIQ K"
                        price="30"
                        post={post}
                      />
                    )
                  })}
                </Fragment>
              ) : (
                <Fragment>
                  {allPosts.map((post) => {
                    return (
                      <ProductSm
                        colClass={cls}
                        proImg={Img}
                        title="wide brim bucket hat "
                        userName="BUTIQ K"
                        price="30"
                        post={post}
                      />
                    )
                  })}
                </Fragment>
              )}
            </div>
          </div>
        </div>
        <div className="ab-mf px-4 mb-4 d-block">
          <FixedFooter />
        </div>
        <FilterMenu />
        <SortView />
      </div>
    )
  }
}

export default connect(
  (state) => ({
    shop: selectActiveShop(state),
    shopPosts: selectShopPosts(state),
    sortView: selectViewType(state),
    user: selectUser(state),
    shopData: selectShop(state),
  }),
  {
    getShopPosts,
    getShopData,
  },
)(Posts)
