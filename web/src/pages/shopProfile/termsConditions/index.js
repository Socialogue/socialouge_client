import React, { Component } from 'react'
import Footer from '../../home/Footer/Footer'
import Header from '../../home/Header/Header'
import CoverPhoto from '../../profile/common/coverPhoto'
import Tab from '../tab/tab'
import UserInfo from '../../profile/common/userInfo/userInfo'
import Img from './../../../images/eco-img.webp'
import './../../profile/index.scss'
import ShopInfo from '../../../components/ShopInfo'
import { connect } from 'react-redux'
import { selectActiveShop } from 'core/selectors/account'
import FixedFooter from '../../Common/fixedFooter'
import { getShopPosts, getShopData } from 'core/actions/shopActionCreators'
import { selectShop, selectShopPosts } from 'core/selectors/shops'
import { selectUser } from 'core/selectors/user'

class TermsConditions extends Component {
  state = {
    allPosts: [],
    limit: 100,
    offset: 0,
    setData: false,
  }

  componentDidMount = () => {
    // TODO need to make decision whether shop have post or not...
    const shopId = this.props.match.params.id
    const limit = this.state.limit
    const offset = this.state.offset
    this.props.getShopData(shopId)
    // this.props.getShopPosts(shopId, limit, offset)
  }
  render() {
    return (
      <div>
        <Header />
        <CoverPhoto src={Img} status="shop" />
        <div className="user-profile-cnt shop-profile-cnt mx-auto mb-5">
          <ShopInfo shop={this.props.shopData} />
          <Tab />
          <div className="tc-cnt">
            <div className="font-weight-bold mb-4 pb-2 text-uppercase">
              TERMS & CODITIONS
            </div>
            <div className="tc-txt mb-5">
              <div className="sm-text-fw4-g mb-3">
                There are many variations of passages of Lorem Ipsum available,
                but the majority have suffered alteration in some form, by
                injected humour, or randomised words which don't look even
                slightly believable. If you are going to use a passage of Lorem
                Ipsum, you need to be sure there isn't anything embarrassing
                hidden in the middle of text. All the Lorem Ipsum generators on
                the Internet tend to repeat predefined chunks as necessary,
                making this the first true generator on the Internet. It uses a
                dictionary of over 200 Latin words, combined with a handful of
                model sentence structures, to generate Lorem Ipsum which looks
                reasonable. The generated Lorem Ipsum is therefore There are
                many variations of passages of Lorem Ipsum available, but the
                majority have suffered alteration in some form, by injected
                humour, or randomised words which don't look even Internet. It
                uses a dictionary of over 200 Latin words, combined with a
                handful of model sentence structures, to generate Lorem Ipsum
                which looks reasonable. The generated Lorem Ipsum is therefore
              </div>
              <ul className="pl-3">
                <li className="sm-text-fw4-g mb-3">
                  If you are going to use a passage of Loremges of Lorem Ipsum
                  available, but the majority have suffered alteration in some
                  form, by injected humour, or randomised words which don't look
                  even slightly believable. Ipsum, you need to be sure there
                  isn't anything embarrassing hidden in the middle of text.
                </li>
                <li className="sm-text-fw4-g mb-3">
                  There are many variations of passagges of Lorem Ipsum
                  available, but the majority have suffered alteration in some
                  form, by injected humour, or randomised words which don't look
                  even slightly believable.es of Lorem Ipsum available, but the
                  majority have suffered alteration in some form, by injected
                  humour, or randomised words which don't look even slightly
                  believable.
                </li>
                <li className="sm-text-fw4-g mb-3">
                  If you are going to use a passage of Lorges of Lorem Ipsum
                  available, but the majority have suffered alteration in some
                  form, by injected humour, or randomised words which don't look
                  even slightly believable.em Ipsum, you need to be sure there
                  isn't anything embarrassing hidden in the middle of text.
                </li>
              </ul>
            </div>
            <div className="font-weight-bold mb-4 pb-2 text-uppercase">
              There are many variations of passages
            </div>
            <div className="tc-txt mb-5">
              <div className="sm-text-fw4-g mb-3">
                There are many variations of passages of Lorem Ipsum available,
                but the majority have suffered alteration in some form, by
                injected humour, or randomised words which don't look even
                slightly believable. If you are going to use a passage of Lorem
                Ipsum, you need to be sure there isn't anything embarrassing
                hidden in the middle of text.
              </div>
              <div className="sm-text-fw4-g mb-3">
                All the Lorem Ipsum generators on the Internet tend to repeat
                predefined chunks as necessary, making this the first true
                generator on the Internet. It uses a dictionary of over 200
                Latin words, combined with a handful of model sentence
                structures, to generate Lorem Ipsum which looks reasonable. The
                generated Lorem Ipsum is therefore.
              </div>
            </div>
          </div>
        </div>
        <Footer />
        <div className="ab-mf px-4 mb-4 d-block d-md-none">
          <FixedFooter />
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    shop: selectActiveShop(state),
    user: selectUser(state),
    shopData: selectShop(state),
  }),
  {
    getShopData,
  },
)(TermsConditions)
