import React, { Component } from 'react'
import Review from '../../profile/common/review/review'
import Header from '../../home/Header/Header'
import Img from './../../../images/eco-img.webp'
import CoverPhoto from '../../profile/common/coverPhoto'
import './../../profile/index.scss'
import UserInfo from '../../profile/common/userInfo/userInfo'
import Tab from '../tab/tab'
import Footer from '../../home/Footer/Footer'
import ShopInfo from '../../../components/ShopInfo'
import { connect } from 'react-redux'
import { selectActiveShop } from 'core/selectors/account'
import FixedFooter from '../../Common/fixedFooter'
import { selectUser } from 'core/selectors/user'
import { getShopPosts, getShopData } from 'core/actions/shopActionCreators'
import { selectShop, selectShopPosts } from 'core/selectors/shops'

class Reviews extends Component {
  state = {
    allPosts: [],
    limit: 100,
    offset: 0,
    setData: false,
  }

  componentDidMount = () => {
    // TODO need to make decision whether shop have post or not...
    const shopId = this.props.match.params.id
    const limit = this.state.limit
    const offset = this.state.offset
    this.props.getShopData(shopId)
    // this.props.getShopPosts(shopId, limit, offset)
  }
  render() {
    return (
      <div>
        <Header />
        <CoverPhoto src={Img} status="shop" />
        <div className="user-profile-cnt shop-profile-cnt mx-auto mb-5 pb-5">
          <ShopInfo shop={this.props.shopData} />
          <Tab url={this.props.location.pathname} />
          <Review />
        </div>
        <Footer />
        <div className="ab-mf px-4 mb-4 d-block d-md-none">
          <FixedFooter />
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    shop: selectActiveShop(state),
    user: selectUser(state),
    shopData: selectShop(state),
  }),
  {
    getShopData,
  },
)(Reviews)
