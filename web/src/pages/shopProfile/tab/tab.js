import React from 'react'
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router-dom'
import {
  ROUTE_SHOP_PROFILE_ABOUT,
  ROUTE_SHOP_PROFILE_POSTS,
  ROUTE_SHOP_PROFILE_PRODUCTS,
  ROUTE_SHOP_PROFILE_REVIEWS,
  ROUTE_SHOP_PROFILE_TERMS_CONDITIONS,
} from '../../../routes/constants'

const Tab = (props) => {
  return (
    <div>
      <ul className="nav nav-fill tab-nav">
        <li className="nav-item">
          <Link
            className={
              props.match.path == ROUTE_SHOP_PROFILE_PRODUCTS
                ? 'nav-link sm-text-fw6-g text-uppercase active'
                : 'nav-link sm-text-fw6-g text-uppercase'
            }
            to={ROUTE_SHOP_PROFILE_PRODUCTS.replace(
              ':id',
              props.match.params.id,
            )}
          >
            Products
          </Link>
        </li>
        <li className="nav-item">
          <Link
            className={
              props.match.path == ROUTE_SHOP_PROFILE_POSTS
                ? 'nav-link sm-text-fw6-g text-uppercase active'
                : 'nav-link sm-text-fw6-g text-uppercase'
            }
            to={ROUTE_SHOP_PROFILE_POSTS.replace(':id', props.match.params.id)}
          >
            Posts
          </Link>
        </li>

        <li className="nav-item">
          <Link
            className={
              props.match.path == ROUTE_SHOP_PROFILE_ABOUT
                ? 'nav-link sm-text-fw6-g text-uppercase active'
                : 'nav-link sm-text-fw6-g text-uppercase'
            }
            to={ROUTE_SHOP_PROFILE_ABOUT.replace(':id', props.match.params.id)}
          >
            About
          </Link>
        </li>

        <li className="nav-item">
          <Link
            className={
              props.match.path == ROUTE_SHOP_PROFILE_REVIEWS
                ? 'nav-link sm-text-fw6-g text-uppercase active'
                : 'nav-link sm-text-fw6-g text-uppercase'
            }
            to={ROUTE_SHOP_PROFILE_REVIEWS.replace(
              ':id',
              props.match.params.id,
            )}
          >
            Reviews
          </Link>
        </li>
        <li className="nav-item">
          <Link
            className={
              props.match.path == ROUTE_SHOP_PROFILE_TERMS_CONDITIONS
                ? 'nav-link sm-text-fw6-g text-uppercase active'
                : 'nav-link sm-text-fw6-g text-uppercase'
            }
            to={ROUTE_SHOP_PROFILE_TERMS_CONDITIONS.replace(
              ':id',
              props.match.params.id,
            )}
          >
            Terms <span className="d-none d-md-inline-block">& Conditions</span>
          </Link>
        </li>
      </ul>
    </div>
  )
}

export default withRouter(Tab)
