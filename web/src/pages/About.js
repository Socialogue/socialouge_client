/** @flow */
import React from 'react';

import Helmet from './Common/Helmet';
import type { RouteStaticContext } from '../TypeDefinition';

type Props = {
  staticContext: ?RouteStaticContext
};

/* eslint-disable max-len */
const About = ({ staticContext }: Props) => (
  <div className='static-pages'>
    <Helmet
      description='about'
      staticContext={staticContext}
      title='about'
    />

    <p>about</p>
  </div>
);

export default About;

const styles = {
  background: '#000000 url(img/about-bg.jpg) no-repeat top right',
  backgroundSize: 'cover',
  minHeight: '60vh'
};
