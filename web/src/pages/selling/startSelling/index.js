import React, { Component } from "react";
import ArrowDown from "../../../images/icons/arrowDown";
import { ROUTE_SELLING_APPLY_SHOP } from "../../../routes/constants";
import FixedFooter from "../../Common/fixedFooter";
import Footer from "../../home/Footer/Footer";
import Header from "../../home/Header/Header";
import Img from "./../../../images/eco-img.webp";
import Img1 from "./../../../images/images.webp";
class StartSelling extends Component {
  applyForShop = () => {
    this.props.history.push(ROUTE_SELLING_APPLY_SHOP);
  };

  render() {
    return (
      <div>
        <Header />
        <div className="max-content-width-1 py-3 py-xl-5 my-3 my-xl-5 px-4">
          <div className="x-md-title border-bottom-1 pb-3 pb-md-4 mb-4 mb-md-5">
            start selling <span className="dot-r" />
          </div>
          <div className="row">
            <div className="col-12 col-md-7" />
            <div className="col-12 col-md-5">
              <div className="d-flex align-items-center mb-2 mb-md-5 pb-3">
                <span className="sm-text-fw6 text-uppercase">
                  selling <br /> information
                </span>
                <span className="ml-2 sel-svg">
                  <ArrowDown />
                </span>
              </div>
            </div>
            <div className="col-12 col-md-7 d-none d-md-block">
              <div className="mr-5">
                <div className="my-5 pt-5 x-md-text-fw6 text-uppecase font-weight-bold">
                  MORE THAN 100 + <br /> SHOPS CHOOSE US
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-12 col-md-7">
              <div className="mr-0 mr-md-5">
                <div className="sel-img pr-0 pr-md-5 mr-0 mr-md-5 mb-5">
                  <img className="cover" src={Img} />
                </div>
                {/* for desktop start */}
                <div className="pl-5 ml-5 d-none d-md-block">
                  <div className="show-img-pp show-img-pp-pb-110">
                    <div className="show-img-p">
                      <img className="cover" src={Img} />
                    </div>
                  </div>
                </div>
                {/* for desktop end */}
              </div>
            </div>
            <div className="col-12 col-md-5">
              <div className="mb-5 pb-3">
                <div className="md-title mb-3"> sell on socialogue </div>
                <div className="x-sm-text-fw4-g mb-4">
                  Simply dummy text of the printing and typesetting
                  industry.Lorem Ipsum has been the industry 's standard dummy
                  text ever since the 1500 s, when an unknown printer took a
                  galley of type and scrambled it to make a type specimen book.
                </div>
                <div className="x-sm-text-fw4-g mb-0 mb-md-4">
                  It has survived not only five centuries, but also the leap
                  into electronic typesetting, remaining essentially
                  unchanged.It was popularised in the 1960 s with the release of
                  Letraset sheets containing Lorem Ipsum passages, and more
                  recently with desktop publishing software like Aldus PageMaker
                  including versions of Lorem Ipsum.
                </div>
                {/* for mobile start */}
                <div className="text-right d-block d-none position-relative">
                  <img className="m-seling-img" src={Img} />
                </div>
                {/* for mobile end */}
              </div>
              <div className="mb-0 mb-md-5 pb-3">
                <div className="md-title mb-3"> benefits </div>
                <div className="x-sm-text-fw4-g mb-0 mb-md-4">
                  Simply dummy text of the printing and typesetting
                  industry.Lorem Ipsum has been the industry 's standard dummy
                  text ever since the 1500 s, when an unknown printer took a
                  galley of type and scrambled it to make a type specimen book.
                </div>
              </div>
              <div className="mb-5">
                <div className="md-title mb-3"> principles </div>
                <div className="x-sm-text-fw4-g mb-4">
                  It has survived not only five centuries, but also the leap
                  into electronic typesetting, remaining essentially unchanged.
                  It was popularised in the 1960s with the release of Letraset
                  sheets containing Lorem Ipsum passages, and more recently with
                  desktop publishing software like Aldus PageMaker including
                  versions of Lorem Ipsum.
                </div>
              </div>
              <div className="text-right mb-5 mb-md-3">
                <button className="btn border-btn" onClick={this.applyForShop}>
                  Apply for shop
                </button>
              </div>
            </div>
          </div>
        </div>
        <Footer />
        <div className="ab-mf px-4 mb-4 d-block d-md-none">
          <FixedFooter />
        </div>
      </div>
    );
  }
}

export default StartSelling;
