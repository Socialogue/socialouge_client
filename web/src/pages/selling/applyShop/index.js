import { selectErrorMsg, selectSuccessMsg } from 'core/selectors/shops'
import { selectUser } from 'core/selectors/user'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import Header from '../../home/Header/Header'
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import HorizontalInput from './../../../pages/userSettings/components/horizontalInput/index'
import HorizontalSelectOption from './../../userSettings/components/horizontalSelectOption'
import CustomSelectOption from './../../userSettings/components/horizontalSelectOption/customSelectOption'
import UploadPhoto from './uploadPhoto'
import { applyForShop } from 'core/actions/shopActionCreators'
import { errorAlert, successAlert } from '../../../utils/alerts'
import ExcB from '.././../../images/icons/excIcon'
import { ROUTE_HOME } from '../../../routes/constants'
import Footer from '../../home/Footer/Footer'
import FixedFooter from '../../Common/fixedFooter'

class ApplyShop extends Component {
  constructor() {
    super()
    this.state = {
      shopName: '',
      itemForSell: 'My own design',
      countryCode: '',
      phone: '',
      companyName: '',
      companyId: '',
      companyVat: '',
      companyEmail: '',
      registeredCompanyAddress: '',
      socialMedia1: '',
      socialMedia2: '',
      whereDidYouHere: 'Google',
    }
  }
  onInputChange = (name, value) => {
    this.setState({
      [name]: value,
    })
  }

  handleCountryCode = (name, value, index) => {
    this.setState({
      countryCode: value,
    })
  }

  handlePhone = (e) => {
    const name = e.target.name
    const value = this.state.countryCode + e.target.value

    this.setState({
      [name]: value,
    })
  }

  onSubmit = async (e) => {
    if (
      this.state.shopName == '' ||
      this.state.itemForSell == '' ||
      this.state.link == '' ||
      this.state.companyName == '' ||
      this.state.companyId == '' ||
      this.state.companyEmail == '' ||
      this.state.registeredCompanyAddress == '' ||
      this.state.socialMedia1 == '' ||
      this.state.whereDidYouHere == ''
    ) {
      errorAlert('Please fillup all required field')
      return
    }
    try {
      await this.props.applyForShop(this.state, this.props.userId._id)
    } catch (e) {
      console.log('catch', e)
    }
    return true
  }

  componentDidUpdate = (prevProps) => {
    if (prevProps.successMsg !== this.props.successMsg) {
      if (this.props.successMsg !== '') {
        successAlert(this.props.successMsg)
        this.props.history.push(ROUTE_HOME)
      }
    }
    if (prevProps.errorMsg !== this.props.errorMsg) {
      if (this.props.errorMsg !== '') {
        errorAlert(this.props.errorMsg)
      }
    }
  }

  render() {
    return (
      <div>
        <Header />
        <div className="max-content-width-1 py-3 py-xl-5 my-3 my-xl-5 px-4">
          <div className="x-md-title border-bottom-1 pb-3 pb-md-4 mb-3 mb-md-5">
            apply for a shop
            <span className="dot-r" />
          </div>
          <div className="pvf-cnt">
            <div className="md-title md-title-res mb-5">
              tell us a bit about your shop
            </div>
            <div className="apply-form">
              <div className="mb-5">
                <HorizontalInput
                  formInputClass="form-input position-relative"
                  label="your shop name"
                  errorText="Please enter Your shop name"
                  isRequired={true}
                  type="text"
                  className="form-control custom-input"
                  placeholder="Enter your shop name"
                  name="shopName"
                  onChange={this.onInputChange}
                />
              </div>
              <div className="mb-5">
                <HorizontalSelectOption
                  formInputClass="form-input position-relative"
                  label="what items do you sell"
                  name="itemForSell"
                  errorText=""
                  isRequired={true}
                  imageRequired={false}
                  selectedText="My own design"
                  options={['My own design', 'others design']}
                  onChange={this.onInputChange}
                />
              </div>
              <div className="form-group row align-items-center mr-0 mb-5">
                <label className="x-sm-text-fw6 text-uppercase col-12 col-md-5 pr-0 mb-0">
                  Your Contact Number<sup>*</sup>
                </label>
                <div className="input-group sc-input apply-sc col-12 col-md-7 pr-0">
                  <span
                    className="prepend-input"
                    style={{ marginLeft: '15px' }}
                  >
                    <ExcB />
                  </span>
                  <PhoneInput
                    country={'lt'}
                    inputStyle={{
                      border: '1px solid black',
                      borderTop: 'none',
                      borderRight: 'none',
                      width: '390px',
                      marginLeft: '5px',
                      borderRadius: '0',
                      outerWidth: '100%',
                    }}
                    className="form-control custom-input"
                    buttonStyle={{
                      border: 'none',
                      backgroundColor: 'white',
                      borderRight: '1px solid black',
                      borderBottom: '1px solid black',
                      borderRadius: '0',
                    }}
                    value={this.state.phone}
                    placeholder="Enter your phone number"
                    onChange={(phone) => this.setState({ phone })}
                  />
                </div>
              </div>
              <div className="mb-5">
                <HorizontalInput
                  formInputClass="form-input position-relative"
                  label="Company Name"
                  errorText="Please enter Your company name"
                  isRequired={true}
                  type="text"
                  className="form-control custom-input"
                  placeholder=""
                  name="companyName"
                  onChange={this.onInputChange}
                />
              </div>
              <div className="mb-5">
                <HorizontalInput
                  formInputClass="form-input position-relative"
                  label="Company Id"
                  errorText="Please enter Your company Id"
                  isRequired={true}
                  type="text"
                  className="form-control custom-input"
                  placeholder="DE12823"
                  name="companyId"
                  onChange={this.onInputChange}
                />
              </div>
              <div className="mb-5">
                <HorizontalInput
                  formInputClass="form-input position-relative"
                  label="Company Vat"
                  type="text"
                  className="form-control custom-input"
                  placeholder=""
                  name="companyVat"
                  onChange={this.onInputChange}
                />
              </div>
              <div className="mb-5">
                <HorizontalInput
                  formInputClass="form-input position-relative"
                  label="Company Email Address"
                  errorText="Please enter Your company email address"
                  isRequired={true}
                  type="text"
                  className="form-control custom-input"
                  placeholder="Name@domain.com"
                  name="companyEmail"
                  onChange={this.onInputChange}
                  validations={['email']}
                />
              </div>
              <div className="mb-5">
                <HorizontalInput
                  formInputClass="form-input position-relative"
                  label="Registered Company Address"
                  errorText="Please enter Your registered company address"
                  isRequired={true}
                  type="text"
                  className="form-control custom-input"
                  placeholder="Little George St, Westminstr, London"
                  name="registeredCompanyAddress"
                  onChange={this.onInputChange}
                />
              </div>
              <div className="mb-5">
                <HorizontalInput
                  formInputClass="form-input position-relative"
                  label="social media"
                  errorText="Please enter a link"
                  isRequired={true}
                  type="text"
                  className="form-control custom-input"
                  placeholder="Enter Links"
                  name="socialMedia1"
                  onChange={this.onInputChange}
                />
              </div>
              <div className="mb-5">
                <HorizontalInput
                  formInputClass="form-input position-relative"
                  label=""
                  type="text"
                  className="form-control custom-input"
                  placeholder="Enter Links"
                  name="socialMedia2"
                  onChange={this.onInputChange}
                />
              </div>
              <div className="mb-5">
                <HorizontalSelectOption
                  formInputClass="form-input position-relative"
                  label="Where did you here about us"
                  name="whereDidYouHere"
                  errorText=""
                  isRequired={true}
                  imageRequired={false}
                  selectedText="Google"
                  options={['Google', 'Facebook', 'Linkedin']}
                  onChange={this.onInputChange}
                />
              </div>
              <div className="mb-5 text-right">
                <button className="btn border-btn " onClick={this.onSubmit}>
                  Submit
                </button>
              </div>
            </div>
          </div>
        </div>
        <Footer />
        <div className="ab-mf px-4 mb-4 d-block d-md-none">
          <FixedFooter />
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    userId: selectUser(state),
    successMsg: selectSuccessMsg(state),
    errorMsg: selectErrorMsg(state),
  }),
  {
    applyForShop,
  },
)(ApplyShop)
