import React, { Component } from "react";
import Delete from "./../../../../images/icons/delete";
import TopArrow from "./../../../../images/icons/topArrow";
class UploadPhoto extends Component {
  render() {
    return (
      <div className="col-12 col-md-6 pr-0 mb-4 pb-2">
        <div className="sm-text-fw6 mb-2 text-uppercase">Custom name</div>
        <div className="show-img-pp show-img-pp-pb-90 upload-img">
          <div className="upload-photo-cnt d-flex-box">
            <div className="upload-photo d-flex-box">
              <div className="upload-photo-txt text-center">
                <div className="upload-icon mb-3">
                  <TopArrow />
                </div>
                <input
                  type="file"
                  className="ctm-hidden-upload-input cover"
                  multiple
                  onChange={this.props.handleImage}
                />
                <div className=" x-sm-text-fw4-g">Upload photo</div>
              </div>
            </div>
          </div>
        </div>
        <div className="x-sm-text-fw6 mt-2 text-uppercase ">
          <span className="d-inline-flex align-items-center cursor-pointer">
            <span className="mr-1 d-flex">
              <Delete />
            </span>
            Delete
          </span>
        </div>
      </div>
    );
  }
}

export default UploadPhoto;
