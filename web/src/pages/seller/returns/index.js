import React, { Component } from "react";
import Down from "../../../images/icons/down";
import Header from "../../home/Header/Header";
import SideNav from "../common/sideNav";
import "./../seller.scss";
import Order from "./../orders/components/order";
import search_icon from "./../../../images/icons/search-icon.svg";
import Img from "./../../../images/eco-img.webp";

class Returns extends Component {
  state = {
    markAs: false,
    status: false
  };
  render() {
    return (
      <div>
        <Header />
        <div className="seller-section d-md-flex px-4 mx-2 py-4 py-md-5">
          <SideNav />
          <div className="seller-cnt pt-2 pt-md-0">
            <div className="mb-5">
              <div className="s-top-tlt mb-4">
                <span className="font-weight-bold text-uppercase">
                  Orders (10)
                </span>
              </div>
              <div className="mb-3">
                <label className="ctm-container d-inline-block">
                  Select All
                  <input type="checkbox" name="selectAll" />
                  <span className="checkmark" />
                </label>
                <span
                  className={
                    this.state.markAs
                      ? "d-inline-block position-relative box-show ml-5 active"
                      : "d-inline-block position-relative box-show ml-5"
                  }
                >
                  <span
                    onClick={() =>
                      this.setState({
                        markAs: !this.state.markAs
                      })
                    }
                    className="d-flex cursor-pointer sm-text-fw6"
                  >
                    <span className="">Marks as</span>
                    <span className="ml-1">
                      <Down />
                    </span>
                  </span>
                  <div className="box-cnt2">
                    <div className="box-ul">
                      <div className="box-li">
                        <label className="ctm-container d-inline-block">
                          Delivered All
                          <input type="checkbox" name="selectAll" />
                          <span className="checkmark" />
                        </label>
                      </div>
                      <div className="box-li">
                        <label className="ctm-container d-inline-block">
                          open returns
                          <input type="checkbox" name="selectAll" />
                          <span className="checkmark" />
                        </label>
                      </div>
                      <div className="box-li">
                        <label className="ctm-container d-inline-block">
                          Shipped
                          <input type="checkbox" name="selectAll" />
                          <span className="checkmark" />
                        </label>
                      </div>
                      <div className="box-li">
                        <label className="ctm-container d-inline-block">
                          Closed
                          <input type="checkbox" name="selectAll" />
                          <span className="checkmark" />
                        </label>
                      </div>
                      <div className="box-li">
                        <label className="ctm-container d-inline-block">
                          Open replacements
                          <input type="checkbox" name="selectAll" />
                          <span className="checkmark" />
                        </label>
                      </div>
                      <div className="box-li">
                        <label className="ctm-container d-inline-block">
                          Open returns
                          <input type="checkbox" name="selectAll" />
                          <span className="checkmark" />
                        </label>
                      </div>
                      <div className="box-li">
                        <label className="ctm-container d-inline-block">
                          Shipped
                          <input type="checkbox" name="selectAll" />
                          <span className="checkmark" />
                        </label>
                      </div>
                    </div>
                    <button className="btn border-btn w-100 black-bg-btn text-uppercase">
                      mark
                    </button>
                  </div>
                </span>
              </div>{" "}
              <div className="seller-table">
                <div className="tbl-cnt">
                  {/* only mobile */}
                  <div className="tbl-1 d-block d-md-none">
                    <table class="table">
                      <thead>
                        <tr>
                          <th className="sm-text-fw6-g text-uppercase" />
                          <th className="sm-text-fw6-g text-uppercase photo-td">
                            Photo
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {/* loop */}
                        <tr className="">
                          {/* when item select that time will be add active class in tr */}
                          <td className="sm-text-fw6 text-uppercase td-border px-0 f-td">
                            <label className="ctm-container d-inline-block">
                              <input type="checkbox" name="selectTr[]" />
                              <span className="checkmark" />
                            </label>
                          </td>
                          <td className="sm-text-fw6 text-uppercase td-border">
                            <img src={Img} width="27" height="36" alt="" />
                          </td>
                        </tr>
                        <tr className="">
                          {/* when item select that time will be add active class in tr */}
                          <td className="sm-text-fw6 text-uppercase td-border px-0 f-td">
                            <label className="ctm-container d-inline-block">
                              <input type="checkbox" name="selectTr[]" />
                              <span className="checkmark" />
                            </label>
                          </td>
                          <td className="sm-text-fw6 text-uppercase td-border">
                            <img src={Img} width="27" height="36" alt="" />
                          </td>
                        </tr>

                        {/* loop */}
                      </tbody>
                    </table>
                  </div>
                  {/* only mobile */}

                  <div className="tbl-2">
                    <table class="table">
                      <thead>
                        <tr>
                          <th className="sm-text-fw6-g text-uppercase  d-none d-md-table-cell" />
                          <th className="sm-text-fw6-g text-uppercase photo-td  d-none d-md-table-cell">
                            Photo
                          </th>
                          <th className="sm-text-fw6-g text-uppercase name-td text-nowrap">
                            Product name
                          </th>
                          <th className="sm-text-fw6-g text-uppercase">Cost</th>
                          <th
                            className={
                              this.state.status
                                ? "position-relative box-show active"
                                : "position-relative box-show"
                            }
                          >
                            <span
                              onClick={() =>
                                this.setState({
                                  status: !this.state.status
                                })
                              }
                              className="d-inline-flex cursor-pointer sm-text-fw6-g text-uppercase"
                            >
                              <span className="">Status</span>
                              <span className="ml-1 d-none d-md-inline-block">
                                <Down />
                              </span>
                            </span>
                            <div className="box-cnt2">
                              <div className="box-ul">
                                <div className="box-li">
                                  <label className="ctm-container d-inline-block">
                                    All Orders
                                    <input type="checkbox" name="selectAll" />
                                    <span className="checkmark" />
                                  </label>
                                </div>
                                <div className="box-li">
                                  <div className="input-group search-group">
                                    <div className="input-group-prepend">
                                      <span className="input-group-text">
                                        <img src={search_icon} alt="img" />
                                      </span>
                                    </div>
                                    <input
                                      type="text"
                                      className="form-control custom-input"
                                      placeholder="Search"
                                    />
                                  </div>
                                </div>
                                <div className="box-li">
                                  <label className="ctm-container d-inline-block">
                                    Delivered (3)
                                    <input type="checkbox" name="selectAll" />
                                    <span className="checkmark" />
                                  </label>
                                </div>
                                <div className="box-li">
                                  <label className="ctm-container d-inline-block">
                                    All open returns (3)
                                    <input type="checkbox" name="selectAll" />
                                    <span className="checkmark" />
                                  </label>
                                </div>
                                <div className="box-li">
                                  <label className="ctm-container d-inline-block">
                                    Closed (0)
                                    <input type="checkbox" name="selectAll" />
                                    <span className="checkmark" />
                                  </label>
                                </div>
                                <div className="box-li">
                                  <label className="ctm-container d-inline-block">
                                    Open replacements (0)
                                    <input type="checkbox" name="selectAll" />
                                    <span className="checkmark" />
                                  </label>
                                </div>
                              </div>
                              <button className="btn border-btn w-100 black-bg-btn text-uppercase">
                                filter
                              </button>
                            </div>
                          </th>
                          <th className="sm-text-fw6-g text-uppercase">
                            Model
                          </th>
                          <th className="sm-text-fw6-g text-uppercase">QTY</th>
                          <th className="sm-text-fw6-g text-uppercase">Date</th>
                          <th className="sm-text-fw6-g text-uppercase">
                            Customer
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <Order />
                        <Order />
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Returns;
