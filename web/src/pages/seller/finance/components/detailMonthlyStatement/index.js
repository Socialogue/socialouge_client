import React, { Component } from "react";
import ArrowLeft from "../../../../../images/icons/arrowLeft";
import Excla from "../../../../../images/icons/excla";
import CustomSelectOption from "../../../../userSettings/components/horizontalSelectOption/customSelectOption";

class DetailMonthlyStatement extends Component {
  onInputChange = (name, value) => {
    this.setState({
      [name]: value
    });
  };
  render() {
    return (
      <div className="">
        <div className="mb-4 mb-md-5">
          <button className="btn border-btn p-0 border-0">
            <ArrowLeft /> Back to finances
          </button>
        </div>
        <div className="font-weight-bold text-uppercase mb-3">
          Detailed monthly statement
        </div>
        <div className="mb-4 font-weight-bold no-border-select d-flex text-uppercase">
          <div className="mr-5">
            <CustomSelectOption
              imageRequired={false}
              selectedText="2021"
              label="year"
              name="year"
              options={["2021", "2020", "2020", "2020", "2020", "2020", "2020"]}
              onChange={this.onInputChange}
            />
          </div>
          <div className="">
            <CustomSelectOption
              imageRequired={false}
              selectedText="february"
              label="month"
              name="month"
              options={["february", "february", "february"]}
              onChange={this.onInputChange}
            />
          </div>
        </div>
        <div className="dt-m-table">
          <table className="table table-bordered silver-border">
            <tbody>
              <tr>
                <td className="p-3 p-md-4">
                  <div className="font-weight-bold text-uppercase mb-2">
                    Sales
                  </div>
                  <div className="font-weight-6 md-title md-title-res mb-3 mb-md-4 d-flex align-items-center text-nowrap">
                    € 143.43{" "}
                    <span className="pl-2 d-inline-flex">
                      <Excla />
                    </span>
                  </div>
                  <div className="sm-text-fw4-g">
                    EUR 0.00 exluding categories where refunds exceeded income
                  </div>
                </td>
                <td className="p-3 p-md-4">
                  <div className="font-weight-bold text-uppercase mb-2">
                    Fees and taxes
                  </div>
                  <div className="font-weight-6 md-title md-title-res mb-3 mb-md-4 d-flex align-items-center text-nowrap">
                    € 143.43{" "}
                    <span className="pl-2 d-inline-flex">
                      <Excla />
                    </span>
                  </div>
                  <div className="sm-text-fw4-g">
                    EUR 0.00 exluding categories where refunds exceeded income
                  </div>
                </td>
              </tr>
              <tr>
                <td className="p-3 p-md-4 x-sm-text-fw6 sale-td" colSpan="2">
                  <div className="row">
                    <div className="col-8 col-md-9">Sale 1</div>
                    <div className="col-4 col-md-3 text-center">EUR 0.00</div>
                  </div>
                  <div className="row">
                    <div className="col-8 col-md-9">Sale 1</div>
                    <div className="col-4 col-md-3 text-center">EUR 0.00</div>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="order-d-cnt mb-3 due-amnt">
          <div className="py-4 d-flex align-items-center">
            <span className="">
              <span className="font-weight-bold mr-4 mr-md-5 text-uppercase text-nowrap">
                VAT statement
              </span>
              <br className="d-block d-lg-none" />
              <span className="font-weight-5 text-nowrap">February 2021</span>
            </span>
            <span className="w-100 text-right">
              <button className="btn border-btn px-4 px-md-5">Download</button>
            </span>
          </div>
        </div>
      </div>
    );
  }
}

export default DetailMonthlyStatement;
