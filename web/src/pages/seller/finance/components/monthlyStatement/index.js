import React, { Component } from "react";
import ArrowDown from "../../../../../images/icons/arrowDown";
import CustomSelectOption from "../../../../userSettings/components/horizontalSelectOption/customSelectOption";

class MonthlyStatement extends Component {
  onInputChange = (name, value) => {
    this.setState({
      [name]: value
    });
  };
  render() {
    return (
      <div className="">
        <div className="order-d-cnt mb-3 due-amnt">
          <div className="py-3 py-md-4 d-flex align-items-center">
            <span className="">
              <span className="font-weight-bold mr-4 mr-md-5 text-uppercase text-nowrap">
                Ammount due april
              </span>
              <br className="d-block d-lg-none" />
              <span className="font-weight-5 text-nowrap">
                EUR 0.38 Due April
              </span>
            </span>
            <span className="w-100 text-right">
              <button className="btn border-btn px-4 px-md-5">Pay Now</button>
            </span>
          </div>
        </div>
        <div className="order-d-cnt mb-3">
          <div className="mb-3 mb-md-2 md-title md-title-res font-weight-6">
            Monthly statements
          </div>
          <div className="mb-3 font-weight-bold no-border-select">
            <CustomSelectOption
              imageRequired={false}
              selectedText="2021"
              label="year"
              name="year"
              options={["2021", "2020", "2020", "2020", "2020", "2020", "2020"]}
              onChange={this.onInputChange}
            />
          </div>
          <div className="st-table">
            <table className="table">
              <tbody>
                <tr>
                  <td className="sm-text-fw6-g text-uppercase text-left pl-0 border-0 w-50">
                    Month
                  </td>
                  <td className="sm-text-fw6-g text-uppercase text-center border-0 w-35">
                    Sales{" "}
                  </td>
                  <td className="sm-text-fw6-g text-uppercase text-center border-0 text-nowrap">
                    Fees and taxes
                  </td>
                </tr>
                <tr>
                  <td className="pl-0 font-weight-5 text-left border-0">
                    April 2021
                  </td>
                  <td className="font-weight-5 text-center text-uppercase border-0 text-nowrap">
                    EUR 0.00
                  </td>
                  <td className="font-weight-5 text-center text-uppercase border-0 text-nowrap">
                    EUR 0.00
                  </td>
                </tr>
                <tr>
                  <td className="pl-0 font-weight-5 text-left border-0">
                    April 2021
                  </td>
                  <td className="font-weight-5 text-center text-uppercase border-0 text-nowrap">
                    EUR 0.00
                  </td>
                  <td className="font-weight-5 text-center text-uppercase border-0 text-nowrap">
                    EUR 0.00
                  </td>
                </tr>
                <tr>
                  <td className="pl-0 font-weight-5 text-left border-0">
                    April 2021
                  </td>
                  <td className="font-weight-5 text-center text-uppercase border-0 text-nowrap">
                    EUR 0.00
                  </td>
                  <td className="font-weight-5 text-center text-uppercase border-0 text-nowrap">
                    EUR 0.00
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div className="text-center my-4 pt-1">
          <button className="btn border-btn p-0 border-0">
            Shoe more... <br />
            <ArrowDown />
          </button>
        </div>
      </div>
    );
  }
}

export default MonthlyStatement;
