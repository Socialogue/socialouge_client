import React, { Component } from "react";
import Header from "../../home/Header/Header";
import SideNav from "../common/sideNav";
import "./../seller.scss";
import DetailMonthlyStatement from "./components/detailMonthlyStatement";
import MonthlyStatement from "./components/monthlyStatement";

class Finance extends Component {
  state = {
    statementShow: true,
    statementDetailsShow: false
  };
  render() {
    return (
      <div>
        <Header />
        <div className="seller-section d-md-flex px-4 mx-0 mx-lg-2 py-4 py-md-5">
          <SideNav />
          <div className="seller-cnt">
            {this.state.statementShow && <MonthlyStatement />}
            {this.state.statementDetailsShow && <DetailMonthlyStatement />}
          </div>
        </div>
      </div>
    );
  }
}

export default Finance;
