import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import Doller from '../../../../images/icons/doller'
import List from '../../../../images/icons/list'
import Return from '../../../../images/icons/return'
import Right from '../../../../images/icons/right'
import Shop from '../../../../images/icons/shop'
import {
  ROUTE_SELLER_FINANCE,
  ROUTE_SELLER_ORDERS,
  ROUTE_SELLER_PRODUCTS_LIST,
  ROUTE_SELLER_RETURNS,
} from '../../../../routes/constants'

class SideNav extends Component {
  render() {
    const { match } = this.props

    if (!match) {
      return null
    }
    return (
      <div className="seller-ul md-w-100">
        <nav class="nav flex-column d-none d-md-flex">
          <Link
            className={
              match.path == ROUTE_SELLER_ORDERS
                ? 'nav-link sm-text-fw6-g text-uppercase active '
                : 'nav-link sm-text-fw6-g text-uppercase'
            }
            to={ROUTE_SELLER_ORDERS}
          >
            <span className="li-icons">
              <Shop />
            </span>{' '}
            Orders{' '}
            <span className="ar-ri">
              <Right />
            </span>
          </Link>
          <Link
            className={
              match.path == ROUTE_SELLER_PRODUCTS_LIST
                ? 'nav-link sm-text-fw6-g text-uppercase active '
                : 'nav-link sm-text-fw6-g text-uppercase'
            }
            to={ROUTE_SELLER_PRODUCTS_LIST}
          >
            <span className="li-icons">
              <List />
            </span>{' '}
            listings{' '}
            <span className="ar-ri">
              <Right />
            </span>
          </Link>
          <Link
            className={
              match.path == ROUTE_SELLER_RETURNS
                ? 'nav-link sm-text-fw6-g text-uppercase active '
                : 'nav-link sm-text-fw6-g text-uppercase'
            }
            to="/seller/returns"
          >
            <span className="li-icons">
              <Return />
            </span>{' '}
            Returns{' '}
            <span className="ar-ri">
              <Right />
            </span>
          </Link>
          <Link
            className={
              match.path == ROUTE_SELLER_FINANCE
                ? 'nav-link sm-text-fw6-g text-uppercase active '
                : 'nav-link sm-text-fw6-g text-uppercase'
            }
            to="/seller/finance"
          >
            <span className="li-icons">
              <Doller />
            </span>{' '}
            Finances{' '}
            <span className="ar-ri">
              <Right />
            </span>
          </Link>
          <Link
            className="nav-link sm-text-fw6-g text-uppercase"
            to="/create-shop"
          >
            <span className="li-icons">
              <Shop />
            </span>{' '}
            Settings{' '}
            <span className="ar-ri">
              <Right />
            </span>
          </Link>
        </nav>

        {/* for mobile  */}
        <div className="d-block d-md-none mb-4">
          <ul className="nav nav-fill tab-nav">
            <li className="nav-item text-left">
              <Link
                className={
                  match.path == ROUTE_SELLER_ORDERS
                    ? "nav-link sm-text-fw6-g text-uppercase active "
                    : "nav-link sm-text-fw6-g text-uppercase"
                }
                to={ROUTE_SELLER_ORDERS}
              >
                orders
              </Link>
            </li>
            <li className="nav-item">
              <Link
                className={
                  match.path == ROUTE_SELLER_PRODUCTS_LIST
                    ? "nav-link sm-text-fw6-g text-uppercase active "
                    : "nav-link sm-text-fw6-g text-uppercase"
                }
                to={ROUTE_SELLER_PRODUCTS_LIST}
              >
                listings
              </Link>
            </li>
            <li className="nav-item">
              <Link
                className={
                  match.path == ROUTE_SELLER_RETURNS
                    ? "nav-link sm-text-fw6-g text-uppercase active "
                    : "nav-link sm-text-fw6-g text-uppercase"
                }
                to={ROUTE_SELLER_RETURNS}
              >
                returns
              </Link>
            </li>
            <li className="nav-item text-right">
              <Link
                className={
                  match.path == ROUTE_SELLER_FINANCE
                    ? "nav-link sm-text-fw6-g text-uppercase active "
                    : "nav-link sm-text-fw6-g text-uppercase"
                }
                to={ROUTE_SELLER_FINANCE}
              >
                finances
              </Link>
            </li>
          </ul>
        </div>
        {/* for mobile  */}
      </div>
    )
  }
}

export default withRouter(SideNav)
