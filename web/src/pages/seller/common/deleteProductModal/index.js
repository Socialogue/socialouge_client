import React, { Component } from "react";

class DeleteProductModal extends Component {
  render() {
    return (
      <div
        className="modal modal-1 d-block show"
        id="deleteProductModal"
        tabindex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-body">
              <div className="sm-text-fw6 text-uppercase mb-3">
                Are you sure you want to{" "}
                <span className="color-orange"> delete</span> 9 Products?
              </div>
              <div className="sm-text-fw5-g mb-5 pr-5">
                There are many variations of passages of Lorem Ipsum available,
                but the majority have suffered alteration.
              </div>
              <div className="d-flex align-items-center">
                <button className="btn border-btn btn-w-115">No</button>

                <div className="w-100 text-right x-sm-text-fw6">
                  <button className="btn border-btn btn-w-115 border-0">
                    Yes, I am Sure
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DeleteProductModal;
