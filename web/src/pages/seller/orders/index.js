import React, { Component } from "react";
import Down from "../../../images/icons/down";
import Header from "../../home/Header/Header";
import SideNav from "../common/sideNav";
import "./../seller.scss";
import OrderDetails from "./components/orderDetails";
import OrderMonthTable from "./components/orderMonthTable";
import OrderTable from "./components/orderTable";
import { connect } from "react-redux";
import {
  fetchAllOrders,
  orderStatusChange
} from "core/actions/dashboardActionCreators";
import { selectActiveShop } from "core/selectors/account";

import Paginate from "../../../components/paginate";
import {
  selectOrders,
  selectHasNextPage,
  selecthasPrevPage,
  selectLimit,
  selectNextPage,
  selectPage,
  selectPagingCounter,
  selectPrevPage,
  selectProductDeleteStatus,
  selectProducts,
  selectTotalDocs,
  selectTotalPages,
  selectOrderStatusChange
} from "core/selectors/dashboard";

class Orders extends Component {
  state = {
    page: 1,
    limit: 10,
    orders: [],
    orderIds: [],
    orderHave: false,
    fetchAllOrders2: true
  };
  componentDidMount = () => {
    const { page, limit } = this.state;
    this.props.fetchAllOrders(this.props.activeShop._id, page, limit);
  };

  componentDidUpdate = prevProps => {
    if (this.props.changeStatus && this.state.fetchAllOrders2) {
      const { page, limit } = this.state;
      this.props.fetchAllOrders(this.props.activeShop._id, page, limit);
      this.setState({ fetchAllOrders2: false });
    }
  };

  onSelectOrder = e => {
    let ids = [...this.state.orderIds];
    const value = e.target.value;
    if (ids.find(id => id == value)) {
      ids = ids.filter(id => id !== value);
      this.setState({ orderIds: ids });
    } else {
      ids = [...ids, value];
      this.setState({ orderIds: ids });
    }
    
    // code of kowser
    var v1;
    //
    const tbl = e.target.closest(".table").querySelectorAll(".pro-tr");
    tbl.forEach(function(userItem, i) {
      if (userItem.classList.contains("index")) {
        userItem.classList.remove("index");
      }
    });
    e.target.closest(".pro-tr").classList.add("index");
    tbl.forEach(function(userItem, i) {
      if (userItem.classList.contains("index")) {
        v1 = i;
      }
    });
    const tbl1 = document.querySelector(".tbl-1").querySelectorAll(".pro-tr");
    const tbl2 = document.querySelector(".tbl-2").querySelectorAll(".pro-tr");
    tbl1[v1].classList.toggle("active");
    tbl2[v1].classList.toggle("active");
    if (
      e.target.closest(".table").querySelectorAll(".active").length ==
      tbl.length
    ) {
      document.querySelector(".all-check").classList.add("ctm-active");
    } else {
      document.querySelector(".all-check").classList.remove("ctm-active");
    }
    // code of kowser
  };

  handlePageClick = e => {
    const { limit } = this.state;
    const page = +e.selected + 1;
    // const skip = (page -1) * limit;
    this.setState({ page: page });
    this.props.fetchAllOrders(this.props.activeShop._id, page, limit);
  };

  pageLabelBuilder = e => {
    // console.log("pageLabelBuilder: ", e);
    return e;
  };

  // handler for select all ...
  onSeletcAll = e => {
    let ids = [];

    if (this.state.orderIds.length == this.props.orders.length) {
      ids = [];
    } else {
      this.props.orders.map(order => {
        ids.push(order._id);
      });
    }

    this.setState({ orderIds: ids });
    // code of kowser
    e.target.closest(".ctm-container").classList.toggle("ctm-active");
    if (e.target.closest(".ctm-container").classList.contains("ctm-active")) {
      document.querySelectorAll(".pro-tr").forEach(function(userItem) {
        userItem.classList.add("active");
      });
    } else {
      document.querySelectorAll(".pro-tr").forEach(function(userItem) {
        userItem.classList.remove("active");
      });
    }
    // code of kowser
  };

  // handler for api call to change order status to api...

  onMarkAs = orderStatus => {
    console.log("mark: ", this.state.orderIds, orderStatus);
    const orderids = this.state.orderIds;

    this.props.orderStatusChange(orderids, orderStatus);
    // TODO for nadim: need to do real-time status change properly/ not another api call...
  };

  render() {
    return (
      <div>
        <Header />
        <div className="seller-section d-md-flex  px-4 mx-0 mx-lg-2 py-4 py-md-5">
          <SideNav />
          <div className="seller-cnt pt-2 pt-md-0">
            <OrderDetails />
            <OrderTable
              orders={this.props.orders}
              totalOrders={this.props.totalDocs}
              orderIds={this.state.orderIds}
              onSelectOrder={this.onSelectOrder}
              onSeletcAll={this.onSeletcAll}
              onMarkAs={this.onMarkAs}
            />

            {this.props.totalPages > 1 && (
              <Paginate
                previousLabel={"previous"}
                nextLabel={"next"}
                breakLabel={"..."}
                breakClassName={"break-me"}
                pageCount={this.props.totalPages}
                marginPagesDisplayed={2}
                pageRangeDisplayed={4}
                onPageChange={this.handlePageClick}
                pageLabelBuilder={this.pageLabelBuilder}
                activeClassName={"page-item active"}
                pageLinkClassName={"page-link"}
                containerClassName={"pagination justify-content-center"}
                previousClasses={"page-item"}
                previousLinkClassName={"page-link"}
                nextClasses={"page-item"}
                nextLinkClassName={"page-link"}
                pageClassName={"page-item"}
              />
            )}
            <OrderMonthTable />
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    orders: selectOrders(state),
    activeShop: selectActiveShop(state),
    page: selectPage(state),
    hasNextPage: selectHasNextPage(state),
    hasPrevPage: selecthasPrevPage(state),
    limit: selectLimit(state),
    nextPage: selectNextPage(state),
    pagingCounter: selectPagingCounter(state),
    prevPage: selectPrevPage(state),
    totalDocs: selectTotalDocs(state),
    totalPages: selectTotalPages(state),
    changeStatus: selectOrderStatusChange(state)
  }),
  {
    fetchAllOrders,
    orderStatusChange
  }
)(Orders);
