import React, { Component } from "react";
import ArrowRight from "../../../../../images/icons/arrowRight";
import Close from "../../../../../images/icons/close";
import Down from "../../../../../images/icons/down";

class OrderMonthTable extends Component {
  render() {
    return (
      <div className="row d-none d-md-flex pt-md-5">
        <div className="col-12 col-lg-4 col-xl-3">
          <table class="table table-bordered">
            <tbody className="font-weight-5">
              <tr>
                <td colSpan="3" className="py-3">
                  <span className="mr-3 sm-text-fw6 text-uppercase text-decoration-underline cursor-pointer">
                    All time
                  </span>
                  <span className="sm-text-fw6 text-uppercase text-decoration-underline cursor-pointer">
                    from-to
                  </span>
                </td>
              </tr>
              {/* Q4  start*/}
              <tr>
                <td>December</td>
                <td rowSpan="3" className="text-center">
                  Q4
                </td>
                <td rowSpan="12" className="text-center">
                  2021
                </td>
              </tr>
              <tr>
                <td>November</td>
              </tr>
              <tr>
                <td>October</td>
              </tr>

              {/* Q4  end*/}

              {/* Q3  start*/}
              <tr>
                <td>September</td>
                <td rowSpan="3" className="text-center">
                  Q3
                </td>
              </tr>
              <tr>
                <td>august</td>
              </tr>
              <tr>
                <td>July</td>
              </tr>

              {/*Q3  end*/}

              {/*  Q2  start*/}
              <tr>
                <td>June</td>
                <td rowSpan="3" className="text-center">
                  Q2
                </td>
              </tr>
              <tr>
                <td>May</td>
              </tr>
              <tr>
                <td>April</td>
              </tr>

              {/* Q2  end*/}

              {/* Q1  start*/}
              <tr>
                <td>March</td>
                <td rowSpan="3" className="text-center">
                  Q1
                </td>
              </tr>
              <tr>
                <td>February</td>
              </tr>
              <tr>
                <td>January</td>
              </tr>
              {/*Q1  end*/}
              <tr>
                <td>December</td>
                <td />
                <td />
              </tr>
            </tbody>
          </table>
        </div>

        <div className="col-12 col-lg-4 col-xl-3">
          <table class="table table-bordered">
            <tbody className="font-weight-5">
              <tr>
                <td colSpan="3" className="py-3">
                  <span className="mr-3 sm-text-fw6 text-uppercase text-decoration-underline cursor-pointer">
                    from <Down />
                  </span>
                  <span className="sm-text-fw6 text-uppercase text-decoration-underline cursor-pointer">
                    to <Down />
                  </span>
                  <span className="sm-text-fw6 text-uppercase text-decoration-underline cursor-pointer float-right">
                    <Down /> filter
                  </span>
                </td>
              </tr>
              {/* Q4  start*/}
              <tr>
                <td>December</td>
                <td rowSpan="3" className="text-center">
                  Q4
                </td>
                <td rowSpan="12" className="text-center">
                  2021
                </td>
              </tr>
              <tr>
                <td>November</td>
              </tr>
              <tr>
                <td>October</td>
              </tr>

              {/* Q4  end*/}

              {/* Q3  start*/}
              <tr>
                <td>September</td>
                <td rowSpan="3" className="text-center">
                  Q3
                </td>
              </tr>
              <tr>
                <td>august</td>
              </tr>
              <tr>
                <td>July</td>
              </tr>

              {/*Q3  end*/}

              {/*  Q2  start*/}
              <tr>
                <td>June</td>
                <td rowSpan="3" className="text-center">
                  Q2
                </td>
              </tr>
              <tr>
                <td>May</td>
              </tr>
              <tr>
                <td>April</td>
              </tr>

              {/* Q2  end*/}

              {/* Q1  start*/}
              <tr>
                <td>March</td>
                <td rowSpan="3" className="text-center">
                  Q1
                </td>
              </tr>
              <tr>
                <td>February</td>
              </tr>
              <tr>
                <td>January</td>
              </tr>
              {/*Q1  end*/}
              <tr>
                <td>December</td>
                <td />
                <td />
              </tr>
            </tbody>
          </table>
        </div>

        <div className="col-12 col-lg-4 col-xl-3">
          <div className="order-d-cnt">
            <div className="sm-text-fw6 text-uppercase mb-3">
              Name Surname
              <span className="float-right scale-hover cursor-pointer">
                <Close />
              </span>
            </div>
            <div className="x-sm-text-fw4">
                <div className="mb-3">Pylimo g. 3, Vilnius 01117</div>
                <div className="mb-3">+ 1234 123 123</div>
                <div className="text-right"><button className="btn border-btn p-0 border-0">Contact <ArrowRight /></button></div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default OrderMonthTable;
