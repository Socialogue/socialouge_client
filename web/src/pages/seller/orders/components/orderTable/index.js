import React, { Component } from "react";
import Order from "..//../components/order";
import Down from "../../../../../images/icons/down";
import search_icon from "../../../../../images/icons/search-icon.svg";
import Img from "./../../../../../images/eco-img.webp";

class OrderTable extends Component {
  state = {
    markAs: false,
    status: false,
    orderStatus: ""
  };
  render() {
    const { orders,onSelectOrder, totalOrders } = this.props;
    if (!orders) {
      return null;
    }

    return (
      <div className="mt-4 mb-5 pb-0 pb-md-3">
        <div className="s-top-tlt mb-4">
          <span className="font-weight-bold text-uppercase">
            Orders ({totalOrders | 0})
          </span>
        </div>
        {orders.length > 0 && (
          <div className="mb-3">
            <label className="ctm-container d-inline-block all-check">
              Select All
              <input
                type="checkbox"
                name="selectAll"
                checked={
                  this.props.orderIds.length > 0 &&
                  this.props.orders.length == this.props.orderIds.length
                    ? true
                    : false
                }
                onClick={this.props.onSeletcAll}
              />
              <span className="checkmark" />
            </label>
            <span
              className={
                this.state.markAs
                  ? "d-inline-block position-relative box-show ml-5 active"
                  : "d-inline-block position-relative box-show ml-5"
              }
            >
              <span
                onClick={() => {
                  if (this.props.orderIds.length > 0) {
                    this.setState({
                      markAs: !this.state.markAs
                    });
                  }
                }}
                className="d-flex cursor-pointer sm-text-fw6"
              >
                <span className="">Marks as</span>
                <span className="ml-1">
                  <Down />
                </span>
              </span>
              <div className="box-cnt2">
                <div className="box-ul">
                  <div className="box-li">
                    <label className="ctm-container d-inline-block">
                      New
                      <input
                        type="checkbox"
                        name="New"
                        onChange={() => this.setState({ orderStatus: "New" })}
                        checked={this.state.orderStatus == "New"}
                      />
                      <span className="checkmark" />
                    </label>
                  </div>
                  <div className="box-li">
                    <label className="ctm-container d-inline-block">
                      Shipped
                      <input
                        type="checkbox"
                        name="Shipped"
                        onChange={() =>
                          this.setState({ orderStatus: "Shipped" })
                        }
                        checked={this.state.orderStatus == "Shipped"}
                      />
                      <span className="checkmark" />
                    </label>
                  </div>
                </div>
                <button
                  className="btn border-btn w-100 black-bg-btn text-uppercase"
                  onClick={() => {
                    this.setState({
                      markAs: !this.state.markAs
                    });
                    this.props.onMarkAs(this.state.orderStatus);
                  }}
                >
                  mark
                </button>
              </div>
            </span>
          </div>
        )}
        <div className="seller-table order-tbl">
          {orders.length > 0 ? (
            <div className="tbl-cnt">
              {/* only mobile */}
              <div className="tbl-1 d-block d-md-none">
                <table class="table">
                  <thead>
                    <tr>
                      <th className="sm-text-fw6-g text-uppercase" />
                      <th className="sm-text-fw6-g text-uppercase photo-td">
                        Photo
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {/* loop */}
                    {orders.map((order, i) => {
                      return (
                        <tr className="pro-tr">
                          {/* when item select that time will be add active class in tr */}
                          <td className="sm-text-fw6 text-uppercase td-border f-td">
                            <label className="ctm-container d-inline-block">
                              <input
                                type="checkbox"
                                name="selectTr[]"
                                value={order._id}
                                checked={
                                  this.props.orderIds
                                    ? this.props.orderIds.includes(order._id)
                                    : false
                                }
                                onClick={e => onSelectOrder(e, order._id)}
                              />
                              <span className="checkmark" />
                            </label>
                          </td>
                          <td className="sm-text-fw6 text-uppercase td-border pl-0">
                            <img
                              src={order.productImage}
                              width="27"
                              height="36"
                              alt=""
                            />
                          </td>
                        </tr>
                      );
                    })}

                    {/* loop */}
                  </tbody>
                </table>
              </div>
              {/* only mobile */}

              <div className="tbl-2 order-tbl-2">
                <table class="table">
                  <thead>
                    <tr>
                      <th className="sm-text-fw6-g text-uppercase  d-none d-md-table-cell" />
                      <th className="sm-text-fw6-g text-uppercase photo-td  d-none d-md-table-cell">
                        Photo
                      </th>
                      <th className="sm-text-fw6-g text-uppercase name-td text-nowrap">
                        Product name
                      </th>
                      <th className="sm-text-fw6-g text-uppercase">Cost</th>
                      <th
                        className={
                          this.state.status
                            ? "position-relative box-show active"
                            : "position-relative box-show"
                        }
                      >
                        <span
                          onClick={() =>
                            this.setState({
                              status: !this.state.status
                            })
                          }
                          className="d-inline-flex cursor-pointer sm-text-fw6-g text-uppercase"
                        >
                          <span className="">Status</span>
                          <span className="ml-1 d-none d-md-inline-block">
                            <Down />
                          </span>
                        </span>
                        <div className="box-cnt2">
                          <div className="box-ul">
                            <div className="box-li">
                              <label className="ctm-container d-inline-block">
                                Select All
                                <input type="checkbox" name="selectAll" />
                                <span className="checkmark" />
                              </label>
                            </div>
                            <div className="box-li">
                              <div className="input-group search-group">
                                <div className="input-group-prepend">
                                  <span className="input-group-text">
                                    <img src={search_icon} alt="img" />
                                  </span>
                                </div>
                                <input
                                  type="text"
                                  className="form-control custom-input"
                                  placeholder="Search"
                                />
                              </div>
                            </div>
                            <div className="box-li">
                              <label className="ctm-container d-inline-block">
                                New (10)
                                <input
                                  type="checkbox"
                                  name="selectAll"
                                  onChange={() => this.props.onMarkAs("New")}
                                />
                                <span className="checkmark" />
                              </label>
                            </div>
                            <div className="box-li">
                              <label className="ctm-container d-inline-block">
                                Shipped (3)
                                <input
                                  type="checkbox"
                                  name="selectAll"
                                  onChange={() =>
                                    this.props.onMarkAs("Shipped")
                                  }
                                />
                                <span className="checkmark" />
                              </label>
                            </div>
                          </div>
                          <button className="btn border-btn w-100 black-bg-btn text-uppercase">
                            filter
                          </button>
                        </div>
                      </th>
                      <th className="sm-text-fw6-g text-uppercase">Model</th>
                      <th className="sm-text-fw6-g text-uppercase">QTY</th>
                      <th className="sm-text-fw6-g text-uppercase">Date</th>
                      <th className="sm-text-fw6-g text-uppercase">Customer</th>
                    </tr>
                  </thead>
                  <tbody>
                    {orders.map((order, i) => {
                      return (
                        <Order
                          key={i}
                          order={order}
                          orderId={this.props.orderIds}
                          onSelectOrder={this.props.onSelectOrder}
                        />
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          ) : (
            <h3 className="h3 color-orange">No order yet</h3>
          )}
        </div>
      </div>
    );
  }
}

export default OrderTable;
