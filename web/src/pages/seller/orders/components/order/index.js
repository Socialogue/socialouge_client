import React, { Component } from "react";
import Gmail from "../../../../../images/icons/gmail";
import Img from "./../../../../../images/eco-img.webp";
import moment from "moment";

class Order extends Component {
  render() {
    const { order, onSelectOrder } = this.props;
    if (!order) {
      return null;
    }
    return (
      <tr className="pro-tr">
        {/* when item select that time will be add active class in tr */}
        <td className="sm-text-fw6 text-uppercase td-border f-td d-none d-md-table-cell">
          <label className="ctm-container d-inline-block">
            <input
              type="checkbox"
              name="selectTr[]"
              value={order._id}
              checked={
                this.props.orderId
                  ? this.props.orderId.includes(order._id)
                  : false
              }
              onClick={e => onSelectOrder(e, order._id)}
            />
            <span className="checkmark" />
          </label>
        </td>
        <td className="sm-text-fw6 text-uppercase td-border d-none d-md-table-cell pl-0">
          <img src={order.productImage} width="27" height="36" alt="" />
        </td>
        <td className="sm-text-fw6 text-uppercase td-border text-left">
          {order.productName}
        </td>
        <td className="sm-text-fw6 text-uppercase td-border text-nowrap">
          {order.totalCost} €{" "}
        </td>
        <td className="sm-text-fw6 text-uppercase td-border">
          <span className="color-orange">{order.orderStatus}</span>
        </td>
        <td className="sm-text-fw6 text-uppercase td-border  text-nowrap">
          {order.model}
        </td>
        <td className="sm-text-fw6 text-uppercase td-border text-nowrap">
          {order.quantity}
        </td>
        <td className="sm-text-fw6 text-uppercase td-border  text-nowrap">
          {moment(order.orderDate).fromNow()}
        </td>
        <td className="sm-text-fw6 text-uppercase td-border l-td">
          <span className="d-flex-box text-nowrap">
            <span className="mr-2">{order.user.name}</span>
            <span className="">
              <Gmail />
            </span>
          </span>
        </td>
      </tr>
    );
  }
}

export default Order;
