import React, { Component } from "react";
import ArrowTopRight from "../../../../../images/icons/arrowTopRight";
import Date from "../../../../../images/icons/date";
import Down from "../../../../../images/icons/down";

class OrderDetails extends Component {
  render() {
    return (
      <div>
        <div className="sm-text-fw6 text-uppercase mb-3">
          <span className="d-inline-flex align-items-center cursor-pointer">
            <span className="">
              {" "}
              <Date />
            </span>
            <span className="selected-txt mx-1">Calendar</span>
            <span className="">
              <Down />
            </span>
          </span>
        </div>
        <div className="row mr-0">
          <div className="col-12 col-lg-7 col-xl-8 pr-0">
            <div className="order-d-cnt mb-4 mb-lg-0">
              <div className="order-d-top mb-3 pb-5">
                <div className="font-weight-bold text-uppercase">sals</div>
                <div className="sm-text-fw6 text-right">
                  <span className="color-green">
                    43,2%{" "}
                    <sup className="c-sup">
                      <ArrowTopRight />
                    </sup>
                  </span>{" "}
                </div>
                <div className="md-title md-title-res mb-2">
                  Total day sales: <span className="float-right">€ 143.43</span>
                </div>
                <div className="sm-text-fw6-g text-uppercase">
                  April 1, 2020
                  <span className="float-right color-orange">12 sold</span>
                </div>
              </div>
              <div className="order-d-bottom">
                <div className="sm-text-fw6 text-uppercase mb-3">
                  All time sales
                </div>
                <div className="text-uppercase font-weight-6">
                  <span className="all-date all-from-date mr-4">
                    April 1, 2020
                  </span>
                  <span className="all-date  all-to-date">April 1, 2020</span>
                  <span className="float-right lg-text font-weight-6">
                    6.564.00€
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-5 col-xl-4 pr-0">
            <div className="order-d-cnt h-100">
              <table className="table">
                <tr>
                  <td className="border-0 pl-0 sm-text-fw6 text-uppercase">
                    Last 24 hours{" "}
                  </td>
                  <td className="border-0 text-center sm-text-fw6 text-uppercase">
                    <span className="color-green">
                      43,2%
                      <sup className="c-sup">
                        <ArrowTopRight />
                      </sup>
                    </span>
                  </td>
                  <td className="border-0 text-right pr-0">
                    <span className="lg-text font-weight-6">165.00 €</span>
                  </td>
                </tr>
                <tr>
                  <td className="border-0 pl-0 sm-text-fw6 text-uppercase">
                    Last 7 hours{" "}
                  </td>
                  <td className="border-0 text-center sm-text-fw6 text-uppercase">
                    <span className="color-green">13,2%</span>
                  </td>
                  <td className="border-0 text-right pr-0">
                    <span className="lg-text font-weight-6">894.00€</span>
                  </td>
                </tr>
                <tr>
                  <td className="border-0 pl-0 sm-text-fw6 text-uppercase">
                    Last 30 hours{" "}
                  </td>
                  <td className="border-0 text-center sm-text-fw6 text-uppercase">
                    <span className="color-orange">22%</span>
                  </td>
                  <td className="border-0 text-right pr-0">
                    <span className="lg-text font-weight-6">6.564.00€</span>
                  </td>
                </tr>
              </table>
              <div className="sm-text-fw4-g">
                Percentage change relative to prior period. <br />
                Data includes shipping and sales tax.
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default OrderDetails;
