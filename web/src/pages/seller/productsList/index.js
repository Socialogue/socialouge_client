import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Delete from '../../../images/icons/delete'
import Plus from '../../../images/icons/plus'
import Header from '../../home/Header/Header'
import SideNav from '../common/sideNav'
import './../seller.scss'
import Item from './item'
import Img from './../../../images/eco-img.webp'
import { connect } from 'react-redux'
import { selectActiveShop } from 'core/selectors/account'
import {
  fetchAllProducts,
  deleteProduct,
} from 'core/actions/dashboardActionCreators'
import {
  selectHasNextPage,
  selecthasPrevPage,
  selectLimit,
  selectNextPage,
  selectPage,
  selectPagingCounter,
  selectPrevPage,
  selectProductDeleteStatus,
  selectProducts,
  selectTotalDocs,
  selectTotalPages,
} from 'core/selectors/dashboard'
import Pagination from '../../../components/pagination'
import Paginate from '../../../components/paginate'
import ConfirmModal from '../../../components/confirmModal'

class ProductsList extends Component {
  state = {
    page: 1,
    limit: 8,
    products: [],
    productsIds: [],
    productHave: false,
    onDelete: false,
    deleteSuccess: true,
  }
  componentDidMount = () => {
    const { page, limit } = this.state
    this.props.fetchAllProducts(this.props.activeShop._id, page, limit)
  }

  componentDidUpdate = (prevProps) => {
    // console.log(
    //   "check: ",
    //   JSON.stringify(prevProps.produucts) !==
    //     JSON.stringify(this.props.produucts),
    //   this.props.deleteSuccess
    // );
    // if (
    //   JSON.stringify(prevProps.produucts) !==
    //   JSON.stringify(this.props.produucts)
    // ) {
    if (this.props.deleteSuccess && this.state.deleteSuccess) {
      // TODO for nadim: need to give real time feelings after delete...
      const { page, limit } = this.state
      this.props.fetchAllProducts(this.props.activeShop._id, page, limit)
      this.setState({
        page: 1,
        limit: 2,
        productsIds: [],
        onDelete: false,
        deleteSuccess: false,
      })
    }
    // }
  }

  onSelectProduct = (e) => {
    let ids = [...this.state.productsIds]
    const value = e.target.value

    if (ids.find((id) => id == value)) {
      ids = ids.filter((id) => id !== value)
      this.setState({ productsIds: ids })
    } else {
      ids = [...ids, value]
      this.setState({ productsIds: ids })
    }

    // code of kowser
    var v1
    //
    const tbl = e.target.closest('.table').querySelectorAll('.pro-tr')
    tbl.forEach(function (userItem, i) {
      if (userItem.classList.contains('index')) {
        userItem.classList.remove('index')
      }
    })
    e.target.closest('.pro-tr').classList.add('index')
    tbl.forEach(function (userItem, i) {
      if (userItem.classList.contains('index')) {
        v1 = i
      }
    })
    const tbl1 = document.querySelector('.tbl-1').querySelectorAll('.pro-tr')
    const tbl2 = document.querySelector('.tbl-2').querySelectorAll('.pro-tr')
    tbl1[v1].classList.toggle('active')
    tbl2[v1].classList.toggle('active')
    if (
      e.target.closest('.table').querySelectorAll('.active').length ==
      tbl.length
    ) {
      document.querySelector('.all-check').classList.add('ctm-active')
    } else {
      document.querySelector('.all-check').classList.remove('ctm-active')
    }
    // code of kowser
  }

  // on select single product...
  onDeleteProduct = () => {
    this.setState({ onDelete: true })
  }

  confirmDeleteProduct = (sure) => {
    if (sure == true) {
      this.props.deleteProduct(this.state.productsIds)
    }
    this.setState({
      page: 1,
      limit: 2,
      products: [],
      productsIds: [],
      productHave: false,
      onDelete: false,
      deleteSuccess: true,
    })
  }

  onDeleteSingleProduct = (productId) => {
    this.setState({ onDelete: true, productsIds: [productId] })
  }

  handlePageClick = (e) => {
    const { limit } = this.state
    const page = +e.selected + 1
    // const skip = (page -1) * limit;
    this.setState({ page: page })
    this.props.fetchAllProducts(this.props.activeShop._id, page, limit)
  }

  pageLabelBuilder = (e) => {
    // console.log("pageLabelBuilder: ", e);
    return e
  }

  // handler for select all ...
  onSeletcAll = (e) => {
    let ids = []
    if (this.state.productsIds.length < 1) {
      this.props.products.map((product) => {
        ids.push(product._id)
      })
    }
    this.setState({ productsIds: ids })

    // code of kowser
    e.target.closest('.ctm-container').classList.toggle('ctm-active')
    if (e.target.closest('.ctm-container').classList.contains('ctm-active')) {
      document.querySelectorAll('.pro-tr').forEach(function (userItem) {
        userItem.classList.add('active')
      })
    } else {
      document.querySelectorAll('.pro-tr').forEach(function (userItem) {
        userItem.classList.remove('active')
      })
    }
    // code of kowser
  }

  render() {
    return (
      <div>
        <Header />
        <div className="seller-section d-md-flex px-4 mx-2 py-4 py-md-5">
          <SideNav />
          <div className="seller-cnt pt-2 pt-md-0">
            <div className="s-top-tlt mb-4">
              <span className="font-weight-bold text-uppercase">
                Listings ({this.props.totalDocs})
              </span>
              <Link
                to={'/create-product'}
                className="btn border-btn float-right"
              >
                Add items <Plus />
              </Link>
            </div>
            {this.props.products.length > 0 && (
              <div className="mb-3">
                <label className="ctm-container all-check d-inline-block">
                  Select All
                  <input
                    type="checkbox"
                    name="selectAll"
                    checked={
                      this.state.productsIds.length > 0 &&
                      this.props.products.length ==
                        this.state.productsIds.length
                        ? true
                        : false
                    }
                    onClick={this.onSeletcAll}
                  />
                  <span className="checkmark" />
                </label>
                {this.state.productsIds.length > 0 && (
                  <button
                    className="btn border-btn p-0 border-0 ml-4 d-inline-flex align-items-center"
                    onClick={() => this.onDeleteProduct()}
                  >
                    <Delete />
                    <span className="ml-1">
                      <span className="text-decoration-underline cursor-pointer mr-1">
                        Delete
                      </span>
                      ({this.state.productsIds.length} Products)
                    </span>
                  </button>
                )}
              </div>
            )}
            <div className="seller-table mb-5">
              {this.props.products.length > 0 ? (
                <div className="tbl-cnt">
                  {/* only mobile */}
                  <div className="tbl-1 d-block d-md-none">
                    <table class="table">
                      <thead>
                        <tr>
                          <th className="sm-text-fw6-g text-uppercase" />
                          <th className="sm-text-fw6-g text-uppercase photo-td px-0">
                            Photo
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {/* loop */}
                        {this.props.products.map((product, i) => {
                          return (
                            <tr className="pro-tr">
                              {/* when item select that time will be add active class in tr */}
                              <td className="sm-text-fw6 text-uppercase td-border f-td">
                                <label className="ctm-container d-inline-block">
                                  <input
                                    type="checkbox"
                                    name="selectTr[]"
                                    value={product._id}
                                    checked={
                                      this.state.productsIds
                                        ? this.state.productsIds.includes(
                                            product._id,
                                          )
                                        : false
                                    }
                                    onClick={(e) =>
                                      this.onSelectProduct(e, product._id)
                                    }
                                  />
                                  <span className="checkmark" />
                                </label>
                              </td>
                              <td className="sm-text-fw6 text-uppercase td-border px-0 ">
                                <img
                                  src={product.mainImage}
                                  width="27"
                                  height="36"
                                  alt=""
                                />
                              </td>
                            </tr>
                          )
                        })}
                        {/* loop */}
                      </tbody>
                    </table>
                  </div>
                  {/* only mobile */}
                  <div className="tbl-2" style={{ minHeight: 'auto' }}>
                    <table class="table">
                      <thead>
                        <tr>
                          <th className="sm-text-fw6-g text-uppercase  d-none d-md-table-cell" />
                          <th className="sm-text-fw6-g text-uppercase photo-td d-none d-md-table-cell px-0">
                            Photo
                          </th>
                          <th className="sm-text-fw6-g text-uppercase name-td ">
                            Product name
                          </th>
                          <th className="sm-text-fw6-g text-uppercase">
                            Quantity
                          </th>
                          <th className="sm-text-fw6-g text-uppercase">
                            Price
                          </th>
                          <th className="sm-text-fw6-g text-uppercase">SKU</th>
                          <th className="sm-text-fw6-g text-uppercase"> </th>
                          <th className="sm-text-fw6-g text-uppercase">
                            Options
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.props.products.map((product, i) => {
                          return (
                            <Item
                              key={i}
                              product={product}
                              productId={this.state.productsIds}
                              onSelectProduct={this.onSelectProduct}
                              onDeleteSingleProduct={this.onDeleteSingleProduct}
                            />
                          )
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
              ) : (
                <h3 className="h3 color-orange">No Products found.</h3>
              )}
            </div>
            {this.props.totalPages > 1 && (
              <Paginate
                previousLabel={'previous'}
                nextLabel={'next'}
                breakLabel={'...'}
                breakClassName={'break-me'}
                pageCount={this.props.totalPages}
                marginPagesDisplayed={2}
                pageRangeDisplayed={4}
                onPageChange={this.handlePageClick}
                pageLabelBuilder={this.pageLabelBuilder}
                activeClassName={'page-item active'}
                pageLinkClassName={'page-link'}
                containerClassName={'pagination justify-content-center'}
                previousClasses={'page-item'}
                previousLinkClassName={'page-link'}
                nextClasses={'page-item'}
                nextLinkClassName={'page-link'}
                pageClassName={'page-item'}
              />
            )}
          </div>
        </div>
        {this.state.onDelete && (
          <ConfirmModal
            actionType="Delete"
            actionMsg="Your product"
            additionalMsg="It will be permanently deleted!"
            sure={this.confirmDeleteProduct}
          />
        )}

        {/* <Pagination
          pages={this.props.totalPages}
          page={this.props.page}
          hasNextPage={this.props.hasNextPage}
          hasPrevPage={this.props.hasPrevPage}
          nextPage={this.props.nextPage}
          prevPage={this.props.prevPage}
          totalDocs={this.props.totalDocs}
          totalPages={this.props.totalPages}
        /> */}
      </div>
    )
  }
}

export default connect(
  (state) => ({
    activeShop: selectActiveShop(state),
    products: selectProducts(state),
    page: selectPage(state),
    hasNextPage: selectHasNextPage(state),
    hasPrevPage: selecthasPrevPage(state),
    limit: selectLimit(state),
    nextPage: selectNextPage(state),
    pagingCounter: selectPagingCounter(state),
    prevPage: selectPrevPage(state),
    totalDocs: selectTotalDocs(state),
    totalPages: selectTotalPages(state),
    deleteSuccess: selectProductDeleteStatus(state),
  }),
  { fetchAllProducts, deleteProduct },
)(ProductsList)
