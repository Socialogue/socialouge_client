import { cropString } from "core/utils/helpers";
import React, { Component } from "react";
import Img from "./../../../../images/eco-img.webp";

class Item extends Component {
  render() {
    const { product, onSelectProduct, onDeleteSingleProduct } = this.props;
    if (!product) {
      return null;
    }
    return (
      <tr className="pro-tr">
        {/* when item select that time will be add active class in tr */}
        <td className="sm-text-fw6 text-uppercase td-border f-td d-none d-md-table-cell">
          <label className="ctm-container d-inline-block">
            <input
              type="checkbox"
              name="selectTr[]"
              value={product._id}
              checked={
                this.props.productId
                  ? this.props.productId.includes(product._id)
                  : false
              }
              onClick={(e) => onSelectProduct(e, product._id)}
            />
            <span className="checkmark" />
          </label>
        </td>
        <td className="sm-text-fw6 text-uppercase td-border d-none d-md-table-cell px-0">
          <img src={product.mainImage} width="27" height="36" alt="" />
        </td>
        <td className="sm-text-fw6 text-uppercase td-border text-left">
          {cropString(product.title, 40)}
        </td>
        <td className="sm-text-fw6 text-uppercase td-border ">
          <span className="v-control text-nowrap">
            {product.variations.length < 1 && (
              <span className="cn-v minus-v">-</span>
            )}
            <span className="td-v">
              {" "}
              {product.variations.length > 1
                ? "VARIATIONS"
                : product.variations[0].quantity}
            </span>{" "}
            {product.variations.length < 1 && (
              <span className="cn-v minus-v">-</span>
            )}
          </span>
        </td>
        <td className="sm-text-fw6 text-uppercase td-border">
          <span className="v-control text-nowrap">
            {product.variations.length < 1 && (
              <span className="cn-v minus-v">-</span>
            )}
            <span className="td-v text-nowrap">
              {" "}
              {product.variations.length > 1
                ? "VARIATIONS"
                : product.variations[0].price + " €"}{" "}
            </span>
            {product.variations.length < 1 && (
              <span className="cn-v plus-v">+</span>
            )}
          </span>
        </td>
        <td className="sm-text-fw6 text-uppercase td-border l-td">
          {product.variations.length > 1
            ? "VARIATIONS"
            : product.variations[0].sku}{" "}
        </td>

        <td className="sm-text-fw6 text-uppercase line-td"> </td>
        <td className="sm-text-fw6 text-uppercase">
          <span className="sm-text-fw6-g text-decoration-underline mr-3 cursor-pointer">
            Edit
          </span>
          <span
            className="text-decoration-underline color-orange cursor-pointer"
            onClick={() => onDeleteSingleProduct(product._id)}
          >
            Delete
          </span>
        </td>
      </tr>
    );
  }
}

export default Item;
