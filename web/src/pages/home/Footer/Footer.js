import React from "react";
import { Link } from "react-router-dom";
import {
  ROUTE_ABOUT_US,
  ROUTE_FAQ,
  ROUTE_PRIVACY_POLICY,
  ROUTE_TERMS_SERVICE
} from "../../../routes/constants";
import "./Footer.scss";

const Footer = () => (
  <div className="footer d-none d-md-block">
    <div className="max-body-content">
      <nav className="navbar navbar-expand-lg w-100 p-0">
        <div className="collapse navbar-collapse">
          <ul className="navbar-nav">
            <li className="nav-item ml-0">
              <Link className="nav-link" to={ROUTE_TERMS_SERVICE}>
                terms 
              </Link> 
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={ROUTE_PRIVACY_POLICY}>
                privacy
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="#">
                sell
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={ROUTE_ABOUT_US}>
                about
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to={ROUTE_FAQ}>
                FAQ
              </Link>
            </li>
          </ul>
        </div>
        <div className="sm-text-fw6 text-nowrap text-uppercase">
          socialogue © 2021
        </div>
      </nav>
    </div>
  </div>
);

export default Footer;
