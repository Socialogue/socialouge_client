import React from 'react'
import { connect } from 'react-redux'
import { selectPosts } from 'core/selectors/posts'
import {
  getPostProducts,
  openPostInDetails,
} from 'core/actions/homeActionCreators'
import { addComment } from 'core/actions/commentActionCreators'
import Header from './Header/Header'
import Footer from './Footer/Footer'
import EcoBrandTitle from './Home/EcoBrandTitle/EcoBrandTitle'
import ProductPostContent from './Home/ProductPostContent/ProductPostContent'
import FilterMenu from './FilterMenu/FilterMenu'
import {
  selectFeed,
  selectPost,
  selectPostChange,
  selectToken,
  selectViewType,
  selectViewTypeFeed,
} from 'core/selectors/user'
import CreatePostModal from '../profile/common/createPostModal/createPostModal'
import AddPost from './addPost'
import SortViewFeed from './sortView/sortViewFeed'
import {
  SORT_VIEW_TOP,
  SORT_VIEW_MID,
  SORT_VIEW_BOTTOM,
} from 'core/constants/index'
import ProductPostContentMid from './Home/ProductPostContentMid/ProductPostContentMid'
import ProductPostContentBottom from './Home/ProductPostContentBottom/ProductPostContentBottom'
import SellerTitle from './Home/sellerTitle'
import WeekTitle from './Home/weekTitle'
import './Home/ProductPostContent/ProductPostContent.scss'
import FixedFooter from '../Common/fixedFooter'
import ScrollTopZero from '../Common/ScrollTopZero'
import MobileProductFilter from '../shopProfile/products/mobileProductFilter'
import MobileSort from '../shopProfile/products/mobileSort'
import PostModal from '../modal/postModal'
import { Fragment } from 'react'
import {
  ROUTE_USER_OWN_PROFILE_POST,
  ROUTE_USER_PROFILE_POST,
} from '../../routes/constants'
import { errorAlert } from '../../utils/alerts'
import { isEmpty } from 'lodash'

class Home extends React.Component {
  constructor() {
    super()
    this.state = {
      showModal: false,
      loading: false,
      postModalOpen: false,
      post: {},
    }
  }
  componentDidMount = () => {
    try {
      this.props.getPostProducts()
    } catch (e) {
      console.log(e)
    }
  }

  componentDidUpdate = (prevProps) => {
    if (prevProps.feed !== this.props.feed) {
      if (this.props.feed) {
        console.log('feed: ', this.props.feed)
        if (this.state.post) {
          const posts = [...this.props.feed.posts]
          const postIndex = posts.findIndex(
            (post) => post._id === this.state.post._id,
          )
          if (postIndex !== -1) {
            this.setState({ post: posts[postIndex] })
          }
        }
      }
    }

    if (prevProps.postChangeStatus !== this.props.postChangeStatus) {
      if (this.props.postChangeStatus) {
        this.props.openPostInDetails(this.props.post)
      }
    }
  }

  toggleModal = () => {
    this.setState({ showModal: !this.state.showModal })
  }

  // handler for goto post details as popup...
  openPostDetailsPopup = (post) => {
    document.body.classList.add('modal-open')
    this.props.openPostInDetails(post)
    this.setState({ postModalOpen: true, post: post })
  }

  // handler for closing post details popup...
  closePostDetailsPopup = () => {
    document.body.classList.remove('modal-open')
    this.setState({ postModalOpen: false, post: null })
    this.props.openPostInDetails('post')
  }

  // handler for adding a comment to post... passing as props
  addingComment = (commentText, postId, postOwner) => {
    const userId = this.props.auth.user._id
    if (!userId) {
      return errorAlert('Please login first.')
    }
    // if not have userId then show some notification ...
    if (!userId) {
      return errorAlert('Login first please!')
    }
    this.props.addComment(commentText, postId, postOwner, userId)
  }

  // handler for goto user profile...
  gotoProfile = (userId) => {
    if (userId == this.props.auth.user._id) {
      this.props.history.push(ROUTE_USER_OWN_PROFILE_POST)
    } else {
      const route = ROUTE_USER_PROFILE_POST.replace(':id', userId)
      this.props.history.push(route)
    }
  }

  render() {
    const { postModalOpen, post } = this.state
    return (
      <div
        className={
          postModalOpen === false && !isEmpty(post) ? 'd-none' : 'd-block show'
        }
      >
        <Fragment>
          <Header history={this.props.history} />
          {this.props.isLoggedIn == true ? (
            <div>
              <AddPost onClick={this.toggleModal} />

              <CreatePostModal
                showModal={this.state.showModal}
                toggleModal={this.toggleModal}
              />
            </div>
          ) : (
            <EcoBrandTitle />
          )}
          {this.props.sortViewTypeFeed == SORT_VIEW_TOP ? (
            <ProductPostContent
              data={this.props.feed}
              addingComment={this.addingComment}
              openPostModal={this.openPostDetailsPopup}
            />
          ) : this.props.sortViewTypeFeed == SORT_VIEW_MID ? (
            <ProductPostContentMid
              data={this.props.feed}
              addingComment={this.addingComment}
              openPostModal={this.openPostDetailsPopup}
            />
          ) : (
            <ProductPostContentBottom
              data={this.props.feed}
              addingComment={this.addingComment}
              openPostModal={this.openPostDetailsPopup}
            />
          )}
          <SellerTitle />
          {this.props.sortViewTypeFeed == SORT_VIEW_TOP ? (
            <ProductPostContent
              data={this.props.feed}
              addingComment={this.addingComment}
              openPostModal={this.openPostDetailsPopup}
            />
          ) : this.props.sortViewTypeFeed == SORT_VIEW_MID ? (
            <ProductPostContentMid
              data={this.props.feed}
              addingComment={this.addingComment}
              openPostModal={this.openPostDetailsPopup}
            />
          ) : (
            <ProductPostContentBottom
              data={this.props.feed}
              addingComment={this.addingComment}
              openPostModal={this.openPostDetailsPopup}
            />
          )}
          <WeekTitle />
          {this.props.sortViewTypeFeed == SORT_VIEW_TOP ? (
            <ProductPostContent
              data={this.props.feed}
              addingComment={this.addingComment}
              openPostModal={this.openPostDetailsPopup}
            />
          ) : this.props.sortViewTypeFeed == SORT_VIEW_MID ? (
            <ProductPostContentMid
              data={this.props.feed}
              addingComment={this.addingComment}
              openPostModal={this.openPostDetailsPopup}
            />
          ) : (
            <ProductPostContentBottom
              data={this.props.feed}
              addingComment={this.addingComment}
              openPostModal={this.openPostDetailsPopup}
            />
          )}
          <MobileProductFilter />
          <MobileSort />
          <SortViewFeed />
          <FilterMenu />
          <FixedFooter />
        </Fragment>

        {postModalOpen === true && (
          <PostModal
            // post={post}
            close={this.closePostDetailsPopup}
            gotoProfile={this.gotoProfile}
            addingComment={this.addingComment}
          />
        )}
        <ScrollTopZero Display="d-flex d-md-none" />
      </div>
    )
  }
}

export default connect(
  (state) => ({
    posts: selectPosts(state),
    isLoggedIn: !!selectToken(state),
    auth: state.auth,
    sortViewTypeFeed: selectViewTypeFeed(state),
    feed: selectFeed(state),
    postChangeStatus: selectPostChange(state),
    post: selectPost(state),
  }),
  {
    getPostProducts,
    addComment,
    openPostInDetails,
  },
)(Home)
