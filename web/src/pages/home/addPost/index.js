import './index.scss'

import {
  ROUTE_SHOP_PROFILE_PRODUCTS,
  ROUTE_USER_OWN_PROFILE_POST,
  ROUTE_USER_PROFILE_POST,
} from '../../../routes/constants'
import React, { useEffect, useState } from 'react'
import { cropString, getImageOriginalUrl } from 'core/utils/helpers'
import { selectIsLoggedIn, selectUser } from 'core/selectors/user'

import ArDown from './../../../images/icons/dropdown-icon.svg'
import PhotoUpload from './../../../images/icons/photoUpload'
import ProImg from './../../../images/login-img1.webp'
import Video from './../../../images/icons/video'
import { connect } from 'react-redux'
import { getTmpShopProfileImage } from 'core/selectors/profile'
import { isEmpty } from 'lodash'
import { selectActiveShop } from 'core/selectors/account'
import { selectShops } from 'core/selectors/shops'
import { setActiveShop } from 'core/actions/accountActionCreators'
import { withRouter } from 'react-router'

const AddPost = (props) => {
  const [showList, setshowList] = useState(false)
  const gotoProfile = () => {
    props.history.push(ROUTE_USER_OWN_PROFILE_POST)
  }

  useEffect(() => {
    document.body.addEventListener('click', () => showUl(false))
  }, [])

  const gotoShopProfile = () => {
    props.history.push(
      ROUTE_SHOP_PROFILE_PRODUCTS.replace(':id', props.activeShop._id),
    )
  }
  const showUl = (status) => {
    setshowList(status)
  }

  const activeShopProfile = (shopName) => {
    const allShops = props.allShops
    const shop = allShops.filter((shop) => shop.shopName == shopName)
    props.setActiveShop(shopName, shop[0])
    showUl(false)

    // props.history.push(ROUTE_SHOP_PROFILE_PRODUCTS.replace(':id', shop[0]._id))
  }

  const gotToProfile = (e) => {
    props.setActiveShop(undefined, undefined)
    // props.history.push(ROUTE_USER_OWN_PROFILE_POST)
  }

  return (
    <div className="add-post d-flex align-items-center px-5 px-md-0">
      <div className="pro-cnt align-items-center mr-2 mr-md-5 cursor-pointer">
        {/* ctm-select2 */}
        <div
          className="pro-img"
          onClick={() => {
            if (isEmpty(props.activeShop)) {
              gotoProfile()
            } else {
              gotoShopProfile()
            }
          }}
        >
          <img
            className="cover"
            src={
              !isEmpty(props.activeShop)
                ? props.activeShop.profileImage
                  ? getImageOriginalUrl(props.activeShop.profileImage)
                  : ProImg
                : props.user && props.user.profileImage
                ? getImageOriginalUrl(props.user.profileImage, '80_80_')
                : ProImg
            }
            alt="img"
          />
        </div>
        <div
          className={
            showList
              ? 'pro-txt-cnt d-none d-md-block position-realtive active '
              : 'pro-txt-cnt d-none d-md-block position-realtive'
          }
          id="show-user-st"
        >
          <div
            onClick={() => showUl(!showList)}
            className="sm-text-fw6 text-uppercase text-nowrap show-fl-n"
          >
            {!isEmpty(props.activeShop)
              ? cropString(props.activeShop.shopName, 10)
              : cropString(props.user.fullName, 10)}
            <img src={ArDown} alt="img" />
            <div className="fl-n">{!isEmpty(props.activeShop)
              ? props.activeShop.shopName
              : props.user.fullName }</div>
          </div>
          <div className="ctm-select-box ctm-select-box2">
            {props.user && (
              <div
                className="ctm-select-option text-uppercase"
                onClick={gotToProfile}
              >
                <img
                  className="cover mr-1"
                  style={{ height: 20, width: 20 }}
                  src={
                    props.user.profileImage
                      ? getImageOriginalUrl(props.user.profileImage, '40_40_')
                      : ProImg
                  }
                  alt="img"
                />{' '}
                {cropString(props.user.fullName, 15)}{' '}
              </div>
            )}
            {props.allShops &&
              props.allShops.map((shop, index) => {
                return (
                  <span key={`${shop._id}_${index}`}>
                    <div
                      className="ctm-select-option text-uppercase"
                      key={shop._id}
                      onClick={() => activeShopProfile(shop.shopName)}
                    >
                      <img
                        className="cover mr-1"
                        style={{ height: 20, width: 20 }}
                        src={
                          props.tmpShopProfile
                            ? getImageOriginalUrl(props.tmpShopProfile)
                            : shop && shop.profileImage
                            ? getImageOriginalUrl(shop.profileImage, '40_40_')
                            : ProImg
                        }
                        alt="img"
                      />{' '}
                      {cropString(shop.shopName, 15)}{' '}
                    </div>
                  </span>
                )
              })}
          </div>
        </div>
      </div>
      <div onClick={props.onClick} className="form-group mb-0 w-100">
        <div className="form-input position-relative form-input-append">
          <input
            type="text"
            className="form-control custom-input"
            placeholder="Start a post..."
            name=""
          />
          <span className="append-input d-flex-box cursor-default">
            <span className="scale-hover cursor-pointer d-inline-flex mr-3">
              <PhotoUpload />
            </span>
            <span className="scale-hover cursor-pointer d-inline-flex">
              <Video />
            </span>
          </span>
        </div>
      </div>
    </div>
  )
}

export default connect(
  (state) => ({
    user: selectUser(state),
    isLoggedIn: selectIsLoggedIn(state),
    activeShop: selectActiveShop(state),
    allShops: selectShops(state),
    tmpShopProfile: getTmpShopProfileImage(state),
  }),
  { setActiveShop },
)(withRouter(AddPost))
