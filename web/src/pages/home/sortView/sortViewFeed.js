import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './sortView.scss'
import ArDown from './../../../images/icons/dropdown-icon.svg'
import { connect } from 'react-redux'
import { selectViewTypeFeed } from 'core/selectors/user'
import { sortViewFeed } from 'core/actions/homeActionCreators'
import {
  SORT_VIEW_TOP,
  SORT_VIEW_MID,
  SORT_VIEW_BOTTOM,
} from 'core/constants/index'
import { Range, Direction } from 'react-range'

class SortViewFeed extends Component {
  constructor(props) {
    super(props)
    this.state = {
      values: [0],
      sortViewDirection:
        window.innerWidth < 768 ? Direction.Right : Direction.Down,
    }
  }

  // event listener for resize...
  handleResize = () => {
    if (window.innerWidth < 768) {
      this.setState({ sortViewDirection: Direction.Right })
    } else {
      this.setState({ sortViewDirection: Direction.Down })
    }
  }

  componentDidMount = () => {
    // handle window resize and window width for conditionally show sort view...

    window.addEventListener('resize', this.handleResize)

    if (this.props.viewSortFeed) {
      if (this.props.viewSortFeed == SORT_VIEW_BOTTOM) {
        this.setState({
          values: [2],
        })
      }
      if (this.props.viewSortFeed == SORT_VIEW_MID) {
        this.setState({
          values: [1],
        })
      }
      if (this.props.viewSortFeed == SORT_VIEW_TOP) {
        this.setState({
          values: [0],
        })
      }
    }
  }

  handleViewType = (values) => {
    this.setState({ values })
    let type = SORT_VIEW_MID
    if (values[0] == 2) {
      type = SORT_VIEW_BOTTOM
      this.props.sortViewFeed(type)
    }
    if (values[0] == 1) {
      type = SORT_VIEW_MID
      this.props.sortViewFeed(type)
    }
    if (values[0] == 0) {
      type = SORT_VIEW_TOP
      this.props.sortViewFeed(type)
    }
    // const type =
    //   this.props.viewSort == SORT_VIEW_TOP
    //     ? SORT_VIEW_MID
    //     : this.props.viewSort == SORT_VIEW_MID
    //       ? SORT_VIEW_BOTTOM
    //       : SORT_VIEW_TOP;
  }

  render() {
    return (
      <div
        className={
          this.props.viewSortFeed == SORT_VIEW_TOP
            ? 'sort-view '
            : this.props.viewSortFeed == SORT_VIEW_MID
            ? 'sort-view '
            : 'sort-view '
        }
      >
        <div className="sm-text-fw4 d-inline-block d-md-none m-vi-txt text-uppercase">
          view
        </div>
        {/* <div
          className={
            this.props.viewSort == SORT_VIEW_TOP
              ? "view-line mb-4"
              : this.props.viewSort == SORT_VIEW_MID
                ? "view-line mb-4 mid-view-line"
                : "view-line mb-4 bottom-view-line"
          }
          onClick={this.handleViewType}
        > */}
        {/* for middle text.thats will be need add mid-view-line class in view-line*/}
        {/* for middle text.thats will be need add bottom-view-line class in view-line */}
        {/* <div className="view sm-text-fw6 sort text-uppercase">View</div> */}
        {/* </div> */}
        {/* <div className="ctm-select2">
          <span className="sm-text-fw6 sort text-uppercase ">
            {" "}
            <img src={ArDown} alt="img" /> Sort
          </span>
          <div className="ctm-select-box ctm-select-box2">
            <div className="ctm-select-option text-uppercase">Name</div>
          </div>
        </div> */}
        <Range
          direction={this.state.sortViewDirection}
          step={1}
          min={0}
          max={2}
          values={this.state.values}
          onChange={(values) => this.handleViewType(values)}
          renderTrack={({ props, children }) => (
            <div
              className="handle-line"
              {...props}
              style={{
                ...props.style,
                backgroundColor: 'transparent',
              }}
            >
              {children}
            </div>
          )}
          renderThumb={({ props }) => (
            <div
              {...props}
              style={{
                ...props.style,
                height: '5px',
                width: '4px',
                border: 'none',
                backgroundColor: 'black',
                outline: 'none',
                borderRadius: '0',
              }}
            >
              <span className="sm-text-fw6 text-uppercase d-none d-md-inline-block">
                {' '}
                <span className="short-v-view"> View </span>
              </span>
            </div>
          )}
        />
        <div className="sm-text-fw6 text-uppercase sort-v-text d-none d-md-inline-block">
          {' '}
          <img src={ArDown} alt="img" /> Sort
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    viewSortFeed: selectViewTypeFeed(state),
  }),
  {
    sortViewFeed,
  },
)(SortViewFeed)
