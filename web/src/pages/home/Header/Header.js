import "./Header.scss";

import {
  ROUTE_CART,
  ROUTE_CHECKOUT,
  ROUTE_CREATE_PRODUCT,
  ROUTE_CREATE_SHOP,
  ROUTE_HOME,
  ROUTE_LOGIN,
  ROUTE_MESSENGER,
  ROUTE_PRODUCTS,
  ROUTE_SELLER_PRODUCTS_LIST,
  ROUTE_SELLING_START,
  ROUTE_SHOP_LIST,
  ROUTE_SHOP_PROFILE_POSTS,
  ROUTE_SHOP_PROFILE_PRODUCTS,
  ROUTE_USER_OWN_PROFILE_FAVORITE,
  ROUTE_USER_OWN_PROFILE_POST
} from "../../../routes/constants";
import React, { Component } from "react";
import { cropString, getImageOriginalUrl } from "core/utils/helpers";
import { errorAlert, successAlert } from "../../../utils/alerts";
import { getCategories, updateCart } from "core/actions/productActionCreators";
import {
  getTmpProfileImage,
  getTmpShopProfileImage
} from "core/selectors/profile";
import {
  selectActiveConversation,
  selectAllConversations
} from "core/messenger/selectors";
import { selectActiveProfile, selectActiveShop } from "core/selectors/account";
import { selectCartData, selectCartPrice } from "core/selectors/cart";
import { selectIsLoggedIn, selectUser } from "core/selectors/user";

import ArDown from "./../../../images/icons/dropdown-icon.svg";
import Bar from "../../../images/icons/bar";
import Cart from "../../../images/icons/cart";
import CategoryList from "./categoryList";
import Close from "../../../images/icons/close";
import CloseSm from "../../../images/icons/closeSm";
import Comment from "../../../images/icons/comment";
import Down from "../../../images/icons/down";
import HeaderMessageList from "./messageList";
import Home from "../../../images/icons/home";
import { Link } from "react-router-dom";
import Message from "../../../images/icons/message";
import MessageSearch from "../../messenger/components/messageList/components/messageSearch";
import MobileMenu from "./mobileMenu";
import Notifications from "../../../images/icons/notifications";
import ProImg from "../../../images/login-img1.webp";
import SearchBox from "./searchBox";
import Shop from "../../../images/icons/shop";
import Star from "../../../images/icons/star";
import { connect } from "react-redux";
import { fetchConversations } from "core/messenger/actionCreators/conversationsActionCreators";
import { fetchFilterdProducts } from "core/actions/productsFilterActionCreators";
import { fetchShop } from "core/actions/shopActionCreators";
import { isEmpty } from "lodash";
import { logOut } from "core/actions/authActionCreators";
import logo from "../../../images/logo.svg";
import logoM from "../../../images/m-logo.svg";
import search_icon from "../../../images/icons/search-icon.svg";
import { selectCategories } from "core/selectors/products";
import { selectShops } from "core/selectors/shops";
import { setActiveShop } from "core/actions/accountActionCreators";
import { withRouter } from "react-router-dom";

class Header extends Component {
  state = {
    profileMenu: false,
    showModal: false,
    showCart: false,
    shopModal: false,
    shopList: false,
    catList: false,
    shopName: "",
    showMessage: false,
    messageCount: this.props.conversations.length
  };

  componentDidMount = () => {
    document.body.addEventListener("click", this.myHandler);
    window.addEventListener("scroll", this.handleScroll, true);
    this.props.fetchShop();
    this.props.getCategories();
  };
  componentWillUnmount() {
    document.body.removeEventListener("click", this.myHandler);
    window.removeEventListener("scroll", this.handleScroll);
  }
  myHandler = () => {
    this.setState({
      profileMenu: false,
      showCart: false,
      shopList: false,
      catList: false
    });
  };

  handleScroll = () => {
    if (window.pageYOffset > 25) {
      this.setState({
        headerFixed: true
      });
    } else {
      this.setState({
        headerFixed: false
      });
    }
  };

  activeShopProfile = shopName => {
    const allShops = this.props.allShops;
    const shop = allShops.filter(post => post.shopName == shopName);
    this.props.setActiveShop(shopName, shop[0]);
    this.clickOnName();

    this.props.history.push(
      ROUTE_SHOP_PROFILE_PRODUCTS.replace(":id", shop[0]._id)
    );
  };
  switchAccount = () => {
    if (!this.props.loggedInStatus) {
      this.props.history.push(ROUTE_LOGIN);
      return;
    }
    const allShops = this.props.allShops;
    const totalItem = allShops.length;
    if (totalItem == 0) {
      this.props.history.push(ROUTE_SELLING_START);
    } else if (totalItem == 1) {
      this.props.setActiveShop(allShops[0].shopName, allShops[0]);
      this.props.history.push(ROUTE_CREATE_PRODUCT);
    } else {
      this.toggleShopModal();
    }
  };
  toggleShopModal = () => {
    this.setState({ shopModal: !this.state.shopModal });
  };
  makeShopSelect = e => {
    this.setState({
      shopName: e.target.value
    });
  };
  selectedShopActive = e => {
    if (this.state.shopName == "") {
      errorAlert("Please select shop");
      return;
    }
    const allShops = this.props.allShops;
    const shopName = this.state.shopName;
    const shop = allShops.filter(post => post.shopName == shopName);
    this.props.setActiveShop(shopName, shop[0]);
    this.props.history.push(ROUTE_CREATE_PRODUCT);
  };

  logoutHandler = () => {
    this.props.logOut();
    this.props.history.push(ROUTE_HOME);
  };

  settings = () => {
    this.props.history.push("/user-settings");
  };

  clickOnName = () => {
    this.setState({
      profileMenu: !this.state.profileMenu
    });
  };

  createShop = e => {
    this.props.history.push(ROUTE_SELLING_START);
  };

  createProduct = e => {
    this.props.history.push(ROUTE_CREATE_PRODUCT);
  };

  gotToProfile = e => {
    this.props.setActiveShop(undefined, undefined);
    this.props.history.push(ROUTE_USER_OWN_PROFILE_POST);
  };
  goAllShop = e => {
    this.props.history.push(ROUTE_SHOP_LIST);
  };

  checkout = e => {
    this.props.history.push(ROUTE_CHECKOUT);
  };

  removeFromCart = key => {
    const updatedCart = [...this.props.cartData];
    const finalPrice = this.props.cartPrice;
    const updatedfinalPrice = +finalPrice - +updatedCart[key].grandTotal;
    // delete updatedCart[key];
    updatedCart.splice(key, 1);
    this.props.updateCart(updatedCart, updatedfinalPrice);
  };

  onCategoryFilter = (key = "category", category) => {
    console.log("category filter: ", category);

    const productFilterRoute = ROUTE_PRODUCTS.replace(
      ":category/:subCategory?/:innerCategory?",
      category.slug
    );
    // call products filtering api with key and query id ...
    this.props.fetchFilterdProducts(key, category._id);
    this.props.history.push(productFilterRoute);
  };

  onSubCategoryFilter = (key = "subCategory", category, subCategory) => {
    console.log("sub-category filter: ", subCategory);
    const productFilterRoute = ROUTE_PRODUCTS.replace(
      ":category",
      category.slug
    ).replace(":subCategory?/:innerCategory?", subCategory.slug);
    // call products filtering api with key and query id ...
    this.props.fetchFilterdProducts(key, subCategory._id);
    this.props.history.push(productFilterRoute);
  };

  onInnerCategoryFilter = (
    key = "innerCategory",
    category,
    subCategory,
    innerCategory
  ) => {
    console.log("inner-category filter: ", subCategory);
    const productFilterRoute = ROUTE_PRODUCTS.replace(
      ":category",
      category.slug
    )
      .replace(":subCategory?", subCategory.slug)
      .replace(":innerCategory?", innerCategory.slug);
    // call products filtering api with key and query id ...
    this.props.fetchFilterdProducts(key, innerCategory._id);
    this.props.history.push(productFilterRoute);
  };
  openModal = event => {
    document.body.classList.add("modal-open");
    this.setState({ showModal: true });
  };
  hideModal = event => {
    document.body.classList.remove("modal-open");
    this.setState({ showModal: false });
  };
  render() {
    var counting = this.props.conversations.map(count => {
      return count.isWindowOpen;
    });
    var count = this.props.conversations.length;
    var countFlag = false;
    return (
      <div
        className={
          this.state.headerFixed
            ? "header-section fixed-header"
            : "header-section"
        }
      >
        <div className="max-body-content">
          <nav className="navbar navbar-expand-lg p-0">
            <Link className="navbar-brand" to="/">
              <img
                src={logo}
                className="d-img d-none d-md-inline-block"
                alt="img"
              />
              <img src={logoM} className="d-md-none d-inline-block" alt="img" />
            </Link>
            <div className="collapse navbar-collapse f-nav d-none d-md-flex">
              <ul className="navbar-nav align-items-center">
                {this.props.categories &&
                  this.props.categories.map(category => {
                    return (
                      <li
                        key={category._id}
                        className="nav-item box-show cat-li"
                      >
                        <span
                          className="nav-link cursor-pointer"
                          onClick={() =>
                            this.onCategoryFilter("category", category)
                          }
                        >
                          {category.name}
                        </span>
                        <CategoryList
                          subCategory={category.subCategory}
                          filter={this.onCategoryFilter}
                          onSubCategoryFilter={this.onSubCategoryFilter}
                          onInnerCategoryFilter={this.onInnerCategoryFilter}
                          category={category}
                          key={category._id}
                        />
                      </li>
                    );
                  })}

                <li className="nav-item">
                  <div className="input-group search-group li-sg">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <img src={search_icon} alt="img" />
                      </span>
                    </div>
                    <input
                      type="text"
                      className="form-control custom-input"
                      placeholder="Search"
                    />
                  </div>
                  <SearchBox />
                </li>
              </ul>
            </div>
            <div className="collapse navbar-collapse justify-content-center d-none d-md-flex">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <Link className="nav-link" to="/">
                    <Home />
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to={ROUTE_SHOP_LIST}>
                    <Shop />
                  </Link>
                </li>
                {this.props.loggedInStatus && (
                  <li className="nav-item">
                    <Link
                      className="nav-link"
                      to={ROUTE_USER_OWN_PROFILE_FAVORITE}
                    >
                      <Star />
                      <span className="count-n">14</span>
                    </Link>
                  </li>
                )}

                <li
                  className={
                    this.state.showCart
                      ? "nav-item box-show active"
                      : "nav-item box-show"
                  }
                >
                  {/* for cart-list show needed to add active class in box-show class */}
                  <span
                    onClick={() =>
                      this.setState({
                        showCart: !this.state.showCart
                      })
                    }
                    className="nav-link cursor-pointer"
                  >
                    <Cart />
                    <span className="count-n">
                      {this.props.cartData.length
                        ? this.props.cartData.length
                        : 0}
                    </span>
                  </span>
                  <div className="box-cnt2 cart-sm-box">
                    <span className="box-arr">
                      <Down />
                    </span>
                    <div className="box-ul">
                      {this.props.cartData &&
                        this.props.cartData.map((cartItem, index) => {
                          return (
                            <div className="box-li pl-4" key={index}>
                              <span
                                className="close-cart scale-hover cursor-pointer"
                                onClick={() => this.removeFromCart(index)}
                              >
                                <Close />
                              </span>
                              <div className="pro-cnt align-items-center ">
                                <div className="pro-img pro-img-r0">
                                  <img
                                    className="cover"
                                    src={
                                      cartItem.color &&
                                      getImageOriginalUrl(
                                        cartItem.color.colorImage
                                      )
                                    }
                                    alt="img"
                                  />
                                </div>
                                <div className="pro-txt-cnt pro-txt-cnt-r0 align-items-center d-flex">
                                  <div className="sm-text-fw4 mr-2">
                                    {cartItem.quantity}x
                                  </div>
                                  <div>
                                    <div className="font-weight-bold text-uppercase mb-1">
                                      {cartItem.product &&
                                        cartItem.product.title.slice(0, 20)}
                                    </div>
                                    <div className="sm-text-fw6-g">
                                      € {cartItem.color && cartItem.color.price}
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          );
                        })}
                      <div className="box-li text-right">
                        <span className="sm-text-fw6 text-uppercase mr-3">
                          Total:
                        </span>
                        <span className="font-weight-bold text-uppercase ">
                          € {this.props.cartPrice}
                        </span>
                      </div>
                      <span className="cursor-pointer ">
                        <Link to={ROUTE_CART}>View cart</Link>
                      </span>
                    </div>
                    <button
                      className="btn border-btn w-100 black-bg-btn text-uppercase"
                      onClick={this.checkout}
                    >
                      Proceed to checkout
                    </button>
                  </div>
                </li>

                {this.props.loggedInStatus && (
                  <li
                    className={
                      this.state.showMessage
                        ? "nav-item box-show msg-box-li active"
                        : "nav-item box-show msg-box-li"
                    }
                  >
                    {/* <Link className="nav-link" to={ROUTE_MESSENGER}>
                      <Comment />
                    </Link> */}
                    <span
                      onClick={() => {
                        this.setState({
                          showMessage: !this.state.showMessage
                        });
                        this.props.conversations.length === 0 && this.props.fetchConversations(this.props.user._id);
                      }}
                      className="nav-link cursor-pointer "
                    >
                      <span className="count-n">
                        {counting[0] == false
                          ? this.props.conversations.length
                          : "0"}
                      </span>
                      <Comment onClick={() => (countFlag = true)} />
                    </span>
                    <HeaderMessageList />
                  </li>
                )}
              </ul>
            </div>
            <div className="collapse navbar-collapse justify-content-end">
              <ul className="navbar-nav align-items-center">
                {this.props.loggedInStatus &&
                  this.props.activeShop &&
                  this.props.activeShop._id && (
                    <li className="nav-item d-md-inline-flex d-none">
                      <Link
                        className="nav-link"
                        to={ROUTE_SELLER_PRODUCTS_LIST}
                      >
                        Dashboard
                      </Link>
                    </li>
                  )}
                <li className="nav-item  d-inline-flex d-md-none">
                  <span className="nav-link cursor-pointer">
                    <img src={search_icon} alt="img" />
                  </span>
                </li>
                <li className="nav-item cursor-pointer d-md-inline-flex d-none">
                  <span
                    className="nav-link"
                    onClick={() => this.switchAccount()}
                  >
                    Sell
                  </span>
                </li>

                {this.props.loggedInStatus && (
                  <li
                    className={
                      this.state.showMessage
                        ? "nav-item box-show msg-box-li active"
                        : "nav-item box-show msg-box-li"
                    }
                  >
                    {/* <Link className="nav-link" to={ROUTE_MESSENGER}>
                      <Comment />
                    </Link> */}
                    <span
                      onClick={() =>
                        this.setState({
                          showMessage: !this.state.showMessage
                        })
                      }
                      className="nav-link cursor-pointer "
                    >
                      <span className="count-n">
                        {counting[0] == false
                          ? this.props.conversations.length
                          : "0"}
                      </span>
                      <Message onClick={() => (countFlag = true)} />
                    </span>
                    <HeaderMessageList />
                  </li>
                )}
                {this.props.loggedInStatus && (
                  <li className="nav-item">
                    <span className="nav-link cursor-pointer">
                      <Notifications />
                    </span>
                  </li>
                )}
                <li
                  className={
                    this.state.showCart
                      ? "nav-item box-show d-inline-flex d-md-none active"
                      : "nav-item box-show  d-inline-flex d-md-none"
                  }
                >
                  {/* for cart-list show needed to add active class in box-show class */}
                  <span
                    onClick={() =>
                      this.setState({
                        showCart: !this.state.showCart
                      })
                    }
                    className="nav-link cursor-pointer"
                  >
                    <Cart />
                    <span className="count-n">
                      {this.props.cartData.length
                        ? this.props.cartData.length
                        : 0}
                    </span>
                  </span>
                  <div className="box-cnt2 cart-sm-box">
                    <span className="box-arr">
                      <Down />
                    </span>
                    <div className="box-ul">
                      {this.props.cartData &&
                        this.props.cartData.map((cartItem, index) => {
                          return (
                            <div className="box-li pl-4" key={index}>
                              <span
                                className="close-cart scale-hover cursor-pointer"
                                onClick={() => this.removeFromCart(index)}
                              >
                                <Close />
                              </span>
                              <div className="pro-cnt align-items-center ">
                                <div className="pro-img pro-img-r0">
                                  <img
                                    className="cover"
                                    src={
                                      cartItem.color &&
                                      cartItem.color.colorImage
                                    }
                                    alt="img"
                                  />
                                </div>
                                <div className="pro-txt-cnt pro-txt-cnt-r0 align-items-center d-flex">
                                  <div className="sm-text-fw4 mr-2">
                                    {cartItem.quantity}x
                                  </div>
                                  <div>
                                    <div className="font-weight-bold text-uppercase mb-1">
                                      {cartItem.product &&
                                        cartItem.product.title.slice(0, 20)}
                                    </div>
                                    <div className="sm-text-fw6-g">
                                      € {cartItem.color && cartItem.color.price}
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          );
                        })}
                      <div className="box-li text-right">
                        <span className="sm-text-fw6 text-uppercase mr-3">
                          Total:
                        </span>
                        <span className="font-weight-bold text-uppercase ">
                          € {this.props.cartPrice}
                        </span>
                      </div>
                      <span className="cursor-pointer ">
                        <Link to={ROUTE_CART}>View cart</Link>
                      </span>
                    </div>
                    <button
                      className="btn border-btn w-100 black-bg-btn text-uppercase"
                      onClick={this.checkout}
                    >
                      Proceed to checkout
                    </button>
                  </div>
                </li>
                {!this.props.loggedInStatus && (
                  <li className="nav-item">
                    <Link className="nav-link" to="/login">
                      Login
                    </Link>
                  </li>
                )}

                {this.props.loggedInStatus && (
                  <li className="nav-item d-none d-md-inline-flex set-li">
                    <div className="pro-cnt align-items-center ">
                      <div className="pro-img cursor-pointer">
                        <Link to="/profile">
                          <img
                            className="cover"
                            src={
                              !isEmpty(this.props.activeShop)
                                ? this.props.activeShop.profileImage
                                  ? getImageOriginalUrl(
                                      this.props.activeShop.profileImage
                                    )
                                  : ProImg
                                : this.props.tempProfileImage &&
                                  this.props.tempProfileImage
                                ? getImageOriginalUrl(
                                    this.props.tempProfileImage
                                  )
                                : this.props.user &&
                                  this.props.user.profileImage
                                ? getImageOriginalUrl(
                                    this.props.user.profileImage,
                                    "80_80_"
                                  )
                                : ProImg
                            }
                            alt="img"
                          />
                        </Link>
                      </div>
                      <div className="pro-txt-cnt">
                        <div
                          className="sm-text-fw6 text-uppercase cursor-pointer"
                          onClick={this.clickOnName}
                        >
                          {!isEmpty(this.props.activeShop)
                            ? cropString(this.props.activeShop.shopName, 10)
                            : cropString(this.props.user.fullName, 10)}
                          <img className="ml-1" src={ArDown} alt="img" />
                        </div>
                      </div>
                      <div
                        className={
                          this.state.profileMenu
                            ? "ctm-select-box ctm-select-box2 sett-box d-block"
                            : "ctm-select-box ctm-select-box2 sett-box"
                        }
                      >
                        <div
                          className="ctm-select-option"
                          onClick={this.gotToProfile}
                        >
                          View Profile{" "}
                        </div>
                        {this.props.allShops &&
                          this.props.allShops.map((shop, index) => {
                            return (
                              <div
                                className="ctm-select-option list-li"
                                onClick={() =>
                                  this.activeShopProfile(shop.shopName)
                                }
                                key={shop._id}
                              >
                                <img
                                  className="cover"
                                  src={
                                    this.props.tmpShopProfile
                                      ? getImageOriginalUrl(
                                          this.props.tmpShopProfile
                                        )
                                      : shop && shop.profileImage
                                      ? getImageOriginalUrl(
                                          shop.profileImage,
                                          "40_40_"
                                        )
                                      : ProImg
                                  }
                                  alt="img"
                                />{" "}
                                {cropString(shop.shopName, 15)}
                              </div>
                            );
                          })}

                        <div
                          className="ctm-select-option"
                          onClick={this.createShop}
                        >
                          Create Shop{" "}
                        </div>

                        {this.props.activeShop && this.props.activeShop._id && (
                          <div
                            className="ctm-select-option"
                            onClick={this.createProduct}
                          >
                            Create Product{" "}
                          </div>
                        )}
                        <div
                          className="ctm-select-option"
                          onClick={this.settings}
                        >
                          Profile Settings{" "}
                        </div>
                        <div
                          className="ctm-select-option"
                          onClick={this.logoutHandler}
                        >
                          Logout{" "}
                        </div>
                      </div>
                    </div>
                  </li>
                )}
                {!this.props.loggedInStatus && (
                  <li className="nav-item d-md-inline-flex d-none">
                    <Link className="nav-link" to="register">
                      Signup
                    </Link>
                  </li>
                )}
                <li className="nav-item d-inline-flex d-md-none">
                  <span
                    className={
                      this.state.showModal ? "nav-link show-m-nav" : "nav-link"
                    }
                  >
                    <span onClick={event => this.hideModal()} className="cl">
                      <CloseSm />
                    </span>
                    <span onClick={event => this.openModal()} className="bar">
                      <Bar />
                    </span>
                  </span>
                </li>
                {/* {this.props.loggedInStatus && (
                  <li className="nav-item">
                    <span
                      className="nav-link cursor-pointer"
                      onClick={this.logoutHandler}
                    >
                      Logout
                    </span>
                  </li>
                )} */}
              </ul>
            </div>
          </nav>
        </div>
        <div
          className={
            this.state.showModal
              ? "mobile-menu d-block d-md-none"
              : "mobile-menu d-none"
          }
        >
          <MobileMenu />
        </div>
        <div
          className={
            this.state.shopModal ? "modal fade d-block show" : "modal fade"
          }
          id="shopModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content p-4">
              <div className="modal-header pt-0 px-0">
                <div className="sm-text-fw6 text-uppercase"> Select Shop</div>
                <span
                  className="cursor-pointer"
                  aria-hidden="true"
                  onClick={this.toggleShopModal}
                >
                  <Close />
                </span>
              </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-md-12" />
                  {this.props.allShops &&
                    this.props.allShops.map((shop, index) => {
                      return (
                        <div className="col-md-12 mb-2" key={shop._id}>
                          <label className="ctm-container radio-ctm d-inline-block">
                            <div className="x-sm-text-fw6 text-uppercase">
                              {shop.shopName}
                            </div>
                            <input
                              type="radio"
                              name="shopName"
                              id={"exampleRadios" + index}
                              value={shop.shopName}
                              onChange={this.makeShopSelect}
                            />
                            <span className="checkmark checkmark-midle" />
                          </label>
                          {/* <div className="form-check">
                            <input
                              className="form-check-input"
                              type="radio"
                              name="shopName"
                              id={"exampleRadios" + index}
                              value={shop.shopName}
                              onChange={this.makeShopSelect}
                            />
                            <label
                              className="form-check-label"
                              htmlfor={"exampleRadios" + index}
                            >
                              {shop.shopName}
                            </label>
                          </div> */}
                        </div>
                      );
                    })}
                </div>
              </div>
              <div className="modal-footer  px-0 pb-0">
                <button
                  type="button"
                  className="btn border-btn btn-w-115"
                  onClick={this.selectedShopActive}
                >
                  Save
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    loggedInStatus: selectIsLoggedIn(state),
    user: selectUser(state),
    allShops: selectShops(state),
    activeProfile: selectActiveProfile(state),
    activeShop: selectActiveShop(state),
    cartData: selectCartData(state),
    cartPrice: selectCartPrice(state),
    categories: selectCategories(state),
    conversations: selectAllConversations(state),
    activeConversation: selectActiveConversation(state),
    tempProfileImage: getTmpProfileImage(state),

    tmpShopProfile: getTmpShopProfileImage(state)
  }),
  {
    logOut,
    fetchShop,
    setActiveShop,
    updateCart,
    getCategories,
    fetchFilterdProducts,
    fetchConversations
  }
)(withRouter(Header));
