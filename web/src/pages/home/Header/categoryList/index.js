import React, { Component } from 'react'

import Down from '../../../../images/icons/down'
import { ROUTE_PRODUCTS } from '../../../../routes/constants'
import Right from '../../../../images/icons/right'
import { withRouter } from 'react-router'

class CategoryList extends Component {
  state = {
    subCategory: '',
    innerCategory: [],
    showInner: false,
  }
  innerCategory = (subCatId) => {
    const subCategory = this.props.subCategory
    const inner = subCategory.filter((item) => item._id == subCatId)
    this.setState({
      innerCategory: inner[0].innerCategory,
      showInner: true,
      subCategory: inner[0],
    })
  }
  goProducListPage = (subCategory) => {
    console.log('subcategory filter: ', subCategory)
    this.props.history.push(ROUTE_PRODUCTS)
  }
  render() {
    return (
      <div className="box-cnt2 cart-sm-box list-box">
        <span className="box-arr">
          <Down />
        </span>
        <div
          className="cat-list border-0"
          onMouseLeave={() => this.setState({ showInner: false })}
        >
          <div className="row mx-0">
            <div className="col-12 col-md-4 px-0 cat-list-col">
              <div
                className="cat sm-text-fw6-g text-uppercase cursor-pointer"
                onClick={() =>
                  this.props.filter('category', this.props.category)
                }
              >
                <span className="mr-2">See all</span> <Right />
              </div>
              {this.props.subCategory &&
                this.props.subCategory.map((subCat, index) => {
                  return (
                    <div
                      key={`subCat_${subCat._id}_${index}`}
                      className="cat sm-text-fw6-g text-uppercase cursor-pointer"
                      onMouseEnter={() => this.innerCategory(subCat._id)}
                      onClick={() =>
                        this.props.onSubCategoryFilter(
                          'subCategory',
                          this.props.category,
                          subCat,
                        )
                      }
                    >
                      <span className="mr-2">{subCat.name}</span> <Right />
                    </div>
                  )
                })}
            </div>
            <div
              className={
                this.state.showInner
                  ? 'col-12 col-md-8 px-0'
                  : 'col-12 col-md-8 px-0 d-none'
              }
            >
              <div className="cat-cnt inner-cat pl-4 pr-0">
                <div className="row mr-0">
                  <div className="col-12 col-lg-6 pr-0">
                    <div
                      className="cat sm-text-fw6-g text-uppercase cursor-pointer"
                      onClick={() =>
                        this.props.onSubCategoryFilter(
                          'subCategory',
                          this.props.category,
                          this.state.subCategory,
                        )
                      }
                    >
                      <span className="mr-2">See all</span> <Right />
                    </div>
                  </div>
                  {this.state.innerCategory &&
                    this.state.innerCategory.map((innerCat) => {
                      return (
                        <div className="col-12 col-lg-6 pr-0">
                          <div
                            className="cat sm-text-fw6-g text-uppercase cursor-pointer"
                            onClick={() =>
                              this.props.onInnerCategoryFilter(
                                'innerCategory',
                                this.props.category,
                                this.state.subCategory,
                                innerCat,
                              )
                            }
                          >
                            <span className="mr-2">{innerCat.name}</span>{' '}
                            <Right />
                          </div>
                        </div>
                      )
                    })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(CategoryList)
