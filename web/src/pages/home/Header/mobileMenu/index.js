import React, { Component } from "react";
import { Link } from "react-router-dom";
import Home from "../../../../images/icons/home";
import Right from "../../../../images/icons/right";
import S from "../../../../images/icons/s";
import Shop from "../../../../images/icons/shop";
import search_icon from "../../../../images/icons/search-icon.svg";
import "./mobileMenu.scss";
import Settings from "../../../../images/icons/settings";
import Dashbord from "../../../../images/icons/dashbord";
import Doller from "../../../../images/icons/doller";
import ProImg from "../../../../images/login-img1.webp";
import Down from "../../../../images/icons/down";
import HorizontalSelectOption from "../../../userSettings/components/horizontalSelectOption";
class MobileMenu extends Component {
  state = {
    liShow: false
  };
  handleInputChange = (name, value) => {
    this.setState({
      [name]: value
    });
  };
  render() {
    return (
      <div className="">
        <div className="mb-3 pb-3">
          <span className="sm-text-fw6-g text-uppercase mr-3">Category</span>
          <Link to="/" className="sm-text-fw6 text-uppercase mr-3">
            Man
          </Link>
          <Link to="/" className="sm-text-fw6 text-uppercase">
            Woman
          </Link>
        </div>
        <div className="input-group search-group mb-3">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <img src={search_icon} alt="img" />
            </span>
          </div>
          <input
            type="text"
            className="form-control custom-input"
            placeholder="Search"
          />
        </div>
        <ul className="nav nav-pills ">
          <li className="nav-item">
            <Link
              to="/"
              className="nav-link sm-text-fw6 text-uppercase d-flex align-items-center"
            >
              <span className="mr-2">
                <Home />
              </span>
              Home
              <span className="mm-arr">
                <Right />
              </span>
            </Link>
          </li>
          <li className="nav-item">
            <Link
              to="/"
              className="nav-link sm-text-fw6 text-uppercase d-flex align-items-center"
            >
              <span className="mr-2">
                <Shop />
              </span>
              shops
              <span className="mm-arr">
                <Right />
              </span>
            </Link>
          </li>
          <li className="nav-item">
            <span
              onClick={() =>
                this.setState({
                  liShow: !this.state.liShow
                })
              }
              className={
                this.state.liShow
                  ? "nav-link sm-text-fw6 text-uppercase d-flex align-items-center active"
                  : "nav-link sm-text-fw6 text-uppercase d-flex align-items-center"
              }
            >
              <span className="mr-2">
                <S />
              </span>
              Socialogue
              <span className="mm-arr">
                <Right />
              </span>
            </span>
            <ul
              className={
                this.state.liShow
                  ? "nav nav-pills ml-4 mt-3"
                  : "nav nav-pills ml-4 mt-3 d-none"
              }
            >
              <li className="nav-item">
                <Link
                  to="/"
                  className="nav-link border-0 sm-text-fw6 text-uppercase d-flex align-items-center"
                >
                  About Socialogue
                  <span className="mm-arr">
                    <Right />
                  </span>
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  to="/"
                  className="nav-link border-0 sm-text-fw6 text-uppercase d-flex align-items-center"
                >
                  Start Selling
                  <span className="mm-arr">
                    <Right />
                  </span>
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  to="/"
                  className="nav-link border-0 sm-text-fw6 text-uppercase d-flex align-items-center"
                >
                  FAQ
                  <span className="mm-arr">
                    <Right />
                  </span>
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  to="/"
                  className="nav-link border-0 sm-text-fw6 text-uppercase d-flex align-items-center"
                >
                  PRIVACY POLICY
                  <span className="mm-arr">
                    <Right />
                  </span>
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  to="/"
                  className="nav-link border-0 sm-text-fw6 text-uppercase d-flex align-items-center"
                >
                  TERMS & CONDITIONS
                  <span className="mm-arr">
                    <Right />
                  </span>
                </Link>
              </li>
            </ul>
          </li>
        </ul>
        <div className="pro-cnt align-items-center mb-4 mt-5">
          <div className="pro-img cursor-pointer">
            <Link to="/profile">
              <img className="cover" src={ProImg} alt="img" />
            </Link>
          </div>
          <div className="pro-txt-cnt">
            <div className="sm-text-fw6 text-uppercase cursor-pointer">
              UNiq butiq
              <span className="ml-2">
                <Down />
              </span>
              <span className="float-right">SWITCH USER</span>
            </div>
          </div>
        </div>
        <ul className="nav nav-pills">
          <li className="nav-item">
            <Link
              to="/"
              className="nav-link sm-text-fw6 text-uppercase d-flex align-items-center"
            >
              <span className="mr-2">
                <Doller />
              </span>
              Sell
              <span className="mm-arr">
                <Right />
              </span>
            </Link>
          </li>
          <li className="nav-item">
            <Link
              to="/"
              className="nav-link sm-text-fw6 text-uppercase d-flex align-items-center"
            >
              <span className="mr-2">
                <Dashbord />
              </span>
              dashboard
              <span className="mm-arr">
                <Right />
              </span>
            </Link>
          </li>
          <li className="nav-item">
            <Link
              to="/"
              className="nav-link sm-text-fw6 text-uppercase d-flex align-items-center"
            >
              <span className="mr-2">
                <Settings />
              </span>
              settings
              <span className="mm-arr">
                <Right />
              </span>
            </Link>
          </li>
        </ul>
        <div className="sm-text-fw6 text-uppercase mt-5 mb-2 lan-sc-d d-flex align-items-center">
          <span className="single-group hide-label mb-0 w-100">
            <HorizontalSelectOption
              formInputClass="form-input position-relative"
              label=""
              name="lan"
              errorText=""
              isRequired={false}
              imageRequired={false}
              selectedText="language"
              options={["LT", "EN", "US"]}
              onChange={this.handleInputChange}
            />
          </span>
          <span className="float-right">LOGout</span>
        </div>
      </div>
    );
  }
}

export default MobileMenu;
