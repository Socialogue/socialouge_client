import React, { Component } from "react";

class SearchBox extends Component {
  render() {
    return (
      <div className="box-cnt2 cart-sm-box search-box">
        <div className="pro-cnt pro-cnt-wr align-items-center">
          <div className="pro-img">
            <img
              className="cover"
              src="http://placeimg.com/640/480"
              alt="img"
            />
          </div>
          <div className="pro-txt-cnt">
            <div className="x-sm-text-fw6">
              Classic <span className="color-orange">shirt</span>{" "}
            </div>
            <div>
              <span className="sm-text-fw4-g cat-s">Cloth</span>
              <span className="sm-text-fw4-g cat-s">Cloth</span>
            </div>
          </div>
        </div>
        <div className="pro-cnt align-items-center">
          <div className="pro-img">
            <img
              className="cover"
              src="http://placeimg.com/640/480"
              alt="img"
            />
          </div>
          <div className="pro-txt-cnt">
            <div className="x-sm-text-fw6">
              Classic <span className="color-orange">shirt</span>{" "}
            </div>
            <div>
              <span className="sm-text-fw4-g cat-s">Users</span>
              <span className="sm-text-fw4-g cat-s">Cloth</span>
            </div>
          </div>
        </div>
        <div className="pro-cnt align-items-center">
          <div className="pro-txt-cnt pl-0">
            <div className="x-sm-text-fw6">
              Classic <span className="color-orange">shirt</span>{" "}
            </div>
            <div>
              <span className="sm-text-fw4-g cat-s">Blogs</span>
              <span className="sm-text-fw4-g cat-s">Cloth</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SearchBox;
