import React, { Component } from "react";
import Down from "../../../../images/icons/down";
import { connect } from "react-redux";
import { selectIsLoggedIn, selectUser } from "core/selectors/user";
import { fetchPosts } from "core/actions/postActionCreators";
import { openChatWindow } from "core/messenger/messageActionCreators";
import ChatWindow from "../../../messenger/components/ChatWindow";
import MessageSearch from "../../../messenger/components/messageList/components/messageSearch";
import ProImg from "../../../../images/login-img1.webp";
import { Link } from "react-router-dom";
import { fetchConversations } from "core/messenger/actionCreators/conversationsActionCreators";
import {
  selectActiveConversation,
  selectAllConversations
} from "core/messenger/selectors";
import { ROUTE_MESSENGER } from "../../../../routes/constants";
import MessageList from "../../../messenger/components/messageList";

class HeaderMessageList extends Component {
  onClickMessage = userId => {
    if (!this.props.isLoggedIn) {
      this.props.history.push(ROUTE_LOGIN);
    } else {
      console.log("error", this.props.shop);
      this.props
        .openChatWindow
        //this.props.match.params.id,
        //this.props.shop.shopName,
        //this.props.shop.profileImage
        ();
    }
  };

  render() {
    return (
      <div className="box-cnt2 cart-sm-box message-box">
        <span className="box-arr">
          <Down />
        </span>
        <div className="box-ul p-0">
          <Link
           
            to={ROUTE_MESSENGER}
          >
            <MessageList conversations={this.props.conversations} />
          </Link>

          {/*<div onClick={this.onClickMessage}>
            
    </div>*/}

          <div className="p-3 text-center see-all-m">
            <Link
              className="cursor-pointer sm-text-fw6 text-uppercase"
              to={ROUTE_MESSENGER}
            >
              See all in messenger
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    conversations: selectAllConversations(state),
    activeConversation: selectActiveConversation(state),
    loggedInUser: selectUser(state),
    isLoggedIn: selectIsLoggedIn(state)
  }),
  {
    fetchPosts,
    openChatWindow,
    fetchConversations
  }
)(HeaderMessageList);
