import React from 'react';
import {
  Link
} from 'react-router-dom';
import './FilterMenu.scss';

const FilterMenu = () => (
  <div className='filter-menu d-none d-md-block'>
    <div className='filter-tlt'>FilterBy</div>
    <nav className='navbar navbar-expand-lg p-0'>
      <div className='collapse navbar-collapse'>
        <ul className='navbar-nav  d-block'>
          <li className='nav-item'>
            <Link className='nav-link' to='#'>Man</Link>
          </li>
          <li className='nav-item active'>
            <Link className='nav-link' to='#'>Woman</Link>
          </li>
          <li className='nav-item'>
            <Link className='nav-link' to='#'>view all</Link>
          </li>
        </ul>
      </div>
    </nav>
  </div>

);

export default FilterMenu;
