import React, { Component } from "react";
import arrow_down_icon from "../../../../images/icons/arrow-down.svg";
import EcoImagesOne from "../../../../images/eco-img.webp";
import EcoImagesTwo from "../../../../images/eco-img1.webp";
import EcoImagesThree from "../../../../images/eco-img2.webp";
import EcoImagesF from "../../../../images/eco-img3.webp";
import EcoImagesFive from "../../../../images/eco-img4.webp";
import "./EcoBrandTitle.scss";
class EcoBrandTitle extends Component {
  componentDidMount() {
    this.myTimeout1();
  }
  myTimeout1() {
    var ww = window.innerWidth;
    const che = document.querySelector(".animation-1");
    var numberC = Math.floor(Math.random() * 4) + 2;
    if (che) {
      if (ww > 767) {
        document.querySelector(".animation-1").classList.add("active");
        document.querySelector(".animation-4").classList.add("active");
        document.querySelector(".animation-2").classList.remove("active");
        document.querySelector(".animation-3").classList.remove("active");
        this.timeout = setTimeout(() => {
          this.myTimeout2();
        }, 6000 * numberC);
      } else {
        document.querySelector(".animation-3").classList.add("active");
        document.querySelector(".animation-4").classList.add("active");
      }
    }
  }
  myTimeout2() {
    const che = document.querySelector(".animation-1");
    var numberC = Math.floor(Math.random() * 4) + 2;
    if (che) {
      document.querySelector(".animation-2").classList.add("active");
      document.querySelector(".animation-3").classList.add("active");
      document.querySelector(".animation-1").classList.remove("active");
      document.querySelector(".animation-4").classList.remove("active");
      this.timeout = setTimeout(() => {
        this.myTimeout1();
      }, 6000 * numberC);
    }
  }
  render() {
    return (
      <div className="max-body-content">
        <div className="module-title-cnt">
          <div className="module-title-top-img mb-5 d-none d-md-block">
            <div className="d-flex">
              <div className="td-img-1 animation-1">
                <div className="show-img-pp">
                  <div className="show-img-p">
                    <img className="cover" src={EcoImagesOne} alt="img" />
                  </div>
                </div>
              </div>
              <div className="td-img-2" />
              <div className="td-img-3 animation-2">
                <div className="show-img-pp">
                  <div className="show-img-p">
                    <img className="cover" src={EcoImagesTwo} alt="img" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="module-title eco-tlt">
            <div className="main-title text-center">
              <div className="lg-title">
                unique eco-brands
                <span className="dot-r" />
              </div>
              <div className="lg-text mb-2">social marketplace catalogue</div>
              <div className="">
                {" "}
                <span className="cursor-pointer">
                  <img src={arrow_down_icon} alt="img" />
                </span>
              </div>
            </div>
            <div className="eco-tlt-with-img d-flex">
              <div className="eco-tlt-col-1 animation-3">
                <div className="eco-tlt-img-top">
                  <div className="show-img-pp">
                    <div className="show-img-p">
                      <img className="cover" src={EcoImagesF} alt="img" />
                    </div>
                  </div>
                </div>
                <div className="col-img-2">
                  <div className="show-img-pp">
                    <div className="show-img-p">
                      <img className="cover" src={EcoImagesThree} alt="img" />
                    </div>
                  </div>
                </div>
              </div>
              <div className="eco-tlt-col-2" />
              <div className="eco-tlt-col-3 pt-4 pt-md-0">
                <div className="bi-cnt animation-4">
                  <div className="show-img-pp">
                    <div className="show-img-p">
                      <img className="cover" src={EcoImagesFive} alt="img" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EcoBrandTitle;
