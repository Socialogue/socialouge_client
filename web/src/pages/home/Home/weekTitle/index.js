import React, { Component } from "react";
import ArrowRight from "../../../../images/icons/arrowRight";

class WeekTitle extends Component {
  render() {
    return (
      <div className="home-title week-title position-relative">
        <div className="home-content">
          <div className="home-title-cnt mx-0">
            <div className="xx-md-title color-white position-relative">
              look of the week<span className="dot-r" />
            </div>
            <div className="sm-text-fw5 color-white seller-txt">
              <span className="position-relative lg-text-fw5">
                Get the look{" "}
            <br className="d-block d-md-none" />
                <span className="seller-ar ml-0 ml-md-0 mt-1 mt-md-0">
                  <ArrowRight />
                </span>
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default WeekTitle;
