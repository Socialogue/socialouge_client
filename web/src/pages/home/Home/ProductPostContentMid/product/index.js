import React, { Component } from 'react'
import Love from '../../../../../images/icons/love'
import Comment from '../../../../../images/icons/comment'
import ArrowRight from '../../../../../images/icons/arrowRight'
import Star from '../../../../../images/icons/star'
import Plus from '../../../../../images/icons/plus'
import Img from '../../../../../images/register-img.webp'
import { Link } from 'react-router-dom'
import Share from '../../../../../images/icons/share'
import moment from 'moment'
import Like from '../../../../../components/Like'
import Favorite from '../../../../../components/Favorite'

class Product extends Component {
  render() {
    const { product } = this.props
    if (!product) {
      return null
    }
    return (
      <div class="col-12 col-md-6 product mb-5">
        <div className="show-img-pp show-img-pp-pb-140 mb-3">
          <div
            className="show-img-p cursor-pointer"
            onClick={() => this.props.viewProduct(product.slug, product._id)}
          >
            <div className="discount">-20%</div>
            {/* // TODO for nadim: need change image url when api give only image name... */}
            <img className="cover" src={product.mainImage} alt="img" />
          </div>
        </div>
        <div className="">
          <div className="tlt-sr mb-1 position-relative">
            <span
              className="md-text-fw6 text-uppercase cursor-pointer"
              onClick={() => this.props.viewProduct(product.slug, product._id)}
            >
              {product.title}
            </span>
            <span className="md-text-fw6">
              <span className="px-2">by</span>
              <Link
                to={
                  product.shopId ? `/shop/products/${product.shopId._id}` : ''
                }
                className="text-uppercase"
              >
                {product.shopId && product.shopId.shopName}
              </Link>
            </span>
            <span className="fr-btn">
              {/* <span className="like mr-2 d-inline-flex align-items-center">
                <Like
                  react={this.props.reactToProduct}
                  productId={product._id}
                />
              </span> */}
              <span className="comment mr-2 d-inline-flex align-items-center">
                <Favorite
                  favorite={this.props.favorite}
                  productId={product._id}
                />
              </span>
              <span className="comment d-inline-flex align-items-center">
                <span className="com-icon">
                  <Share />
                </span>
              </span>
            </span>
          </div>
          <div className="mb-3">
            <span
              className="cursor-pointer x-sm-text-fw6 text-uppercase"
              onClick={() => this.props.gotoProfile(product.userId._id)}
            >
              {product.userId && product.userId.fullName}
            </span>
          </div>
          <div className="">
            <span className="sm-text-fw4-g mr-2 d-none d-md-inline-block">
              {' '}
              {product.createdAt && moment(product.createdAt).fromNow()}
            </span>
            <span className="sm-text-fw6">
              {product.variations[0] && product.variations[0].price} €
            </span>
          </div>
        </div>
      </div>
    )
  }
}

export default Product
