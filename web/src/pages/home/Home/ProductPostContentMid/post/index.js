import React, { Component } from 'react'

import Love from '../../../../../images/icons/love'
import Comment from '../../../../../images/icons/comment'
import './post.scss'
import Star from '../../../../../images/icons/star'
import Plus from '../../../../../images/icons/plus'
import Img from '../../../../../images/register-img.webp'
import { Link } from 'react-router-dom'
import ReadMoreAndLess from '../../../../../components/readMore/readMore'
import moment from 'moment'
import Like from '../../../../../components/Like'
import { errorAlert } from '../../../../../utils/alerts'
import { getImageOriginalUrl } from 'core/utils/helpers'
import { connect } from 'react-redux'
import { selectUser } from 'core/selectors/user'
import { reactToPostSocket } from 'core/actions/postActionCreators'

class Post extends Component {
  constructor(props) {
    super(props)
    this.state = {
      commentText: '',
      postId: '',
      activeCommentSubmit: false,
      alreadyLiked: !!props.data.postLikes.filter(
        (item) => item.userId === this.props.user._id,
      ).length,
    }
  }

  //handler for onChange on add comment box...
  onCommentChange = (e, postId) => {
    const commentText = e.target.value
    this.setState({ commentText: commentText, postId: postId }, () => {
      if (
        this.state.commentText &&
        this.state.commentText !== '' &&
        this.state.postId &&
        this.state.postId !== ''
      ) {
        this.setState({ activeCommentSubmit: true })
      } else {
        this.setState({ activeCommentSubmit: true })
      }
    })
  }

  // handler for comment submiting...
  onSubmitComment = () => {
    const commentText = this.state.commentText
    const postId = this.state.postId
    if (commentText === '' || postId === '') {
      return errorAlert('Write somthing good!')
    }
    this.props.addingComment(commentText, postId)
  }

  reactToPost = () => {
    this.setState((prevState) => ({
      alreadyLiked: !prevState.alreadyLiked,
    }))
    this.props.reactToPostSocket(this.props.user._id, this.props.data._id)
  }

  render() {
    const { data } = this.props
    if (!data) {
      return null
    }

    console.log('mid:', data)

    return (
      <div className={this.props.class}>
        <div className="show-img-pp show-img-pp-pb-140">
          <div className="show-img-p">
            <img
              className="cover"
              src={data.images[0] && getImageOriginalUrl(data.images[0].url)}
              alt="img"
              onClick={() => this.props.openPost(data)}
            />
          </div>
        </div>
        <div className="mt-3">
          <div className="pro-cnt mb-3">
            <div className="pro-img">
              {' '}
              <img
                className="cover"
                src={
                  data.userId && data.userId.profileImage
                    ? getImageOriginalUrl(data.userId.profileImage)
                    : Img
                }
                alt="img"
              />
              <span className="user-plus">
                <Plus />{' '}
              </span>
            </div>
            <div className="pro-txt-cnt">
              <div>
                <span
                  className="font-weight-bold text-uppercase cursor-pointer"
                  onClick={() =>
                    this.props.gotoProfile(data.userId && data.userId._id)
                  }
                >
                  {data.userId && data.userId.fullName
                    ? data.userId.fullName
                    : this.props.user && this.props.user.fullName}
                </span>
                <span className="float-right">
                  <span className="like mr-2 d-inline-flex align-items-center">
                    <span className="com-icon">
                      <Like
                        fill={this.state.alreadyLiked}
                        react={this.reactToPost}
                        postId={data._id}
                      />
                    </span>{' '}
                    {data.totalLike | 0} likes
                  </span>
                  <span className="comment  d-inline-flex align-items-center">
                    <span className="com-icon">
                      <Comment />
                    </span>{' '}
                    1
                  </span>
                </span>
              </div>
              <div className="sm-text-fw4-g">
                {data.createdAt && moment(data.createdAt).fromNow()}
              </div>
            </div>
          </div>

          <div className="sm-text-fw4-g mb-2">
            {' '}
            <ReadMoreAndLess
              // ref={this.ReadMore}
              className="btn border-btn"
              charLimit={250}
              readMoreText=" Read more"
              readLessText="Read less"
            >
              {data.content}
            </ReadMoreAndLess>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    user: selectUser(state),
  }),
  {
    reactToPostSocket,
  },
)(Post)
