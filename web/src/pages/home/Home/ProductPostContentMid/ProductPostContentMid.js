import {
  addOutfitsCollection,
  createCollection,
  fetchOutfitCollection,
} from 'core/actions/outfitsActionCreators'
import { reactToPost } from 'core/actions/postActionCreators'
import {
  favoriteProduct,
  reactToProduct,
} from 'core/actions/productActionCreators'
import {
  selectAddOutfitsSuccessMsg,
  selectCreateCollectionMsg,
  selectOutfitsCollection,
} from 'core/selectors/outfits'
import { selectUser } from 'core/selectors/user'
import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import SingleLineInput from '../../../../components/SingleLineInput'
import {
  ROUTE_LOGIN,
  ROUTE_PRODUCT_DETAILS,
  ROUTE_USER_OWN_PROFILE_POST,
  ROUTE_USER_PROFILE_POST,
} from '../../../../routes/constants'
import Post from './post'
import Product from './product'

const ProductPostContentMid = (props) => {
  const gotoProfile = (userId) => {
    if (userId == props.user._id) {
      props.history.push(ROUTE_USER_OWN_PROFILE_POST)
    } else {
      const route = ROUTE_USER_PROFILE_POST.replace(':id', userId)
      props.history.push(route)
    }
  }

  const reactToPost = (postId) => {
    if (!props.user._id) {
      const route = ROUTE_LOGIN
      return props.history.push(route)
    }
    props.reactToPost(props.user._id, postId)
  }

  const reactToProduct = (productId) => {
    if (!props.user._id) {
      const route = ROUTE_LOGIN
      return props.history.push(route)
    }
    props.reactToProduct(props.user._id, productId)
  }

  const viewProduct = (slug, productId) => {
    this.props.history.push(
      ROUTE_PRODUCT_DETAILS.replace(':slug', slug).replace(
        ':productId',
        productId,
      ),
    )
  }

  return (
    <div className="product-post-section ">
      <div className="product-post-1 home-content">
        <div className="row h-100">
          {props.data.posts &&
            props.data.posts.length == 0 &&
            props.data.products &&
            props.data.products.length == 0 && (
              <h3 className="col-12 color-orange">Feed is empty right now!</h3>
            )}

          {/* for post */}
          {props.data.posts &&
            props.data.posts.map((post) => {
              return (
                <Post
                  key={post._id}
                  data={post}
                  class="col-12 col-md-6 post mb-5"
                  gotoProfile={gotoProfile}
                  reactToPost={reactToPost}
                  addingComment={props.addingComment}
                  openPost={props.openPostModal}
                />
              )
            })}
          {/* for post */}

          {/* for product */}
          {props.data.products &&
            props.data.products.map((product) => {
              return (
                <Product
                  key={product._id}
                  product={product}
                  viewProduct={viewProduct}
                  gotoProfile={gotoProfile}
                  favorite={reactToProduct}
                />
              )
            })}
          {/* for product */}
        </div>
      </div>
    </div>
  )
}

export default connect(
  (state) => ({
    user: selectUser(state),
    outfitsCollection: selectOutfitsCollection(state),
    addOutfitsSuccessMsg: selectAddOutfitsSuccessMsg(state),
    createCollectionMsg: selectCreateCollectionMsg(state),
  }),
  {
    reactToPost,
    favoriteProduct,
    fetchOutfitCollection,
    addOutfitsCollection,
    createCollection,
    reactToProduct,
  },
)(withRouter(ProductPostContentMid))
