import moment from "moment";
import React from "react";
import ReadMoreAndLess from "../../../../components/readMore/readMore";
import { withRouter } from "react-router-dom";
import LoveIcon from "../../../../images/icons/love.svg";
import StarIcon from "../../../../images/icons/star.svg";
import { reactToPost } from "core/actions/postActionCreators";
import { favoriteProduct } from "core/actions/productActionCreators";
import { connect } from "react-redux";
import { selectUser } from "core/selectors/user";
import Love from "../../../../images/icons/love";
import Comment from "../../../../images/icons/comment";
import ArrowRight from "../../../../images/icons/arrowRight";
import Star from "../../../../images/icons/star";

import Post from "./post";
import Product from "./product";
import {
  ROUTE_LOGIN,
  ROUTE_PRODUCT_DETAILS,
  ROUTE_USER_OWN_PROFILE_POST,
  ROUTE_USER_PROFILE_POST,
} from "../../../../routes/constants";
import Comments from "./../../../../images/icons/comments.svg";
import LoveRed from "./../../../../images/icons/love-red.svg";

const ProductPostContentMid = (props) => {
  const fovoriteProduct = (productId) => {
    if (!props.user._id) {
      const route = ROUTE_LOGIN;
      return props.history.push(route);
    }
    props.favoriteProduct(props.user._id, productId);
  };

  const gotoProfile = (userId) => {
    console.log("user id: ", userId);
    if (userId == props.user._id) {
      props.history.push(ROUTE_USER_OWN_PROFILE_POST);
    } else {
      const route = ROUTE_USER_PROFILE_POST.replace(":id", userId);
      props.history.push(route);
    }
  };

  const reactToPost = (postId) => {
    if (!props.user._id) {
      const route = ROUTE_LOGIN;
      return props.history.push(route);
    }
    props.reactToPost(props.user._id, postId);
  };

  const viewProduct = (slug, productId) => {
    props.history.push(
      ROUTE_PRODUCT_DETAILS.replace(":slug", slug).replace(
        ":productId",
        productId
      )
    );
  };

  return (
    <div className="product-post-section ">
      <div className="product-post-2 home-content">
        <div className="row h-100">
          {props.data.posts &&
            props.data.posts.length == 0 &&
            props.data.products &&
            props.data.products.length == 0 && (
              <h3 className="col-12 color-orange">Feed is empty right now!</h3>
            )}
          {/* for post */}
          {props.data.posts &&
            props.data.posts.map((post) => {
              return (
                <Post
                  key={post._id}
                  data={post}
                  class="col-12 col-sm-6 col-md-4 post mb-5"
                  gotoProfile={gotoProfile}
                  reactToPost={reactToPost}
                  addingComment={props.addingComment}
                  openPost={props.openPostModal}
                />
              );
            })}
          {/* for post */}
          {/* for product */}
          {props.data.products &&
            props.data.products.map((product) => {
              return (
                <Product
                  key={product._id}
                  product={product}
                  viewProduct={viewProduct}
                  gotoProfile={gotoProfile}
                  favorite={fovoriteProduct}
                  colClass="col-12 col-sm-6 col-md-4 product mb-5"
                />
              );
            })}
          {/* for product */}
        </div>
      </div>
    </div>
  );
};

export default connect(
  (state) => ({
    user: selectUser(state),
  }),
  {
    reactToPost,
    favoriteProduct,
  }
)(withRouter(ProductPostContentMid));
