import React, { Component } from 'react'
import Love from '../../../../../images/icons/love'
import Star from '../../../../../images/icons/star'
import Img from '../../../../../images/register-img.webp'
import { Link } from 'react-router-dom'
import moment from 'moment'

import Share from '../../../../../images/icons/share'
import Favorite from '../../../../../components/Favorite'
class Product extends Component {
  render() {
    const { product } = this.props
    if (!product) {
      return null
    }
    return (
      <div className={this.props.colClass}>
        <div className="show-img-pp show-img-pp-pb-140 mb-3">
          <div className="discount">-20%</div>
          <div
            className="show-img-p cursor-pointer"
            onClick={() => this.props.viewProduct(product.slug, product._id)}
          >
            <img className="cover" src={product.mainImage} alt="img" />
          </div>
        </div>
        <div className="">
          <div className="mb-1">
            <div
              className="md-text-fw6 text-uppercase cursor-pointer"
              onClick={() => this.props.viewProduct(product.slug, product._id)}
            >
              {product.title}
            </div>
            <div className="md-text-fw6">
              {' by '}

              <Link
                to={
                  product.shopId ? `/shop/products/${product.shopId._id}` : ''
                }
                className="text-uppercase text-decoration-underline"
              >
                {product.shopId && product.shopId.shopName}
              </Link>
            </div>
          </div>
          <div className="mb-3">
            <span
              className="x-sm-text-fw6 text-uppercase cursor-pointer"
              onClick={() => this.props.gotoProfile(product.userId._id)}
            >
              {product.userId && product.userId.fullName}
            </span>
          </div>
          <div className="">
            <span>
              <span className="sm-text-fw4-g mr-2 d-none d-md-inline-block">
                {product.createdAt && moment(product.createdAt).fromNow()}
              </span>
              <span className="sm-text-fw6">
                {product.variations[0] && product.variations[0].price} €
              </span>
            </span>

            <span className="float-right">
              {/* <span className="like mr-2 d-inline-flex align-items-center">
                <span className="com-icon">
                  <Love />
                </span>
              </span> */}
              <span className="comment mr-2 d-inline-flex align-items-center">
                <Favorite
                  favorite={this.props.favorite}
                  productId={product._id}
                />
              </span>
              <span className="dl d-inline-flex align-items-center">
                <span className="com-icon">
                  <Share />
                </span>
              </span>
            </span>
          </div>
        </div>
      </div>
    )
  }
}

export default Product
