import React, { Component } from 'react'
import Love from '../../../../../images/icons/love'
import Comment from '../../../../../images/icons/comment'
import ArrowRight from '../../../../../images/icons/arrowRight'
import Img from '../../../../../images/register-img.webp'
import { Link } from 'react-router-dom'
import moment from 'moment'
import ReadMoreAndLess from '../../../../../components/readMore/readMore'
import Like from '../../../../../components/Like'
import { errorAlert } from '../../../../../utils/alerts'
import LessThan from '../../../../../images/icons/lessThan'
import GreaterThan from '../../../../../images/icons/greaterThan'
import { cropString, getImageOriginalUrl } from 'core/utils/helpers'
import { selectCmtEditStatus, selectUser } from 'core/selectors/user'
import { reactToPostSocket } from 'core/actions/postActionCreators'
import { loadMoreComment } from 'core/actions/commentActionCreators'
import {
  addToFollow,
  addToFollowShop,
  unFollowUser,
  unFollowShop,
} from 'core/actions/publicProfileActionCreator'
import { connect } from 'react-redux'
import Comments from '../../../../../components/Comment'
import { selectActiveShop } from 'core/selectors/account'

class Post extends Component {
  constructor(props) {
    super(props)
    this.state = {
      commentText: '',
      postId: props.data._id,
      postOwner: props.data.userId && props.data.userId.fullName,
      activeCommentSubmit: false,
      post: props.data,
      imageUrls: [],
      currentImage: '',
      alreadyLiked: props.user._id
        ? !!props.data.postLikes.filter(
            (item) => item.userId === this.props.user._id,
          ).length
        : false,
      commentLimit: 2,
      commentSkip: 0,
    }
  }

  componentDidMount = () => {
    // making post images urls for slider...

    const urls = []
    this.state.post.images.map((image) => {
      urls.push(getImageOriginalUrl(image.url))
    })
    this.setState({ imageUrls: urls, currentImage: urls[0] })
  }

  //handler for onChange on add comment box...
  onCommentChange = (e, postId) => {
    const commentText = e.target.value
    this.setState({ commentText: commentText, postId: postId }, () => {
      if (
        this.state.commentText &&
        this.state.commentText !== '' &&
        this.state.postId &&
        this.state.postId !== ''
      ) {
        this.setState({ activeCommentSubmit: true })
      } else {
        this.setState({ activeCommentSubmit: false })
      }
    })
  }

  // handler for comment submiting...
  onSubmitComment = () => {
    if (!this.props.user._id) {
      return
    }
    const commentText = this.state.commentText
    const postId = this.state.postId
    const postOwner = this.state.postOwner
    if (commentText === '' || postId === '') {
      return
    }
    this.props.addingComment(commentText, postId, postOwner)
    this.setState({ commentText: '' })
  }

  // change slider image...
  changeImage = (num) => {
    const allImage = [...this.state.imageUrls]
    const currentImage = this.state.currentImage
    const currentIndex = allImage.indexOf(currentImage)

    // if number is -ve
    if (num == -1) {
      if (currentIndex + num < 0) {
        this.setState({ currentImage: allImage[allImage.length + num] })
      } else {
        this.setState({ currentImage: allImage[currentIndex + num] })
      }
    }

    // if number is +ve
    if (num == 1) {
      if (currentIndex + num < allImage.length) {
        this.setState({ currentImage: allImage[currentIndex + num] })
      } else {
        this.setState({ currentImage: allImage[0] })
      }
    }
  }

  reactToPost = () => {
    if (!this.props.user._id) {
      return
    }
    this.setState((prevState) => ({
      alreadyLiked: !prevState.alreadyLiked,
    }))
    this.props.reactToPostSocket(this.props.user._id, this.state.post._id)
  }

  // handler for loadmore comments...
  loadMoreComments = () => {
    this.setState(
      (prevState) => ({
        commentSkip: prevState.commentSkip + prevState.commentLimit,
      }),
      () => {
        // call action for load more comment using current limit and offset...
        this.props.loadMoreComment(
          this.state.postId,
          this.state.commentSkip,
          this.state.commentLimit,
        )
      },
    )
  }

  onClickFollow = () => {
    if (!this.props.user) {
      return
    }
    // check for post owner is a user...
    if (this.props.data.userId) {
      // set status = 1; if you try to follow a user... if follow shop then set it to 2...
      const followingId = this.props.user._id
      const followerId = this.props.data.userId._id
      const status = 1
      this.props.addToFollow(followingId, followerId, status)
    }
    // check for post owner is a shop...
    if (this.props.data.shopId) {
      // set status = 1; if you try to follow a user... if follow shop then set it to 2...
      const followingId = this.props.user._id
      const shopId = this.props.data.shopId._id
      const status = 2
      this.props.addToFollowShop(followingId, shopId, status)
    }
  }

  // unfollow...
  onClickUnFollow = () => {
    if (!this.props.user) {
      return
    }
    // check for post owner is a user...
    if (this.props.data.userId) {
      // set status = 1; if you try to follow a user... if follow shop then set it to 2...

      const status = 1
      const userId = this.props.user._id
      const unFollowId = this.props.data.userId._id
      const shopId = null

      this.props.unFollowUser(status, userId, unFollowId, shopId)
    }
    // check for post owner is a shop...
    if (this.props.data.shopId) {
      // set status = 1; if you try to follow a user... if follow shop then set it to 2...
      const followingId = this.props.user._id
      const shopId = this.props.data.shopId._id
      const status = 2
      this.props.unFollowShop(followingId, shopId, status)
    }
  }

  render() {
    const { data } = this.props
    const { activeCommentSubmit, imageUrls, currentImage } = this.state
    if (!data) {
      return null
    }
    let isFollowed = false
    if (this.props.data.userId) {
      // console.log('this.props.myFollowings: ', this.props.myFollowings)
      isFollowed = this.props.myFollowings.includes(this.props.data.userId._id)
    } else {
      isFollowed = this.props.myFollowings.includes(this.props.data.shopId._id)
    }

    return (
      <div
        className="product-post"
        // onClick={() => this.props.openPost(data)}
      >
        <div className="row h-100">
          <div className="col-12 col-md-6 mb-3 mb-md-0">
            <div className="post-img wh-100 ">
              {/* for slider control */}
              {imageUrls.length > 1 && (
                <span
                  className="prev-btn scale-hover"
                  onClick={() => this.changeImage(-1)}
                >
                  <LessThan />
                </span>
              )}
              {imageUrls.length > 1 && (
                <span
                  className="next-btn scale-hover"
                  onClick={() => this.changeImage(1)}
                >
                  <GreaterThan />
                </span>
              )}
              {imageUrls.length > 1 && (
                <div className="slide-indicator">
                  {imageUrls.map((url) => {
                    return (
                      <span
                        key={url + Math.random()}
                        className={
                          url === currentImage ? 'in-li active' : 'in-li'
                        }
                        onClick={() => this.setState({ currentImage: url })}
                      ></span>
                    )
                  })}
                </div>
              )}
              {/* for slider control */}
              <img
                className="cover cursor-pointer"
                src={currentImage && currentImage}
                alt="img"
                onClick={() => this.props.openPost(data)}
              />
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="post-info ml-0 ml-md-3">
              <div className="pro-cnt mb-3">
                <div
                  className="pro-img cursor-pointer"
                  onClick={() =>
                    this.props.gotoProfile(data.userId && data.userId._id)
                  }
                >
                  {' '}
                  <img
                    className="cover"
                    src={
                      data.userId && data.userId.profileImage
                        ? getImageOriginalUrl(data.userId.profileImage)
                        : Img
                    }
                    alt="img"
                  />
                </div>
                <div className="pro-txt-cnt">
                  <div>
                    <span
                      className="font-weight-bold text-uppercase cursor-pointer"
                      onClick={() =>
                        this.props.gotoProfile(data.userId && data.userId._id)
                      }
                    >
                      {data.userId && data.userId.fullName
                        ? data.userId.fullName
                        : this.props.user && this.props.user.fullName}
                    </span>

                    {/* check if post owner is a user */}
                    {data.userId &&
                    data.userId._id &&
                    data.userId._id !== this.props.user._id ? (
                      isFollowed ? (
                        <button
                          className="btn border-btn btn-w-115 float-right"
                          onClick={() => this.onClickUnFollow()}
                        >
                          Following
                        </button>
                      ) : (
                        <button
                          className="btn border-btn btn-w-115 float-right"
                          onClick={() => this.onClickFollow()}
                        >
                          Follow
                        </button>
                      )
                    ) : null}
                  </div>
                  <div className="sm-text-fw4-g">
                    {data.createdAt && moment(data.createdAt).fromNow()}
                  </div>
                </div>
              </div>
              <div className="sm-text-fw4-g mb-2">
                <ReadMoreAndLess
                  // ref={this.ReadMore}
                  className="btn border-btn"
                  charLimit={250}
                  readMoreText=" Read more"
                  readLessText="Read less"
                >
                  {data.content}
                </ReadMoreAndLess>
              </div>
              <div className="like-com-d sm-text-fw6 text-uppercase mb-3">
                <span className="d-inline-flex mr-2">
                  <span className="com-icon">
                    <Like
                      fill={this.state.alreadyLiked}
                      react={this.reactToPost}
                      postId={data._id}
                    />
                  </span>
                  {data.totalLike | 0} likes
                </span>
                <span className="d-inline-flex">
                  <span className="com-icon">
                    <Comment />
                  </span>
                  {data.grandTotalComment | 0} comments
                </span>
              </div>
              <div className="comment-box pl-4 mb-4">
                {data.comments.map((comment) => {
                  return (
                    <Comments
                      comment={comment}
                      key={comment._id}
                      commentAddStatus={this.props.cmtAddStatus}
                    />
                  )
                })}
              </div>
              {data.comments.length < data.totalComment && (
                <button
                  className="btn border-btn border-0 p-0"
                  onClick={() => this.loadMoreComments()}
                >
                  Load more comment...
                </button>
              )}
              <div className="pro-bottom-part mt-5 mt-md-0">
                <div className="form-group ">
                  <div className="form-input position-relative form-input-append">
                    <input
                      type="text"
                      className="form-control custom-input"
                      placeholder="Add Comment..."
                      name="comment"
                      value={this.state.commentText}
                      onChange={(e) => this.onCommentChange(e, data._id)}
                      onKeyUp={(e) => {
                        if (e.key === 'Enter') {
                          this.onSubmitComment
                        }
                      }}
                    />
                    <span className="append-input d-block">
                      <button
                        className={
                          activeCommentSubmit
                            ? 'btn border-btn border-0 p-0'
                            : 'btn border-btn border-0 p-0 disabled'
                        }
                        onClick={this.onSubmitComment}
                      >
                        Post{' '}
                        <span className="ml-1">
                          <ArrowRight />
                        </span>
                      </button>
                    </span>
                  </div>
                </div>{' '}
                <div className="like-com-d sm-text-fw6 text-uppercase">
                  <span className="d-inline-flex mr-2">
                    <span className="com-icon">
                      <Love />
                    </span>
                    like
                  </span>
                  <span className="d-inline-flex">
                    <span className="com-icon">
                      <Comment />
                    </span>
                    comment
                  </span>
                </div>
                <div className="multi-product row mr-0 mt-3">
                  {data.tags.map((tag) => {
                    return (
                      <div key={tag._id} className="col-6 col-md-4 pr-0 mb-0">
                        <div className="show-img-pp show-img-pp-pb-140 mb-2">
                          <div
                            className="show-img-p cursor-pointer"
                            onClick={() =>
                              this.props.viewProduct(
                                tag.productId.slug,
                                tag.productId._id,
                              )
                            }
                          >
                            <img
                              className="cover"
                              src={
                                tag.productId ? tag.productId.mainImage : Img
                              }
                              alt="img"
                            />
                          </div>
                        </div>
                        <div
                          className="sm-text-fw6 text-uppercase mb-1 cursor-pointer"
                          onClick={() =>
                            this.props.viewProduct(
                              tag.productId.slug,
                              tag.productId._id,
                            )
                          }
                        >
                          {cropString(tag.productId.title, 20)}
                        </div>
                        <div className="sm-text-fw4">
                          {tag.productId &&
                            tag.productId.variations &&
                            tag.productId.variations[0].price}{' '}
                          €
                        </div>
                      </div>
                    )
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    user: selectUser(state),
    activeShop: selectActiveShop(state),
    cmtAddStatus: selectCmtEditStatus(state),
  }),
  {
    reactToPostSocket,
    loadMoreComment,
    addToFollow,
    addToFollowShop,
    unFollowUser,
    unFollowShop,
  },
)(Post)
