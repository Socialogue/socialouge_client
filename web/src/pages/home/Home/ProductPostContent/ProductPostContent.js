import React from 'react'
import moment from 'moment'
import ReadMoreAndLess from '../../../../components/readMore/readMore'
import { withRouter } from 'react-router-dom'
import LoveIcon from '../../../../images/icons/love.svg'
import StarIcon from '../../../../images/icons/star.svg'
import Img from '../../../../images/register-img.webp'
import { reactToPost } from 'core/actions/postActionCreators'
import { favoriteProduct } from 'core/actions/productActionCreators'
import { connect } from 'react-redux'
import { selectMyFollowings, selectUser } from 'core/selectors/user'
import {
  ROUTE_LOGIN,
  ROUTE_PRODUCT_DETAILS,
  ROUTE_USER_OWN_PROFILE_POST,
  ROUTE_USER_PROFILE_POST,
} from './../../../../routes/constants'
import Love from '../../../../images/icons/love'
import Comment from '../../../../images/icons/comment'
import ArrowRight from '../../../../images/icons/arrowRight'
import Star from '../../../../images/icons/star'
import Post from './post'
// import Product from "./product";
import Download from '../../../../images/icons/download'
import Share from '../../../../images/icons/share'
import Product from './product/product'

const ProductPostContent = (props) => {
  const reactToPost = (postId) => {
    if (!props.user._id) {
      const route = ROUTE_LOGIN
      return props.history.push(route)
    }
    props.reactToPost(props.user._id, postId)
  }

  const fovoriteProduct = (productId) => {
    if (!props.user._id) {
      const route = ROUTE_LOGIN
      return props.history.push(route)
    }
    props.favoriteProduct(props.user._id, productId)
  }

  const gotoProfile = (userId) => {
    if (userId == props.user._id) {
      props.history.push(ROUTE_USER_OWN_PROFILE_POST)
    } else {
      const route = ROUTE_USER_PROFILE_POST.replace(':id', userId)
      props.history.push(route)
    }
  }

  const viewProduct = (slug, productId) => {
    props.history.push(
      ROUTE_PRODUCT_DETAILS.replace(':slug', slug).replace(
        ':productId',
        productId,
      ),
    )
  }

  return (
    <div className="product-post-section ">
      <div className="home-content">
        {props.data.posts &&
          props.data.posts.length == 0 &&
          props.data.products &&
          props.data.products.length == 0 && (
            <h3 className="col-12 color-orange">Feed is empty right now!</h3>
          )}
        {props.data.posts &&
          props.data.posts.map((post) => {
            return (
              <Post
                key={post._id}
                data={post}
                class="col-12 col-md-4 post mb-5"
                gotoProfile={gotoProfile}
                reactToPost={reactToPost}
                addingComment={props.addingComment}
                openPost={props.openPostModal}
                viewProduct={viewProduct}
                myFollowings={props.myFollowings}
              />
            )
          })}

        {props.data.products &&
          props.data.products.map((product) => {
            return (
              <Product
                key={product._id}
                product={product}
                viewProduct={viewProduct}
                gotoProfile={gotoProfile}
                favorite={fovoriteProduct}
                colClass="col-12 col-md-4 product mb-5"
                myFollowings={props.myFollowings}
              />
            )
          })}
      </div>
    </div>
  )
}

export default connect(
  (state) => ({
    user: selectUser(state),
    myFollowings: selectMyFollowings(state),
  }),
  {
    reactToPost,
    favoriteProduct,
  },
)(withRouter(ProductPostContent))
