import { SORT_VIEW_TOP } from 'core/constants'
import moment from 'moment'
import React, { Component } from 'react'
import Favorite from '../../../../../components/Favorite'
import Love from '../../../../../images/icons/love'
import Share from '../../../../../images/icons/share'
import Star from '../../../../../images/icons/star'
import Img from './../../../../../images/eco-img.webp'
import { Link } from 'react-router-dom'

export default class Product extends Component {
  render() {
    const { product } = this.props
    const { userId } = product
    if (!product && !userId) {
      return null
    }
    return (
      <div
        className={
          this.props.view && this.props.view == SORT_VIEW_TOP
            ? this.props.colClass + ' ml-10'
            : 'product-post'
        }
        key={product._id}
      >
        {' '}
        <div className="row h-100">
          <div className="col-12 col-md-6 mb-3 mb-md-0 product">
            <div
              className="product-img wh-100 cursor-pointer position-relative"
              onClick={() => this.props.viewProduct(product.slug, product._id)}
            >
              {/* for slider control */}
              <div className="slide-indicator">
                <span className="in-li active"></span>
                <span className="in-li"></span>
                <span className="in-li"></span>
                <span className="in-li"></span>
                <span className="in-li"></span>
              </div>
              {/* for slider control */}
              <div className="discount">-20%</div>
              <img className="cover" src={product.mainImage} alt="img" />
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="product-info ml-0 ml-md-3">
              <div className="info-div">
                <div className="">
                  <span
                    className="font-weight-6 text-uppercase cursor-pointer"
                    onClick={() =>
                      this.props.viewProduct(product.slug, product._id)
                    }
                  >
                    {product.title}
                  </span>{' '}
                  by{' '}
                  <Link
                    to={
                      product.shopId
                        ? `/shop/products/${product.shopId._id}`
                        : ''
                    }
                    className="text-uppercase font-weight-6"
                  >
                    {product.shopId && product.shopId.shopName}
                  </Link>
                  <span
                    className="md-text-fw6 cursor-pointer"
                    onClick={() => this.props.gotoProfile(userId._id)}
                  >
                    <span className="text-uppercase">
                      {' '}
                      {product.userId && product.userId.fullName}
                    </span>
                  </span>
                </div>
                <div className="mb-3 d-flex align-items-center">
                  <span className="sm-text-fw4-g mr-2">
                    {product.createdAt && moment(product.createdAt).fromNow()}
                  </span>
                  <span className="">
                    {product.variations[0] && product.variations[0].price} €
                  </span>
                </div>
                <div className="sm-text-fw4-g mb-4">
                  {product.lognDesc && (
                    <ReadMoreAndLess
                      // ref={this.ReadMore}
                      className="btn border-btn"
                      charLimit={250}
                      readMoreText=" Read more"
                      readLessText="Read less"
                    >
                      {product.lognDesc}
                    </ReadMoreAndLess>
                  )}
                </div>
              </div>
              <div className="pro-bottom-part mt-4 mt-md-0">
                <div className="multi-product row mr-0">
                  <div className="col-6 col-md-4 pr-0 mb-4">
                    <div className="show-img-pp show-img-pp-pb-140 mb-2">
                              <div className="show-img-p">
                                <img
                                  className="cover"
                                  // src={product.images && product.images[0].url}
                                  src={Img}
                                  alt="imwg"
                                />
                              </div>
                            </div>
                    <div className="sm-text-fw6 text-uppercase mb-1">
                      limited edition coat
                    </div>
                    <div className="sm-text-fw4">
                      {product.variations[0] && product.variations[0].price} €
                    </div>
                  </div>
                </div>

                <div className="match-style">
                  <span className="md-text-fw6 text-uppercase match-txt mr-3 cursor-pointer">
                    Match style
                  </span>
                  {/* <span className="md-text-fw6 text-uppercase mr-3 cursor-pointer">
                    <Love />
                  </span> */}
                  <Favorite
                    favorite={this.props.favorite}
                    productId={product._id}
                  />
                  <span className="md-text-fw6 text-uppercase ml-2 cursor-pointer">
                    <Share />
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
