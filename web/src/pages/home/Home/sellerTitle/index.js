import React, { Component } from "react";
import ArrowRight from "../../../../images/icons/arrowRight";
import "./sellerTitle.scss";
class SellerTitle extends Component {
  render() {
    return (
      <div className="home-title seller-title position-relative">
        <div className="home-title-cnt">
          <div className="xx-md-title color-white position-relative">
            become a seller<span className="dot-r" />{" "}
            <br className="d-block d-md-none" />
            <span className="seller-ar">
              <ArrowRight />
            </span>
          </div>
          <div className="sm-text-fw5 color-white seller-txt mt-2 justify-content-md-end">
            <span className="txt-w">
              Wide brim bucket hat, contrast drawcord fastening, poster tag on
              side, 100% Polyester. One size – fits most.
            </span>
          </div>
        </div>
      </div>
    );
  }
}

export default SellerTitle;
