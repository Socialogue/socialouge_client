import React, { Component } from "react";
import Close from "../../../images/icons/close";

class CookieModal extends Component {
  render() {
    return (
      <div
        className="modal fade modal-1"
        id="cookieModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content p-4">
            <div className="modal-header p-0 border-0">
              <div className="sm-text-fw6 text-uppercase">
                your cookie settings
              </div>
              <span className="cursor-pointer" aria-hidden="true">
                <Close />
              </span>
            </div>

            <div className="modal-body pt-4 pb-5 px-0">
              <div className="x-sm-text-fw4-g mb-4">
                Simply dummy text of the printing and typesetting industry.
                Lorem Ipsum has been the industry's standard dummy text ever
                since the 1500s, when an unknown printer took a galley of type
                and scrambled it to make a type specimen book.
              </div>
              <div className="x-sm-text-fw4-g">
                You can always change your preferences by visiting the “Cookie
                Settings” at the bottom of the page. View Privacy & Cookie
                Policy for full details.
              </div>
            </div>

            <div className="modal-footer pb-0 px-0">
              <div className="w-100 x-sm-text-f6">More information</div>
              <button type="button" className="btn border-btn btn-w-115 ">
                I accept
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CookieModal;
