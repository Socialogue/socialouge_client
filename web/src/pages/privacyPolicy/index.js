import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import ArrowLeft from '../../images/icons/arrowLeft'
import { ROUTE_HOME } from '../../routes/constants'
import FixedFooter from '../Common/fixedFooter'
import Footer from '../home/Footer/Footer'
import Header from '../home/Header/Header'
import CookieModal from './cookieModal'

class PrivacyPolicy extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="max-content-width-1 py-3 py-xl-5 my-3 my-xl-5 px-4">
          <div className="x-md-title border-bottom-1 pb-3 pb-md-4 mb-4 mb-md-5">
            privacy policy
            <span className="dot-r" />
          </div>
          <div className="pvf-cnt">
            <div className="mb-4">
              <div className="md-title md-title-res mb-3">
                Socialogue Privacy Policy
              </div>
              <ul className="pl-3">
                <li className="x-sm-text-fw4-g mb-1">Medium Privacy Policy</li>
                <li className="x-sm-text-fw4-g mb-1">
                  Data Protection Statement for European Union Users
                </li>
                <li className="x-sm-text-fw4-g mb-1">
                  Consumer Privacy for California Users
                </li>
              </ul>
            </div>
            <div className="mb-4">
              <div className="x-md-text-fw6 font-weight-bold mb-3 text-uppercase">
                Information We Collect & How We Use It
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                Medium uses third-party vendors and hosting partners, such as
                Amazon, for hardware, software, networking, storage, and related
                technology we need to run Medium. We maintain two types of logs:
                server logs and event logs. By using the Services, you authorize
                Medium to transfer, store, and use your information in the
                United States and any other country where we operate.
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                Medium uses third-party vendors and hosting partners, such as
                Amazon, for hardware, software, networking, storage, and related
                technology we need to run Medium. We maintain two types of logs:
                server logs and event logs. By using the Services, you authorize
                Medium to transfer, store, and use your information in the
                United States and any other country where we operate.
              </div>
            </div>
            <div className="mb-4">
              <div className="x-md-text-fw6 font-weight-bold mb-3 text-uppercase">
                Information Disclosure
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                Medium uses third-party vendors and hosting partners, such as
                Amazon, for hardware, software, networking, storage, and related
                technology we need to run Medium. We maintain two types of logs:
                server logs and event logs. By using the Services, you authorize
                Medium to transfer, store, and use your information in the
                United States and any other country where we operate.
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                Medium uses third-party vendors and hosting partners, such as
                Amazon, for hardware, software, networking, storage, and related
                technology we need to run Medium. We maintain two types of logs:
                server logs and event logs. By using the Services, you authorize
                Medium to transfer, store, and use your information in the
                United States and any other country where we operate.
              </div>
            </div>
            <div className="mb-4">
              <div className="x-md-text-fw6 font-weight-bold mb-3 text-uppercase">
                Public Data
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                Medium uses third-party vendors and hosting partners, such as
                Amazon, for hardware, software, networking, storage, and related
                technology we need to run Medium. We maintain two types of logs:
                server logs and event logs. By using the Services, you authorize
                Medium to transfer, store, and use your information in the
                United States and any other country where we operate.
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                Medium uses third-party vendors and hosting partners, such as
                Amazon, for hardware, software, networking, storage, and related
                technology we need to run Medium. We maintain two types of logs:
                server logs and event logs. By using the Services, you authorize
                Medium to transfer, store, and use your information in the
                United States and any other country where we operate.
              </div>
            </div>
            <div className="mb-4">
              <div className="x-md-text-fw6 font-weight-bold mb-3 text-uppercase">
                Data Storage
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                Medium uses third-party vendors and hosting partners, such as
                Amazon, for hardware, software, networking, storage, and related
                technology we need to run Medium. We maintain two types of logs:
                server logs and event logs. By using the Services, you authorize
                Medium to transfer, store, and use your information in the
                United States and any other country where we operate.
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                Medium uses third-party vendors and hosting partners, such as
                Amazon, for hardware, software, networking, storage, and related
                technology we need to run Medium. We maintain two types of logs:
                server logs and event logs. By using the Services, you authorize
                Medium to transfer, store, and use your information in the
                United States and any other country where we operate.
              </div>
            </div>
            <div className="mb-4">
              <div className="x-md-text-fw6 font-weight-bold mb-3 text-uppercase">
                Third-Party Embeds
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                Medium uses third-party vendors and hosting partners, such as
                Amazon, for hardware, software, networking, storage, and related
                technology we need to run Medium. We maintain two types of logs:
                server logs and event logs. By using the Services, you authorize
                Medium to transfer, store, and use your information in the
                United States and any other country where we operate.
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                Medium uses third-party vendors and hosting partners, such as
                Amazon, for hardware, software, networking, storage, and related
                technology we need to run Medium. We maintain two types of logs:
                server logs and event logs. By using the Services, you authorize
                Medium to transfer, store, and use your information in the
                United States and any other country where we operate.
              </div>
            </div>
            <div className="mb-4">
              <div className="x-md-text-fw6 font-weight-bold mb-3 text-uppercase">
                Contact Us
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                You may contact us by emailing us at contact@socialogue.com
              </div>
            </div>
            <div>
              <Link className="x-sm-text-fw6" to={ROUTE_HOME}>
                <ArrowLeft /> Back to Main
              </Link>
            </div>
          </div>
        </div>
        <CookieModal />
        <Footer />
        <div className="ab-mf px-4 mb-4 d-block d-md-none">
          <FixedFooter />
        </div>
      </div>
    )
  }
}

export default PrivacyPolicy
