import React, { Component } from 'react';
import CloseSm from '../../../images/icons/closeSm';
import ProImg from "./../../../images/login-img1.webp";

class NotificationMessage extends Component {
    render() {
        return (
            <div className="noti-message">
            <div className="pro-cnt">
                <span className="n-remove scale-hover"><CloseSm /></span>
            <div className="pro-img pro-img-md">
          <img
            className="cover"
            src={ProImg}
            alt="img"
          />
        </div>
        <div className="pro-txt-cnt pro-txt-cnt-md">
          <div className="x-sm-text-fw6 text-uppercase">
          John
          </div>
          <div className="x-sm-text-fw4-g">Your order has been confirmed and the goods are being prepared.....</div>
        </div>
      </div>
      </div>
        );
    }
}

export default NotificationMessage;