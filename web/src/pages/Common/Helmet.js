/**
 * @flow
 */
import React from 'react';
import { Helmet as ReactHelmet } from 'react-helmet';

import config from '../../Config';
import type { RouteStaticContext } from '../../TypeDefinition';

type Props = {
  appLink?: string,
  description: string,
  disableSuffix?: boolean,
  image?: string,
  staticContext: ?RouteStaticContext,
  title: string,
  productInfo?: {
    brand: string,
    price: number,
    id: number,
    category: string,
    retailer: number
  }
};

const Helmet = ({
  appLink,
  description,
  disableSuffix = false,
  image,
  staticContext,
  title,
  productInfo
}: Props) => {
  let fullUrl = '';
  let baseUrl = '';

  if (!staticContext) {
    // Client mode
    baseUrl = `${window.location.protocol}//${window.location.host}`;
    fullUrl = window.location.href;
  } else {
    // Server mode
    baseUrl = staticContext.baseUrl;
    fullUrl = staticContext.fullUrl;
  }

  const defaultImage = `${baseUrl}/img/logo-512x512.png`;

  return (
    <ReactHelmet>
      <title>{disableSuffix ? title : `${title} | ${String(config.SITE_NAME)}`}</title>
      <meta name='description' content={description} />
      <meta property='og:image' content={image || defaultImage} />
      <meta property='og:type' content={productInfo ? 'product' : 'website'} />
      <meta property='og:title' content={title} />
      <meta property='og:description' content={description} />
      <meta property='og:url' content={fullUrl} />
      <meta property='fb:app_id' content={config.FB_APP_ID} />
      <meta name='twitter:card' content='summary_large_image' />
      <meta name='twitter:title' content={title} />
      <meta name='twitter:description' content={description} />
      <meta name='twitter:image' content={image || defaultImage} />
      <link rel='canonical' href={fullUrl} />
      {!!appLink && <meta property='al:ios:url' content={appLink} />}
      {!!appLink && <meta property='al:ios:app_store_id' content='00' />}
      {!!appLink && <meta property='al:ios:app_name' content='sl' />}
      {!!appLink && <meta property='al:android:url' content={appLink} />}
      {!!appLink && <meta property='al:android:package' content='com.com' />}
      {!!appLink && <meta property='al:android:app_name' content='sl' />}
      <script src="https://apis.google.com/js/platform.js" async defer></script>
      <meta name="google-signin-client_id" content="1074659719210-1uiu9dbmg2ssuglmrpq6pq13q2260um5.apps.googleusercontent.com" />


    </ReactHelmet>
  );
};

export default Helmet;
