import React, { Component } from "react";
import {
  ROUTE_ABOUT_US,
  ROUTE_FAQ,
  ROUTE_PRIVACY_POLICY,
  ROUTE_TERMS_SERVICE
} from "../../../routes/constants";

import { Link } from "react-router-dom";
import "./fixedFooter.scss";
class FixedFooter extends Component {
  render() {
    return (
      <div className="fixed-footer d-none d-md-block">
        <nav aria-label="breadcrumb" className="">
          <ol className="breadcrumb custom-breadcrumb mb-0">
            <li className="breadcrumb-item">
              <Link to={ROUTE_TERMS_SERVICE}>Terms</Link>
            </li>
            <li className="breadcrumb-item">
              <Link to={ROUTE_PRIVACY_POLICY}>Privacy</Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="#">Sell</Link>
            </li>
            <li className="breadcrumb-item">
              <Link to={ROUTE_ABOUT_US}>About</Link>
            </li>
            <li className="breadcrumb-item faq-li">
              <Link to={ROUTE_FAQ}>FAQ</Link>
            </li>
            <li className="breadcrumb-item">socialogue © 2021</li>
          </ol>
        </nav>
      </div>
    );
  }
}

export default FixedFooter;
