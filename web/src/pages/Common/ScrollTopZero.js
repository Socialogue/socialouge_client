import React, { Component } from "react";
import ArrowTop from '../../images/icons/arrowTop'
class ScrollTopZero extends Component {
  state = {
    intervalId: 0,
  };
  scrollStep() {
    if (window.pageYOffset === 0) {
      clearInterval(this.state.intervalId);
    }
    window.scroll(0, window.pageYOffset - 50);
  }

  scrollToTop() {
    let intervalId = setInterval(this.scrollStep.bind(this), 16);
    this.setState({ intervalId: intervalId });
  }
  render() {
    return (
      <button
        className={this.props.Display + " btn border-btn p-0 srl-top-btn"}
        onClick={() => {
          this.scrollToTop();
        }}
      >
        <ArrowTop />
      </button>
    );
  }
}

export default ScrollTopZero;
