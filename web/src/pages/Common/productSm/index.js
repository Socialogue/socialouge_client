import React, { Component } from 'react'
import Star from './../../../images/icons/star.js'
import Love from './../../../images/icons/love.js'
import { Link } from 'react-router-dom'
class ProductSm extends Component {
  render() {
    const { product } = this.props
    if (!product) {
      return null
    }
    return (
      <div className={this.props.colClass}>
        <div className="show-img-pp show-img-pp-pb-140">
          <div
            className="show-img-p cursor-pointer"
            onClick={() => this.props.viewProduct(product.slug, product._id)}
          >
            <img
              className="cover"
              src={product.mainImage ? product.mainImage : this.props.proImg}
              alt="img"
            />
          </div>
        </div>
        <div className="mt-3">
          <div className=" mb-3">
            <div
              className="font-weight-bold text-uppercase cursor-pointer"
              onClick={() => this.props.viewProduct(product.slug, product._id)}
            >
              {product.title}
            </div>
            <div className="font-weight-6">
              by{' '}
              {product.shopId && (
                <span
                  className="text-uppercase text-decoration-underline cursor-pointer"
                  onClick={() => this.props.shopProfile(product.shopId._id)}
                >
                  {product.shopId && product.shopId.shopName}
                </span>
              )}
            </div>
          </div>
          <div className="">
            <span>
              {product.variations[0] && product.variations[0].price} €
            </span>
            <span className="float-right">
              <span className="like mr-2 icon-hover">
                <Love />
              </span>
              <span className="star icon-hover">
                <Star />
              </span>
            </span>
          </div>
        </div>
      </div>
    )
  }
}

export default ProductSm
