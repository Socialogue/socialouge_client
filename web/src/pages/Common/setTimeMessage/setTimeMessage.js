import React, { Component } from 'react';
import CloseSm from '../../../images/icons/closeSm';
import Success from '../../../images/icons/success';

class SetTimeMessage extends Component {
    render() {
        return (
            <div className={this.props.class}>
             <span className="success-i"><Success /></span>
             <span className="error-i"><CloseSm /></span>
              {this.props.message}
            </div>
        );
    }
}

export default SetTimeMessage;