import { element } from "prop-types";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import ArrowLeft from "../../images/icons/arrowLeft";
import Down from "../../images/icons/down";
import FixedFooter from "../Common/fixedFooter";
import Footer from "../home/Footer/Footer";
import Header from "../home/Header/Header";

class Faq extends Component {
  toggleClass(e) {
    e.target.closest(".txt-cnt-ul").classList.toggle("active");
  }
  render() {
    return (
      <div>
        <Header />
        <div className="max-content-width-1 py-3 py-xl-5 my-3 my-xl-5 px-4">
          <div className="x-md-title border-bottom-1 pb-3 pb-md-4 mb-4 mb-md-5 text-uppercase">
            Faq
            <span className="dot-r" />
          </div>
          <div className="pvf-cnt">
            <div className="mb-5">
              <div className="md-title md-title-res mb-3">
                quick answers to questions about socialogue
              </div>
              <div className="x-sm-text-fw4-g">
                Here you can find answers to some of the most popular questions
                about Socialogue.
              </div>
            </div>

            <div className="mb-4 txt-cnt-ul">
              <span
                onClick={event => this.toggleClass(event)}
                className="x-md-text-fw6 font-weight-bold text-uppercase txt-cnt-li"
              >
                Who founded socialogue? <Down />
              </span>
              <div className="txt-cnt-s mt-3">
                <div className="x-sm-text-fw4-g mb-3">
                  Search engines may index your Medium user profile page, public
                  interactions (such as claps or highlights), and post pages,
                  such that people may find these pages when searching against
                  your name on services like Google, DuckDuckGo, or Bing. Users
                  may also share links to your content on social media platforms
                  such as Facebook or Twitter.
                </div>
                <div className="x-sm-text-fw4-g mb-3">
                  Search engines may index your Medium user profile page, public
                  interactions (such as claps or highlights), and post pages,
                  such that people may find these pages when searching against
                  your name on services like Google, DuckDuckGo, or Bing. Users
                  may also share links to your content on social media platforms
                  such as Facebook or Twitter.
                </div>
              </div>
            </div>
            <div className="mb-4 txt-cnt-ul">
              <span
                onClick={event => this.toggleClass(event)}
                className="x-md-text-fw6 font-weight-bold text-uppercase txt-cnt-li"
              >
                Does it cost money to join socialogue? <Down />
              </span>
              <div className="txt-cnt-s mt-3">
                <div className="x-sm-text-fw4-g mb-3">
                  Search engines may index your Medium user profile page, public
                  interactions (such as claps or highlights), and post pages,
                </div>
                <div className="x-sm-text-fw4-g mb-3">
                  Search engines may index your Medium user profile page, public
                  interactions (such as claps or highlights), and post pages,
                  such that people may find these pages when searching against
                  your name on services like Google, DuckDuckGo, or Bing. Users
                  may also share links to your content on social media platforms
                  such as Facebook or Twitter.
                </div>
              </div>
            </div>
            <div className="mb-4 txt-cnt-ul">
              <span
                onClick={event => this.toggleClass(event)}
                className="x-md-text-fw6 font-weight-bold text-uppercase txt-cnt-li"
              >
                can I get a custom domain name for my socialogue shop? <Down />
              </span>
              <div className="txt-cnt-s mt-3">
                <div className="x-sm-text-fw4-g mb-3">
                  Search engines may index your Medium user profile page, public
                  interactions (such as claps or highlights), and post pages,
                  such that people may find these pages when searching against
                  your name on services like Google, DuckDuckGo, or Bing. Users
                  may also share links to your content on social media platforms
                  such as Facebook or Twitter.
                </div>
              </div>
            </div>

            <div className="mb-4 pt-5">
              <div className="x-md-text-fw6 font-weight-bold mb-3 text-uppercase">
                Contact Us
              </div>
              <div className="x-sm-text-fw4-g mb-3">
                You may contact us by emailing us at contact@socialogue.com
              </div>
              <div>
                <Link className="x-sm-text-fw6" to="/">
                  <ArrowLeft /> Back to Main
                </Link>
              </div>
            </div>
          </div>
        </div>
        <Footer />
        <div className="ab-mf px-4 mb-4 d-block d-md-none">
          <FixedFooter />
        </div>
      </div>
    );
  }
}

export default Faq;
