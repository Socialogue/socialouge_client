import React from 'react'
import { connect } from 'react-redux'
import ArrowRight from '../../images/icons/arrowRight'
import Category from './components/category'
import InnerCategory from './components/innerCategory'
import SubCategory from './components/subCategory'
import './productListing.scss'
import {
  getCategories,
  getSubCategories,
  setCatDataForProduct,
} from 'core/actions/productActionCreators'
import {
  selectCatData,
  selectCategories,
  selectErrorMsg,
  selectInnerCatData,
  selectSubCatData,
} from 'core/selectors/products'
import { errorAlert } from '../../utils/alerts'

// components

class ProductListing extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      categories: [],
      subCategories: [],
      innerCategories: [],
      innerCategoriesTwo: [],
      category: {},
      subCategory: {},
      innercategory: {},
      innerCategoryTwo: {},
    }
  }

  componentDidMount = () => {
    try {
      this.props.getCategories()
      if (this.props.category) {
      }
    } catch (err) {
      console.log(err)
    }
  }
  componentDidUpdate = (prevProps) => {
    if (prevProps.errorMsg !== this.props.errorMsg) {
      if (this.props.errorMsg !== '') {
        errorAlert('Something went wrong!')
      }
    }
  }

  populateSubCategory = (catId) => {
    this.props.categories.map((category) => {
      if (category._id == catId) {
        this.setState({
          category: category,
          subCategories: category.subCategory,
          innerCategories: [],
        })
        console.log('cat :', this.state.category)
      }
    })
  }

  populateInnerCategory = (subCatId) => {
    this.state.subCategories.map((subcategory) => {
      if (subcategory._id === subCatId) {
        this.setState({
          subCategory: subcategory,
          innerCategories: subcategory.innerCategory,
        })
      }
    })
  }

  populateInnerCategoryTwo = (innerCatId) => {
    this.state.innerCategories.map((innercategory) => {
      if (innercategory._id === innerCatId) {
        this.setState({
          innercategory: innercategory,
          innerCategoriesTwo: innercategory.innerCategory2,
        })
      }
    })
  }

  gotoNext = () => {
    const category = this.state.category
    const subCategory = this.state.subCategory
    const innerCategory = this.state.innercategory
    if (category.name) {
      this.props.setCatDataForProduct(category, subCategory, innerCategory)
      this.props.onPressNext()
    } else if (this.props.category.name) {
      this.props.setCatDataForProduct(
        this.props.category,
        this.props.subCategory,
        this.props.innerCategory,
      )
      this.props.onPressNext()
    }
  }

  render() {
    return (
      <div>
        {/* <Header /> */}
        <div className="max-content-width my-5 pb-5">
          {/* <nav aria-label="breadcrumb" className="mb-5">
            <ol className="breadcrumb custom-breadcrumb">
              <li className="breadcrumb-item">
                <Link to="#">Home</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to="#">Settings</Link>
              </li>
              <li className="breadcrumb-item">Product listing</li>
            </ol>
          </nav> */}

          <div className="">
            <div className="x-md-title ">
              Product Listing
              <span className="dot-r" />
            </div>
            <div className="sm-text-fw4-g">Select product category</div>
          </div>

          <div className="my-5">
            <div className="mb-4 font-weight-bold text-uppercase">
              tell us what you selling
            </div>

            <nav aria-label="breadcrumb" className="mb-3">
              <ol className="breadcrumb custom-breadcrumb">
                {this.props.category.name && (
                  <li className="breadcrumb-item">
                    {this.props.category.name}
                  </li>
                )}
                {this.props.subCategory.name && (
                  <li className="breadcrumb-item">
                    {this.props.subCategory.name}
                  </li>
                )}
              </ol>
            </nav>

            <div className="cat-list d-block d-md-flex">
              <div className="cat-list-col cat-list-col1 main-cat">
                <Category
                  categories={this.props.categories}
                  getSubCategories={this.populateSubCategory}
                />
              </div>
              <div className="cat-list-col2 position-relative">
                <div className="cat-cnt sub-cate">
                  <div className="row mx-0">
                    <SubCategory
                      subCategories={this.state.subCategories}
                      getInnerCategory={this.populateInnerCategory}
                    />
                    <InnerCategory
                      innerCategories={this.state.innerCategories}
                      getInnerCategoryTwo={this.populateInnerCategoryTwo}
                    />
                  </div>

                  <button
                    className={
                      !this.state.category.name
                        ? 'btn border-btn border-0 cat-next-btn p-0 disabled'
                        : 'btn border-btn border-0 cat-next-btn p-0'
                    }
                    onClick={() => this.gotoNext()}
                  >
                    Next step <ArrowRight />
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    errorMsg: selectErrorMsg(state),
    categories: selectCategories(state),
    category: selectCatData(state),
    subCategory: selectSubCatData(state),
    innerCategory: selectInnerCatData(state),
  }),
  {
    getCategories,
    getSubCategories,
    setCatDataForProduct,
  },
)(ProductListing)
