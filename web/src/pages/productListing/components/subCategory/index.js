import React, { Component } from "react";
import Categories from "./../categories";
class SubCategory extends Component {
  render() {
    const { subCategories } = this.props;
    if (subCategories) {
      console.log("sub: ", subCategories);
    }
    if (!subCategories) {
      return null;
    }
    return (
      <div className="col-6 col-sm-4 col-md-3 px-0 cat-list-col s-cat">
        {subCategories.map((subCat, index) => {
          return (
            <Categories
              class="cat sm-text-fw6-g text-uppercase cursor-pointer"
              catName="See all"
              cat={subCat}
              key={index}
              getNextData={this.props.getInnerCategory}
              innerLength={subCat.innerCategory.length}
            />
          );
        })}
      </div>
    );
  }
}

export default SubCategory;
