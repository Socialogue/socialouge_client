import React, { Component } from "react";
import Categories from "./../categories";
class Category extends Component {
  render() {
    const { categories } = this.props;
    if (!categories) {
      return null;
    }

    return (
      <div className="cat-cnt ">
        {categories.map(category => {
          return (
            <Categories
              class="cat sm-text-fw6-g text-uppercase cursor-pointer"
              cat={category}
              key={category._id}
              getNextData={this.props.getSubCategories}
              innerLength={category.subCategory.length}
            />
          );
        })}
      </div>
    );
  }
}

export default Category;
