import React, { Component } from "react";
import Right from "../../../../images/icons/right";
class Categories extends Component {
  selectCat(e) {
    if (e.currentTarget.closest(".main-cat")) {
      const sl = e.currentTarget.closest(".main-cat").querySelectorAll(".cat");
      sl.forEach(function (userItem, i) {
        userItem.classList.remove("active");
      });
    } else if (e.currentTarget.closest(".s-cat")) {
      const sl = e.currentTarget.closest(".s-cat").querySelectorAll(".cat");
      sl.forEach(function (userItem, i) {
        userItem.classList.remove("active");
      });
    } else {
      const sl = e.currentTarget.closest(".inner-cat").querySelectorAll(".cat");
      sl.forEach(function (userItem, i) {
        userItem.classList.remove("active");
      });
    }
    e.currentTarget.classList.add("active");
  }
  render() {
    const { cat } = this.props;
    if (!cat) {
      return null;
    }
    const { name, _id } = this.props.cat;
    if (!name || !_id) {
      return null;
    }
    return (
      <div
        onClick={(e) => this.selectCat(e)}
        className={this.props.class}
        key={_id}
      >
        <span className="mr-3" onClick={() => this.props.getNextData(_id)}>
          {name}
        </span>{" "}
        <Right />
      </div>
    );
  }
}

export default Categories;
