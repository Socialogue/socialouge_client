import React, { Component } from "react";
import Categories from "./../categories";
class InnerCategory extends Component {
  render() {
    const { innerCategories } = this.props;
    if (!innerCategories) {
      return null;
    }
    return (
      <div className="col-6 col-sm-8 col-md-9 px-0">
        <div className="cat-cnt inner-cat">
          <div className="row mx-0">
            {innerCategories.map((innerCat) => {
              return (
                <div className="col-12 col-sm-6 px-0">
                  <Categories
                    class="cat sm-text-fw6-g text-uppercase cursor-pointer"
                    catName="See all"
                    cat={innerCat}
                    key={innerCat._id}
                    getNextData={this.props.getInnerCategoryTwo}
                  />
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default InnerCategory;
