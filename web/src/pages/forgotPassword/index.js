import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import SmLogo from './../../images/sm-logo.svg'
import Back from './../../images/icons/back.svg'
import ResetImgLg from './../../images/reset-img.webp'
import parsePhoneNumber from 'libphonenumber-js'
import { forgotPassword } from 'core/actions/authActionCreators'
import { selectUserToVerifyOtp } from 'core/selectors/user'
import { relativeTimeThreshold } from 'moment'
import {
  ROUTE_FORGOT_PASSWORD_OTP_VERIFY,
  ROUTE_LOGIN,
} from '../../routes/constants'

class ForgotPassword extends React.Component {
  constructor() {
    super()
    this.state = {
      loading: false,
      username: '',
      error: '',
      accountType: null,
    }
  }

  inputHandler = (e) => {
    const name = e.target.name
    const value = e.target.value
    if (name == 'username' && this.validateEmail(value)) {
      this.setState({
        [name]: value,
        error: '',
        accountType: 1,
      })
    } else if (name == 'username' && parsePhoneNumber(value)) {
      this.setState({
        [name]: value.replace('+', ''),
        error: '',
        accountType: 2,
      })
    } else {
      this.setState({
        error: 'Enter valid email or phone',
        accountType: null,
      })
    }
  }
  submit = async (e) => {
    if (this.state.error != '') {
      return
    }
    try {
      await this.props.forgotPassword(
        this.state.username,
        this.state.accountType,
      )
      if (this.props.userToVerify.username) {
        this.props.history.push(ROUTE_FORGOT_PASSWORD_OTP_VERIFY)
      }
    } catch (e) {
      console.log('catch', e)
    } finally {
      console.log('finally')
    }
    return true
  }

  validateEmail = (email) => {
    const re = /\S+@\S+\.\S+/
    return re.test(email)
  }

  render() {
    return (
      <div className="form-module-cnt">
        <div className="row m-0 wh-100">
          <div className="col-12 col-md-7 p-0">
            <div className="form-info-cnt d-flex-box wh-100">
              <div className="back-link">
                <Link to="/">
                  <img className="" src={Back} alt="img" />
                  Back to Main
                </Link>
              </div>
              <div className="form-info">
                <div className="form-logo">
                  {' '}
                  <img className="" src={SmLogo} alt="img" />
                </div>
                <div className="lg-title">
                  resset password
                  <span className="dot-r" />
                </div>
                <div className="form">
                  <div className="form-group">
                    <label className="x-sm-text-fw6 text-uppercase">
                      Email / phone number
                    </label>
                    <input
                      className="form-control custom-input"
                      placeholder="Your Email or phone number"
                      name="username"
                      onChange={this.inputHandler}
                    />
                    {this.state.error && (
                      <span className="text-danger">{this.state.error}</span>
                    )}
                  </div>
                </div>
                <div className="form-group">
                  <button
                    onClick={this.submit}
                    className="btn black-bg-btn text-uppercase w-100"
                  >
                    Send
                  </button>
                </div>
                <div className="form-group text-center">
                  <Link to={ROUTE_LOGIN} className="md-text-fw6 color-black">
                    {' '}
                    Back to Login
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-5 p-0 h-100 d-none d-md-block">
            <div className="form-img-cnt wh-100">
              <img className="cover" src={ResetImgLg} alt="img" />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    userToVerify: selectUserToVerifyOtp(state),
  }),
  {
    forgotPassword,
  },
)(ForgotPassword)
