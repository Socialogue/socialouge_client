import React from 'react'
import { connect } from 'react-redux'
import {
  selectOtpErrorMsg,
  selectOtpSendMsg,
  selectToken,
  selectUserToVerifyOtp,
} from 'core/selectors/user'
import { verifyOtp, resendOtp } from 'core/actions/authActionCreators'
import { Link } from 'react-router-dom'

import SmLogo from '../../images/sm-logo.svg'
import Back from '../../images/icons/back.svg'
import LoginImgLg from '../../images/lr.webp'
import { ROUTE_HOME } from '../../routes/constants'
import { errorAlert, successAlert } from '../../utils/alerts'

class Otp extends React.Component {
  constructor() {
    super()
    this.state = {
      loading: false,
      otp: '',
    }
  }

  componentDidUpdate = (prevProps) => {
    if (prevProps.otpSendMsg !== this.props.otpSendMsg) {
      if (this.props.otpSendMsg !== '') {
        successAlert(this.props.otpSendMsg)
      }
    }

    if (prevProps.otpErrorMsg !== this.props.otpErrorMsg) {
      if (this.props.otpErrorMsg !== '') {
        errorAlert(this.props.otpErrorMsg)
      }
    }
  }

  handleOtpChange = (e) => {
    const name = e.target.name
    const value = e.target.value

    this.setState({
      otp: value,
    })
  }

  onSubmit = async (e) => {
    if (this.state.otp.length < 4) {
      alert('please enter otp currectly')
      return
    }

    try {
      await this.props.verifyOtp(
        this.state.otp,
        this.props.user.username,
        this.props.user.accountType,
      )
      if (this.props.isLoggedIn) {
        this.props.history.push(ROUTE_HOME)
      }
    } catch (e) {
      console.log('catch', e)
    } finally {
      console.log('finally')
    }
    return true
  }

  handleResendOtp = () => {
    this.props.resendOtp(this.props.user.username, this.props.user.accountType)
  }

  render() {
    return (
      <div className="form-module-cnt">
        <div className="row m-0 wh-100">
          <div className="col-12 col-md-7 p-0 h-100">
            <div className="form-info-cnt d-flex-box wh-100">
              <div className="back-link">
                <Link to={ROUTE_HOME}>
                  <img className="" src={Back} alt="img" />
                  Back to Main
                </Link>
              </div>
              <div className="form-info">
                <div className="form-logo">
                  {' '}
                  <img className="" src={SmLogo} alt="img" />
                </div>
                <div className="lg-title">
                  Verify OTP
                  <span className="dot-r" />
                </div>
                <div className="form">
                  <div className="form-group">
                    <label className="x-sm-text-fw6 text-uppercase">OTP</label>
                    <input
                      className="form-control custom-input"
                      placeholder="Enter your otp here"
                      onChange={this.handleOtpChange}
                    />
                  </div>
                </div>
                <div className="form-group">
                  <button
                    className="btn black-bg-btn text-uppercase w-100"
                    onClick={this.onSubmit}
                  >
                    Verify
                  </button>
                </div>
                <span
                  className="color-orange cursor-pointer font-weight-bold"
                  onClick={this.handleResendOtp}
                >
                  Resend OTP
                </span>
                <div className="form-group text-center">
                  <span className="md-text-fw6">Don’t have an account?</span>
                  <Link to="/register" className="md-text-fw6 color-black">
                    {' '}
                    Register
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-5 p-0 h-100 d-none d-md-block">
            <div className="form-img-cnt wh-100">
              <img className="cover" src={LoginImgLg} alt="img" />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    user: selectUserToVerifyOtp(state),
    isLoggedIn: !!selectToken(state),
    auth: state.auth,
    otpSendMsg: selectOtpSendMsg(state),
    otpErrorMsg: selectOtpErrorMsg(state),
  }),
  {
    verifyOtp,
    resendOtp,
  },
)(Otp)
