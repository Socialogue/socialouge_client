import { selectCartData, selectCartPrice } from 'core/selectors/cart'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import Header from '../home/Header/Header'
import CartInfo from './components/cartInfo'
import CartTable from './components/cartTable'
import { updateCart } from 'core/actions/productActionCreators'
import { updateCartDb } from 'core/actions/cartActionCreators'
import { selectUser } from 'core/selectors/user'

class Cart extends Component {
  state = {
    cart: [],
    quantity: 1,
    totalPrice: '',
    cartPrice: 0,
  }

  handleQtyAndPrice = (quantity, totalPrice, id) => {
    this.setState({
      quantity: quantity,
      totalPrice: totalPrice,
    })
    const cart = [...this.props.cart]
    let finalPrice = this.props.cartPrice

    let updatedCart = cart.map((item) => {
      if (item.color._id == id) {
        item.quantity = quantity

        item.totalPrice = +quantity * +item.price // quantity*price...
        item.vatAmmount = (+item.totalPrice * +item.vat) / 100 // (totalPrice*vat)/100
        finalPrice -= item.grandTotal
        item.grandTotal = +item.totalPrice + +item.vatAmmount // totalPrice+vatAmmount;
        finalPrice += item.grandTotal
      }
      return item
    })

    this.props.updateCart(updatedCart, finalPrice)
  }

  removeFromCart = (key) => {
    const updatedCart = [...this.props.cart]
    const finalPrice = this.props.cartPrice
    const updatedfinalPrice = +finalPrice - +updatedCart[key].grandTotal
    // delete updatedCart[key];
    updatedCart.splice(key, 1)
    this.props.updateCart(updatedCart, updatedfinalPrice)
    this.props.updateCartDb(updatedCart, updatedfinalPrice, this.props.user._id)
    this.setState({
      cart: updatedCart,
    })
  }

  componentDidUpdate = (prevProps) => {
    if (JSON.stringify(prevProps.cart) !== JSON.stringify(this.props.cart)) {
      if (this.props.cart) {
        //
      }
    }
  }

  render() {
    const { cart, cartPrice } = this.props
    if (!cart) {
      return null
    }
    return (
      <div>
        <Header />
        <div className="max-content-width-1 mt-4 mt-md-5 mb-md-5 pb-md-5 px-4">
          <div className="row w-100 cart-row">
            <CartTable
              cart={cart}
              cartPrice={cartPrice}
              quantity={this.props.quantity}
              totalPrice={this.props.totalPrice}
              handleQtyAndPrice={this.handleQtyAndPrice}
              removeFromCart={this.removeFromCart}
            />
            <CartInfo cart={cart} totalPrice={this.props.cartPrice} />
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    user: selectUser(state),
    cart: selectCartData(state),
    cartPrice: selectCartPrice(state),
  }),
  { updateCart, updateCartDb },
)(Cart)
