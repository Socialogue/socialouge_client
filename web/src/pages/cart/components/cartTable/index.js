import React, { Component } from 'react'
import Excla from '../../../../images/icons/excla'
import Info from '../../../../images/icons/info'
import CartProduct from './cartProduct'

class CartTable extends Component {
  render() {
    const { cart, cartPrice } = this.props
    if (!cart) {
      return null
    }
    console.log('cart: ', cart)
    return (
      <div className="col-12 col-lg-8  pr-0 pr-md-5">
        <table className="table black-border cart-table mb-0">
          <tbody>
            <tr>
              <td colSpan="2" className=" pl-0">
                <span className="d-flex align-items-center">
                <span className="md-title font-weight-6 w-100">
                  bag
                  <span className="dot-r" />
                </span>
                <span className="d-inline-flex d-md-none">
                  <Excla />
                </span>
                </span>
              </td>
              <td className="sm-text-fw6-g text-uppercase  text-center d-none d-md-table-cell">
                Product name
              </td>
              <td className="sm-text-fw6-g text-uppercase text-nowrap text-center d-none d-md-table-cell">
                Price each
              </td>
              <td className="sm-text-fw6-g text-uppercase  text-center d-none d-md-table-cell">
                quantity
              </td>
              <td className="sm-text-fw6-g text-uppercase  text-center pr-0 d-none d-md-table-cell">
                Total
              </td>
            </tr>
            {cart.length < 1 && (
              <h3 className="mt-3 color-orange">No product in cart!</h3>
            )}
            {cart.map((product, i) => {
              return (
                <CartProduct
                  product={product}
                  cartPrice={cartPrice}
                  key={i}
                  item={i}
                  quantity={this.props.quantity}
                  totalPrice={this.props.totalPrice}
                  handleQtyAndPrice={this.props.handleQtyAndPrice}
                  removeFromCart={this.props.removeFromCart}
                />
              )
            })}
          </tbody>
        </table>
      </div>
    )
  }
}

export default CartTable
