import { getImageOriginalUrl } from 'core/utils/helpers'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Close from '../../../../../images/icons/close'
import CloseSm from '../../../../../images/icons/closeSm'
import Img from './../../../../../images/eco-img.webp'

class CartProduct extends Component {
  state = {
    quantity: 1,
    totalPrice: '',
  }

  quantityCng = (e) => {
    const quantity = e.target.value
    this.setState({ quantity: quantity })
    const price = this.props.product.color.price
    this.props.handleQtyAndPrice(quantity, price, this.props.product.color._id)
    this.setState({ quantity: quantity, totalPrice: price })
  }

  incQuantity = (e) => {
    const quantity = +this.props.product.quantity + 1
    const price = this.props.product.color.price
    this.props.handleQtyAndPrice(quantity, price, this.props.product.color._id)
    this.setState({ quantity: quantity, totalPrice: price })
  }

  decQuantity = (e) => {
    const quantity = +this.props.product.quantity - 1
    const price = -this.props.product.color.price
    this.props.handleQtyAndPrice(quantity, price, this.props.product.color._id)
    this.setState({ quantity: quantity, totalPrice: price })
  }

  render() {
    const { product, cartPrice } = this.props
    if (!product) {
      return null
    }

    console.log('product: ', product.color.color)
    return (
      <tr>
        <td className="border-0 d-none d-md-table-cell">
          {' '}
          <span
            className="scale-hover cursor-pointer"
            onClick={() => this.props.removeFromCart(this.props.item)}
          >
            <CloseSm />
          </span>
        </td>
        <td className="px-0 img-td">
          <span className="d-flex align-items-center">
            <img
              className=""
              width="80"
              height="116"
              src={
                product.color && getImageOriginalUrl(product.color.colorImage)
              }
              alt="img"
            />
          </span>
        </td>
        <td className="align-top position-relative pb-4">
          <span
            onClick={() => this.props.removeFromCart(this.props.item)}
            className="close-cart scale-hover d-inline-block d-md-none"
          >
            <CloseSm />
          </span>
          <div className="font-weight-bold text-uppercase">
            {product.product && product.product.title}
          </div>
          <div className="sm-text-fw6 mb-2">
            by{' '}
            <Link to="/" className="text-decoration-underline text-uppercase">
              {product.product &&
                product.product.shopId &&
                product.product.shopId.shopName}
            </Link>
            <p className="m-0 p-0 d-none d-md-block">
              color : {product.color.color.name}
            </p>
            <p className="m-0 p-0  d-none d-md-block">
              size : {product.color.sizes.lt}
            </p>
          </div>
          <div className="cart-p-code w-100 pl-2 ml-1">
            <div className="sm-text-fw4-g d-none d-md-block">
              Product code: 1946487
            </div>
            {/* for mobile start */}
            <div className="d-flex align-items-center d-md-none">
              <div className="position-relative d-flex-box">
                <span
                  className="scale-hover cursor-pointer"
                  onClick={this.decQuantity}
                >
                  -
                </span>
                <input
                  className="td-input"
                  type="text"
                  value={product.quantity}
                  onChange={this.quantityCng}
                />
                <span
                  className="scale-hover cursor-pointer"
                  onClick={this.incQuantity}
                >
                  +
                </span>
              </div>
              <span className="w-100 text-right font-weight-6 pr-1">
                {' '}
                € {product.color.price * product.quantity}
              </span>
            </div>
            {/* for mobile end */}
          </div>
        </td>
        <td className="font-weight-6  text-center text-nowrap d-none d-md-table-cell">
          € {product.color && product.color.price}
        </td>

        {/* for desktop start */}
        <td className="font-weight-6  text-center d-none d-md-table-cell">
          <div className="position-relative d-flex-box">
            <span
              className="scale-hover cursor-pointer"
              onClick={this.decQuantity}
            >
              -
            </span>
            <input
              className="td-input"
              type="text"
              value={product.quantity}
              onChange={this.quantityCng}
            />
            <span
              className="scale-hover cursor-pointer"
              onClick={this.incQuantity}
            >
              +
            </span>
          </div>
        </td>
        {/* for desktop end */}

        <td className="font-weight-6 text-center  text-nowrap d-none d-md-table-cell">
          € {product.color.price * product.quantity}
        </td>
      </tr>
    )
  }
}

export default CartProduct
