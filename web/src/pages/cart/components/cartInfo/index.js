import { selectCartVat } from "core/selectors/cart";
import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import SingleLineInput from "../../../../components/SingleLineInput";
import ArrowRight from "../../../../images/icons/arrowRight";
import Excla from "../../../../images/icons/excla";
import { ROUTE_CHECKOUT } from "../../../../routes/constants";

class CartInfo extends Component {
  state = {
    vat: 0
  };

  componentDidMount = () => {
    const { cart } = this.props;
    let vat = 0;
    cart.map(item => {
      vat += item.vatAmmount;
    });
    this.setState({ vat: vat });
  };

  render() {
    const { totalPrice } = this.props;
    if (!totalPrice) {
      return null;
    }
    return (
      <div className="col-12 col-lg-4">
        {/* <div className="mb-5 pb-5 mt-5">
          <div className="position-relative cart-in-div">
            <button className="btn border-btn border-0 p-0 cart-btn">
              Add <ArrowRight />
            </button>
            <SingleLineInput
              label="enter a discount code"
              placeholder="Code"
              type="text"
              name="code"
            />
          </div>
        </div> */}
        <div className="mt-0 mt-md-4">
          <div className="font-weight-bold text-uppercase mb-2 d-none d-md-block">
            Ordering overview
          </div>
          <div className="overview-box mb-0 mb-md-5">
            <div className="overview-cnt">
              <div className="d-none d-md-block">
                <div className="sm-text-fw6 text-uppercase mb-3 ">
                  <span className="mr-2">Products price: </span>
                  <span className="float-right">€ {totalPrice}</span>
                </div>
                <div className="sm-text-fw6 text-uppercase mb-3">
                  <span className="mr-2">Vat: </span>
                  <span className="float-right">€ {this.props.vat}</span>
                </div>
              </div>
              <div className="sm-text-fw6 text-uppercase pt-3 mb-3 cart-total-price d-flex align-items-center">
                <span className="w-100">
                  <div className="position-relative cart-in-div d-block d-md-none">
                    <button className="btn border-btn border-0 p-0 cart-btn">
                      <ArrowRight />
                    </button>
                    <SingleLineInput
                      label=""
                      placeholder="Discount code"
                      type="text"
                      name="code"
                    />
                  </div>
                </span>
                <span className="d-flex align-items-center">
                  Total
                  {/* for desktop start */}
                  <span className="text-nowrap d-none d-md-block  ml-2">
                    € {totalPrice + +this.props.vat}
                  </span>
                   {/* for desktop end */}

                    {/* for mobile start */}
                  <span className="text-nowrap m-total d-block d-md-none ml-2">
                    € {totalPrice + +this.props.vat}
                  </span>
                   {/* for mobile end */}
                </span>
              </div>
            </div>
            <button
              className="btn text-uppercase w-100 black-bg-btn"
              onClick={() => this.props.history.push(ROUTE_CHECKOUT)}
            >
              Proceed to checkout
            </button>
          </div>
          <div className="d-none d-md-block">
            <div>
              <Excla />
            </div>
            <div className="x-sm-text-fw4-g">
              There are products in your cart from 2 different sellers, thats
              why your order will be devided into two seperate payments.
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(
  connect(state => ({
    vat: selectCartVat(state)
  }))(CartInfo)
);
