import React, { Component } from "react";
import { Link } from "react-router-dom";
import BlackLogo from "../../images/blackLogo";
import NotificationMessage from "../Common/notificationMessage/notificationMessage";
import SetTimeMessage from "../Common/setTimeMessage/setTimeMessage";
import "./buyer.scss";
class BuyerEmail extends Component {
  render() {
    return (
      <div className="buyer-section">
        <div className="buyer-cnt">
          <div className="md-title md-title-res25 font-weight-6 text-center mb-4 mb-md-5 pb-4 pb-md-5">
            Thanks for your order at socialogue!
          </div>
          <div className="be-message">
            <div className="hi-txt font-weight-6 text-center mb-3">
              Hi, John
            </div>

            {/* same plece text */}
            <div className="text-center mb-3 px-0 px-md-4 mx-2">
              Your order has been confirmed and the goods are being prepared for
              shipment. When the goods are ready, we will inform you about it in
              a separate letter.
            </div>
            {/* Or */}
            <div className="text-center mb-3 px-0 px-md-4 mx-2">
              An item you listed in your butiq{" "}
              <span className="font-weight-bold">„Name of Buitq“</span> has been
              ordered by John Smith.
            </div>
            {/* same plece text */}

            <div className="md-title md-title-res25 font-weight-6 text-center mb-3">
              Order number: 546425
            </div>
            <div className="pb-3 border-bottom-1 font-weight-bold text-uppercase">
              Order information
            </div>
            <div className="row py-2 mx-0 border-bottom-1">
              <div className="col-12 col-sm-6 my-3 px-0">
                <div className="font-weight-bold text-uppercase">Order ID:</div>
                <div className="sm-text-fw5-g">123214123</div>
              </div>
              <div className="col-12 col-sm-6 my-3 px-0">
                <div className="font-weight-bold text-uppercase">
                  Invoice Written to:
                </div>
                <div className="sm-text-fw5-g">Jogn@domain.com</div>
              </div>
              <div className="col-12 col-sm-6 my-3 px-0">
                <div className="font-weight-bold text-uppercase">
                  Order date:
                </div>
                <div className="sm-text-fw5-g">July 21, 2020</div>
              </div>
              <div className="col-12 col-sm-6 my-3 px-0">
                <div className="font-weight-bold text-uppercase">Comapny:</div>
                <div className="sm-text-fw5-g">Uniq Butiq</div>
              </div>
              <div className="col-12 col-sm-6 my-3 px-0">
                <div className="font-weight-bold text-uppercase">Payment:</div>
                <div className="sm-text-fw5-g">Paysera</div>
              </div>
              <div className="col-12 col-sm-6 my-3 px-0">
                <div className="font-weight-bold text-uppercase">Delivery:</div>
                <div className="sm-text-fw5-g">DPD Fast 1 - 2 Days</div>
              </div>
            </div>
            <div className="row py-2 mx-0 border-bottom-1">
              <div className="col-12 col-md-4 my-3 px-0">
                <div className="sm-text-fw6-g text-uppercase mb-3">
                  Order information
                </div>
                <div className="font-weight-bold text-uppercase">
                  limited edition coat
                </div>
                <div className="sm-text-fw5-g">Product code: 1946487</div>
              </div>
              <div className="col-12 col-md-4 my-3 px-0  text--md-center">
                <div className="sm-text-fw6-g text-uppercase mb-3">
                  quantity:
                </div>
                <div className="font-weight-bold text-uppercase">1</div>
              </div>
              <div className="col-12 col-md-4 my-3 px-0 text-md-center">
                <div className="sm-text-fw6-g text-uppercase mb-3">Price:</div>
                <div className="font-weight-bold text-uppercase">€ 240.00</div>
              </div>
            </div>
            <div className="row py-3 mx-0 border-bottom-1">
              <div className="col-12 col-md-6 px-0" />
              <div className="col-12 col-md-6 px-0">
                <div className="overview-cnt">
                  <div className="sm-text-fw6 text-uppercase mb-3">
                    <span>Products price: </span>
                    <span className="float-right">€ 240.00</span>
                  </div>
                  <div className="sm-text-fw6 text-uppercase mb-3">
                    <span>Vat: </span>
                    <span className="float-right">€ 41.32</span>
                  </div>
                  <div className="sm-text-fw6 text-uppercase mb-3">
                    <span>Delivery: </span>
                    <span className="float-right">€ 2.32</span>
                  </div>
                  <div className="sm-text-fw6 text-uppercase pt-3 cart-total-price d-flex">
                    <span className="w-100" />
                    <span className="text-nowrap">Total: € 720.00</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="row pt-3 mx-0">
              <div className="col-12 col-md-6 px-0 mb-4 mb-md-0">
                <div className="font-weight-bold text-uppercase mb-2">
                  buyer
                </div>
                <div className="font-weight-5 mb-2">John Smith</div>
                <div className="font-weight-5 mb-2">
                  Little George St, Westminster, London
                </div>
                <div className="font-weight-5 mb-2">London. 123432</div>
                <div className="font-weight-5 mb-2">United Kingdom</div>
                <div className="font-weight-5 mb-2">Phone: +1232 123 235</div>
              </div>
              <div className="col-12 col-md-6 px-0">
                <div className="font-weight-bold text-uppercase mb-2">
                  Deliver to the following address
                </div>
                <div className="font-weight-5 mb-2">John Smith</div>
                <div className="font-weight-5 mb-2">
                  Little George St, Westminster, London
                </div>
                <div className="font-weight-5 mb-2">London. 123432</div>
                <div className="font-weight-5 mb-2">United Kingdom</div>
                <div className="font-weight-5 mb-2">Phone: +1232 123 235</div>
              </div>
            </div>
          </div>
          <div className="text-center mt-4 pt-2">
            <Link to="/">
              <BlackLogo />
            </Link>
          </div>
        </div>
        
        {/* <NotificationMessage /> */}
        {/* <SetTimeMessage message="success massage" class="set-message-show success" /> */}
        {/* <SetTimeMessage message="error massage" class="set-message-show error" /> */}
      </div>
    );
  }
}

export default BuyerEmail;
