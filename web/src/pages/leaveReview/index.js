import React, { Component } from "react";
import FixedFooter from "../Common/fixedFooter";
import Footer from "../home/Footer/Footer";
import Header from "../home/Header/Header";
import Star from "../../images/icons/star";
import Share from "../../images/icons/share";
import Img from "../../images/register-img.webp";
import { Link } from "react-router-dom";

class LeaveReview extends Component {
  setStar(star, index, e) {
    document.getElementById("star").value = star;
    console.log(index);
    document.querySelectorAll(".r-star").forEach(function (userItem, i) {
      userItem.classList.remove("active");
    });
    document.querySelectorAll(".r-star").forEach(function (userItem, i) {
      if (i >= index - 1) userItem.classList.add("active");
    });
  }
  render() {
    return (
      <div>
        <Header />
        <div className="leave-review-cnt">
          <div className="row">
            <div className="col-12 col-md-4 col-lg-3 pr-3 pr-md-1 mb-4 mb-md-0">
              <div className="product">
                <div className="show-img-pp show-img-pp-pb-140 mb-3">
                  <div className="show-img-p cursor-pointer">
                    <img className="cover" src={Img} alt="img" />
                  </div>
                </div>
                <div className="">
                  <div className="mb-3">
                    <div className="md-text-fw6 text-uppercase cursor-pointer">
                      wide brim bucket hat
                    </div>
                    <div className="md-text-fw6">
                      {" by "}

                      <Link
                        to=""
                        className="text-uppercase text-decoration-underline"
                      >
                        Bullek
                      </Link>
                    </div>
                  </div>
                  <div className="">
                    <span>
                      <span className="sm-text-fw4-g mr-2">12 days</span>
                      <span className="sm-text-fw6">20 €</span>
                    </span>

                    <span className="float-right">
                      <span className="comment mr-2 d-inline-flex align-items-center">
                        <Star />
                      </span>
                      <span className="dl d-inline-flex align-items-center">
                        <span className="com-icon">
                          <Share />
                        </span>
                      </span>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 col-md-8 col-lg-9 pl-3 pl-md-5">
              <form>
                <div className="form-group mb-4">
                  <div className="font-weight-bold text-uppercase mb-3">
                    Your review <sup>*</sup>
                  </div>
                  <textarea
                    className="form-control textarea-msg mb-3"
                    placeholder="Write review..."
                  />
                  <div className="color-gray7 font-weight-5">
                    Tell other people more about the product. What about the
                    quality? Or the comfort?
                  </div>
                </div>
                <div className="form-group mb-4">
                  <div className="font-weight-bold text-uppercase">
                    Your overall rating <sup>*</sup>
                  </div>
                  <div className="color-gray7 font-weight-5">Please select</div>
                  <div class="rating rating2">
                    <input type="hidden" name="star" id="star" />
                    <span
                      className="r-star cursor-pointer"
                      onClick={(e) => this.setStar(5, 1, e)}
                      title="Give 5 stars"
                    >
                      ★
                    </span>
                    <span
                      className="r-star cursor-pointer"
                      onClick={(e) => this.setStar(4, 2, e)}
                      title="Give 4 stars"
                    >
                      ★
                    </span>
                    <span
                      className="r-star cursor-pointer"
                      onClick={(e) => this.setStar(3, 3, e)}
                      title="Give 3 stars"
                    >
                      ★
                    </span>
                    <span
                      className="r-star cursor-pointer"
                      onClick={(e) => this.setStar(2, 4, e)}
                      title="Give 2 stars"
                    >
                      ★
                    </span>
                    <span
                      className="r-star cursor-pointer"
                      onClick={(e) => this.setStar(1, 5, e)}
                      title="Give 1 star"
                    >
                      ★
                    </span>
                  </div>
                </div>
                <div className="form-group mb-4  pb-2">
                  <div className="font-weight-bold text-uppercase mb-2">
                    Upload photo
                  </div>
                  <div className="d-block d-sm-flex upload-box">
                    <div className="upload-div1 position-relative">
                      Select photo +
                      <input
                        type="file"
                        className="ctm-hidden-upload-input cover"
                      />
                    </div>
                    <div className="upload-div2 text-center text-sm-left">
                      Upload your .PNG or .JPG file
                    </div>
                  </div>
                </div>
                <div className="form-group text-right mb-0">
                  <button className="btn border-btn px-5"> Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <Footer />
        <div className="ab-mf px-4 mb-4 d-block d-md-none">
          <FixedFooter />
        </div>
      </div>
    );
  }
}

export default LeaveReview;
