import React from "react";
import Up from "../../../../images/icons/up";
import Excla from "../../../../images/icons/excla";
import Item from "./Item";
import { addOrUpdate } from "core/utils/array";

class VariationsPhotos extends React.Component {
  state = {
    images: [], //{colorId, file}
  };

  getState = () => {
    return this.state;
  };

  onSelectFile = (color, e) => {
    const { images } = this.state;
    const updated = addOrUpdate(
      images,
      { colorId: color._id, file: e.target.files[0] },
      "colorId"
    );
    this.setState({ images: updated }, this.onChange);
  };

  onClickDelete = (color) => {
    const { images } = this.state;
    const updated = images.filter((i) => i.colorId !== color._id);
    this.setState({ images: updated }, this.onChange);
  };

  onChange = () => {
    const { images } = this.state;
    this.props.onChange(images);
  };

  render() {
    const { colors } = this.props;
    const { images } = this.state;

    return (
      <div>
        <div className="row mb-4">
          <div className="col-12 col-md-7">
            <div className="md-title text-uppercase d-inline-flex align-items-center mb-1">
              <Excla />
              <span className="px-2">VARIATIONS Photos</span> <Up />
            </div>
            <div className="sm-text-fw4-g">
              Chnge photos in your listing based on this attribute. This
              determines which photos buyers see when they select a variation
              option.
            </div>
          </div>
        </div>
        <div className="row mr-0 mb-5">
          {colors.map((color) => {
            const file = images.find((i) => i.colorId === color._id);
            return (
              <Item
                color={color}
                onSelectFile={this.onSelectFile}
                file={file ? file.file : null}
                onClickDelete={this.onClickDelete}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

export default VariationsPhotos;
