import React from "react";

import TopArrow from "../../../../images/icons/topArrow";
import Delete from "../../../../images/icons/delete";

export default class Item extends React.Component {

  render() {

    const {color, file} = this.props;

    return (
      <div className="col-12 col-color-upload pr-0 mb-3">
        <div className="sm-text-fw6 mb-2 text-uppercase">{color.name}</div>
        <div className="show-img-pp show-img-pp-pb-90 upload-img">
          <div className="upload-photo-cnt d-flex-box">
            <div className="upload-photo d-flex-box">
              <div className="upload-photo-txt">
                <div className="upload-icon">
                  <TopArrow />
                </div>
                {file && <img src={URL.createObjectURL(file)} alt=""/>}
                <input
                  onChange={(e) => this.props.onSelectFile(color, e)}
                  type="file"
                  className="ctm-hidden-upload-input cover"
                />
              </div>
              <div className="upload-txt x-sm-text-fw4-g">
                Select photo by <br />
                {color.name} colour
              </div>
            </div>
          </div>
        </div>
        <div className="x-sm-text-fw6 mt-2 text-uppercase" onClick={() => this.props.onClickDelete(color)}>
          <span className="d-inline-flex align-items-center cursor-pointer">
           <span className="mr-1 d-flex"> <Delete /></span> Delete
          </span>
        </div>
      </div>
    )
  }
}
