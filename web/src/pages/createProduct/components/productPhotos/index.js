import React from 'react';
import { Link } from 'react-router-dom';
import Delete from './../../../../images/icons/delete';
import Screen from './../../../../images/icons/screen';
import Upload from './../../../../images/icons/upload';
import Image from './../../../../images/eco-img.webp';

class ProductPhotos extends React.Component {
  state = {
    images: [],
    mainImage:null
  }

  getState = () => {
    return this.state;
  };


  onSelectExtraImages = (event) => {
    event.stopPropagation();
    event.preventDefault();
    const selected = Array.from(event.target.files);
    this.setState((prevState) => {
      const {images} = prevState;
      const updated = images.concat(selected);
      return {images: updated};
    });
  }

  onSelectMainImage = (event) => {
    event.stopPropagation();
    event.preventDefault();
    const file = event.target.files[0];this.setState({ mainImage: file});

  }

  removeByIndex = (index) => {
    this.setState((prevState) => {
      const {images} = prevState;
      images.splice(index, 1);
      return {images};
    });
  }

  removeMainImage=()=>{
    this.setState({mainImage:null})
  }

  onDragStart = (event, index, draggingMain = false) => {
    event.dataTransfer.setData("text/html", String(event.target.outerHTML));
    event.dataTransfer.setData("dragIndex", index);
    if (draggingMain) {
      event.dataTransfer.setData("draggingMain", true);
    }
  }

  onDrop = (event, dropIndex, droppingOnMain = false) => {
    const dragIndex = event.dataTransfer.getData("dragIndex");
    const draggingMain = event.dataTransfer.getData("draggingMain");
    const {images} = this.state;

    if (droppingOnMain && this.state.mainImage) {
      const mainImage = images[dragIndex];
      images[dragIndex] = this.state.mainImage;
      this.setState({images, mainImage});
      return true;
    }

    if (draggingMain) {
      const mainImage = images[dropIndex];
      images[dropIndex] = this.state.mainImage;
      this.setState({images, mainImage});
      return true;
    }

    const tmp = images[dropIndex]
    images[dropIndex] = images[dragIndex];
    images[dragIndex] = tmp;
    this.setState({images});
    return true;
  }

  /*
  * The default drop behavior for certain DOM elements like <div>s in browsers typically does not accept dropping.
  * This behavior will intercept the behavior we are attempting to implement.
  * To ensure that we get the desired drop behavior, we will apply preventDefault.
  * */
  onDragOver = (event) => {
    event.preventDefault();
  }

  render() {
    const {mainImage} = this.state;

    return (
      <div>
        <div className='row mb-4'>
          <div className='col-12 col-md-5'>
            <div className='md-title text-uppercase mb-1'>PRODUCT PHOTOS</div>
            <div className='sm-text-fw4-g'>
              Add as many as you can so buyers can see every detail. Use up to 15
              photos to show your items's most important qualities.
            </div>
          </div>
        </div>
        <input
          id='myInput'
          multiple
          type='file'
          ref={ref => this.upload = ref}
          style={{ display: 'none' }}
          onChange={this.onSelectMainImage}
        />
        <div className='row mb-5'>
          <div className='col-12 col-md-5 pb-3'onClick={() => {
              this.upload.click();
            }}
               draggable
               onDragStart={(e) => this.onDragStart(e, 0)}
               onDragOver={this.onDragOver}
               onDrop={(e) => this.onDrop(e, 0)}>
            <div className='position-relative wh-100'>
              <img className='cover' src={mainImage ? URL.createObjectURL(mainImage) : Image} />
              <div className='photo-control'>
                <span className='screen-icon'>
                  <Screen />
                </span>
                <span className='photo-delete d-flex-box x-sm-text-fw6'
                               onClick={() => this.removeMainImage()}>
                            <Delete />Delete
                          </span>
                <span className='photo-txt x-sm-text-fw6'>#1 (Main Photo)</span>
              </div>
            </div>
          </div>
          <div className='col-12 col-md-7'>
            <div className='row mr-0'>
              {this.state.images.map( (image, i) => {
                var mainImg = [];
                mainImg.push(image);
                return (
                  <div className='col-12 col-sm-6 col-md-4 pr-0 mb-3'
                       key={i}
                       draggable
                       onDragOver={this.onDragOver}
                       onDragStart={(e) => this.onDragStart(e, i)}
                       onDrop={(e) => this.onDrop(e, i)}
                  >
                    <div className='show-img-pp show-img-pp-pb-90 upload-img'>
                      <div className='show-img-p'>
                        <img className='cover' src={window.URL.createObjectURL(
                            new Blob(mainImg, { type: "jpeg/jpg/png" })
                          )} />
                        <div className='photo-control'>
                      <span className='screen-icon'>
                        <Screen />
                      </span>
                          <div className='photo-delete d-flex-box x-sm-text-fw6'
                               onClick={() => this.removeByIndex(i)}>
                            <Delete />Delete
                          </div>
                          <span className='photo-txt x-sm-text-fw6'>#{i+2}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                )
              })
              }
              <div className='col-12 col-sm-6 col-md-4 pr-0 mb-3'>
                <div className='show-img-pp show-img-pp-pb-90 upload-img'>
                  <div className='upload-photo-cnt d-flex-box'>
                    <div className='upload-photo d-flex-box'>
                      <div className='upload-photo-txt'>
                        <div className='upload-icon'>
                          <Upload />
                        </div>
                        <input
                          type='file'
                          className='ctm-hidden-upload-input cover'
                          multiple
                          onChange={this.onSelectExtraImages}
                        />
                      </div>
                      <div className='upload-txt x-sm-text-fw4-g'>
                        Upload Photos
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default ProductPhotos;
