import {
  selectCatData,
  selectInnerCatData,
  selectSubCatData,
} from 'core/selectors/products'
import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { VIEW_CATEGORY } from '../../initialState'
import ExcB from './../../../../images/icons/excIcon'
const TopPart = (props) => (
  <div>
    <nav aria-label="breadcrumb" className="mb-5 d-none d-md-block">
      <ol className="breadcrumb custom-breadcrumb">
        <li className="breadcrumb-item">
          <Link to="#">Home</Link>
        </li>
        <li className="breadcrumb-item">
          <Link to="#">Settings</Link>
        </li>
        <li className="breadcrumb-item active">Product listing</li>
      </ol>
    </nav>
    <div className="row">
      <div className="col-12">
        <div className="x-md-title border-bottom-1 pb-3 mb-3">
          Product listing
          <span className="dot-r" />
        </div>
      </div>
      <div className="col-12 col-md-8 d-none d-md-block">
        <div className="sm-text-fw4-g">Your product description</div>
      </div>
      <div className="col-12 col-md-4 text-right">
        <div className="font-weight-bold mb-2">CATEGORIES</div>
        <nav aria-label="breadcrumb" className="">
          <ol className="breadcrumb custom-breadcrumb mb-2 justify-content-end">
            {props.categoryData.name && (
              <li className="breadcrumb-item">
                <Link to="#">{props.categoryData.name}</Link>
              </li>
            )}
            {props.subCategoryData.name && (
              <li className="breadcrumb-item">
                <Link to="#">{props.subCategoryData.name}</Link>
              </li>
            )}
            {props.innerCategoryData.name && (
              <li className="breadcrumb-item active">
                {props.innerCategoryData.name}
              </li>
            )}
          </ol>
        </nav>
        <div className="">
          <span
            className="color-primary-gray text-decoration-underline cursor-pointer"
            onClick={() => props.changeView(VIEW_CATEGORY)}
          >
            Edit categories
          </span>
        </div>
      </div>
    </div>
    <div className="form-group row mb-5 mt-3 mx-0">
      <div className="col-12 col-md-5 p-0">
        <div className="x-sm-text-fw6 text-uppercase mb-3">
          Enter product name
        </div>
        <div className="form-input position-relative">
          <span className="prepend-input">
            <ExcB />
          </span>
          <input
            type="text"
            className="form-control custom-input"
            placeholder="Product name here"
            name="productName"
            value={props.productName}
            onChange={props.onChangeName}
          />
        </div>
      </div>
    </div>
  </div>
)

export default connect(
  (state) => ({
    categoryData: selectCatData(state),
    subCategoryData: selectSubCatData(state),
    innerCategoryData: selectInnerCatData(state),
  }),
  {},
)(TopPart)
