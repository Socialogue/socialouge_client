import { days } from "core/utils/dummyDate";
import React, { Component } from "react";
import InputWithoutLabel from "../../../../components/InputWithoutLabel";
import SingleSelect from "../../../../components/SingleSelect";
import HorizontalInput from "../../../userSettings/components/horizontalInput";
import HorizontalSelectOption from "../../../userSettings/components/horizontalSelectOption";
class ShippingOptions extends Component {
  state = {
    days: []
  };

  componentDidMount = () => {
    const daysData = days();
    this.setState({ days: daysData });
  };

  onChange = (value, name) => {
    this.props.addShippingData(value, name);
  };

  onInputChange = (name, value) => {
    this.setState({
      [name]: value
    });
  };
  handleInputChange = (name, value) => {
    this.setState({
      [name]: value
    });
    this.props.deliveryData(name, value);
  };
  render() {
    const { countries, carriers } = this.props;
    return (
      <div>
        <div className="row mb-4">
          <div className="col-12 col-md-7">
            <div className="md-title text-uppercase mb-1">SHIPPING OPTIONS</div>
            <div className="sm-text-fw4-g">
              Fill out your shipping options for this listing. You can keep
              these options specific to this listing, or save them as a shipping
              profile to apply them to future listings.
            </div>
          </div>
          <div className="col-12 col-md-1"></div>
          <div className="col-12 col-md-4 mb-4">
            <div className="single-group hide-label w-100">
              <HorizontalSelectOption
                formInputClass="form-input position-relative"
                label=""
                name="templete"
                errorText=""
                isRequired={false}
                imageRequired={false}
                selectedText="Choose shipping template"
                options={["Template1", "Template2"]}
                onChange={this.onInputChange}
              />
            </div>
          </div>
        </div>
        <div className="row mb-5">
          <div className="col-12 col-md-4 mb-4">
            <div className="form-group">
              <div className="single-group label-mb">
                <HorizontalSelectOption
                  formInputClass="form-input position-relative"
                  label="Shipping country"
                  name="country"
                  errorText="Enter the country"
                  isRequired={true}
                  imageRequired={true}
                  selectedText="Select Countries"
                  options={["Uk", "LT"]}
                  onChange={this.onInputChange}
                />
              </div>
              {/* <div className="font-weight-bold text-uppercase mb-3">
                Shipping country <sup>*</sup>
              </div>
              <SingleSelect
                placeholder="Select Countries"
                options={countries}
                name="name"
                id="shippingCountry"
                onChange={this.onChange}
              /> */}
              <div className="x-sm-text-fw4-g pt-3">
                The country you’re shipping from
              </div>
            </div>
          </div>

          <div className="col-12 col-md-4 mb-4">
            <div className="form-group">
              <div className="single-group label-mb">
                <HorizontalSelectOption
                  formInputClass="form-input position-relative"
                  label="Processing time"
                  name="proTime"
                  errorText="Processing time"
                  isRequired={true}
                  imageRequired={false}
                  selectedText="1-2 Business days"
                  options={["1-2 Business days", "1-2 Business days"]}
                  onChange={this.onInputChange}
                />
              </div>
              {/* <div className="font-weight-bold text-uppercase mb-3">
                Processing time <sup>*</sup>
              </div>
              <SingleSelect
                placeholder="Select Proccessing time"
                options={this.state.days}
                name="day"
                id="processingTime"
                onChange={this.onChange}
                additionalData="Business days"
              /> */}
              <div className="x-sm-text-fw4-g pt-3">
                Once purchased, how long does it take you to process an item?
              </div>
            </div>
          </div>

          <div className="col-12 col-md-4 mb-4">
            <div className="form-group">
              <div className="single-group label-mb">
                <HorizontalSelectOption
                  formInputClass="form-input position-relative"
                  label="Shipping carrier"
                  name="proTime"
                  errorText="Enter Shipping carrier"
                  isRequired={true}
                  imageRequired={false}
                  selectedText="Lietuvos paštas"
                  options={["Lietuvos paštas", "Lietuvos paštas"]}
                  onChange={this.onInputChange}
                />
              </div>
              {/* <div className="font-weight-bold text-uppercase mb-3">
                Shipping carrier <sup>*</sup>
              </div>
              <SingleSelect
                placeholder="Select shipping carrier"
                options={carriers}
                name="name"
                id="shippingCarrier"
                onChange={this.onChange}
              /> */}
            </div>
          </div>

          <div className="col-12 col-md-4 mb-4">
            <div className="form-group">
              <div className="font-weight-bold text-uppercase mb-2 pb-1">
                Delivery time
              </div>
              <div className="row">
                <div className="col-12 col-md-6">
                  <div className="single-group label-mb hide-label">
                    <HorizontalSelectOption
                      formInputClass="form-input position-relative"
                      label=""
                      name="DeFromTimeFrom"
                      errorText=""
                      isRequired={false}
                      imageRequired={false}
                      selectedText="From"
                      options={["1 Business days", "1 Business days"]}
                      onChange={this.onInputChange}
                    />
                  </div>
                  {/* <SingleSelect
                    placeholder="Select Delivery time"
                    options={this.state.days}
                    name="day"
                    id="deliveryTimeFrom"
                    onChange={this.onChange}
                    additionalData="Business days"
                  /> */}
                </div>
                <div className="col-12 col-md-6">
                  <div className="single-group label-mb hide-label">
                    <HorizontalSelectOption
                      formInputClass="form-input position-relative"
                      label=""
                      name="DeFromTimeTo"
                      errorText=""
                      isRequired={false}
                      imageRequired={false}
                      selectedText="To"
                      options={["1 Business days", "1 Business days"]}
                      onChange={this.onInputChange}
                    />
                  </div>
                  {/* <SingleSelect
                    placeholder="Select Delivery time "
                    options={this.state.days}
                    name="day"
                    id="deliveryTimeTo"
                    onChange={this.onChange}
                    additionalData="Business days"
                  /> */}
                </div>
              </div>
              <div className="x-sm-text-fw4-g mt-3">Business days</div>
            </div>
          </div>
          <div className="col-12 col-md-4 mb-4">
            <div className="row">
              <div className="col-12 col-md-6">
                <div className="single-group label-mb">
                  <HorizontalInput
                    formInputClass="form-input position-relative"
                    label="Shipping price"
                    type="number"
                    placeholder="Your Shipping price"
                    name="shipingPrice"
                    errorText="Please enter your Shipping price"
                    isRequired={true}
                    onChange={this.handleInputChange}
                  />
                </div>
                {/* <div className="font-weight-bold text-uppercase mb-3">
                Shipping price <sup>*</sup>
              </div>
              <InputWithoutLabel
                placeholder="Enter shipping price"
                type="number"
                name="shippingPrice"
                id="shippingPrice"
                onChange={this.onChange}
              /> */}
              </div>
              <div className="col-12 col-md-6">
                <div className="single-group label-mb">
                  <HorizontalInput
                    formInputClass="form-input position-relative"
                    label=" Additional item"
                    type="number"
                    placeholder="Your Additional item"
                    name="shippingAdditionalItem"
                    id="shippingAdditionalItem"
                    errorText=""
                    isRequired={false}
                    onChange={this.handleInputChange}
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="col-12 col-md-4 mb-4 d-flex align-items-center">
            <label className="ctm-container d-inline-block mb-4">
              Free shipping
              <input type="checkbox" name="selectAll" />
              <span className="checkmark" />
            </label>
          </div>
          <div className="col-12 col-md-4 mb-4">
            <div className="single-group label-mb">
              <HorizontalSelectOption
                formInputClass="form-input position-relative"
                label="International shipping"
                name="templete"
                errorText=""
                isRequired={false}
                imageRequired={false}
                selectedText="Choose international shipping template"
                options={[
                  "International shipping",
                  "No International shipping"
                ]}
                onChange={this.onInputChange}
              />
            </div>
          </div>
          <div className="col-12 col-md-4 mb-4">
            <div className="single-group label-mb">
              <HorizontalInput
                formInputClass="form-input position-relative form-input-append"
                label="Template name"
                type="text"
                placeholder="Enter Template name"
                name="templateName"
                errorText=""
                appendText="Save"
                isRequired={false}
                onChange={this.handleInputChange}
              />
            </div>
          </div>
        </div>
        <div className="row mb-4">
          <div className="col-12 col-md-7">
            <div className="md-title text-uppercase mb-1">
              International shipping
            </div>
          </div>
          <div className="col-12 col-md-1"></div>
          <div className="col-12 col-md-4">
            <div className="single-group hide-label w-100">
              <HorizontalSelectOption
                formInputClass="form-input position-relative"
                label=""
                name="InterShiping"
                errorText=""
                isRequired={false}
                imageRequired={false}
                selectedText="Choose international shipping template"
                options={["Template1", "Template2"]}
                onChange={this.onInputChange}
              />
            </div>
          </div>
        </div>
        <div className="row my-5">
          <div className="col-12 col-md-4">
            <div className="form-group">
              <div className="single-group label-mb">
                <HorizontalSelectOption
                  formInputClass="form-input position-relative"
                  label=" Ship to"
                  name="shipTo"
                  errorText=""
                  isRequired={true}
                  imageRequired={false}
                  selectedText="Custom location"
                  options={["Custom location", "Custom location"]}
                  onChange={this.onInputChange}
                />
              </div>
              {/* <div className="font-weight-bold text-uppercase mb-3">
                Ship to <sup>*</sup>
              </div>
              <SingleSelect
                placeholder="Select a Country"
                options={countries}
                name="name"
                id="country"
                onChange={this.onChange}
              /> */}
              <div className="form-group row mt-3">
                <div className="col-12 col-md-6 mb-3">
                  <label
                    className="ctm-container radio-ctm font-weight-5"
                    onClick={() =>
                      this.props.selectCustomCountry("Europe + UK")
                    }
                  >
                    Europe + UK
                    <input type="radio" name="location" />
                    <span className="checkmark" />
                  </label>
                </div>
                <div className="col-12 col-md-6 mb-3">
                  <label
                    className="ctm-container radio-ctm font-weight-5"
                    onClick={() => this.props.selectCustomCountry("France")}
                  >
                    France
                    <input type="radio" name="location" />
                    <span className="checkmark" />
                  </label>
                </div>
                <div className="col-12 col-md-6 mb-3">
                  <label
                    className="ctm-container radio-ctm font-weight-5"
                    onClick={() =>
                      this.props.selectCustomCountry("United Kingdom")
                    }
                  >
                    United Kingdom
                    <input type="radio" name="location" />
                    <span className="checkmark" />
                  </label>
                </div>
                <div className="col-12 col-md-6 mb-3">
                  <label
                    className="ctm-container radio-ctm font-weight-5"
                    onClick={() => this.props.selectCustomCountry("Italy")}
                  >
                    Italy
                    <input type="radio" name="location" />
                    <span className="checkmark" />
                  </label>
                </div>
                <div className="col-12 col-md-6 mb-3">
                  <label
                    className="ctm-container radio-ctm font-weight-5"
                    onClick={() => this.props.selectCustomCountry("Germany")}
                  >
                    Germany
                    <input type="radio" name="location" />
                    <span className="checkmark" />
                  </label>
                </div>
                <div className="col-12 col-md-6 mb-3">
                  <label
                    className="ctm-container radio-ctm font-weight-5"
                    onClick={() => this.props.selectCustomCountry("USA")}
                  >
                    USA
                    <input type="radio" name="location" />
                    <span className="checkmark" />
                  </label>
                </div>
                <div className="col-12 col-md-6 mb-3">
                  <label
                    className="ctm-container radio-ctm font-weight-5"
                    onClick={() => this.props.selectCustomCountry("Spain")}
                  >
                    Spain
                    <input type="radio" name="location" />
                    <span className="checkmark" />
                  </label>
                </div>
                <div className="col-12 mt-5 p-4 text-right">
                  <button
                    className="btn border-btn border-0 p-0 mr-3"
                    onClick={this.props.addShipToData}
                  >
                    &times; Delele country
                  </button>
                  <button
                    className="btn border-btn border-0 p-0 mr-4"
                    onClick={this.props.addShipToData}
                  >
                    + Add additional country
                  </button>
                </div>
                {/* <div className="col-12 pt-2">
                  <span className="x-sm-text-fw4-g cursor-pointer">
                    Show more
                  </span>
                </div> */}
              </div>
            </div>
          </div>
          <div className="col-12 col-md-8">
            {/* <div className="form-group mb-5 pb-4">
              <div className=" mb-3">
                <span className="font-weight-bold text-uppercase">
                  {" "}
                  Services Calculate shipping
                </span>
                <span className="sm-text-fw5 float-right cursor-pointer">
                  Calculate shipping
                </span>
              </div>
              <select className="form-control custom-input" name="brand">
                <option>--</option>
                <option value="United Kingdom">Custom location</option>
              </select>
            </div> */}
            <div className="form-group row">
              <div className="col-12 col-md-6  mb-5">
                <div className="single-group label-mb">
                  <HorizontalInput
                    formInputClass="form-input position-relative"
                    label="One item"
                    type="number"
                    placeholder="Your One item"
                    name="item"
                    errorText="Please enter your One item"
                    isRequired={true}
                    onChange={this.handleInputChange}
                  />
                </div>
                {/* <div className="font-weight-bold text-uppercase mb-3">
                  One item <sup>*</sup>
                </div>
                <InputWithoutLabel
                  placeholder="Enter price (Euro)"
                  type="number"
                  name="shipToOneItemPrice"
                  onChange={this.onChange}
                /> */}
              </div>
              <div className="col-12 col-md-6 mb-5">
                <div className="single-group label-mb">
                  <HorizontalInput
                    formInputClass="form-input position-relative"
                    label=" Additional item"
                    type="number"
                    placeholder="Your Additional item"
                    name="shippingAdditionalItem"
                    id="shippingAdditionalItem"
                    errorText=""
                    isRequired={false}
                    onChange={this.handleInputChange}
                  />
                </div>
                {/* <div className="font-weight-bold text-uppercase mb-3">
                  Additional item <sup>*</sup>
                </div>
                <InputWithoutLabel
                  placeholder="Enter price (Euro)"
                  type="number"
                  name="shiptToAdditionalItemPrice"
                  onChange={this.onChange}
                /> */}
              </div>
              <div className="col-12 col-md-6 mb-5">
                <div className="form-group">
                  <div className="font-weight-bold text-uppercase mb-2 pb-1">
                    Delivery time
                  </div>
                  <div className="row">
                    <div className="col-12 col-md-6">
                      <div className="single-group label-mb hide-label">
                        <HorizontalSelectOption
                          formInputClass="form-input position-relative"
                          label=""
                          name="DeFromTimeFrom"
                          errorText=""
                          isRequired={false}
                          imageRequired={false}
                          selectedText="From"
                          options={["1 Business days", "1 Business days"]}
                          onChange={this.onInputChange}
                        />
                      </div>
                    </div>
                    <div className="col-12 col-md-6">
                      <div className="single-group label-mb hide-label">
                        <HorizontalSelectOption
                          formInputClass="form-input position-relative"
                          label=""
                          name="DeFromTimeTo"
                          errorText=""
                          isRequired={false}
                          imageRequired={false}
                          selectedText="To"
                          options={["1 Business days", "1 Business days"]}
                          onChange={this.onInputChange}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="x-sm-text-fw4-g mt-3">Business days</div>
                </div>
              </div>
              <div className="col-12 col-md-6 mb-5">
                <div className="form-group">
                  <div className="single-group label-mb">
                    <HorizontalSelectOption
                      formInputClass="form-input position-relative"
                      label="Shipping carrier"
                      name="proTime"
                      errorText="Enter Shipping carrier"
                      isRequired={true}
                      imageRequired={false}
                      selectedText="Lietuvos paštas"
                      options={["Lietuvos paštas", "Lietuvos paštas"]}
                      onChange={this.onInputChange}
                    />
                  </div>
                </div>
              </div>
              <div className="col-12 col-md-6 mb-5">
                <div className="single-group label-mb">
                  <HorizontalInput
                    formInputClass="form-input position-relative form-input-append"
                    label="Template name"
                    type="text"
                    placeholder="Enter Template name"
                    name="templateName"
                    errorText=""
                    appendText="Save"
                    isRequired={false}
                    onChange={this.handleInputChange}
                  />
                </div>
              </div>
            </div>
          </div>

          {/* <div className="col-12 mt-4 mb-5">
            <button
              className="btn border-btn mr-3 btn-w-115"
              onClick={this.props.addShipToData}
            >
              Add
            </button>
          </div> */}

          {this.props.shipToData &&
            this.props.shipToData.map((data, index) => {
              return (
                <div className="col-12 col-md-12 mb-2 " key={index}>
                  <div className="row border border-dark">
                    <div className="col-4 col-md-4">
                      <div className="font-weight-bold text-uppercase mb-3">
                        Country
                      </div>
                      <h5>{data.country}</h5>
                    </div>
                    <div className="col-4 col-md-4">
                      <div className="font-weight-bold text-uppercase mb-3">
                        One item price
                      </div>
                      <h5>{data.shipToOneItemPrice}</h5>
                    </div>
                    <div className="col-4 col-md-4">
                      <div className="font-weight-bold text-uppercase mb-3">
                        Additional item price
                      </div>
                      <h5>{data.shiptToAdditionalItemPrice}</h5>
                    </div>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    );
  }
}

export default ShippingOptions;
