import React from "react";
const InventoryPricing = () => (
  <div className="mb-5">
    <div className="md-title text-uppercase mb-4">INVENTORY AND PRICING</div>
    <div className="font-weight-bold text-uppercase mb-4">
      Price and quantity 
      {/* for mobile */}
      <button className="btn border-btn btn-w-115 d-block d-md-none float-right">
        {" "}
        Edit
      </button>
      {/* for mobile */}
    </div>
    <div className="row">
      <div className="col-12 col-md-6 sm-text-fw6 text-uppercase mb-3">
        <div className="row">
          <div className="col-6">Number of variations:</div>
          <div className="col-6">16</div>
        </div>
      </div>
      <div className="col-12 col-md-6 sm-text-fw6 text-uppercase mb-3">
        <div className="row">
          <div className="col-6">Price range:</div>
          <div className="col-6">0.00 € - 80.99 €</div>
        </div>
      </div>
      <div className="col-12 col-md-6 sm-text-fw6 text-uppercase mb-3">
        <div className="row">
          <div className="col-6">Size:</div>
          <div className="col-6">S, M, L, XL</div>
        </div>
      </div>
      <div className="col-12 col-md-6 sm-text-fw6 text-uppercase mb-3">
        <div className="row">
          <div className="col-6">Quantity:</div>
          <div className="col-6">16</div>
        </div>
      </div>
      <div className="col-12 col-md-6 sm-text-fw6 text-uppercase mb-3">
        <div className="row">
          <div className="col-6">Waist Size:</div>
          <div className="col-6">22 in, 23 in, 32 in, 31 in</div>
        </div>
      </div>
    </div>
    {/* for desktop */}
    <div className="text-right mb-3 d-none d-md-block">
      <button className="btn border-btn btn-w-115"> Edit</button>
    </div>
    {/* for desktop */}
  </div>
);

export default InventoryPricing;
