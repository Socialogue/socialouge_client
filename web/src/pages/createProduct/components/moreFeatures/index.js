import React, { Component } from "react";
import CustomSelect from "../../../../components/CustomSelect";
import InputWithoutLabel from "../../../../components/InputWithoutLabel";
import TagInput from "../../../../components/TagInput";
import HorizontalSelectOption from "../../../userSettings/components/horizontalSelectOption";
import Close from "./../../../../images/icons/close";
class MoreFeatures extends Component {
  onChange = (value, name) => {
    this.props.addMoreFeature(value, name);
  };

  onInputChange = (name, value) => {
    this.setState({
      [name]: value
    });
  };
  render() {
    const { materials, seasons } = this.props;
    return (
      <div className="mb-5">
        <div className="md-title text-uppercase mb-4">More features</div>
        <div className="row">
          <div className="col-12 col-md-6 pr-5">
            <div className="form-group mb-4 pb-2">
              <div className="font-weight-bold text-uppercase mb-1">
                Product brand
              </div>
              <div className="x-sm-text-fw4-g">
                All the Lorem Ipsum generators on the Internet tend to repeat
                predefined chunks as necessary, making this the first
              </div>
            </div>
            <div className="hide-label single-group">
              <HorizontalSelectOption
                formInputClass="form-input position-relative"
                label=""
                name="templete"
                errorText=""
                isRequired={false}
                imageRequired={false}
                selectedText="Select brand"
                options={["Bershka", "Bershka"]}
                onChange={this.onInputChange}
              />
            </div>
            <div className="form-group mb-4">
              <label className="ctm-container">
                No brand
                <input type="checkbox" name="brand" />
                <span className="checkmark" />
              </label>
            </div>

            <div className="form-group  mb-4 pb-2">
              <div className="form-input position-relative ml-4">
                <span className="prepend-input">
                  <label className="ctm-container">
                    {" "}
                    <input type="checkbox" name="brand" />
                    <span className="checkmark" />
                  </label>
                </span>
                <input
                  type="text"
                  className="form-control custom-input"
                  placeholder="Enter brand"
                  name="brand_name"
                />
              </div>
            </div>
          </div>
          <div className="col-12 col-md-6 pl-5">
            <div className="form-group">
              <div className="font-weight-bold text-uppercase mb-1">
                Material
              </div>
              <div className="x-sm-text-fw4-g  mb-4 pb-2">
                What is your product made out of? People can be alergic to
                certain fabrics.
              </div>
            </div>
            <div className="hide-label single-group">
              <HorizontalSelectOption
                formInputClass="form-input position-relative"
                label=""
                name="material"
                errorText=""
                isRequired={false}
                imageRequired={false}
                selectedText="Material"
                options={["Material", "Material"]}
                onChange={this.onInputChange}
              />
            </div>

            {/* <CustomSelect
              placeholder="Select materials"
              options={materials}
              name="name"
              id="materials"
              onChange={this.onChange}
            /> */}
          </div>

          <div className="col-12 col-md-6 pr-5">
            <div className="form-group">
              <div className="font-weight-bold text-uppercase mb-1">Season</div>
              <div className="x-sm-text-fw4-g mb-4 pb-2">
                Is your product suited for all seasons? Which season is it most
                suited for?
              </div>
            </div>
            <div className="hide-label single-group">
              <HorizontalSelectOption
                formInputClass="form-input position-relative"
                label=""
                name="season"
                errorText=""
                isRequired={false}
                imageRequired={false}
                selectedText="Season"
                options={["Season1", "Season2"]}
                onChange={this.onInputChange}
              />
            </div>
            {/* <CustomSelect
              placeholder="Select seasons"
              options={seasons}
              name="name"
              id="seasons"
              onChange={this.onChange}
            /> */}
          </div>
          <div className="col-12 col-md-6 pl-5">
            <div className="form-group">
              <div className="font-weight-bold text-uppercase mb-1">
                Tags (Optional)
              </div>
              <div className="x-sm-text-fw4-g  mb-4 pb-2">
                what words might someone use to search for your listings? Use
                all 13 tags to get found.
              </div>
            </div>
            
            <div className="hide-label single-group">
              <HorizontalSelectOption
                formInputClass="form-input position-relative"
                label=""
                name="Tags"
                errorText=""
                isRequired={false}
                imageRequired={false}
                selectedText="Tags"
                options={["Tags", "Tags"]}
                onChange={this.onInputChange}
              />
            </div>
            {/* <TagInput
              placeholder="Enter tags"
              type="text"
              name="name"
              onChange={this.onChange}
              id="tags"
            /> */}
            {/* <InputWithoutLabel
              placeholder="Enter tags"
              type="text"
              name="tags"
              onChange={this.onChange}
            /> */}
          </div>
        </div>
      </div>
    );
  }
}

export default MoreFeatures;
