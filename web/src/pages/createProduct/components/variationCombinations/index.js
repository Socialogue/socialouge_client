import React from "react";
import Down from "./../../../../images/icons/down";
import Close from "./../../../../images/icons/close";
import Image from "./../../../../images/eco-img.webp";
import CloseSm from "../../../../images/icons/closeSm";

class VariationCombinations extends React.Component {
  render() {
    const { variationCombinations } = this.props;

    return (
      <div className="mb-5">
        <div className="md-title text-uppercase mb-3">VARIATIONS Combinations</div>
        <div className="seller-table create-p-list item-list">
          <table class="table">
            <thead>
              <tr>
                <th className="sm-text-fw6-g text-uppercase" />
                <th className="sm-text-fw6-g text-uppercase photo-td">Photo</th>
                <th className="sm-text-fw6-g text-uppercase">Size</th>
                <th className="sm-text-fw6-g text-uppercase">color</th>
                <th className="sm-text-fw6-g text-uppercase">SKU</th>
                <th className="sm-text-fw6-g text-uppercase">Quantity</th>
                <th className="sm-text-fw6-g text-uppercase">Price</th>
              </tr>
            </thead>
            <tbody>
              <tr className="">
                <td className="sm-text-fw6 text-uppercase px-0">
                  <CloseSm />
                </td>
                <td className="sm-text-fw6 text-uppercase td-border f-td">
                  <img src={Image} width="38" height="48" alt="" />
                </td>
                <td className="sm-text-fw6 text-uppercase td-border">
                  36 / 8 / S
                </td>
                <td className="sm-text-fw6 text-uppercase td-border">Black</td>
                <td className="sm-text-fw6 text-uppercase td-border">
                  A1B2021
                </td>
                <td className="sm-text-fw6 td-border">
                  <div className="">
                  <span className="v-control">
                    <span className="cn-v minus-v">-</span>
                    <span className="td-v m-0">
                      <input className="ct-mp" type="text" value="50" />
                    </span>
                    <span className="cn-v plus-v">+</span>
                  </span>
                  </div>
                  <span className="all-pq">
                    <label className="ctm-container d-inline-block sm-text-fw4-g">
                      Same Qty for all
                      <input type="checkbox" name="selectTr[]" />
                      <span className="checkmark" />
                    </label>
                  </span>
                </td>
                <td className="sm-text-fw6 td-border l-td">
                  <span className="all-pq">
                    <input placeholder="Enter price.." />
                    <label className="ctm-container d-inline-block sm-text-fw4-g">
                    Same price for all
                      <input type="checkbox" name="selectTr[]" />
                      <span className="checkmark" />
                    </label>
                  </span>
                </td>
              </tr>
              <tr className="">
                <td className="sm-text-fw6 text-uppercase px-0">
                  <CloseSm />
                </td>
                <td className="sm-text-fw6 text-uppercase td-border f-td">
                  <img src={Image} width="38" height="48" alt="" />
                </td>
                <td className="sm-text-fw6 text-uppercase td-border">
                  36 / 8 / S
                </td>
                <td className="sm-text-fw6 text-uppercase td-border">Black</td>
                <td className="sm-text-fw6 text-uppercase td-border">
                  A1B2021
                </td>
                <td className="sm-text-fw6 text-uppercase td-border">
                  <span className="v-control">
                    <span className="cn-v minus-v">-</span>
                    <span className="td-v m-0">
                      <input className="ct-mp" type="text" value="50" />
                    </span>
                    <span className="cn-v plus-v">+</span>
                  </span>
                </td>
                <td className="sm-text-fw6 text-uppercase td-border l-td">
                  <span className="td-v">
                    <span className="color-orange">22.00</span> €
                  </span>
                </td>
              </tr>
              <tr className="">
                <td className="sm-text-fw6 text-uppercase px-0">
                  <CloseSm />
                </td>
                <td className="sm-text-fw6 text-uppercase td-border f-td">
                  <img src={Image} width="38" height="48" alt="" />
                </td>
                <td className="sm-text-fw6 text-uppercase td-border">
                  36 / 8 / S
                </td>
                <td className="sm-text-fw6 text-uppercase td-border">Black</td>
                <td className="sm-text-fw6 text-uppercase td-border">
                  A1B2021
                </td>
                <td className="sm-text-fw6 text-uppercase td-border">
                  <span className="v-control">
                    <span className="cn-v minus-v">-</span>
                    <span className="td-v m-0">
                      <input className="ct-mp" type="text" value="50" />
                    </span>
                    <span className="cn-v plus-v">+</span>
                  </span>
                </td>
                <td className="sm-text-fw6 text-uppercase td-border l-td">
                  <span className="td-v">
                    <span className="color-orange">22.00</span> €
                  </span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>


        {/* <div className="item-list d-none">
          <table className="table table-bordered mb-0">
            <thead>
              <tr>
                <th
                  className="sm-text-fw6 text-uppercase"
                  scope="col"
                  colSpan={5}
                />
                <th className="sm-text-fw6 text-uppercase" scope="col">
                  Quantity
                  <Down />
                </th>
                <th className="sm-text-fw6 text-uppercase" scope="col">
                  Price
                  <Down />
                </th>
              </tr>
              <tr>
                <th scope="col" />
                <th className="sm-text-fw6 text-uppercase" scope="col">
                  Photo
                </th>
                <th scope="col">
                  Size
                  <Down />
                </th>
                <th className="sm-text-fw6 text-uppercase" scope="col">
                  Color
                  <Down />
                </th>
                <th className="sm-text-fw6 text-uppercase" scope="col">
                  SKU
                  <Down />
                </th>
                <td scope="col" className="p-0" style={{ borderBottom: "0" }}>
                  <input
                    type="text"
                    className="form-control table-input"
                    placeholder="Same qty"
                  />
                  <span className="pm-q">
                    <span className="p-q">+</span>
                    <span className="m-q">-</span>
                  </span>
                </td>
                <td scope="col" className="p-0" style={{ borderBottom: "0" }}>
                  <input
                    type="text"
                    className="form-control table-input"
                    placeholder="Enter price…"
                  />{" "}
                  <span className="td-right-txt">€</span>
                </td>
              </tr>
            </thead>
            <tbody>
              {variationCombinations.map(item => {
                return (
                  <tr className="">
                    <th className="one-z">
                      <span className="close-sc scale-hover cursor-pointer">
                        <Close />
                      </span>
                    </th>
                    <td className="one-td">
                      <img className="cover" src={Image} />
                    </td>
                    <td className="two-td">
                      36 / 8 / S{" "}
                      <span className="td-right-txt">
                        <i className="far fa-ellipsis-h" />
                      </span>
                    </td>
                    <td className="three-td">
                      {item.color ? item.color.name : ""}
                    </td>
                    <td className="three-td p-0">
                      <input
                        type="text"
                        className="form-control table-input"
                        placeholder=""
                      />
                      <span className="td-right-txt">
                        <i className="far fa-ellipsis-h" />
                      </span>
                    </td>
                    <td className="four-td p-0">
                      <input
                        type="text"
                        className="form-control table-input"
                        placeholder="price"
                      />
                      <span className="pm-q">
                        <span className="p-q">+</span>
                        <span className="m-q">-</span>
                      </span>
                    </td>
                    <td className="five-td p-0">
                      <input
                        type="text"
                        className="form-control table-input"
                        placeholder="2000"
                      />
                      <span className="td-right-txt">€</span>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div> */}
        <div className="text-right mt-5 pb-5">
      <button className="btn border-btn mr-3 btn-w-115">Save</button>
      <button className="btn border-btn border-btn-gray">
        Save and preview
      </button>
    </div>
      </div>
    );
  }
}

export default VariationCombinations;
