import React from "react";
import { Component } from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css"; // ES6

class ItemDescription extends Component {

  handleChange = html => {
    this.props.onChange(html);
  };

  modules = {
    toolbar: [
      [{ header: "1" }, { header: "2" }, { font: [] }],
      [{ size: [] }],
      ["bold", "italic", "underline", "strike", "blockquote"],
      [
        { list: "ordered" },
        { list: "bullet" },
        { indent: "-1" },
        { indent: "+1" }
      ],
      ["link", "image", "video"],
      ["clean"]
    ],
    clipboard: {
      matchVisual: false
    }
  };
  formats = [
    "header",
    "font",
    "size",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "indent",
    "link",
    "image",
    "video"
  ];

  render() {
    return (
      <div>
        <div className="mb-5">
          <div className="md-title text-uppercase mb-4">Item description</div>
          <div className="form-group">
            <ReactQuill
              value={this.props.description}
              onChange={this.handleChange}
              // theme={this.state.theme}
              modules={this.modules}
              formats={this.formats}
              placeholder="Please enter your product description"
            />
            {/* <textarea className="form-control item-des-form-control">
              There are many variations of passages of Lorem Ipsum available,
            </textarea> */}
          </div>
          <div className="color-primary-gray">
            At least <span className="color-black font-weight-5">500</span>{" "}
            characters
          </div>
        </div>
      </div>
    );
  }
}

export default ItemDescription;
