import React, { Component } from "react";
import CustomSelect from "../../../components/CustomSelect";
import TagInput from "../../../components/TagInput";
import FormGroup from "./FormGroup";

class MoreFeatures extends Component {

  state = {
    materials: [],
    seasons: [],
    tags: []
  }

  getState = () => {
    return this.state;
  }

  onChange = (value, name) => {
    this.setState({[name]: value});
  };

  render() {
    const { materials, seasons } = this.props;
    return (
      <div className="mb-5">
        <div className="md-title text-uppercase mb-4">More features</div>
        <div className="row">
          {/*<div className="col-12 col-md-6 pr-3 pr-md-5">
            <FormGroup
              title='Product brand'
              desc='All the Lorem Ipsum generators on the Internet tend to repeat
                predefined chunks as necessary, making this the first'
              inputComponent={<CustomSelect
                placeholder="Select seasons"
                options={seasons}
                name="name"
                id="seasons"
                onChange={this.onChange}
              />}
            />
          </div>*/}

          <div className="col-12 col-md-6 pl-3 pl-md-5">
            <FormGroup
              title='Material'
              desc='What is your product made out of? People can be alergic to
                certain fabrics.'
              inputComponent={<CustomSelect
                placeholder="Select materials"
                options={materials}
                name="name"
                id="materials"
                onChange={this.onChange}
              />}
            />
          </div>

          <div className="col-12 col-md-6 pr-3 pr-md-5">
            <FormGroup
              title='Season'
              desc='Is your product suited for all seasons? Which season is it most
                suited for?'
              inputComponent={<CustomSelect
                placeholder="Select seasons"
                options={seasons}
                name="name"
                id="seasons"
                onChange={this.onChange}
              />}
            />
          </div>
          <div className="col-12 col-md-6 pl-3 pl-md-5">
            <FormGroup
              title='Tags (optional)'
              desc='what words might someone use to search for your listings? Use
                all 13 tags to get found.'
              inputComponent={<TagInput
                placeholder="Enter tags"
                type="text"
                name="name"
                onChange={this.onChange}
                id="tags"
              />}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default MoreFeatures;
