import React from "react";

export default (props) => {
  return (
    <React.Fragment>
      <div className="form-group">
        <div className="font-weight-bold text-uppercase mb-1">
          {props.title}
        </div>
        <div className="x-sm-text-fw4-g  mb-4 pb-2">
          {props.desc}
        </div>
      </div>

      {props.inputComponent}
    </React.Fragment>
  )
}
