import {days} from "core/utils/dummyDate";
import React, {Component} from "react";
import {emptyIntlShippingOption} from "../initialState";
import SingleBlock from "./SingleBlock";

class InternationalShipping extends Component {
  state = {
    data: [emptyIntlShippingOption],
  };

  getState = () => {
    return this.state;
  }

  componentDidMount = () => {
    const daysData = days();
    this.setState({ days: daysData });
  };

  onChange = (index, stateObj) => {
    let {data} = this.state;
    data[index] = stateObj;
    this.setState({data});
  };

  addOption = () => {
    const {data} = this.state;
    data.push(emptyIntlShippingOption);
    this.setState({
      data
    });
  };

  deleteOption = (index) => {
    if (index === 0) return;

    const {data} = this.state;
    data.splice(index, 1);
    this.setState({
      data
    });
  };

  render() {
    const { data } = this.state;

    return (
      <React.Fragment>
        {data.map((item, index) => {
          return (
            <SingleBlock
              item={item}
              index={index}
              onClickAdd={this.addOption}
              onClickDelete={this.deleteOption}
              onChangeInput={this.onChange}
              carriers={this.props.carriers}
            />
          )
        })
        }
      </React.Fragment>
    );
  }
}

export default InternationalShipping;
