import React, {Component} from "react";
import HorizontalInput from "../../userSettings/components/horizontalInput";
import HorizontalSelectOption from "../../userSettings/components/horizontalSelectOption";
import HorizontalSingleSelectInput from "web/src/pages/userSettings/components/HorizontalSingleSelectInput";
import HorizontalMultiSelectInput from "web/src/pages/userSettings/components/HorizontalMultiSelectInput";
import {allGroupCodes, countryGroupEuUk, countryList} from "web/src/utils/countryList";
import CountryGroup from "./CountryGroup";
import {getThirtyBusinessDays} from "../../../utils/misc";

class SingleBlock extends Component {

  state = {
    shipToCountries: [],
    oneItemCost: '',
    additionalItemCost: '',
    deliveryTimeFrom: '',
    deliveryTimeTo: '',
    shippingCarrierId: '',
    templateName: ''
  };

  onChange = (value, name) => {
    this.props.addShippingData(value, name);
  };

  onInputChange = (name, value) => {
    this.setState({
      [name]: value
    }, () => {
      this.props.onChangeInput(this.props.index, this.state);
    });
  };

  onChangeCountryGroup = (selectedGroups) => {
    // deselect all countries which belongs to a group
    const removed = this.state.shipToCountries.filter( i => !allGroupCodes.includes(i));
    if (selectedGroups.length) {
      selectedGroups.map( i => {
        if (i === 'EUUK') {
          countryGroupEuUk.map( j => removed.push(j));
        } else {
          removed.push(i);
        }
      })
    }
    this.setState({
      shipToCountries: removed
    });
  };

  render() {
    const { item, index } = this.props;
    const {shipToCountries} = this.state;

    return (
      <React.Fragment>
        <div className="row mb-4">
          <div className="col-12 col-md-7">
            <div className="md-title text-uppercase mb-1">
              International shipping ({index+1})
            </div>
          </div>
          <div className="col-12 col-md-1"></div>
          <div className="col-12 col-md-4 d-none d-md-block">
            <div className="single-group hide-label w-100">
              <HorizontalSelectOption
                formInputClass="form-input position-relative"
                label=""
                name="InterShiping"
                errorText=""
                isRequired={false}
                imageRequired={false}
                selectedText="Choose international shipping template"
                options={["Template1", "Template2"]}
                onChange={this.onInputChange}
              />
            </div>
          </div>
        </div>
        <div className="row my-5">
          <div className="col-12 col-md-4">
            <div className="form-group">
              <div className="single-group label-mb">
                <HorizontalMultiSelectInput
                  formInputClass="form-input position-relative"
                  label=" Ship to"
                  name="shipToCountries"
                  errorText=""
                  isRequired={true}
                  imageRequired={false}
                  selectedText="Select country"
                  options={countryList}
                  values={shipToCountries}
                  labelKey='name'
                  valueKey='code'
                  onChange={this.onInputChange}
                />
              </div>
              {/* <div className="font-weight-bold text-uppercase mb-3">
                Ship to <sup>*</sup>
              </div>
              <SingleSelect
                placeholder="Select a Country"
                options={countries}
                name="name"
                id="country"
                onChange={this.onChange}
              /> */}
              <div className="form-group row mt-3">
                <CountryGroup
                  onChange={this.onChangeCountryGroup}
                  onClickAdd={this.props.onClickAdd}
                  onClickDelete={this.props.onClickDelete}
                />
              </div>
            </div>
          </div>
          <div className="col-12 col-md-8">
            <div className="form-group row">
              <div className="col-12 col-md-6  mb-5">
                <div className="single-group label-mb">
                  <HorizontalInput
                    formInputClass="form-input position-relative"
                    label="One item"
                    type="number"
                    placeholder="Your One item"
                    name="oneItemCost"
                    errorText="Please enter your One item"
                    isRequired={true}
                    onChange={this.onInputChange}
                  />
                </div>
              </div>
              <div className="col-12 col-md-6 mb-5">
                <div className="single-group label-mb">
                  <HorizontalInput
                    formInputClass="form-input position-relative"
                    label=" Additional item"
                    type="number"
                    placeholder="Your Additional item"
                    name="additionalItemCost"
                    id="shippingAdditionalItem"
                    errorText=""
                    isRequired={false}
                    onChange={this.onInputChange}
                  />
                </div>
              </div>
              <div className="col-12 col-md-6 mb-5">
                <div className="form-group">
                  <div className="font-weight-bold text-uppercase mb-2 pb-1">
                    Delivery time
                  </div>
                  <div className="row">
                    <div className="col-6">
                      <div className="single-group label-mb hide-label">
                        <HorizontalSingleSelectInput
                          formInputClass="form-input position-relative"
                          nameKey='_id'
                          labelkey='name'
                          name="deliveryTimeFrom"
                          errorText="Processing time"
                          isRequired={true}
                          imageRequired={false}
                          placeholder="Select"
                          options={getThirtyBusinessDays()}
                          onChange={this.onInputChange}
                        />
                      </div>
                    </div>
                    <div className="col-6">
                      <div className="single-group label-mb hide-label">
                        <HorizontalSingleSelectInput
                          formInputClass="form-input position-relative"
                          nameKey='_id'
                          labelkey='name'
                          name="deliveryTimeTo"
                          errorText="Processing time"
                          isRequired={true}
                          imageRequired={false}
                          placeholder="Select"
                          options={getThirtyBusinessDays()}
                          onChange={this.onInputChange}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="x-sm-text-fw4-g mt-3">Business days</div>
                </div>
              </div>
              <div className="col-12 col-md-6 mb-2 mb-md-5">
                <div className="form-group">
                  <div className="single-group label-mb">
                    <HorizontalSingleSelectInput
                      formInputClass="form-input position-relative"
                      label="Shipping carrier"
                      name="shippingCarrierId"
                      errorText="Enter Shipping carrier"
                      isRequired={true}
                      imageRequired={false}
                      nameKey='_id'
                      labelkey='name'
                      placeholder="Select carrier"
                      options={this.props.carriers}
                      onChange={this.onInputChange}
                    />
                  </div>
                </div>
              </div>
              {/* for mobile1 */}
              <div className="col-12 d-block d-md-none mb-5">
                <button
                  className="btn border-btn border-0 p-0 mr-3"
                  onClick={this.props.addShipToData}
                >
                  &times; Delele country
                </button>
                <button
                  className="btn border-btn border-0 p-0 mr-4"
                  onClick={this.props.onClickAdd}
                >
                  + Add additional country
                </button>
              </div>
              {/* for mobile1 */}
              <div className="col-12 col-md-6 mb-0 mb-md-5">
                <div className="single-group label-mb">
                  <HorizontalInput
                    formInputClass="form-input position-relative form-input-append"
                    label="Template name"
                    type="text"
                    placeholder="Enter Template name"
                    name="templateName"
                    errorText=""
                    appendText="Save"
                    isRequired={false}
                    onChange={this.onInputChange}
                  />
                </div>
              </div>
            </div>
          </div>

          {/* <div className="col-12 mt-4 mb-5">
            <button
              className="btn border-btn mr-3 btn-w-115"
              onClick={this.props.addShipToData}
            >
              Add
            </button>
          </div> */}

          {this.props.shipToData &&
          this.props.shipToData.map((data, index) => {
            return (
              <div className="col-12 col-md-12 mb-2 " key={index}>
                <div className="row border border-dark">
                  <div className="col-4 col-md-4">
                    <div className="font-weight-bold text-uppercase mb-3">
                      Country
                    </div>
                    <h5>{data.country}</h5>
                  </div>
                  <div className="col-4 col-md-4">
                    <div className="font-weight-bold text-uppercase mb-3">
                      One item price
                    </div>
                    <h5>{data.shipToOneItemPrice}</h5>
                  </div>
                  <div className="col-4 col-md-4">
                    <div className="font-weight-bold text-uppercase mb-3">
                      Additional item price
                    </div>
                    <h5>{data.shiptToAdditionalItemPrice}</h5>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </React.Fragment>
    );
  }
}

export default SingleBlock;
