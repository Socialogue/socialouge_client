import React, {Component} from "react";
import {addOrRemove} from "core/utils/array";

class CountryGroup extends Component {

  state = {
    selected: []
  }

  onSelectCountryGroup = (name, isChecked) => {
    let {selected} = this.state;
    if(isChecked) {
      selected.push(name);
    } else {
      selected = selected.filter( i => i !== name)
    }

    this.setState({
      selected
    }, () => {
      this.props.onChange(selected)
    });
  };

  render() {

    return (
      <React.Fragment>
        <div className="col-6 mb-3">
          <label
            className="ctm-container radio-ctm font-weight-5"
            onClick={(e) => this.onSelectCountryGroup('EUUK', e.target.checked)}
          >
            Europe + UK
            <input type="checkbox" name="location" />
            <span className="checkmark" />
          </label>
        </div>
        <div className="col-6 mb-3">
          <label
            className="ctm-container radio-ctm font-weight-5"
            onClick={(e) => this.onSelectCountryGroup('FR', e.target.checked)}
          >
            France
            <input type="checkbox" name="location" />
            <span className="checkmark" />
          </label>
        </div>
        <div className="col-6 mb-3">
          <label
            className="ctm-container radio-ctm font-weight-5"
            onClick={(e) => this.onSelectCountryGroup('UK', e.target.checked)}
          >
            United Kingdom
            <input type="checkbox" name="location" />
            <span className="checkmark" />
          </label>
        </div>
        <div className="col-6 mb-3">
          <label
            className="ctm-container radio-ctm font-weight-5"
            onClick={(e) => this.onSelectCountryGroup('IT', e.target.checked)}
          >
            Italy
            <input type="checkbox" name="location" />
            <span className="checkmark" />
          </label>
        </div>
        <div className="col-6 mb-3">
          <label
            className="ctm-container radio-ctm font-weight-5"
            onClick={(e) => this.onSelectCountryGroup('DE', e.target.checked)}
          >
            Germany
            <input type="checkbox" name="location" />
            <span className="checkmark" />
          </label>
        </div>
        <div className="col-6 mb-3">
          <label
            className="ctm-container radio-ctm font-weight-5"
            onClick={(e) => this.onSelectCountryGroup('US', e.target.checked)}
          >
            USA
            <input type="checkbox" name="location" />
            <span className="checkmark" />
          </label>
        </div>
        <div className="col-6 mb-3">
          <label
            className="ctm-container radio-ctm font-weight-5"
            onClick={(e) => this.onSelectCountryGroup('ES', e.target.checked)}
          >
            Spain
            <input type="checkbox" name="location" />
            <span className="checkmark" />
          </label>
        </div>
        {/* for desktop1 */}
        <div className="col-12 mt-5 p-4 text-right d-none d-md-block">
          <button
            className="btn border-btn border-0 p-0 mr-3"
            onClick={() => this.props.onClickDelete(index)}
          >
            &times; Delele country
          </button>
          <button
            className="btn border-btn border-0 p-0 mr-4"
            onClick={this.props.onClickAdd}
          >
            + Add additional country
          </button>
        </div>
        {/* for desktop1 */}

        {/* <div className="col-12 pt-2">
                  <span className="x-sm-text-fw4-g cursor-pointer">
                    Show more
                  </span>
                </div> */}
      </React.Fragment>
    );
  }
}

export default CountryGroup;
