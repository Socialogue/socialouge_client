import React, { Component } from "react";
import SingleSelect from "../../../components/SingleSelect";

export default class Size extends Component {
  state = {
    multiBox: false,
  };
  render() {
    const { categoryData } = this.props;
    if (!categoryData) {
      return null;
    }
    return (
      <div>
        <div className="row align-items-center mb-5">
          <div className="col-12 col-md-5 mb-3 mb-md-4">
            <SingleSelect
              placeholder="Select measuring scale"
              options={[
                { scale: "Lithuania" },
                { scale: "United states" },
                { scale: "United kingdom" },
                { scale: "European union" },
              ]}
              name="scale"
              onChange={this.props.onChangeSize}
            />
          </div>

          <div className="sm-box-p mb-5 text-center text-md-left">
            {this.props.allSizes.map((sz) => {
              return (
                <span
                  className={
                    this.props.selectedSizes.includes(sz)
                      ? "sm-box sm-text-fw6 text-uppercase active"
                      : "sm-box sm-text-fw6 text-uppercase "
                  }
                  onClick={() => this.props.selectSize(sz)}
                >
                  {sz[this.props.selectedScaleValue]}
                </span>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}
