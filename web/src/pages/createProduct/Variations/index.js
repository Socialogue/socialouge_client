import React, { Component } from "react";
import Close from "../../../images/icons/close";
import Size from "./Size";
import Color from "./Color";
import { connect } from "react-redux";
import { selectCatData } from "core/selectors/products";
import { selectUser } from "core/selectors/user";
import CloseSm from "../../../images/icons/closeSm";
import Info from "../../../images/icons/info";
import HorizontalInput from "../../userSettings/components/horizontalInput";

const countryCodeMap = {
  "european union": "eu",
  "united kingdom": "uk",
  "united states": "us",
  lithuania: "lt"
};

class Variatios extends Component {
  state = {
    sizeOrColor: "size",
    selectedScale: "Lithuania",
    selectedScaleValue: "lt",
    allSizes: [],
    selectedSizes: [],
    allColors: [],
    selectedColors: [],
    attAddBox: false
  };

  componentDidMount = () => {
    const catSizes =
      this.props.selectedCategory && this.props.selectedCategory.sizes
        ? [...this.props.selectedCategory.sizes]
        : [];
    const allSizes = [...this.props.sizes];

    //all available sizes..

    const data = [];
    catSizes.map(catSize => {
      let sizes = allSizes.filter(allSize => {
        return allSize.category == catSize;
      });
      sizes.map(size => {
        data.push(size);
      });

      this.setState({
        allSizes: data
      });
    });

    //all lithuanian sizes...
    // let allAvailableSize = [...this.state.allSizes];
    // allAvailableSize.filter(allAvSize => allAvSize);

    this.setState({ allColors: this.props.colors });
  };

  changeTab = value => {
    this.setState({ sizeOrColor: value });
  };

  onChangeSize = (value, name) => {
    this.setState({ selectedScaleValue: countryCodeMap[value] });
  };

  selectSize = val => {
    const data = this.state.selectedSizes;
    const index = data.findIndex(i => i._id === val._id);
    if (index > -1) {
      data.splice(index, 1);
      this.setState({ selectedSizes: data });
      this.props.selectVariation(data, "selectedSizes");
      return true;
    }

    data.push(val);
    this.setState({ selectedSizes: data });
    this.props.selectVariation(data, "selectedSizes");
    return true;
  };

  selectColor = val => {
    const data = this.state.selectedColors;
    const index = data.findIndex(i => i._id === val._id);
    if (index > -1) {
      data.splice(index, 1);
      this.setState({ selectedColors: data });
      this.props.selectVariation(data, "selectedColors");
      return true;
    }

    data.push(val);
    this.setState({ selectedColors: data });
    this.props.selectVariation(data, "selectedColors");
    return true;
  };

  handleInputChange = (name, value) => {
    this.setState({
      [name]: value
    });
    this.props.deliveryData(name, value);
  };
  render() {
    const { variationCombinations } = this.props;

    return (
      <div>
        <div className="row mb-4">
          <div className="col-12 col-md-6">
            <div className="md-title text-uppercase  mb-1">VARIATIONS</div>
            <div className="sm-text-fw4-g">
              Choose your product attributes here. If you select multiple of the
              same atribute it will be considered as variantion. You can even
              create your own.
            </div>
          </div>
        </div>
        <ul className="nav tab-nav v-tab-nav mb-5">
          <li className="nav-item ml-0 ml-md-4 mr-4">
            <span
              className={
                this.state.sizeOrColor == "size"
                  ? "nav-link sm-text-fw6-g text-uppercase active cursor-pointer"
                  : "nav-link sm-text-fw6-g text-uppercase cursor-pointer"
              }
              onClick={() => this.changeTab("size")}
            >
              Size <Info />
            </span>
          </li>
          {this.props.selectedCategory && this.props.selectedCategory.color && (
            <li className="nav-item ml-0 ml-md-4 mr-4">
              <span
                className={
                  this.state.sizeOrColor == "color"
                    ? "nav-link sm-text-fw6-g text-uppercase active cursor-pointer"
                    : "nav-link sm-text-fw6-g text-uppercase cursor-pointer"
                }
                onClick={() => this.changeTab("color")}
              >
                Color
                <Info />
              </span>
            </li>
          )}
          {/*<li className="nav-item ml-0 ml-md-4 mr-4">
            <span
              className={
                this.state.sizeOrColor == "own"
                  ? "nav-link sm-text-fw6-g text-uppercase active"
                  : "nav-link sm-text-fw6-g text-uppercase "
              }
              onClick={() => this.changeTab("own")}
            >
              YOUR OWN{" "}
              <span className="scale-hover cursor-pointer">
                <CloseSm />
              </span>
            </span>
          </li>
          <li
            className={
              this.state.attAddBox
                ? "nav-item ml-0 ml-md-4 mr-0 add-atr-li active"
                : "nav-item ml-0 ml-md-4 mr-0 add-atr-li"
            }
          >
            <span
              onClick={() =>
                this.setState({
                  attAddBox: !this.state.attAddBox
                })
              }
              className="nav-link sm-text-fw6-g text-uppercase cursor-pointer"
            >
              + Add
              <Info />
            </span>
            <div className="add-atr-box">
              <div className="mb-1">Add your own atribute</div>
              <div className="x-sm-text-fw4-g mb-4">
                Enter a distinguishing feature that describes how your items
                differ.
              </div>
              <div className="single-group hide-label">
                <HorizontalInput
                  formInputClass="form-input position-relative form-input-append"
                  label=""
                  type="text"
                  placeholder="Enter your attribute"
                  name="add"
                  errorText=""
                  appendText="Add"
                  isRequired={false}
                  onChange={this.handleInputChange}
                />
              </div>
            </div>
          </li>*/}
        </ul>
        {this.state.sizeOrColor == "own" && (
          <div>
            <div className="mb-4">
              <span className="sm-box d-inline-flex sm-text-fw6 text-uppercase px-3">
                <span className="w-100">Silver</span>
                <span className="d-flex-box scale-hover cursor-pointer ml-2">
                  <CloseSm />
                </span>
              </span>
            </div>
            <div className="row">
              <div className="single-group hide-label  col-12 col-md-4">
                <HorizontalInput
                  formInputClass="form-input position-relative form-input-append"
                  label=""
                  type="text"
                  placeholder="Create your own"
                  name="add"
                  errorText=""
                  appendText="Add"
                  isRequired={false}
                  onChange={this.handleInputChange}
                />
              </div>
            </div>
          </div>
        )}
        {this.state.sizeOrColor == "size" ? (
          <Size
            categoryData={this.props.selectedCategory}
            sizes={this.props.sizes}
            onChangeSize={this.onChangeSize}
            allSizes={this.state.allSizes}
            selectedSizes={this.state.selectedSizes}
            selectSize={this.selectSize}
            selectedScaleValue={this.state.selectedScaleValue}
          />
        ) : (
          <Color
            colors={this.props.colors}
            selectedColors={this.state.selectedColors}
            selectColor={this.selectColor}
          />
        )}

        <div className="mb-5">
          <div className="font-weight-bold mb-2 text-uppercase">
            Attributes you’ve selected
          </div>
          <div>
            <span className="sm-text-fw4-g mr-1">Number of variations</span>
            <span className="sm-text-fw6">{variationCombinations.length}</span>
          </div>
        </div>

        <div className="row mb-5 cs-sm-box">
          <div className="col-12 col-md-6">
            <div className="font-weight-bold mb-3 text-uppercase">
              Size list
            </div>
            <div className="">
              {this.state.selectedSizes &&
                this.state.selectedSizes.map(slSize => {
                  return (
                    <span className="sm-box d-flex  font-weight-5 w-100 px-3 m-0">
                      <span className="w-100 text-left">
                        {slSize[this.state.selectedScaleValue]}{" "}
                      </span>
                      <span
                        className="float-right d-flex-box scale-hover cursor-pointer"
                        onClick={() => this.selectSize(slSize)}
                      >
                        <Close />
                      </span>
                    </span>
                  );
                })}
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="font-weight-bold mb-3 text-uppercase">
              Color list
            </div>
            <div className="">
              {this.state.selectedColors &&
                this.state.selectedColors.map(slColor => {
                  return (
                    <span className="sm-box font-weight-5 w-100 px-3 d-flex">
                      <span className="w-100 d-flex align-items-center text-left text-capitalize m-0">
                        {/* <span className="color-s" />  */}
                        {slColor["name"]}
                      </span>
                      <span className="float-right d-flex-box">
                        {/* <span className="scale-hover cursor-pointer d-flex-box mr-3">
                          <UploadSm />
                        </span> */}
                        <span
                          className="scale-hover cursor-pointer d-flex-box"
                          onClick={() => this.selectColor(slColor)}
                        >
                          <Close />
                        </span>
                      </span>
                    </span>
                  );
                })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    selectedCategory: selectCatData(state),
    user: selectUser(state)
  }),
  {}
)(Variatios);
