import React from "react";
import Upload from "../../../images/icons/upload";

export default (props) => {
  return (
    <div className="show-img-pp show-img-pp-pb-90 upload-img">
      <div className="upload-photo-cnt d-flex-box">
        <div className="upload-photo d-flex-box">
          <div className="upload-photo-txt">
            <div className="upload-icon">
              <Upload />
            </div>
            <input
              type="file"
              className="ctm-hidden-upload-input cover"
              multiple
              onChange={props.onSelectMainImage}
            />
          </div>
          <div className="upload-txt x-sm-text-fw4-g">
            Upload Photos
          </div>
        </div>
      </div>
    </div>
  )
}
