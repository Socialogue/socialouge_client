import React from 'react';
import ReactPlayer from 'react-player'
import Close from '../../../images/icons/close';

class VideoPlayerPopup extends React.Component {

  render() {
    let styles = this.props.show
      ? { display: "block" }
      : { display: "none" };
    return (
      <div>
        <div className="modal video-modal" tabIndex="-1" role="dialog" style={styles}>
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content p-4">
              <div className="modal-header pt-0 px-0">
              <div className="sm-text-fw6 text-uppercase">Uplode Video 1</div>
              <span
                className="cursor-pointer"
                aria-hidden="true"
                onClick={this.props.onClose}
              >
                <Close />
              </span>
            </div>
              <div className="modal-body px-0 pb-0">
                {!!(this.props.video) &&
                <ReactPlayer url={URL.createObjectURL(this.props.video)} playing controls/>
                }
              </div>
              {/*<div className="modal-footer">
                <button type="button" className="btn btn-primary">Save changes</button>
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>*/}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default VideoPlayerPopup;
