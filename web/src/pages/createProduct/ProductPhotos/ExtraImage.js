import React, { useState, useEffect } from 'react';
import Delete from "../../../images/icons/delete";
import PlayIcon from '../../../images/icons/playIcon.svg';
import Screen from "../../../images/icons/screen";
import {getVideoCover, isVideoFile} from "../../../utils/videoFile";

export default (props) => {

  const [img, setImg] = useState(URL.createObjectURL(props.image));

  useEffect(() => {
    if (isVideoFile(props.image)) {
      getVideoCoverFromFile(props.image);
    }
  });

  const getVideoCoverFromFile = async (image) => {
    const cover = await getVideoCover(image);
    const reader = new FileReader();
    reader.readAsDataURL(cover);
    reader.onloadend = () => {
      const base = reader.result;
      setImg(base);
    }
  }

  return (
    <div className="show-img-pp show-img-pp-pb-90 upload-img">
      <div className="show-img-p">
        <img
          className="cover"
          src={img}
        />

        {/* for play video */}
        {props.image.type.startsWith('video') && <span className="play-icon"><img src={PlayIcon} /></span>}
        {/* for play video */}

        <div className="photo-control">
                          <span className="screen-icon">
                            <Screen />
                          </span>
          <div
            className="photo-delete d-flex-box x-sm-text-fw6"
            onClick={props.removeByIndex}
          >
            <Delete />
            Delete
          </div>
          <span className="photo-txt x-sm-text-fw6">
                            #{props.index}
                          </span>
        </div>
      </div>
    </div>
  )
}
