import React from "react";
import Upload from "../../../images/icons/upload";
import MainImagePlaceholder from './MainImagePlaceholder';
import MainImage from './MainImage';
import ExtraImage from './ExtraImage';
import VideoPlayerPopup from './VideoPlayerPopup';

class Index extends React.Component {
  state = {
    images: [],
    mainImage: null,
    videoPlayerPopupVisible: false,
    currentlyPlaying: null,
  };

  getState = () => {
    return this.state;
  }

  onSelectExtraImages = event => {
    event.stopPropagation();
    event.preventDefault();
    const selected = Array.from(event.target.files);
    this.setState(prevState => {
      const { images } = prevState;
      const updated = images.concat(selected);
      return { images: updated };
    });
  };

  showVideoPlayer = file => {
    if(!file.type.startsWith('video')) {
      return 0;
    }
    this.setState({videoPlayerPopupVisible: true, currentlyPlaying: file});
  };

  closeVideoPlayer = () => {
    this.setState({videoPlayerPopupVisible: false, currentlyPlaying: null});
  };

  onSelectMainImage = event => {
    event.stopPropagation();
    event.preventDefault();
    const file = event.target.files[0];
    this.setState({ mainImage: file });
  };

  removeByIndex = index => {
    this.setState(prevState => {
      const { images } = prevState;
      images.splice(index, 1);
      return { images };
    });
  };

  onDragStart = (event, index, draggingMain = false) => {
    event.dataTransfer.setData("text/html", String(event.target.outerHTML));
    event.dataTransfer.setData("dragIndex", index);
    if (draggingMain) {
      event.dataTransfer.setData("draggingMain", true);
    }
  };

  onDrop = (event, dropIndex, droppingOnMain = false) => {
    const dragIndex = event.dataTransfer.getData("dragIndex");
    const draggingMain = event.dataTransfer.getData("draggingMain");
    const { images } = this.state;

    if (droppingOnMain && this.state.mainImage) {
      const mainImage = images[dragIndex];
      images[dragIndex] = this.state.mainImage;
      this.setState({ images, mainImage });
      return true;
    }

    if (draggingMain) {
      const mainImage = images[dropIndex];
      images[dropIndex] = this.state.mainImage;
      this.setState({ images, mainImage });
      return true;
    }

    const tmp = images[dropIndex];
    images[dropIndex] = images[dragIndex];
    images[dragIndex] = tmp;
    this.setState({ images });
    return true;
  };

  /*
   * The default drop behavior for certain DOM elements like <div>s in browsers typically does not accept dropping.
   * This behavior will intercept the behavior we are attempting to implement.
   * To ensure that we get the desired drop behavior, we will apply preventDefault.
   * */
  onDragOver = event => {
    event.preventDefault();
  };

  render() {
    const { mainImage, videoPlayerPopupVisible, currentlyPlaying } = this.state;

    return (
      <div>
        <VideoPlayerPopup
          video={currentlyPlaying}
          show={videoPlayerPopupVisible}
          onClose={this.closeVideoPlayer}
        />
        <div className="row mb-4">
          <div className="col-12 col-md-5">
            <div className="md-title text-uppercase mb-1">PRODUCT PHOTOS</div>
            <div className="sm-text-fw4-g">
              Add as many as you can so buyers can see every detail. Use up to
              15 photos to show your items's most important qualities.
            </div>
          </div>
        </div>

        <div className="row mb-5">
          <div
            className="col-12 col-md-5 pb-3"
            draggable
            onDragStart={e => this.onDragStart(e, 0, true)}
            onDragOver={this.onDragOver}
            onDrop={e => this.onDrop(e, 0, true)}
          >
            {!!(mainImage) &&
            <MainImage
              onClickPlay={() => this.showVideoPlayer(mainImage)}
              image={mainImage}
              onDelete={() => this.setState({mainImage: null})}/>

            }

            {(mainImage === null) &&
              <MainImagePlaceholder onSelectMainImage={this.onSelectMainImage}/>
            }

          </div>
          <div className="col-12 col-md-7">
            <div className="row mr-0">
              {this.state.images.map((image, i) => {
                return (
                  <div
                    onClick={() => this.showVideoPlayer(image)}
                    className="col-6 col-lg-4 pr-0 mb-3"
                    key={i}
                    draggable
                    onDragOver={this.onDragOver}
                    onDragStart={e => this.onDragStart(e, i)}
                    onDrop={e => this.onDrop(e, i)}
                  >
                    <ExtraImage image={image} index={i+2} removeByIndex={() => this.removeByIndex(i)}/>
                  </div>
                );
              })}
              <div className="col-6 col-lg-4 pr-0 mb-3">
                <div className="show-img-pp show-img-pp-pb-90 upload-img">
                  <div className="upload-photo-cnt d-flex-box">
                    <div className="upload-photo d-flex-box">
                      <div className="upload-photo-txt">
                        <div className="upload-icon">
                          <Upload />
                        </div>
                        <input
                          type="file"
                          className="ctm-hidden-upload-input cover"
                          multiple
                          onChange={this.onSelectExtraImages}
                        />
                      </div>
                      <div className="upload-txt x-sm-text-fw4-g">
                        Upload Photos
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Index;
