import React, {useEffect, useState} from "react";
import Delete from "../../../images/icons/delete";
import Screen from "../../../images/icons/screen";
import {getVideoCover, isVideoFile} from "../../../utils/videoFile";
import PlayIcon from "../../../images/icons/playIcon.svg";

export default (props) => {

  const [img, setImg] = useState(URL.createObjectURL(props.image));

  useEffect(() => {
    if (isVideoFile(props.image)) {
      getVideoCoverFromFile(props.image);
    }
  });

  const getVideoCoverFromFile = async (image) => {
    const cover = await getVideoCover(image);
    const reader = new FileReader();
    reader.readAsDataURL(cover);
    reader.onloadend = () => {
      const base = reader.result;
      setImg(base);
    }
  }

  return (
    <div className="position-relative wh-100">
      <img
        onClick={props.onClick}
        className="cover"
        src={img}
      />
      {isVideoFile(props.image) && <span className="play-icon" onClick={props.onClickPlay}><img src={PlayIcon} /></span>}
      <div className="photo-control">
                <span className="screen-icon">
                  <Screen />
                </span>
        <span className="photo-delete d-flex-box x-sm-text-fw6" onClick={props.onDelete}>
                  <Delete />
                  Delete
                </span>
        <span className="photo-txt x-sm-text-fw6">#1 (Main Photo)</span>
      </div>
    </div>
  )
}
