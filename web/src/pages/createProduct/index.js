//@flow

import {selectAdditionalData, selectCatData, selectInnerCatData, selectSubCatData} from 'core/selectors/products';
import {selectActiveShop} from 'core/selectors/account';
import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import ProductListing from '../productListing';
import Header from './../home/Header/Header';
import ItemDescription from './components/itemDescription';
import ProductPhotos from './ProductPhotos';
import ShippingOptions from './ShippingOptions';
import InternationalShipping from './InternationalShipping';
import TopPart from './components/topPart';
import VariationCombinations from './VariationCombination';
import Variations from './Variations';
import VariationsPhotos from './components/variationsPhotos';
import {getAdditionalDataForProduct} from 'core/actions/productActionCreators';
import {getAllVariations} from 'core/utils/product';
import {state, VIEW_CATEGORY, VIEW_PRODUCT} from './initialState';
import './index.scss';
import config from "../../Config";
import {FILE_SERVER_SINGLE_IMAGE, FILE_SERVER_SINGLE_VIDEO} from "core/constants/apiEndpoints";
import {selectUser} from "core/selectors/user";
import {insertProduct} from "core/actions/productInsertActionCreators";
import {selectErrorMessage, selectSuccessMessage} from "core/selectors/productsInsert";
import {getFilename} from "core/utils/url";
import {isVideoFile} from "../../utils/videoFile";
import {insertShippingFromTemplate, insertShippingToTemplate} from "core/actions/productInsertActionCreators";

localStorage.setItem('myData', 1);

class CreateProduct extends React.Component {

  state = { ...state };

  changeView = (value) => {
    this.setState({
      view: value
    });
  };

  componentDidMount = () => {
    if (this.props.activeShopId._id) {
      this.props.getAdditionalDataForProduct(this.props.activeShopId._id);
    }
  };

  componentDidUpdate = (prevProps) => {
    if (prevProps.additionalData !== this.props.additionalData) {
      if (this.props.additionalData !== '') {
        console.log('additional data: ', this.props.additionalData);
      }
    }

    if (prevProps.successMsg !== this.props.successMsg) {
      if (this.props.successMsg !== '') {
        alert(this.props.successMsg);
        this.props.history.push('/');
      }
    }

    if (prevProps.errorMsg !== this.props.errorMsg) {
      if (this.props.errorMsg !== '') {
        alert(this.props.errorMsg);
      }
    }
  };

  selectVariation = (values, name) => {
    this.setState({ [name]: values }, () => {
      const variationCombinations = getAllVariations({
        color: this.state.selectedColors,
        sizes: this.state.selectedSizes
      });
      const addedExtraProps = variationCombinations.map( item => {
        return {
          ...item,
          sku: '',
          qty: 0,
          price: ''
        }
      })
      this.setState({ variationCombinations: addedExtraProps });
    });
  };

  onChangeVariationPhotos = (variationPhotos) => {
    this.setState({ variationPhotos });
  };

  updateVariations = (field, index, value) => {
    const {variationCombinations} = this.state;
    let updated = variationCombinations.map((item, i) => {
      if(index === i && ['qty', 'price', 'sku'].includes(field) /*we dont need to do this for smae_qty & same_price*/) {
        item[field] = value;
      }
      return item;
    });

    // if I'm updating 0 index's qty or price, and the same qty or same price
    // checkbox is selected, then I should update all items with the value from 0 index item
    if(index === 0 && ['qty', 'price'].includes(field)) {
      if(this.state.variationSameQtyForAll && field === 'qty') {
        const first = updated[0];
        updated = updated.map( item => {
          item.qty = first.qty;
          return item;
        });
      }
      if(this.state.variationSamePriceForAll && field === 'price') {
        const first = updated[0];
        updated = updated.map( item => {
          item.price = first.price;
          return item;
        });
      }
    }
    this.setState({ variationCombinations: updated });
  };

  onChangeVariation = (field, index, value) => {
    if(field === 'same_qty') {
      this.setState({variationSameQtyForAll: value}, () => {
        this.updateVariations(field, index, value);
      });
    } else if(field === 'same_price') {
      this.setState({variationSamePriceForAll: value}, () => {
        this.updateVariations(field, index, value);
      });
    } else {
      this.updateVariations(field, index, value);
    }
  };

  onRemoveVariation = (index) => {
    const data = [...this.state.variationCombinations];
    data.splice(index, 1);
    this.setState({variationCombinations: data});
  };

  addShipToData = () => {
    const data = [...this.state.shipToData];
    data.push({
      country: this.state.country,
      shipToOneItemPrice: this.state.shipToOneItemPrice,
      shiptToAdditionalItemPrice: this.state.shiptToAdditionalItemPrice
    });
    this.setState({ shipToData: data });
  };

  selectCustomCountry = (country) => {
    this.setState({
      customCountry: country
    });
  };

  changeStep = (value) => {
    let n_step = parseInt(localStorage.getItem('myData'));
    if (value == 'next') {
      n_step += 1;
    } else {
      n_step -= 1;
    }
    localStorage.setItem('myData', n_step);
    if (n_step < 5 && n_step > 0) {
      if (n_step == 1) {
        this.setState({
          stepOne: true,
          stepTwo: false,
          stepThree: false,
          stepFour: false,
          backBtn: false,
          saveBtn: false,
          nextBtn: true,
          previewBtn: false
        });
      }
      if (n_step == 2) {
        this.setState({
          stepOne: false,
          stepTwo: true,
          stepThree: false,
          stepFour: false,
          backBtn: true,
          saveBtn: false,
          nextBtn: true,
          previewBtn: false
        });
      } else if (n_step == 3) {
        this.setState({
          stepOne: false,
          stepTwo: false,
          stepThree: true,
          stepFour: false,
          nextBtn: true,
          saveBtn: false,
          previewBtn: false
        });
      } else if (n_step == 4) {
        this.setState({
          stepOne: false,
          stepTwo: false,
          stepThree: false,
          stepFour: true,
          saveBtn: true,
          nextBtn: false,
          previewBtn: true
        });
      }
    }
  };

  onChangeName = (e) => {
    this.setState({productName: e.target.value});
  };

  onChangeDescription = (html) => {
    this.setState({description: html});
  };

  createProduct = (mainImageUrl, extraImageUrls, variationPhotos) => {
    const payload = {};
    payload.title = this.state.productName;
    payload.description = this.state.description;
    payload.userId = this.props.authUser._id;
    payload.shopId = this.props.activeShopId._id;
    payload.category = this.props.categoryData._id;
    payload.subCategory = this.props.subCategoryData._id;
    payload.innerCategory = this.props.innerCategoryData._id;

    //images
    payload.mainImage = mainImageUrl;
    payload.additionalImage = extraImageUrls;
    payload.variationImages = variationPhotos;

    //shipping options
    const shippingOptions = this.shippingOptions.getState();
    payload.shippingOptions = shippingOptions
    payload.hasIntlShipping = this.state.intShipping;
    if (this.state.intShipping) {
      const intShippingOptions = this.intlShipping.getState();
      payload.intlShippingOptions = intShippingOptions;
    }

    //variations
    payload.totalVariations = this.state.variationCombinations.length;
    const variations = this.insertVariationImage(variationPhotos);
    payload.variations = variations;

    console.log({payload});
    this.props.insertProduct(payload);
  };

  insertVariationImage = (variationPhotos) => {
    const data = this.state.variationCombinations.map( item => {
      const colorphoto = variationPhotos.find( i => i.colorId === item.color._id);
      if (colorphoto) {
        item.colorImage = colorphoto.imageUrl;
      }
      return item;
    });

    return data;
  }

  insertShippingTemplates = () => {
    const fromData = this.shippingOptions.getState();

    const fromPayload = {
      shopId: this.props.activeShopId._id,
      country: fromData.country,
      processingTime: fromData.processingTime,
      carrier: fromData.carrier,
      // carrierName:usps,
      templateName: fromData.templateName,
      deliveryTimeFrom: fromData.deliveryTimeFrom,
      deliveryTimeTo: fromData.deliveryTimeTo,
      oneItemPrice: fromData.oneItemCost,
      additionalItemPrice: fromData.additionalItemCost,
      freeShipping: fromData.freeShipping,
      status: 'shipFromTemplate' /*Or "shipToTemplate" required field*/
    };
    this.props.insertShippingFromTemplate(fromPayload);

    if (fromData.int_shipping == "International shipping" && this.intlShipping) {
      const toData = this.intlShipping.getState();
      const toPayload = {
        shopId: this.props.activeShopId._id,
        country: toData.country,
        processingTime: toData.processingTime,
        carrier: toData.carrier,
        // carrierName:usps,
        templateName: toData.templateName,
        deliveryTimeFrom: toData.deliveryTimeFrom,
        deliveryTimeTo: toData.deliveryTimeTo,
        oneItemPrice: toData.oneItemCost,
        additionalItemPrice: toData.additionalItemCost,
        freeShipping: toData.freeShipping,
        status: 'shipToTemplate'
      };
      this.props.insertShippingToTemplate(toPayload);
    }
  }

  uploadSingleFile = (file) => {
    const isVideo = isVideoFile(file);
    const formData = new FormData();
    const key = isVideo ? 'video' : 'image';
    const url = isVideo ? config.FILE_SERVER_URL + FILE_SERVER_SINGLE_VIDEO
      : config.FILE_SERVER_URL + FILE_SERVER_SINGLE_IMAGE;

    formData.append(key, file);
    formData.append("key", config.FILE_SERVER_KEY);
    const prom = axios.post(url, formData, {
      headers: {
        Accept: "Application/json",
        allowed_headers: "*",
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json"
      }
    });

    return prom;
  }

  onSubmit = async () => {

    //TODO validation

    let mainImageUrl = '';
    let extraImageUrls = [];
    let variationPhotos = []; //{colorId, imageUrl}

    const photos = this.productPhotos.getState();

    const mainImageResp = await this.uploadSingleFile(photos.mainImage);
    mainImageUrl = getFilename(mainImageResp.data.original_path); //TODO check api fail, get converted path

    // extra images
    const extraImages = photos.images;
    const promiseArray = [];
    if (extraImages.length) {
      extraImages.map( img => {
        const prom = this.uploadSingleFile(img);
        promiseArray.push(prom);
      });
    }
    const extraImagesResult = await Promise.all(promiseArray);
    extraImagesResult.map( resp => extraImageUrls.push(getFilename(resp.data.original_path))); //TODO check api fail, get converted path

    // variation images (color)
    const variationPhotosState = this.variationPhotos.getState();
    if(variationPhotosState.images.length) {
      const allPromisesArray = variationPhotosState.images.map( item => {
        return this.uploadSingleFile(item.file).then( result => {
          return {
            colorId: item.colorId,
            imageUrl: getFilename(result.data.original_path)
          }
        });
      }); //TODO check api fail, get converted path
      variationPhotos = await Promise.all(allPromisesArray);
    }

    //insert shipping templates
    this.insertShippingTemplates();

    this.createProduct(mainImageUrl, extraImageUrls, variationPhotos);
  };

  render() {
    const { additionalData } = this.props;
    const {
      variationCombinations,
      selectedColors,
      intShipping,
      intShippingOptions,
      productName
    } = this.state;

    console.log({additionalData});
    return (
      <div>
        <Header />
        <div className='max-content-width my-5 px-4 px-xl-0'>
          <div
            className={
              this.state.stepOne ? 'step-1' : 'step-1 d-none d-md-block'
            }
          >
            {this.state.view === VIEW_PRODUCT && (
              <TopPart changeView={this.changeView} onChangeName={this.onChangeName} productName={productName} />
            )}
          </div>
          {this.state.view === VIEW_CATEGORY && (
            <ProductListing
              changeView={this.changeView}
              onPressNext={() => this.setState({ view: VIEW_PRODUCT })}
            />
          )}
          {this.state.view === VIEW_PRODUCT && (
            <Fragment>
              <div className={this.state.stepOne ? 'step-1' : 'step-1 d-none d-md-block'}>
                <ProductPhotos ref={ref => (this.productPhotos = ref)} />
              </div>

              <div
                className={
                  this.state.stepTwo
                    ? 'step-2 d-block'
                    : 'step-2 d-none d-md-block'
                }
              >
                <Variations
                  colors={additionalData.colors ? additionalData.colors : []}
                  sizes={additionalData.sizes ? additionalData.sizes : []}
                  selectVariation={this.selectVariation}
                  variationCombinations={variationCombinations}
                />
                <VariationsPhotos
                  onChange={this.onChangeVariationPhotos}
                  ref={ref => (this.variationPhotos = ref)}
                  colors={selectedColors}
                />
                <VariationCombinations
                  onChange={this.onChangeVariation}
                  onClickRemove={this.onRemoveVariation}
                  variationCombinations={variationCombinations}
                  variationPhotos={this.state.variationPhotos}
                />
              </div>
              <div
                className={
                  this.state.stepThree
                    ? 'step-3 d-block'
                    : 'step-3 d-none d-md-block'
                }
              >
                <ItemDescription
                  onChange={this.onChangeDescription}
                  description={this.state.description}
                />
              </div>
              <div
                className={
                  this.state.stepFour
                    ? 'step-4 d-block'
                    : 'step-4 d-none d-md-block'
                }
              >
                <ShippingOptions
                  ref={ref => (this.shippingOptions = ref)}
                  countries={additionalData.countries}
                  carriers={additionalData.carriers}
                  shipToData={this.state.shipToData}
                  addShippingData={this.selectVariation}
                  addShipToData={this.addShipToData}
                  selectCustomCountry={this.selectCustomCountry}
                  onUpdateIntlShippingOption={intShipping =>
                    this.setState({ intShipping })
                  }
                />
                {intShipping && (
                  <InternationalShipping
                    ref={ref => (this.intlShipping = ref)}
                    items={intShippingOptions}
                    onAddAdditionalCountry={this.addIntlShippingOption}
                    carriers={additionalData.carriers}
                  />
                )}
              </div>
              <div className='d-flex justify-content-end mb-5'>
                <button
                  onClick={() => this.changeStep('back')}
                  className={
                    this.state.backBtn
                      ? 'btn border-btn border-btn-gray ml-3 px-4 d-inline-block d-md-none'
                      : 'btn border-btn border-btn-gray ml-3 px-4 d-none'
                  }
                >
                  Back
                </button>
                <button
                  onClick={() => this.changeStep('next')}
                  className={
                    this.state.nextBtn
                      ? 'btn border-btn ml-3 px-5 d-inline-block d-md-none'
                      : 'btn border-btn ml-3 px-5 d-none'
                  }
                >
                  Next
                </button>
                <button
                  className={
                    this.state.saveBtn
                      ? 'btn border-btn ml-3 btn-w-115 '
                      : 'btn border-btn ml-3 btn-w-115 d-none d-md-inline-block'
                  }
                  onClick={this.onSubmit}
                >
                  Save
                </button>
                <button
                  className={
                    this.state.previewBtn
                      ? 'btn ml-3 border-btn border-btn-gray'
                      : 'btn ml-3 border-btn border-btn-gray d-none d-md-inline-block'
                  }
                >
                  Preview listing
                </button>
              </div>
            </Fragment>
          )}
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    categoryData: selectCatData(state),
    subCategoryData: selectSubCatData(state),
    innerCategoryData: selectInnerCatData(state),
    additionalData: selectAdditionalData(state),
    activeShopId: selectActiveShop(state),
    authUser: selectUser(state),
    successMsg: selectSuccessMessage(state),
    errorMsg: selectErrorMessage(state)
  }),
  {
    getAdditionalDataForProduct,
    insertProduct,
    insertShippingToTemplate,
    insertShippingFromTemplate
  }
)(CreateProduct);
