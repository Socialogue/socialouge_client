import React from "react";
import Image from "../../../images/eco-img.webp";
import CloseSm from "../../../images/icons/closeSm";

export default (props) => {
  function inputValueCnt(v, e) {
    if (v == 1) {
      if (e.currentTarget.nextSibling.firstChild) {
        var value = e.currentTarget.nextSibling.firstChild.value;
       
        value = isNaN(value) ? 0 : value;
        value < 1 ? (value = 1) : "";
        value--;
        e.currentTarget.nextSibling.firstChild.value = value;
      }
    } else {
      if (e.currentTarget.previousSibling.firstChild) {
        var value = e.currentTarget.previousSibling.firstChild.value;
        value = isNaN(value) ? 0 : value;
        value++;
        e.currentTarget.previousSibling.firstChild.value = value;
      }
    }
  }
  let img = Image;
  const colorphoto = props.variationPhotos.find(
    (i) => i.colorId === props.item.color._id
  );
  if (colorphoto) {
    img = URL.createObjectURL(colorphoto.file);
  }
  return (
    <tr className="">
      <td className="sm-text-fw6 text-uppercase d-none d-md-table-cell">
        <span
          className="scale-hover"
          onClick={() => props.onClickRemove(props.index)}
        >
          <CloseSm />
        </span>
      </td>
      <td className="sm-text-fw6 text-uppercase td-border f-td d-none d-md-table-cell">
        <img src={img} width="38" height="48" alt="" />
      </td>
      <td className="sm-text-fw6 text-uppercase td-border text-nowrap">
        {props.item.sizes.eu} / {props.item.sizes.lt} / {props.item.sizes.uk} /{" "}
        {props.item.sizes.us}
      </td>
      <td className="sm-text-fw6 text-uppercase td-border">
        {props.item.color ? props.item.color.name : ""}
      </td>
      <td className="sm-text-fw6 text-uppercase td-border">
        <input
          className="ct-mp"
          type="text"
          placeholder="Enter SKU"
          value={props.item.sku}
          onChange={(e) => props.onChange("sku", props.index, e.target.value)}
        />
      </td>
      <td className="sm-text-fw6 td-border">
        <div className="">
          <span className="v-control text-nowrap">
            <span onClick={(e) => inputValueCnt(1, e)} className="cn-v minus-v">
              -
            </span>
            <span className="td-v m-0">
              <input
                className="ct-mp"
                type="text"
                value={props.item.qty}
                onChange={(e) =>
                  props.onChange("qty", props.index, e.target.value)
                }
                placeholder="Enter quantity"
              />
            </span>
            <span onClick={(e) => inputValueCnt(2, e)} className="cn-v plus-v">
              +
            </span>
          </span>
        </div>
        {!!(props.index === 0) && (
          <span className="all-pq">
            <label className="ctm-container d-inline-block sm-text-fw4-g text-nowrap">
              Same Qty for all
              <input
                type="checkbox"
                onChange={(e) =>
                  props.onChange("same_qty", 0, e.target.checked)
                }
              />
              <span className="checkmark" />
            </label>
          </span>
        )}
      </td>
      <td className="sm-text-fw6 td-border l-td">
        <span className="all-pq">
          <input
            placeholder="Enter price.."
            value={props.item.price}
            onChange={(e) =>
              props.onChange("price", props.index, e.target.value)
            }
          />
          {!!(props.index === 0) && (
            <label className="ctm-container d-inline-block sm-text-fw4-g text-nowrap">
              Same price for all
              <input
                type="checkbox"
                onChange={(e) =>
                  props.onChange("same_price", 0, e.target.checked)
                }
              />
              <span className="checkmark" />
            </label>
          )}
        </span>
      </td>
    </tr>
  );
};
