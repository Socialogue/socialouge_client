import React from "react";
import Image from "../../../images/eco-img.webp";
import CloseSm from "../../../images/icons/closeSm";
import RowItem from "./RowItem";

class VariationCombinations extends React.Component {
  render() {
    const { variationCombinations } = this.props;

    return (
      <div className="mb-5">
        <div className="md-title text-uppercase mb-3">
          VARIATIONS Combinations
        </div>
        <div className="seller-table create-p-list">
          <div className="tbl-cnt">
            {/* only mobile */}
            <div className="tbl-1 d-block d-md-none">
              <table class="table">
                <thead>
                  <tr>
                    <th className="sm-text-fw6-g text-uppercase" />
                    <th className="sm-text-fw6-g text-uppercase photo-td">
                      Photo
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {/* loop */}
                  {variationCombinations.map((item, index) => {
                    return (
                      <tr className="">
                        {/* when item select that time will be add active class in tr */}
                        <td className="sm-text-fw6 text-uppercase px-0">
                          <CloseSm />
                        </td>
                        <td className="sm-text-fw6 text-uppercase td-border f-td">
                          <img src={Image} width="38" height="48" alt="" />
                        </td>
                      </tr>
                    );
                  })}
                  {/* loop */}
                </tbody>
              </table>
            </div>
            {/* only mobile */}

            <div className="tbl-2">
              <table class="table">
                <thead>
                  <tr>
                    <th className="sm-text-fw6-g text-uppercase d-none d-md-table-cell" />
                    <th className="sm-text-fw6-g text-uppercase photo-td d-none d-md-table-cell">
                      Photo
                    </th>
                    <th className="sm-text-fw6-g text-uppercase s-td-w">
                      Size
                    </th>
                    <th className="sm-text-fw6-g text-uppercase s-td-w">
                      color
                    </th>
                    <th className="sm-text-fw6-g text-uppercase s-td-w">SKU</th>
                    <th className="sm-text-fw6-g text-uppercase s-td-w">
                      Quantity
                    </th>
                    <th className="sm-text-fw6-g text-uppercase s-td-w">
                      Price
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {variationCombinations.map((item, index) => {
                    return (
                      <RowItem
                        item={item}
                        index={index}
                        onChange={this.props.onChange}
                        onClickRemove={this.props.onClickRemove}
                        variationPhotos={this.props.variationPhotos}
                      />
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div className="text-right mt-5 pb-5 d-none d-md-block">
          <button className="btn border-btn mr-3 btn-w-115">Save</button>
          <button className="btn border-btn border-btn-gray">
            Save and preview
          </button>
        </div>
      </div>
    );
  }
}

export default VariationCombinations;
