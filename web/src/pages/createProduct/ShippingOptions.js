//@flow

import {days} from "core/utils/dummyDate";
import React, {Component} from "react";
import HorizontalInput from "../userSettings/components/horizontalInput";
import HorizontalSelectOption from "../userSettings/components/horizontalSelectOption";
import {countryList} from "../../utils/countryList";
import HorizontalSingleSelectInput from "../userSettings/components/HorizontalSingleSelectInput";
import {getThirtyBusinessDays} from "../../utils/misc";

type Props = {
  name: string
}

class ShippingOptions extends Component<Props> {
  state = {
    "country": "",
    "processingTime": 0,
    "carrier": "",
    "carrierName": "",
    "templateName": "",
    "templateId": "",
    "int_shipping": "International shipping",
    "freeShipping": false,
    "deliveryTimeFrom": 0,
    "deliveryTimeTo": 0,
    "oneItemCost": 0,
    "additionalItemCost": 0
  };

  getState = () => {
    return this.state;
  }

  onChange = (value, name) => {
    this.props.addShippingData(value, name);
  };

  onInputChange = (name, value) => {
    this.setState({
      [name]: value
    });

    if (name === 'int_shipping') {
      const i = value === 'International shipping';
      this.props.onUpdateIntlShippingOption(i);
    }

  };

  handleInputChange = (name, value) => {
    this.setState({
      [name]: value
    });
  };

  render() {
    const { countries, carriers } = this.props;
    const { freeShipping } = this.state;

    return (
      <div>
        <div className="row mb-4 pb-3 pb-md-2">
          <div className="col-12 col-md-7">
            <div className="md-title text-uppercase mb-4 mb-md-1">
              SHIPPING OPTIONS
            </div>
            <div className="sm-text-fw4-g">
              Fill out your shipping options for this listing. You can keep
              these options specific to this listing, or save them as a shipping
              profile to apply them to future listings.
            </div>
          </div>
          <div className="col-12 col-md-1"></div>
          <div className="col-12 col-md-4 mb-4 d-none d-md-block">
            <div className="single-group hide-label w-100">
              <HorizontalSelectOption
                formInputClass="form-input position-relative"
                label=""
                name="templete"
                errorText=""
                isRequired={false}
                imageRequired={false}
                selectedText="Choose shipping template"
                options={["Template1", "Template2"]}
                onChange={this.onInputChange}
              />
            </div>
          </div>
        </div>
        <div className="row mb-5">
          <div className="col-12 col-md-4 mb-4">
            <div className="form-group">
              <div className="single-group label-mb">
                <HorizontalSingleSelectInput
                  formInputClass="form-input position-relative"
                  label="Shipping country"
                  name="country"
                  errorText="Enter the country"
                  isRequired={true}
                  imageRequired={true}
                  selectedText="Select Countries"
                  options={countryList}
                  nameKey='code'
                  labelkey='name'
                  onChange={this.onInputChange}
                />
              </div>
              {/* <div className="font-weight-bold text-uppercase mb-3">
                Shipping country <sup>*</sup>
              </div>
              <SingleSelect
                placeholder="Select Countries"
                options={countries}
                name="name"
                id="shippingCountry"
                onChange={this.onChange}
              /> */}
              <div className="x-sm-text-fw4-g pt-3">
                The country you’re shipping from
              </div>
            </div>
          </div>

          <div className="col-12 col-md-4 mb-4">
            <div className="form-group">
              <div className="single-group label-mb">
                <HorizontalSingleSelectInput
                  formInputClass="form-input position-relative"
                  label="Processing time"
                  nameKey='_id'
                  labelkey='name'
                  name="processingTime"
                  errorText="Processing time"
                  isRequired={true}
                  imageRequired={false}
                  placeholder="Select"
                  options={getThirtyBusinessDays()}
                  onChange={this.onInputChange}
                />
              </div>
              {/* <div className="font-weight-bold text-uppercase mb-3">
                Processing time <sup>*</sup>
              </div>
              <SingleSelect
                placeholder="Select Proccessing time"
                options={this.state.days}
                name="day"
                id="processingTime"
                onChange={this.onChange}
                additionalData="Business days"
              /> */}
              <div className="x-sm-text-fw4-g pt-3">
                Once purchased, how long does it take you to process an item?
              </div>
            </div>
          </div>

          <div className="col-12 col-md-4 mb-4">
            <div className="form-group">
              <div className="single-group label-mb">
                <HorizontalSingleSelectInput
                  formInputClass="form-input position-relative"
                  label="Shipping carrier"
                  name="carrier"
                  errorText="Enter Shipping carrier"
                  isRequired={true}
                  imageRequired={false}
                  nameKey='_id'
                  labelkey='name'
                  placeholder="Select carrier"
                  options={carriers}
                  onChange={this.onInputChange}
                />
              </div>
              {/* <div className="font-weight-bold text-uppercase mb-3">
                Shipping carrier <sup>*</sup>
              </div>
              <SingleSelect
                placeholder="Select shipping carrier"
                options={carriers}
                name="name"
                id="shippingCarrier"
                onChange={this.onChange}
              /> */}
            </div>
          </div>

          <div className="col-12 col-md-4 mb-4">
            <div className="form-group">
              <div className="font-weight-bold text-uppercase mb-2 pb-1">
                Delivery time
              </div>
              <div className="row">
                <div className="col-6">
                  <div className="single-group label-mb hide-label">
                    <HorizontalSingleSelectInput
                      formInputClass="form-input position-relative"
                      label="Processing time"
                      nameKey='_id'
                      labelkey='name'
                      name="deliveryTimeFrom"
                      errorText="Processing time"
                      isRequired={true}
                      imageRequired={false}
                      placeholder="Select"
                      options={getThirtyBusinessDays()}
                      onChange={this.onInputChange}
                    />
                  </div>
                  {/* <SingleSelect
                    placeholder="Select Delivery time"
                    options={this.state.days}
                    name="day"
                    id="deliveryTimeFrom"
                    onChange={this.onChange}
                    additionalData="Business days"
                  /> */}
                </div>
                <div className="col-6">
                  <div className="single-group label-mb hide-label">
                    <HorizontalSingleSelectInput
                      formInputClass="form-input position-relative"
                      label="Processing time"
                      nameKey='_id'
                      labelkey='name'
                      name="deliveryTimeTo"
                      errorText="Processing time"
                      isRequired={true}
                      imageRequired={false}
                      placeholder="Select"
                      options={getThirtyBusinessDays()}
                      onChange={this.onInputChange}
                    />
                  </div>
                  {/* <SingleSelect
                    placeholder="Select Delivery time "
                    options={this.state.days}
                    name="day"
                    id="deliveryTimeTo"
                    onChange={this.onChange}
                    additionalData="Business days"
                  /> */}
                </div>
              </div>
              <div className="x-sm-text-fw4-g mt-3">Business days</div>
            </div>
          </div>
          <div className="col-12 col-md-4 mb-4">
            <div className="row">
              <div className="col-6">
                <div className="single-group label-mb">
                  <HorizontalInput
                    formInputClass="form-input position-relative"
                    label="Shipping price"
                    type="number"
                    placeholder="Your Shipping price"
                    name="oneItemCost"
                    errorText="Please enter your Shipping price"
                    isRequired={true}
                    onChange={this.handleInputChange}
                  />
                </div>
                {/* <div className="font-weight-bold text-uppercase mb-3">
                Shipping price <sup>*</sup>
              </div>
              <InputWithoutLabel
                placeholder="Enter shipping price"
                type="number"
                name="shippingPrice"
                id="shippingPrice"
                onChange={this.onChange}
              /> */}
              </div>
              <div className="col-6">
                <div className="single-group label-mb">
                  <HorizontalInput
                    formInputClass="form-input position-relative"
                    label=" Additional item"
                    type="number"
                    placeholder="Your Additional item"
                    name="additionalItemCost"
                    id="shippingAdditionalItem"
                    errorText=""
                    isRequired={false}
                    onChange={this.handleInputChange}
                  />
                </div>
              </div>
            </div>
          </div>

          <div className="col-12 col-md-4 mb-4 d-flex align-items-center">
            <label className="ctm-container d-inline-block mb-4">
              Free shipping
              <input type="checkbox" name="selectAll" checked={freeShipping} onChange={(e) => this.setState({freeShipping: e.target.checked})}/>
              <span className="checkmark" />
            </label>
          </div>
          <div className="col-12 col-md-4 mb-4">
            <div className="single-group label-mb">
              <HorizontalSelectOption
                formInputClass="form-input position-relative"
                label="International shipping"
                name="int_shipping"
                errorText=""
                isRequired={false}
                imageRequired={false}
                selectedText="Choose international shipping template"
                options={[
                  "International shipping",
                  "No International shipping"
                ]}
                onChange={this.onInputChange}
              />
            </div>
          </div>
          <div className="col-12 col-md-4 mb-4">
            <div className="single-group label-mb">
              <HorizontalInput
                formInputClass="form-input position-relative form-input-append"
                label="Template name"
                type="text"
                placeholder="Enter Template name"
                name="templateName"
                errorText=""
                appendText="Save"
                isRequired={false}
                onChange={this.onInputChange}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ShippingOptions;
