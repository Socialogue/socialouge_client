import React from 'react'
import { connect } from 'react-redux'
import { selectPosts } from 'core/selectors/posts'
import { updateShop } from 'core/actions/accountActionCreators'
import Header from './../home/Header/Header'
import ExcB from './../../images/icons/excIcon'
import ArrowLeft from './../../images/icons/arrowLeft'
import parsePhoneNumber from 'libphonenumber-js'
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import { selectUser } from 'core/selectors/user'
import HorizontalInput from '../../pages/userSettings/components/horizontalInput/index'
import HorizontalSelectOption from '../userSettings/components/horizontalSelectOption'
import { errorAlert, successAlert } from '../../utils/alerts'
import { selectErrorMsg, selectSuccessMsg } from 'core/selectors/shops'
import CustomSelectOption from '../userSettings/components/horizontalSelectOption/customSelectOption'
import { Link } from 'react-router-dom'
import {
  selectActiveShop,
  updateShopError,
  updateShopSuccess,
} from 'core/selectors/account'
import { fetchShop } from 'core/actions/shopActionCreators'

class CreateShop extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      companyEmail: props.activeShop.companyEmail,
      companyId: props.activeShop.companyId,
      companyName: props.activeShop.companyName,
      companyVat: props.activeShop.companyVat,
      countrycode: props.activeShop.countryCode,
      itemforsell: props.activeShop.itemForSell,
      phone: props.activeShop.phone,
      registeredCompanyAddress: props.activeShop.registeredCompanyAddress,
      shopName: props.activeShop.shopName,
      socialMedia1: props.activeShop.socialMedia1,
      socialMedia2: props.activeShop.socialMedia2,
      userId: props.activeShop.userId,
      wheredidyouhere: props.activeShop.whereDidYouHere,
      shopId: props.activeShop._id,
      changeInput: {},
      count: 0,
    }
  }

  onInputChange = (name, value) => {
    this.inputChangeCounter(name, value)
    this.setState({
      [name]: value,
    })
  }
  handlePhone = (e) => {
    const name = e.target.name
    const value = e.target.value
    this.inputChangeCounter(name, value)
    this.setState({
      [name]: value,
    })
  }
  handleCountryCode = (name, value) => {
    this.inputChangeCounter(name, value)
    this.setState({
      [name]: value,
    })
  }

  inputChangeCounter = (name, value) => {
    if (name == 'wheredidyouhere') {
      name = 'whereDidYouHere'
    }
    if (name == 'itemforsell') {
      name = 'itemForSell'
    }
    if (name == 'countrycode') {
      name = 'countryCode'
    }
    if (this.props.activeShop[name] != value) {
      if (this.state.changeInput.hasOwnProperty(name)) {
        var stateObj = this.state.changeInput
        stateObj[name] = 1
        this.setState({ stateObj })
      } else {
        var stateObj = this.state.changeInput
        stateObj[name] = 1
        this.setState({ stateObj })
      }
    } else {
      var stateObj = this.state.changeInput
      stateObj[name] = 0
      this.setState({ stateObj })
    }
    if (this.state.changeInput) {
      const inp = this.state.changeInput
      let count = 0
      for (let key in inp) {
        if (inp[key] == 1) {
          count++
        }
      }
      this.setState({
        count: count,
      })
    }
  }

  componentDidUpdate = (prevProps) => {
    if (prevProps.updateShopSuccess !== this.props.updateShopSuccess) {
      if (this.props.updateShopSuccess !== '') {
        this.props.fetchShop()
        successAlert(this.props.updateShopSuccess)
      }
    }
    if (prevProps.updateShopError !== this.props.updateShopError) {
      if (this.props.updateShopError !== '') {
        errorAlert(this.props.updateShopError)
      }
    }
  }

  onSubmit = async (e) => {
    if (
      this.state.companyEmail == '' ||
      this.state.companyId == '' ||
      this.state.companyName == '' ||
      // this.state.countrycode == '' ||
      this.state.itemforsell == '' ||
      this.state.phone == '' ||
      this.state.registeredCompanyAddress == '' ||
      this.state.shopName == '' ||
      this.state.socialMedia1 == '' ||
      this.state.wheredidyouhere == '' ||
      this.state.userId == ''
    ) {
      errorAlert('Please fill up form properly.')
      return
    }
    try {
      // TODO need to change local storage shop and active profile data after updating shop data
      await this.props.updateShop(this.state)
    } catch (e) {
      console.log('catch', e)
    }
    return true
  }

  render() {
    return (
      <div>
        <Header />
        <div className="form-module-cnt-1 mt-5 pb-0 pb-md-5 pt-0 pt-md-5 mx-auto px-4">
          <div className="font-weight-bold text-uppercase mb-5">
            Update shop
          </div>
          <div className="apply-form">
            <div className="mb-5">
              <HorizontalInput
                formInputClass="form-input position-relative"
                label="your shop name"
                errorText="Please enter Your shop name"
                isRequired={true}
                type="text"
                className="form-control custom-input"
                placeholder="Enter your shop name"
                name="shopName"
                value={this.state.shopName}
                onChange={this.onInputChange}
              />
            </div>
            <div className="mb-5">
              <HorizontalSelectOption
                formInputClass="form-input position-relative"
                label="what items do you sell"
                name="itemforsell"
                errorText=""
                isRequired={true}
                imageRequired={false}
                selectedText={this.state.itemforsell}
                options={['My own design', 'others design']}
                onChange={this.onInputChange}
              />
            </div>
            <div className="form-group row align-items-center mr-0 mb-5">
              <label className="x-sm-text-fw6 text-uppercase col-12 col-md-5 pr-0 mb-2 mb-md-0">
                Your Contact Number<sup>*</sup>
              </label>
              <div className="input-group sc-input apply-sc col-12 col-md-7 pr-0">
                <span className="prepend-input" style={{ marginLeft: '15px' }}>
                  <ExcB />
                </span>
                <PhoneInput
                  country={'lt'}
                  inputStyle={{
                    border: '1px solid black',
                    borderTop: 'none',
                    borderRight: 'none',
                    width: '390px',
                    marginLeft: '5px',
                    borderRadius: '0',
                    outerWidth: '100%',
                  }}
                  className="form-control custom-input"
                  buttonStyle={{
                    border: 'none',
                    backgroundColor: 'white',
                    borderRight: '1px solid black',
                    borderBottom: '1px solid black',
                    borderRadius: '0',
                  }}
                  value={this.state.phone}
                  placeholder="Enter your phone number"
                  onChange={(phone) => this.setState({ phone })}
                />
              </div>
            </div>
            <div className="mb-5">
              <HorizontalInput
                formInputClass="form-input position-relative"
                label="Company Name"
                errorText="Please enter Your company name"
                isRequired={true}
                type="text"
                className="form-control custom-input"
                placeholder=""
                name="companyName"
                value={this.state.companyName}
                onChange={this.onInputChange}
              />
            </div>
            <div className="mb-5">
              <HorizontalInput
                formInputClass="form-input position-relative"
                label="Company Id"
                errorText="Please enter Your company Id"
                isRequired={true}
                type="text"
                className="form-control custom-input"
                placeholder="DE12823"
                name="companyId"
                value={this.state.companyId}
                onChange={this.onInputChange}
              />
            </div>
            <div className="mb-5">
              <HorizontalInput
                formInputClass="form-input position-relative"
                label="Company Vat"
                type="text"
                className="form-control custom-input"
                placeholder=""
                name="companyVat"
                value={this.state.companyVat}
                onChange={this.onInputChange}
              />
            </div>
            <div className="mb-5">
              <HorizontalInput
                formInputClass="form-input position-relative"
                label="Company Email Address"
                errorText="Please enter Your company email address"
                isRequired={true}
                type="text"
                className="form-control custom-input"
                placeholder="Name@domain.com"
                name="companyEmail"
                value={this.state.companyEmail}
                onChange={this.onInputChange}
              />
            </div>
            <div className="mb-5">
              <HorizontalInput
                formInputClass="form-input position-relative"
                label="Registered Company Address"
                errorText="Please enter Your registered company address"
                isRequired={true}
                type="text"
                className="form-control custom-input"
                placeholder="Little George St, Westminstr, London"
                name="registeredCompanyAddress"
                value={this.state.registeredCompanyAddress}
                onChange={this.onInputChange}
              />
            </div>
            <div className="mb-5">
              <HorizontalInput
                formInputClass="form-input position-relative"
                label="social media"
                errorText="Please enter a link"
                isRequired={true}
                type="text"
                className="form-control custom-input"
                placeholder="Enter Links"
                name="socialMedia1"
                value={this.state.socialMedia1}
                onChange={this.onInputChange}
              />
            </div>
            <div className="mb-5">
              <HorizontalInput
                formInputClass="form-input position-relative"
                label=""
                type="text"
                className="form-control custom-input"
                placeholder="Enter Links"
                name="socialMedia2"
                value={this.state.socialMedia2}
                onChange={this.onInputChange}
              />
            </div>
            <div className="mb-5">
              <HorizontalSelectOption
                formInputClass="form-input position-relative"
                label="Where did you here about us"
                name="wheredidyouhere"
                errorText=""
                isRequired={true}
                imageRequired={false}
                selectedText={this.state.wheredidyouhere}
                options={['Google', 'Facebook', 'Linkedin']}
                onChange={this.onInputChange}
              />
            </div>
          </div>
        </div>

        <div className="border-top-1 p-4 md-border-0 mb-5 mb-md-0">
          <div className="max-body-width">
            <div className="save-btn-cnt mx-auto">
              <div className="row">
                <div className="text-left col-md-6 d-none d-md-block">
                  <button className="btn border-btn border-0 btn-w-115 text-uppercase">
                    <span className="color-orange">{this.state.count}</span>{' '}
                    Change
                  </button>
                </div>
                <div className="col-md-6 text-right">
                  <Link
                    to={'seller/products-list'}
                    className="btn border-btn border-0 btn-w-115"
                  >
                    <span className="pr-2">
                      <ArrowLeft />
                    </span>{' '}
                    Cancel
                  </Link>
                  <button
                    className="btn border-btn btn-w-115"
                    onClick={this.onSubmit}
                  >
                    Save
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    posts: selectPosts(state),
    userId: selectUser(state),
    successMsg: selectSuccessMsg(state),
    errorMsg: selectErrorMsg(state),
    activeShop: selectActiveShop(state),
    updateShopError: updateShopError(state),
    updateShopSuccess: updateShopSuccess(state),
  }),
  {
    updateShop,
    fetchShop,
  },
)(CreateShop)
