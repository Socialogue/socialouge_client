import React from 'react'
import { connect } from 'react-redux'
import { selectLoadedStatus, selectPostsLoading } from 'core/selectors/posts'

import { selectOutfitsData, selectOutfitsMsg } from 'core/selectors/outfits'

import {
  selectFavoriteProducts,
  selectTotalFavorites,
} from 'core/selectors/products'
import {
  fetchFavoriteProducts,
  reactToProduct,
} from 'core/actions/productActionCreators'
import { fetchOutfits } from 'core/actions/outfitsActionCreators'
import Header from './../../../home/Header/Header'
import Footer from './../../../home/Footer/Footer'

// user profile
import UserInfo from './../../common/userInfo/userInfo'
import Tab from './../../common/tab/tab'
import Favorite from './../../common/favorite/favorite'
import './../../index.scss'
import { errorAlert } from '../../../../utils/alerts'
import { selectUser } from 'core/selectors/user'
import FixedFooter from '../../../Common/fixedFooter'
import {
  ROUTE_PRODUCT_DETAILS,
  ROUTE_USER_OWN_PROFILE_POST,
  ROUTE_USER_PROFILE_POST,
} from '../../../../routes/constants'

class UserOwnProfileFavorite extends React.Component {
  constructor() {
    super()
    this.state = {
      loading: false,
      favoriteProducts: [],
      count: 10,
      start: 0,
      total: null,
      setData: false,
    }
  }

  componentDidMount = () => {
    try {
      this.props.fetchFavoriteProducts(
        this.props.user._id,
        this.state.count,
        this.state.start,
      )

      // this.props.fetchOutfits();
    } catch (err) {
      console.log(err)
      errorAlert('Something went wrong!')
    }
  }

  componentDidUpdate = (prevProps) => {
    if (
      JSON.stringify(prevProps.favoriteProducts) !==
        JSON.stringify(this.props.favoriteProducts) ||
      !this.state.setData
    ) {
      if (this.props.favoriteProducts) {
        this.setState({
          favoriteProducts: this.state.favoriteProducts.concat(
            this.props.favoriteProducts,
          ),
          total: this.props.favoriteCount,
          setData: true,
        })
      }
    }

    // if (prevProps.outfits !== this.props.outfits) {
    //   if (this.props.outfits) {
    //   }
    // }
  }

  reactToProduct = (productId) => {
    let favProducts = [...this.state.favoriteProducts]
    let filteredProducts = favProducts.filter(
      (product) => product.productId._id !== productId,
    )
    this.setState({
      favoriteProducts: filteredProducts,
      total: this.state.total - 1,
    })
    this.props.reactToProduct(this.props.user._id, productId)
  }

  fetchMoreData = () => {
    this.setState({ start: this.state.start + this.state.count }, () => {
      this.props.fetchFavoriteProducts(
        this.props.user._id,
        this.state.count,
        this.state.start,
      )
    })
  }

  // handler for goto user profile...
  gotoProfile = (userId) => {
    if (userId == this.props.user._id) {
      this.props.history.push(ROUTE_USER_OWN_PROFILE_POST)
    } else {
      const route = ROUTE_USER_PROFILE_POST.replace(':id', userId)
      this.props.history.push(route)
    }
  }

  // open product in details page...
  viewProduct = (slug, productId) => {
    this.props.history.push(
      ROUTE_PRODUCT_DETAILS.replace(':slug', slug).replace(
        ':productId',
        productId,
      ),
    )
  }

  render() {
    return (
      <div>
        <Header />
        <div className="user-profile-cnt mt-4 mt-md-5">
          <UserInfo isOwn={true} user={this.props.user} />
          <Tab url={this.props.location.pathname} />
          <Favorite
            data={this.state.favoriteProducts}
            outfits={this.props.outfits}
            favorite={this.reactToProduct}
            fetchMoreData={this.fetchMoreData}
            total={this.state.total}
            isOwn={true}
            loadedStatus={this.props.loadedStatus}
            gotoProfile={this.gotoProfile}
            viewProduct={this.viewProduct}
          />
        </div>
        <Footer />
        <div className="ab-mf px-4 mb-4 d-block d-md-none">
          <FixedFooter />
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    favoriteProducts: selectFavoriteProducts(state),
    favoriteCount: selectTotalFavorites(state),
    user: selectUser(state),
    postLoading: selectPostsLoading(state),
    loadedStatus: selectLoadedStatus(state),
    outfits: selectOutfitsData(state),
    outfitsErrorMsg: selectOutfitsMsg(state),
  }),
  {
    fetchFavoriteProducts,
    fetchOutfits,
    reactToProduct,
  },
)(UserOwnProfileFavorite)
