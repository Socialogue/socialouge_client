import React from 'react'
import { connect } from 'react-redux'
import { selectPosts, selectPostsLoading } from 'core/selectors/posts'
import { fetchPosts } from 'core/actions/postActionCreators'
import Header from './../../../home/Header/Header'
import FilterMenu from './../../../home/FilterMenu/FilterMenu'

// user profile
import UserInfo from './../../common/userInfo/userInfo'
import Tab from './../../common/tab/tab'
import Post from './../../common/post/post'
import './../../index.scss'
import { errorAlert } from '../../../../utils/alerts'
import { selectUser } from 'core/selectors/user'
import SortView from '../../../home/sortView'
import FixedFooter from '../../../Common/fixedFooter'
import PostModal from '../../../modal/postModal'
import { addComment } from 'core/actions/commentActionCreators'
import {
  ROUTE_USER_OWN_PROFILE_POST,
  ROUTE_USER_PROFILE_POST,
} from '../../../../routes/constants'

class UserOwnProfilePost extends React.Component {
  constructor() {
    super()
    this.state = {
      loading: false,
      allPosts: [],
      count: 2,
      start: 0,
      setData: false,
      postModalOpen: false,
      post: {},
    }
  }

  componentDidMount = () => {
    try {
      this.props.fetchPosts(
        this.props.user._id,
        this.state.count,
        this.state.start,
      )
    } catch (e) {
      console.log('catch', e)
      errorAlert('Something went wrong!')
    }
    return true
  }

  componentDidUpdate = (prevProps) => {
    if (
      JSON.stringify(prevProps.posts) !== JSON.stringify(this.props.posts) ||
      !this.state.setData
    ) {
      if (this.props.posts) {
        this.setState({
          allPosts: this.state.allPosts.concat(this.props.posts),
          setData: true,
        })
      }
    }
  }

  fetchMoreData = () => {
    this.setState({ start: this.state.start + this.state.count }, () => {
      this.props.fetchPosts(
        this.props.user._id,
        this.state.count,
        this.state.start,
      )
    })
  }

  // handler for goto post details as popup...
  openPostDetailsPopup = (postId) => {
    //TODO have some issue, need test and fix
    // let posts = [...this.state.allPosts]
    // const post = posts.filter((item) => item._id === postId)
    // console.log('post: ', post[0])
    // document.body.classList.add('modal-open')
    // this.setState({ postModalOpen: true, post: post[0] })
  }

  // handler for closing post details popup...
  closePostDetailsPopup = () => {
    document.body.classList.remove('modal-open')
    this.setState({ postModalOpen: false, post: {} })
  }

  // handler for adding a comment to post... passing as props
  addingComment = (commentText, postId) => {
    const userId = this.props.user._id
    // if not have userId then show some notification ...
    if (!userId) {
      return errorAlert('Login first please!')
    }
    this.props.addComment(commentText, postId, userId)
  }

  // handler for goto user profile...
  gotoProfile = (userId) => {
    if (userId == this.props.user._id) {
      this.props.history.push(ROUTE_USER_OWN_PROFILE_POST)
    } else {
      const route = ROUTE_USER_PROFILE_POST.replace(':id', userId)
      this.props.history.push(route)
    }
  }

  render() {
    const { postModalOpen, post } = this.state
    return (
      <div>
        <Header history={this.props.history} />
        <div className="user-profile-cnt mt-4 mt-md-5 pt-4 pt-md-0">
          <UserInfo isOwn={true} user={this.props.user} />
          <Tab url={this.props.location.pathname} />
          <Post
            data={this.state.allPosts}
            fetchMoreData={this.fetchMoreData}
            isOwn={true}
            openPostDetails={this.openPostDetailsPopup}
          />
          <SortView />
          {postModalOpen === true && (
            <PostModal
              post={post}
              close={this.closePostDetailsPopup}
              gotoProfile={this.gotoProfile}
              addingComment={this.addingComment}
            />
          )}
        </div>
        <FilterMenu />
        <FixedFooter />
      </div>
    )
  }
}

export default connect(
  (state) => ({
    posts: selectPosts(state),
    postLoading: selectPostsLoading(state),
    user: selectUser(state),
  }),
  {
    fetchPosts,
    addComment,
  },
)(UserOwnProfilePost)
