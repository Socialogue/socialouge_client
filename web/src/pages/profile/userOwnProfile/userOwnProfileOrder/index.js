import React from "react";
import { connect } from "react-redux";
import { selectPosts } from "core/selectors/posts";
import { fetchPosts } from "core/actions/postActionCreators";
import Header from "./../../../home/Header/Header";
import Footer from "./../../../home/Footer/Footer";

// user profile
import UserInfo from "./../../common/userInfo/userInfo";
import Tab from "./../../common/tab/tab";
import Orders from "./../../common/orders/orders";
import "./../../index.scss";
import FixedFooter from "../../../Common/fixedFooter";
import { selectUser } from "core/selectors/user";

class UserOwnProfileOrder extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: false,
    };
  }

  onSubmit = async (e) => {
    try {
      await this.props.fetchPosts();
      this.setState({
        loading: true,
      });
    } catch (e) {
      console.log("catch", e);
    } finally {
      console.log("finally");
    }
    return true;
  };

  render() {
    return (
      <div>
        <Header />
        <div className="user-profile-cnt mx-auto my-5">
          <UserInfo isOwn={true} user={this.props.user} />
          <Tab url={this.props.location.pathname} />
          <Orders />
        </div>
        <Footer />
        <div className="ab-mf px-4 mb-4 d-block d-md-none">
          <FixedFooter />
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    posts: selectPosts(state),
    user: selectUser(state),
  }),
  {
    fetchPosts,
  }
)(UserOwnProfileOrder);
