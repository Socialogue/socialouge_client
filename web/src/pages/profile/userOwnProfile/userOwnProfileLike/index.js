import React from 'react'
import { connect } from 'react-redux'
import {
  selectCount,
  selectLikedPosts,
  selectPostsLoading,
} from 'core/selectors/posts'
import { fetchLikedPosts, reactToPost } from 'core/actions/postActionCreators'
import Header from './../../../home/Header/Header'
import FilterMenu from './../../../home/FilterMenu/FilterMenu'

// user profile
import UserInfo from './../../common/userInfo/userInfo'
import Tab from './../../common/tab/tab'
import Like from './../../common/like/like'
import './../../index.scss'
import { selectUser } from 'core/selectors/user'
import { errorAlert } from '../../../../utils/alerts'
import SortView from '../../../home/sortView'
import FixedFooter from '../../../Common/fixedFooter'
import {
  ROUTE_USER_OWN_PROFILE_POST,
  ROUTE_USER_PROFILE_POST,
} from '../../../../routes/constants'

class UserOwnProfileLike extends React.Component {
  constructor() {
    super()
    this.state = {
      loading: false,
      likedPosts: [],
      count: 10,
      start: 0,
      total: null,
      setData: false,
    }
  }

  componentDidMount = () => {
    try {
      this.props.fetchLikedPosts(
        this.props.user._id,
        this.state.count,
        this.state.start,
      )
      // this.setState(
      //   {
      //     loading: true,
      //     likedPosts: this.props.likedPosts,
      //   },
      //   () => {
      //     console.log(
      //       'this.props.likedPosts: ',
      //       this.props.likedPosts,
      //       this.state.likedPosts,
      //     )
      //   },
      // )
    } catch (e) {
      console.log('catch', e)
      errorAlert('Something went wrong!')
    }
    return true
  }

  componentDidUpdate = (prevProps) => {
    console.log('like: ', prevProps.likedPosts, this.props.likedPosts)
    if (
      JSON.stringify(prevProps.likedPosts) !==
        JSON.stringify(this.props.likedPosts) ||
      !this.state.setData
    ) {
      if (this.props.likedPosts) {
        this.setState({
          likedPosts: this.state.likedPosts.concat(this.props.likedPosts),
          total: this.props.total,
          setData: true,
        })
      }
    }
  }

  reactToPost = (postId) => {
    let likedPosts = [...this.state.likedPosts]
    let filteredPost = likedPosts.filter((post) => post.postId._id !== postId)
    this.setState({ likedPosts: filteredPost, total: this.state.total - 1 })
    this.props.reactToPost(this.props.user._id, postId)
  }

  onSubmit = async (e) => {
    try {
      await this.props.fetchPosts()
      this.setState({
        loading: true,
      })
    } catch (e) {
      console.log('catch', e)
    } finally {
      console.log('finally')
    }
    return true
  }

  fetchMoreData = () => {
    this.setState({ start: this.state.start + this.state.count })
    this.props.fetchLikedPosts(
      this.props.user._id,
      this.state.count,
      this.state.start,
    )
  }

  // handler for goto user profile...
  gotoProfile = (userId) => {
    if (userId == this.props.user._id) {
      this.props.history.push(ROUTE_USER_OWN_PROFILE_POST)
    } else {
      const route = ROUTE_USER_PROFILE_POST.replace(':id', userId)
      this.props.history.push(route)
    }
  }

  render() {
    return (
      <div>
        <Header />
        <div className="user-profile-cnt mx-auto my-5">
          <UserInfo isOwn={true} user={this.props.user} />
          <Tab url={this.props.location.pathname} />
          <Like
            data={this.state.likedPosts}
            reactToPost={this.reactToPost}
            fetchMoreData={this.fetchMoreData}
            count={this.state.total}
            gotoProfile={this.gotoProfile}
          />
          <SortView />
        </div>
        <FilterMenu />
        <FixedFooter />
      </div>
    )
  }
}

export default connect(
  (state) => ({
    likedPosts: selectLikedPosts(state),
    total: selectCount(state),
    user: selectUser(state),
    postLoading: selectPostsLoading(state),
  }),
  {
    fetchLikedPosts,
    reactToPost,
  },
)(UserOwnProfileLike)
