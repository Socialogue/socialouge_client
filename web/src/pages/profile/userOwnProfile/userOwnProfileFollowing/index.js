import React from 'react'
import { connect } from 'react-redux'
import { selectLikedPosts, selectPosts } from 'core/selectors/posts'
import { selectFollowings, selectFollowingsCount } from 'core/selectors/shops'
import { fetchFollowings } from 'core/actions/shopActionCreators'
import Header from './../../../home/Header/Header'
import Footer from './../../../home/Footer/Footer'

// user profile
import UserInfo from './../../common/userInfo/userInfo'
import Tab from './../../common/tab/tab'
import Following from './../../common/following/following'
import './../../index.scss'
import { selectUser } from 'core/selectors/user'
import { errorAlert } from '../../../../utils/alerts'
import FixedFooter from '../../../Common/fixedFooter'
import {
  ROUTE_PRODUCT_DETAILS,
  ROUTE_SHOP_PROFILE_PRODUCTS,
  ROUTE_USER_OWN_PROFILE_POST,
  ROUTE_USER_PROFILE_POST,
} from '../../../../routes/constants'

class UserOwnProfileFollowing extends React.Component {
  constructor() {
    super()
    this.state = {
      loading: false,
      followingsCount: null,
      followings: [],
      count: 10,
      start: 0,
      setData: false,
    }
  }

  componentDidMount = () => {
    try {
      this.props.fetchFollowings(
        this.props.user._id,
        this.state.start,
        this.state.count,
      )
    } catch (err) {
      console.log(err)
      errorAlert('Something went wrong.')
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.followingsCount !== this.props.followingsCount) {
      if (this.props.followingsCount) {
        this.setState({
          followingsCount: this.props.followingsCount,
        })
      }
    }
    if (
      JSON.stringify(prevProps.followings) !==
        JSON.stringify(this.props.followings) ||
      !this.state.setData
    ) {
      if (this.props.followings.length > 0) {
        this.setState({
          followings: this.state.followings.concat(this.props.followings),
          setData: true,
        })
      }
    }
  }

  // handler for infinite scroll ...

  fetchMoreFollowings = () => {
    this.setState({ start: this.state.start + this.state.count }, () => {
      this.props.fetchFollowings(
        this.props.user._id,
        this.state.start,
        this.state.count,
      )
    })
  }
  gotoProfile = (userId) => {
    if (userId == this.props.user._id) {
      this.props.history.push(ROUTE_USER_OWN_PROFILE_POST)
    } else {
      const route = ROUTE_USER_PROFILE_POST.replace(':id', userId)
      this.props.history.push(route)
    }
  }

  shopProfile = (shopId) => {
    const route = ROUTE_SHOP_PROFILE_PRODUCTS.replace(':id', shopId)
    this.props.history.push(route)
  }

  // handler for go to product details page...
  viewProduct = (slug, productId) => {
    this.props.history.push(
      ROUTE_PRODUCT_DETAILS.replace(':slug', slug).replace(
        ':productId',
        productId,
      ),
    )
  }

  render() {
    return (
      <div>
        <Header />
        <div className="user-profile-cnt mx-auto my-5">
          <UserInfo isOwn={true} user={this.props.user} />
          <Tab url={this.props.location.pathname} />
          <Following
            data={this.state.followings}
            fetchMoreFollowings={this.fetchMoreFollowings}
            followingsCount={this.props.followingsCount}
            viewProduct={this.viewProduct}
            gotoProfile={this.gotoProfile}
            shopProfile={this.shopProfile}
          />
        </div>
        <Footer />
        <div className="ab-mf px-4 mb-4 d-block d-md-none">
          <FixedFooter />
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    followings: selectFollowings(state),
    followingsCount: selectFollowingsCount(state),
    user: selectUser(state),
  }),
  {
    fetchFollowings,
  },
)(UserOwnProfileFollowing)
