import React from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
import {
  ROUTE_USER_OWN_PROFILE_FOLLOWING,
  ROUTE_USER_OWN_PROFILE_LIKE,
  ROUTE_USER_OWN_PROFILE_ORDER,
  ROUTE_USER_OWN_PROFILE_POST,
  ROUTE_USER_OWN_PROFILE_REVIEW,
  ROUTE_USER_PROFILE_FAVORITE,
  ROUTE_USER_PROFILE_FOLLOWING,
  ROUTE_USER_PROFILE_POST,
  ROUTE_USER_PROFILE_REVIEW,
  ROUTE_USER_OWN_PROFILE_FAVORITE
} from "../../../../routes/constants";

const Tab = props => {
  console.log(props.match);
  return (
    <div>
      <ul className="nav nav-fill tab-nav">
        <li className="nav-item">
          {!props.match.params.id && (
            <Link
              className={
                props.match.path == ROUTE_USER_OWN_PROFILE_POST
                  ? "nav-link sm-text-fw6-g text-uppercase active"
                  : "nav-link sm-text-fw6-g text-uppercase"
              }
              to={ROUTE_USER_OWN_PROFILE_POST}
            >
              Post
            </Link>
          )}
          {props.match.params.id && (
            <Link
              className={
                props.match.path == ROUTE_USER_PROFILE_POST
                  ? "nav-link sm-text-fw6-g text-uppercase active"
                  : "nav-link sm-text-fw6-g text-uppercase"
              }
              to={ROUTE_USER_PROFILE_POST.replace(":id", props.match.params.id)}
            >
              Post
            </Link>
          )}
        </li>
        {!props.match.params.id && (
          <li className="nav-item">
            <Link
              className={
                props.match.path == ROUTE_USER_OWN_PROFILE_LIKE
                  ? "nav-link sm-text-fw6-g text-uppercase active"
                  : "nav-link sm-text-fw6-g text-uppercase"
              }
              to={ROUTE_USER_OWN_PROFILE_LIKE}
            >
              Liked
            </Link>
          </li>
        )}
        <li className="nav-item">
          {!props.match.params.id && (
            <Link
              className={
                props.match.path == ROUTE_USER_OWN_PROFILE_FAVORITE
                  ? "nav-link sm-text-fw6-g text-uppercase active"
                  : "nav-link sm-text-fw6-g text-uppercase"
              }
              to={ROUTE_USER_OWN_PROFILE_FAVORITE}
            >
              Favorites
            </Link>
          )}
          {props.match.params.id && (
            <Link
              className={
                props.match.path == ROUTE_USER_PROFILE_FAVORITE
                  ? "nav-link sm-text-fw6-g text-uppercase active"
                  : "nav-link sm-text-fw6-g text-uppercase"
              }
              to={ROUTE_USER_PROFILE_FAVORITE.replace(
                ":id",
                props.match.params.id
              )}
            >
              Favorites
            </Link>
          )}
        </li>
        <li className="nav-item">
          {!props.match.params.id && (
            <Link
              className={
                props.match.path == ROUTE_USER_OWN_PROFILE_FOLLOWING
                  ? "nav-link sm-text-fw6-g text-uppercase active"
                  : "nav-link sm-text-fw6-g text-uppercase"
              }
              to={ROUTE_USER_OWN_PROFILE_FOLLOWING}
            >
              Following
            </Link>
          )}
          {props.match.params.id && (
            <Link
              className={
                props.match.path == ROUTE_USER_PROFILE_FOLLOWING
                  ? "nav-link sm-text-fw6-g text-uppercase active"
                  : "nav-link sm-text-fw6-g text-uppercase"
              }
              to={ROUTE_USER_PROFILE_FOLLOWING.replace(
                ":id",
                props.match.params.id
              )}
            >
              Following
            </Link>
          )}
        </li>
        {!props.match.params.id && (
          <li className="nav-item">
            <Link
              className={
                props.match.path == ROUTE_USER_OWN_PROFILE_ORDER
                  ? "nav-link sm-text-fw6-g text-uppercase active"
                  : "nav-link sm-text-fw6-g text-uppercase"
              }
              to={ROUTE_USER_OWN_PROFILE_ORDER}
            >
              Orders
            </Link>
          </li>
        )}
        {!props.match.params.id && (
          <li className="nav-item">
            <Link
              className={
                props.match.path == ROUTE_USER_OWN_PROFILE_REVIEW
                  ? "nav-link sm-text-fw6-g text-uppercase active"
                  : "nav-link sm-text-fw6-g text-uppercase"
              }
              to={ROUTE_USER_OWN_PROFILE_REVIEW}
            >
              Reviews
            </Link>
          </li>
        )}
        {props.match.params.id && (
          <li className="nav-item">
            <Link
              className={
                props.match.path == ROUTE_USER_PROFILE_REVIEW
                  ? "nav-link sm-text-fw6-g text-uppercase active"
                  : "nav-link sm-text-fw6-g text-uppercase"
              }
              to={ROUTE_USER_PROFILE_REVIEW.replace(
                ":id",
                props.match.params.id
              )}
            >
              Reviews
            </Link>
          </li>
        )}
      </ul>
    </div>
  );
};

export default withRouter(Tab);
