import React from 'react'
import PostCmp from '../components/like/post'
import PostCmpTwo from '../components/like/postTwo'
import PostCmpThree from '../components/like/postThree'
import { SORT_VIEW_BOTTOM, SORT_VIEW_MID, SORT_VIEW_TOP } from 'core/constants'
import { connect } from 'react-redux'
import { selectViewType } from 'core/selectors/user'
import { Component } from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'

class Like extends Component {
  render() {
    const { data, count } = this.props
    if (!data) {
      return null
    }

    return (
      <div className="like-content mt-5">
        <div className="user-fav-products-content pb-4 mb-5">
          <InfiniteScroll
            dataLength={data.length}
            next={this.props.fetchMoreData}
            hasMore={count > data.length ? true : false}
            loader={<h4>Loading...</h4>}
          >
            <div className="row">
              {data &&
                data.map((post) => {
                  if (this.props.sortView == SORT_VIEW_TOP) {
                    return (
                      <PostCmp
                        post={post}
                        key={post._id}
                        reactToPost={this.props.reactToPost}
                        gotoProfile={this.props.gotoProfile}
                      />
                    )
                  }
                  if (this.props.sortView == SORT_VIEW_MID) {
                    return (
                      <PostCmpTwo
                        post={post}
                        key={post._id}
                        reactToPost={this.props.reactToPost}
                        gotoProfile={this.props.gotoProfile}
                      />
                    )
                  }
                  if (this.props.sortView == SORT_VIEW_BOTTOM) {
                    return (
                      <PostCmpThree
                        post={post}
                        key={post._id}
                        reactToPost={this.props.reactToPost}
                        gotoProfile={this.props.gotoProfile}
                      />
                    )
                  }
                })}
            </div>
          </InfiniteScroll>
        </div>
      </div>
    )
  }
}

export default connect((state) => ({
  sortView: selectViewType(state),
}))(Like)
