import moment from 'moment'
import React, { Component } from 'react'
import Comments from './../../../../../images/icons/comments.svg'
import LoveRed from './../../../../../images/icons/love-red.svg'
import Star from './../../../../../images/icons/star.svg'
import Love from './../../../../../images/icons/love.svg'
import ReadMoreAndLess from '../../../../../components/readMore/readMore'
import Like from '../../../../../components/Like'
import { getImageOriginalUrl } from 'core/utils/helpers'

export default class PostThree extends Component {
  render() {
    const { post } = this.props
    if (!post) {
      return null
    }
    return (
      <div className="col-12 col-md-6 col-lg-4 post mb-5">
        <div className="show-img-pp show-img-pp-pb-140">
          <div className="show-img-p">
            <img
              className="cover"
              src={
                post.postId &&
                post.postId.images[0] &&
                getImageOriginalUrl(post.postId.images[0].url)
              }
              alt="img"
            />
          </div>
        </div>
        <div className="mt-3">
          <div className=" mb-3">
            <div className="sm-text-fw4-g">
              {post.postId && (
                <ReadMoreAndLess
                  // ref={this.ReadMore}
                  className="read-more-content"
                  charLimit={250}
                  readMoreText=" Read more"
                  readLessText="Read less"
                >
                  {post.postId.content}
                </ReadMoreAndLess>
              )}
            </div>
            <div className="font-weight-6">
              by{' '}
              {post.postId && post.postId.userId && (
                <span
                  className="text-uppercase text-decoration-underline cursor-pointer"
                  onClick={() => this.props.gotoProfile(post.postId.userId._id)}
                >
                  {/* don't have user details  */}
                  {post.postId &&
                    post.postId.userId &&
                    post.postId.userId.fullName}
                </span>
              )}
            </div>
          </div>
          <div className="">
            {/* <span>30 €</span> */}
            <span className="float-right">
              <Like
                react={this.props.reactToPost}
                postId={post.postId && post.postId._id}
                fill={true}
              />
              {/* <span className="star">
                <img className="mr-1" src={Star} alt="img" />
              </span> */}
            </span>
          </div>
        </div>
      </div>
    )
  }
}
