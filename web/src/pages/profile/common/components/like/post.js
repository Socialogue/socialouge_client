import { getImageOriginalUrl } from 'core/utils/helpers'
import moment from 'moment'
import React, { Component } from 'react'
import Like from '../../../../../components/Like'
import ReadMoreAndLess from '../../../../../components/readMore/readMore'
import Comments from './../../../../../images/icons/comments.svg'
import Love from './../../../../../images/icons/love-red.svg'
import Star from './../../../../../images/icons/star.svg'

export default class Post extends Component {
  render() {
    const { post } = this.props
    if (!post) {
      return null
    }

    return (
      <div className="col-12 post mb-5">
        <div className="row">
          <div className="col-md-6">
            <div className="show-img-pp show-img-pp-pb-140">
              <div className="show-img-p">
                <img
                  className="cover"
                  src={
                    post.postId &&
                    post.postId.images[0] &&
                    getImageOriginalUrl(post.postId.images[0].url)
                  }
                  alt="img"
                />
              </div>
            </div>
          </div>
          <div className="col-md-6">
            <div className="mt-3 mt-md-0 ml-0 ml-md-3">
              <div className=" mb-3">
                <div className="sm-text-fw4-g">
                  {post.postId && (
                    <ReadMoreAndLess
                      // ref={this.ReadMore}
                      className="read-more-content"
                      charLimit={250}
                      readMoreText=" Read more"
                      readLessText="Read less"
                    >
                      {post.postId.content}
                    </ReadMoreAndLess>
                  )}
                </div>
                <div className="font-weight-6">
                  by{' '}
                  {post.postId && post.postId.userId && (
                    <span
                      className="text-uppercase text-decoration-underline cursor-pointer"
                      onClick={() =>
                        this.props.gotoProfile(post.postId.userId._id)
                      }
                    >
                      {/* don't have user details  */}
                      {post.postId &&
                        post.postId.userId &&
                        post.postId.userId.fullName}
                    </span>
                  )}
                </div>
              </div>
              <div className="">
                {/* <span>30 €</span> */}
                <span className="float-right">
                  <Like
                    react={this.props.reactToPost}
                    postId={post.postId && post.postId._id}
                    fill={true}
                  />
                  {/* <span className="star">
                    <img className="mr-1" src={Star} alt="img" />
                  </span> */}
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
