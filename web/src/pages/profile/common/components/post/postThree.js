import { getImageOriginalUrl } from 'core/utils/helpers'
import moment from 'moment'
import React, { Component } from 'react'
import Like from '../../../../../components/Like'
import ReadMoreAndLess from '../../../../../components/readMore/readMore'
import Comments from './../../../../../images/icons/comments.svg'
import LoveRed from './../../../../../images/icons/love-red.svg'

export default class PostThree extends Component {
  render() {
    const { post } = this.props
    if (!post) {
      return null
    }
    return (
      <div className="col-12 col-md-6 col-lg-4 post mb-5" key={post._id}>
        <div className="show-img-pp show-img-pp-pb-140">
          {post.images && (
            <div
              className="show-img-p cursor-pointer"
              onClick={() => this.props.openPostDetails(post._id)}
            >
              <img
                className="cover"
                src={post.images[0] && getImageOriginalUrl(post.images[0].url)}
                alt="img"
              />
            </div>
          )}
        </div>
        <div className="mt-3">
          <div className="sm-text-fw5 mb-3">
            <span>{moment(post.createdAt).fromNow()}</span>
            <span className="float-right">
              <Like react={this.props.reactToPost} postId={post._id} />

              <span className="comment  d-inline-flex align-items-center">
                <img className="mr-1" src={Comments} alt="img" />1
              </span>
            </span>
          </div>
          <div className="sm-text-fw4-g">
            <ReadMoreAndLess
              // ref={this.ReadMore}
              className="read-more-content"
              charLimit={250}
              readMoreText=" Read more"
              readLessText="Read less"
            >
              {post.content}
            </ReadMoreAndLess>
          </div>
        </div>
      </div>
    )
  }
}
