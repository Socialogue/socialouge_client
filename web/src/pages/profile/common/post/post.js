import { reactToPost } from 'core/actions/postActionCreators'
import { selectCount, selectPosts } from 'core/selectors/posts'
import { selectUser, selectViewType } from 'core/selectors/user'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import ReadMoreAndLess from '../../../../components/readMore/readMore'
import Comments from './../../../../images/icons/comments.svg'
import PostCmp from '../components/post/post'
import PostCmpTwo from '../components/post/postTwo'
import PostCmpThree from '../components/post/postThree'
import { SORT_VIEW_BOTTOM, SORT_VIEW_MID, SORT_VIEW_TOP } from 'core/constants'
import InfiniteScroll from 'react-infinite-scroll-component'
import { selectTotalCount } from 'core/selectors/profile'

class Post extends Component {
  reactToPost = (postId) => {
    this.props.reactToPost(this.props.user._id, postId)
  }

  componentDidUpdate = (prevProps) => {
    if (prevProps !== this.props) {
    }
  }

  render() {
    const { data, postCount, isOwn, totalCount } = this.props
    if (!data) {
      return null
    }

    return (
      <div className="post-content mt-4 mt-md-5 pt-3">
        <InfiniteScroll
          dataLength={data.length}
          next={this.props.fetchMoreData}
          hasMore={
            isOwn
              ? postCount > data.length
              : totalCount > data.length
              ? true
              : false
          }
          loader={<h4>Loading...</h4>}
        >
          <div className="row">
            {this.props.data &&
              this.props.data.map((post) => {
                if (this.props.sortView == SORT_VIEW_TOP) {
                  return (
                    <PostCmp
                      post={post}
                      key={post._id}
                      reactToPost={this.reactToPost}
                      openPostDetails={this.props.openPostDetails}
                    />
                  )
                }
                if (this.props.sortView == SORT_VIEW_MID) {
                  return (
                    <PostCmpTwo
                      post={post}
                      key={post._id}
                      reactToPost={this.reactToPost}
                      openPostDetails={this.props.openPostDetails}
                    />
                  )
                }
                if (this.props.sortView == SORT_VIEW_BOTTOM) {
                  return (
                    <PostCmpThree
                      post={post}
                      key={post._id}
                      reactToPost={this.reactToPost}
                      openPostDetails={this.props.openPostDetails}
                    />
                  )
                }
              })}
          </div>
        </InfiniteScroll>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    user: selectUser(state),
    sortView: selectViewType(state),
    postCount: selectCount(state),
    totalCount: selectTotalCount(state),
  }),
  {
    reactToPost,
  },
)(Post)
