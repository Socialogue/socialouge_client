import {
  searchProducts,
  getAllProducts,
} from "core/actions/productsFilterActionCreators";
import { selectSearchProducts } from "core/selectors/productsFilter";
import { selectUser } from "core/selectors/user";
import { cropString } from "core/utils/helpers";
import { trim } from "lodash";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import ArrowLeft from "../../../../images/icons/arrowLeft";
import { errorAlert } from "../../../../utils/alerts";
import Close from "./../../../../images/icons/close";
import search_icon from "./../../../../images/icons/search-icon.svg";
import SearchItem from "./searchItem";

const TagModal = (props) => {
  // constant for tabs...
  // here i maintain tabs value with status for query in api, 1 for your items, 2 for socialogue...

  const YOUR_ITEMS = 1;
  const FROM_SOCIALOGUE = 2;
  const SHOPS = 3;
  const ALL_PRODUCTS = 4;

  const [tab, setTab] = useState(YOUR_ITEMS);
  const [searchQuery, setSearchQuery] = useState("");
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [searchProducts, setSearchProducts] = useState([]);

  // handler for product search, invoke when anyone type on searchbox...
  const queryProducts = (e) => {
    const query = trim(e.target.value);
    setSearchQuery(query);
    const userId = props.user._id;

    setTimeout(() => {
      // check trimmed search query for undefined, empty-string and length is greater then 2,
      // if checking pass then call actions or set searchProducts = [];
      if (query && query !== " " && query.length > 2) {
        props.searchProducts(userId, query, tab);
      } else {
        setSearchProducts([]);
      }
    }, 1000);
  };

  // this useEffect for component did mount with all the products for authenticated user.
  // to show initially in search result...
  useEffect(() => {
    // api call for products of this user..
    // status 4 for all products...
    const userId = props.user._id;

    props.getAllProducts(userId, ALL_PRODUCTS);

    return () => {
      setSearchQuery("");
      setSelectedProducts(null);
      setSearchProducts([]);
    };
  }, []);

  // use useEffect for side effects- looking for change in search-Result in redux and set to this local state
  useEffect(() => {
    setSearchProducts(props.products);
  }, [props.products]);

  // hander for tab change..
  const onTabChange = (value) => {
    setSearchQuery("");
    setSearchProducts([]);
    if (value == YOUR_ITEMS) {
      const userId = props.user._id;
      props.getAllProducts(userId, ALL_PRODUCTS);
    }
    setTab(value);
  };

  // handler for select product from search-suggestions...
  const onSelectProduct = (product, e) => {
    const allProducts = [...selectedProducts];
    const filtered = allProducts.filter((item) => item._id === product._id);
    if (!filtered[0]) {
      if (selectedProducts.length < 3) {
        const products = [...selectedProducts, product];
        setSelectedProducts(products);
        // coding of kowser
        e.currentTarget.classList.add("selected-item");
        // coding of kowser
      } else {
        errorAlert("Maximum 3 products you can tag!");
      }
    }
  };

  //handler for remove selected product...
  const onRemoveSelectedProduct = (product, index) => {
    const products = [...selectedProducts];
    const filtered = products.filter((item) => item._id !== product._id);
    setSelectedProducts(filtered);
    // coding of kowser
    document.querySelectorAll(".selected-item").forEach(function (userItem, i) {
      if (i == index) userItem.classList.remove("selected-item");
    });
    // coding of kowser
  };

  return (
    <div
      className="modal fade modal-1 d-block show"
      id="tagModal"
      tabindex="-1"
      role="dialog"
      aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content p-4">
          <div className="modal-header pt-0 px-0 d-none d-md-flex">
            <div className="sm-text-fw6 text-uppercase">find A tag</div>
            <span
              className="cursor-pointer"
              aria-hidden="true"
              onClick={props.toggleTagModal}
            >
              <Close />
            </span>
          </div>
          <div className="bk-m d-block d-md-none">
            <span onClick={props.toggleTagModal} className="cursor-pointer">
              <ArrowLeft />
            </span>
          </div>

          <div className="modal-body px-0 pt-2 pt-md-4 mt-2 mt-md-0">
            <div className="row ">
              <div className="col-12 col-md-7 tag-part-one">
                <ul className="nav nav-fill tab-nav border-0 mb-4">
                  <li className="nav-item text-left">
                    <span
                      className={
                        tab == YOUR_ITEMS
                          ? "nav-link px-0 py-1 sm-text-fw6-g text-uppercase cursor-pointer active"
                          : "nav-link px-0 py-1 sm-text-fw6-g text-uppercase cursor-pointer"
                      }
                      onClick={() => onTabChange(YOUR_ITEMS)}
                    >
                      Your ITems
                    </span>
                  </li>
                  <li className="nav-item text-left">
                    <span
                      className={
                        tab == SHOPS
                          ? "nav-link px-0 py-1 sm-text-fw6-g text-uppercase cursor-pointer active"
                          : "nav-link px-0 py-1 sm-text-fw6-g text-uppercase cursor-pointer"
                      }
                      onClick={() => onTabChange(SHOPS)}
                    >
                      shops
                    </span>
                  </li>
                  <li className="nav-item text-left">
                    <span
                      className={
                        tab == FROM_SOCIALOGUE
                          ? "nav-link px-0 py-1 sm-text-fw6-g text-uppercase cursor-pointer active"
                          : "nav-link px-0 py-1 sm-text-fw6-g text-uppercase cursor-pointer"
                      }
                      onClick={() => onTabChange(FROM_SOCIALOGUE)}
                    >
                      From socialogue
                    </span>
                  </li>
                </ul>

                <div className="input-group search-group pt-2">
                  <div className="input-group-prepend">
                    <span className="input-group-text">
                      <img src={search_icon} alt="img" />
                    </span>
                  </div>
                  <input
                    type="text"
                    className="form-control custom-input"
                    placeholder="Search"
                    onChange={queryProducts}
                  />
                </div>
                <div className="mt-4 search-item-all">
                  {/* looping over search results and render... */}
                  {searchProducts.map((product) => {
                    return (
                      <SearchItem
                        key={product._id}
                        product={product}
                        onSelectProduct={onSelectProduct}
                      />
                    );
                  })}
                </div>
              </div>
              <div className="col-12 col-md-5 tag-part-two d-none d-md-block mt-4 mt-md-0">
                <div
                  className="row ml-0 h-100"
                  style={{ marginRight: "-.5rem" }}
                >
                  {/* Show selecte product if click on an product... */}
                  {selectedProducts.map((product, i) => {
                    return (
                      <div
                        className={
                          selectedProducts.length <= 2
                            ? selectedProducts.length == 1
                              ? "col-12 pl-0 h-100 pr"
                              : "col-12 pl-0 pb-2 h-50 pr-2"
                            : i == 0
                            ? "col-12 pl-0 pb-2 pr-2 h-50"
                            : "col-6 pl-0 pr-2 h-50"
                        }
                      >
                        <span className="tag-img-txt d-flex-box">
                          {/* {cropString(product.title, 15)}{' '} */}
                          <span
                            className="d-flex cursor-pointer"
                            onClick={() => onRemoveSelectedProduct(product, i)}
                          >
                            <Close />
                          </span>
                        </span>
                        <img
                          className="cover"
                          src={product.mainImage}
                          alt="img"
                        />
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>

          <div className="modal-footer pb-0 px-0">
            <div className="w-100">
              <button
                type="button"
                className="btn border-btn border-btn-gray btn-w-115 "
                onClick={props.toggleTagModal}
              >
                Cancel
              </button>
            </div>
            <button
              type="button"
              className="btn border-btn btn-w-115 "
              onClick={() => props.setTaggedProducts(selectedProducts)}
            >
              save
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default connect(
  (state) => ({
    products: selectSearchProducts(state),
    user: selectUser(state),
  }),
  {
    searchProducts,
    getAllProducts,
  }
)(TagModal);
