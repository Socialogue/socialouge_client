import { cropString } from "core/utils/helpers";
import React from "react";
import ProImg from "./../../../../../images/login-img1.webp";

const SearchItem = ({ product, onSelectProduct }) => {
  return (
    <div
      className="pro-cnt mb-2 cursor-pointer"
      onClick={(e) => onSelectProduct(product,e)}
    >
      <div className="pro-img pro-img-r0">
        <img className="cover" src={product.mainImage} alt="img" />
      </div>
      <div className="pro-txt-cnt pro-txt-cnt-r0 pl-2">
        <div className="x-sm-text-fw6">{cropString(product.title, 30)}</div>
        <div className="cat-ul sm-text-fw4-g">
          <span className="cat-li">Clothes</span>
          <span className="cat-li">Favorites</span>
        </div>
      </div>
    </div>
  );
};

export default SearchItem;
