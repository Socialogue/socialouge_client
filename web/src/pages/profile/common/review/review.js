import React from "react";
import ProImg from "./../../../../images/login-img1.webp";
import StarFa from "./../../../../images/icons/star-f-a.svg";
import StarFi from "./../../../../images/icons/star-f-in.svg";
import PostImage from "./../../../../images/eco-img1.webp";

const Review = () => (
  <div className="user-review-content pt-4 pb-5">
    <div className="font-weight-bold text-uppercase">Reviews (33)</div>
    <div className="review py-4 border-bottom-1">
      <div className="pro-cnt mt-4 mt-md-0">
        <div className="pro-img pro-img-md">
          <img className="cover" src={ProImg} alt="img" />
        </div>
        <div className="pro-txt-cnt pro-txt-cnt-md pl-3">
          <div className="mb-3">
            <span className="sm-text-fw6">
              <span className="text-uppercase">UNiq butiq</span>
              <span className="sm-text-fw5-s2 ml-2">at 19 Jan 2018</span>
            </span>
            <span className="float-right review-star-box position-apsolute position-md-relative">
              <span className="review-star pr-1">
                <img src={StarFa} alt="img" />
              </span>
              <span className="review-star pr-1">
                <img src={StarFa} alt="img" />
              </span>
              <span className="review-star pr-1">
                <img src={StarFa} alt="img" />
              </span>
              <span className="review-star pr-1">
                <img src={StarFa} alt="img" />
              </span>
              <span className="review-star pr-1">
                <img src={StarFi} alt="img" />
              </span>
            </span>
          </div>
          <div className="sm-text-fw5-g mb-3">
            It is a long established fact that a reader will be distracted by
            the readable content of a page when looking at its layout. The point
            of using Lorem Ipsum is that it has a more-or-less normal
            distribution of letters, as opposed to using
          </div>
          <div className="row">
            <div className="col-12 col-sm-6 col-lg-4 col-xl-3">
              <div className="show-img-pp show-img-pp-pb-140">
                <div className="show-img-p">
                  <img className="cover" src={PostImage} alt="img" />
                </div>
              </div>
              <div className="mt-3">
                <div className="font-weight-bold text-uppercase">
                  wide brim bucket hat{" "}
                </div>
                <div className="font-weight-6">
                  by{" "}
                  <span className="text-uppercase text-decoration-underline">
                    BUTIQ K
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Review;
