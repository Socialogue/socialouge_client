import 'emoji-mart/css/emoji-mart.css'

import { addPost, fetchPosts } from 'core/actions/postActionCreators'
import { errorAlert, successAlert } from '../../../../utils/alerts'
import {
  selectConvertedPaths,
  selectOriginalPath,
  selectUploadkey,
} from 'core/selectors/fileUpload'
import {
  selectPostAddStatus,
  selectPosts,
  selectPostsLoading,
} from 'core/selectors/posts'

import Close from './../../../../images/icons/close'
import { FILE_SERVER_SINGLE_IMAGE } from 'core/constants/apiEndpoints'
import Imoji from './../../../../images/icons/imoji'
import PhotoUpload from './../../../../images/icons/photoUpload'
import { Picker } from 'emoji-mart'
import PrimaryButton from '../../../../components/PrimaryButton'
import ProImg from './../../../../images/login-img1.webp'
import React from 'react'
import Tag from './../../../../images/icons/tag'
import TagModal from '../tagModal/tagModal'
import Video from './../../../../images/icons/video'
import axios from 'axios'
import config from '../../../../Config'
import { connect } from 'react-redux'
import { getImageOriginalUrl } from 'core/utils/helpers'
import { selectActiveShop } from 'core/selectors/account'
import { selectUser } from 'core/selectors/user'
import { uploadMultipleImages } from 'core/actions/fileUploadActionCreators'
class CreatePostModal extends React.Component {
  key = 'POST_IMAGE'
  constructor() {
    super()
    this.state = {
      loading: false,
      description: '',
      taggedProductIds: [],
      taggedProducts: [],
      images: [],
      photos: [],
      imageUrls: [],
      showTagModal: false,
      chooseImoji: null,
      showImoji: false,
    }
    this.wrapperRef = React.createRef()
  }

  componentDidUpdate = (prevProps) => {
    if (prevProps.postAddStatus !== this.props.postAddStatus) {
      if (this.props.postAddStatus == false) {
        this.props.toggleModal()
      }
      if (prevProps.posts !== this.props.posts) {
        successAlert('Post added!')
        this.setState({
          description: '',
          images: [],
          photos: [],
          imageUrls: [],
        })
      }
    }

    // if post images uploaded then call action for store post content with image urls ...
    if (
      JSON.stringify(prevProps.convertedPaths) !==
      JSON.stringify(this.props.convertedPaths)
    ) {
      // if (this.props.uploadKey == this.key) {
      //   this.onSubmit()
      // }
    }
  }

  componentDidMount = () => {
    if (this.props.user) {
      this.props.fetchPosts(this.props.user._id)
    }

    document.addEventListener('mousedown', this.handleClickOutside)
  }
  componentWillUnmount = () => {
    document.removeEventListener('mousedown', this.handleClickOutside)
  }

  handleClickOutside = (event) => {
    if (
      this.wrapperRef &&
      !this.wrapperRef.current.contains(event.target) &&
      this.state.showImoji
    ) {
      this.setState({ showImoji: false })
    }
  }

  changeDescription = (e) => {
    this.setState({
      description: e.target.value,
    })
  }

  clickImage = (e) => {
    document.getElementById('image').click()
  }

  onSelectImage = () => {
    // const key = 'PROFILE_IMAGE'
    // const files = [...this.state.images]
    // write a action creator function in file upload action creator for multiple file upload...
    // TODO for nadim: handle multiple image upload with aziz's file server, but can't give base 64 for multiple image
    // and file for single image, need to specify.
    // this.props.uploadPostImage(key, files)
  }

  // handler for selecting image/images from disk...

  handleImage = (e) => {
    e.persist()

    // for multiple image....

    for (let i = 0; i < e.target.files.length; i++) {
      const fr = new FileReader()
      fr.onload = () => {
        this.setState({
          imageUrls: [...this.state.imageUrls, { url: fr.result }],
          images: [...this.state.images, e.target.files[i]],
          photos: [...this.state.photos, fr.result],
        })
      }
      fr.readAsDataURL(e.target.files[i])
    }
  }

  removeImage = (e, i) => {
    let imageUrls = [...this.state.imageUrls]
    let images = [...this.state.images]
    let photos = [...this.state.photos]
    images.splice(i, 1)
    photos.splice(i, 1)
    imageUrls.splice(i, 1)
    this.setState({
      imageUrls: imageUrls,
      images: images,
      photos: photos,
    })
  }

  // post images upload via file server api...
  photosUpload = () => {
    if (this.state.photos.length < 1) {
      errorAlert('Please add some image!')
      return
    }

    const files = [...this.state.photos]
    // write a action creator function in file upload action creator for multiple file upload...
    // TODO for nadim: handle multiple image upload with aziz's file server, but can't give base 64 for multiple image
    // and file for single image, need to specify.

    // this.props.uploadMultipleImages(this.key, files)
  }

  // upload single image with loop...
  uploadSingleFile = (file) => {
    const formData = new FormData()
    formData.append('image', file)
    formData.append('key', config.FILE_SERVER_KEY)
    formData.append('type', config.FILE_SERVER_KEY)

    const prom = axios.post(
      config.FILE_SERVER_URL + FILE_SERVER_SINGLE_IMAGE,
      formData,
      {
        headers: {
          // "Content-Type": "multipart/form-data",
          Accept: 'Application/json',
          allowed_headers: '*',
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
        },
      },
    )
    return prom
  }

  onSubmit = async () => {
    if (this.state.photos.length < 1) {
      errorAlert('Please add some image!')
      return
    }

    try {
      this.setState({ loading: true })
      let postImages = this.state.images
      let promiseArray = []
      let postImagesUrls = []

      if (postImages.length) {
        postImages.map((img) => {
          const prom = this.uploadSingleFile(img)
          promiseArray.push(prom)
        })
      }
      const postImagesResult = await Promise.all(promiseArray)
      postImagesResult.map((resp) =>
        postImagesUrls.push({ url: resp.data.file }),
      )

      this.setState({ loading: false })

      // with active shopId and userId together...
      if (this.props.activeShop) {
        this.props.addPost(
          this.state.description,
          postImagesUrls,
          this.state.taggedProductIds,
          this.props.user._id,
          this.props.activeShop._id,
        )
      } else {
        // if don't have active shop then send null as active shopId
        this.props.addPost(
          this.state.description,
          postImagesUrls,
          this.state.taggedProductIds,
          this.props.user._id,
          null,
        )
      }
    } catch (err) {
      console.log('error: ', err)
      errorAlert('Something went wrong!')
      return
    }
    this.setState({
      loading: false,
    })
    return true
  }

  toggleTagModal = () => {
    this.setState({
      showTagModal: !this.state.showTagModal,
    })
  }

  onEmojiClick = (emojiObject) => {
    let des = this.state.description
    des = des + ' ' + emojiObject.native + ' '
    console.log('flag: ', emojiObject)
    this.setState({
      chooseImoji: emojiObject,
      description: des,
    })
  }

  toggleImoji = (e) => {
    this.setState((prevState) => ({
      showImoji: !prevState.showImoji,
    }))
  }

  // handler for set tagged products and close tag modal...

  setTaggedProducts = (products) => {
    const ids = []
    products.map((product) => {
      ids.push({ productId: product._id })
    })
    this.setState({ taggedProducts: products, taggedProductIds: ids }, () => {
      this.toggleTagModal()
    })
  }

  render() {
    return (
      <div
        className={
          this.props.showModal
            ? 'modal show d-block fade modal-1'
            : 'modal  fade modal-1'
        }
        id="postModal"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content p-4">
            <div className="modal-header pt-0 px-0">
              <div className="sm-text-fw6 text-uppercase">create A post</div>
              <span
                className="cursor-pointer"
                aria-hidden="true"
                onClick={this.props.toggleModal}
              >
                <Close />
              </span>
            </div>

            <div className="modal-body px-0">
              <div className="pro-cnt align-items-center mb-3">
                <div className="pro-img">
                  <img
                    className="cover"
                    src={
                      this.props.user && this.props.user.profileImage
                        ? getImageOriginalUrl(
                            this.props.user.profileImage,
                            '80_80_',
                          )
                        : ProImg
                    }
                    alt="img"
                  />
                </div>
                <div className="pro-txt-cnt pl-2">
                  <div className="sm-text-fw6 text-uppercase">
                    <span className="pro-txt">
                      {this.props.user && this.props.user.fullName}
                    </span>
                  </div>
                </div>
              </div>
              <div className="form-group">
                <textarea
                  className="form-control custom-input border-0 px-0"
                  placeholder="What’s on your mind?"
                  name="description"
                  value={this.state.description}
                  onChange={this.changeDescription}
                />
              </div>
              <div className="imo-phpto-cnt d-flex d-md-block align-items-end">
                <div className="w-100">
                  <div className="text-right mb-2 imo-select-box position-relative">
                    <span
                      className="cursor-pointer d-inline-flex"
                      onClick={this.toggleImoji}
                    >
                      <span
                        className={
                          this.state.showImoji
                            ? 'cursor-pointer d-inline-flex imo-b'
                            : 'cursor-pointer d-inline-flex'
                        }
                      >
                        {' '}
                        <Imoji />
                      </span>
                    </span>
                    <span ref={this.wrapperRef}>
                      {this.state.showImoji && (
                        <Picker
                          title="Pick your emoji…"
                          emoji="point_up"
                          // native={true}
                          onSelect={this.onEmojiClick}
                        />
                      )}
                    </span>
                  </div>
                  <div className="row mr-0">
                    {this.state.imageUrls.map((url, i) => {
                      return (
                        <div className="col-12 post-col pr-0" key={i}>
                          <div className="show-img-pp show-img-pp-pb-140">
                            <span
                              className="cursor-pointer close-p-img"
                              aria-hidden="true"
                            >
                              <span onClick={(e) => this.removeImage(e, i)}>
                                <Close />
                              </span>
                            </span>
                            <div className="show-img-p">
                              <img className="cover" src={url.url} alt="img" />
                            </div>
                            <div className="mid-txt sm-text-fw6-w text-uppercase cursor-pointer">
                              Add a tag
                            </div>
                          </div>
                        </div>
                      )
                    })}
                  </div>
                </div>
              </div>
            </div>

            <div className="modal-footer pb-0 px-0">
              <div className="w-100">
                <span className="up-span" onClick={(e) => this.clickImage(e)}>
                  <PhotoUpload />
                  <input
                    className="d-none"
                    id="image"
                    type="file"
                    onChange={this.handleImage}
                    multiple
                  />
                </span>
                <span className="up-span" onClick={this.toggleTagModal}>
                  <Tag />
                  <span className="tag-pro">Tag a product</span>
                </span>
              </div>

              <PrimaryButton
                loading={this.state.loading}
                onClick={this.onSubmit}
                label="Post"
                className="btn border-btn btn-w-115"
              />
            </div>
          </div>
        </div>
        {this.state.showTagModal && (
          <TagModal
            showTagModal={this.state.showTagModal}
            toggleTagModal={this.toggleTagModal}
            setTaggedProducts={this.setTaggedProducts}
          />
        )}
      </div>
    )
  }
}

export default connect(
  (state) => ({
    user: selectUser(state),
    activeShop: selectActiveShop(state),
    postLoading: selectPostsLoading(state),
    postAddStatus: selectPostAddStatus(state),
    posts: selectPosts(state),
    uploadKey: selectUploadkey(state),
    originalPaths: selectOriginalPath(state),
    convertedPaths: selectConvertedPaths(state),
  }),
  {
    fetchPosts,
    addPost,
    uploadMultipleImages,
  },
)(CreatePostModal)
