import React, { Fragment } from 'react'
import arrow_down_icon from './../../../../images/icons/arrow-down.svg'
import PostImage from './../../../../images/eco-img1.webp'
import AddToFavorite from '../../../../components/Favorite'
import InfiniteScroll from 'react-infinite-scroll-component'
import Down from '../../../../images/icons/down'
import HorizontalSelectOption from '../../../userSettings/components/horizontalSelectOption'
import ArrowLeft from '../../../../images/icons/arrowLeft'
import CloseSm from '../../../../images/icons/closeSm'
import Share from '../../../../images/icons/share'
import { Link } from 'react-router-dom'

const Favorite = (props) => {
  return (
    <div className="favorite-content mt-4 mt-md-5 pt-3 pb-5">
      <div className="user-fav-products-content pb-4 mb-5 border-bottom-1">
        <div className="sm-text-fw6 text-uppercase mb-3 sr-btn-all d-none">
          <span> Winter Outfit 2020</span>
          <button className="btn border-btn p-0 border-0 position-relative ml-4 text-uppercase">
            <span>
              {' '}
              <ArrowLeft /> Winter Outfit 2020
            </span>
          </button>
          <button className="btn border-btn p-0 border-0 float-right text-uppercase">
            <CloseSm /> remove
          </button>
          <button className="btn border-btn p-0 border-0 float-right mr-4 text-uppercase">
            <Share /> Share
          </button>
        </div>

        <div className="sm-text-fw6 text-uppercase mb-3 folder-btn-all">
          <span> Products</span>
          <button className="btn border-btn p-0 border-0 position-relative float-right disabled">
            {/* <span>
              {" "}
              Add to existing folder <Down />
            </span> */}
            <div className="single-group hide-label w-100">
              <HorizontalSelectOption
                formInputClass="form-input position-relative"
                label=""
                name="folder"
                errorText=""
                isRequired={false}
                imageRequired={false}
                selectedText="Add to existing folder"
                options={['folder1', 'folder2']}
                onChange={this.onInputChange}
              />
            </div>
          </button>
          <button className="btn border-btn p-0 border-0 float-right mr-4 pr-3">
            + Create new folder
          </button>
        </div>

        <div className="row">
          {props.data &&
            props.data.map((product) => {
              return (
                <div className="col-12 col-md-6 col-lg-4 post mb-4 mb-md-5">
                  <label className="ctm-container pro-check">
                    <input type="checkbox" />
                    <span className="checkmark" />
                  </label>
                  <div className="show-img-pp show-img-pp-pb-140">
                    {product.productId && (
                      <div
                        className="show-img-p cursor-pointer"
                        onClick={() =>
                          props.viewProduct(
                            product.productId.slug,
                            product.productId._id,
                          )
                        }
                      >
                        <img
                          className="cover"
                          src={product.productId && product.productId.mainImage}
                          alt="img"
                        />
                      </div>
                    )}
                  </div>
                  <div className="mt-3">
                    <div className="mb-3">
                      {product.productId && (
                        <div
                          className="font-weight-bold text-uppercase cursor-pointer"
                          onClick={() =>
                            props.viewProduct(
                              product.productId.slug,
                              product.productId._id,
                            )
                          }
                        >
                          {product.productId && product.productId.title}
                        </div>
                      )}
                      <div className="font-weight-6">
                        by{' '}
                        {product.productId && product.productId.shopId && (
                          <Link
                            to={
                              product.productId.shopId
                                ? `/shop/products/${product.productId.shopId._id}`
                                : ''
                            }
                            className="text-uppercase"
                          >
                            {product.productId &&
                              product.productId.shopId &&
                              product.productId.shopId.shopName}
                          </Link>
                        )}{' '}
                        {product.productId && product.productId.userId && (
                          <span
                            className="text-uppercase text-decoration-underline cursor-pointer"
                            onClick={() =>
                              props.gotoProfile(product.productId.userId._id)
                            }
                          >
                            <span className="text-uppercase">
                              {' '}
                              {product.productId &&
                                product.productId.userId &&
                                product.productId.userId.fullName}
                            </span>
                          </span>
                        )}
                      </div>
                    </div>
                    <div className="">
                      <span>
                        {product.productId &&
                          product.productId.variations &&
                          product.productId.variations[0].price}{' '}
                        €
                      </span>
                      <span className="float-right">
                        {/* <span className="like mr-2">
                          <img className="mr-1" src={Love} alt="img" />
                        </span> */}
                        {product.productId && (
                          <AddToFavorite
                            favorite={props.favorite}
                            productId={product.productId._id}
                            fill={true}
                          />
                        )}
                      </span>
                    </div>
                  </div>
                </div>
              )
            })}
        </div>
        {props.data &&
          props.data.length > 0 &&
          props.data.length < props.total && (
            <div className="text-center">
              <button
                className="btn border-btn border-0 p-0"
                onClick={props.fetchMoreData}
              >
                <span>Show more...</span>
                <br />
                <img src={arrow_down_icon} alt="img" />
              </button>
            </div>
          )}

        {props.loadedStatus === true &&
          props.data &&
          props.data.length == 0 && (
            <h2 className="color-orange">
              Do not have any favorite products...
            </h2>
          )}
      </div>
    </div>
  )
}

export default Favorite
