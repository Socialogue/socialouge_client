import React from "react";
import ArrowR from "./../../../../images/icons/arrow-right.svg";
import ExcB from "./../../../../images/icons/exc-b.svg";
import PostImage from "./../../../../images/eco-img1.webp";
import Order from "./order";

const Orders = () => (
  <div className="user-order-content mt-4 mb-5 pb-5">
    <div className="active-order mb-5 pb-2">
      <div className="sm-text-fw6 text-uppercase mb-2">Active orders (3)</div>
      <div className="tbl-cnt">
        {/* only mobile */}
        <div className="tbl-1 d-block d-md-none">
          <table class="table custom-table">
            <thead>
              <tr>
                <th className="x-sm-text-fw6-g pl-0">Product</th>
              </tr>
            </thead>
            <tbody>
              {/* loop */}
              <tr className="">
                <td className="pl-0">
                  <div className="pro-image">
                    <div className="show-img-pp show-img-pp-pb-140">
                      <div className="show-img-p">
                        <img className="w-100" src={PostImage} alt="img" />
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              <tr className="">
                <td className="pl-0">
                  <div className="pro-image">
                    <div className="show-img-pp show-img-pp-pb-140">
                      <div className="show-img-p">
                        <img className="w-100" src={PostImage} alt="img" />
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              {/* loop */}
            </tbody>
          </table>
        </div>
        {/* only mobile */}

        <div className="tbl-2">
          <table class="table custom-table">
            <thead>
              <tr>
                <th className="x-sm-text-fw6-g pl-0 d-none d-md-table-cell">
                  Product
                </th>
                <th className="x-sm-text-fw6-g text-nowrap">Order name</th>
                <th className="x-sm-text-fw6-g text-nowrap">Order date</th>
                <th className="x-sm-text-fw6-g">Status</th>
                <th className="x-sm-text-fw6-g text-nowrap">Order total</th>
                <th className="x-sm-text-fw6-g" />
              </tr>
            </thead>
            <tbody>
              <Order orderComplete={false} />
              <Order orderComplete={false} />
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div className="complete-order mb-5">
      <div className="sm-text-fw6 text-uppercase mb-2">
        Completed orders (3)
      </div>

      <div className="tbl-cnt">
        {/* only mobile */}
        <div className="tbl-1 d-block d-md-none">
          <table class="table custom-table">
            <thead>
              <tr>
                <th className="x-sm-text-fw6-g pl-0">Product</th>
              </tr>
            </thead>
            <tbody>
              {/* loop */}
              <tr className="">
                <td className="pl-0">
                  <div className="pro-image">
                    <div className="show-img-pp show-img-pp-pb-140">
                      <div className="show-img-p">
                        <img className="w-100" src={PostImage} alt="img" />
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              <tr className="">
                <td className="pl-0">
                  <div className="pro-image">
                    <div className="show-img-pp show-img-pp-pb-140">
                      <div className="show-img-p">
                        <img className="w-100" src={PostImage} alt="img" />
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
              {/* loop */}
            </tbody>
          </table>
        </div>
        {/* only mobile */}

        <div className="tbl-2">
          <table class="table custom-table">
            <thead>
              <tr>
                <th className="x-sm-text-fw6-g pl-0 d-none d-md-table-cell">
                  Product
                </th>
                <th className="x-sm-text-fw6-g text-nowrap">Order name</th>
                <th className="x-sm-text-fw6-g text-nowrap">Order date</th>
                <th className="x-sm-text-fw6-g">Status</th>
                <th className="x-sm-text-fw6-g text-nowrap">Order total</th>
                <th className="x-sm-text-fw6-g" />
              </tr>
            </thead>
            <tbody>
              <Order orderComplete={true} />
              <Order orderComplete={true} />
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
);

export default Orders;
