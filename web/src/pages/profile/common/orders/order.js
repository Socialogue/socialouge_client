import React, { Component } from "react";
import ExcO from "./../../../../images/icons/exc.svg";
import PostImage from "./../../../../images/eco-img1.webp";
import ExcB from "./../../../../images/icons/exc-b.svg";
import ArrowR from "./../../../../images/icons/arrow-right.svg";

class Order extends Component {
  render() {
    return (
      <tr
        className={this.props.orderComplete ? "complete-order" : "active-order"}
      >
        <th className="pl-0 d-none d-md-table-cell">
          <div className="pro-image">
            <div className="show-img-pp show-img-pp-pb-140">
              <div className="show-img-p">
                <img className="w-100" src={PostImage} alt="img" />
              </div>
            </div>
          </div>
        </th>
        <td>
          <div className="mb-3">
            <div className="font-weight-bold text-uppercase text-nowrap">
              wide brim bucket hat{" "}
            </div>
            <div className="font-weight-6 mb-1">
              by{" "}
              <span className="text-uppercase text-decoration-underline">
                BUTIQ K
              </span>
            </div>
            <div className="sm-text-fw6-sf text-nowrap">Order number: 13-21323-00233</div>
          </div>
        </td>
        <td>
          <span className="font-family-roboto text-nowrap">Jan 03, 2021</span>
        </td>
        <td>
          {/* for active order */}
          <span className="sm-text-fw6-o text-uppercase active-status text-nowrap">
            <img className="mr-1" src={ExcO} alt="img" /> In process
          </span>
          {/* for active order */}
          {/* for complete order */}
          <span className="sm-text-fw6 text-uppercase complete-status text-nowrap">
            <img className="mr-1" src={ExcB} alt="img" /> shipped
          </span>
          {/* for complete order */}
        </td>
        <td>
          <span className="text-nowrap">€ 240.00</span>
        </td>
        <td className="text-right pr-0">
          {/* for complete order */}
          <button className="btn border-btn mb-2 complete-view">
            Return item
          </button>{" "}
          <br className="complete-view" />
          {/* for complete order */}

          <button className="btn border-btn mb-1">Contact seller</button>{" "}

          {/* for complete order */}
          <br className="complete-view" />
          <button className="btn border-btn border-0 px-0 text-right complete-view">
            Leave review <img className="" src={ArrowR} alt="img" />
          </button>
          {/* for complete order */}
        </td>
      </tr>
    );
  }
}

export default Order;
