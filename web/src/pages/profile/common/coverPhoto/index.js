import React, { Component } from 'react'
import { connect } from 'react-redux'
import PhotoUpload from '../../../../images/icons/photoUpload'
import {
  uploadSingleImage,
  afterProfileImageUpload,
  storeShopImageUrl,
  afterShopCoverImageUpload,
  setTmpShopCoverImage,
  setTmpCoverImage,
  afterCoverImageUpload,
  storeImageUrl,
} from 'core/actions/fileUploadActionCreators'
import {
  selectConvertedPaths,
  selectUploadkey,
  selectFile,
} from 'core/selectors/fileUpload'
import { selectActiveShop } from 'core/selectors/account'
import { getTmpShopCoverImage } from 'core/selectors/profile'
import { selectUser } from 'core/selectors/user'
import { getImageOriginalUrl } from 'core/utils/helpers'
import { selectShop, selectShops } from 'core/selectors/shops'
import { withRouter } from 'react-router-dom'

class CoverPhoto extends Component {
  // consts ...
  profileImage = 'profile'
  coverImage = 'shop_cover'

  constructor(props) {
    super(props)
    this.state = {
      coverImage: this.props.src,
    }
  }

  componentDidUpdate = (prevProps) => {
    if (
      JSON.stringify(prevProps.convertedPaths) !==
      JSON.stringify(this.props.convertedPaths)
    ) {
      if (this.props.uploadedKey == this.coverImage) {
        // api call for cover photo url save to database...
        if (this.props.status == 'shop') {
          let image = this.props.file
          const shopId = this.props.shop._id
          const type = 'coverImage'
          const status = 'shop'
          this.props.storeShopImageUrl(shopId, image, type, status)
          this.props.afterShopCoverImageUpload({
            key: type,
            coverImage: image,
          })
          this.props.setTmpShopCoverImage({
            key: type,
            coverImage: image,
          })
        } else {
          let image = this.props.file
          const userId = this.props.loggedInUser._id
          const type = 'coverImage'
          const status = 'profile'
          this.props.storeImageUrl(userId, image, type, status)
          this.props.afterCoverImageUpload({
            key: type,
            coverImage: image,
          })
          this.props.setTmpCoverImage({
            key: type,
            coverImage: image,
          })
        }
      }
    }
  }

  // handle cover image uploading...
  // key means a flag to determine what type of image we uploading

  onSelectImage = (e, key) => {
    // const key = this.profileImage;
    e.stopPropagation()
    e.preventDefault()
    const file = e.target.files[0]

    // call action creator that responsible for image upload
    this.props.uploadSingleImage(key, file)
  }

  render() {
    const { coverImage } = this.state
    if (!coverImage) {
      return null
    }
    return (
      <div className="pro-cover position-relative upload-ov-show">
        <img
          src={
            this.props.tmpShopCoverImage && this.props.tmpShopCoverImage
              ? getImageOriginalUrl(this.props.tmpShopCoverImage)
              : this.props.shop && this.props.shop.coverImage
              ? getImageOriginalUrl(this.props.shop.coverImage, '570_400_')
              : this.props.shopData && this.props.shopData.coverImage
              ? getImageOriginalUrl(this.props.shopData.coverImage)
              : coverImage
          }
          className="cover"
        />
        {this.props.myShops.some(
          (item) => item._id === this.props.match.params.id,
        ) && (
          <div className="cover-upload upload-input">
            <div className="upload-overlay cover d-flex">
              <span className="sm-text-fw5">Edit Cover</span>
              <input
                type="file"
                className="ctm-hidden-upload-input cover"
                name="cover"
                onChange={(e) => this.onSelectImage(e, this.coverImage)}
              />
            </div>
          </div>
        )}
      </div>
    )
  }
}

export default connect(
  (state) => ({
    uploadedKey: selectUploadkey(state),
    convertedPaths: selectConvertedPaths(state),
    file: selectFile(state),
    loggedInUser: selectUser(state),
    shop: selectActiveShop(state),
    shopData: selectShop(state),
    tmpShopCoverImage: getTmpShopCoverImage(state),
    myShops: selectShops(state),
  }),
  {
    uploadSingleImage,
    afterProfileImageUpload,
    storeShopImageUrl,
    afterShopCoverImageUpload,
    setTmpShopCoverImage,
    afterCoverImageUpload,
    setTmpCoverImage,
    storeImageUrl,
  },
)(withRouter(CoverPhoto))
