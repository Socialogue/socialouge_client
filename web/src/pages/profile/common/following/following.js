import React from 'react'
import ArrowR from './../../../../images/icons/arrow-right.svg'
import PostImage from './../../../../images/eco-img1.webp'
import ProImg from './../../../../images/login-img1.webp'
import InfiniteScroll from 'react-infinite-scroll-component'
import { getImageOriginalUrl } from 'core/utils/helpers'

const Following = (props) => {
  const { data, followingsCount } = props
  if (!data) {
    return null
  }

  return (
    <InfiniteScroll
      dataLength={data.length}
      next={props.fetchMoreFollowings}
      hasMore={followingsCount > data.length ? true : false}
      loader={<h4>Loading...</h4>}
    >
      <div className="user-following-content mb-5">
        {data.map((follow) => {
          return (
            <div className="review py-4 py-md-5 border-bottom-1">
              <div className="row">
                <div className="col-12 col-md-5 mb-3 mb-md-0">
                  <div className="pro-cnt">
                    <div
                      className="pro-img pro-img-md cursor-pointer"
                      onClick={() => {
                        if (follow.userId) {
                          props.gotoProfile(follow.userId._id)
                        } else {
                          props.shopProfile(follow.shopId._id)
                        }
                      }}
                    >
                      <img
                        className="cover"
                        src={
                          follow.userId && follow.userId.profileImage
                            ? getImageOriginalUrl(
                                follow.userId.profileImage,
                                '80_80_',
                              )
                            : follow.shopId && follow.shopId.profileImage
                            ? getImageOriginalUrl(
                                follow.shopId.profileImage,
                                '80_80_',
                              )
                            : ProImg
                        }
                      />
                    </div>
                    <div className="pro-txt-cnt pro-txt-cnt-md pl-3 pr-0 pr-md-3 pr-lg-5">
                      <div
                        className="md-title md-title-res25 text-uppercase mb-1 cursor-pointer"
                        onClick={() => {
                          if (follow.userId) {
                            props.gotoProfile(follow.userId._id)
                          } else {
                            props.shopProfile(follow.shopId._id)
                          }
                        }}
                      >
                        {follow.userId
                          ? follow.userId.fullName
                          : follow.shopId.shopName}
                      </div>
                      <div className="sm-text-fw6 text-uppercase mb-3">
                        {follow.userId
                          ? follow.userId.totalFollowers
                          : follow.shopId.totalFollowers}{' '}
                        Followers
                      </div>
                      <div className="d-flex mb-2">
                        <button className="btn border-btn">Following</button>
                        <button className="btn border-btn border-0">
                          Message
                          <img className="ml-2" src={ArrowR} alt="img" />
                        </button>
                      </div>
                      {/* <div className="sm-text-fw5-g mb-3 pr-5">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod.
                      </div> */}
                    </div>
                  </div>
                </div>
                <div className="col-12 col-md-7">
                  <div className="row mr-0 mr-md-3">
                    {/* this part render only if following person is a user and show post images */}
                    {follow.userId &&
                      follow.posts.map((post) => {
                        return (
                          <div
                            className="col-6 col-lg-4 mb-3 pr-0 pr-md-3"
                            key={post._id}
                          >
                            <div className="show-img-pp show-img-pp-pb-80">
                              <div className="show-img-p">
                                <img
                                  className="cover"
                                  src={getImageOriginalUrl(post.images[0].url)}
                                  alt="img"
                                />
                              </div>
                            </div>
                          </div>
                        )
                      })}

                    {/* this part render only if following person is a shop and show products images */}
                    {follow.shopId &&
                      follow.products.map((product) => {
                        return (
                          <div
                            className="col-6 col-lg-4 mb-3 pr-0 pr-md-3 "
                            key={product._id}
                          >
                            <div className="show-img-pp show-img-pp-pb-80">
                              <div
                                className="show-img-p cursor-pointer"
                                onClick={() =>
                                  props.viewProduct(product.slug, product._id)
                                }
                              >
                                <img
                                  className="cover"
                                  src={product.mainImage}
                                  alt="img"
                                />
                              </div>
                            </div>
                          </div>
                        )
                      })}
                  </div>
                </div>
              </div>
            </div>
          )
        })}
      </div>
    </InfiniteScroll>
  )
}
export default Following
