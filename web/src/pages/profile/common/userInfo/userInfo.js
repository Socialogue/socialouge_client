import React from 'react'
import ProImg from './../../../../images/login-img1.webp'
import Comments from './../../../../images/icons/comments.svg'
import { connect } from 'react-redux'
import {
  selectIsLoggedIn,
  selectMyFollowings,
  selectUser,
} from 'core/selectors/user'
import {
  getFollowedStatus,
  getTmpProfileImage,
  selectFollowStatus,
} from 'core/selectors/profile'
import {
  addToFollow,
  unFollowUser,
  addFollower,
  removeFollower,
} from 'core/actions/publicProfileActionCreator'
import {
  uploadSingleImage,
  afterProfileImageUpload,
  storeImageUrl,
  setTmpProfileImage,
} from 'core/actions/fileUploadActionCreators'
import { withRouter } from 'react-router'
import { errorAlert, successAlert } from '../../../../utils/alerts'
import { Component } from 'react'
import { openChatWindow } from 'core/messenger/messageActionCreators'
import { fetchConversations } from 'core/messenger/actionCreators/conversationsActionCreators'
import { ROUTE_LOGIN, ROUTE_USER_SETTINGS } from '../../../../routes/constants'
import PhotoUpload from '../../../../images/icons/photoUpload'
import { Link } from 'react-router-dom'
import {
  selectConvertedPaths,
  selectFile,
  selectOriginalPath,
  selectUploadkey,
} from 'core/selectors/fileUpload'
import { getImageOriginalUrl } from 'core/utils/helpers'
import qs from 'qs'

class UserInfo extends Component {
  // consts ...
  profileImage = 'profile'
  coverImage = 'cover'

  constructor(props) {
    super(props)
    this.state = {
      profileImage: props.user.profileImage,
      coverImage: props.user.coverImage,
      convertedPaths: [],
      uploadKey: '',
    }
  }
  componentDidMount = () => {}
  componentDidUpdate = (prevProps) => {
    // check for image upload success or not...
    if (
      JSON.stringify(prevProps.convertedPaths) !==
      JSON.stringify(this.props.convertedPaths)
    ) {
      if (this.props.uploadKey == this.profileImage) {
        this.setState({
          convertedPaths: this.props.convertedPaths,
          profileImage: this.props.originalPaths,
        })

        let image = this.props.file
        const userId = this.props.loggedInUser._id
        const type = 'profileImage'
        const status = 'profile'
        this.props.storeImageUrl(userId, image, type, status)
        this.props.afterProfileImageUpload({
          key: type,
          profileImage: image,
        })
        this.props.setTmpProfileImage({
          key: type,
          profileImage: image,
        })
      }
    }

    if (prevProps.followStatus !== this.props.followStatus) {
      if (this.props.followStatus == 'followed') {
        this.props.addFollower()
      }
      if (this.props.followStatus == 'unFollowed') {
        this.props.removeFollower()
      }
    }
  }

  follow = () => {
    if (!this.props.loggedInUser) {
      return
    }
    if (this.props.loggedInUser) {
      // set status = 1; if you try to follow a user... if follow shop then set it to 2...
      const followingId = this.props.loggedInUser._id
      const followerId = this.props.match.params.id
      const status = 1
      this.props.addToFollow(followingId, followerId, status)
      this.props.fetchConversations(followingId)
    }
  }

  // unfollow...
  onClickUnFollow = () => {
    if (!this.props.loggedInUser) {
      return
    }
    // check for post owner is a user...
    if (this.props.loggedInUser) {
      // set status = 1; if you try to follow a user... if follow shop then set it to 2...

      const status = 1
      const userId = this.props.loggedInUser._id
      const unFollowId = this.props.user._id
      const shopId = null

      this.props.unFollowUser(status, userId, unFollowId, shopId)
    }
  }

  onClickMessage = (userId) => {
    if (!this.props.isLoggedIn) {
      this.props.history.push(ROUTE_LOGIN)
    } else {
      this.props.openChatWindow(
        this.props.match.params.id,
        this.props.user.fullName,
        this.props.user.profileImage,
      )
    }
  }

  // handle profile image uploading...
  // key means a flag to determine what type of image we uploading

  onSelectImage = (e, key) => {
    // const key = this.profileImage;
    e.stopPropagation()
    e.preventDefault()
    const file = e.target.files[0]

    // call action creator that responsible for image upload
    this.props.uploadSingleImage(key, file)

    // initially show profile image from base64 data...
    const fr = new FileReader()

    fr.onload = () => {
      // check if selected image is profile image...
      if (key == this.profileImage) {
        this.setState({
          profileImage: fr.result,
        })
      }

      // check if selected image is cover image...
      if (key == this.coverImage) {
        this.setState({
          coverImage: fr.result,
        })
      }
    }
    fr.readAsDataURL(e.target.files[0])
  }

  render() {
    const { isOwn } = this.props
    if (isOwn == undefined) {
      return null
    }

    let isFollowed = false
    if (this.props.loggedInUser) {
      isFollowed = this.props.myFollowings.includes(this.props.match.params.id)
    }
    return (
      <div
        className={
          this.props.match.params.id
            ? 'pro-cnt d-block d-md-flex cw-user-info user-info align-items-center mb-0 mb-md-5'
            : 'pro-cnt user-info align-items-center mb-4 mb-md-5'
        }
      >
        <div className="pro-img pro-img-xl position-relative overflow-hidden upload-ov-show mb-1 mb-md-0">
          <img
            className="cover"
            src={
              this.props.tempProfileImage && this.props.tempProfileImage
                ? getImageOriginalUrl(this.props.tempProfileImage)
                : this.props.user && this.props.user.profileImage
                ? getImageOriginalUrl(this.props.user.profileImage, '80_80_')
                : ProImg
            }
            alt="img"
          />
          {this.props.isOwn === true && (
            <div className="upload-overlay cover upload-input">
              <span className="sm-text-fw5">Upload picture</span>
              <input
                type="file"
                className="ctm-hidden-upload-input cover"
                onChange={(e) => this.onSelectImage(e, this.profileImage)}
              />
            </div>
          )}
        </div>
        <div
          className={
            this.props.match.params.id
              ? 'pro-txt-cnt pro-txt-cnt-xl md-w-100 pl-0 pl-md-3 d-flex'
              : 'pro-txt-cnt pro-txt-cnt-xl pl-3'
          }
        >
          <div className="w-100">
            <div className="md-title md-title-res25 text-uppercase mb-1">
              <span className="pro-txt pr-3 w-100">
                {this.props.user && this.props.user.fullName}{' '}
              </span>
            </div>
            <div className="sm-text-fw6 text-uppercase mb-1">
              {this.props.user && this.props.user.totalFollowers} Followers
            </div>
            {!this.props.match.params.id && (
              <div>
                <Link
                  className="color-gray7 text-decoration-underline"
                  to={ROUTE_USER_SETTINGS}
                >
                  Edit Profile
                </Link>
              </div>
            )}
          </div>

          {this.props.match.params.id && (
            <span className="d-block d-md-flex align-items-start">
              {/* check if post owner is a user */}
              {this.props.loggedInUser && isFollowed ? (
                <button
                  className="btn border-btn mr-0 mr-md-3 mb-2 mb-md-0"
                  onClick={() => this.onClickUnFollow()}
                >
                  Following
                </button>
              ) : (
                <button
                  className="btn border-btn mr-0 mr-md-3 mb-2 mb-md-0"
                  onClick={() => this.follow()}
                >
                  Follow
                </button>
              )}
              <br className="d-block d-md-none" />
              <button className="btn border-btn" onClick={this.onClickMessage}>
                Message
                <img className="ml-2" src={Comments} alt="img" />
              </button>
            </span>
          )}
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    loggedInUser: selectUser(state),
    isLoggedIn: selectIsLoggedIn(state),
    isFollowed: getFollowedStatus(state),
    uploadKey: selectUploadkey(state),
    originalPaths: selectOriginalPath(state),
    convertedPaths: selectConvertedPaths(state),
    tempProfileImage: getTmpProfileImage(state),
    file: selectFile(state),
    myFollowings: selectMyFollowings(state),
    followStatus: selectFollowStatus(state),
  }),
  {
    addToFollow,
    openChatWindow,
    fetchConversations,
    uploadSingleImage,
    afterProfileImageUpload,
    storeImageUrl,
    setTmpProfileImage,
    unFollowUser,
    addFollower,
    removeFollower,
  },
)(withRouter(UserInfo))
