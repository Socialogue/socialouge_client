import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SmLogo from '../../images/sm-logo.svg'
import Back from '../../images/icons/back.svg'
import LoginImgLg from '../../images/reset-img.webp'
import { ROUTE_HOME } from '../../routes/constants'

class Verification extends Component {
  render() {
    return (
      <div className="form-module-cnt">
        <div className="row m-0 wh-100">
          <div className="col-12 col-md-7 p-0 h-100">
            <div className="form-info-cnt d-flex-box wh-100">
              <div className="back-link">
                <Link to={ROUTE_HOME}>
                  <img className="" src={Back} alt="img" />
                  Back to Main
                </Link>
              </div>
              <div className="form-info">
                <div className="form-logo">
                  {' '}
                  <img className="" src={SmLogo} alt="img" />
                </div>
                <div className="lg-title">
                  verification
                  <span className="dot-r" />
                </div>
                <div className="form-group">
                  <span className="x-sm-text-fw4">
                    Enter the verification code we send you on your email or
                    mobile phone
                  </span>
                </div>
                <div className="form-group row mr-0 mb-4">
                  <div className="col-2 pr-0">
                    <input
                      type="text"
                      className="form-control custom-input text-center"
                      placeholder="-"
                    />
                  </div>
                  <div className="col-2 pr-0">
                    <input
                      type="text"
                      className="form-control custom-input text-center"
                      placeholder="-"
                    />
                  </div>
                  <div className="col-2 pr-0">
                    <input
                      type="text"
                      className="form-control custom-input text-center"
                      placeholder="-"
                    />
                  </div>
                  <div className="col-2 pr-0">
                    <input
                      type="text"
                      className="form-control custom-input text-center"
                      placeholder="-"
                    />
                  </div>
                  <div className="col-2 pr-0">
                    <input
                      type="text"
                      className="form-control custom-input text-center"
                      placeholder="-"
                    />
                  </div>
                  <div className="col-2 pr-0">
                    <input
                      type="text"
                      className="form-control custom-input text-center"
                      placeholder="-"
                    />
                  </div>
                </div>
                <div className="form-group mb-5">
                  <span className="md-text-fw6">Didin’t get sms?</span>
                  <Link to="/register" className="md-text-fw6">
                    {' '}
                    Resend
                  </Link>
                </div>
                <div className="form-group">
                  <button className="btn black-bg-btn text-uppercase w-100">
                    Login
                  </button>
                </div>
                <div className="form-group text-center">
                  <Link to="/" className="md-text-fw6">
                    Back
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-5 p-0 h-100 d-none d-md-block">
            <div className="form-img-cnt wh-100">
              <img className="cover" src={LoginImgLg} alt="img" />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Verification
