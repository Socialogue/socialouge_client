/** @flow */
import React from 'react';
import Helmet from './Common/Helmet';
import type { RouteStaticContext } from '../TypeDefinition';

type Props = {
  staticContext: ?RouteStaticContext
};

/* eslint-disable max-len */
const Faq = ({ staticContext }: Props) => (
  <div className='static-pages'>
    <Helmet
      description='Frequently asked questions & answers'
      staticContext={staticContext}
      title='F.A.Q'
    />
    <p>faq</p>
  </div>
);

export default Faq;

const styles = {
  background: '#000000 url(img/faq-bg.jpg) no-repeat top right',
  backgroundSize: 'cover',
  minHeight: '60vh'
};
