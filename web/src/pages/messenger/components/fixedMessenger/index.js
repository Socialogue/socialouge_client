import React from "react";
import Edit from "../../../../images/icons/edit";
import Message from "../massage";
import MessageList from "../messageList";
import ProImg from "./../../../../images/login-img1.webp";
const FixedMessenger = () => (
  <div className="fixed-messenger">
    {/* <div className="fixed-message-cnt">
      <Message />
    </div>
    <div className="fixed-message-cnt">
      <Message />
    </div>
    <div className="fixed-message-cnt">
      <MessageList />
    </div> */}

    <div className="fixed-message-pro">
      <div>
        <div className="pro-cnt align-items-center w-100 cursor-pointer mt-2">
          <div className="pro-img pro-img-md item-pro-img">
            <img className="cover" src={ProImg} alt="img" />
          </div>
        </div>
        <div className="pro-cnt align-items-center w-100 cursor-pointer mt-2">
          <div className="pro-img pro-img-md item-pro-img">
            <img className="cover" src={ProImg} alt="img" />
          </div>
        </div>

        <div className="pro-cnt align-items-center w-100 cursor-pointer mt-2">
          <div className="pro-img pro-img-md item-pro-img d-flex-box">
            <span>
              <Edit />
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default FixedMessenger;
