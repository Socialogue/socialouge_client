import React, { Component } from "react";
import Dot from "./../../../../../../images/icons/dot";
import search_icon from "./../../../../../../images/icons/search-icon.svg";
class MessageSearch extends Component {
  render() {
    return (
      <div className="input-group search-group item-top w-100">
        <div className="input-group-prepend">
          <span className="input-group-text">
            <img src={search_icon} alt="img" />
          </span>
        </div>
        <input
          type="text"
          className="form-control custom-input"
          placeholder="Search"
        />
        <div className="input-group-append ml-5">
          <span className="input-group-text mr-0 cursor-pointer scale-hover">
            <Dot />
          </span>
        </div>
      </div>
    );
  }
}

export default MessageSearch;
