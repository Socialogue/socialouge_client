import React, { Component } from "react";
import { selectActiveMembers, selectAllConversations } from "core/messenger/selectors";

import Notification from "./../../../../../../images/icons/notification";
import ProImg from "./../../../../../../images/login-img1.webp";
import {activateConversation} from "core/messenger/messageActionCreators";
import {connect} from "react-redux";
import {fetchMessages} from "core/messenger/actionCreators/conversationsActionCreators";
import {getImageOriginalUrl} from "core/utils/helpers";
import {getMessengerActiveClassOrEmpty} from "../../../../../../utils/misc";
import {getOpponentUser} from "core/messenger/services";
import moment from "moment";
import {selectUser} from "core/selectors/user";

class SingleMessage extends Component {

  constructor() {
    super();
    var globalLastMessage = '';
  }

  activate = (id) => {
    this.props.fetchMessages(id);
    this.props.activateConversation(id);
  }

  render() {
    const opponentUser = getOpponentUser(this.props.conversation, this.props.authUser._id);

    if (this.props.conversation.messages) {
      const last_messageObj = this.props.conversation.messages.map((msg) => {
        return msg.text;
      });

      const last_message = last_messageObj[last_messageObj.length-1];
      this.globalLastMessage = last_message;
    }

    return (
      <div className={this.props.action ? 'item active' : 'item'} onClick={() => this.activate(this.props.conversation._id)}>
        <span className="time x-sm-text-fw4-g">{moment(this.props.conversation.updatedAt).utc().fromNow(true)}</span>
        <div className="pro-cnt align-items-center">
          <div className={`pro-img pro-img-md item-pro-img ${getMessengerActiveClassOrEmpty(this.props.activeMembersIds, opponentUser.userId)}`}>
            <img
              className="cover"
              src={opponentUser.userAvatar ? getImageOriginalUrl(opponentUser.userAvatar, "40_40") : ProImg}
              alt="img"
            />
          </div>
          <div className="pro-txt-cnt pro-txt-cnt-md">
            <div className="font-weight-bold text-uppercase d-flex align-items-center">
              {opponentUser.hasOwnProperty('userName') ? opponentUser.userName : ''}
              <span className="ml-2">
                {/*<Notification />*/}
              </span>
            </div>
            <div className="x-sm-text-fw4-g">
              {this.globalLastMessage}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    activeMembersIds: selectActiveMembers(state),
    conversations: selectAllConversations(state),
    authUser: selectUser(state),
  }),
  {
    activateConversation,
    fetchMessages
  }
)(SingleMessage);
