import React,{useState} from "react";

import Dot from "./../../../../images/icons/dot"
import MessageSearch from "./components/messageSearch";
import SingleMessage from "./components/singleMessage";
import search_icon from './../../../../images/icons/search-icon.svg'

const MessageList = (props) => {

  const [search, setSearch] = useState('');

  return(
    <div className="message-list-cnt">
    <div className="input-group search-group item-top w-100">
        <div className="input-group-prepend">
          <span className="input-group-text">
            <img src={search_icon} alt="img" />
          </span>
        </div>
        <input
          type="text"
          className="form-control custom-input"
          placeholder="Search"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
        <div className="input-group-append ml-5">
          <span className="input-group-text mr-0 cursor-pointer scale-hover">
            <Dot />
          </span>
        </div>
      </div>
    <div className="item-list">
      {!!(props.conversations.length) && (
        props.conversations.filter((val) => {
          if (search == '') {
            return val;
          } else if (val.memberUsers[0].userName.toLowerCase().includes(search.toLowerCase())) {
            return val;
          }
        }).map( item => {
          return (
            <SingleMessage key={item._id || item.tempConversationId} conversation={item} />
          )
        }).reverse()
      )}
    </div>
  </div>
  );

};

export default MessageList;
