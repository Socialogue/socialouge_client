import { selectActiveMembers, selectOpenChatWindows, selectUsersLoggedIn } from 'core/messenger/selectors';
import { selectLoginErrorMsg, selectNetworkError, selectToken, selectUser } from 'core/selectors/user';
import { sendChatMessage, sendInitialChatMessage } from 'core/messenger/socketActionCreators';

import ChatItem from './ChatItem';
import Header from './Header';
import InputBox from './InputBox';
import React from 'react';
import { closeChatWindow } from 'core/messenger/messageActionCreators';
import { connect } from 'react-redux';
import moment from 'moment'

class ChatWindow extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      chats: []
    };
  }

  onClickClose = () => {
    const { window } = this.props;
    this.props.closeChatWindow(window.conversationId, window.tempConversationId);
  }

  onClickSend = (text) => {
    const { window } = this.props;
    if (window._id) {
      // eslint-disable-next-line max-len
      this.props.sendChatMessage(text, this.props.loggedInUser._id, window._id);
    } else {
      this.props.sendInitialChatMessage(text, window.members, window.memberUsers, this.props.loggedInUser, window.tempConversationId);
    }
  }

  convertTime=()=> {
    var chatTime=moment(this.props.window.updatedAt).utc();
    var realTime=moment(chatTime).subtract(6, 'hours').format('HH:mm')

    return realTime;


  }

  render() {
    if (!this.props.window) {
      return null;
    }

    console.log('window',this.props.window);
    //console.log('loggedin', this.props.isLoggedIn)
    console.log('loggedIn2',this.props.usersLoggedIn)

    return (
      <div className='message'>
        <Header
          activeMembersIds={this.props.activeMembersIds}
          onClickClose={this.onClickClose}
          window={this.props.window}
          loggedInUser={this.props.loggedInUser}
          oppnentLoggedIn={this.props.isLoggedIn}
        />

        <div className='chat-list'>
          {!!(this.props.window && this.props.window.messages && this.props.window.messages.length) && (
            this.props.window.messages.map(msg => (
              <ChatItem
                key={msg._id}
                memberUsers={this.props.window.memberUsers}
                authUser={this.props.loggedInUser}
                message={msg}
                time={this.convertTime()}
                window={this.props.window}
                /*chatClass='chat self'
                proImgClass='pro-img pro-img-sm item-pro-img'
                proTxtCntClass='pro-txt-cnt pro-txt-cnt-sm'*/
              />
              ))
          )}
          {/*<ChatItem
            chatClass="chat self"
            proImgClass="pro-img pro-img-sm item-pro-img"
            proTxtCntClass="pro-txt-cnt pro-txt-cnt-sm"
          />
          <ChatItem
            chatClass="chat other"
            proImgClass="d-none"
            proTxtCntClass="pro-txt-cnt w-100"
          />*/}
        </div>




        <InputBox onClickSend={this.onClickSend} />
      </div>
    );
  }
}


export default connect(
  state => ({
    activeMembersIds: selectActiveMembers(state),
    loggedInUser: selectUser(state),
    isLoggedIn: !!selectToken(state),
    auth: state.auth,
    networkError: selectNetworkError(state),
    loginErrorMsg: selectLoginErrorMsg(state),
    chatWindows: selectOpenChatWindows(state),
    usersLoggedIn:selectUsersLoggedIn(state)
  }),
  {
    sendChatMessage,
    sendInitialChatMessage,
    closeChatWindow
  }
)(ChatWindow);
