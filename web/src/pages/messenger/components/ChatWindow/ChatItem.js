import React, { Component } from "react";
import ProImg from "../../../../images/login-img1.webp";
import moment from "moment";
import notification from "../../sound/notification.mp3";
import Sound from "react-sound";
const processString = require('react-process-string');

class ChatItem extends Component {
  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: "smooth" });
  };

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  render() {
    const self = this.props.message.userId === this.props.authUser._id;
    console.log("message", this.props.message);
    console.log("user", this.props.authUser._id);
    console.log("window", this.props.window);
    console.log("self", self);
    console.log("id", this.props.window.members[1]);
    console.log("id2", this.props.message.fromUserId);
    let config = [{
      regex: /(http|https):\/\/(\S+)\.([a-z]{2,}?)(.*?)( |\,|$|\.)/gim,
      fn: (key, result) => <span key={key}>
                               <a target="_blank" href={`${result[1]}://${result[2]}.${result[3]}${result[4]}`}>{result[2]}.{result[3]}{result[4]}</a>{result[5]}
                           </span>
  }, {
      regex: /(\S+)\.([a-z]{2,}?)(.*?)( |\,|$|\.)/gim,
      fn: (key, result) => <span key={key}>
                               <a target="_blank" href={`http://${result[1]}.${result[2]}${result[3]}`}>{result[1]}.{result[2]}{result[3]}</a>{result[4]}
                           </span>
  }];

  let stringWithLinks = this.props.message.text;
  let processed = processString(config)(stringWithLinks);

    return (
      <div className={self ? "chat self" : "chat other"}>
        <div className="">
          <div className="x-sm-text-fw4-g m-time">{this.props.time}</div>
          <div className="pro-cnt">
            <div
              className={self ? "pro-img pro-img-sm item-pro-img" : "d-none"}
            >
              <img className="cover" src={ProImg} alt="img" />
            </div>
            <div
              className={
                self ? "pro-txt-cnt pro-txt-cnt-sm" : "pro-txt-cnt w-100"
              }
            >
              <span className="chat-txt x-sm-text-fw4">
                {processed}
                {/*this.props.window.members[1] !==
                this.props.message.fromUserId ? (
                  <Sound playStatus={Sound.status.STOPPED} />
                ) : (
                  <Sound
                    url={notification}
                    playStatus={Sound.status.PLAYING}
                    volume={10}
                  />
                )*/}
              </span>
            </div>
            {this.props.window.members[1] ==
                this.props.message.fromUserId ? (
              <div
                style={{ float: "left", clear: "both", position: "relative" }}
                ref={(el) => {
                  this.messagesEnd = el;
                }}
              ></div>
            ) : (
              <div
                style={{ float: "none", clear: "none", position: "sticky" }}
                ref={(el) => {
                  this.messagesEnd = el;
                }}
              ></div>
            )}

            <div></div>
          </div>
        </div>
      </div>
    );
  }
}

export default ChatItem;
