import React, { Component } from "react";

import Close from "../../../../images/icons/close";
import Delete from "../../../../images/icons/delete";
import Dot from "../../../../images/icons/dot";
import Down from "../../../../images/icons/down";
import ProImg from "../../../../images/login-img1.webp";
import User from "../../../../images/icons/user";
import UserBlock from "../../../../images/icons/userBlock";
import {getImageOriginalUrl} from "core/utils/helpers";
import {getMessengerActiveClassOrEmpty} from "../../../../utils/misc";
import {getOpponentUser} from "core/messenger/services";

class Header extends Component {
  state = {
    msgSetting: false,
  };

  render() {
    const { activeMembersIds } = this.props;
    const opponentUser = getOpponentUser(this.props.window, this.props.loggedInUser._id);

    return (
      <div>
        {/* for fixed messenger message setting */}
        <div className={
              this.state.msgSetting
                ? "ctm-select-box black-select-box d-block"
                : "ctm-select-box black-select-box "
            }>
          <div className="ctm-select-option">
            <User /> <span className="ml-3 x-sm-text-fw6">View profile</span>{" "}
          </div>
          <div className="ctm-select-option">
            <Delete />{" "}
            <span className="ml-3 x-sm-text-fw6">Delete conversation</span>{" "}
          </div>
          <div className="ctm-select-option">
            <UserBlock />{" "}
            <span className="ml-3 x-sm-text-fw6">Block profile</span>{" "}
          </div>
        </div>
        {/* for fixed messenger */}

        <div className="d-flex align-items-center item-top item-top1">
          {/* for fixed messenger */}
          <span
              onClick={() =>
                this.setState({
                  msgSetting: !this.state.msgSetting
                })
              } className="mr-2 cursor-pointer fms">
          <Dot />
          </span>
          {/* for fixed messenger */}

          <div className="pro-cnt align-items-center w-100">
            <div className={`pro-img pro-img-md item-pro-img ${getMessengerActiveClassOrEmpty(activeMembersIds, opponentUser.userId)}`}>
              <img className="cover" src={
                opponentUser.userAvatar
                  ? getImageOriginalUrl(opponentUser.userAvatar, "40_40_")
                  : ProImg
              } alt="img" />
            </div>
            <div className="pro-txt-cnt pro-txt-cnt-md">
              <div className="font-weight-bold text-uppercase">{
                opponentUser.userName
                  ? opponentUser.userName
                  : ''
              }</div>
              <div className="x-sm-text-fw4-g">{(this.props.window.isWindowOpen ? 'online' : 'Active 2 min. ago')}</div>
            </div>
          </div>
          <div className="d-flex align-items-center">
            {/* for fixed messenger */}
            <span className="scale-hover cursor-pointer fms" onClick={this.props.onClickClose}>
              <Close />
            </span>
            {/* for fixed messenger */}
            <span className="scale-hover cursor-pointer nms">
              <Dot />
            </span>
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
