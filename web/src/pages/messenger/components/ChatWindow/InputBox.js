import React, { Component } from "react";
import Imoji from "../../../../images/icons/imoji";
import File from "../../../../images/icons/file";
import PhotoUpload from "../../../../images/icons/photoUpload";
import ArrowRight from "../../../../images/icons/arrowRight";
import 'emoji-mart/css/emoji-mart.css'
import {Picker} from "emoji-mart"


class InputBox extends Component {

  constructor() {
    super();
    this.state = {
      text: '',
      chooseImoji: null,
      showImoji: false,
    };

    
  }

  keyPress = (e) =>{
    if(e.keyCode == 13){
      this.onClickSend();
    }
  }

  onEmojiClick = emojiObject => {
    console.log("imoji: ", emojiObject);
    let des = this.state.text;
    des = des + " " + emojiObject.native + " ";
    this.setState({
      chooseImoji: emojiObject,
      text: des
    });
  };

  toggleImoji = e => {
    this.setState({
      showImoji: !this.state.showImoji
    });
  };




  onClickSend = () => {
    this.props.onClickSend(this.state.text);
    this.setState({text: ''})
  }

  

  render()
  
    
  {

   

    const {text} = this.state;

    return (
      <div className="chat-input">
        <div className="input-group align-items-center position-relative form-input-append">
          <span className="d-flex add-file mr-3">
            <span className="cursor-pointer d-flex">
              <input
                type="file"
                className="ctm-hidden-upload-input cover"
                name="image"
              />
              <PhotoUpload />
            </span>
            <div className="text-right mb-2 imo-select-box position-relative">
                    <span
                      className="cursor-pointer d-inline-flex"
                      onClick={this.toggleImoji}
                    >
                      <span
                        className={
                          this.state.showImoji
                            ? "cursor-pointer d-inline-flex imo-b"
                            : "cursor-pointer d-inline-flex"
                        }
                      >
                        {" "}
                        <Imoji />
                      </span>
                    </span>
                    {this.state.showImoji && (
                      <Picker
                        title="Pick your emoji…"
                        emoji="point_up"
                        onSelect={this.onEmojiClick}
                      />
                    )}
                  </div>
            <span className="cursor-pointer d-flex">
              <input
                type="file"
                className="ctm-hidden-upload-input cover"
                name="file"
              />
              <File />
            </span>
          </span>
          <input
            type="text"
            className="form-control custom-input"
            placeholder="Add comment..."
            name="chat"
            value={text}
            onKeyDown={this.keyPress}
            onChange={e => this.setState({text: e.target.value})}
          />
          <button className="append-input d-block btn border-btn border-0 p-0"
              onClick={this.onClickSend}>
            Send <ArrowRight />
          </button>
        </div>
      </div>
    );
  }
}

export default InputBox;
