import React, {useState} from "react";
import { Link } from "react-router-dom";
import UserInfo from "./components/userInfo";
import Conversation from "./components/conversation";
import WriteMessage from "./components/writeMessage";
const Message = () => (
  <div className="message">
    <UserInfo />

    <div className="chat-list">
      <Conversation
        chatClass="chat self"
        proImgClass="pro-img pro-img-sm item-pro-img"
        proTxtCntClass="pro-txt-cnt pro-txt-cnt-sm"
      />
      <Conversation
        chatClass="chat other"
        proImgClass="d-none"
        proTxtCntClass="pro-txt-cnt w-100"
      />
    </div>

    <WriteMessage />
  </div>
);

export default Message;
