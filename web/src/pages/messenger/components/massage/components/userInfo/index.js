import React, { Component } from "react";
import Close from "../../../../../../images/icons/close";
import Dot from "../../../../../../images/icons/dot";
import Down from "../../../../../../images/icons/down";
import User from "../../../../../../images/icons/user";
import Delete from "../../../../../../images/icons/delete";
import UserBlock from "../../../../../../images/icons/userBlock";
import ProImg from "./../../../../../../images/login-img1.webp";
class UserInfo extends Component {
  render() {
    return (
      <div>
        {/* for fixed messenger message setting */}
        <div className="ctm-select-box black-select-box">
          <div className="ctm-select-option">
            <User /> <span className="ml-3 x-sm-text-fw6">View profile</span>{" "}
          </div>
          <div className="ctm-select-option">
            <Delete />{" "}
            <span className="ml-3 x-sm-text-fw6">Delete conversation</span>{" "}
          </div>
          <div className="ctm-select-option">
            <UserBlock />{" "}
            <span className="ml-3 x-sm-text-fw6">Block profile</span>{" "}
          </div>
        </div>
        {/* for fixed messenger */}

        <div className="d-flex align-items-center item-top item-top1">
          {/* for fixed messenger */}
          <span className="mr-2 cursor-pointer fms">
            <Down />
          </span>
          {/* for fixed messenger */}

          <div className="pro-cnt align-items-center w-100">
            <div className="pro-img pro-img-md item-pro-img">
              <img className="cover" src={ProImg} alt="img" />
            </div>
            <div className="pro-txt-cnt pro-txt-cnt-md">
              <div className="font-weight-bold text-uppercase">Roman Por</div>
              <div className="x-sm-text-fw4-g">Active 2 min. ago</div>
            </div>
          </div>
          <div className="d-flex align-items-center">
            {/* for fixed messenger */}
            <span className="scale-hover cursor-pointer fms">
              <Close />
            </span>
            {/* for fixed messenger */}
            <span className="scale-hover cursor-pointer nms">
              <Dot />
            </span>
          </div>
        </div>
      </div>
    );
  }
}

export default UserInfo;
