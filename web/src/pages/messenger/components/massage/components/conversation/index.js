import React, { Component } from "react";
import ProImg from "./../../../../../../images/login-img1.webp";
class Conversation extends Component {
  render() {
    return (
      <div className={this.props.chatClass}>
        <div className="">
          <div className="x-sm-text-fw4-g m-time">2 min. ago</div>
          <div className="pro-cnt">
            <div className={this.props.proImgClass}>
              <img className="cover" src={ProImg} alt="img" />
            </div>
            <div className={this.props.proTxtCntClass}>
              <span className="chat-txt x-sm-text-fw4">
                Hello! I’am sending you a free sample of my new jumper
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Conversation;
