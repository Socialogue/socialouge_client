import React, { Component } from "react";
import Imoji from "../../../../../../images/icons/imoji";
import File from "../../../../../../images/icons/file";
import PhotoUpload from "../../../../../../images/icons/photoUpload";
import ArrowRight from "../../../../../../images/icons/arrowRight";
class WriteMessage extends Component {
  render() {
    return (
      <div className="chat-input">
        <div className="input-group align-items-center position-relative form-input-append">
          <span className="d-flex add-file mr-3">
            <span className="cursor-pointer d-flex">
              <input
                type="file"
                className="ctm-hidden-upload-input cover"
                name="image"
              />
              <PhotoUpload />
            </span>
            <span className="cursor-pointer d-flex">
              <Imoji />
            </span>
            <span className="cursor-pointer d-flex">
              <input
                type="file"
                className="ctm-hidden-upload-input cover"
                name="file"
              />
              <File />
            </span>
          </span>
          <input
            type="text"
            className="form-control custom-input"
            placeholder="Add comment..."
            name="chat"
          />
          <button className="append-input d-block btn border-btn border-0 p-0">
            Send <ArrowRight />
          </button>
        </div>
      </div>
    );
  }
}

export default WriteMessage;
