import {
  selectActiveConversation,
  selectActiveMembers,
  selectAllConversations,
  selectUsersLoggedIn
} from "core/messenger/selectors";

import ChatWindow from "./components/ChatWindow";
import FixedMessenger from "./FixedMessenger";
import Header from "./../home/Header/Header";
import MessageList from "./components/messageList";
import React from "react";
import { connect } from "react-redux";
import { fetchConversations } from 'core/messenger/actionCreators/conversationsActionCreators';
import { fetchMessages } from 'core/messenger/api/MessengerApiClient';
import { fetchPosts } from "core/actions/postActionCreators";

class Messenger extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      chats: [],
      page: 1,
      limit: 10,
    };
  }

  fetchMoreMessages = () => {
    this.setState({ page: this.state.page + this.state.limit }, () => {
      this.props.fetchMessages(
        this.props.user._id,
        this.state.page,
        this.state.limit
      );
    });
  };

  componentDidMount() {
    this.props.fetchConversations(this.props.loggedInUsers._id);
  }

  onSend = async (chat) => {
    /*window.sl_socket.emit('message', {
      text: chat
    });*/
    return true;
  };

  render() {
    const { conversations, activeMembersIds } = this.props;
    return (
      <div>
        <Header activeMembersIds={activeMembersIds} />
        <div className="messenger-module d-flex">
          <MessageList conversations={conversations} />
          <ChatWindow
            chats={this.state.chats}
            onSend={this.onSend}
            window={this.props.activeConversation}
            fetchMoreMessages={this.fetchMoreMessages}
            loggedIn={this.props.loggedInUsers}
          />
        </div>
        <FixedMessenger />
      </div>
    );
  }
}

export default connect(
  state => ({
    activeMembersIds: selectActiveMembers(state),
    conversations: selectAllConversations(state),
    activeConversation: selectActiveConversation(state),
    loggedInUsers: selectUsersLoggedIn(state)
  }),
  {
    fetchPosts,
    fetchMessages,
    fetchConversations
  }
)(Messenger);
