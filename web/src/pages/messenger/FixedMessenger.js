import React, {Component} from "react";
import {
  selectLoginErrorMsg,
  selectNetworkError,
  selectOtpSendMsg,
  selectOtpVerifyStatus,
  selectToken,
  selectUser
} from "core/selectors/user";

import ChatWindow from "./components/ChatWindow";
import Edit from "../../images/icons/edit";
import ProImg from "../../images/login-img1.webp";
import {connect} from "react-redux";
import {login} from "core/actions/authActionCreators";
import {selectOpenChatWindows} from "core/messenger/selectors";

class FixedMessenger extends Component {

  render() {
    return (
      <div className="fixed-messenger">
        {!!(this.props.chatWindows.length) && this.props.chatWindows.map( window => {
          return (
            <div
              key={`chatWindow_${window._id || window.tempConversationId}`}
              className="fixed-message-cnt">
              <ChatWindow window={window}/>
            </div>
          )
        })
        }

        {/*<div className="fixed-message-pro">
          <div>
            <div className="pro-cnt align-items-center w-100 cursor-pointer mt-2">
              <div className="pro-img pro-img-md item-pro-img">
                <img className="cover" src={ProImg} alt="img" />
              </div>
            </div>
            <div className="pro-cnt align-items-center w-100 cursor-pointer mt-2">
              <div className="pro-img pro-img-md item-pro-img">
                <img className="cover" src={ProImg} alt="img" />
              </div>
            </div>

            <div className="pro-cnt align-items-center w-100 cursor-pointer mt-2">
              <div className="pro-img pro-img-md item-pro-img d-flex-box">
            <span>
              <Edit />
            </span>
              </div>
            </div>
          </div>
        </div>*/}
      </div>
    )
  }
}

export default connect(
  state => ({
    user: selectUser(state),
    isLoggedIn: !!selectToken(state),
    auth: state.auth,
    networkError: selectNetworkError(state),
    otpSendMsg: selectOtpSendMsg(state),
    otpVerifyStatus: selectOtpVerifyStatus(state),
    loginErrorMsg: selectLoginErrorMsg(state),
    chatWindows: selectOpenChatWindows(state),
  }),
  {
    login
  }
)(FixedMessenger);
