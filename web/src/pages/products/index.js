import { SORT_VIEW_BOTTOM, SORT_VIEW_MID, SORT_VIEW_TOP } from "core/constants";
import { selectFeed, selectUser, selectViewType } from "core/selectors/user";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import FixedFooter from "../Common/fixedFooter";
import Header from "../home/Header/Header";
import Product from "../home/Home/ProductPostContentBottom/product";
import ProductTop from "../home/Home/ProductPostContent/product/product";
import SellerTitle from "../home/Home/sellerTitle";
import SortView from "../home/sortView";
import FilterProduct from "./filterProduct";
import { parseQuery } from "core/utils/helpers";
import {
  ROUTE_PRODUCT_DETAILS,
  ROUTE_USER_OWN_PROFILE_POST,
  ROUTE_USER_PROFILE_POST
} from "../../routes/constants";
import qs from "qs";
import {
  fetchFilterdProducts,
  fetchProductsWithPriceFilter
} from "core/actions/productsFilterActionCreators";
import {
  selectAllColors,
  selectAllSizes,
  selectFilteredProducts,
  selectLoadingStatus,
  selectTotalFilteredProducts
} from "core/selectors/productsFilter";

class Products extends Component {
  state = {
    price: [0, 100],
    minPrice: null,
    maxPrice: null,
    sizes: [],
    colors: [],
    category: "",
    subCategory: "",
    innerCategory: ""
  };

  // constant for query type...

  QUERIES = [
    { key: "category", value: 0 },
    { key: "subCategory", value: 1 },
    { key: "innerCategory", value: 2 }
  ];

  //get a key for query
  getKey = type => {
    return this.QUERIES[type].key;
  };

  componentDidMount = () => {
    // call parseQuery method from core/utils/helper ... for parse query string of url
    // const parsedQuery = parseQuery(this.props.location.params)
    console.log("parsed query: ", this.props.match.params);
    const params = this.props.match.params;
    if (params.category) {
      this.setState({ category: params.category });
    }
    if (params.subCategory) {
      this.setState({
        category: params.category,
        subCategory: params.subCategory
      });
    }
    if (params.innerCategory) {
      this.setState({
        category: params.category,
        subCategory: params.subCategory,
        innerCategory: params.innerCategory
      });
    }
    //   const key = parsedQuery.query
    //   const query = parsedQuery.q

    //   // call products filtering api with key and query id ...
    //   this.props.fetchFilterdProducts(key, query)
  };

  componentDidUpdate = prevProps => {
    if (prevProps !== this.props) {
      const params = this.props.match.params;
      if (params.category) {
        this.setState({ category: params.category });
      }
      if (params.subCategory) {
        this.setState({
          category: params.category,
          subCategory: params.subCategory
        });
      }
      if (params.innerCategory) {
        this.setState({
          category: params.category,
          subCategory: params.subCategory,
          innerCategory: params.innerCategory
        });
      }
    }
  };

  viewProduct = (slug, productId) => {
    this.props.history.push(
      ROUTE_PRODUCT_DETAILS.replace(":slug", slug).replace(
        ":productId",
        productId
      )
    );
  };

  gotoProfile = userId => {
    if (userId == this.props.user._id) {
      this.props.history.push(ROUTE_USER_OWN_PROFILE_POST);
    } else {
      const route = ROUTE_USER_PROFILE_POST.replace(":id", userId);
      this.props.history.push(route);
    }
  };

  // set colors and sizes ...
  onColorAndSizeChange = (e, name, value) => {
    let colors = [...this.state.colors];
    let sizes = [...this.state.sizes];

    // on select colors...
    if (name == "color") {
      if (colors.find(color => color == value)) {
        colors = colors.filter(color => color !== value);
        this.setState({ colors: colors }, () => {
          this.filter(this.state.price);
        });
      } else {
        colors.push(value);
        this.setState({ colors: colors }, () => {
          this.filter(this.state.price);
        });
      }
    }

    // on select size...
    if (name == "size") {
      if (sizes.find(size => size == value)) {
        sizes = sizes.filter(size => size !== value);
        this.setState({ sizes: sizes }, () => {
          this.filter(this.state.price);
        });
      } else {
        sizes = [...sizes, value];
        this.setState({ sizes: sizes }, () => {
          this.filter(this.state.price);
        });
      }
    }
  };

  filter = (price = [this.state.price[0], this.state.price[1]]) => {
    // set min and max price to state...
    this.setState({ minPrice: price[0], maxPrice: price[1] });

    // call parseQuery method from core/utils/helper ... for parse query string of url
    const parsedQuery = parseQuery(this.props.location.search);

    const key = parsedQuery.t;
    const query = parsedQuery.q;
    const minPrice = price[0];
    const maxPrice = price[1];
    const colors = this.state.colors;
    const sizes = this.state.sizes;

    // call action creator for size filter with category/sub-category/ inner-category...
    this.props.fetchProductsWithPriceFilter(
      key,
      query,
      minPrice,
      maxPrice,
      colors,
      sizes
    );
  };

  render() {
    const { allColors, allSizes } = this.props;
    const { category, subCategory, innerCategory } = this.props.match.params;
    let view = this.props.sortViewType;
    return (
      <div>
        <Header />
        <div className={view == SORT_VIEW_TOP ? 'sticky-d-scroll pb-0 pb-5' : ''}>
        <div className="max-content-width-1 pt-0 pt-md-5 px-4">
          <nav aria-label="breadcrumb" className="">
            <ol className="breadcrumb custom-breadcrumb">
              <li className="breadcrumb-item">
                <Link to="/">Home</Link>
              </li>
              {category && (
                <li
                  className={
                    subCategory ? "breadcrumb-item" : "breadcrumb-item active"
                  }
                >
                  {subCategory ? <Link to="#">{category}</Link> : category}
                </li>
              )}
              {subCategory && (
                <li
                  className={
                    innerCategory ? "breadcrumb-item" : "breadcrumb-item active"
                  }
                >
                  {innerCategory ? (
                    <Link to="#">{subCategory}</Link>
                  ) : (
                    subCategory
                  )}
                </li>
              )}
              {innerCategory && (
                <li className="breadcrumb-item active">{innerCategory}</li>
              )}
            </ol>
          </nav>
          <div className="d-flex align-items-center mb-3">
            {/* <span className="md-title text-uppercase">Coats</span> */}
            <span className="font-weight-5 text-right w-100">
              Showing {this.props.filteredProducts.length} products of{" "}
              {this.props.productsCount}
            </span>
          </div>
          <div className="row">
            {this.props.loadedStatus === true &&
              this.props.filteredProducts.length == 0 && (
                <h2 className="col-12 color-orange">No Product found.</h2>
              )}
            {this.props.filteredProducts.length > 0 &&
              this.props.filteredProducts.map(product => {
                return view == SORT_VIEW_TOP ? (
                  <ProductTop
                    colClass="col-12 product mb-5"
                    gotoProfile={this.gotoProfile}
                    viewProduct={this.viewProduct}
                    product={product}
                    view={view}
                  />
                ) : view == SORT_VIEW_MID ? (
                  <Product
                    colClass="col-12 col-md-6 product mb-5"
                    gotoProfile={this.gotoProfile}
                    viewProduct={this.viewProduct}
                    product={product}
                  />
                ) : (
                  <Product
                    colClass="col-12 col-md-4 product mb-5"
                    gotoProfile={this.gotoProfile}
                    viewProduct={this.viewProduct}
                    product={product}
                  />
                );
              })}
          </div>
        </div>

        <SellerTitle />

        <div className="max-content-width-1 pt-0 pt-md-5 px-4">
          <div className="row">
            {this.props.filteredProducts.length > 0 &&
              this.props.filteredProducts.map(product => {
                return view == SORT_VIEW_TOP ? (
                  <ProductTop
                    colClass="col-12 product mb-5"
                    gotoProfile={this.gotoProfile}
                    viewProduct={this.viewProduct}
                    product={product}
                    view={view}
                  />
                ) : view == SORT_VIEW_MID ? (
                  <Product
                    colClass="col-12 col-md-6 product mb-5"
                    gotoProfile={this.gotoProfile}
                    viewProduct={this.viewProduct}
                    product={product}
                  />
                ) : (
                  <Product
                    colClass="col-12 col-md-4 product mb-5"
                    gotoProfile={this.gotoProfile}
                    viewProduct={this.viewProduct}
                    product={product}
                  />
                );
              })}
          </div>
        </div>

        <FilterProduct
          colors={allColors}
          sizes={allSizes}
          filter={this.filter}
          selectedSizes={this.state.sizes}
          selectedColors={this.state.colors}
          onVariationChange={this.onColorAndSizeChange}
        />
        <SortView />
        <div className="ab-mf px-4 mb-4 d-block">
          <FixedFooter />
        </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    user: selectUser(state),
    sortViewType: selectViewType(state),
    filteredProducts: selectFilteredProducts(state),
    productsCount: selectTotalFilteredProducts(state),
    allColors: selectAllColors(state),
    allSizes: selectAllSizes(state),
    loadedStatus: selectLoadingStatus(state)
  }),
  { fetchFilterdProducts, fetchProductsWithPriceFilter }
)(Products);
