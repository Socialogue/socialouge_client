import React, { Component } from "react";
import Down from "./../../../images/icons/down";
import { Range } from "rc-slider";
import "rc-slider/assets/index.css";
import "./filter.scss";

class FilterProduct extends Component {
  // constant for toggle filters...
  PRICE_FILTER = 1;
  SIZE_FILTER = 2;
  COLOR_FILTER = 3;

  state = {
    showFilterLi: false,
    value: [0, 400],
    defaultValue: [0, 400],
    showFilter: 1
  };

  onChangePrice = value => {
    this.setState({ value });
  };

  onAfterChangePrice = value => {
    this.setState({ value });
    setTimeout(() => {
      const price = this.state.value;
      this.props.filter(price);
    }, 1000);
  };

  render() {
    const { showFilter, value } = this.state;
    const { colors, sizes } = this.props;
    return (
      <div className="fixed-filter d-none d-md-inline-block">
        <div className="filter-ul">
          {/* li need active class */}
          <div
            className={
              showFilter == this.PRICE_FILTER
                ? "filter-li sm-text-fw6 text-uppercase active"
                : "filter-li sm-text-fw6 text-uppercase"
            }
          >
            <div
              onClick={() =>
                this.setState({
                  showFilter: this.PRICE_FILTER
                })
              }
              className="mb-2 pb-2 cursor-pointer"
            >
              price <Down />
            </div>
            <div className="li-cnt pb-2 mb-4 ">
              {showFilter == this.PRICE_FILTER && (
                <div className="position-relative">
                  {" "}
                  
                  {/* minimum range value shown, need to style a bit @kowser */}
                  <span className="min-pr m-pr sm-text-fw4-g"> {value[0]}€ </span>
                  {/* maximum range value shown, need to style a bit @kowser */}
                  <span className="max-pr m-pr sm-text-fw4-g"> {value[1]}€</span>
                  <Range
                    // className=""
                    defaultValue={this.state.defaultValue}
                    value={this.state.value}
                    allowCross={false}
                    step={1}
                    onChange={this.onChangePrice}
                    onAfterChange={this.onAfterChangePrice}
                  />
                </div>
              )}
            </div>
          </div>
          {/* li need active class */}
          <div
            className={
              showFilter == this.SIZE_FILTER
                ? "filter-li sm-text-fw6 text-uppercase active"
                : "filter-li sm-text-fw6 text-uppercase"
            }
          >
            <div
              onClick={() =>
                this.setState({
                  showFilter: this.SIZE_FILTER
                })
              }
              className="mb-2 pb-2 cursor-pointer"
            >
              size <Down />
            </div>
            {showFilter == this.SIZE_FILTER && (
              <div className="li-cnt mult-cnt">
              <div className="max-li-cnt">
                {sizes.length > 0 &&
                  sizes.map((size, i) => {
                    return (
                      <label className="ctm-container d-block  mb-1" key={i}>
                        {size.lt}
                        <input
                          type="checkbox"
                          checked={this.props.selectedSizes.includes(size.lt)}
                          onChange={e =>
                            this.props.onVariationChange(e, "size", size.lt)
                          }
                        />
                        <span className="checkmark" />
                        {/* <span className="sm-text-fw6-g float-right">(12)</span> */}
                      </label>
                    );
                  })}
              </div>
              </div>
            )}
          </div>
          {/* li need active class */}
          <div
            className={
              showFilter == this.COLOR_FILTER
                ? "filter-li sm-text-fw6 text-uppercase active"
                : "filter-li sm-text-fw6 text-uppercase"
            }
          >
            <div
              onClick={() =>
                this.setState({
                  showFilter: this.COLOR_FILTER
                })
              }
              className="mb-2 pb-2 cursor-pointer"
            >
              colour <Down />
            </div>
            {showFilter == this.COLOR_FILTER && (
              <div className="li-cnt mult-cnt">
              <div className="max-li-cnt">
                {colors.length > 0 &&
                  colors.map((color, i) => {
                    return (
                      <label className="ctm-container d-block mb-1" key={i}>
                        {color.name}
                        <input
                          type="checkbox"
                          checked={this.props.selectedColors.includes(
                            color.name
                          )}
                          onChange={e =>
                            this.props.onVariationChange(e, "color", color.name)
                          }
                        />
                        <span className="checkmark" />
                        {/* <span className="sm-text-fw6-g float-right">(12)</span> */}
                      </label>
                    );
                  })}
              </div>
              </div>
            )}
          </div>
          {/* li need active class */}
          {/* <div
            className={
              this.state.showFilterLi
                ? "filter-li sm-text-fw6 text-uppercase active"
                : "filter-li sm-text-fw6 text-uppercase"
            }>
            <div onClick={() =>
              this.setState({
                showFilterLi: !this.state.showFilterLi
              })
            } className="mb-2 pb-2 cursor-pointer">
              brand <Down />
            </div>
            <div className="li-cnt">
              <label className="ctm-container d-block  mb-1">
                brand
                <input type="checkbox" />
                <span className="checkmark" />
                <span className="sm-text-fw6-g float-right">(12)</span>
              </label>
              <label className="ctm-container d-block mb-1">
                brand
                <input type="checkbox" />
                <span className="checkmark" />
                <span className="sm-text-fw6-g float-right">(12)</span>
              </label>
            </div>
          </div> */}
        </div>
      </div>
    );
  }
}

export default FilterProduct;
