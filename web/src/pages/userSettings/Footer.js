import React, { Component } from 'react'
import ArrowLeft from '../../images/icons/arrowLeft'

class Footer extends Component {
  render() {
    return (
      <div className="border-top-1 p-4 md-border-0 mb-5 mb-md-0">
        <div className="user-setting-cnt mx-auto d-flex">
          {this.props.view === 'personal' && (
            <div className="text-right d-none d-md-block">
              <button className="btn border-btn border-0 btn-w-115 text-uppercase">
                <span className="color-orange">
                  {this.props.changesCount | 0}
                </span>{' '}
                Change
              </button>
            </div>
          )}
          <div className="text-right w-100">
            <button
              className="btn border-btn border-0 btn-w-115"
              onClick={this.props.onClickCancel}
            >
              <span className="pr-2">
                <ArrowLeft />
              </span>{' '}
              Cancel
            </button>
            <button
              className="btn border-btn btn-w-115"
              onClick={this.props.onClickSave}
            >
              Save
            </button>
          </div>
        </div>
      </div>
    )
  }
}

export default Footer
