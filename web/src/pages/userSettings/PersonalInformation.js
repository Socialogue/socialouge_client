import { days, generateArrayOfYears } from 'core/utils/dummyDate'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import HorizontalInput from './components/horizontalInput'
import { selectPersonalData } from 'core/selectors/settings'
import { getUserData } from 'core/actions/userSettingsActionCreators'

import HorizontalSelectOption from './components/horizontalSelectOption'
import { successAlert } from '../../utils/alerts'

class PersonalInformation extends Component {
  constructor(props) {
    super(props)
    this.state = {
      nickName: props.personalInfo.nickName,
      gender: props.personalInfo.gender,
      fullName: props.personalInfo.fullName,
      email: props.personalInfo.email,
      birthday: props.personalInfo.birthday | 1,
      birthmonth: props.personalInfo.birthmonth | 'January',
      birthyear: props.personalInfo.birthyear | 1995,
      country: props.personalInfo.country,
      comapnyaddress: props.personalInfo.companyAddress,
      zipcode: props.personalInfo.zipcode,
      phone: props.personalInfo.phone,

      name: '',

      years: [],
      days: [],
    }

    console.log('gender: ', props)
  }

  getState = () => {
    return this.state
  }

  componentDidMount = () => {
    const years = generateArrayOfYears()
    this.setState({ years: years })
    this.props.getUserData()
  }

  componentDidUpdate = (prevProps) => {
    if (
      JSON.stringify(prevProps.personalInfo) !==
        JSON.stringify(this.props.personalInfo) &&
      !this.state.nickName
    ) {
      const { personalInfo } = this.props

      this.setState(
        {
          nickName: personalInfo.nickName,
          gender: personalInfo.gender,
          fullName: personalInfo.fullName,
          email: personalInfo.email,
          birthday: personalInfo.birthday,
          birthmonth: personalInfo.birthmonth,
          birthyear: personalInfo.birthyear,
          country: personalInfo.country,
          comapnyaddress: personalInfo.companyAddress,
          zipcode: personalInfo.zipcode,
          phone: personalInfo.phone,
        },
        () => {
          console.log('state: ', this.state.gender, personalInfo.gender)
          for (const key in personalInfo) {
            this.props.onChange(key, personalInfo[key])
          }
        },
      )
    }
  }
  handleInputChange = (name, value) => {
    this.setState({
      [name]: value,
    })
    this.props.onChange(name, value)
  }

  handleBirthMonth = (name, value, index) => {
    const dayLength = days(index + 1, this.state.birthyear)
    let dayArr = []
    for (let index = 1; index <= dayLength; index++) {
      dayArr.push(index)
    }
    this.setState({
      days: dayArr,
    })
    this.handleInputChange(name, value)
  }

  handleBirthYear = (name, value, index) => {
    this.setState({
      birthyear: value,
    })
    this.handleInputChange(name, value)
  }

  onSubmit = () => {}

  render() {
    return (
      <div className="form-module-cnt-1 mx-auto mt-0 pt-3">
        <div className="font-weight-bold text-uppercase mb-4">
          PERSONAL settings
        </div>
        <div className="form form-1">
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="Nickname"
            type="text"
            placeholder="Your nick name"
            value={this.state.nickName}
            name="nickName"
            errorText="Please enter your Nickname"
            isRequired={true}
            onChange={this.handleInputChange}
          />
          <HorizontalSelectOption
            formInputClass="form-input position-relative"
            label="Gender"
            errorText=""
            isRequired={false}
            imageRequired={false}
            selectedText={this.state.gender}
            options={['Male', 'Female', 'Other']}
            onChange={this.handleInputChange}
          />
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="Full name"
            type="text"
            placeholder="Your Full Name"
            name="fullName"
            value={this.state.fullName}
            errorText="Please enter your Full Name"
            isRequired={false}
            value={this.state.fullName}
            onChange={this.handleInputChange}
          />

          {/* <div className="form-group row align-items-center mr-0">
            <div className="x-sm-text-fw6 text-uppercase col-12 col-md-5 pr-0">
              Date of birth <sup> *</sup>
            </div>
            <div className="col-12 col-md-7 pr-0">
              <div className="form-input position-relative">
                <span className="prepend-input">
                  <ExcB />
                </span>
                <div className="row mr-0">
                  <div className="col-4 pr-0">
                    <CustomSelectOption
                      imageRequired={false}
                      selectedText={this.state.birthyear}
                      label="year"
                      name="birthyear"
                      options={this.state.years}
                      onChange={this.handleBirthYear}
                    />
                  </div>
                  <div className="col-4 pr-0">
                    <CustomSelectOption
                      imageRequired={false}
                      selectedText="January"
                      options={months}
                      label="month"
                      name="birthmonth"
                      onChange={this.handleBirthMonth}
                    />
                  </div>
                  <div className="col-4 pr-0">
                    <CustomSelectOption
                      imageRequired={false}
                      selectedText="1"
                      label="day"
                      name="birthday"
                      options={this.state.days}
                      onChange={this.handleInputChange}
                    />
                  </div>
                </div>
                <div className="err-txt x-sm-text-fw4 color-orange pl-3 pt-2">
                  Please enter your Country name
                </div>
              </div>
            </div>
          </div> */}
        </div>
        <div className="form form-1">
          <HorizontalSelectOption
            formInputClass="form-input position-relative"
            label="Country"
            errorText=""
            isRequired={false}
            imageRequired={true}
            selectedText={this.state.country}
            options={['UK', 'IT', 'LT', 'US']}
            onChange={this.handleInputChange}
          />
          {/* <HorizontalInput
            formInputClass="form-input position-relative form-input-append"
            label="Company address"
            type="text"
            placeholder="Your Company address"
            name="comapnyaddress"
            errorText="Please enter your Company address"
            appendText="Find address"
            value={this.state.comapnyaddress}
            isRequired={true}
            onChange={this.handleInputChange}
          /> */}
          {/* <HorizontalInput
            formInputClass="form-input position-relative form-input-append"
            label="Zip code"
            type="text"
            placeholder="Your Zip code"
            name="zipcode"
            errorText="Please enter your Zip code"
            appendText="Find code"
            isRequired={true}
            onChange={this.handleInputChange}
          /> */}
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="Your phone number"
            type="text"
            placeholder="Your phone number"
            name="phone"
            value={this.state.phone}
            errorText="Please enter Your phone number"
            isRequired={false}
            onChange={this.handleInputChange}
            validations={['phone']}
            disabled={true}
          />
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="Your Email address"
            type="email"
            placeholder="Your Email address"
            name="email"
            value={this.state.email}
            errorText="Please enter Your Email address"
            isRequired={false}
            disabled={true}
          />
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    personalInfo: selectPersonalData(state),
  }),
  {
    getUserData,
  },
)(PersonalInformation)
