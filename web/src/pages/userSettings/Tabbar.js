import React, {Component} from 'react';
import ArrowLeft from "../../images/icons/arrowLeft";

class Tabbar extends Component {
  render() {

    const {view, onTabChange} = this.props;

    return (
      <ul className="nav nav-fill tab-nav mb-4 mb-md-5">
        <li
          className="nav-item"
          onClick={() => onTabChange("personal")}
        >
              <span
                className={
                  view === "personal"
                    ? "nav-link cursor-pointer sm-text-fw6-g text-uppercase active"
                    : "nav-link cursor-pointer sm-text-fw6-g text-uppercase"
                }
              >
                PERSONAL <span className="d-none d-md-inline-block">INFORMATION</span>
              </span>
        </li>
        <li className="nav-item" onClick={() => onTabChange("email")}>
              <span
                className={
                  view === "email"
                    ? "nav-link cursor-pointer sm-text-fw6-g text-uppercase active"
                    : "nav-link cursor-pointer sm-text-fw6-g text-uppercase"
                }
              >
                EMAIL & PASSWORD
              </span>
        </li>
        <li
          className="nav-item"
          onClick={() => onTabChange("delivery")}
        >
              <span
                className={
                  view === "delivery"
                    ? "nav-link cursor-pointer sm-text-fw6-g text-uppercase active"
                    : "nav-link cursor-pointer sm-text-fw6-g text-uppercase"
                }
              >
                delivery <span className="d-none d-md-inline-block">INFORMATION</span>
              </span>
        </li>
      </ul>
    );
  }
}

export default Tabbar;


