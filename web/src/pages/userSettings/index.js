// @flow

import React from 'react'
import { connect } from 'react-redux'
import {
  deleteAccount,
  getUserData,
  getDeliveryData,
  updateDeliveryData,
  updateEmailData,
  updatePasswordData,
  updatePersonalData,
  addDeliveryData,
} from 'core/actions/userSettingsActionCreators'
import Header from './../home/Header/Header'
import { Link } from 'react-router-dom'
import './userSettings.scss'
import PersonalInformation from './PersonalInformation'
import EmailPassword from './components/emailPassword'
import DeliveryInformation from './components/deliveryInformation'
import { getMonthIndex } from 'core/utils/dummyDate'
import { errorAlert, successAlert } from '../../utils/alerts'
import {
  selectDeliveryData,
  selectErrorMsg,
  selectModalType,
  selectSuccessMsg,
  selectPersonalData,
} from 'core/selectors/settings'
import EmailCheck from './components/emailPassword/emailCheck'
import Footer from './Footer'
import Tabbar from './Tabbar'
import type { UserData } from 'core/types/UserData'
import {
  ROUTE_USER_OWN_PROFILE_POST,
  ROUTE_USER_SETTINGS,
} from '../../routes/constants'

type State = {
  userData: UserData,
}

class UserSettings extends React.Component<null, State> {
  constructor() {
    super()
    this.state = {
      loading: false,
      userData: {},
      view: 'personal',
      mailOrPass: '',
      nickName: '',
      gender: 'Male',
      name: '',
      email: '',
      address: '',
      surname: '',
      birthday: '1',
      birthmonth: 'January',
      birthyear: 2004,
      country: 'UK',
      comapnyaddress: '',
      zipcode: '',
      phone: '',
      deliveryId: '',
      billing: '',
      prevEmail: '',
      newEmail: '',
      oldPass: '',
      newPass: '',
      confirmNewPass: '',
      personalInfo: {},
      changesCount: 0,
    }
  }

  onTabChange = (value) => {
    this.setState({
      view: value,
      mailOrPass: this.state.mailOrPass == 'check' ? '' : this.state.mailOrPass,
    })
  }

  backToEmail = (value) => {
    this.setState({
      view: value,
    })
  }

  componentDidMount = () => {
    // this.props.getDeliveryData();
    this.props.getUserData()
  }

  componentDidUpdate = (prevProps) => {
    if (
      prevProps.successMsg !== this.props.successMsg ||
      JSON.stringify(prevProps.personalInfo) !==
        JSON.stringify(this.props.personalInfo)
    ) {
      if (this.props.successMsg !== '') {
        successAlert(this.props.successMsg)
        if (this.props.modalType == 'email') {
          this.setState({
            view: 'email',
            mailOrPass: 'check',
          })
        }
      }
    }
    if (
      prevProps.errorMsg !== this.props.errorMsg ||
      JSON.stringify(prevProps.personalInfo) !==
        JSON.stringify(this.props.personalInfo)
    ) {
      if (this.props.errorMsg !== '') {
        errorAlert(this.props.errorMsg)
      }
    }
  }

  onCancel = () => {
    this.props.history.push(ROUTE_USER_OWN_PROFILE_POST)
  }

  getChangesCount = (count) => {
    this.setState({ changesCount: count })
  }

  onSubmit = (e) => {
    this.setState({ loading: true })
    // if active tab is personal...

    if (this.state.view === 'personal') {
      // const personalData = this.personaDataState.getState();
      if (this.state.nickName == '') {
        errorAlert('Please fill up the form')
        return
      }
      // const monthIndex = getMonthIndex(this.state.birthmonth);
      // const birthday = new Date(
      //   this.state.birthyear,
      //   monthIndex,
      //   this.state.birthday
      // );

      try {
        const personal = {
          nickName: this.state.nickName,
          gender: this.state.gender,
          fullName: this.state.fullName,
          country: this.state.country,
        }
        this.props.updatePersonalData(personal)
      } catch (e) {
        console.log('catch', e)
      }
    }

    // if active tab is delivery...
    if (this.state.view === 'delivery') {
      // const deliveryData = this.deliveryDataState.getState();

      if (
        this.state.name == '' ||
        this.state.surname == '' ||
        this.state.country == '' ||
        this.state.address == '' ||
        this.state.zipCode == '' ||
        this.state.email == '' ||
        this.state.phone == ''
      ) {
        errorAlert('Please fill up the form')
        return
      }
      // const monthIndex = getMonthIndex(this.state.birthmonth);
      // const birthday = new Date(
      //   this.state.birthyear,
      //   monthIndex,
      //   this.state.birthday
      // );

      try {
        const delivery = {
          name: this.state.name,
          surname: this.state.surname,
          country: this.state.country,
          address: this.state.address,
          zipCode: this.state.zipCode,
          billing: this.state.billing,
          phone: this.state.phone,
          email: this.state.email,
          id: this.state.deliveryId !== '' ? this.state.deliveryId : null,
        }
        if (this.state.deliveryId) {
          this.props.updateDeliveryData(delivery)
        } else {
          this.props.addDeliveryData(delivery)
        }
      } catch (e) {
        console.log('catch', e)
      }
    }

    // if active tab is email and password(change email)...
    if (this.state.view === 'email') {
      if (this.state.mailOrPass === 'email') {
        if (this.state.email == '' || this.state.newEmail == '') {
          errorAlert('Please fill up the form')
          return
        }
        try {
          const emailData = {
            email: this.state.email,
            newEmail: this.state.newEmail,
          }
          this.props.updateEmailData(emailData)
        } catch (e) {
          console.log('catch', e)
        }
      }
    }

    // if active tab is email and password(change password)...
    if (this.state.view === 'email') {
      if (this.state.mailOrPass === 'pass') {
        if (
          this.state.oldPass == '' ||
          this.state.newPass == '' ||
          this.state.confirmNewPass == ''
        ) {
          errorAlert('Please fill up the form')
          return
        }
        if (this.state.newPass !== this.state.confirmNewPass) {
          errorAlert('New password and confirm password not match!')
          return
        }
        try {
          const passwordData = {
            currentPassword: this.state.oldPass,
            newPassword: this.state.newPass,
            confirmPassword: this.state.confirmNewPass,
          }
          this.props.updatePasswordData(passwordData)
        } catch (e) {
          console.log('catch', e)
        }
      }
    }
  }

  gotPersonalData = (name, value) => {
    if (this.props.personalInfo[name] != value) {
      const changes = { ...this.state.personalInfo }
      changes[name] = 1
      const count = Object.keys(changes).length
      this.setState({ personalInfo: changes, changesCount: count })
    } else {
      const changes = { ...this.state.personalInfo }
      delete changes[name]
      const count = Object.keys(changes).length
      this.setState({ personalInfo: changes, changesCount: count })
    }
    this.setState({
      [name]: value,
    })
  }

  gotEmailData = (key, value) => {
    this.setState({
      [key]: value,
    })
  }

  gotDeliveryData = (key, value) => {
    this.setState({
      [key]: value,
    })
  }

  onEditDeliveryData = (item) => {
    if (item) {
      this.setState({
        name: item.name,
        surname: item.surname,
        country: item.country,
        address: item.address,
        zipCode: item.zipCode,
        phone: item.phone,
        email: item.email,
        deliveryId: item._id,
        billing: item.billing,
      })
    } else {
      this.setState({
        name: '',
        surname: '',
        country: '',
        address: '',
        zipCode: '',
        phone: '',
        email: '',
        deliveryId: '',
        billing: '',
      })
    }
  }

  deleteAccount = () => {
    this.props.deleteAccount()
  }

  emailOrPass = (value) => {
    this.setState({
      mailOrPass: value,
    })
  }

  render() {
    const { view, mailOrPass } = this.state

    return (
      <div>
        <Header />
        <div className="user-setting-cnt mx-auto my-4 my-lg-5 px-4">
          <nav aria-label="breadcrumb" className="mb-4">
            <ol className="breadcrumb custom-breadcrumb">
              <li className="breadcrumb-item">
                <Link to="#">Home</Link>
              </li>
              <li className="breadcrumb-item">Settings</li>
            </ol>
          </nav>
          <Tabbar view={view} onTabChange={this.onTabChange} />

          {view === 'personal' && (
            <PersonalInformation
              // personalInfo={this.props.personalInfo}
              onChange={this.gotPersonalData}
              getChangesCount={this.getChangesCount}
              // ref={ref => (this.personaDataState = ref)}
            />
          )}
          {view === 'email' && mailOrPass !== 'check' && (
            <EmailPassword
              emailData={this.gotEmailData}
              passwordData={this.gotEmailData}
              deleteAccount={this.deleteAccount}
              popupChangeHandler={this.emailOrPass}
              personalInfo={this.props.personalInfo}
            />
          )}
          {view === 'email' && mailOrPass === 'check' && (
            <EmailCheck change={this.onTabChange} />
          )}
          {view === 'delivery' && (
            <DeliveryInformation
              // ref={ref => (this.deliveryDataState = ref)}
              // deliveryData={this.props.deliveryData}
              onChange={this.gotDeliveryData}
              onEditDeliveryData={this.onEditDeliveryData}
            />
          )}
        </div>

        <Footer
          onClickSave={this.onSubmit}
          onClickCancel={this.onCancel}
          view={view}
          changesCount={this.state.changesCount}
        />
      </div>
    )
  }
}

export default connect(
  (state) => ({
    successMsg: selectSuccessMsg(state),
    errorMsg: selectErrorMsg(state),
    modalType: selectModalType(state),
    deliveryData: selectDeliveryData(state),
    personalInfo: selectPersonalData(state),
  }),
  {
    updatePersonalData,
    updateEmailData,
    updatePasswordData,
    deleteAccount,
    updateDeliveryData,
    getUserData,
    getDeliveryData,
    addDeliveryData,
  },
)(UserSettings)
