import React, { Component } from "react";
import Down from "./../../../../../images/icons/down";
import SelectedImg from "./../../../../../images/flag.svg";
class CustomSelectOption extends Component {
  state = {
    clickOnSelect: false,
    selected: this.props.selectedText
  };

  dropDown = () => {
    this.setState({
      clickOnSelect: !this.state.clickOnSelect
    });
  };

  changeValue = (name, value, index) => {
    const nameLower = name.toLowerCase();
    this.setState({
      selected: value
    });
    this.dropDown();
    this.props.onChange(nameLower, value, index);
  };

  render() {
    return (
      <div className="ctm-select ctm-select-main">
        <div className="ctm-select-txt mr-2" onClick={this.dropDown}>
          {this.props.imageRequired && (
            <span className="select-img pr-2">
              <img src={SelectedImg} />
            </span>
          )}
          <span className="slct-txt">{this.props.selectedText}</span>
          <span className="select-arr">
            <Down />
          </span>
        </div>
        <div
          className={
            this.state.clickOnSelect
              ? "ctm-select-box d-block"
              : "ctm-select-box"
          }
        >
          <div className="box-cnt">
            {this.props.options &&
              this.props.options.map((option, i) => {
                return (
                  <div
                    className="ctm-select-option"
                    key={option}
                    onClick={() => this.changeValue(this.props.name, option, i)}
                  >
                    {option}
                  </div>
                );
              })}
          </div>
        </div>
      </div>
    );
  }
}

export default CustomSelectOption;
