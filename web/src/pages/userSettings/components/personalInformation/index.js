import { getUserData } from "core/actions/userSettingsActionCreators";
import { selectPersonalData } from "core/selectors/settings";
import { days, generateArrayOfYears, months } from "core/utils/dummyDate";
import React, { Component } from "react";
import { connect } from "react-redux";
import ExcB from "./../../../../images/icons/excIcon";
import HorizontalInput from "./../horizontalInput";
import HorizontalSelectOption from "./../horizontalSelectOption";
import CustomSelectOption from "./../horizontalSelectOption/customSelectOption";
class PersoanalInformation extends Component {
  state = {
    nickname: "",
    gender: "Male",
    name: "",
    surename: "",
    birthday: "1",
    birthmonth: "January",
    birthyear: 2004,
    country: "UK",
    comapnyaddress: "",
    zipcode: "",
    phone: "",
    years: [],
    days: []
  };

  componentDidMount = () => {
    const years = generateArrayOfYears();
    this.setState({ years: years });
    this.props.getUserData();
  };

  componentDidUpdate = prevProps => {
    if (prevProps.personalInfo !== this.props.personalInfo) {
      if (this.props.personalInfo) {
        console.log("personal dat: ", this.props.personalInfo);
        this.setState({
          nickname: this.props.personalInfo.nickName,
          gender: this.props.personalInfo.gender,
          name: this.props.personalInfo.name,
          surename: this.props.personalInfo.surename,
          birthday: this.props.personalInfo.birthday,
          birthmonth: this.props.personalInfo.birthmonth,
          birthyear: this.props.personalInfo.birthyear,
          country: this.props.personalInfo.country,
          comapnyaddress: this.props.personalInfo.companyAddress,
          zipcode: this.props.personalInfo.zipcode,
          phone: this.props.personalInfo.phone
        });
      }
    }
  };
  handleInputChange = (name, value) => {
    console.log("input value: ", name, value);
    this.setState({
      [name]: value
    });
    this.props.personalData(name, value);
  };

  // handleHorizotalDropDown = (name, value) => {
  //   this.setState({
  //     [name]: value
  //   });
  // };

  handleBirthMonth = (name, value, index) => {
    const dayLength = days(index + 1, this.state.birthyear);
    let dayArr = [];
    for (let index = 1; index <= dayLength; index++) {
      dayArr.push(index);
    }
    this.setState({
      days: dayArr
    });
    this.handleInputChange(name, value);
  };

  handleBirthYear = (name, value, index) => {
    this.setState({
      birthyear: value
    });
    this.handleInputChange(name, value);
  };

  onSubmit = () => {};

  render() {
    return (
      <div className="form-module-cnt-1 mx-auto mt-0 pt-3">
        <div className="font-weight-bold text-uppercase mb-4">
          PERSONAL settings
        </div>
        <div className="form form-1">
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="Nickname"
            type="text"
            placeholder="Your nick name"
            value={this.state.nickname}
            name="nickname"
            errorText="Please enter your Nickname"
            isRequired={false}
            onChange={this.handleInputChange}
          />
          <HorizontalSelectOption
            formInputClass="form-input position-relative"
            label="Gender"
            errorText=""
            isRequired={true}
            imageRequired={false}
            selectedText="Male"
            options={["Male", "Female", "Other"]}
            onChange={this.handleInputChange}
          />
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="Name"
            type="text"
            placeholder="Your Name"
            name="name"
            errorText="Please enter your Name"
            isRequired={true}
            onChange={this.handleInputChange}
          />
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="Surename"
            type="text"
            placeholder="Your Surename"
            name="surename"
            errorText="Please enter your Surename"
            isRequired={true}
            onChange={this.handleInputChange}
          />
          <div className="form-group row align-items-center mr-0">
            <div className="x-sm-text-fw6 text-uppercase col-12 col-md-5 pr-0">
              Date of birth <sup> *</sup>
            </div>
            <div className="col-12 col-md-7 pr-0">
              <div className="form-input position-relative">
                <span className="prepend-input">
                  <ExcB />
                </span>
                <div className="row mr-0">
                  <div className="col-12 col-md-4 pr-0">
                    <CustomSelectOption
                      imageRequired={false}
                      selectedText={this.state.birthyear}
                      label="year"
                      name="birthyear"
                      options={this.state.years}
                      onChange={this.handleBirthYear}
                    />
                  </div>
                  <div className="col-12 col-md-4 pr-0">
                    <CustomSelectOption
                      imageRequired={false}
                      selectedText="January"
                      options={months}
                      label="month"
                      name="birthmonth"
                      onChange={this.handleBirthMonth}
                    />
                  </div>
                  <div className="col-12 col-md-4 pr-0">
                    <CustomSelectOption
                      imageRequired={false}
                      selectedText="1"
                      label="day"
                      name="birthday"
                      options={this.state.days}
                      onChange={this.handleInputChange}
                    />
                  </div>
                </div>
                <div className="err-txt x-sm-text-fw4 color-orange pl-3 pt-2">
                  Please enter your Country name
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="form form-1">
          <HorizontalSelectOption
            formInputClass="form-input position-relative"
            label="Country"
            errorText=""
            isRequired={true}
            imageRequired={true}
            selectedText={this.state.country}
            options={["UK", "IT", "LT", "US"]}
            onChange={this.handleInputChange}
          />
          <HorizontalInput
            formInputClass="form-input position-relative form-input-append"
            label="Company address"
            type="text"
            placeholder="Your Company address"
            name="comapnyaddress"
            errorText="Please enter your Company address"
            appendText="Find address"
            value={this.state.comapnyaddress}
            isRequired={true}
            onChange={this.handleInputChange}
          />
          <HorizontalInput
            formInputClass="form-input position-relative form-input-append"
            label="Zip code"
            type="text"
            placeholder="Your Zip code"
            name="zipcode"
            errorText="Please enter your Zip code"
            appendText="Find code"
            isRequired={true}
            onChange={this.handleInputChange}
          />
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="Your phone number"
            type="text"
            placeholder="Your phone number"
            name="phone"
            errorText="Please enter Your phone number"
            isRequired={true}
            onChange={this.handleInputChange}
            validations={["phone"]}
            disabled={true}
          />
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="Your Email address"
            type="email"
            placeholder="Your Email address"
            name="email"
            errorText="Please enter Your Email address"
            isRequired={false}
            disabled={true}
          />
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    personalInfo: selectPersonalData(state)
  }),
  {
    getUserData
  }
)(PersoanalInformation);
