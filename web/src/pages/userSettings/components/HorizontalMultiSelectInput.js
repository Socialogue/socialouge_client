import React, { Component } from "react";
import Down from "web/src/images/icons/down";
import ExcB from "web/src/images/icons/excIcon";
import SelectedImg from "web/src/images/flag.svg";
import {addOrRemove} from "core/utils/array";

class HorizontalMultiSelectInput extends Component {

  state = {
    clickOnSelect: false,
    selected: this.props.selectedText
  };

  dropDown = () => {
    this.setState({
      clickOnSelect: !this.state.clickOnSelect
    });
  };

  changeValue = (name, option) => {
    const old = this.props.values;
    const text = typeof option === 'string' ? option : option[this.props.valueKey];
    const values = addOrRemove(old, text);
    this.props.onChange(values);
    this.dropDown();
  };

  renderOption = (i, nameKey, option) => {
    const label = typeof option === 'string' ? option : option[this.props.labelKey];
    const value = typeof option === 'string' ? option : option[this.props.valueKey];
    const selected = this.props.values.includes(value);
    return (
      <div
        key={i}
        className="ctm-select-option"
        onClick={() =>
          this.changeValue(nameKey, option)
        }
      >
        {selected && <span>✓</span>} {label}
      </div>
    )
  }

  render() {

    const nameKey = this.props.name ? this.props.name : this.props.label;

    return (
      <div className="form-group row align-items-center mr-0">
        <div className="x-sm-text-fw6 text-uppercase col-12 col-md-5 pr-0 mb-2 mb-md-0">
          {this.props.label}
          {this.props.isRequired && <sup>*</sup>}
        </div>
        <div className="col-12 col-md-7 pr-0">
          {/* for error need this class (error-input) */}
          <div
            className={
              this.props.isRequired
                ? this.props.formInputClass + " error-input"
                : this.props.formInputClass
            }
          >
            <span className="prepend-input">
              <ExcB />
            </span>
            <div className="ctm-select py-1 ctm-select-main">
              <div className="ctm-select-txt mr-2" onClick={this.dropDown}>
                {this.props.imageRequired && (
                  <span className="select-img pr-2">
                    <img src={SelectedImg} />
                  </span>
                )}
                <span className="slct-txt">{this.state.selected}</span>
                <span className="select-arr">
                  <Down />
                </span>
              </div>
              <div
                className={
                  this.state.clickOnSelect
                    ? "ctm-select-box d-block"
                    : "ctm-select-box"
                }
              >
                <div className="box-cnt">
                  {this.props.options &&
                    this.props.options.map((option, i) => this.renderOption(i, nameKey, option))}
                </div>
              </div>
            </div>
            {this.props.isRequired && (
              <div className="err-txt x-sm-text-fw4 color-orange pl-3 pt-2">
                {this.props.errorText}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default HorizontalMultiSelectInput;
