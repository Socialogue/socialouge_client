import React, { Component } from "react";

class DeleteAcModal extends Component {
  render() {
    return (
      <div
        className="modal modal-1 d-block show"
        id="deleteAcModal"
        tabindex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-body">
              <div className="sm-text-fw6 text-uppercase mb-3">
                Are you sure you want to{" "}
                <span className="color-orange"> delete</span> your account?
              </div>
              <div className="sm-text-fw5-g mb-5">
                Your account will be deleted after 30 days, you can cancel by
                logging to your account again.
              </div>
              <div className="d-flex align-items-center">
                <button
                  className="btn border-btn btn-w-115"
                  onClick={() => this.props.change("")}
                >
                  No, I am not
                </button>

                <div className="w-100 text-right x-sm-text-fw6">
                  <button
                    className="btn border-btn btn-w-115 border-0"
                    onClick={() => {
                      this.props.change("");
                      this.props.deleteAccount();
                    }}
                  >
                    Yes, I am Sure
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DeleteAcModal;
