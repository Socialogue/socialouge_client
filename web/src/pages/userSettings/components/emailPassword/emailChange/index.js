import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import ArrowLeft from "../../../../../images/icons/arrowLeft";
import HorizontalInput from "./../../horizontalInput";
import { selectErrorMsg, selectSuccessMsg } from "core/selectors/settings";
import { errorAlert, successAlert } from "../../../../../utils/alerts";

class EmailChange extends Component {
  // componentDidUpdate = prevProps => {
  //   console.log("msg: ", this.props.successMsg, this.props.errorMsg);

  //   if (prevProps.successMsg !== this.props.successMsg) {
  //     if (this.props.successMsg !== "") {
  //       successAlert(this.props.successMsg);
  //     }
  //   }
  //   console.log("erroro: ", this.props.errorMsg);
  //   if (prevProps.errorMsg !== this.props.errorMsg) {
  //     if (this.props.errorMsg !== "") {
  //       errorAlert(this.props.errorMsg);
  //     }
  //   }
  // };

  render() {
    return (
      <div className="form-module-cnt-1 mx-auto mt-0 pt-4 pt-md-3 mb-5 pb-0 pb-md-5 position-relative">
        <span className="back-ep" onClick={() => this.props.change("")}>
          <ArrowLeft />
        </span>
        <div className="font-weight-bold text-uppercase mb-4">
          change email address
        </div>
        <div className="form form-1">
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="Your Email address"
            type="email"
            placeholder="Your Email address"
            name="email"
            errorText="Please enter Your Email address"
            isRequired={true}
            onChange={this.props.emailChanges}
            validations={["email"]}
          />
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="ENTER New email"
            type="email"
            placeholder="Your Email New address"
            name="newEmail"
            errorText="Please enter ENTER New email"
            isRequired={true}
            onChange={this.props.emailChanges}
            validations={["email"]}
          />
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    successMsg: selectSuccessMsg(state),
    errorMsg: selectErrorMsg(state)
  }),
  {}
)(EmailChange);
