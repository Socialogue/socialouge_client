import React, { Component } from "react";

import { selectToken, selectUser, selectOtpSendMsg } from "core/selectors/user";

import { logOut, resendOtp } from "core/actions/authActionCreators";
import { verifyOtpForEmailChange } from "core/actions/userSettingsActionCreators";
import { errorAlert, successAlert } from "../../../../../utils/alerts";
import { connect } from "react-redux";
import ArrowLeft from "../../../../../images/icons/arrowLeft";
import {
  selectEmailData,
  selectOtpSuccessMsg,
  selectOtpErrorMsg
} from "core/selectors/settings";
import { withRouter } from "react-router";
import { ROUTE_LOGIN } from "../../../../../routes/constants";

class EmailCheck extends Component {
  state = {
    otp: ""
  };

  componentDidUpdate = prevProps => {
    if (prevProps.otpSuccessMsg !== this.props.otpSuccessMsg) {
      console.log("this.props.otpSuccessMsg: ", this.props.otpSuccessMsg);
      if (this.props.otpSuccessMsg !== "") {
        this.props.logOut();
        this.props.history.push(ROUTE_LOGIN);
      }
    }
  };

  handleOtpChange = e => {
    const value = e.target.value;

    this.setState({
      otp: value
    });
  };

  onSubmit = async e => {
    if (this.state.otp.length < 4) {
      errorAlert("please enter otp currectly");
      return;
    }

    try {
      await this.props.verifyOtpForEmailChange(
        this.state.otp,
        this.props.emailData.username,
        this.props.emailData.newEmail
      );
    } catch (e) {
      console.log("catch", e);
    }
    return true;
  };

  handleResendOtp = () => {
    this.props.resendOtp(this.props.user.email, this.props.user.accountType);
  };

  render() {
    return (
      <div className="form-module-cnt-1 mx-auto mt-0 pt-4 pt-md-3 mb-5 pb-0 pb-md-5 position-relative">
        {/* <span className="back-ep" onClick={() => this.props.change("email")}>
          <ArrowLeft />
        </span>
        <div className="my-5 text-center pb-4">
          <div className="font-weight-bold text-uppercase mb-3">
            check your email
          </div>
          <div className="sm-text-fw5 mb-3">
            Please check your email to verify your new email.
          </div>
        </div> */}
        <span className="back-ep" onClick={() => this.props.change("email")}>
          <ArrowLeft />
        </span>
        <div className="form">
          <div className="form-group">
            <label className="x-sm-text-fw6 text-uppercase">OTP</label>
            <input
              className="form-control custom-input"
              placeholder="Enter your otp here"
              onChange={this.handleOtpChange}
            />
          </div>
        </div>
        <div className="form-group">
          <button
            className="btn black-bg-btn text-uppercase w-100"
            onClick={this.onSubmit}
          >
            Verify
          </button>
        </div>
        <span
          className="color-orange cursor-pointer font-weight-bold"
          onClick={this.handleResendOtp}
        >
          Resend OTP
        </span>
      </div>
    );
  }
}

export default connect(
  state => ({
    user: selectUser(state),
    emailData: selectEmailData(state),
    isLoggedIn: !!selectToken(state),
    auth: state.auth,
    otpSendMsg: selectOtpSendMsg(state),
    otpErrorMsg: selectOtpErrorMsg(state),
    otpSuccessMsg: selectOtpSuccessMsg(state)
  }),
  {
    verifyOtpForEmailChange,
    resendOtp,
    logOut
  }
)(withRouter(EmailCheck));
