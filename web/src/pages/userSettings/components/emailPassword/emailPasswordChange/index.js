import React, { Component } from "react";

class EmailPasswordChange extends Component {
  render() {
    return (
      <div className="form-module-cnt-1 mx-auto mt-0 pt-4 pt-md-3 mb-5 pb-0 pb-md-5">
        <div className="font-weight-bold text-uppercase mb-4">
          email & password
        </div>
        <div className="form form-1">
          <div className="row mb-4 mb-md-5 pb-2 pb-md-0">
            <div className="col-12 col-md-4 mb-2 mb-md-0 sm-text-fw6 text-uppercase">
              account email address
            </div>
            <div className="col-8 col-md-4">
              {this.props.personalInfo.email}
            </div>
            <div className="col-4 col-md-4 text-right">
              <button
                className="btn border-btn border-0 p-0 sm-text-fw4"
                onClick={() => this.props.view("email")}
              >
                Change
              </button>
            </div>
          </div>
          <div className="row mb-4 mb-md-5 pb-2 pb-md-0">
            <div className="col-12 col-md-4 mb-2 mb-md-0 sm-text-fw6 text-uppercase">
              change password
            </div>
            <div className="col-8 col-md-4">
              Password was changed -- time ago
            </div>
            <div className="col-4 col-md-4 text-right">
              <button
                className="btn border-btn border-0 p-0 sm-text-fw4"
                onClick={() => this.props.view("pass")}
              >
                Change
              </button>
            </div>
          </div>
          <div className="row mb-4 mb-md-5 pb-2 pb-md-0">
            <div className="col-8 col-md-4 mb-2 mb-md-0 sm-text-fw6 text-uppercase">
              delete account
            </div>
            <div className="col-8 col-md-4 d-none d-md-block" />
            <div className="col-4 col-md-4 text-right">
              <button
                className="btn border-btn border-0 p-0 sm-text-fw4 color-orange"
                onClick={() => this.props.view("delete")}
              >
                Delete
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EmailPasswordChange;
