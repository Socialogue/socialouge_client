import React, { Component } from "react";
import DeleteAcModal from "./deleteAcModal";
import EmailChange from "./emailChange";
import EmailCheck from "./emailCheck";
import EmailPasswordChange from "./emailPasswordChange";
import PasswordChange from "./passwordChange";
class EmailPassword extends Component {
  state = {
    mailOrPass: ""
  };

  change = value => {
    this.setState({ mailOrPass: value });
    this.props.popupChangeHandler(value);
  };

  render() {
    return (
      <div className="email-password">
        {this.state.mailOrPass === "" && (
          <EmailPasswordChange
            view={this.change}
            personalInfo={this.props.personalInfo}
          />
        )}
        {this.state.mailOrPass === "email" && (
          <EmailChange
            change={this.change}
            emailChanges={this.props.emailData}
          />
        )}
        {this.state.mailOrPass === "pass" && (
          <PasswordChange
            change={this.change}
            passChanges={this.props.passwordData}
          />
        )}

        {this.state.mailOrPass === "delete" && (
          <DeleteAcModal
            change={this.change}
            deleteAccount={this.props.deleteAccount}
          />
        )}
      </div>
    );
  }
}

export default EmailPassword;
