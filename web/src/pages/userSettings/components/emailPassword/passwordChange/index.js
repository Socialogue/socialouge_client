import React, { Component } from "react";
import { Link } from "react-router-dom";
import HorizontalInput from "./../../horizontalInput";
import ArrowLeft from "../../../../../images/icons/arrowLeft";
class PasswordChange extends Component {
  render() {
    return (
      <div className="form-module-cnt-1 mx-auto mt-0 pt-3 mb-md-5 mb-0 pb-3 pb-md-5 position-relative">
        <span className="back-ep" onClick={() => this.props.change("")}>
          <ArrowLeft />
        </span>
        <div className="font-weight-bold text-uppercase mb-4">
          change password
        </div>
        <div className="mb-4 row">
          <div className="sm-text-fw5-g col-12 col-md-6">
            There are many variations of passages of Lorem Ipsum available, but
            the majority have suffered alteration
          </div>
        </div>
        <div className="form form-1">
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="OLD PASSWORD"
            type="password"
            placeholder="Old Password"
            name="oldPass"
            errorText="Please enter Your Old Password"
            isRequired={true}
            onChange={this.props.passChanges}
            validations={["password"]}
          />
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="NEW PASSWORD"
            type="password"
            placeholder="Enter new password"
            name="newPass"
            errorText="Please enter new password"
            isRequired={true}
            onChange={this.props.passChanges}
            validations={["password"]}
          />
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="REPEAT NEW PASSWORD"
            type="password"
            placeholder="Repeat new password"
            name="confirmNewPass"
            errorText="Please enter Repeat new password"
            isRequired={true}
            onChange={this.props.passChanges}
            validations={["password"]}
          />
        </div>
      </div>
    );
  }
}

export default PasswordChange;
