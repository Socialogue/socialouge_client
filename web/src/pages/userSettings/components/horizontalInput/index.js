import React, { Component } from "react";
import ExcB from "./../../../../images/icons/excIcon";
import parsePhoneNumber from "libphonenumber-js";

class HorizontalInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      touched: false,
      value: props.value,
      errorText: ""
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {
      value: nextProps.value
    };
  }

  onChange = e => {
    const name = e.target.name;
    const value = e.target.value;

    this.setState({
      value
    });
    // validations
    if (this.isValid(value)) {
    }
    this.props.onChange(name, value);
  };

  isValid = value => {
    const { validations } = this.props;
    if (validations === undefined) return true;
    if (validations && Array.isArray(validations) && validations.length === 0)
      return true;

    // email
    if (validations.includes("email")) {
      if (!this.validateEmail(value)) {
        this.setState({
          errorText: "Please add a valid email address"
        });
        return false;
      }
    }

    // email or phone
    if (validations.includes("email-phone")) {
      if (!this.validateEmail(value) && !parsePhoneNumber(value)) {
        this.setState({
          errorText: "Please enter a valid email or phone"
        });
        return false;
      }
    }

    // phone
    if (validations.includes("phone")) {
      if (!parsePhoneNumber(value)) {
        this.setState({
          errorText: "Please enter a valid phone"
        });
        return false;
      }
    }

    //password
    if (validations.includes("password")) {
      if (value.length < 6) {
        this.setState({
          errorText: "Password must be at least 6 characters"
        });
        return false;
      }
    }

    // if code reach this point, that means all valdiation check has passed
    this.setState({
      errorText: ""
    });
    return true;
  };

  validateEmail = email => {
    const re = /\S+@\S+\.\S+/;
    return re.test(email);
  };

  render() {
    const { touched, value, errorText } = this.state;
    const { isRequired } = this.props;
    const error =
      touched && value === "" && isRequired ? this.props.errorText : errorText;

    return (
      <div className="form-group row align-items-center mr-0">
        <div className="x-sm-text-fw6 text-uppercase col-12 col-md-5 pr-0 mb-2 mb-md-0">
          {this.props.label}
          {this.props.isRequired && <sup>*</sup>}
        </div>
        <div className="col-12 col-md-7 pr-0">
          {/* for error need this class (error-input) */}
          <div
            className={
              this.props.isRequired && error !== ""
                ? this.props.formInputClass + " error-input"
                : this.props.formInputClass
            }
          >
            <span className="prepend-input">
              <ExcB />
            </span>
            <input
              type={this.props.type}
              className="form-control custom-input"
              placeholder={this.props.placeholder}
              name={this.props.name}
              value={value}
              onChange={this.onChange}
              onBlur={() => this.setState({ touched: true })}
              disabled={this.props.disabled | false}
            />
            {/* <span className="append-input">{this.props.appendText}</span> */}
            {this.props.isRequired && (
              <div className="err-txt x-sm-text-fw4 color-orange pl-3 pt-2">
                {error}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default HorizontalInput;
