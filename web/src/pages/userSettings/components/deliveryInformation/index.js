import { getDeliveryData } from 'core/actions/userSettingsActionCreators'
import { selectDeliveryData } from 'core/selectors/settings'
import { days, generateArrayOfYears } from 'core/utils/dummyDate'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import ArrowRight from '../../../../images/icons/arrowRight'
import HorizontalInput from './../horizontalInput'
import HorizontalSelectOption from './../horizontalSelectOption'

class DeliveryInformation extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      surname: '',
      birthday: '1',
      birthmonth: 'January',
      birthyear: '',
      country: 'UK',
      address: '',
      zipCode: '',
      phone: '',
      email: '',
      billing: false,
      years: [],
      days: [],
      deliveryData: [],
      deliveryId: '',
    }
  }

  getState = () => {
    return this.state
  }

  componentDidMount = () => {
    const years = generateArrayOfYears()
    this.setState({ years: years })
    this.props.getDeliveryData()
  }

  componentDidUpdate = (prevProps) => {
    if (
      JSON.stringify(prevProps.deliveryData) !==
        JSON.stringify(this.props.deliveryData) ||
      this.state.birthyear == ''
    ) {
      this.setState({
        deliveryData: this.props.deliveryData,
        birthyear: '2004',
      })
    }
  }

  handleInputChange = (name, value) => {
    this.setState({
      [name]: value,
    })
    this.props.onChange(name, value)
  }

  handleBirthMonth = (name, value, index) => {
    const dayLength = days(index + 1, this.state.birthyear)
    let dayArr = []
    for (let index = 1; index <= dayLength; index++) {
      dayArr.push(index)
    }
    this.setState({
      days: dayArr,
    })
  }

  handleBirthYear = (name, value, index) => {
    this.setState({
      birthyear: value,
    })
  }

  onClickBilling = (e) => {
    const name = e.target.name
    const value = e.target.checked
    this.setState({
      [name]: value,
    })
    this.props.onChange(name, value)
  }

  onSelectAddress = (item) => {
    this.setState(
      {
        name: item.name,
        surname: item.surname,
        country: item.country,
        address: item.address,
        zipCode: item.zipCode,
        phone: item.phone,
        // email: item.email,
        deliveryId: item._id,
        billing: item.billing,
      },
      () => {
        this.props.onEditDeliveryData(item)
      },
    )
  }

  onSelectAnotherAddress = () => {
    this.setState(
      {
        name: '',
        surname: '',
        country: '',
        address: '',
        zipCode: '',
        phone: '',
        email: '',
        deliveryId: '',
        billing: '',
      },
      () => {
        this.props.onEditDeliveryData(undefined)
      },
    )
  }

  render() {
    return (
      <div className="form-module-cnt-1 mx-auto mt-0 pt-3 mb-5 pb-0 pb-md-0">
        <div className="mb-4">
          <div className="font-weight-bold text-uppercase ">
            delivery information
          </div>
          <div className="text-right">
            <label className="ctm-container check-right">
              Use a billing address
              <input
                type="checkbox"
                name="billing"
                checked={this.state.billing}
                onChange={this.onClickBilling}
              />
              <span className="checkmark" />
            </label>
          </div>
        </div>
        <div className="form form-1">
          <div className="form-group row  mr-0 sh-add">
            <div className="x-sm-text-fw6 text-uppercase col-12 col-md-5 pr-0 mb-4 mb-md-0">
              Shifting address
              <sup>*</sup>
            </div>
            <div className="col-12 col-md-7 pr-0">
              <div className="row ml-0">
                {this.state.deliveryData &&
                  this.state.deliveryData.map((item) => {
                    return (
                      <div className="col-6 col-lg-4 mb-2 pl-0" key={item._id}>
                        <label className="ctm-container radio-ctm d-inline-block">
                          <div className="sm-text-fw6 text-uppercase">
                            {item.name}
                          </div>
                          <div className="sm-text-fw6">{item.address}</div>
                          <input
                            type="radio"
                            name="address"
                            onClick={() => this.onSelectAddress(item)}
                          />
                          <span className="checkmark" />
                        </label>
                      </div>
                    )
                  })}
              </div>
            </div>
          </div>
        </div>
        <div className="form form-1">
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="Name"
            type="text"
            placeholder="Your Name"
            name="name"
            value={this.state.name}
            errorText="Please enter your Name"
            isRequired={true}
            onChange={this.handleInputChange}
          />
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="Surename"
            type="text"
            placeholder="Your Surename"
            name="surname"
            value={this.state.surname}
            errorText="Please enter your Surename"
            isRequired={true}
            onChange={this.handleInputChange}
          />
          {/* <div className="form-group row align-items-center mr-0">
            <div className="x-sm-text-fw6 text-uppercase col-12 col-md-5 pr-0">
              Date of birth <sup> *</sup>
            </div>
            <div className="col-12 col-md-7 pr-0">
              <div className="form-input position-relative">
                <span className="prepend-input">
                  <ExcB />
                </span>
                <div className="row mr-0">
                  <div className="col-4 pr-0">
                    <CustomSelectOption
                      imageRequired={false}
                      selectedText={this.state.birthyear}
                      label="year"
                      name="birthyear"
                      options={this.state.years}
                      onChange={this.handleBirthYear}
                    />
                  </div>
                  <div className="col-4 pr-0">
                    <CustomSelectOption
                      imageRequired={false}
                      selectedText="January"
                      options={months}
                      label="month"
                      name="birthmonth"
                      onChange={this.handleBirthMonth}
                    />
                  </div>
                  <div className="col-4 pr-0">
                    <CustomSelectOption
                      imageRequired={false}
                      selectedText="1"
                      label="day"
                      name="birthday"
                      options={this.state.days}
                      onChange={this.handleInputChange}
                    />
                  </div>
                </div>
                <div className="err-txt x-sm-text-fw4 color-orange pl-3 pt-2">
                  Please enter your Country name
                </div>
              </div>
            </div>
          </div> */}
        </div>
        <div className="form form-1 mb-0 pb-0">
          <HorizontalSelectOption
            formInputClass="form-input position-relative"
            label="Country"
            errorText=""
            isRequired={true}
            imageRequired={true}
            selectedText={this.state.country}
            options={['UK', 'IT', 'LT', 'US']}
            onChange={this.handleInputChange}
          />
          <HorizontalInput
            formInputClass="form-input position-relative form-input-append"
            label="address"
            type="text"
            placeholder="Your address"
            name="address"
            value={this.state.address}
            errorText="Please enter your address"
            appendText="Find address"
            isRequired={true}
            onChange={this.handleInputChange}
          />
          <HorizontalInput
            formInputClass="form-input position-relative form-input-append"
            label="Zip code"
            type="text"
            placeholder="Your Zip code"
            name="zipCode"
            value={this.state.zipCode}
            errorText="Please enter your Zip code"
            appendText="Find code"
            isRequired={true}
            onChange={this.handleInputChange}
          />
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="Your phone number"
            type="text"
            placeholder="Your phone number"
            name="phone"
            value={this.state.phone}
            errorText="Please enter your Your phone number"
            isRequired={true}
            onChange={this.handleInputChange}
            validations={['phone']}
          />
          {/* <HorizontalInput
            formInputClass="form-input position-relative"
            label="Your Email address"
            type="email"
            placeholder="Your Email address"
            name="email"
            value={this.state.email}
            errorText="Please enter Your Email address"
            isRequired={true}
            onChange={this.handleInputChange}
            validations={['email']}
          /> */}
        </div>
        <div className="text-right mt-5 pb-5">
          <button
            className="btn border-btn border-0 p-0"
            onClick={this.onSelectAnotherAddress}
          >
            Add another delivery address <ArrowRight />
          </button>
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    deliveryData: selectDeliveryData(state),
  }),
  { getDeliveryData },
)(DeliveryInformation)
