import React, { Component } from "react";
import Down from "web/src/images/icons/down";
import ExcB from "web/src/images/icons/excIcon";
import SelectedImg from "web/src/images/flag.svg";

class HorizontalSingleSelectInput extends Component {

  state = {
    clickOnSelect: false,
    selected: null
  };

  dropDown = () => {
    this.setState({
      clickOnSelect: !this.state.clickOnSelect
    });
  };

  changeValue = (option) => {
    this.setState({
      selected: option
    });
    this.props.onChange(this.props.name, option[this.props.nameKey]);
    this.dropDown();
  };

  render() {

    const text = this.state.selected ? this.state.selected[this.props.labelkey] : this.props.placeholder;

    return (
      <div className="form-group row align-items-center mr-0">
        <div className="x-sm-text-fw6 text-uppercase col-12 col-md-5 pr-0 mb-2 mb-md-0">
          {this.props.label}
          {this.props.isRequired && <sup>*</sup>}
        </div>
        <div className="col-12 col-md-7 pr-0">
          {/* for error need this class (error-input) */}
          <div
            className={
              this.props.isRequired
                ? this.props.formInputClass + " error-input"
                : this.props.formInputClass
            }
          >
            <span className="prepend-input">
              <ExcB />
            </span>
            <div className="ctm-select py-1 ctm-select-main">
              <div className="ctm-select-txt mr-2" onClick={this.dropDown}>
                {this.props.imageRequired && (
                  <span className="select-img pr-2">
                    <img src={SelectedImg} />
                  </span>
                )}
                <span className="slct-txt">{text}</span>
                <span className="select-arr">
                  <Down />
                </span>
              </div>
              <div
                className={
                  this.state.clickOnSelect
                    ? "ctm-select-box d-block"
                    : "ctm-select-box"
                }
              >
                <div className="box-cnt">
                  {this.props.options &&
                    this.props.options.map((option, i) => {
                      return (
                        <div
                          key={`ctm-select-option-${i}`}
                          className="ctm-select-option"
                          onClick={() =>
                            this.changeValue(option)
                          }
                        >
                          {option[this.props.labelkey]}
                        </div>
                      );
                    })}
                </div>
              </div>
            </div>
            {this.props.isRequired && (
              <div className="err-txt x-sm-text-fw4 color-orange pl-3 pt-2">
                {this.props.errorText}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default HorizontalSingleSelectInput;
