import React, { Component } from "react";
import Down from "./../../../../images/icons/down";
import "./filter.scss";
class Filter extends Component {
  state = {
    filterBox: false,
    filterGenderBox: false,
    filterCountryBox: false,
    filterCityBox: false,
    filterShopBox: false
  };
  render() {
    return (
      <div
        className={
          this.state.filterBox
            ? "fixed-filter d-none d-md-block shop-filter active"
            : "fixed-filter d-none d-md-block shop-filter"
        }
      >
        {/* in fixed-filter class need active class */}
        <div
          onClick={() =>
            this.setState({
              filterBox: !this.state.filterBox
            })
          }
          className="sm-text-fw6 text-uppercase mb-3 ff-txt cursor-pointer"
        >
          Filter <Down />
        </div>
        <div className="filter-ul">
          {/* li need active class */}
          <div
            className={
              this.state.filterGenderBox
                ? "filter-li sm-text-fw6 text-uppercase active"
                : "filter-li sm-text-fw6 text-uppercase "
            }
          >
            <div
              onClick={() =>
                this.setState({
                  filterGenderBox: !this.state.filterGenderBox
                })
              }
              className="mb-3 cursor-pointer"
            >
              Gender <Down />
            </div>
            <div className="li-cnt">
              <label className="ctm-container d-block  mb-1">
                Male
                <input type="checkbox" />
                <span className="checkmark" />
                <span className="sm-text-fw6-g float-right">(12)</span>
              </label>
              <label className="ctm-container d-block mb-1">
                FeMale
                <input type="checkbox" />
                <span className="checkmark" />
                <span className="sm-text-fw6-g float-right">(12)</span>
              </label>
              <label className="ctm-container d-block mb-1">
                unisex
                <input type="checkbox" />
                <span className="checkmark" />
                <span className="sm-text-fw6-g float-right">(122)</span>
              </label>
            </div>
          </div>
          <div
            className={
              this.state.filterCountryBox
                ? "filter-li sm-text-fw6 text-uppercase active"
                : "filter-li sm-text-fw6 text-uppercase "
            }
          >
            <div
              onClick={() =>
                this.setState({
                  filterCountryBox: !this.state.filterCountryBox
                })
              }
              className="mb-3 cursor-pointer"
            >
              country <Down />
            </div>
            <div className="li-cnt">
              <label className="ctm-container d-block  mb-1">
                uk
                <input type="checkbox" />
                <span className="checkmark" />
                <span className="sm-text-fw6-g float-right">(12)</span>
              </label>
              <label className="ctm-container d-block mb-1">
                Lt
                <input type="checkbox" />
                <span className="checkmark" />
                <span className="sm-text-fw6-g float-right">(12)</span>
              </label>
            </div>
          </div>
          <div
            className={
              this.state.filterCityBox
                ? "filter-li sm-text-fw6 text-uppercase active"
                : "filter-li sm-text-fw6 text-uppercase "
            }
          >
            <div
              onClick={() =>
                this.setState({
                  filterCityBox: !this.state.filterCityBox
                })
              }
              className="mb-3 cursor-pointer"
            >
              city <Down />
            </div>
            <div className="li-cnt">
              <label className="ctm-container d-block  mb-1">
                city
                <input type="checkbox" />
                <span className="checkmark" />
                <span className="sm-text-fw6-g float-right">(12)</span>
              </label>
              <label className="ctm-container d-block mb-1">
                city
                <input type="checkbox" />
                <span className="checkmark" />
                <span className="sm-text-fw6-g float-right">(12)</span>
              </label>
            </div>
          </div>
          <div
            className={
              this.state.filterShopBox
                ? "filter-li sm-text-fw6 text-uppercase active"
                : "filter-li sm-text-fw6 text-uppercase "
            }
          >
            <div
              onClick={() =>
                this.setState({
                  filterShopBox: !this.state.filterShopBox
                })
              }
              className="mb-3 cursor-pointer"
            >
              shop <Down />
            </div>
            <div className="li-cnt">
              <label className="ctm-container d-block  mb-1">
                onliine
                <input type="checkbox" />
                <span className="checkmark" />
                <span className="sm-text-fw6-g float-right">(12)</span>
              </label>
              <label className="ctm-container d-block mb-1">
                offline
                <input type="checkbox" />
                <span className="checkmark" />
                <span className="sm-text-fw6-g float-right">(12)</span>
              </label>
              <label className="ctm-container d-block mb-1">
                I follow
                <input type="checkbox" />
                <span className="checkmark" />
                <span className="sm-text-fw6-g float-right">(12)</span>
              </label>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Filter;
