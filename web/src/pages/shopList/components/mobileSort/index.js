import React, { Component } from 'react';
import Close from '../../../../images/icons/close';
import Down from '../../../../images/icons/down';

class MobileSort extends Component {
    state = {
        showMobileS: false
      };
    render() {
        return (
            <div className="mobile-sort-filter mobile-pro-sort d-inline-block d-md-none">
            <div
              onClick={() =>
                this.setState({
                  showMobileS: !this.state.showMobileS
                })
              }
              className="sm-text-fw6 text-uppercase ff-txt cursor-pointer"
            >
              <Down /> sort
            </div>
            <div
              className={
                this.state.showMobileS
                  ? "mobile-filter-box d-block"
                  : "mobile-filter-box d-none"
              }
            >
              <div className="">
                <span
                  onClick={() =>
                    this.setState({
                      showMobileS: !this.state.showMobileS
                    })
                  }
                  className="cursor-pointer"
                >
                  <Close />
                </span>
                <span className="sm-text-fw6 text-uppercase ff-txt float-right">
                  <Down /> sort
                </span>
              </div>
              <div className="filter-ul">
                <div className="filter-li pb-2 sm-text-fw6 text-uppercase active">
                  <div className="li-cnt mb-3">
                    <label className="ctm-container d-block  pb-2">
                      new
                      <input type="checkbox" />
                      <span className="checkmark" />
                      <span className="sm-text-fw6-g float-right">(12)</span>
                    </label>
                    <label className="ctm-container d-block pb-2">
                      Popular
                      <input type="checkbox" />
                      <span className="checkmark" />
                      <span className="sm-text-fw6-g float-right">(12)</span>
                    </label>
                    <label className="ctm-container d-block pb-2">
                      Trending
                      <input type="checkbox" />
                      <span className="checkmark" />
                      <span className="sm-text-fw6-g float-right">(12)</span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
    }
}

export default MobileSort;