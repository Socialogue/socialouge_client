import { getImageOriginalUrl } from "core/utils/helpers";
import React, { Component } from "react";
import Img from "./../../../../images/eco-img.webp";
import "./../shop/shop.scss";

class ShopViewThree extends Component {
  render() {
    const { shop } = this.props;

    return (
      <div className="col-12 col-md-6 col-lg-4 shop-cnt shop-cnt-view-2">
        <div className="pro-cnt mb-3 align-items-center">
          <div
            className="pro-img pro-img-lg cursor-pointer"
            onClick={() => this.props.shopProfile(shop._id)}
          >
            <img
              className="cover"
              src={
                shop.profileImage ? getImageOriginalUrl(shop.coverImage) : Img
              }
            />
          </div>
          <div className="pro-txt-cnt pro-txt-cnt-lg">
            <div
              className="md-title md-title-res25 mb-1 cursor-pointer text-uppercase"
              onClick={() => this.props.shopProfile(shop._id)}
            >
              {shop.shopName}
            </div>
            <div className="sm-text-fw6 text-uppercase">
              {shop.totalFollowers && shop.totalFollowers | 0} Followers
            </div>
          </div>
        </div>
        <div className="mb-3">
          <button className="btn border-btn btn-w-115">Follow</button>
        </div>
        <div className="row mr-0">
          {shop.products.map((product, i) => {
            if (i == 2) {
              return;
            }
            return (
              <div className="col-6 pr-0" key={product._id}>
                <div className="show-img-pp show-img-pp-pb-140 mb-3">
                  <div
                    className="show-img-p cursor-pointer"
                    onClick={() =>
                      this.props.viewProduct(product.slug, product._id)
                    }
                  >
                    <img className="cover" src={product.mainImage} />
                  </div>
                </div>
                <div
                  className="font-weight-bold cursor-pointer text-uppercase mb-2 cursor-pointer"
                  onClick={() =>
                    this.props.viewProduct(product.slug, product._id)
                  }
                >
                  {product.title}
                </div>
                <div className="font-weight-5">
                  {product.variations[0].price} €
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default ShopViewThree;
