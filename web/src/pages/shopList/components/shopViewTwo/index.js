import { getImageOriginalUrl } from 'core/utils/helpers'
import React, { Component } from 'react'
import Img from './../../../../images/eco-img.webp'
import './../shop/shop.scss'
class ShopViewTwo extends Component {
  render() {
    const { shop } = this.props
    if (!shop) {
      return null
    }
    return (
      <div className="col-12 col-md-6 shop-cnt shop-cnt-view-2">
        <div
          className="shop-cover cursor-pointer"
          onClick={() => this.props.shopProfile(shop._id)}
        >
          <img
            className="cover"
            src={shop.coverImage ? getImageOriginalUrl(shop.coverImage) : Img}
          />
        </div>
        <div className="pro-cnt shop-pro-cnt">
          <div
            className="pro-img pro-img-xl shop-pro-img cursor-pointer"
            onClick={() => this.props.shopProfile(shop._id)}
          >
            <img
              className="cover"
              src={
                shop.profileImage ? getImageOriginalUrl(shop.profileImage) : Img
              }
            />
          </div>
          <div className="pro-txt-cnt pro-txt-cnt-xl pt-3 pr-2">
            <div className="font-weight-bold mb-1">
              <span
                className="cursor-pointer text-uppercase"
                onClick={() => this.props.shopProfile(shop._id)}
              >
                {shop.shopName}
              </span>
              <button className="btn border-btn btn-w-115 float-right">
                Follow
              </button>
            </div>
            <div className="sm-text-fw6 text-uppercase v2-fol-txt">
              {shop.totalFollowers && shop.totalFollowers | 0} Followers
            </div>
          </div>
        </div>
        <div className="row mr-0 mt-3">
          {shop.products &&
            shop.products.map((product) => {
              return (
                <div className="col-4 pr-0" key={product._id}>
                  <div className="show-img-pp show-img-pp-pb-140">
                    <div
                      className="show-img-p cursor-pointer"
                      onClick={() =>
                        this.props.viewProduct(product.slug, product._id)
                      }
                    >
                      <img className="cover" src={product.mainImage} />
                    </div>
                  </div>
                </div>
              )
            })}
        </div>
      </div>
    )
  }
}

export default ShopViewTwo
