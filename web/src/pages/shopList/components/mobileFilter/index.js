import React, { Component } from "react";
import Close from "../../../../images/icons/close";
import Down from "../../../../images/icons/down";
class MobileFilter extends Component {
  state = {
    showMobileF: false
  };
  render() {
    return (
      <div className="mobile-sort-filter mobile-pro-filter d-inline-block d-md-none">
        <div
          onClick={() =>
            this.setState({
              showMobileF: !this.state.showMobileF
            })
          }
          className="sm-text-fw6 text-uppercase ff-txt cursor-pointer"
        >
          Filter <Down />
        </div>
        <div
          className={
            this.state.showMobileF
              ? "mobile-filter-box d-block"
              : "mobile-filter-box d-none"
          }
        >
          <div className="mb-3">
            <span className="sm-text-fw6 text-uppercase ff-txt">
              Filter <Down />
            </span>
            <span
              onClick={() =>
                this.setState({
                  showMobileF: !this.state.showMobileF
                })
              }
              className="float-right cursor-pointer"
            >
              <Close />
            </span>
          </div>
          <div className="filter-ul">
            {/* li need active class */}
            <div className="filter-li pb-2 sm-text-fw6 text-uppercase active">
              <div className="mb-2 pb-1 cursor-pointer">
                Gender <Down />
              </div>
              <div className="li-cnt mb-3">
                <label className="ctm-container d-block  pb-2">
                  Male
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
                <label className="ctm-container d-block pb-2">
                  FeMale
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
                <label className="ctm-container d-block pb-2">
                  Unisex
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
              </div>
            </div>

            <div className="filter-li pb-2 sm-text-fw6 text-uppercase">
              <div className="mb-2 pb-1 cursor-pointer">
                country <Down />
              </div>
              <div className="li-cnt mb-3">
                <label className="ctm-container d-block  mb-1">
                  Uk
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
                <label className="ctm-container d-block mb-1">
                  Lt
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
              </div>
            </div>
            <div className="filter-li pb-2 sm-text-fw6 text-uppercase">
              <div className="mb-2 pb-1 cursor-pointer">
                city <Down />
              </div>
              <div className="li-cnt mb-3">
                <label className="ctm-container d-block  mb-1">
                  City
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
                <label className="ctm-container d-block mb-1">
                  City
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
              </div>
            </div>
            <div className="filter-li pb-2 sm-text-fw6 text-uppercase">
              <div className="mb-2 pb-1 cursor-pointer">
                shop <Down />
              </div>
              <div className="li-cnt mb-3">
                <label className="ctm-container d-block  mb-1">
                  onliine
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
                <label className="ctm-container d-block mb-1">
                  offline
                  <input type="checkbox" />
                  <span className="checkmark" />
                  <span className="sm-text-fw6-g float-right">(12)</span>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MobileFilter;
