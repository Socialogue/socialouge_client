import { getImageOriginalUrl } from 'core/utils/helpers'
import React, { Component } from 'react'
import Img from './../../../../images/eco-img.webp'
import './shop.scss'
class Shop extends Component {
  render() {
    const { shop } = this.props
    return (
      <div className="shop-cnt">
        <div
          className="shop-cover cursor-pointer"
          onClick={() => this.props.shopProfile(shop._id)}
        >
          <img
            className="cover"
            src={shop.coverImage ? getImageOriginalUrl(shop.coverImage) : Img}
          />
        </div>
        <div className="row">
          <div className="col-12 col-lg-5">
            <div className="pro-cnt shop-pro-cnt d-flex d-lg-block">
              <div
                className="pro-img pro-img-xxxl shop-pro-img cursor-pointer"
                onClick={() => this.props.shopProfile(shop._id)}
              >
                <img
                  className="cover"
                  src={
                    shop.coverImage ? getImageOriginalUrl(shop.coverImage) : Img
                  }
                />
              </div>
              <div className="pro-txt-cnt pro-txt-cnt-xxxl pl-3 pl-lg-0 pt-3 pt-lg-0">
                <div
                  className="md-title md-title-res25 mb-1 cursor-pointer text-uppercase"
                  onClick={() => this.props.shopProfile(shop._id)}
                >
                  {shop.shopName}
                </div>
                <div className="sm-text-fw6 text-uppercase mb-3">
                  {shop.totalFollowers && shop.totalFollowers | 0} Followers
                </div>
                <div>
                  <button className="btn border-btn btn-w-115 mr-0 mr-sm-3 mb-2 mb-sm-0">
                    Follow
                  </button>
                  <br className="d-block d-sm-none" />
                  <button className="btn border-btn">Contact Seller</button>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-lg-7">
            <div className="sm-text-fw6 text-uppercase my-3">
              Newest products
            </div>
            <div className="row mr-0">
              {shop.products.map((product) => {
                return (
                  <div className="col-4 pr-0" key={product._id}>
                    <div className="show-img-pp show-img-pp-pb-140 mb-3 mb-md-3">
                      <div
                        className="show-img-p cursor-pointer"
                        onClick={() =>
                          this.props.viewProduct(product.slug, product._id)
                        }
                      >
                        <img className="cover" src={product.mainImage} />
                      </div>
                    </div>
                    <div
                      className="font-weight-bold cursor-pointer text-uppercase mb-2 d-none d-md-inline-block cursor-pointer"
                      onClick={() =>
                        this.props.viewProduct(product.slug, product._id)
                      }
                    >
                      {product.title}
                    </div>
                    <div className="font-weight-5 d-none d-md-inline-block">
                      {product.variations[0].price} €
                    </div>
                  </div>
                )
              })}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Shop
