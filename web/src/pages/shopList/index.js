import React, { Component } from 'react'
import Header from '../home/Header/Header'
import { Link } from 'react-router-dom'
import Shop from './components/shop'
import ShopViewTwo from './components/shopViewTwo'
import ShopViewThree from './components/shopViewThree'
import Footer from '../home/Footer/Footer'
import SortView from '../home/sortView'
import Filter from './components/filter'
import { selectUser, selectViewType } from 'core/selectors/user'
import { SORT_VIEW_BOTTOM, SORT_VIEW_MID, SORT_VIEW_TOP } from 'core/constants'
import { connect } from 'react-redux'
import InfiniteScroll from 'react-infinite-scroll-component'
import FixedFooter from '../Common/fixedFooter'
import MobileSort from '../shopProfile/products/mobileSort'
import MobileFilter from './components/mobileFilter'
import {
  selectAllShops,
  selectAllShopsCount,
  selectErrorMsg,
} from 'core/selectors/shops'
import { getAllShops } from 'core/actions/shopActionCreators'
import { errorAlert } from '../../utils/alerts'
import {
  ROUTE_PRODUCT_DETAILS,
  ROUTE_SHOP_PROFILE_PRODUCTS,
} from '../../routes/constants'

class ShopList extends Component {
  state = {
    allShops: [],
    count: 10,
    start: 0,
    setData: false,
  }
  componentDidMount = () => {
    this.props.getAllShops(this.state.count, this.state.start)
  }

  componentDidUpdate = (prevProps) => {
    if (
      JSON.stringify(prevProps.errorMsg) !== JSON.stringify(this.props.errorMsg)
    ) {
      if (this.props.errorMsg !== '') {
        errorAlert(this.props.errorMsg)
        // this.props.history.goBack();
      }
    }

    if (
      JSON.stringify(prevProps.allShops) !==
        JSON.stringify(this.props.allShops) ||
      !this.state.setData
    ) {
      if (this.props.allShops) {
        this.setState({
          allShops: this.state.allShops.concat(this.props.allShops),
          setData: true,
        })
      }
    }
  }

  viewProduct = (slug, productId) => {
    this.props.history.push(
      ROUTE_PRODUCT_DETAILS.replace(':slug', slug).replace(
        ':productId',
        productId,
      ),
    )
  }

  shopProfile = (shopId) => {
    const route = ROUTE_SHOP_PROFILE_PRODUCTS.replace(':id', shopId)
    this.props.history.push(route)
  }

  fetchMoreData = () => {
    this.setState({ start: this.state.start + this.state.count })
    this.props.getAllShops(this.state.count, this.state.start)
  }

  render() {
    // const { allShops } = this.props;
    const { allShops } = this.state

    return (
      <div>
        <Header />
        <div className="shop-content max-content-width-1 px-4">
          <nav aria-label="breadcrumb" className="pt-4 pt-md-0 mb-3 mb-md-5">
            <ol className="breadcrumb custom-breadcrumb mt-2 mt-md-0">
              <li className="breadcrumb-item">
                <Link to="#">Home</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to="#">Settings</Link>
              </li>
              <li className="breadcrumb-item active">Shop list</li>
            </ol>
          </nav>
          <div className="mb-0 mb-md-5 pb-3">
            <div className="x-md-title sx-md-title">
              all shops
              <span className="dot-r" />
            </div>
            {/* <div className="x-sm-text-fw4-g d-none d-md-block">
              Your product description
            </div> */}
          </div>
          <InfiniteScroll
            dataLength={allShops.length}
            next={this.fetchMoreData}
            hasMore={this.props.allShopCount > allShops.length ? true : false}
            loader={<h4>Loading...</h4>}
          >
            {this.props.sortView == SORT_VIEW_BOTTOM && (
              <div className="row">
                {allShops.map((item, i) => {
                  return (
                    <ShopViewThree
                      key={item._id}
                      shop={item}
                      shopProfile={this.shopProfile}
                      viewProduct={this.viewProduct}
                    />
                  )
                })}
              </div>
            )}
            {this.props.sortView == SORT_VIEW_TOP && (
              <div className="shop-view-1 mb-0 mb-md-5 pb-0 pb-md-5">
                {allShops.map((item, i) => {
                  return (
                    <Shop
                      key={item._id}
                      shop={item}
                      shopProfile={this.shopProfile}
                      viewProduct={this.viewProduct}
                    />
                  )
                })}
              </div>
            )}
            {this.props.sortView == SORT_VIEW_MID && (
              <div className="shop-view-2 mb-0 mb-md-5 pb-0 pb-md-5">
                <div className="row">
                  {allShops.map((item, i) => {
                    return (
                      <ShopViewTwo
                        key={item._id}
                        shop={item}
                        shopProfile={this.shopProfile}
                        viewProduct={this.viewProduct}
                      />
                    )
                  })}
                </div>
              </div>
            )}
          </InfiniteScroll>
          <div className="shop-view-2 shop-view-3 mb-5 py-0 py-md-5" />
        </div>
        <SortView />
        <Filter />
        <MobileSort />
        <MobileFilter />
        {this.props.sortView !== SORT_VIEW_TOP ? (
          <div>
            {' '}
            <Footer />
            <div className="ab-mf px-4 mb-4 d-block d-md-none">
              <FixedFooter />
            </div>
          </div>
        ) : (
          <div className="ab-mf px-4 mb-4 d-block">
            <FixedFooter />
          </div>
        )}
      </div>
    )
  }
}

export default connect(
  (state) => ({
    user: selectUser(state),
    sortView: selectViewType(state),
    allShops: selectAllShops(state),
    errorMsg: selectErrorMsg(state),
    allShopCount: selectAllShopsCount(state),
  }),
  {
    getAllShops,
  },
)(ShopList)
