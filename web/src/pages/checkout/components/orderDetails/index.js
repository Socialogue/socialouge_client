import { getImageOriginalUrl } from 'core/utils/helpers'
import React, { Component } from 'react'
import Close from '../../../../images/icons/close'
import Excla from '../../../../images/icons/excla'
import ProImg from '../../../../images/login-img1.webp'

class OrderDetails extends Component {
  render() {
    const { order } = this.props
    if (!order) {
      return null
    }
    if (!order.products[0].shopId) {
      return null
    }
    return (
      <div className="">
        {/* for disable this box need to added box-disable class in checkout-col-1 class */}
        <div className="font-weight-bold text-uppercase mb-3">
          “{order.products[0].product.shopId.shopName}” Ordering overview
        </div>
        <div className="overview-box">
          <div className="cart-sm-box">
            <div className="box-ul">
              {order.products.map((or, index) => {
                return (
                  <div className="box-li pl-4" key={index}>
                    <span
                      className="close-cart scale-hover cursor-pointer"
                      onClick={() =>
                        this.props.removeItem(index, this.props.orderIndex)
                      }
                    >
                      <Close />
                    </span>
                    <div className="pro-cnt align-items-center ">
                      <div className="pro-img pro-img-r0">
                        <img
                          className="cover"
                          src={getImageOriginalUrl(or.color.colorImage)}
                          alt="img"
                        />
                      </div>
                      <div className="pro-txt-cnt pro-txt-cnt-r0">
                        <div className="font-weight-bold text-uppercase mb-1 d-flex align-items-center">
                          <span className="w-100 mr-2"> {or.quantity}x {or.product.title.slice(0, 50)}</span>
                          <span className="text-nowrap">€ {or.totalPrice}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                )
              })}
            </div>
          </div>
          <div className="overview-cnt">
            <div className="sm-text-fw6 text-uppercase mb-3">
              <span className="mr-2">Products price: </span>
              <span className="float-right">€ {order.productsPrice}</span>
            </div>
            <div className="sm-text-fw6 text-uppercase mb-3">
              <span className="mr-2">Vat: </span>
              <span className="float-right">€ {order.vat}</span>
            </div>
            <div className="sm-text-fw6 text-uppercase mb-3">
              <span className="mr-2">Delivery: </span>
              <span className="float-right">€ {order.delivery}</span>
            </div>
            <div className="sm-text-fw6 text-uppercase mb-3 cart-total-price d-flex border-0">
              <span className="w-100" />
              <span className="text-nowrap">Total: € {order.total}</span>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default OrderDetails
