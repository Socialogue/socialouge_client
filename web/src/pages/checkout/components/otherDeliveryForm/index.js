import React, { Component } from "react";
import HorizontalInput from "../../../userSettings/components/horizontalInput";
import HorizontalSelectOption from "../../../userSettings/components/horizontalSelectOption";

class OtherDeleveryForm extends Component {
  render() {
    console.log("data: ", this.props.deliveryData);
    return (
      <div className="form  mb-5">
        <div className="single-group">
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="Name"
            type="text"
            placeholder="Your Name"
            name="name"
            value={this.props.deliveryData.name}
            errorText="Please enter your Name"
            isRequired={true}
            onChange={this.props.handleInputChange}
          />
          <div className="sur-form">
            <HorizontalInput
              formInputClass="form-input position-relative"
              label="Surename"
              type="text"
              placeholder="Your Surename"
              name="surename"
              value={this.props.deliveryData.surename}
              errorText="Please enter your Surename"
              isRequired={true}
              onChange={this.props.handleInputChange}
            />
          </div>
          <HorizontalSelectOption
            formInputClass="form-input position-relative"
            label="Country"
            errorText=""
            isRequired={true}
            imageRequired={true}
            selectedText={this.props.deliveryData.country}
            options={["UK", "IT", "LT", "US"]}
            onChange={this.props.handleInputChange}
          />
          <HorizontalInput
            formInputClass="form-input position-relative form-input-append"
            label="address"
            type="text"
            placeholder="Your address"
            name="address"
            value={this.props.deliveryData.address}
            errorText="Please enter your address"
            appendText="Find address"
            isRequired={true}
            onChange={this.props.handleInputChange}
          />
          <HorizontalInput
            formInputClass="form-input position-relative form-input-append"
            label="Zip code"
            type="text"
            placeholder="Your Zip code"
            name="zipcode"
            value={this.props.deliveryData.zipcode}
            errorText="Please enter your Zip code"
            appendText="Find code"
            isRequired={true}
            onChange={this.props.handleInputChange}
          />

          <HorizontalInput
            formInputClass="form-input position-relative"
            label="Your phone number"
            type="text"
            placeholder="Your phone number"
            name="phone"
            value={this.props.deliveryData.phone}
            errorText="Please enter your Your phone number"
            isRequired={true}
            onChange={this.props.handleInputChange}
            validations={["phone"]}
          />
          <HorizontalInput
            formInputClass="form-input position-relative"
            label="Your Email address"
            type="email"
            placeholder="Your Email address"
            name="email"
            value={this.props.deliveryData.email}
            errorText="Please enter Your Email address"
            isRequired={true}
            onChange={this.props.handleInputChange}
            validations={["email"]}
          />
        </div>
      </div>
    );
  }
}

export default OtherDeleveryForm;
