import React, { Component } from "react";
import Down from "../../../../images/icons/down";

class DeliveryForm extends Component {
  render() {
    const { deliveryTypes } = this.props;
    return (
      <div className="form mb-5">
        <div className="text-uppercase font-weight-bold mb-3">Delivery</div>

        {deliveryTypes.map(item => {
          return (
            <div
              className="form-group d-flex align-items-center"
              key={item._id}
            >
              <div className="w-100">
                <label className="ctm-container radio-ctm d-inline-block">
                  <div className="text-uppercase font-weight-bold">
                    {item.name}
                  </div>
                  <div className="x-sm-text-fw4-g ">
                    Delivery {item.fromDay} - {item.toDay} working days
                  </div>
                  <input
                    type="radio"
                    name="delivery"
                    // onClick={e => this.props.addDelivery("delivery", "fast")}
                  />
                  <span className="checkmark checkmark-midle" />
                </label>
              </div>
              <div className="">
                <span className="font-weight-bold text-nowrap">€ 2.99</span>
              </div>
            </div>
          );
        })}
        {/* <div className="form-group d-flex align-items-center">
          <div className="w-100">
            <label className="ctm-container radio-ctm d-inline-block">
              <div className="text-uppercase font-weight-bold">
                Pickup lockers
              </div>
              <div className="x-sm-text-fw4-g ">
                Delivery 1 - 2 working days
              </div>
              <input
                type="radio"
                name="delivery"
                // onClick={e => this.props.addDelivery("delivery", "pickup")}
              />
              <span className="checkmark checkmark-midle" />
            </label>
          </div>
          <div className="">
            <span className="font-weight-bold text-nowrap">
              <Down />
            </span>
          </div>
        </div> */}
      </div>
    );
  }
}

export default DeliveryForm;
