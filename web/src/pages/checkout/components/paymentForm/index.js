import React, { Component } from "react";

class PaymentForm extends Component {
  render() {
    return (
      <div className="form  mb-5">
        <div className="text-uppercase font-weight-bold mb-3">Payment</div>
        <div className="form-group payment-group">
          <label className="ctm-container radio-ctm d-inline-block mr-4">
            <div className="text-uppercase font-weight-bold color-gray7">
              Paysera
            </div>
            <input
              type="radio"
              name="payment"
              onClick={() => this.props.selectPayment("payment", "paysera")}
            />
            <span className="checkmark checkmark-midle" />
          </label>
          <label className="ctm-container radio-ctm d-inline-block">
            <div className="text-uppercase font-weight-bold color-gray7">
              Paypal
            </div>
            <input
              type="radio"
              name="payment"
              onClick={() => this.props.selectPayment("payment", "paypal")}
            />
            <span className="checkmark checkmark-midle" />
          </label>
        </div>
      </div>
    );
  }
}

export default PaymentForm;
