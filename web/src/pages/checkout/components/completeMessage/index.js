import React, { Component } from "react";
import Success from "../../../../images/icons/success";

class CompleteMessage extends Component {
  render() {
    return (
      <div className="complete-message text-center py-5">
        <div className="mb-3">
          <Success />
        </div>
        <div className="md-title thank-text font-weight-6 mb-3">
          Thank you for your purchase!
        </div>
        <div className="sm-text-fw6 text-uppercase mb-5 pb-4">
          Order number: 546425
        </div>
        <div className="font-weight-5 mb-3">
          We’ll email you an order confirmation with order details.
        </div>
        <div className="">
          <button className="btn black-bg-btn w-100">back to socialogue</button>
        </div>
      </div>
    );
  }
}

export default CompleteMessage;
