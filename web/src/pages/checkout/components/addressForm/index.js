import { selectUser } from "core/selectors/user";
import React, { Component } from "react";
import { connect } from "react-redux";
import HorizontalInput from "../../../userSettings/components/horizontalInput";

class AddressForm extends Component {
  render() {
    const { user, deliveryData } = this.props;
    if (!user) {
      return null;
    }
    return (
      <div className="form  mb-5">
        {deliveryData &&
          deliveryData.map(item => {
            return (
              <div className="form-group d-flex align-items-center">
                <div className="w-100">
                  <label className="ctm-container radio-ctm d-inline-block">
                    <div className="text-uppercase font-weight-bold">
                      {item.name}
                    </div>
                    <div>{item.address}</div>
                    <input
                      type="radio"
                      name="address"
                      onClick={() => this.props.onSelectAddress(item)}
                    />
                    <span className="checkmark checkmark-midle" />
                  </label>
                </div>
                <div className="">
                  <span
                    className="sm-text-fw6-g cursor-pointer text-decoration-underline text-uppercase"
                    onClick={() => this.props.onEditAddress(item)}
                  >
                    Edit
                  </span>
                </div>
              </div>
            );
          })}

        <div className="form-group">
          <label className="ctm-container radio-ctm d-inline-block">
            <div className="text-uppercase font-weight-bold">
              Use other address
            </div>
            <input
              type="radio"
              name="address"
              onClick={() => this.props.showAddressToggle(true)}
            />
            <span className="checkmark checkmark-midle" />
          </label>
        </div>
      </div>
    );
  }
}

export default AddressForm;
