import { selectCartData } from "core/selectors/cart";
import { selectDeliveryAddressMsg, selectUser } from "core/selectors/user";
import React, { Component } from "react";
import { connect } from "react-redux";
import Close from "../../images/icons/close";
import Down from "../../images/icons/down";
import Excla from "../../images/icons/excla";
import { errorAlert } from "../../utils/alerts";
import Header from "../home/Header/Header";
import ProImg from "./../../images/login-img1.webp";
import AddressForm from "./components/addressForm";
import CompleteMessage from "./components/completeMessage";
import DeliveryForm from "./components/deliveryForm";
import OrderDetails from "./components/orderDetails";
import OtherDeleveryForm from "./components/otherDeliveryForm";
import PaymentForm from "./components/paymentForm";
import { getDeliverAddress } from "core/actions/homeActionCreators";
import {
  getDeliveryData,
  getDeliveryTypes
} from "core/actions/userSettingsActionCreators";
import {
  selectDeliveryData,
  selectDeliveryTypes
} from "core/selectors/settings";

class Checkout extends Component {
  state = {
    email: "",
    phone: "",
    zipcode: "",
    address: "",
    country: "",
    surename: "",
    name: "",
    orders: [],
    productsPrice: 0,
    vat: 0,
    delivery: {
      standard: 2.99,
      fast: 2.99,
      pickup: 2.99
    },
    payment: null,
    total: 0,
    enableCheckout: false,
    showAddress: false,
    mobileStepShow: false
  };

  handleInputChange = (name, value) => {
    this.setState({
      [name]: value
    });
    // this.props.deliveryData(name, value);
  };

  addDelivery = (name, value) => {};

  selectPayment = (name, value) => {
    this.setState({
      [name]: value,
      enableCheckout: true
    });
  };

  componentDidMount = () => {
    this.props.getDeliverAddress();
    this.props.getDeliveryData();
    this.props.getDeliveryTypes();

    let products = [...this.props.cartData];
    let shopIds = [];
    let sortedOrders = [];

    products.map(product => {
      if (!shopIds.includes(product.shopId)) {
        shopIds.push(product.shopId);
      }
    });

    shopIds.map(id => {
      let productsPrice = 0;
      let vat = 0;
      let delivery = 2.32;
      let total = 0;
      let prods = products.filter(item => {
        if (item.shopId == id) {
          productsPrice += item.totalPrice;
          vat += item.vatAmmount;
          total = productsPrice + vat + delivery;
        }
        return item.shopId == id;
      });
      sortedOrders.push({
        products: prods,
        productsPrice: productsPrice,
        vat: vat,
        delivery: delivery,
        total: total
      });
      this.setState({
        orders: sortedOrders
      });
    });

    this.props.user.companyAddress
      ? this.setState({ showAddress: false })
      : this.setState({ showAddress: true });
  };

  componentDidUpdate = prevProps => {
    if (prevProps.deliveryAddressMsg !== this.props.deliveryAddressMsg) {
      if (this.props.deliveryAddressMsg !== "") {
        // this.setState({ showAddress: true });
      }
    }
  };

  removeItem = (i, oi) => {
    const order = [...this.state.orders];
    if (this.state.orders[oi].products.length < 2) {
      return errorAlert("Minimum one item per order required!");
    }

    order[oi].productsPrice -= +order[oi].products[i].totalPrice;
    order[oi].total -= +order[oi].products[i].grandTotal;
    order[oi].vat -= +order[oi].products[i].vatAmmount;
    order[oi].products.splice(i, 1);
    console.log(order);
    this.setState({
      orders: order
    });
  };

  showAddressToggle = value => {
    this.setState({
      showAddress: value,
      email: "",
      phone: "",
      zipcode: "",
      address: "",
      country: "",
      surename: "",
      name: ""
    });
  };

  onEditAddress = address => {
    this.setState({
      showAddress: true,
      email: address.email,
      phone: address.phone,
      zipcode: address.zipCode,
      address: address.address,
      country: address.country,
      surename: address.surname,
      name: address.name
    });
  };

  onSelectAddress = address => {
    this.setState({
      showAddress: false,
      email: address.email,
      phone: address.phone,
      zipcode: address.zipCode,
      address: address.address,
      country: address.country,
      surename: address.surname,
      name: address.name
    });
  };

  render() {
    return (
      <div>
        <Header />
        <div className="max-content-width my-4 my-md-5 px-4 px-xl-0">
          
          {/* for success massage */}
          <div className="d-none">
            <CompleteMessage />
          </div>
          {/* for success massage */}

          {/* lopp for multiple order*/}
          {this.state.orders.map((order, index) => {
            return (
              <div className="checkout mb-5 pb-2 pb-md-5" key={index}>
                <div className="text-uppercase font-weight-bold mb-5 d-none d-md-block">
                  <span>Order </span>
                  <span className="color-gray7">
                    {index + 1} out of {this.state.orders.length}
                  </span>
                </div>
                <div className="row checkout-row">
                  <div
                    className={
                      this.state.mobileStepShow
                        ? "col-12 col-md-7 checkout-col pr-0 pr-md-5 d-none d-md-block"
                        : "col-12 col-md-7 checkout-col pr-0 pr-md-5"
                    }
                  >
                    <div className="form-all pr-0 pr-md-5 mr-0 mr-md-5">
                      {this.props.deliveryData && (
                        <AddressForm
                          user={this.props.user}
                          deliveryData={this.props.deliveryData}
                          handleInputChange={this.handleInputChange}
                          showAddressToggle={this.showAddressToggle}
                          onEditAddress={this.onEditAddress}
                          onSelectAddress={this.onSelectAddress}
                        />
                      )}
                      {this.state.showAddress && (
                        <OtherDeleveryForm
                          handleInputChange={this.handleInputChange}
                          deliveryData={this.state}
                        />
                      )}
                      <DeliveryForm
                        addDelivery={this.addDelivery}
                        deliveryTypes={this.props.deliveryTypes}
                      />
                      <PaymentForm selectPayment={this.selectPayment} />
                    </div>
                    <div className="d-block d-md-none">
                      <button
                        onClick={() =>
                          this.setState({
                            mobileStepShow: !this.state.mobileStepShow
                          })
                        }
                        className="btn text-uppercase w-100 black-bg-btn"
                      >
                        Next step
                      </button>
                    </div>
                  </div>
                  <div
                    className={
                      this.state.mobileStepShow
                        ? "col-12 col-md-5 pl-3 pl-md-5 pr-0 pr-md-3 checkout-col-1"
                        : "col-12 col-md-5 pl-3 pl-md-5 pr-0 pr-md-3 checkout-col-1 d-none d-md-block"
                    }
                  >
                    <OrderDetails
                      order={order}
                      removeItem={this.removeItem}
                      orderIndex={index}
                    />
                    <div>
                      <button
                        className={
                          this.state.enableCheckout
                            ? "btn text-uppercase w-100 black-bg-btn"
                            : "btn text-uppercase w-100 py-2 disabled"
                        }
                        // disabled={this.state.enableCheckout ? true : false}
                      >
                        Order
                      </button>
                      <div className="d-none d-md-block">
                        <div className="mt-2">
                          <Excla />
                        </div>
                        <div className="x-sm-text-fw4-g">
                          There are products in your cart from 2 different
                          sellers, thats why your order will be devided into two
                          seperate payments.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
          {/* lopp end */}
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    user: selectUser(state),
    cartData: selectCartData(state),
    deliveryAddressMsg: selectDeliveryAddressMsg(state),
    deliveryData: selectDeliveryData(state),
    deliveryTypes: selectDeliveryTypes(state)
  }),
  {
    getDeliverAddress,
    getDeliveryData,
    getDeliveryTypes
  }
)(Checkout);
