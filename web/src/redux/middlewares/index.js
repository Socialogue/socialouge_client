import createSagaMiddleware from 'redux-saga';
import axiosClientMiddleware from 'core/redux/axios/middleware';
import injectTokenBeforeRequest from 'core/redux/middlewares/injectTokenBeforeRequest';

import config from '../../Config';

export const sagaMiddleware = createSagaMiddleware();

const middlewares = [
  injectTokenBeforeRequest(config.GUEST_SECRET),
  axiosClientMiddleware,
  sagaMiddleware
];

/** Add development specific middlewares here */
if (process.env.NODE_ENV === 'development') {
  if (config.REDUX_LOGGER) {
    middlewares.push(require('redux-logger').default);
  }
}


export default middlewares;
