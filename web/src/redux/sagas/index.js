import { axiosClient, axiosClientConfig } from 'core/client';

import { all } from 'redux-saga/effects';
import axiosSaga from 'core/redux/axios/saga';
import config from '../../Config';
import messengerSagas from "core/messenger/saga";
import { selectToken } from 'core/selectors/user';

const baseUrl = config.API_URL;
const apiVersion = config.API_VERSION;
const guestToken = config.GUEST_SECRET;

const client = axiosClient({
  baseUrl,
  apiVersion
});
const clientConfig = axiosClientConfig({
  guestToken,
  getTokenFromState: selectToken
});

export default function* rootSaga() {
  yield all([
    axiosSaga(client, clientConfig),
    messengerSagas()
  ]);
}
