import authReducer from "core/redux/reducers/authReducer";
import messengerReducer from "core/messenger/messageReducer";
import {selectUser} from "core/selectors/user";

export default (state = {}, action: Action) => {
  const loggedInUser = selectUser(state);
  return {
    auth: authReducer(state.auth, action),
    messenger: messengerReducer(state.messenger, {...action, loggedInUser})
  };
};
