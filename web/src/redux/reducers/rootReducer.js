import { combineReducers } from "redux";
import authReducer from "core/redux/reducers/authReducer";
import postReducer from "core/redux/reducers/postReducer";
import shopReducer from "core/redux/reducers/shopReducer";
import productReducer from "core/redux/reducers/productReducer";
import productsInsertReducer from "core/redux/reducers/productsInsertReducer";
import cartReducer from "core/redux/reducers/cartReducer";
import socketReducer from "core/messenger/socketReducer";
import homeReducer from "core/redux/reducers/homeReducer";
import publicProfileReducer from "core/redux/reducers/publicProfileReducer";
import userSettingsReducer from "core/redux/reducers/userSettingReducer";
import outfitsReducer from "core/redux/reducers/outfitsReducer";
import messengerReducer from "core/messenger/messageReducer";
import accountReducer from "core/redux/reducers/accountReducer";
import dashboardReducer from "core/redux/reducers/dashboardReducer";
import fileUploadReducer from "core/redux/reducers/fileUploadReducer";
import productsFilterReducer from "core/redux/reducers/productsFilterReducer";
import commentReducer from "core/redux/reducers/commentReducer";

export default combineReducers({
  auth: authReducer,
  posts: postReducer,
  shops: shopReducer,
  socket: socketReducer,
  products: productReducer,
  productsInsert: productsInsertReducer,
  cart: cartReducer,
  home: homeReducer,
  publicProfile: publicProfileReducer,
  messenger: messengerReducer,
  settings: userSettingsReducer,
  outfits: outfitsReducer,
  account: accountReducer,
  dashboard: dashboardReducer,
  productsFilter: productsFilterReducer,
  comment: commentReducer,
  file: fileUploadReducer
});
