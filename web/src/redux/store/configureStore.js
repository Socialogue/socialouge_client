import { applyMiddleware, createStore } from 'redux';
import { createBrowserHistory, createHashHistory, createMemoryHistory } from 'history';
import middlewares, { sagaMiddleware } from '../middlewares';

import Config from '../../Config';
import { autoRehydrate } from 'redux-persist';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import rootReducer from '../reducers/rootReducer';
import { routerMiddleware } from 'react-router-redux';
import socketClient from 'socket.io-client';
import socketIoMiddleware from 'redux-socket.io-middleware';

const ioAPIConfig = socketClient(Config.API_URL, {
  transports: ['websocket', 'polling', 'flashsocket'],
  reconnectionDelayMax: 10000,
  auth: {
    token: '123',
  },
  query: {
    'my-key': 'my-value',
  },
});

const ioMessagingConfig = socketClient(Config.MESSAGING_API, {
  transports: ['websocket', 'polling', 'flashsocket'],
  reconnectionDelayMax: 10000,
  auth: {
    token: '123',
  },
  query: {
    'my-key': 'my-value',
  },
});

const socketMiddlewares = [
  socketIoMiddleware(ioMessagingConfig, 'msg_action'),
  socketIoMiddleware(ioAPIConfig),
];

const isServer = !(
  typeof window !== 'undefined' &&
  window.document &&
  window.document.createElement
)

export const history = isServer ? createMemoryHistory() : createBrowserHistory()

export default function configureStore(initialState = {}) {

  /** We need multiple compositions before applying middlewares */
  const enhancer = composeWithDevTools(
    applyMiddleware(
      ...middlewares,
      routerMiddleware(history),
      ...socketMiddlewares,
    ),
    autoRehydrate(),
  )

  const store = createStore(rootReducer, initialState, enhancer)

  /** Enable hot reloading of reducers */
  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('../reducers/rootReducer').default
      store.replaceReducer(nextRootReducer)
    })
  }

  store.runSaga = sagaMiddleware.run

  return store
}
