import fs from 'fs';
import path from 'path';
import querystring from 'querystring';
import { URL } from 'url';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { Helmet } from 'react-helmet';
import Loadable from 'react-loadable';
import { Provider } from 'react-redux';
import { StaticRouter, matchPath } from 'react-router-dom';
import { LOCATION_CHANGE } from 'react-router-redux';

import generateMarkup from '../generateMarkup';
import paths from '../paths';

/* eslint-disable import/no-dynamic-require */
const App = require(path.join(paths.appSrc, 'App')).default;
const authActions = require(path.join(paths.core, 'actions', 'authActionCreators'));
const configureStore = require(path.join(paths.appSrc, 'redux', 'store', 'configureStore')).default;
const sagas = require(path.join(paths.appSrc, 'redux', 'sagas', 'serverSagas')).default;
const routes = require(path.join(paths.appSrc, 'routes')).default;
const ROUTES = require(path.join(paths.appSrc, 'routes', 'constants'));
/* eslint-enable import/no-dynamic-require */

const extractAssets = (assets, chunks) => Object.keys(assets)
  .filter(asset => chunks.includes(asset.replace('.js', '')))
  .map(k => assets[k]);

const Controller = (req, res) => {
  const filePath = path.join(paths.appBuild, 'index.html');

  fs.readFile(filePath, 'utf8', async (err, htmlData) => {
    if (err) {
      console.error('err', err); // eslint-disable-line no-console
      return res.status(404).end();
    }

    const store = configureStore();
    store.runSaga(sagas);

    const activeRoute = routes.find(route => matchPath(req.path, route)) || {};
    const baseUrl = `${req.protocol}://${req.get('host')}`;
    const fullUrl = `${baseUrl}${req.originalUrl}`;
    const parsedUrl = new URL(fullUrl);
    const context = { baseUrl, fullUrl };
    const modules = [];
    const promises = [];
    const isAuthenticated = req.cookies.cu_tok &&
      req.cookies.cu_tok_exp &&
      (req.cookies.cu_tok_exp * 1000) > Date.now();

    // Push the current url into the store to let Redux know what state to load
    store.dispatch({
      type: LOCATION_CHANGE,
      payload: {
        pathname: parsedUrl.pathname,
        search: parsedUrl.search,
        hash: parsedUrl.hash,
        key: Math.random().toString(36).substr(2, 6)
      }
    });

    if (isAuthenticated) {
      store.dispatch(authActions.saveAuthCredentials({
        token: req.cookies.cu_tok,
        expiresIn: parseInt(req.cookies.cu_tok_exp, 10)
      }));

      promises.push(store.dispatch(authActions.getUserInfo()));
    }

    if (activeRoute.actions && activeRoute.actions.length) {
      activeRoute.actions.forEach(action => promises.push(store.dispatch(action(req))));
    }

    if (promises.length) {
      // Resolve all promise before render
      try {
        await Promise.all(promises);
      } catch (error) {
        // eslint-disable-next-line no-console
        console.log(error);
      }
    }

    const html = ReactDOMServer.renderToString(
      <Loadable.Capture report={m => modules.push(m)}>
        <Provider store={store}>
          <StaticRouter location={req.url} context={context}>
            <App />
          </StaticRouter>
        </Provider>
      </Loadable.Capture>
    );

    if (context.url) {
      // If context has a url property, then we need to handle a redirection
      if (req.path === ROUTES.INDEX_ROUTE && context.url === ROUTES.FEED_ROUTE) {
        return res.redirect(context.url);
      }

      const queryParams = {
        ...req.query,
        redirect_url: req.query.redirect_url || fullUrl
      };
      const queryString = querystring.stringify(queryParams);
      return res.redirect(`${context.url}?${queryString}`);
    }

    // Use Helmet to compute the right meta tags, title, and such
    const helmet = Helmet.renderStatic();

    // eslint-disable-next-line import/no-dynamic-require
    const jsAssetManifest = require(path.join(paths.appBuild, 'asset-manifest.json'));
    const extraChunks = extractAssets(jsAssetManifest, modules)
      .map(c => `<script type="text/javascript" src="/${c}"></script>`);

    // inject the rendered app into our html and send it
    const markup = generateMarkup(htmlData, {
      title: helmet.title.toString(),
      meta: helmet.meta.toString(),
      body: html,
      scripts: extraChunks,
      // http://redux.js.org/recipes/ServerRendering.html#security-considerations
      state: JSON.stringify(store.getState()).replace(/</g, '\\u003c')
    });

    return res.send(markup);
  });
};

module.exports = Controller;
