const path = require('path');

const paths = require('../paths');

/* eslint-disable import/no-dynamic-require */
const apiEndPoints = require(path.join(paths.core, 'constants', 'apiEndpoints'));
const client = require(path.join(paths.core, 'client')).axiosClient;
const routes = require(path.join(paths.appSrc, 'routes', 'constants'));
/* eslint-enable import/no-dynamic-require */
const axiosClient = client({
  baseUrl: process.env.SL_API_URL,
  apiVersion: process.env.SL_API_VERSION
});

// eslint-disable-next-line consistent-return
const Controller = (req, res) => {
  const akToken = req.body.ak_token;
  const redirectUrl = req.body.redirect_url;

  if (!akToken) return res.redirect('back');

  axiosClient.post(apiEndPoints.ACCOUNT_KIT_AUTH_CODE_API, {
    authorization_code: akToken
  }).then((result) => {
    const token = result.data.token;
    const expiresIn = result.data.expires_in;
    const isProfileComplete = result.data.user.is_complete;
    const cookieOptions = {
      domain: req.hostname,
      expires: new Date(expiresIn * 1000),
      httpOnly: true,
      secure: req.hostname !== 'localhost'
    };

    res.cookie('sl_tok', token, cookieOptions);
    res.cookie('sl_tok_exp', expiresIn, cookieOptions);

    /*
     * Scenarios after successful login
     *
     * 1) If profile is incomplete, go to /profile page
     *    (also keep the reference which page it came from)
     * 2) If profile is complete -
     *    - If we were redirected from other page, go to that page
     *    - If referrer is landing page, go to /feed page
     *    - Stay on the current page
     */
    if (!isProfileComplete) {
      // Add referrer to the url so that we can redirect back
      const url = redirectUrl || req.get('Referrer');
      return res.redirect(`${routes.USER_PROFILE_ROUTE}?redirect_url=${encodeURIComponent(url)}`);
    }

    if (redirectUrl) {
      return res.redirect(redirectUrl);
    }

    // TODO: Check if referrer is landing page
    return res.redirect('back');
  }).catch(() => (
    // TODO: let user know that login has failed
    res.redirect('back')
  ));
};

module.exports = Controller;
