const path = require('path');

const paths = require('../paths');

/* eslint-disable import/no-dynamic-require */
const routes = require(path.join(paths.appSrc, 'routes', 'constants'));
/* eslint-enable import/no-dynamic-require */

const Controller = (req, res) => {
  const cookieOptions = {
    domain: req.hostname,
    httpOnly: true,
    secure: req.hostname !== 'localhost'
  };

  res.clearCookie('cu_tok', cookieOptions);
  res.clearCookie('cu_tok_exp', cookieOptions);

  return res.redirect(routes.INDEX_ROUTE);
};

module.exports = Controller;
