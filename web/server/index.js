require('babel-register')({
  ignore: [/(node_modules)/],
  presets: ['env', 'react-app'],
  plugins: [
    'syntax-dynamic-import',
    'dynamic-import-node',
    'react-loadable/babel'
  ]
});
require('ignore-styles');
require('./env');

const compression = require('compression');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const express = require('express');
const helmet = require('helmet');
const Loadable = require('react-loadable');

const paths = require('./paths');
const loginController = require('./controllers/login');
const logoutController = require('./controllers/logout');
const mainController = require('./controllers/main');

// Create our express app using the port optionally specified
const app = express();
const PORT = process.env.PORT || 3001;

// Trust our nginx proxy
app.enable('trust proxy');

// Middlewares
// For secure HTTP headers
app.use(helmet());
app.use(compression());
app.use(cookieParser());
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Set up static assets
app.use(express.static(paths.appBuild, { index: false }));

// Login/Logout route
app.post('/login', loginController);
app.get('/logout', logoutController);

// Handle all routes
app.get('*', mainController);

// We tell React Loadable to load all required assets and start listening
Loadable.preloadAll().then(() => {
  app.listen(PORT, (err) => {
    if (err) {
      console.log('Server error', err); // eslint-disable-line no-console
    }

    console.log(`Running on ${PORT}...`); // eslint-disable-line no-console
  });
}).catch(console.log); // eslint-disable-line no-console
