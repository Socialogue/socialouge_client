const path = require('path');
const fs = require('fs');

// Make sure any symlinks in the project folder are resolved:
// https://github.com/facebookincubator/create-react-app/issues/637
const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

module.exports = {
  dotenv: resolveApp(path.join('web', '.env')),
  appBuild: resolveApp(path.join('build', 'web')),
  appSrc: resolveApp(path.join('web', 'src')),
  core: resolveApp('core')
};
