const generateMarkup = (data, { title, meta, body, scripts, state }) => {
  let html = data.replace(/<title>.*?<\/title>/g, title);
  html = html.replace('</head>', `${meta}</head>`);

  // Expose the redux state before all other scripts
  html = html.replace(
    '<div id="root"></div>',
    `<div id="root">${body}</div><script>window.__SL_REDUX_STATE__ = ${state}</script>`
  );

  html = html.replace('</body>', `${scripts.join('')}</body>`);

  return html;
};

export default generateMarkup;
