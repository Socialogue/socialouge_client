module.exports = {
  parser: 'babel-eslint',
  extends: ['airbnb', 'plugin:flowtype/recommended'],
  plugins: ['flowtype'],
  rules: {
    camelcase: 'off',
    'comma-dangle': ['error', 'never'],
    'function-paren-newline': ['error', 'consistent'],
    'global-require': 'off',
    'jsx-quotes': ['error', 'prefer-single'],
    'no-underscore-dangle': 'off',
    'no-use-before-define': 0,
    'object-curly-newline': ['error', { consistent: true }],
    'prefer-destructuring': 'off',
    'spaced-comment': 'off',

    'jsx-a11y/anchor-is-valid': 'off',
    'jsx-a11y/label-has-for': ['error', {
      required: {
        every: ['id']
      }
    }],

    'react/jsx-filename-extension': [
      'error',
      {
        extensions: ['.js', '.jsx']
      }
    ],
    'react/jsx-tag-spacing': [
      'error',
      'always',
      {
        beforeSelfClosing: 'always'
      }
    ],
    'react/forbid-prop-types': 0,
    'react/sort-comp': 0,
    'react/require-default-props': 0,

    'flowtype/delimiter-dangle': ['error', 'never'],
    'flowtype/no-dupe-keys': 'error',
    'flowtype/no-primitive-constructor-types': 'error',
    'flowtype/object-type-delimiter': ['error', 'comma'],
    'flowtype/require-parameter-type': [
      'error',
      {
        excludeArrowFunctions: true
      }
    ],
    'flowtype/require-return-type': 'off',
    'flowtype/semi': ['error', 'always']
  },
  settings: {
    flowtype: {
      onlyFilesWithFlowAnnotation: true
    },
    'import/resolver': {
      'react-native': {
        extensions: ['.js', '.android.js', '.ios.js', '.web.js']
      }
    }
  },
  env: {
    jest: true,
    browser: true
  },
  globals: {
    FormData: true,
    __DEV__: true
  }
};
