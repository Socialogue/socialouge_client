import * as React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../utils/constants";


const cap = require('../images/cap.png');
const coat = require('../images/coat.png');

class CartItems extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.cartWarpper}>
          <View style={styles.cartItem}>
            <Image source={cap} style={styles.cap} />
            <Text style={styles.itemTitle}>SIMPLE HAT</Text>
            <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>30 €</Text>
            <TouchableOpacity style={styles.cartButton}>
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>Cart</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.cartItem}>
            <Image source={coat} style={styles.coat} />
            <Text style={styles.itemTitle}>LIMITED EDITION COAT</Text>
            <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>30 €</Text>
            <TouchableOpacity style={styles.cartButton}>
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>Cart</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default CartItems;

const styles = StyleSheet.create({
  container: {
    // backgroundColor: 'red'
  },
  cartWarpper: {
    flexDirection: 'row',
  },
  cartItem: {
    flex: 5
  },
  itemTitle: {
    fontSize: 13,
    marginTop: 10,
    marginBottom: 10,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  cartButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 35,
    borderWidth: 1,
    marginRight: 10,
    marginTop: 20,

  }

});
