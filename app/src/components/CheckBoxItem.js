import React, {Component, useState} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import CheckBox from "@react-native-community/checkbox";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY, PRIMARY_WHITE} from '../utils/constants';

const CheckBoxItem = (props) => {

  const [clicked,isClicked] = useState(false);

    return (
        <View style={styles.checkboxContainer}>
            <View style={styles.content}>
                <CheckBox value={clicked} 
                          onValueChange={()=> isClicked(!clicked)}
                          style={styles.checkBox}
                          />
                        <Text style={styles.label}>{props.name}</Text>
                      </View>
                      <Text style={styles.quantity}>(12)</Text>
                </View>
    );
}


const styles = StyleSheet.create({
    container: {
      flexDirection: 'row',
      paddingVertical: 10,
      paddingHorizontal: 20
    },
    checkboxContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      width: '85%'
    },
    content: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    label: {
      fontFamily: GROTESK_SEMIBOLD,
      color: PRIMARY_BLACK,
      textTransform: 'uppercase'
    },
    quantity: {
      color: PRIMARY_GRAY
    },
   
  });

export default CheckBoxItem;
