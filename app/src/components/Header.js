import * as React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../utils/constants";
import {navigateToAppMenu, navigateToCart, navigateToHome, navigateToLogin} from "../navigator";
import {connect} from "react-redux";
import {selectPosts} from "core/selectors/posts";
import {selectFeed, selectToken, selectViewType} from "core/selectors/user";
import {getPostProducts} from "core/actions/homeActionCreators";
import {Navigation} from "react-native-navigation";


const socialoguelogo = require('../images/Socialoguelogo.png');

const cart = require('../images/cart.png');
const menubar = require('../images/menubar.png');



class Header extends React.Component {
  navigateHome = () => {
    Navigation.mergeOptions('BOTTOM_TABS_LAYOUT', {
      bottomTabs: {
        currentTabIndex: 0,
        visible: true,
      }
    });
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerWarpper}>
          <View style={styles.logo}>
            <TouchableOpacity
              style={{paddingVertical: 10, paddingHorizontal: 5}}
              onPress={this.navigateHome}>
              <Image source={socialoguelogo} style={{width: 100, height: 25}} />
            </TouchableOpacity>
          </View>
          <View style={styles.iconPart}>
            {this.props.isLoggedIn &&
              <TouchableOpacity
                style={{paddingVertical: 10, paddingHorizontal: 5}}
                onPress={() => navigateToCart(this.props.componentId)}
              >
                <Image source={cart} style={{width: 22, height: 22}} />
              </TouchableOpacity>
            }
            {!this.props.isLoggedIn &&
              <TouchableOpacity
                style={{paddingVertical: 10, paddingHorizontal: 5}}
                onPress={() => navigateToLogin(this.props.componentId)}
              >
                <Text style={styles.logInText}>LOGIN</Text>
              </TouchableOpacity>
            }


            <TouchableOpacity
              style={{paddingVertical: 10, paddingRight: 20, paddingLeft: 10}}
              onPress={() => navigateToAppMenu(this.props.componentId)}
              >

              <Image source={menubar} style={{width: 25, height: 25}} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    posts: selectPosts(state),
    isLoggedIn: !!selectToken(state),
    auth: state.auth,
    sortViewType: selectViewType(state),
    feed: selectFeed(state)
  }),
  {
    getPostProducts
  }
)(Header);

const styles = StyleSheet.create({
  container: {
    flex: 100,
    borderBottomWidth: 1,

  },
  headerWarpper: {
    flex: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: 20,
    position: 'relative',
  },
  iconPart: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  logInText: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },

});
