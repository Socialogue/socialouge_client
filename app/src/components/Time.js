import * as React from 'react';
import {Text} from 'react-native';
import moment from 'moment';

class Time extends React.Component {
  constructor(props) {
    super(props);
    this.date = props.time;
  }
  render() {
    const time = moment( this.date || moment.now() ).fromNow();
    return (
      <Text style={{fontSize: 11}}>{time}</Text>
    );
  }
}

export default Time;

