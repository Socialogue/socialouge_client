import * as React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View, Avatar} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../utils/constants";
import {
  navigateToAppMenu,
  navigateToCart, navigateToChatList,
  navigateToHome,
  navigateToLogin,
  navigateToPlus, navigateToSearch,
  navigateToShop, navigateToShopList
} from "../navigator";


const home = require('../images/bottom/home.png');
const home_selected = require('../images/bottom/home-selected.png');
const shop = require('../images/bottom/shop.png');
const shop_selected = require('../images/bottom/shop-selected.png');
const plus = require('../images/bottom/plus.png');
const plus_selected = require('../images/bottom/plus-selected.png');
const message = require('../images/bottom/message.png');
const message_selected = require('../images/bottom/message-selected.png');
const search = require('../images/bottom/search.png');
const search_selected = require('../images/bottom/search-selected.png');
const Rectangle = require('../images/bottom/Rectangle423.png');


class BottomTab extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.contet}>
          <TouchableOpacity
            onPress={() => navigateToHome(this.props.componentId)}
          >
            <Image source={home} style={{width: 25, height: 25}} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigateToShopList(this.props.componentId)}
          >
            <Image source={shop} style={{width: 25, height: 25}} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigateToPlus(this.props.componentId)}
          >
            <Image source={plus} style={{width: 50, height: 50}} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigateToChatList(this.props.componentId)}
          >
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image source={message} style={{width: 25, height: 25}} />
              <View style={{flexDirection: 'row'}}>
                <Text style={{marginLeft: 5, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>12</Text>
                <Image source={Rectangle} style={{width: 4, height: 5}} />
              </View>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigateToSearch(this.props.componentId)}
          >
            <Image source={search} style={{width: 25, height: 25}} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default BottomTab;

const styles = StyleSheet.create({
  container: {
    height: 80,
    borderTopWidth: 1,
    paddingHorizontal: 30
  },
  contet: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  }

});
