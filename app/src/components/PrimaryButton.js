import * as React from "react";
import {ActivityIndicator, StyleSheet, Text, TouchableOpacity} from "react-native";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_WHITE} from "../utils/constants";


class PrimaryButton extends React.Component {
  render() {
    return (
      <TouchableOpacity
        disabled={this.props.loading}
        style={styles.logInButton}
        onPress={this.props.onPress}
      >
        {this.props.loading &&
        <ActivityIndicator size="large" color="#fff" />
        }

        {!this.props.loading &&
        <Text style={styles.logInButtonText}>{this.props.title.toUpperCase()}</Text>
        }
      </TouchableOpacity>
    );
  }
}

export default PrimaryButton;

const styles = StyleSheet.create({
  logInButton: {
    backgroundColor: PRIMARY_BLACK,
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logInButtonText: {
    color: PRIMARY_WHITE,
    fontSize: 15,
    fontFamily: GROTESK_SEMIBOLD,
  },

});
