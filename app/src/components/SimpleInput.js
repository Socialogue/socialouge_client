import {StyleSheet, Text, TextInput, View} from "react-native";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, SECONDARY_ORANGE} from "../utils/constants";
import * as React from "react";

class SimpleInput extends React.Component{

  render(): React.ReactNode {
    const {label, value, placeholder, isRequired, onChange, inputCount, find, findName,isEditable} = this.props
    return (
      <View style={styles.content}>
      <View  style={styles.lebel}>
        <Text style={styles.lebelText}>{label}
          {
            isRequired &&
            <Text style={{color: SECONDARY_ORANGE}}>*</Text>
          }
        </Text>
        { find &&
          <Text style={{
            fontSize: 12,
            fontFamily: GROTESK_SEMIBOLD,
            color: PRIMARY_BLACK
          }}>{findName}</Text>
        }
      </View>
      <View>
        <TextInput
          style={styles.input}
          placeholder={placeholder}
          value={value}
          onChangeText={onChange}
          editable= {isEditable == undefined ? true : isEditable}
        />
        {/*{(inputCount && inputCount === 2) &&
        <TextInput
          style={styles.input}
          placeholder={placeholder}
          value={value}
          onChangeText={ text => onChange('input2', text)}
        />
        }*/}
      </View>
    </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 10
  },
  content: {
    // flexDirection: 'row',
    // alignItems: 'center',
    // justifyContent: 'space-between',
    marginVertical: 20
  },
  input: {
    borderBottomWidth: 1,
    // width: 200
  },
  lebel: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  lebelText: {
    fontSize: 14,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    textTransform: 'uppercase'
  },
});

export default SimpleInput;
