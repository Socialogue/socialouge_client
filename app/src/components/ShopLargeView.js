import * as React from 'react';
import {Image, ScrollView, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY} from "../utils/constants";

const shopAvater = require('../images/userAvater.png');
const shopImage_1 = require('../images/shopImage_1.png');
const shopImage_2 = require('../images/shopImage_2.png');
const shopLargeImage = require('../images/shopLargeImage.png');

class ShopLargeViewView extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.shop}>
          <View style={styles.shopImage}>
            <Image source={shopLargeImage} style={styles.coverImage} />
            <Image source={shopAvater} style={styles.avterImage}/>
          </View>
          <View style={styles.shopName}>
            <View>
              <Text style={styles.shopTitle}>{this.props.item.title}</Text>
              <Text style={styles.shopFollowers}>232 followers</Text>
            </View>
            <TouchableOpacity style={styles.followButton}>
              <Text style={styles.followButtonText}>follow</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.additionalImageView}>
            <View style={{width: '31%'}}>
              <Image source={shopImage_1} style={styles.additionalImage} />
            </View>
            <View style={{width: '31%'}}>
              <Image source={shopImage_2} style={styles.additionalImage}/>
            </View>
            <View style={{width: '31%'}}>
              <Image source={shopImage_2} style={styles.additionalImage}/>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
export default ShopLargeViewView;

const styles = StyleSheet.create({
  container: {
    flex: 10,
    padding: 20
  },
  shopTitle: {
    fontSize: 16,
    textTransform: 'uppercase',
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  shopFollowers: {
    fontSize: 12,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_GRAY,
    marginVertical: 5
  },
  followButton: {
    width: 100,
    height: 35,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5
  },
  followButtonText: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  shopImage: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  coverImage: {
    width: '100%',
    height: 130
  },
  avterImage: {
    width: 60,
    height: 60,
    position: 'absolute',
    borderRadius: 37.5,
    bottom: '-35%',
    backgroundColor: '#fff'
  },
  shopName: {
    marginLeft: '25%',
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10
  },
  additionalImageView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  additionalImage: {
    width: '100%',
    height: 150
  }


});
