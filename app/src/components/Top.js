import * as React from 'react';
import {Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY, PRIMARY_WHITE} from '../utils/constants';
import CheckBox from "@react-native-community/checkbox";
import DropDownItem from "react-native-drop-down-item";

import Slider from '@react-native-community/slider';
import ProductFilter from './ProductFilter';

const filterArrow = require('../screens/Home/images/downarrow.png');
const sortArrow = require('../screens/Home/images/downarrow.png');
const viewArrow = require('../screens/Home/images/View.png');
const cross = require('../images/cross.png');
const IC_ARR_DOWN = require('../images/ic_arr_down.png');
const IC_ARR_UP = require('../images/ic_arr_up.png');
const priceView = require('../images/priceView.png');

class Top extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
      isFilter: false,
      isSort: false
    };
  }

  toggleCheckbox = newValue => {
    this.setState({checked: newValue});
  };
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={() => this.setState({isFilter: true})}
        >
          <View style={styles.filterWarpper}>
            <Text style={styles.filter}>FILTERS</Text>
            <Image source={filterArrow} style={styles.filterArrow} />
          </View>
        </TouchableOpacity>
        <View style={styles.viewWarpper}>
          <Text style={styles.view}>VIEW</Text>
          <Slider
            style={{width: 100, height: 40, marginTop: 5}}
            thumbImage={viewArrow}
            // trackImage={viewArrow}
            minimumValue={1}
            maximumValue={2}
            step={1}
            value={this.props.postView}
            minimumTrackTintColor="#000"
            maximumTrackTintColor="#000"
            onValueChange={this.props.onValueChange}
          />
        </View>
        <TouchableOpacity
          onPress={() => this.setState({isSort: true})}
        >
          <View style={styles.sortWarpper}>
            <Image source={sortArrow} style={styles.sortArrow} />
            <Text style={styles.sort}>SORT</Text>
          </View>
        </TouchableOpacity>
        {this.state.isFilter &&
          <View style={styles.filterWrapper}>
          <ScrollView>
            <View style={{flexDirection: 'row', justifyContent: 'space-between',}}>
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>FILTER</Text>
              <TouchableOpacity
                style={{paddingRight: 5, paddingLeft: 20, paddingBottom: 20}}
                onPress={() => this.setState({isFilter: false})}
              >
                <Image source={cross} style={{width: 22, height: 22}}/>
              </TouchableOpacity>
            </View>
            <ScrollView style={{alignSelf: 'stretch'}}>
              <View style={styles.downMenu}>
                <DropDownItem
                  style={styles.dropDownItem}
                  contentVisible={false}
                  invisibleImage={IC_ARR_DOWN}
                  visibleImage={IC_ARR_UP}
                  useNativeDriver={false}
                  header={
                    <View style={styles.menuHead}>
                      <Text style={styles.lebelTitle}>GENDER</Text>
                    </View>
                  }>
                  <View>
                    <View style={styles.checkboxContainer}>
                      <View style={styles.content}>
                        <CheckBox
                          style={styles.checkBox}
                          value={this.state.checked}
                          onValueChange={this.toggleCheckbox}
                        />
                        <Text style={styles.label}>MALE</Text>
                      </View>
                      <Text style={styles.quantity}>(63)</Text>
                    </View>
                    <View style={styles.checkboxContainer}>
                      <View style={styles.content}>
                        <CheckBox
                          style={styles.checkBox}
                          value={this.state.checked}
                          onValueChange={this.toggleCheckbox}
                        />
                        <Text style={styles.label}>WOMALE</Text>
                      </View>
                      <Text style={styles.quantity}>(12)</Text>
                    </View>
                    <View style={styles.checkboxContainer}>
                      <View style={styles.content}>
                        <CheckBox
                          style={styles.checkBox}
                        />
                        <Text style={styles.label}>UNISEX</Text>
                      </View>
                      <Text style={styles.quantity}>(124)</Text>
                    </View>
                  </View>
                </DropDownItem>
              </View>
              <View style={styles.downMenu}>
                <DropDownItem
                  style={styles.dropDownItem}
                  contentVisible={false}
                  invisibleImage={IC_ARR_DOWN}
                  visibleImage={IC_ARR_UP}
                  useNativeDriver={false}
                  header={
                    <View style={styles.menuHead}>
                      <Text style={styles.lebelTitle}>PRICE</Text>
                    </View>
                  }>
                  <View>
                    <Image source={priceView} style={{width: 105, height: 5}}/>
                    <Text>10 €</Text>
                  </View>
                </DropDownItem>
              </View>
              <View style={styles.downMenu}>
                <DropDownItem
                  style={styles.dropDownItem}
                  contentVisible={false}
                  invisibleImage={IC_ARR_DOWN}
                  visibleImage={IC_ARR_UP}
                  useNativeDriver={false}
                  header={
                    <View style={styles.menuHead}>
                      <Text style={styles.lebelTitle}>SIZE</Text>
                    </View>
                  }>
                  <View>
                    <View style={styles.checkboxContainer}>
                      <View style={styles.content}>
                        <CheckBox
                          style={styles.checkBox}
                        />
                        <Text style={styles.label}>S</Text>
                      </View>
                      <Text style={styles.quantity}>(63)</Text>
                    </View>
                    <View style={styles.checkboxContainer}>
                      <View style={styles.content}>
                        <CheckBox
                          style={styles.checkBox}
                        />
                        <Text style={styles.label}>M</Text>
                      </View>
                      <Text style={styles.quantity}>(12)</Text>
                    </View>
                    <View style={styles.checkboxContainer}>
                      <View style={styles.content}>
                        <CheckBox
                          style={styles.checkBox}
                        />
                        <Text style={styles.label}>L</Text>
                      </View>
                      <Text style={styles.quantity}>(124)</Text>
                    </View>
                    <View style={styles.checkboxContainer}>
                      <View style={styles.content}>
                        <CheckBox
                          style={styles.checkBox}
                        />
                        <Text style={styles.label}>XL</Text>
                      </View>
                      <Text style={styles.quantity}>(12)</Text>
                    </View>
                    <View style={styles.checkboxContainer}>
                      <View style={styles.content}>
                        <CheckBox
                          style={styles.checkBox}
                        />
                        <Text style={styles.label}>XS</Text>
                      </View>
                      <Text style={styles.quantity}>(23)</Text>
                    </View>
                    <View style={styles.checkboxContainer}>
                      <View style={styles.content}>
                        <CheckBox
                          style={styles.checkBox}
                        />
                        <Text style={styles.label}>XXL</Text>
                      </View>
                      <Text style={styles.quantity}>(124)</Text>
                    </View>
                  </View>
                </DropDownItem>
              </View>
              <View style={styles.downMenu}>
                <DropDownItem
                  style={styles.dropDownItem}
                  contentVisible={false}
                  invisibleImage={IC_ARR_DOWN}
                  visibleImage={IC_ARR_UP}
                  useNativeDriver={false}
                  header={
                    <View style={styles.menuHead}>
                      <Text style={styles.lebelTitle}>COLOUR</Text>
                    </View>
                  }>
                  <View>
                    <View style={styles.checkboxContainer}>
                      <View style={styles.content}>
                        <CheckBox
                          style={styles.checkBox}
                        />
                        <Text style={styles.label}>BLACK</Text>
                      </View>
                      <Text style={styles.quantity}>(63)</Text>
                    </View>
                    <View style={styles.checkboxContainer}>
                      <View style={styles.content}>
                        <CheckBox
                          style={styles.checkBox}
                        />
                        <Text style={styles.label}>blue</Text>
                      </View>
                      <Text style={styles.quantity}>(12)</Text>
                    </View>
                    <View style={styles.checkboxContainer}>
                      <View style={styles.content}>
                        <CheckBox
                          style={styles.checkBox}
                        />
                        <Text style={styles.label}>brown</Text>
                      </View>
                      <Text style={styles.quantity}>(124)</Text>
                    </View>
                    <View style={styles.checkboxContainer}>
                      <View style={styles.content}>
                        <CheckBox
                          style={styles.checkBox}
                        />
                        <Text style={styles.label}>ecru</Text>
                      </View>
                      <Text style={styles.quantity}>(12)</Text>
                    </View>
                    <View style={styles.checkboxContainer}>
                      <View style={styles.content}>
                        <CheckBox
                          style={styles.checkBox}
                        />
                        <Text style={styles.label}>green</Text>
                      </View>
                      <Text style={styles.quantity}>(23)</Text>
                    </View>
                    <View style={styles.checkboxContainer}>
                      <View style={styles.content}>
                        <CheckBox
                          style={styles.checkBox}
                        />
                        <Text style={styles.label}>grey </Text>
                      </View>
                      <Text style={styles.quantity}>(124)</Text>
                    </View>
                  </View>
                </DropDownItem>
              </View>
            </ScrollView>
          </ScrollView>
        </View>
        }
        {this.state.isSort &&
          <View style={styles.sortWrapper}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between',}}>
            <TouchableOpacity
              style={{paddingRight: 5, paddingRight: 20, paddingBottom: 20}}
              onPress={() => this.setState({isSort: false})}
            >
              <Image source={cross} style={{width: 22, height: 22}}/>
            </TouchableOpacity>
            <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>SORT</Text>
          </View>
          <View style={{padding: 10}}>
            <View style={styles.checkboxSortContainer}>
              <View style={styles.content}>
                <CheckBox
                  style={styles.checkBox}
                  value={this.state.checked}
                  onValueChange={this.toggleCheckbox}
                />
                <Text style={styles.label}>new</Text>
              </View>
              <Text style={styles.quantity}>(63)</Text>
            </View><View style={styles.checkboxSortContainer}>
            <View style={styles.content}>
              <CheckBox
                style={styles.checkBox}
                value={this.state.checked}
                onValueChange={this.toggleCheckbox}
              />
              <Text style={styles.label}>popular</Text>
            </View>
            <Text style={styles.quantity}>(63)</Text>
          </View><View style={styles.checkboxSortContainer}>
            <View style={styles.content}>
              <CheckBox
                style={styles.checkBox}
                value={this.state.checked}
                onValueChange={this.toggleCheckbox}
              />
              <Text style={styles.label}>trending</Text>
            </View>
            <Text style={styles.quantity}>(63)</Text>
          </View>
          </View>
        </View>
        }

      </View>
    );
  }
}

export default Top;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 20,
  },
  filterWarpper: {
    flexDirection: 'row',
    flex: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  filter: {
    marginRight: 10,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  viewWarpper: {
    flexDirection: 'row',
    flex: 65,
    alignItems: 'center',
    justifyContent: 'center',
  },
  view: {
    // marginRight: 10,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  line: {
    borderBottomWidth: 2,
    width: 60,
  },
  sortWarpper: {
    flexDirection: 'row',
    flex: 15,
    alignItems: 'center',
  },
  sort: {
    marginLeft: 10,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  filterWrapper: {
    width: '70%',
    backgroundColor: PRIMARY_WHITE,
    position: 'absolute',
    padding: 10,
    top: -1,
    left: 0,
    borderWidth: 1,
    zIndex: 999
  },
  downMenu: {
    paddingVertical: 10,
    paddingHorizontal: 20
  },
  lebelTitle: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  checkboxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '85%'
  },
  content: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    textTransform: 'uppercase'
  },
  quantity: {
    color: PRIMARY_GRAY
  },
  sortWrapper: {
    width: '60%',
    backgroundColor: PRIMARY_WHITE,
    position: 'absolute',
    padding: 10,
    top: -1,
    right: 0,
    borderWidth: 1,
    zIndex: 999
  },
  checkboxSortContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '95%'
  }
});

