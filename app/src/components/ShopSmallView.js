import * as React from 'react';
import {Image, ScrollView, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY} from "../utils/constants";

const Ellipse45 = require('../images/Ellipse454.png');
const shopImage_1 = require('../images/shopImage_1.png');
const shopImage_2 = require('../images/shopImage_2.png');

class ShopSmallView extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.shop}>
          <View style={{
            flexDirection: 'row',
            alignItems: 'center'
          }}>
            <Image source={Ellipse45} style={{width: 60, height: 60}} />
            <View style={{marginLeft: 20}}>
              <Text style={styles.shopTitle}>uniq butiq</Text>
              <Text style={styles.shopFollowers}>232 followers</Text>
            </View>
          </View>
          <TouchableOpacity style={styles.followButton}>
            <Text style={styles.followButtonText}>follow</Text>
          </TouchableOpacity>
          <View style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
            <View style={{width: '48%'}}>
              <Image source={shopImage_1} style={{width: '100%', height: 220}} />
            </View>
            <View style={{width: '48%'}}>
              <Image source={shopImage_2}  style={{width: '100%', height: 220}}/>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
export default ShopSmallView;

const styles = StyleSheet.create({
  container: {
    flex: 10,
    padding: 20
  },
  shopTitle: {
    fontSize: 23,
    textTransform: 'uppercase',
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  shopFollowers: {
    fontSize: 14,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_GRAY,
    marginVertical: 5
  },
  followButton: {
    width: 100,
    height: 35,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20
  },
  followButtonText: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  }


});
