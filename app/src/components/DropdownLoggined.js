import * as React from 'react';
import {Image, StyleSheet, Text, View, TextInput} from 'react-native';

const search = require('../images/search.png');
const home = require('../images/homeicons.png');
const rightarrow = require('../images/arrowright.png');
const shop = require('../images/shopicons.png');
const sell = require('../images/sellicon.png');
const Ellipse45 = require('../images/Ellipse45.png');
const dashboard = require('../images/dashboard.png');
const setting = require('../images/setting.png');

class DropdownLoggined extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.searchWarpper}>
          <Image source={search} style={search} />
          <View style={styles.searchInput}>
            <TextInput
              style={styles.searchBar}
              placeholder="Search..."
            />
          </View>
        </View>
        <View style={styles.userId}>
          <Image source={Ellipse45} style={styles.ellipse} />
          <Text style={styles.userIdName}>UNIQ BUTIQ</Text>
          <Image source={rightarrow} style={styles.rightArrow} />
        </View>
        <View style={styles.menuWarpper}>
          <View style={styles.homeMenu}>
            <View style={styles.textIcon}>
              <Image source={home} style={styles.homeIcon} />
              <Text style={styles.homeText}>HOME</Text>
            </View>
            <Image source={rightarrow} style={styles.rightArrow} />
          </View>
          <View style={styles.shopMenu}>
            <View style={styles.textIcon}>
              <Image source={shop} style={styles.shopIcon} />
              <Text style={styles.shopText}>SHOP</Text>
            </View>
            <Image source={rightarrow} style={styles.rightArrow} />
          </View>
          <View style={styles.subMenu}>
            <View style={styles.sellMenu}>
              <View style={styles.textIcon}>
                <Image source={sell} style={styles.sellIcon} />
                <Text style={styles.sellText}>SELLS</Text>
              </View>
              <Image source={rightarrow} style={styles.rightArrow} />
            </View>
            <View style={styles.dashboardMenu}>
              <View style={styles.textIcon}>
                <Image source={dashboard} style={styles.dashboardIcon} />
                <Text style={styles.dashboardText}>DASHBOARD</Text>
              </View>
              <Image source={rightarrow} style={styles.rightArrow} />
            </View>
            <View style={styles.settingMenu}>
              <View style={styles.textIcon}>
                <Image source={setting} style={styles.settingIcon} />
                <Text style={styles.settingText}>SETTING</Text>
              </View>
              <Image source={rightarrow} style={styles.rightArrow} />
            </View>
          </View>
        </View>
        <View style={styles.bottomWarpper}>
          <Text style={styles.logOut}>LOGOUT</Text>
        </View>
      </View>
    );
  }
}

export default DropdownLoggined;

const styles = StyleSheet.create({
  container: {
    padding: 20,
    flex: 100
  },
  searchWarpper: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 10
  },
  search: {
    marginRight: 20,
  },
  searchInput: {
    borderBottomWidth: 1,
    width: '95%'
  },
  userId: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20
  },
  userIdName: {
    marginLeft: 10,
    marginRight: 10,
  },
  menuWarpper: {
    flex: 80,
  },
  homeMenu: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 30,
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#E5E5E5'
  },
  textIcon: {
    flexDirection: 'row',
  },
  homeIcon: {
    marginRight: 10
  },
  shopMenu: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#E5E5E5'
  },
  shopIcon: {
    marginRight: 10
  },
  sellMenu: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 15,
  },
  sellIcon: {
    marginRight: 10
  },
  sellText: {
    marginRight: 20
  },
  subMenu: {
    flex: 100,
    marginLeft: '50%',
    marginTop: 30
  },
  dashboardMenu: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 15
  },
  dashboardIcon: {
    marginRight: 10,
  },
  dashboardText: {
    marginRight: 20
  },
  settingMenu: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 15
  },
  settingIcon: {
    marginRight: 10
  },
  settingText: {
    marginRight: 20
  },
  bottomWarpper: {
    flex: 10,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  }
});
