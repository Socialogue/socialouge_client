import * as React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY} from "../utils/constants";


const FirstCommentPost = require('../images/images-3.png');
const Like = require('../images/Like.png');
const Comment = require('../images/Comment.png');
const Ellipse45 = require('../images/Ellipse45.png');

class PostWithComment extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Image source={FirstCommentPost} style={styles.FirstCommentPost} />
        <View style={styles.bottomWarpper}>
          <View style={styles.byTitle}>
            <Image source={Ellipse45} style={styles.ellipse} />
            <View style={styles.nameTitle}>
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>BUTIQ K</Text>
              <Text style={{fontSize: 12}}>Yesterday. 19:23</Text>
            </View>
          </View>
          <View style={styles.reactBox}>
            <Image source={Like} style={{width: 20, height: 22, marginRight: 20}} />
            <Image source={Comment} style={{width: 20, height: 22}} />
          </View>
        </View>
        <Text style={styles.desscription}>Follow our page and don't miss the news on sustainable clothes! New shirt is coming to.</Text>
      </View>
    );
  }
}

export default PostWithComment;

const styles = StyleSheet.create({
  container: {
    padding: 20
  },
  FirstCommentPost: {
    resizeMode: 'contain',
    width: "100%",
    height: 500 
  },
  byTitle: {
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10
  },
  ellipse: {
    marginRight: 10
  },
  bottomWarpper: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  reactBox: {
    flexDirection: 'row',
    marginTop: 10
  },
  like: {
    marginRight: 10
  },
  desscription: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_GRAY
  },


});
