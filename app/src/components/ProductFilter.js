import React, {Component} from 'react';
import {ExpandableListView} from 'react-native-expandable-listview';
import CheckBoxItem from './CheckBoxItem';


const CONTENT = [
  {
    id: '42',
    categoryName: 'GENDER',
    subCategory: [
      {id: '1',name: "MEN", customInnerItem: (<CheckBoxItem name="MEN"/>)},
      {id: '2', name: 'WOMEN', customInnerItem: (<CheckBoxItem name="WOMEN"/>)},
      {id: '3', name: 'UNISEX',customInnerItem: (<CheckBoxItem name="UNISEX"/>)},

    ],
  },
  {
    id: '95',
    categoryName: 'PRICE',
    subCategory: [
        {id: '1', name: '10 €',customInnerItem: (<CheckBoxItem name="10 €"/>)},
        {id: '2', name: '100 €',customInnerItem: (<CheckBoxItem name="100 €"/>)},
        {id: '3', name: '500 €',customInnerItem: (<CheckBoxItem name="500€"/>)},
    ],
  },
  {
    id: '94',
    categoryName: 'SIZE',
    subCategory: [
        {id: '1', name: 'S',customInnerItem: (<CheckBoxItem name="S"/>)},
        {id: '2', name: 'M',customInnerItem: (<CheckBoxItem name="M"/>)},
        {id: '3', name: 'L',customInnerItem: (<CheckBoxItem name="L"/>)},
        {id: '4', name: 'XL',customInnerItem: (<CheckBoxItem name="XL"/>)},
        {id: '5', name: 'XS',customInnerItem: (<CheckBoxItem name="XS"/>)},
        {id: '6', name: 'XXL',customInnerItem: (<CheckBoxItem name="XXL"/>)}
    ],
  },
  {
    id: '93',
    categoryName: 'COLOR',
    subCategory: [
        {id: '1', name: 'BLACK',customInnerItem: (<CheckBoxItem name="BLACK"/>)},
        {id: '2', name: 'BLUE',customInnerItem: (<CheckBoxItem name="BLUE"/>)},
        {id: '3', name: 'BRAWN',customInnerItem: (<CheckBoxItem name="BRAWN"/>)},
        {id: '4', name: 'YELLOW',customInnerItem: (<CheckBoxItem name="YELLOW"/>)},
        {id: '5', name: 'GRAY',customInnerItem: (<CheckBoxItem name="GRAY"/>)},
        {id: '6', name: 'SKY',customInnerItem: (<CheckBoxItem name="SKY"/>)}
    ],
  },
];

const ProductFilter = () => {
  function handleItemClick({index}) {
    console.log(index);
  };

  function handleInnerItemClick({innerIndex, item, itemIndex}) {
    console.log(innerIndex);
  };

  return (
      <ExpandableListView
        data={CONTENT} // required
        onInnerItemClick={handleInnerItemClick}
        onItemClick={handleItemClick}
      />
    );
}

export default ProductFilter;