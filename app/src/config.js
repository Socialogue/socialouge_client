import ENV from 'react-native-config';
import invariant from 'invariant';

function getEnv(key, defaultValue) {
  if (ENV[key] === undefined) {
    return defaultValue;
  } else if (ENV[key] === 'true') {
    return true;
  } else if (ENV[key] === 'false') {
    return false;
  }

  return ENV[key];
}


let config = {
  APP_VERSION: getEnv('APP_VERSION'),
  API_URL: getEnv('API_URL'),
  SITE_URL: getEnv('SITE_URL'),
  GUEST_SECRET: getEnv('GUEST_SECRET'),
  SUPPORT_MAIL: getEnv('SUPPORT_MAIL'),
  SUPPORT_PHONE: getEnv('SUPPORT_PHONE'),

  FB_APP_EVENTS: getEnv('FB_APP_EVENTS', false),
  ENABLE_SENTRY: true,
  REDUX_ANALYTICS: true,
  SENTRY_ENDPOINT: '',

  // Dev configs
  ACCESS_TOKEN: getEnv('ACCESS_TOKEN', null),
  DISABLE_YELLOW_BOX: getEnv('DISABLE_YELLOW_BOX', true),
  NETWORK_DEBUG: getEnv('NETWORK_DEBUG', false),
  REDUX_LOGGER: getEnv('REDUX_LOGGER', false),
  CONFIG_LOGGER: getEnv('CONFIG_LOGGER', false)
};

if (__DEV__) {
  const devConfig = require('./config.development').default;
  config = {
    ...config,
    ...devConfig,
    ENABLE_SENTRY: false,
    REDUX_ANALYTICS: false,
    FB_APP_EVENTS: false,
  };
}

invariant(config.API_URL, 'API_URL is not set in .env');
invariant(config.GUEST_SECRET, 'GUEST_SECRET is not set in .env');
invariant(
  config.ENABLE_SENTRY !== undefined,
  'ENABLE_SENTRY is not set in .env'
);
invariant(
  config.FB_APP_EVENTS !== undefined,
  'FB_APP_EVENTS is not set in .env'
);

export default config;
