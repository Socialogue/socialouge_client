/**  @flow  */

import React from 'react';
import { Navigation } from 'react-native-navigation';

/**
 * @param store
 * @param provider
 * @returns {function(string, ): (*|string)}
 */
export function registerReduxComponent(store: Object, provider: Object) {
  return (screenName: string, component: React.Component<*, *, *>) =>
    Navigation.registerComponent(
      screenName,
      () => component,
      store,
      provider
    );
}

/**
 * @param screenName
 * @param component
 * @returns {*|string}
 */
export function registerComponent(screenName: string, component: React.Component<*, *, *>) {
  return Navigation.registerComponent(
    screenName,
    () => component
  );
}
