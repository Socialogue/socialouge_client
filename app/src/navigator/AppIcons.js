/* eslint-disable new-cap */
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const icons = {
  grid: ['grid', 24, SimpleLineIcons],
  magnifier: ['magnifier', 24, SimpleLineIcons],
  basket: ['basket-loaded', 24, SimpleLineIcons],
  bell: ['bell', 24, SimpleLineIcons],
  menu: ['menu', 24, SimpleLineIcons],
  filter: ['filter', 20, MaterialCommunityIcons, '#666']
};

const iconsMap = {};

// const iconsLoaded = new Promise((resolve) => {
//   new Promise.all(
//     Object.keys(icons).map((iconName) => {
//       const IconPack = icons[iconName][2];
//
//       if (iconName === 'filter') {
//         return IconPack.getImageSource(
//           icons[iconName][0], icons[iconName][1], icons[iconName][3]
//         );
//       }
//
//       return IconPack.getImageSource(icons[iconName][0], icons[iconName][1]);
//     })
//   ).then((sources) => {
//     Object.keys(icons).forEach((iconName, idx) => {
//       iconsMap[iconName] = sources[idx];
//     });
//
//     // Call resolve (and we are done)
//     resolve(true);
//   });
// });

export {
  iconsMap,
  // iconsLoaded
};
