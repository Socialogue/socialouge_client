/**  @flow  */
import get from 'lodash/get';
import * as React from 'react';
import { connect } from 'react-redux';
import { selectIsLoggedIn, selectUser } from 'core/selectors/user';

import {
  navStatusBarStyle,
  startMainActivity
} from './index';

type Props = {
  isLoggedIn: boolean,
  authByAK: Function,
  saveRedirectOfferId: Function,
  navigator: Object
};

/**
 * This is a wrapper component which passes two props,
 *
 * -> isLoggedIn
 *
 * -> onLoggedIn
 *
 * for components who need it.
 *
 * Also logs in the user and start the main app on `onLoggedIn` callback.
 */
const LoginWrapper = (OriginalComponent: React.ComponentType<any>) => {
  class LoginApp extends React.Component<Props> {
    props: Props

    // eslint-disable-next-line no-unused-vars
    shouldComponentUpdate(nextProps: Props) {
      return false;
    }

    handleSMSLogin = async (token, offerId = 0) => {
      const { navigator, user } = this.props;

      try {
        // Does not work on iOS.
        // if (Platform.OS === 'android') {
        //   showLoadingModal();
        // }


      } catch (err) {
        // Error Handler will give proper alert message, just handle user banned case.
        const code = get(err, 'error.response.data.error.code', 0);

        if (code === 4001) {
          navigator.push({
            screen: 'AccountSuspendScreen',
            title: 'Account Suspended'
          });
        }
      } finally {
        // Does not work on iOS.
        // if (Platform.OS === 'android') {
        //   hideLoadingModal();
        // }
      }
    };

    render() {

      return (
        <OriginalComponent {...props} onLoggedIn={this.handleSMSLogin} />
      );
    }
  }

  // Navigation customizations
  // $FlowFixMe
  LoginApp.navigatorStyle = {
    // $FlowFixMe
    ...OriginalComponent.navigatorStyle,
    ...navStatusBarStyle
  };

  return connect(
    state => ({
      isLoggedIn: !!selectIsLoggedIn(state),
      user: selectUser(state)
    }),
    null
  )(LoginApp);
};

export default LoginWrapper;
