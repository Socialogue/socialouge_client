import React from 'react';
import { Navigation } from 'react-native-navigation';

import Home from '../screens/Home';
import Search from '../screens/Search';
import LoginWrapper from './LoginWrapper';
import MainAppWrapper from './MainAppWrapper';
import Login from '../screens/Login';
import Register from '../screens/Register';
import ProductPage from '../screens/ProductPage';
import ChatList from '../screens/ChatList';
import ChatConversation from '../screens/ChatConversation';
import AppMenu from '../screens/AppMenu';
import Shop from '../screens/Shop';
import Plus from '../screens/Plus';
import TagList from '../screens/TagList';
import UserProfile from '../screens/UserProfile';
import PublicUserProfile from '../screens/PublicUserProfile';
import ShopProfile from '../screens/ShopProfile';
import ShopProfileSetting from '../screens/ShopProfileSetting';
import UserProfileSetting from '../screens/UserProfileSetting';
import ResetPassword from "../screens/ResetPassword";
import SubmitEmail from "../screens/SubmitEmail";
import Success from "../screens/Success";
import OtpInput from "../screens/OtpInput";
import ShopList from "../screens/ShopList";
import PrivacyPolicy from "../screens/PrivacyPolicy";
import StartSelling from "../screens/StartSelling";
import ApplyShop from "../screens/ApplyShop";
import Orders from "../screens/Orders";
import Finances from "../screens/Finances";
import Cart from "../screens/Cart";
import CheckOutAddress from "../screens/CheckOutAddress";
import OrderingOverView from "../screens/OrderingOverView";
import ThankYou from "../screens/ThankYou";
import Swiper from "../screens/Swiper";


// eslint-disable-next-line import/prefer-default-export

function WrappedComponent(Component, Provider, store) {
  return function inject(props) {
    const EnhancedComponent = () => (
      <Provider store={store}>
        <Component
          {...props}
        />
      </Provider>
    );

    return <EnhancedComponent />;
  };
}

export function registerScreens(store, provider) {


  Navigation.registerComponent('LoginScreen', () => WrappedComponent(Login, provider, store));
  Navigation.registerComponent('RegisterScreen', () => WrappedComponent(Register, provider, store));
  Navigation.registerComponent('ProductPageScreen', () => WrappedComponent(ProductPage, provider, store));
  Navigation.registerComponent('ChatListScreen', () => WrappedComponent(ChatList, provider, store));
  Navigation.registerComponent('ChatConversationScreen', () => WrappedComponent(ChatConversation, provider, store));
  Navigation.registerComponent('AppMenuScreen', () => WrappedComponent(AppMenu, provider, store));
  Navigation.registerComponent('ShopScreen', () => WrappedComponent(Shop, provider, store));
  Navigation.registerComponent('PlusScreen', () => WrappedComponent(Plus, provider, store));
  Navigation.registerComponent('TagListScreen', () => WrappedComponent(TagList, provider, store));
  Navigation.registerComponent('UserProfileScreen', () => WrappedComponent(UserProfile, provider, store));
  Navigation.registerComponent('PublicUserProfileScreen', () => WrappedComponent(PublicUserProfile, provider, store));
  Navigation.registerComponent('ShopProfileScreen', () => WrappedComponent(ShopProfile, provider, store));
  Navigation.registerComponent('ShopProfileSettingScreen', () => WrappedComponent(ShopProfileSetting, provider, store));
  Navigation.registerComponent('UserProfileSettingScreen', () => WrappedComponent(UserProfileSetting, provider, store));
  Navigation.registerComponent('ResetPasswordScreen', () => WrappedComponent(ResetPassword, provider, store));
  Navigation.registerComponent('SubmitEmailScreen', () => WrappedComponent(SubmitEmail, provider, store));
  Navigation.registerComponent('SuccessScreen', () => WrappedComponent(Success, provider, store));
  Navigation.registerComponent('OtpInputScreen', () => WrappedComponent(OtpInput, provider, store));
  Navigation.registerComponent('ShopListScreen', () => WrappedComponent(ShopList, provider, store));
  Navigation.registerComponent('PrivacyPolicyScreen', () => WrappedComponent(PrivacyPolicy, provider, store));
  Navigation.registerComponent('StartSellingScreen', () => WrappedComponent(StartSelling, provider, store));
  Navigation.registerComponent('ApplyShopScreen', () => WrappedComponent(ApplyShop, provider, store));
  Navigation.registerComponent('OrdersScreen', () => WrappedComponent(Orders, provider, store));
  Navigation.registerComponent('FinancesScreen', () => WrappedComponent(Finances, provider, store));
  Navigation.registerComponent('CartScreen', () => WrappedComponent(Cart, provider, store));
  Navigation.registerComponent('CheckOutAddressScreen', () => WrappedComponent(CheckOutAddress, provider, store));
  Navigation.registerComponent('OrderingOverViewScreen', () => WrappedComponent(OrderingOverView, provider, store));
  Navigation.registerComponent('ThankYouScreen', () => WrappedComponent(ThankYou, provider, store));
  Navigation.registerComponent('SwiperScreen', () => WrappedComponent(Swiper, provider, store));
  Navigation.registerComponent('HomeScreen', () => WrappedComponent(MainAppWrapper(Home), provider, store));
  Navigation.registerComponent('SearchScreen', () => WrappedComponent(MainAppWrapper(Search), provider, store));



  // Dumb screens
  //Navigation.registerComponent('DeprecateScreen', () => DeprecateScreen);
}
