import {TEXT_GREEN} from "core/constants/colors";
const iconPlus = require('../../images/bottom/plus.png');
const iconPlusSelected = require('../../images/bottom/plus-selected.png');

export default {
  stack: {
    id: 'PLUS_TAB',
    children: [
      {
        component: {
          id: 'LOGIN_SCREEN',
          name: 'LoginScreen',
          options: {
            topBar: {
              visible: false
            }
          }
        }
      }
    ],
    options: {
      bottomTab: {
        iconHeight: 50,
        iconWidth: 50,
        icon: iconPlus,
        selectedIcon: iconPlusSelected,
        disableSelectedIconTint: true,
        // selectedIconColor: TEXT_GREEN,
        text: '',
        selectedTextColor: TEXT_GREEN
      }
    }
  }
}
