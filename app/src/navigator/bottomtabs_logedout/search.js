import {TEXT_GREEN} from "core/constants/colors";
const iconSearch = require('../../images/bottom/search.png');
const iconSearchSelected = require('../../images/bottom/search-selected.png');

export default {
  stack: {
    id: 'SEARCH_TAB',
    children: [
      {
        component: {
          id: 'SEARCH_SCREEN',
          name: 'SearchScreen',
          options: {
            topBar: {
              visible: false
            }
          }
        }
      }
    ],
    options: {
      bottomTab: {
        icon: iconSearch,
        selectedIcon: iconSearchSelected,
        // selectedIconColor: TEXT_GREEN,
        text: '',
        selectedTextColor: TEXT_GREEN
      }
    }
  }
}
