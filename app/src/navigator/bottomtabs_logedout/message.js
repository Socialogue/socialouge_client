import {TEXT_GREEN} from "core/constants/colors";
const iconMessage = require('../../images/bottom/message.png');
const iconMessageSelected = require('../../images/bottom/message-selected.png');

export default {
  stack: {
    id: 'MESSAGE_TAB',
    children: [
      {
        component: {
          id: 'LOGIN_SCREEN',
          name: 'LoginScreen',
          options: {
            topBar: {
              visible: false
            }
          }
        }
      }
    ],
    options: {
      bottomTab: {
        icon: iconMessage,
        selectedIcon: iconMessageSelected,
        // selectedIconColor: TEXT_GREEN,
        text: '',
        badge: '12',
        // disableIconTint: false,
        // animateBadge: false,
        // iconWidth: 100,
        iconInsets: {
          top: 100
        },
        selectedTextColor: TEXT_GREEN
      }
    }
  }
}
