import {TEXT_GREEN} from "core/constants/colors";
const iconShop = require('../../images/bottom/shop.png');
const iconShopSelected = require('../../images/bottom/shop-selected.png');

export default {
  stack: {
    id: 'SHOP_TAB',
    children: [
      {
        component: {
          id: 'LOGIN_SCREEN',
          name: 'ShopListScreen',
          options: {
            topBar: {
              title: {
                text: ''
              },
              visible: false
            }
          }
        }
      }
    ],
    options: {
      bottomTab: {
        icon: iconShop,
        // selectedIconColor: TEXT_GREEN,
        selectedIcon: iconShopSelected,
        text: '',
        selectedTextColor: TEXT_GREEN
      }
    }
  }
}
