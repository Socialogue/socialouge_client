import homeTab from "./home";
import shopTab from "./shop";
import plusTab from "./plus";
import messageTab from "./message";
import searchTab from "./search";

export default [
  homeTab,
  shopTab,
  plusTab,
  messageTab,
  searchTab,
]
