/**  @flow  */
import { Platform } from 'react-native';
import { Navigation } from 'react-native-navigation';
import {
  COLOR_BLACK,
  COLOR_STATUS_BAR,
  COLOR_WHITE,
  TEXT_BLACK,
  TEXT_GREEN
} from 'core/constants/colors';

import { iconsLoaded, iconsMap } from './AppIcons';

export const navStatusBarStyle = {
  navBarBackgroundColor: COLOR_WHITE,
  navBarTextColor: TEXT_BLACK,
  navBarButtonColor: COLOR_BLACK,
  statusBarColor: Platform.Version >= 23 ? COLOR_STATUS_BAR : COLOR_BLACK,
  statusBarTextColorScheme: (Platform.OS === 'ios' || Platform.Version >= 23) ? 'dark' : 'light'
};

//Tab icons
const iconReturn = require('../images/bottom/Return.png');
const iconReturnSelected = require('../images/bottom/Return-selected.png');
const iconFinance = require('../images/bottom/finance.png');
const iconFinanceSelected = require('../images/bottom/finance-selected.png');
const iconMessage = require('../images/bottom/message.png');
const iconMessageSelected = require('../images/bottom/message-selected.png');
const iconSearch = require('../images/bottom/search.png');
const iconSearchSelected = require('../images/bottom/search-selected.png');
const iconShopList = require('../images/bottom/shopList.png');
const iconShopListSelected = require('../images/bottom/shopList-selected.png');
const iconShop = require('../images/bottom/shop.png');
const iconShopSelected = require('../images/bottom/shop-selected.png');

//Tab stacks
import returnTab from "./bottomtabs_shop/return";
import fenanceTab from "./bottomtabs_shop/fenance";
import messageTab from "./bottomtabs/message";
import searchTab from "./bottomtabs/search";
import shopListTab from "./bottomtabs_shop/shopList";
import shopTab from "./bottomtabs/shop"

import mainAppTabs from './bottomtabs';
import LoginTabs from './bottomtabs_logedout';
import shopTabs from './bottomtabs_shop';
import ShopProfile from "../screens/ShopProfile";

export const startSwiperActivity = () => (
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: 'SwiperScreen',
              options: {
                topBar: {
                  visible: false
                },
                animations: {
                  push: {
                    enabled: false
                  },
                  pop: {
                    enabled: false
                  }
                }
              }
            }
          }
        ]
      }
    }
  })
);

export const startMainActivity = () => (
  Navigation.setRoot({
    root: {
      bottomTabs: {
        id: 'BOTTOM_TABS_LAYOUT',
        children: mainAppTabs,
        options: {
          bottomTabs: {
            titleDisplayMode: "alwaysShow",
            barStyle: 'black',
            //tabsAttachMode: 'onSwitchToTab'
            // animate: false,
            // animateTabSelection: false,
          },
        }
      },
    },
  })
);


//Navigate to shop user
export const ChangeNavigationToShop = () => {
//Change shop tab to ShopList tab
  Navigation.mergeOptions('SHOP_TAB',{
    bottomTab: {
      icon: iconShopList,
      // selectedIconColor: TEXT_GREEN,
      selectedIcon: iconShopListSelected,
      text: '',
      selectedTextColor: TEXT_GREEN
    }
  });
  Navigation.setStackRoot('SHOP_TAB',shopListTab);


  //change message tab to return tab
  Navigation.mergeOptions('MESSAGE_TAB',{
    bottomTab: {
      icon: iconReturn,
      selectedIcon: iconReturnSelected,
      selectedIconColor: TEXT_GREEN,
      text: '',
      badge: '1',
      // disableIconTint: false,
      // animateBadge: false,
      // iconWidth: 100,
      iconInsets: {
        top: 100
      },
      selectedTextColor: TEXT_GREEN
    }
  });
  Navigation.setStackRoot('MESSAGE_TAB',returnTab);

  //Change search tab to fenanceTab
  Navigation.mergeOptions('SEARCH_TAB',{

      bottomTab: {
        icon: iconFinance,
        selectedIcon: iconFinanceSelected,
        // selectedIconColor: TEXT_GREEN,
        text: '',
        selectedTextColor: TEXT_GREEN
      }

  });
  Navigation.setStackRoot('SEARCH_TAB',fenanceTab);
};

//Navigate to  user
export const ChangeNavigationToUser = () => {
  //Shop
  Navigation.mergeOptions('SHOP_TAB',{
    bottomTab: {
      icon: iconShop,
      // selectedIconColor: TEXT_GREEN,
      selectedIcon: iconShopSelected,
      text: '',
      selectedTextColor: TEXT_GREEN
    }
  });
  Navigation.setStackRoot('SHOP_TAB',shopTab);
//Message
  Navigation.mergeOptions('MESSAGE_TAB',{
    bottomTab: {
      icon: iconMessage,
      selectedIcon: iconMessageSelected,
      // selectedIconColor: TEXT_GREEN,
      text: '',
      badge: '12',
      // disableIconTint: false,
      // animateBadge: false,
      // iconWidth: 100,
      iconInsets: {
        top: 100
      },
      selectedTextColor: TEXT_GREEN
    }
  });
  Navigation.setStackRoot('MESSAGE_TAB',messageTab);

  //Search
  Navigation.mergeOptions('SEARCH_TAB',{

    bottomTab: {
      icon: iconSearch,
      selectedIcon: iconSearchSelected,
      // selectedIconColor: TEXT_GREEN,
      text: '',
      selectedTextColor: TEXT_GREEN
    }

  });
  Navigation.setStackRoot('SEARCH_TAB',searchTab);
};


export const startLoginActivity = () => (
  Navigation.setRoot({
    root: {
      bottomTabs: {
        id: 'BOTTOM_TABS_LAYOUT_LOGGEDOUT',
        children: LoginTabs,
        options: {
          bottomTabs: {
            titleDisplayMode: "alwaysShow",
            barStyle: 'black',
            //tabsAttachMode: 'onSwitchToTab'
            // animate: false,
            // animateTabSelection: false,
          },
        }
      },
    },
  })
);



export const startShopActivity = () => (
  Navigation.setRoot({
    root: {
      bottomTabs: {
        id: 'BOTTOM_TABS_LAYOUT_SHOP',
        children: shopTabs,
        options: {
          bottomTabs: {
            titleDisplayMode: "alwaysShow",
            barStyle: 'black'
            // animate: false,
            // animateTabSelection: false,
          },
        }
      },
    },
  })
);


export const navigateToSwiper = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'SwiperScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);

export const navigateToHome = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'HomeScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);

export const navigateToLogin = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'LoginScreen',
      backButtonTitle: '',

      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToRegister = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'RegisterScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);

export const navigateToProductPage = (componentId, product) => {

  Navigation.push(componentId, {
    component: {
      name: 'ProductPageScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      },
      passProps: {
        product: product,
      }
    }
  })
};
export const navigateToChatList = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'ChatListScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToChatConversation = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'ChatConversationScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToAppMenu = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'AppMenuScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);

export const navigateToPlus = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'PlusScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToTagList = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'TagListScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToUserProfile = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'UserProfileScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);

export const navigateToPublicUserProfile = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'PublicUserProfileScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);

export const navigateToShopProfile = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'ShopProfileScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);

export const navigateToShopProfileSetting= componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'ShopProfileSettingScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);

export const navigateToUserProfileSetting = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'UserProfileSettingScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToResetPassword = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'ResetPasswordScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false,
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToSubmitEmail = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'SubmitEmailScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToSuccess = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'SuccessScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToOtpInput = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'OtpInputScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToShopList = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'ShopListScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToPrivacyPolicy = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'PrivacyPolicyScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToStartSelling = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'StartSellingScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToApplyShop = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'ApplyShopScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToOrders = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'OrdersScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToFinances = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'FinancesScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToCart = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'CartScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToCheckOutAddress = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'CheckOutAddressScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToOrderingOverView = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'OrderingOverViewScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToThankYou = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'ThankYouScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        bottomTabs: {
          visible: false,
          animate: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
export const navigateToSearch = componentId => (
  Navigation.push(componentId, {
    component: {
      name: 'SearchScreen',
      backButtonTitle: '',
      options: {
        topBar: {
          visible: false
        },
        animations: {
          push: {
            enabled: false
          },
          pop: {
            enabled: false
          }
        }
      }
    }
  })
);
