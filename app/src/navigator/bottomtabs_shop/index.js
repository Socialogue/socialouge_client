import homeTab from "./home";
import shopTab from "./shopList";
import plusTab from "./plus";
import returnTab from "./return";
import fenanceTab from "./fenance";

export default [
  homeTab,
  shopTab,
  plusTab,
  returnTab,
  fenanceTab,
]
