import {TEXT_GREEN} from "core/constants/colors";
const iconReturn = require('../../images/bottom/Return.png');
const iconReturnSelected = require('../../images/bottom/Return-selected.png');

export default {
  stack: {
    id: 'RETURN_TAB',
    children: [
      {
        component: {
          id: 'RETURN_SCREEN',
          name: 'OrdersScreen',
          options: {
            topBar: {
              visible: false
            }
          }
        }
      }
    ],
    options: {
      bottomTab: {
        icon: iconReturn,
        selectedIcon: iconReturnSelected,
        // selectedIconColor: TEXT_GREEN,
        text: '',
        badge: '1',
        // disableIconTint: false,
        // animateBadge: false,
        // iconWidth: 100,
        iconInsets: {
          top: 100
        },
        selectedTextColor: TEXT_GREEN
      }
    }
  }
}
