import {TEXT_GREEN} from "core/constants/colors";
const iconFinance = require('../../images/bottom/finance.png');
const iconFinanceSelected = require('../../images/bottom/finance-selected.png');

export default {
  stack: {
    id: 'FINANCE_TAB',
    children: [
      {
        component: {
          id: 'FINANCE_SCREEN',
          name: 'FinancesScreen',
          options: {
            topBar: {
              visible: false
            }
          }
        }
      }
    ],
    options: {
      bottomTab: {
        icon: iconFinance,
        selectedIcon: iconFinanceSelected,
        // selectedIconColor: TEXT_GREEN,
        text: '',
        selectedTextColor: TEXT_GREEN
      }
    }
  }
}
