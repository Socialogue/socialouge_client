import {TEXT_GREEN} from "core/constants/colors";
const iconHome = require('../../images/bottom/home.png');
const iconHomeSelected = require('../../images/bottom/home-selected.png');

export default {
  stack: {
    id: 'HOME_TAB',
    children: [
      {
        component: {
          id: 'HOME_SCREEN',
          name: 'HomeScreen',
          options: {
            topBar: {
              visible: false
            }
          }
        }
      }
    ],
    options: {
      bottomTab: {
        icon: iconHome,
        animateBadge: false,
        selectedIcon: iconHomeSelected,
        text: '',
        disableSelectedIconTint: false,
        selectedTextColor: TEXT_GREEN
      }
    }
  }
}
