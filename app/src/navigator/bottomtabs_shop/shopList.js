import {TEXT_GREEN} from "core/constants/colors";
const iconShop = require('../../images/bottom/shopList.png');
const iconShopSelected = require('../../images/bottom/shopList-selected.png');

export default {
  stack: {
    id: 'SHOPLIST_TAB',
    children: [
      {
        component: {
          id: 'SHOP_LIST_SCREEN',
          name: 'ShopListScreen',
          options: {
            topBar: {
              title: {
                text: ''
              },
              visible: false
            }
          }
        }
      }
    ],
    options: {
      bottomTab: {
        icon: iconShop,
        // selectedIconColor: TEXT_GREEN,
        selectedIcon: iconShopSelected,
        text: '',
        selectedTextColor: TEXT_GREEN
      }
    }
  }
}
