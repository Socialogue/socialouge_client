/**
 * @flow
 */
import get from 'lodash/get';
import * as React from 'react';
import { Alert, AppState, Platform, View } from 'react-native';
import { connect } from 'react-redux';


/**
 * This component will setup all the functionalities for the app, mainly -
 *
 * `ErrorHandler`
 *
 * `AppLinkHandler`
 *
 * `PushController` (If loggedIn)
 *
 * Also it'll listen for AppState change & notify components who need it.
 */
const MainAppWrapper = (OriginalComponent: React.ComponentType<any>) => {
  type Props = {
    getBootstrapConfig: (type: string) => Promise<*>,
    isLoggedIn: boolean // Available from LoginWrapper
  };

  type State = {
    appState: ?string
  };

  class MainApp extends React.Component<Props, State> {
    props: Props;

    state = {
      appState: AppState.currentState
    };

    componentDidMount() {

    }

    shouldComponentUpdate(nextProps: Props, nextState: State) {
      // Only update the component if app state changes.
      return this.state.appState !== nextState.appState;
    }

    render() {
      const { isLoggedIn } = this.props;
      const { appState } = this.state;
      return (
        <View style={{ flex: 1 }}>
          <OriginalComponent {...this.props} />
        </View>
      );
    }
  }

  return connect(
    null,
    null
  )(MainApp);
};

export default MainAppWrapper;
