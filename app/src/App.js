import AsyncStorage from '@react-native-async-storage/async-storage';
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';
import { selectInitialRoute } from 'core/selectors/nav';
import configureStore from './redux/store/configureStore';
import { registerScreens } from './navigator/Screens';
import {
  startLoginActivity,
  startMainActivity, startSwiperActivity
} from './navigator';
import { Navigation } from 'react-native-navigation';

/** setup dev environment */
if (process.env.NODE_ENV === 'development') {
  require('./devSetup');
}

const store = configureStore();

registerScreens(store, Provider);

Navigation.events().registerAppLaunchedListener(async () => {
  persistStore(store, {
    storage: AsyncStorage,
    whitelist: ['auth', 'appConfig', 'misc', 'settings']
  }, () => {
    const state = store.getState();
    const initialScreen = selectInitialRoute(state);
    // const initialScreen = 'App';
    startApp(initialScreen);
  });
});


function startApp(initialScreen) {
  switch (initialScreen) {
    case 'Swiper':
      startSwiperActivity();
      break;
    case 'Login':
      startLoginActivity();
      break;
    case 'App':
      startMainActivity();
      break;
    default:
      break;
  }
}
