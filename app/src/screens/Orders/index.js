import * as React from 'react';
import {Image, ScrollView, StyleSheet, Text, TouchableOpacity, View, TextInput} from 'react-native';
import styles from './styles';

import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY, SECONDARY_ORANGE, LIGHT_GRAY} from "../../utils/constants";

import CheckBox from "@react-native-community/checkbox";


const cross = require('./images/cross.png');
const calendar = require('./images/calendar.png');
const downArrow = require('./images/downArrow.png');
const Rectangle1464 = require('./images/Rectangle1464.png');
const Group1269 = require('./images/Group1269.png');


class Orders extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
    };
  }

  toggleCheckbox = newValue => {
    this.setState({checked: newValue});
  };
  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.headerWrapper}>
            <Text style={{fontSize: 25, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK,}}>orders<Text style={{fontSize: 40, color: SECONDARY_ORANGE}}>.</Text></Text>
            <Image source={cross} style={{width: 22, height: 22, marginTop: 10}}/>
          </View>
          <View style={styles.bodyWrapper}>
            <View style={{flexDirection: 'row', alignItems: 'center', marginVertical: 20}}>
              <Image source={calendar} style={{width: 20, height: 20}}/>
              <Text style={{marginHorizontal: 10}}>calendar</Text>
              <Image source={downArrow} style={{width: 10, height: 6}}/>
            </View>
            <View style={{borderWidth: 1, borderColor: LIGHT_GRAY}}>
              <View style={{flexDirection: 'row', margin: 10, paddingBottom: 10, borderBottomWidth: 1, borderColor: LIGHT_GRAY, justifyContent: 'space-between',}}>
                <View>
                  <Text style={styles.lebelText}>Total day sales</Text>
                  <Text style={styles.subText}>April 1, 2020</Text>
                </View>
                <View>
                  <Text style={styles.lebelTextBold}>€ 143.43</Text>
                  <Text style={styles.greenText}>43,2% <Text style={styles.orangeText}>{"   "}12 SOLD</Text></Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10}}>
                <Text style={styles.lebelText}>All time sales</Text>
                <Text style={{fontSize: 16, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>6.564.00€</Text>
              </View>
            </View>
            <View style={{borderWidth: 1, borderColor: LIGHT_GRAY, marginVertical: 20}}>
              <View style={{ margin: 10, paddingBottom: 10, borderBottomWidth: 1, borderColor: LIGHT_GRAY, justifyContent: 'space-between',}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10, alignItems: 'center',}}>
                  <Text style={styles.lebelText}>Last 24 hours</Text>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.greenText}>43,2%</Text>
                    <Image source={Group1269} />
                  </View>
                  <Text style={{fontSize: 16, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>165.00 €</Text>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10, alignItems: 'center',}}>
                  <Text style={styles.lebelText}>Last 7 days</Text>
                  <Text style={styles.greenText}>43,2%</Text>
                  <Text style={{fontSize: 16, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>165.00 €</Text>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10, alignItems: 'center',}}>
                  <Text style={styles.lebelText}>Last 30 days</Text>
                  <Text style={styles.orangeText}>22%</Text>
                  <Text style={{fontSize: 16, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>165.00 €</Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 10}}>
                <Text style={{fontSize: 12, color: PRIMARY_GRAY}}>Percentage change relative to prior period. Data includes shipping and sales tax.</Text>
              </View>
            </View>
            <View>
              <Text style={styles.lebelText}>Orders (10)</Text>
              <View>
                <View style={styles.content}>
                  <CheckBox
                    style={styles.checkBox}
                    value={this.state.checked}
                    onValueChange={this.toggleCheckbox}
                  />
                  <Text style={styles.selectLabel}>Select all</Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 40, marginVertical: 20}}>
                <Text>PHOTO</Text>
                <Text>PRODUCT NAME</Text>
              </View>
              <View style={styles.content}>
                <CheckBox
                  style={styles.checkBox}
                  value={this.state.checked}
                  onValueChange={this.toggleCheckbox}
                />
                <Image source={Rectangle1464} style={{width: 40, height: 40, marginHorizontal: 20}}/>
                <Text style={styles.selectLabel}>New Look co-ord nylon overshirt in black</Text>
              </View>
              <View style={styles.content}>
                <CheckBox
                  style={styles.checkBox}
                  value={this.state.checked}
                  onValueChange={this.toggleCheckbox}
                />
                <Image source={Rectangle1464} style={{width: 40, height: 40, marginHorizontal: 20}}/>
                <Text style={styles.selectLabel}>New Look co-ord nylon overshirt in black</Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
export default Orders;


