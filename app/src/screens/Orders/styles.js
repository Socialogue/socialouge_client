import {StyleSheet, Text} from 'react-native';
import {GROTESK_SEMIBOLD, LIGHT_GRAY, PRIMARY_BLACK, PRIMARY_GRAY, SECONDARY_ORANGE} from "../../utils/constants";
import * as React from "react";

export default StyleSheet.create({
  container: {
    flex: 10,
    paddingLeft: 20,
    paddingRight: 20,
  },
  headerWrapper: {
    height: 60,
    borderBottomWidth: 1,
    paddingBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  lebelText: {
    fontSize: 14,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    textTransform: 'uppercase',
    marginBottom: 10
  },
  subText: {
    fontSize: 12,
    color: PRIMARY_GRAY
  },
  lebelTextBold: {
    fontSize: 20,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,

    marginBottom: 10
  },
  orangeText: {
    fontSize: 12,
    color: SECONDARY_ORANGE
  },
  greenText: {
    fontSize: 12,
    color: '#29D425',
  },
  content: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10
  },
  selectLabel: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  }
});
