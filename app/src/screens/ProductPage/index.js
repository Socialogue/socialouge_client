import * as React from 'react';
import {Text, View, Image, TouchableOpacity, ScrollView} from 'react-native';

import styles from './styles';
import ProductDetails from './component/ProductDetails';
import DropdownMenu from './component/DropdownMenu';
import {Navigation} from "react-native-navigation";
import {navigateToAppMenu, navigateToCart, navigateToHome} from "../../navigator";
import { selectUser } from 'core/selectors/user';
import { selectProduct } from 'core/selectors/products';
import { selectCartData , selectCartPrice } from 'core/selectors/cart';
import { addToCart , getProductDetails } from 'core/actions/productActionCreators';
import { addToCartDb } from 'core/actions/cartActionCreators';
import { connect } from "react-redux";
import {getImageOriginalUrl} from "core/utils/helpers";


const cart = require('./images/cart.png');
const menubar = require('./images/menubar.png');
const arrowLeft = require('./images/arrowLeft.png');
const Share = require('./images/Share.png');
const sliderimage = require('./images/sliderimage.png');

class ProductsPages extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeview: 'scrollview',
      activeImage: null
    };
  }

  renderExtraImages = (product) => {
    if (product.additionalImage && product.additionalImage.length) {
      const jsx = [];
      product.additionalImage.map(image => {
        jsx.push(
          <TouchableOpacity
            activeOpacity={.8}
            key={image}
            onPress={() => this.setState({activeImage: image})}>
            <Image source={{ uri: getImageOriginalUrl(image) }} style={styles.shortImage} />
          </TouchableOpacity>
        );
      });
      return jsx;
    }
    return null;
  }

  render() {
    const {product} = this.props;
    const {activeImage} = this.state;
    let mainImage = sliderimage;
    if (activeImage === null) {
      if (product.mainImage) {
        mainImage = {uri: product.mainImage};
      }
    } else {
      mainImage = {uri: getImageOriginalUrl(activeImage)};
    }
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.headerWarpper}>
            <TouchableOpacity
              onPress={() => Navigation.pop(this.props.componentId)}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image source={arrowLeft} style={{width: 25, height: 12, marginRight: 5}} />
                <Text style={styles.lebelText}>Capri pants</Text>
              </View>
            </TouchableOpacity>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image source={Share} style={{width: 22, height: 22}} />
              <TouchableOpacity
                onPress={() => navigateToCart(this.props.componentId)}
                >
                <Image source={cart} style={{width: 22, height: 22, marginHorizontal: 15}} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => navigateToAppMenu(this.props.componentId)}
                >
                <Image source={menubar} style={{width: 22, height: 25}} />
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <Image source={mainImage} style={styles.sliderimage} />
            <ScrollView horizontal={true} style={styles.smallImage}>
              {this.renderExtraImages(product)}
            </ScrollView>
          </View>
          <View style={styles.bodyWarpper}>
            <View style={styles.productCodeView}>
              <Text style={styles.productCode}>PRODUCT CODE: {product._id}</Text>
            </View>

            <View style={styles.productDetails}>
              <ProductDetails product={product} />
            </View>
            <View style={styles.dropdownMenu}>
              <DropdownMenu product={product} />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default connect(
  state => ({
    user: selectUser(state),
    product: selectProduct(state),
    cartData: selectCartData(state),
    cartPrice: selectCartPrice(state)
  }),
  {
    getProductDetails,
    addToCart,
    addToCartDb
  }
)(ProductsPages);
