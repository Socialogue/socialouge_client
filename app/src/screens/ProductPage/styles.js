import {StyleSheet} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../utils/constants";

export default StyleSheet.create({
  container: {
  },
  headerWarpper: {
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 20,
    borderBottomWidth: 1
  },
  lebelText: {
    fontSize: 15,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    textTransform: 'uppercase'
  },
  bodyWarpper: {
    paddingHorizontal: 20
  },
  logInPart: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  favorite: {
    marginRight: 10
  },
  massegeicon: {
    marginRight: 10
  },
  notificationicon: {
    marginRight: 10
  },
  cart: {
    marginRight: 10
  },
  logInText: {
    marginLeft: 20,
    marginRight: 20,
  },
  menuTopbar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15
  },
  sliderimage: {
    width: '100%',
    height: 400
  },
  smallImage: {
    flexDirection: 'row',
  },
  shortImage: {
    width: 50,
    height: 50,
    margin: 10
  },


  category: {
    flexDirection: 'row',
  },
  manCategory: {
    marginRight: 20
  },
  logInView: {
    flex: 100
  },
  productCategory: {
    margin: 20
  },
  productCodeView: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: 10
  },
  productCode: {
    fontSize: 11,
    color: '#868686',
    marginBottom: 5
  },
  dropdownMenu: {
    paddingBottom: 100
  },

});
