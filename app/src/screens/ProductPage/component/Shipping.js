import * as React from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';



class Shipping extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.shippedFrom}>This item shipped from UK</Text>
        <Text style={styles.shippedOption}>You can select shipping options at the checkout.</Text>
        <View style={styles.shippingOption}>
          <View style={styles.methodOption}>
            <Text style={styles.title}>METHOD</Text>
            <Text style={styles.method}>UK Standard</Text>
            <Text style={styles.method}>UK Standard</Text>
            <Text style={styles.method}>UK Standard</Text>
            <Text style={styles.method}>UK Standard</Text>
          </View>
          <View style={styles.priceOption}>
            <Text style={styles.title}>PRICE</Text>
            <Text style={styles.price}>2.00 €</Text>
            <Text style={styles.price}>2.00 €</Text>
            <Text style={styles.price}>2.00 €</Text>
            <Text style={styles.price}>2.00 €</Text>
          </View>
          <View style={styles.multiitenOption}>
            <Text style={styles.title}>MULTI ITEM</Text>
            <Text style={styles.multiitem}>1.00 €</Text>
            <Text style={styles.multiitem}>1.00 €</Text>
            <Text style={styles.multiitem}>1.00 €</Text>
            <Text style={styles.multiitem}>1.00 €</Text>
          </View>
          <View style={styles.daysOption}>
            <Text style={styles.title}>DAYS </Text>
            <Text style={styles.days}>3 day</Text>
            <Text style={styles.days}>3 day</Text>
            <Text style={styles.days}>3 day</Text>
            <Text style={styles.days}>3 day</Text>
          </View>
        </View>
        <Text style={styles.shippingBottom}>
          If you region isn't listed, the seller may still ship to youy ask them.
        </Text>
      </View>
    );
  }
}

export default Shipping;

const styles = StyleSheet.create({
  shippedFrom: {
    fontSize: 12,
    color: '#868686',
    marginTop: 20,
  },
  shippedOption: {
    fontSize: 12,
    color: '#868686',
    marginBottom: 20
  },
  shippingOption: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  methodOption: {
    marginRight: 30,
    alignItems: 'center'
  },
  priceOption: {
    marginRight: 20,
    alignItems: 'center'
  },
  multiitenOption: {
    marginRight: 20,
    alignItems: 'center'
  },
  daysOption: {
    marginRight: 20,
    alignItems: 'center'
  },
  title: {
    marginBottom: 10,
    color: '#868686'
  },
  method: {
    marginBottom: 7,
    fontSize: 13
  },
  price: {
    marginBottom: 7,
    fontSize: 13
  },
  multiitem: {
    marginBottom: 7,
    fontSize: 13
  },
  days: {
    marginBottom: 7,
    fontSize: 13
  },
  shippingBottom: {
    color: '#868686',
    fontSize: 12,
    marginTop: 20,
    marginBottom: 20
  }

});
