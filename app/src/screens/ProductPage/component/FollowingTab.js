import * as React from 'react';
import {Text, View, TouchableOpacity, StyleSheet, Image} from 'react-native';

const Ellipse45 = require('../images/Ellipse45.png');
const followingimage_1 = require('../images/followingimage_1.png');
const followingimage_2 = require('../images/followingimage_2.png');
const grouparrow = require('../images/grouparrow.png');
const sendarrow = require('../images/sendarrow.png');

class FollowingTab extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.followingWarpper}>
          <View style={styles.followingId}>
            <Image source={Ellipse45} style={styles.followingIcon} />
            <View style={styles.followingDetails}>
              <Text style={styles.followingName}>UNIQ BUTIQ </Text>
              <Text style={styles.followerCount}>232 FOLLOWERS</Text>
              <View style={styles.followMessage}>
                <TouchableOpacity style={styles.followButton}>
                  <Text style={styles.buttonText}>Follow</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.messageButton}>
                  <Text style={styles.messageText}>Message</Text>
                  <Image source={sendarrow} style={styles.sendarrow} />
                </TouchableOpacity>
              </View>
              <Text style={styles.idDesscription}>
                The passage is attributed to an unknown typesetter in the 15th
                century.
              </Text>
            </View>
          </View>
          <View style={styles.folloingImage}>
            <Image
              source={followingimage_1}
              style={styles.followingimage_1}
            />
            <Image
              source={followingimage_2}
              style={styles.followingimage_2}
            />
          </View>
          <View style={styles.showMore}>
            <Text style={styles.showMoreText}>Show more...</Text>
            <Image source={grouparrow} style={styles.grouparrow} />
          </View>
        </View>
      </View>
    );
  }
}

export default FollowingTab;

const styles = StyleSheet.create({
  followingWarpper: {
    borderBottomWidth: 1,
    paddingBottom: 20,
    marginBottom: 20
  },
  followingId: {
    flexDirection: 'row'
  },
  followingDetails: {
    paddingRight: 20,
    paddingLeft: 20
  },
  followingName: {
    fontSize: 18,
    marginBottom: 10
  },
  followerCount: {
    fontSize: 13,
    marginBottom: 15
  },
  followMessage: {
    flexDirection: 'row',
    marginBottom: 10
  },
  followButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 115,
    height: 33,
    borderWidth: 1
  },
  messageButton: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 115,
    height: 33,
  },
  messageText: {
    marginRight: 10
  },
  idDesscription: {
    fontSize: 13,
    color: '#6B6B6B',
  },
  folloingImage: {
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 30
  },
  followingimage_1: {
    marginRight: 30,
  },
  showMore: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  showMoreText: {
    marginBottom: 20
  },
  grouparrow: {
    paddingBottom: 20
  },
});
