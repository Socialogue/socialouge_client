import * as React from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
} from 'react-native';

import DropDownItem from 'react-native-drop-down-item';
import Description from './Description';
import Shipping from './Shipping';
import CompleteLook from './CompleteLook';
import PostWithComment from '../../../components/PostWithComment';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY} from "../../../utils/constants";

const IC_ARR_DOWN = require('../images/ic_arr_down.png');
const IC_ARR_UP = require('../images/ic_arr_up.png');

class DropdownMenu extends React.Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     contents: [
  //       {
  //         title: "Title 1",
  //         body: "Hi. I love this component. What do you think?"
  //       },
  //       {
  //         title: "See this one too",
  //         body: "Yes. You can have more items."
  //       },
  //       {
  //         title: "Thrid thing",
  //         body:
  //           "What about very long text? What about very long text? What about very long text? What about very long text? What about very long text? What about very long text? What about very long text? What about very long text? What about very long text? What about very long text? What about very long text? What about very long text?"
  //       }
  //     ]
  //   };
  // }

  render() {
    const {product} = this.props;
    return (
      <ScrollView style={{alignSelf: 'stretch'}}>
        {/*{*/}
        {/*  this.state.contents*/}
        {/*    ? this.state.contents.map((param, i) => {*/}
        {/*      return (*/}
        <DropDownItem
          // key={i}
          style={styles.dropDownItem}
          contentVisible={false}
          invisibleImage={IC_ARR_DOWN}
          visibleImage={IC_ARR_UP}
          useNativeDriver={false}
          header={
            // <View>
            //   <Text style={{
            //     fontSize: 16,
            //     color: 'blue',
            //   }}>{param.title}</Text>
            // </View>
            <View>
              <Text style={styles.dropTitle}>DESCRIPTION</Text>
            </View>
          }>
          {/*<Text style={[*/}
          {/*  styles.txt,*/}
          {/*  {*/}
          {/*    fontSize: 20,*/}
          {/*  }*/}
          {/*]}>*/}
          {/*  {param.body}*/}
          {/*</Text>*/}
          <View>
            <Description product={product} />
          </View>
        </DropDownItem>

        <DropDownItem
          style={styles.dropDownItem}
          contentVisible={false}
          invisibleImage={IC_ARR_DOWN}
          visibleImage={IC_ARR_UP}
          useNativeDriver={false}
          header={
            <View>
              <Text style={styles.dropTitle}>SHIPPING</Text>
            </View>
          }>
          <View>
            <Shipping />
          </View>
        </DropDownItem>

        <DropDownItem
          style={styles.dropDownItem}
          contentVisible={false}
          invisibleImage={IC_ARR_DOWN}
          visibleImage={IC_ARR_UP}
          useNativeDriver={false}
          header={
            <View>
              <Text style={styles.dropTitle}>COMPLETE LOOK</Text>
            </View>
          }>
          <View>
            <CompleteLook />
          </View>
        </DropDownItem>

        <DropDownItem
          style={styles.dropDownItem}
          contentVisible={false}
          invisibleImage={IC_ARR_DOWN}
          visibleImage={IC_ARR_UP}
          useNativeDriver={false}
          header={
            <View>
              <Text style={styles.dropTitle}>MATCH STYLE</Text>
            </View>
          }>
          <View>
            <Text>body</Text>
          </View>
        </DropDownItem>

        <DropDownItem

          invisibleImage={IC_ARR_DOWN}
          visibleImage={IC_ARR_UP}
          useNativeDriver={false}
          header={
            <View>
              <Text style={styles.dropTitle}>POSTS</Text>
            </View>
          }>
          <View style={styles.postWarpper}>
            <PostWithComment />
            <PostWithComment />
          </View>
        </DropDownItem>
        {/*      );*/}
        {/*    })*/}
        {/*    : null*/}
        {/*}*/}

      </ScrollView>
    );
  }
}

export default DropdownMenu;

const styles = StyleSheet.create({
  container: {

  },
  dropDownItem: {
    marginBottom: 20
  },
  desTitle: {
    color: PRIMARY_GRAY
  },
  dropTitle: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  }



});
