import * as React from 'react';
import { Image, StyleSheet, Text, View, ScrollView, TouchableOpacity } from "react-native";

import PostTab from './PostTab';
import FavoriteTab from './FavoriteTab';
import FollowingTab from './FollowingTab';
import ReviewTab from './ReviewTab';

class ProfileTab extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      activetab: 'post'
    };
  }

  render() {

    const {activetab} = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.tabButton}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.setState({activetab: 'post'})}>
            <Text
              style={
                activetab === 'post'
                  ? styles.activeButtonText
                  : styles.inActiveButtonText
              }>
              POSTS
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.setState({activetab: 'favorite'})}>
            <Text
              style={
                activetab === 'favorite'
                  ? styles.activeButtonText
                  : styles.inActiveButtonText
              }>
              FAVORITES
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.setState({activetab: 'following'})}>
            <Text
              style={
                activetab === 'following'
                  ? styles.activeButtonText
                  : styles.inActiveButtonText
              }>
              FOLLOWING
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.lastButton}
            onPress={() => this.setState({activetab: 'review'})}>
            <Text
              style={
                activetab === 'review'
                  ? styles.activeButtonText
                  : styles.inActiveButtonText
              }>
              REVIEWS
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.tabContent}>
          {activetab === 'post' && <PostTab />}
          {activetab === 'favorite' && <FavoriteTab />}
          {activetab === 'following' && <FollowingTab />}
          {activetab === 'review' && <ReviewTab />}
        </View>
      </View>
    );
  }
}

export default ProfileTab;

const styles = StyleSheet.create({
  container: {
    padding: 20
  },
  button: {
    marginRight: 28,
  },
  tabButton: {
    justifyContent: 'center',
    flexDirection: 'row',
    borderBottomWidth: 1,
    marginBottom: 20,
  },

  activeButtonText: {
    color: '#000',
    borderBottomWidth: 3,
    paddingBottom: 5
  },
  inActiveButtonText: {
    color: '#868686',
  },
});
