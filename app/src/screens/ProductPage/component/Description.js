import * as React from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../../utils/constants";



class Description extends React.Component {
  render() {
    const {product} = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.desbodyTitle}>{product.title}</Text>
        <Text style={styles.desbodydescription}>{product.description}</Text>
        <View style={styles.careSpecification}>
          <Text style={styles.careTitle}>CARE</Text>
          <View style={styles.care}>
            <View>
              <Text style={styles.carePoint}>{'\u2022'} Do not bleach</Text>
              <Text style={styles.carePoint}>{'\u2022'} Do not dry clean</Text>
              <Text style={styles.carePoint}>{'\u2022'} Machine wash warm</Text>
            </View>
            <View>
              <Text style={styles.carePoint}>{'\u2022'} Do not bleach</Text>
              <Text style={styles.carePoint}>{'\u2022'} Do not dry clean</Text>
              <Text style={styles.carePoint}>{'\u2022'} Machine wash warm</Text>
            </View>
          </View>
          <Text style={styles.specificationTitle}>SPECIFICATIONS</Text>
          <View style={styles.specification}>
            <View>
              <Text style={styles.specificationPoint}>{'\u2022'} Do not bleach</Text>
              <Text style={styles.specificationPoint}>{'\u2022'} Do not dry clean</Text>
              <Text style={styles.specificationPoint}>{'\u2022'} Machine wash warm</Text>
            </View>
            <View>
              <Text style={styles.specificationPoint}>{'\u2022'} Do not bleach</Text>
              <Text style={styles.specificationPoint}>{'\u2022'} Do not dry clean</Text>
              <Text style={styles.specificationPoint}>{'\u2022'} Machine wash warm</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default Description;

const styles = StyleSheet.create({
  desbodyTitle: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    fontSize: 18,
    marginTop: 20,
    marginBottom: 15
  },
  desbodydescription: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    color: '#868686',
    marginBottom: 20
  },
  careTitle: {
    fontSize: 14,
    marginBottom: 10
  },
  care: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  carePoint: {
    color: '#868686',
    fontSize: 12,
    marginTop: 10,
    marginBottom: 10
  },
  specificationTitle: {
    fontSize: 14,
    marginBottom: 10,
    marginTop: 20
  },
  specification: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  specificationPoint: {
    color: '#868686',
    fontSize: 12,
    marginTop: 10,
    marginBottom: 10
  },

});
