import * as React from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';
import CartItems from '../../../components/CartItems';



class CompleteLook extends React.Component {
  render() {
    return (
      <View style={styles.container}>

        <CartItems />

      </View>
    );
  }
}

export default CompleteLook;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: 300
  }

});
