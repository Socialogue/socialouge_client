import * as React from 'react';
import {
  Image,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';

import DropDownPicker from 'react-native-dropdown-picker';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY, PRIMARY_WHITE, LIGHT_GRAY} from "../../../utils/constants";
import PrimaryButton from "../../../components/PrimaryButton";
import { navigateToAppMenu } from '../../../navigator';
import { connect } from 'react-redux';
import { selectUser } from 'core/selectors/user';
import { selectProduct } from 'core/selectors/products';
import { selectCartData , selectCartPrice } from 'core/selectors/cart';
import { addToCart , getProductDetails } from 'core/actions/productActionCreators';
import { addToCartDb } from 'core/actions/cartActionCreators';
import {getImageOriginalUrl} from "core/utils/helpers";

const Like = require('../images/Like.png');
const Favorite = require('../images/Favorite.png');
const share = require('../images/Share.png');
const userAvater = require('../../../images/userAvater.png');



class ProductDetails extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  onPressButton = () => {
    this.setState({loading: true});
  }

  getPrice = (product) => {
    if (product.variations && product.variations.length) {
      return product.variations[0].price;
    }
    return '';
  };

  getSize = (product) => {
    if (product.variations && product.variations.length) {
      return product.variations[0].sizes.lt;
    }
    return '';
  };

  render() {
    const { product } = this.props;
    console.log('products', product);
    const colors = [];
    if(product.variations && product.variations.length) {
      product.variations.map((variation) => {
        let color = variation.color.name
        colors.push({label: color, value: color})
      });
    }


    return (
      <View style={styles.container}>
        <Text style={styles.productTitle}>{product.title}</Text>
        <Text style={styles.byShopName}>by {product.shopId ? product.shopId.shopName : ''}</Text>
        <View style={styles.productIden}>
          <View style={styles.shopContact}>
            <View style={styles.userContact}>
              <Image
                source={product.userId === undefined ? userAvater : {uri:getImageOriginalUrl(product.userId.profileImage)}}
                style={{ width: 35, height: 35, borderRadius: 37.5, marginRight: 10 }} />
              <View>
                <Text style={styles.byName}>{product.userId ? product.userId.fullName : ''}</Text>
              </View>
            </View>
            <TouchableOpacity
              style={styles.sellerbutton}
              onPress={() => navigateToAppMenu(this.props.componentId)}
            >
              <Text style={styles.byName}>Contact seller</Text>
            </TouchableOpacity>
          </View>
        </View>
        <Text style={styles.productPrice}>€ {this.getPrice(product)}</Text>
        <View style={styles.colorPicker}>
          <Text style={styles.colorList}>COLOR LIST:</Text>
          <DropDownPicker
            items={colors}
            defaultIndex={0}
            showArrow={true}
            containerStyle={{height: 40}}
            onChangeItem={item => console.log(item.label, item.value)}
          />
        </View>
        <View style={styles.productSize}>
          <View style={styles.sizeText}>
            <Text style={{
              fontSize: 13,
              fontFamily: GROTESK_SEMIBOLD,
              color: PRIMARY_BLACK
            }}>
              SIZE:
            </Text>
            <Text style={styles.sizeGuides}>Size guides</Text>
          </View>
          <View style={styles.sizeBox}>
            <View style={[styles.sizeBoxCount, styles.active]}>
              <Text>{this.getSize(product)}</Text>
            </View>
          </View>
        </View>
        <View style={styles.abbToCartWarpper}>
            <PrimaryButton
              loading={this.state.loading}
              title='add to cart'
              onPress={this.onPressButton}/>
        </View>
        <View style={styles.productLikes}>
          <Image source={Like} style={{width: 22, height: 22}} />
          <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK, marginLeft: 10}}>{product.userId ? product.userId.totalLikedPost : ''} LIKES</Text>
          <Image source={Favorite} style={{width: 22, height: 22, marginLeft: 10}} />
          <Image source={share} style={{width: 22, height: 22, marginLeft: 10}} />
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    user: selectUser(state),
    product: selectProduct(state),
    cartData: selectCartData(state),
    cartPrice: selectCartPrice(state)
  }),
  {
    getProductDetails,
    addToCart,
    addToCartDb
  }
)(ProductDetails);

const styles = StyleSheet.create({
   container: {
   paddingVertical: 15,
  },
  productTitle: {
    fontSize: 16,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  },
  shopContact: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 5
  },
  userContact: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  sellerbutton: {
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 5
  },
  byName: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  },
  byShopName: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    marginVertical: 5
  },
  productPrice: {
    fontSize: 18,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  },
  colorPicker: {
    marginBottom: 30
  },
  colorList: {
     fontSize: 13,
    marginTop: 30,
    marginBottom: 5,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  },
  sizeText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  sizeGuides: {
    color: PRIMARY_GRAY,
    fontSize: 13,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  },
  sizeBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20
  },
  sizeBoxCount: {
    width: 70,
    height: 40,
    borderWidth: 1,
    borderColor: LIGHT_GRAY,
    justifyContent: 'center',
    alignItems: 'center'
  },
  active: {
    borderColor: PRIMARY_BLACK
  },
  addToCartButton: {
    backgroundColor: PRIMARY_BLACK,
    height: 57,
    justifyContent: 'center',
    alignItems: 'center'
  },
  addToCartText: {
    color: PRIMARY_WHITE
  },
  productLikes: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 10
  },
  marginLeft: {
    marginLeft: 10
  }
});
