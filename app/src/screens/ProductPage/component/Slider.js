import * as React from 'react';
import {
  Image,
  StyleSheet,
  View,
} from 'react-native';

import Swiper from 'react-native-swiper';

const sliderimage = require('../images/sliderimage.png');

class Slider extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Swiper style={styles.wrapper} showsPagination={false} >
          <View style={styles.slide1}>
            <Image source={sliderimage} style={styles.sliderimage} />
          </View>
          <View style={styles.slide2}>
            <Image source={sliderimage} style={styles.sliderimage} />
          </View>
          <View style={styles.slide3}>
            <Image source={sliderimage} style={styles.sliderimage} />
          </View>
        </Swiper>
      </View>
    );
  }
}

export default Slider;

const styles = StyleSheet.create({
  wrapper: {
    height: 480
  },
  sliderimage: {
    width: '100%',
    height: '100%'
  }
});
