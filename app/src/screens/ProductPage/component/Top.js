import * as React from 'react';
import { Image, StyleSheet, Text, View, ScrollView, TouchableOpacity } from "react-native";

const filterArrow = require('../images/downarrow.png');
const sortArrow = require('../images/downarrow.png');
const viewArrow = require('../images/View.png');
const coverphoto = require('../images/coverphoto.png');
const message = require('../images/massegeicon.png');

class Top extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.cover}>
          <Image source={coverphoto} style={styles.coverphoto} />
        </View>
        <View style={styles.nameProfile}>
          <View style={styles.name}>
            <Text style={styles.nameTitle}>JORA JOHANSON</Text>
            <Text style={styles.followersCount}>232 FOLLOWERS</Text>
            <Text style={styles.editProfile}>Edit Profile</Text>
          </View>
          <View style={styles.follow}>
            <TouchableOpacity
              style={styles.followButton}
              >
              <Text style={styles.followText}>Follow</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.messageButton}
            >
              <Text style={styles.messageText}>Message</Text>
              <Image source={message} style={styles.messageIcon} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default Top;

const styles = StyleSheet.create({
  container: {

  },
  cover: {
    flex: 30
  },
  nameProfile: {
    flexDirection: 'row',
    flex: 100,
    padding: 20,
    marginTop: 40
  },
  nameTitle: {
    fontSize: 30
  },
  name: {
    flex: 60
  },
  followersCount: {
    marginTop: 10,
    marginBottom: 10
  },
  editProfile: {
    color: '#868686'
  },
  follow: {
    flex: 40
  },
  followButton: {
    borderWidth: 1,
    height: 33,
    width: 115,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20
  },
  messageButton: {
    flexDirection: 'row',
    borderWidth: 1,
    height: 33,
    width: 115,
    justifyContent: 'center',
    alignItems: 'center'
  },
  messageText: {
    marginRight: 10
  }

});
