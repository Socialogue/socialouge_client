import * as React from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';


const largeimage = require('../images/largeimage.png');
const smallimage_1 = require('../images/smallimage-1.png');
const smallimage_2 = require('../images/smallimage-2.png');
const smallimage_3 = require('../images/smallimage-3.png');
const grouparrow = require('../images/grouparrow.png');

import PostSimple from "../../../components/PostSimple";

class FavoriteTab extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.outfitWarpper}>
          <Text style={styles.outfitHeadTitle}>MY OUTFITS </Text>
          <View style={styles.largeImageView}>
            <Image source={largeimage} style={styles.largeImage} />
          </View>
          <View style={styles.smallImageView}>
            <Image source={smallimage_1} style={styles.smallimage_1} />
            <Image source={smallimage_2} style={styles.smallimage_2} />
            <Image source={smallimage_3} style={styles.smallimage_3} />
          </View>
          <View style={styles.outfitBottom}>
            <Text style={styles.outfitTitle}>Winter Outfit 2020</Text>
            <Text style={styles.outfitProduct}>14 Products</Text>
          </View>
          <View style={styles.showMore}>
            <Text style={styles.showMoreText}>Show more...</Text>
            <Image source={grouparrow} style={styles.grouparrow}/>
          </View>
        </View>
        <View style={styles.postWarpper}>
          <PostSimple />
          <View style={styles.showMore}>
            <Text style={styles.showMoreText}>Show more...</Text>
            <Image source={grouparrow} style={styles.grouparrow}/>
          </View>
        </View>
      </View>
    );
  }
}

export default FavoriteTab;

const styles = StyleSheet.create({
  outfitWarpper: {
    borderBottomWidth: 1,
    paddingBottom: 30,
    marginBottom: 20
  },
  outfitHeadTitle: {
    fontSize: 18,
    marginBottom: 20
  },
  largeImage: {
    width: '100%'
  },
  smallImageView: {
    flexDirection: 'row',
    marginTop: 20,
    marginBottom: 30
  },
  smallimage_1: {
    width: '31%'
  },
  smallimage_2: {
    width: '31%',
    marginLeft: 10,
    marginRight: 10
  },
  smallimage_3: {
    width: '31%'
  },
  outfitBottom: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  outfitTitle: {
    fontSize: 18,
    marginBottom: 10
  },
  outfitProduct: {
    marginBottom: 30
  },
  showMore: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  showMoreText: {
    marginBottom: 20
  },
  grouparrow: {
    paddingBottom: 20
  },


});
