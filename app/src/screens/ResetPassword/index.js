import * as React from 'react';
import {Image, StyleSheet, Text, TextInput, View} from 'react-native';
import {Navigation} from "react-native-navigation";
import styles from './styles';

import {GROTESK_SEMIBOLD, PRIMARY_BLACK, SECONDARY_ORANGE} from '../../utils/constants';
import PrimaryButton from "../../components/PrimaryButton";
import {navigateToLogin} from "../../navigator";

const leftArrow = require('.././../images/leftArrow.png');
const Socialoguelogo = require('.././../images/Socialoguelogo.png');



class ResetPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }
  onPressButton = () => {
    this.setState({loading: true});
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerWarpper}>
          <View style={styles.headTop}>
            <Image source={leftArrow} style={{width: 6, height: 10, marginRight: 10}} />
            <Text
              style={styles.backTo}
              onPress={() => navigateToLogin(this.props.componentId)}
            >Back to main
            </Text>
          </View>
        </View>
        <View style={styles.bodyWarpper}>
          <Image source={Socialoguelogo} style={{width: 100, height: 25, marginRight: 10}} />
          <Text style={styles.ressetText}>resset password<Text style={{fontSize: 50, color: SECONDARY_ORANGE}}>.</Text></Text>
          <View style={styles.passInput}>
            <Text style={styles.title}>NEW PASSWORD</Text>
            <TextInput
              secureTextEntry={true}
              style={styles.newPass}
              placeholder="New Password"
            />
            <Text style={styles.title}>REPEAT NEW PASSWORD</Text>
            <TextInput
              secureTextEntry={true}
              style={styles.rePass}
              placeholder="Repeat New Password"

            />
          </View>
          <View style={styles.resstWrapper}>
            <PrimaryButton
              loading={this.state.loading}
              title='resset'
              onPress={this.onPressButton}/>
          </View>
          <View style={styles.bottom}>
            <Text
              style={{color: PRIMARY_BLACK, fontFamily: GROTESK_SEMIBOLD,}}
              onPress={() => navigateToLogin(this.props.componentId)}
            >Back to Login
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

export default ResetPassword;


