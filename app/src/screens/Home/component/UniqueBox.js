import * as React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';

const grouparrow = require('../images/grouparrow.png');

class UniqueBox extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.uniqueTextBold}>unique eco-brands .</Text>
        <Text style={styles.uniqueTextRegular}>
          social marketplace catalogue
        </Text>
        <Image source={grouparrow} style={styles.grouparrow} />
      </View>
    );
  }
}

export default UniqueBox;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 300,
  },
  uniqueTextRegular: {
    marginTop: 10,
    marginBottom: 20,
    fontSize: 16,
  },
  uniqueTextBold: {
    fontSize: 30,
  },
});
