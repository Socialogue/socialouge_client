import * as React from 'react';
import { Image, StyleSheet, Text, TextInput, View } from "react-native";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../../utils/constants";

const Like = require('../../../images/Like.png');
const Comment = require('../../../images/Comment.png');
const Ellipse45 = require('../images/Ellipse45.png');
const sendarrow = require('../../../images/rightArrow.png');


class PostStatus extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.postWarpper}>
          <View style={styles.idWarpper}>
            <View style={styles.idTitle}>
              <Image source={Ellipse45} style={styles.ellipse} />
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>BUTIQ K</Text>
            </View>
            <Text style={styles.desscription}>
              There are many variations of passages of Lorem Ipsum available, but
              the majority have suffered alteration in some form.
            </Text>
            <View style={styles.timeSet}>
              <Text style={styles.byTime}>2 Hours Ago</Text>
              <Text style={styles.byReply}>REPLY</Text>
            </View>
          </View>
          <View style={styles.replyWarpper}>
            <View style={styles.idTitle}>
              <Image source={Ellipse45} style={styles.ellipse} />
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>BUTIQ K</Text>
            </View>
            <Text style={styles.desscription}>
              There are many variations of passages of Lorem Ipsum available, but
              the majority have suffered alteration in some form.
            </Text>
            <View style={styles.timeSet}>
              <Text style={styles.byTime}>2 Hours Ago</Text>
              <Text style={{fontSize: 13, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>REPLY</Text>
            </View>
            <View style={styles.addReply}>
              <View style={styles.replyText}>
                <TextInput
                  style={styles.replyInput}
                  placeholder="Add reply"
                />
              </View>
              <View style={styles.replyButton}>
                <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>Reply</Text>
                <Image source={sendarrow} style={{width: 20, height: 22}} />
              </View>
            </View>
            <Text style={styles.loadMore}>LOAD MORE...</Text>
          </View>
        </View>
        <View style={styles.addPost}>
          <View style={styles.replyText}>
            <TextInput
              style={styles.replyInput}
              placeholder="Add comment..."
            />
          </View>
          <View style={styles.replyButton}>
            <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>Post</Text>
            <Image source={sendarrow} style={{width: 20, height: 22}} />
          </View>
        </View>
        <View style={styles.reactBox}>
          <Image source={Like} style={{width: 20, height: 22, marginRight: 10}} />
          <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>LIKES</Text>
          <Image source={Comment} style={{width: 20, height: 22, marginRight: 10, marginLeft: 10}} />
          <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>COMMENT</Text>
        </View>
      </View>
    );
  }
}

export default PostStatus;

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  idWarpper: {
    marginLeft: 20
  },
  postWarpper: {
    borderLeftWidth: 1,
    marginBottom: 10
  },
  postCommentReply: {
    width: '100%',
    height: 500,
  },
  idTitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  ellipse: {
    marginRight: 5,
  },
  desscription: {
    color: '#868686',
    marginTop: 10,
  },
  timeSet: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
  },
  byTime: {
    marginRight: 10,
    fontSize: 12
  },
  replyWarpper: {
    marginLeft: 40
  },
  addReply: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1
  },
  replyButton: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  loadMore: {
    marginTop: 20,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  addPost: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1
  },

  reactBox: {
    flexDirection: 'row',
    marginTop: 10,
  },
  like: {
    marginRight: 10,
  },
  comment: {
    marginRight: 10,
    marginLeft: 10,
  },
});
