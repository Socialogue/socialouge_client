import * as React from 'react';
import {Dimensions, Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import TimeAgo from '../../../components/Time';
import {Navigation} from "react-native-navigation";
import {
  navigateToChatConversation,
  navigateToProductPage,
  navigateToPublicUserProfile
} from '../../../navigator';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../../utils/constants";
import {connect} from "react-redux";
import {selectUser} from "core/selectors/user";
import {selectProduct} from "core/selectors/products";
import {selectCartData, selectCartPrice} from "core/selectors/cart";
import {addToCart, getProductDetails} from "core/actions/productActionCreators";
import {addToCartDb} from "core/actions/cartActionCreators";

const Placeholder = require('../../../images/Placeholder_product.png');
const Favorite = require('../../../images/Favorite.png');
const share = require('../../../images/share.png');


class Product extends React.Component {

  goToProductDetailsPage = () => {
    this.props.getProductDetails(this.props.item._id);
    navigateToProductPage(this.props.componentId, this.props.item);
  };

  render() {
    const {item} = this.props;
    // console.log('testeste', item);
    return (
      <View style={styles.container}>
        <TouchableOpacity
          activeOpacity={.8}
          onPress={this.goToProductDetailsPage}>
          <Image source={item.mainImage === undefined ? Placeholder : { uri: item.mainImage }}
                 style={styles.productImage}/>
        </TouchableOpacity>
        <Text style={styles.postTitle}>{item.title}</Text>
        <TouchableOpacity
          activeOpacity={.8}
          onPress={() => navigateToPublicUserProfile(this.props.componentId)}>
          <Text style={styles.byTitle}>{item.userId.fullName}</Text>
        </TouchableOpacity>
        <TimeAgo time={item.createdAt}/>
        <View style={styles.bottomWarpper}>
          <View style={styles.priceBox}>
            <Text style={styles.price}>30 €</Text>
          </View>
          <View style={styles.reactBox}>
            <Image source={Favorite} style={{ width: 20, height: 22, marginRight: 20 }}/>
            <Image source={share} style={{ width: 20, height: 22 }}/>
          </View>
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    product: selectProduct(state),
  }),
  {
    getProductDetails,
  }
)(Product);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  },
  productImage: {
    width: '100%',
    height: 400
  },
  postTitle: {
    fontSize: 16,
    paddingVertical: 10,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  byTitle: {
    fontSize: 14,
    paddingVertical: 5,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  bottomWarpper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 5,

  },
  price: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  reactBox: {
    flexDirection: 'row',
  },
});
