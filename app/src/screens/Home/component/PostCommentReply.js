import * as React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY} from "../../../utils/constants";

import TimeAgo from '../../../components/Time';

const postCommentReply = require('../images/images-5.png');
const Like = require('../../../images/Like.png');
const Comment = require('../../../images/Comment.png');
const Ellipse45 = require('../../../images/Ellipse45.png');
const userAvater = require('../../../images/userAvater.png');

class PostCommentReply extends React.Component {
  render() {
    const {item} = this.props;

    // console.log('item', item);
    return (
      <View style={styles.container}>
        <Image source={item.images[0].url === undefined ? postCommentReply : {uri: item.images[0].url}} style={styles.postCommentReply} />
        <View style={styles.bottomWarpper}>
          <View style={styles.byTitle}>
            <Image source={userAvater} style={{ width: 40, height: 40, borderRadius: 37.5 }} />
            <View style={styles.nameTitle}>
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>{item.userId.name}</Text>
              <TimeAgo time={item.createdAt} />
            </View>
          </View>
        </View>
        <Text style={styles.desscription}>
          {item.content}
        </Text>
        <View style={styles.reactBox}>
          <Image source={Like} style={{width: 20, height: 22, marginRight: 10}} />
          <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>{item.totalLike} LIKES</Text>
          <Image source={Comment} style={{width: 20, height: 22, marginRight: 10, marginLeft: 10}} />
          <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>{item.totalComment} COMMENT</Text>
        </View>
      </View>
    );
  }
}

export default PostCommentReply;

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  postCommentReply: {
    width: '100%',
    height: 500,
  },
  byTitle: {
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
  },
  nameTitle: {
    marginLeft: 10
  },
  // bottomWarpper: {
  //   flexDirection: 'row',
  //   justifyContent: 'space-between',
  // },
  reactBox: {
    flexDirection: 'row',
    marginTop: 10,
  },
  like: {
    marginRight: 10,
  },
  comment: {
    marginRight: 10,
    marginLeft: 10,
  },
  desscription: {
    color: PRIMARY_GRAY,
  },
});
