import * as React from 'react';
import {Dimensions, Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import TimeAgo from '../../../components/Time';
import {Navigation} from "react-native-navigation";
import {
  navigateToChatConversation,
  navigateToProductPage,
  navigateToPublicUserProfile
} from '../../../navigator';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../../utils/constants";

const Placeholder = require('../../../images/Placeholder_product.png');
const Favorite = require('../../../images/Favorite.png');
const share = require('../../../images/share.png');


class Product extends React.Component {
  render() {
    const {item} = this.props;
    console.log('product title', item)
    return (
      <View style={styles.container}>
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => navigateToProductPage(this.props.componentId)}>
          <Image source={item.mainImage === undefined ? Placeholder : { uri: item.mainImage }}
                 style={styles.productImage}/>
        </TouchableOpacity>
        <Text style={styles.postTitle}>{item.title}</Text>
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => navigateToPublicUserProfile(this.props.componentId)}>
          <Text style={styles.byTitle}>{item.userId.fullName}</Text>
        </TouchableOpacity>
        <TimeAgo time={item.createdAt}/>
        <View style={styles.bottomWarpper}>
          <View style={styles.priceBox}>
            <Text style={styles.price}>30 €</Text>
          </View>
          <View style={styles.reactBox}>
            <Image source={Favorite} style={{ width: 15, height: 17, marginRight: 10 }}/>
            <Image source={share} style={{ width: 15, height: 17 }}/>
          </View>
        </View>
      </View>
    );
  }
}

export default Product;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  productImage: {
    width: '100%',
    height: 250,
  },
  postTitle: {
    fontSize: 12,
    paddingVertical: 5,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  byTitle: {
    fontSize: 12,
    paddingVertical: 5,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  bottomWarpper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 5,

  },
  price: {
    fontSize: 12,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  reactBox: {
    flexDirection: 'row',
  },
});
