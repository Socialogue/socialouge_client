import * as React from 'react';
import { Image, StyleSheet, Text, View, TextInput, ScrollView } from "react-native";

import DropDownItem from "react-native-drop-down-item";

const search = require('../images/search.png');
const home = require('../images/homeicons.png');
const downarrow = require('../images/downarrow.png');
const shop = require('../images/shopicons.png');
const sell = require('../images/sellicon.png');
const IC_ARR_DOWN = require('../images/ic_arr_down.png');
const IC_ARR_UP = require('../images/ic_arr_up.png');

class MenuWarpper extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.searchWarpper}>
          <Image source={search} style={search} />
          <View style={styles.searchInput}>
            <TextInput
              style={styles.searchBar}
              placeholder="Search..."
            />
          </View>
        </View>
        <ScrollView style={{alignSelf: 'stretch'}}>
          <View style={styles.downMenu}>
            <DropDownItem
              style={styles.dropDownItem}
              contentVisible={false}
              invisibleImage={IC_ARR_DOWN}
              visibleImage={IC_ARR_UP}
              useNativeDriver={false}
              header={
                <View style={styles.homeMenu}>
                  <Image source={home} style={styles.homeIcon} />
                  <Text style={styles.homeText}>HOME</Text>
                </View>
              }>
              <View>
                <Text>Content</Text>
              </View>
            </DropDownItem>
          </View>
          <View style={styles.downMenu}>
            <DropDownItem
              style={styles.dropDownItem}
              contentVisible={false}
              invisibleImage={IC_ARR_DOWN}
              visibleImage={IC_ARR_UP}
              useNativeDriver={false}
              header={
                <View style={styles.shopMenu}>
                  <Image source={shop} style={styles.shopIcon} />
                  <Text style={styles.shopText}>SHOP</Text>
                </View>
              }>
              <View>
                <Text>Content</Text>
              </View>
            </DropDownItem>
          </View>
          <View style={styles.downMenu}>
            <DropDownItem
              style={styles.dropDownItem}
              contentVisible={false}
              invisibleImage={IC_ARR_DOWN}
              visibleImage={IC_ARR_UP}
              useNativeDriver={false}
              header={
                <View style={styles.sellMenu}>
                  <Image source={sell} style={styles.sellIcon} />
                  <Text style={styles.sellText}>SELLS</Text>
                </View>
              }>
              <View>
                <Text>Content</Text>
              </View>
            </DropDownItem>
          </View>
        </ScrollView>

        <View style={styles.bottomWarpper}>
          <View style={styles.language}>
            <Text style={styles.languageText}>LANGUAGE</Text>
            <Image source={downarrow} style={styles.languageIcon} />
          </View>
          <View style={styles.loginSignup}>
            <Text style={styles.logIn}>LOGIN</Text>
            <Text style={styles.signUp}>SIGNUP</Text>
          </View>
        </View>
      </View>
    );
  }
}

export default MenuWarpper;

const styles = StyleSheet.create({
  container: {
    padding: 20,
    flex: 100
  },
  searchWarpper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 30
  },
  search: {
    marginRight: 20,
  },
  searchInput: {
    borderBottomWidth: 1,
    width: '95%'
  },
  homeMenu: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#E5E5E5'
  },
  downMenu: {
    marginTop: 10
  },
  homeIcon: {
    marginRight: 10
  },
  shopMenu: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#E5E5E5'
  },
  shopIcon: {
    marginRight: 10
  },
  sellMenu: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#E5E5E5'
  },
  sellIcon: {
    marginRight: 10
  },
  bottomWarpper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  language: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  languageText: {
    marginRight: 5,
  },
  loginSignup: {
    flexDirection: 'row',
  },
  logIn: {
    marginRight: 10
  }

});
