import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity, Image, TextInput,
} from "react-native";

const search = require('../images/search.png');
const searchresultimage = require('../images/searchresultimage.png');
const Ellipse45 = require('../images/Ellipse45.png');



class SearchSection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activetab: 'clothes',
    };
  }

  render() {
    const {activetab} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.tabContainer}>
          <View style={styles.tabButton}>
            <TouchableOpacity
              style={styles.clothesButton}
              onPress={() => this.setState({activetab: 'clothes'})}>
              <Text
                style={
                  activetab === 'clothes'
                    ? styles.activeClothesButtonText
                    : styles.inActiveClothesButtonText
                }>
                CLOTHES
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.shopsButton}
              onPress={() => this.setState({activetab: 'shops'})}>
              <Text
                style={
                  activetab === 'shops'
                    ? styles.activeShopsButtonText
                    : styles.inActiveShopsButtonText
                }>
                SHOPS
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.accountsButton}
              onPress={() => this.setState({activetab: 'accounts'})}>
              <Text
                style={
                  activetab === 'accounts'
                    ? styles.activeAccountsButtonText
                    : styles.inActiveAccountsButtonText
                }>
                ACCOUNTS
              </Text>
            </TouchableOpacity>
          </View>

          <View style={styles.tabContent}>
            {activetab === 'clothes' &&
            <View>
              <View style={styles.searchWarpper}>
                <Image source={search} style={search} />
                <View style={styles.searchInput}>
                  <TextInput
                    style={styles.searchBar}
                    placeholder="Search..."
                  />
                </View>
              </View>
              <View style={styles.searchResult}>
                <Image source={searchresultimage} style={searchresultimage} />
                <View style={styles.searchDetails}>
                  <View style={styles.DetailsName}>
                    <Text style={styles.name}>Classic</Text>
                    <Text style={styles.searchName}>shart</Text>
                  </View>
                  <View style={styles.productsCategory}>
                    <Text style={styles.category}>Cloths</Text>
                    <Text style={styles.dots}>{'\u2022'}</Text>
                    <Text style={styles.productsCondition}>Fevorite</Text>
                  </View>
                </View>
              </View>
              <View style={styles.searchResult}>
                <Image source={searchresultimage} style={searchresultimage} />
                <View style={styles.searchDetails}>
                  <View style={styles.DetailsName}>
                    <Text style={styles.name}>Classic</Text>
                    <Text style={styles.searchName}>shart</Text>
                  </View>
                  <View style={styles.productsCategory}>
                    <Text style={styles.category}>Cloths</Text>
                    <Text style={styles.dots}>{'\u2022'}</Text>
                    <Text style={styles.productsCondition}>Fevorite</Text>
                  </View>
                </View>
              </View>
            </View>
            }
            {activetab === 'shops' &&
            <View>
              <View style={styles.searchWarpper}>
                <Image source={search} style={search} />
                <View style={styles.searchInput}>
                  <TextInput
                    style={styles.searchBar}
                    placeholder="Search..."
                  />
                </View>
              </View>
            </View>
            }
            {activetab === 'accounts' &&
              <View>
                <View style={styles.searchWarpper}>
                  <Image source={search} style={search} />
                  <View style={styles.searchInput}>
                    <TextInput
                      style={styles.searchBar}
                      placeholder="Search..."
                    />
                  </View>
                </View>
                <View style={styles.searchResult}>
                  <Image source={Ellipse45} style={styles.reviewerIdIcon} />
                    <View style={styles.accountsDetailsName}>
                      <Text style={styles.name}>LILY-ROSE FLOWERS</Text>
                      <Text style={styles.searchName}>SHIRT</Text>
                    </View>
                </View>
                <View style={styles.searchResult}>
                  <Image source={Ellipse45} style={styles.reviewerIdIcon} />
                  <View style={styles.accountsDetailsName}>
                    <Text style={styles.name}>LILY-ROSE FLOWERS</Text>
                    <Text style={styles.searchName}>SHIRT</Text>
                  </View>
                </View>
              </View>
            }
          </View>
        </View>
      </View>
    );
  }
}

export default SearchSection;

const styles = StyleSheet.create({
  container: {
    padding: 20
  },
  tabContent: {
    height: '80 %',
  },
  tabButton: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  clothesButton: {
    marginRight: 20,
  },
  shopsButton: {
    marginRight: 20
  },
  accountsButton: {
    marginRight: 20
  },
  activeClothesButtonText: {
    color: '#000',
    borderBottomWidth: 3,
    paddingBottom: 7
  },
  inActiveClothesButtonText: {
    color: '#868686',
  },
  activeShopsButtonText: {
    color: '#000',
    borderBottomWidth: 3,
    paddingBottom: 7
  },
  inActiveShopsButtonText: {
    color: '#868686',
  },
  activeAccountsButtonText: {
    color: '#000',
    borderBottomWidth: 3,
    paddingBottom: 7
  },
  inActiveAccountsButtonText: {
    color: '#868686',
  },
  searchWarpper: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 10,
    marginBottom: 20,
    marginTop: 10
  },
  searchInput: {
    borderBottomWidth: 1,
    width: '90%',
    marginLeft: 10,
  },
  searchResult: {
    marginTop: 10,
    flexDirection: 'row'
  },
  searchDetails: {
    marginLeft: 15
  },
  DetailsName: {
    flexDirection: 'row'
  },
  searchName: {
    marginLeft: 5,
    color: '#FF6A00'
  },
  productsCategory: {
    flexDirection: 'row',

  },
  category: {
    color: '#8E8E8E'
  },
  dots: {
    color: '#8E8E8E',
    marginLeft: 10,
    marginRight: 10
  },
  productsCondition: {
    color: '#8E8E8E'
  },
  accountsDetailsName: {
    marginLeft: 15,
    flexDirection: 'row',
    alignItems: 'center'
  }

});
