import * as React from 'react';
import {Dimensions, Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Navigation} from "react-native-navigation";

import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../../utils/constants";
import {navigateToProductPage} from "../../../navigator";

const FirstPost = require('../images/images-2.png');
const Like = require('../images/Like.png');
const Favorite = require('../images/Favorite.png');


class SmallView extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => navigateToProductPage(this.props.componentId)}>
          <Image source={FirstPost} style={{width: '100%', height: 200}} />
          <Text style={styles.postTitle}>{this.props.item.title}</Text>
          <Text style={styles.byTitle}>by BUTIQ K</Text>
          <View style={styles.bottomWarpper}>
            <View style={styles.priceBox}>
              <Text style={styles.price}>30 €</Text>
            </View>
            <View style={styles.reactBox}>
              <Image source={Like} style={{width: 14, height: 16, marginRight: 10}} />
              <Image source={Favorite} style={{width: 14, height: 16}} />
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default SmallView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // padding: 20
    marginVertical: 20,
    marginHorizontal: 10,
  },
  postTitle: {
    fontSize: 12,
    paddingVertical: 5,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  byTitle: {
    fontSize: 11,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  bottomWarpper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  price: {
    fontSize: 11,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  reactBox: {
    flexDirection: 'row',
  },
});
