import * as React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY} from "../../../utils/constants";

import ReadMore from 'react-native-read-more-text';
import TimeAgo from '../../../components/Time';
import { navigateToProductPage, navigateToPublicUserProfile } from '../../../navigator';
import {getImageOriginalUrl} from "core/utils/helpers";

const postImage = require('../images/images-5.png');
const Like = require('../../../images/Like.png');
const Comment = require('../../../images/Comment.png');
const userAvater = require('../../../images/userAvater.png');

class Post extends React.Component {
  _renderTruncatedFooter = (handlePress) => {
    return (
      <Text style={{color: PRIMARY_BLACK, fontFamily: GROTESK_SEMIBOLD, marginTop: 5}} onPress={handlePress}>
        Read more
      </Text>
    );
  }

  _renderRevealedFooter = (handlePress) => {
    return (
      <Text style={{color: PRIMARY_BLACK, fontFamily: GROTESK_SEMIBOLD, marginTop: 5}} onPress={handlePress}>
        Show less
      </Text>
    );
  }
  _handleTextReady = () => {
    // ...
  }
  render() {

    const {item} = this.props;
    let imgSrc = postImage;
    if(item.images && item.images.length) {
      imgSrc = {uri: getImageOriginalUrl(item.images[0].url)};
    }
    // console.log('name', item.userId.fullName);
    return (
      <View style={styles.container}>
        <View style={styles.imgCover}>
          <Image source={imgSrc} style={styles.postImage} />
        </View>
        <View style={styles.bottomWarpper}>
          <TouchableOpacity
            activeOpacity={.7}
            onPress={() =>navigateToPublicUserProfile(this.props.componentId)}>
            <View style={styles.byTitle}>
              <Image source={userAvater} style={{ width: 40, height: 40, borderRadius: 37.5 }} />
              <View style={styles.nameTitle}>
                <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>{item.userId ? item.userId.fullName : ''}</Text>
                <TimeAgo time={item.createdAt} />
              </View>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.desscription}>
          <ReadMore
            numberOfLines={3}
            renderTruncatedFooter={this._renderTruncatedFooter}
            renderRevealedFooter={this._renderRevealedFooter}
            onReady={this._handleTextReady}>
            <Text>
              {item.content}
            </Text>
          </ReadMore>
        </View>

        <View style={styles.reactBox}>
          <Image source={Like} style={{width: 20, height: 22, marginRight: 10}} />
          <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>{item.totalLike} LIKES</Text>
          <Image source={Comment} style={{width: 20, height: 22, marginRight: 10, marginLeft: 10}} />
          <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>{item.totalComment} COMMENT</Text>
        </View>
      </View>
    );
  }
}

export default Post;

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  imgCover: {
    width: '100%',
    height: 450,
  },
  postImage: {
    width: '100%',
    height: '100%',
  },
  byTitle: {
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
  },
  nameTitle: {
    marginLeft: 10
  },
  // bottomWarpper: {
  //   flexDirection: 'row',
  //   justifyContent: 'space-between',
  // },
  reactBox: {
    flexDirection: 'row',
    marginTop: 10,
  },
  like: {
    marginRight: 10,
  },
  comment: {
    marginRight: 10,
    marginLeft: 10,
  },
  desscription: {
    color: PRIMARY_GRAY,
  },
});
