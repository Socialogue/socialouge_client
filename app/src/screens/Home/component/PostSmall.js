import * as React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY} from "../../../utils/constants";
import ReadMore from 'react-native-read-more-text';

import TimeAgo from '../../../components/Time';
import { navigateToProductPage, navigateToPublicUserProfile } from '../../../navigator';
import {getImageOriginalUrl} from "core/utils/helpers";

const postImage = require('../images/images-5.png');
const Like = require('../../../images/Like.png');
const Comment = require('../../../images/Comment.png');
const userAvater = require('../../../images/userAvater.png');

class PostSmall extends React.Component {
  _renderTruncatedFooter = (handlePress) => {
    return (
      <Text style={{color: PRIMARY_BLACK, fontFamily: GROTESK_SEMIBOLD, marginTop: 5, fontSize: 12}} onPress={handlePress}>
        Read more
      </Text>
    );
  }

  _renderRevealedFooter = (handlePress) => {
    return (
      <Text style={{color: PRIMARY_BLACK, fontFamily: GROTESK_SEMIBOLD, marginTop: 5, fontSize: 12}} onPress={handlePress}>
        Show less
      </Text>
    );
  }
  _handleTextReady = () => {
    // ...
  }
  render() {
    const {item} = this.props;
    // console.log('item', item);
    return (
      <View style={styles.container}>
        <Image source={item.images[0].url === undefined ? postImage : {uri: getImageOriginalUrl(item.images[0].url)}} style={styles.postImage} />
        <View style={styles.bottomWarpper}>
          <TouchableOpacity
            activeOpacity={.7}
            onPress={() =>navigateToPublicUserProfile(this.props.componentId)}>
            <View style={styles.byTitle}>
              <Image source={userAvater} style={{ width: 30, height: 30, borderRadius: 37.5 }} />
              <View style={styles.nameTitle}>
                <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK, fontSize: 12}}>{item.userId.fullName}</Text>
                <TimeAgo time={item.createdAt} />
              </View>
            </View>
          </TouchableOpacity>
        </View>
        <View>
          <ReadMore
            numberOfLines={3}
            renderTruncatedFooter={this._renderTruncatedFooter}
            renderRevealedFooter={this._renderRevealedFooter}
            onReady={this._handleTextReady}>
            <Text style={styles.desscription}>
              {item.content}
            </Text>
          </ReadMore>
        </View>
        <View style={styles.reactBox}>
          <View style={{flexDirection: 'row', marginBottom: 10}}>
            <Image source={Like} style={{width: 14, height: 16, marginRight: 5}} />
            <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK, fontSize: 11}}>{item.totalLike} LIKES</Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image source={Comment} style={{width: 14, height: 16, marginRight: 5}} />
            <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK, fontSize: 11}}>{item.totalComment} COMMENT</Text>
          </View>
        </View>
      </View>
    );
  }
}

export default PostSmall;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // padding: 20
    marginVertical: 20,
    marginHorizontal: 10,
  },
  postImage: {
    width: '100%',
    height: 250,
  },
  byTitle: {
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
  },
  nameTitle: {
    marginLeft: 10
  },
  // bottomWarpper: {
  //   flexDirection: 'row',
  //   justifyContent: 'space-between',
  // },
  reactBox: {
    marginTop: 10,
  },
  like: {
    marginRight: 10,
  },
  comment: {
    marginRight: 10,
    marginLeft: 10,
  },
  desscription: {
    color: PRIMARY_GRAY,
    fontSize: 11
  },
});
