import {StyleSheet, Text} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../utils/constants";
import * as React from "react";

export default StyleSheet.create({
  container: {
    flex: 100,
    position: 'relative'
  },
  headerWarpper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 60
  },
  bodyWarpper: {
    flex: 90,
  },
  logInPart: {
    flexDirection: 'row',
  },
  logInText: {
    marginLeft: 20,
    marginRight: 20,
  },
  menuTopbar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15
  },
  category: {
    flexDirection: 'row',
  },
  activeCategory: {
    marginRight: 20,
    color: '#868686'
  },
  manCategory: {
    marginRight: 20
  },
  logInView: {
    flex: 100
  },
  uniqueBox: {
    borderBottomWidth: 1,
  },
  uniqueTextRegular: {
    marginTop: 10,
    marginBottom: 20,
    fontSize: 16,
  },
  uniqueTextBold: {
    fontSize: 30,
  },
  lookWeekTop: {
    padding: 40,
    backgroundColor: '#000',
  },
  titleText: {
    color: '#fff',
    fontSize: 25,
    fontFamily: GROTESK_SEMIBOLD,
  },
  subText: {
    color: '#fff',
    fontSize: 16,
    marginTop: 5,
    marginBottom: 5,
  },
  postWarpper: {
    flex: 1,
  },
  bottomWarpper: {
    borderTopWidth: 1
  },

  itemInvisible: {
    backgroundColor: 'transparent',
  },
  item: {
    flex: 1,
    marginVertical: 20,
    marginHorizontal: 10,
  }

});
