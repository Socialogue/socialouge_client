import * as React from 'react';

import {FlatList, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../utils/constants";
import {navigateToProductPage, navigateToUserProfile} from "../../navigator";
import {selectFeed, selectToken, selectViewType} from "core/selectors/user";

import CartItems from '../../components/CartItems';
import Header from '../../components/Header';
import Post from './component/Post';
import PostSmall from "./component/PostSmall";
import PostStatus from './component/PostStatus';
import PostWithComment from '../../components/PostWithComment';
import Product from './component/Product';
import ProductSmall from './component/ProductSmall';
import Top from '../../components/Top';
import UniqueBox from './component/UniqueBox';
import { connect } from "react-redux";
import {getPostProducts} from "core/actions/homeActionCreators";
import {selectPosts} from "core/selectors/posts";

const sendarrowwhite = require('./images/sendarrowwhite.png');


const numColumns = 2;

class HomePage extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      postView: 1,
    };
  }

  onValueChange = (postView) => {
    this.setState({postView});
  };


  renderItem = ({ item }) => {
  //   if(item.type  === 'products') {
  //     return <PostCommentReply componentId={this.props.componentId} item={item}/>;
  //   }

    return (
      <Post componentId={this.props.componentId} item={item}/>
    );
  };
  _renderItem = ({ item }) => {
    if (item.empty === true) {
      return <View style={[styles.item, styles.itemInvisible]}/>;
    }
    return (
      <PostSmall componentId={this.props.componentId} item={item}/>
    );
  };

  renderItemProduct = ({ item }) => {
    return (
      <Product componentId={this.props.componentId} item={item}/>
    );
  };

  _renderItemProduct = ({ item }) => {
    if (item.empty === true) {
      return <View style={[styles.item, styles.itemInvisible]}/>;
    }
    return (
      <ProductSmall componentId={this.props.componentId} item={item}/>
    );
  };



  formatData = (DATA, numColumns) => {
    const totalRows = Math.floor(DATA.length / numColumns)
    let totalLastRow = DATA.length - (totalRows * numColumns)

    while (totalLastRow !== 0 && totalLastRow !== numColumns) {
      DATA.push({ id: 'blank', empty: true });
      totalLastRow++
    }

    return DATA;
  };



  componentDidMount = () => {
    try {
      this.props.getPostProducts();
    } catch (e) {

    }
  };

  componentDidUpdate = prevProps => {
    if (prevProps.feed !== this.props.feed) {
      if (this.props.feed) {

      }
    }
  };


  render() {

    return (
      <View style={styles.container}>
        <View style={styles.headerWarpper}>
          <Header componentId={this.props.componentId} />
        </View>

        <ScrollView>
          <View style={styles.top}>
            <Top
              postView={this.state.postView}
              onValueChange={this.onValueChange}
            />
          </View>
          <View style={styles.bodyWarpper}>
            <View style={styles.body}>
              {!this.props.isLoggedIn &&
                <View style={styles.uniqueBox}>
                  <UniqueBox />
                </View>
              }
            </View>
            <View style={styles.postWarpper}>
              {this.state.postView === 1 &&
                <View>
                {this.props.feed.products && <FlatList
                    data={this.props.feed.products}
                    renderItem={this.renderItemProduct}
                    keyExtractor={item => item._id.toString()}
                  />
                }
                {this.props.feed.posts && <FlatList
                    data={this.props.feed.posts}
                    renderItem={this.renderItem}
                    keyExtractor={item => item._id.toString()}
                  />
                }
                </View>
              }

              {this.state.postView === 2 &&
                <View style={{marginHorizontal: 10}}>
                {this.props.feed.posts && <FlatList
                    data={this.formatData(this.props.feed.posts, numColumns)}
                    renderItem={this._renderItem}
                    keyExtractor={item => item._id.toString()}
                    numColumns={numColumns}
                  />
                }
                {this.props.feed.products && <FlatList
                    data={this.formatData(this.props.feed.products, numColumns)}
                    renderItem={this._renderItemProduct}
                    keyExtractor={item => item._id.toString()}
                    numColumns={numColumns}
                  />
                }
                </View>
              }
            <PostWithComment />
            </View>
            <View style={styles.cartWarpper}>
              <CartItems />
            </View>
            <View style={styles.postCommentWarpper}>
              {/*<PostCommentReply />*/}
            </View>
            <View style={styles.postStatusWarpper}>
              <PostStatus />
            </View>
            <View style={styles.lookWeek}>
              <View style={styles.lookWeekTop}>
                <Text style={styles.titleText}>Look of the week.</Text>
                <Text style={styles.subText}>Get the look</Text>
                <Image source={sendarrowwhite} style={sendarrowwhite} />
              </View>
              <CartItems />
            </View>
          </View>
        </ScrollView>
        <View style={styles.bottomWarpper}>
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    posts: selectPosts(state),
    isLoggedIn: !!selectToken(state),
    auth: state.auth,
    sortViewType: selectViewType(state),
    feed: selectFeed(state)
  }),
  {
    getPostProducts
  }
)(HomePage);

const styles = StyleSheet.create({
  container: {
    flex: 100,
    position: 'relative'
  },
  headerWarpper: {
    height: 60
  },
  bodyWarpper: {
    flex: 90,
  },
  logInPart: {
    flexDirection: 'row',
  },
  logInText: {
    marginLeft: 20,
    marginRight: 20,
  },
  menuTopbar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15
  },
  category: {
    flexDirection: 'row',
  },
  activeCategory: {
    marginRight: 20,
    color: '#868686'
  },
  manCategory: {
    marginRight: 20
  },
  logInView: {
    flex: 100
  },
  uniqueBox: {
    borderBottomWidth: 1,
  },
  uniqueTextRegular: {
    marginTop: 10,
    marginBottom: 20,
    fontSize: 16,
  },
  uniqueTextBold: {
    fontSize: 30,
  },
  lookWeekTop: {
    padding: 40,
    backgroundColor: '#000',
  },
  titleText: {
    color: '#fff',
    fontSize: 25,
    fontFamily: GROTESK_SEMIBOLD,
  },
  subText: {
    color: '#fff',
    fontSize: 16,
    marginTop: 5,
    marginBottom: 5,
  },
  postWarpper: {
    flex: 1,
  },
  bottomWarpper: {
    borderTopWidth: 1
  },

  itemInvisible: {
    backgroundColor: 'transparent',
  },
  item: {
    flex: 1,
    marginVertical: 20,
    marginHorizontal: 10,
  }

});
