import {StyleSheet, Text} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY} from "../../utils/constants";
import * as React from "react";

export default StyleSheet.create({
  container: {
    flex: 100
  },
  headerWarpper: {
    // flex: 10
    height: 60
  },
  bodyWarpper: {
    flex: 90
  },
  bottomWarpper: {
    borderTopWidth: 1
  },
  searchWarpper: {
    padding: 20,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1
  },
  searchInput: {
    borderBottomWidth: 1,
    width: '78%',
    marginLeft: 10,
  },
  searchBar: {
    padding: 0
  },
  more: {
    width: 20,
    height: 10,
    marginLeft: 20,
  },
  listWarpper: {
    padding: 20,
    flexDirection: 'row',
    borderBottomWidth: 1
  },
  idName: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  idTitle: {
    color: '#000'
  },
  chatImg: {
    marginRight: 15
  },
  msgInitialView: {
    fontSize: 12,
    color: '#868686'
  },
  msgTime: {
    paddingLeft: '13%'
  },
  writeNew: {
    position: 'absolute',
    bottom: 20,
    right: 20
  },
  WritenewIcon: {
    width: 55,
    height: 55
  }
});
