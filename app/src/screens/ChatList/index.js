import * as React from 'react';
import {View, TouchableOpacity, Text, Image, StyleSheet, TextInput, ScrollView} from 'react-native';
import Header from "../../components/Header";
import styles from './styles';

import {navigateToChatConversation, navigateToChatList} from "../../navigator";


const search = require('../../images/search.png');
const more = require('../../images/more.png');
const Ellipse45 = require('../../images/Ellipse45.png');
const Writenew = require('../../images/Writenew.png');

class ChatList extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerWarpper}>
          <Header componentId={this.props.componentId} />
        </View>
        <View style={styles.bodyWarpper}>
          <View style={styles.searchWarpper}>
            <Image source={search} style={{width: 22, height: 22}} />
            <View style={styles.searchInput}>
              <TextInput
                style={styles.searchBar}
                placeholder="Search..."
              />
            </View>
            <Image source={more} style={styles.more}/>
          </View>
          <ScrollView>
            <TouchableOpacity
              onPress={() => navigateToChatConversation(this.props.componentId)}
              >

              <View style={styles.listWarpper}>
                <View style={styles.idName}>
                  <View style={styles.chatImg}>
                    <Image source={Ellipse45} style={styles.img} />
                  </View>
                  <View style={styles.id}>
                    <Text style={styles.idTitle}>NICO RUBIO</Text>
                    <Text style={styles.msgInitialView}>Hello I'am sending  tou a free sample... </Text>
                  </View>
                </View>
                <View style={styles.msgTime}>
                  <Text style={styles.time}>3 min</Text>
                </View>
              </View>

            </TouchableOpacity>

            <View style={styles.listWarpper}>
              <View style={styles.idName}>
                <View style={styles.chatImg}>
                  <Image source={Ellipse45} style={styles.img} />
                </View>
                <View style={styles.id}>
                  <Text style={styles.idTitle}>NICO RUBIO</Text>
                  <Text style={styles.msgInitialView}>Hello I'am sending  tou a free sample... </Text>
                </View>
              </View>
              <View style={styles.msgTime}>
                <Text style={styles.time}>3 min</Text>
              </View>
            </View>
          </ScrollView>
        </View>
        <View style={styles.writeNew}>
          <Image source={Writenew} style={styles.WritenewIcon} />
        </View>
        <View style={styles.bottomWarpper}>
        </View>
      </View>
    );
  }
}

export default ChatList;

