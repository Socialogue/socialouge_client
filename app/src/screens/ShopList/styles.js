import {StyleSheet, Text} from 'react-native';
import {GROTESK_SEMIBOLD, LIGHT_GRAY, PRIMARY_BLACK, PRIMARY_GRAY, SECONDARY_ORANGE} from "../../utils/constants";
import * as React from "react";

export default StyleSheet.create({
  container: {
    flex: 10,
  },
  headerWrapper: {
    height: 60
  },
  bodyWrapper: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20,
    // backgroundColor: 'red'
  },
  bottomWarpper: {
    borderTopWidth: 1
  },
  shopListTitle: {
    fontSize: 35,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  },
  shopListTitleText: {
    fontSize: 30,
    color: SECONDARY_ORANGE
  }
});
