import * as React from 'react';

import {FlatList, ScrollView, Text, View} from 'react-native';
import Header from "../../components/Header";
import ShopLargeView from "../../components/ShopLargeView";
import ShopSmallView from "../../components/ShopSmallView";
import Top from "../../components/Top";
import styles from './styles';
import { connect } from "react-redux";

import {selectUser, selectViewType} from "core/selectors/user";
import {selectAllShops, selectAllShopsCount, selectErrorMsg} from "core/selectors/shops";
import {getAllShops} from "core/actions/shopActionCreators";

const DATA = [
  {
    id: 1,
    title: 'UNIQ BUTIQ',
  },
  {
    id: 2,
    title: 'Second Item',
  },
  {
    id: 3,
    title: 'Third Item',
  },
];

class ShopList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      postView: 1
    };
  }

  onValueChange = (postView) => {
    this.setState({postView});
  };

  renderItem = ({ item }) => (
    <ShopLargeView item={item} />
  );

  _renderItem = ({ item }) => (
    <ShopSmallView item={item} />
  );

  render() {
    console.log('shop', this.props.allShops[0]);
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.headerWrapper}>
            <Header componentId={this.props.componentId} />
          </View>
          <Top postView={this.state.postView}
               onValueChange={this.onValueChange} />
          <View style={styles.bodyWrapper}>
            <View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.shopListTitle}>all shop<Text style={styles.shopListTitleText}>.</Text></Text>
              </View>
            </View>
            {this.state.postView === 1 &&
            <View>
              <FlatList
                data={DATA}
                renderItem={this.renderItem}
                keyExtractor={item => item.id.toString()}
              />
            </View>
            }
            {this.state.postView === 2 &&
            <View>
              <FlatList
                data={DATA}
                renderItem={this._renderItem}
                keyExtractor={item => item.id.toString()}
              />
            </View>
            }
          </View>
        </ScrollView>
        <View style={styles.bottomWarpper}></View>
      </View>
    );
  }
}
export default connect(
  (state) => ({
    allShops: selectAllShops(state),
    errorMsg: selectErrorMsg(state),
    allShopCount: selectAllShopsCount(state),
  }),
  {
    getAllShops,
  },
)(ShopList);


