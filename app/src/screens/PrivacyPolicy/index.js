import * as React from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import styles from './styles';

import Header from "../../components/Header";
import Top from "../../components/Top";
import ShopSmallView from "../../components/ShopSmallView";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, SECONDARY_ORANGE, PRIMARY_GRAY, LIGHT_GRAY} from "../../utils/constants";

const arrowLeft = require('./images/arrowLeft.png');


class PrivacyPolicy extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.headerWrapper}>
            <Header componentId={this.props.componentId} />
          </View>
          <View style={styles.bodyWrapper}>
            <View style={{borderBottomWidth: 1, paddingBottom: 15}}>
              <Text style={{fontSize: 30, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK,}}>privvacy policy<Text style={{fontSize: 40, color: SECONDARY_ORANGE}}>.</Text></Text>
            </View>
            <Text style={{fontSize: 20, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK, marginVertical: 20}}>Socialogue Privacy Policy</Text>

            <View>
              <Text style={{
                fontSize: 18,
                fontFamily: GROTESK_SEMIBOLD,
                color: PRIMARY_BLACK,
                marginVertical: 20,
                textTransform: 'uppercase'
              }}>
                Information We Collect & How We Use It
              </Text>
              <Text style={{color: PRIMARY_GRAY, marginBottom: 20}}>Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
                standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                it to make a type specimen book.</Text>
              <Text style={{color: PRIMARY_GRAY, marginBottom: 20}}>It has survived not only five centuries, but also the leap into electronic
                typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker
                including versions of Lorem Ipsum.</Text>
              <Text style={{color: PRIMARY_GRAY,fontSize: 15,fontFamily: GROTESK_SEMIBOLD, marginBottom: 20}}>We use this information to: </Text>
              <Text style={{color: PRIMARY_GRAY, marginBottom: 20}}>provide, test, improve, promote and personalize
                the Services fight spam and other forms of abuse generate aggregate, non-identifying information about how
                people use the Services.</Text>
              <Text style={{color: PRIMARY_GRAY, marginBottom: 20}}>When you create your Medium account, and authenticate with a third-party service
                (like Twitter, Facebook, Apple or Google) we may collect, store, and periodically update information
                associated with that third-party account, such as your lists of friends or followers. We will
                never publish something through one of your third-party accounts without your express permission.</Text>
            </View>
            <View>
              <Text style={{
                fontSize: 18,
                fontFamily: GROTESK_SEMIBOLD,
                color: PRIMARY_BLACK,
                marginVertical: 20,
                textTransform: 'uppercase'
              }}>
                Information Disclosure
              </Text>
              <Text style={{color: PRIMARY_GRAY, marginBottom: 20}}>Medium won’t transfer information about you to third parties for
                the purpose of providing or facilitating third-party advertising to you. We won’t sell
                information about you to a third-party.</Text>
              <Text style={{color: PRIMARY_GRAY, marginBottom: 20}}> We may transfer your account information with third parties in some circumstances, including: (1) with your consent;
                (2) to a service provider or partner who meets our data protection standards; (3) with academic or non-profit researchers, with aggregation, anonymization,
                or pseudonymization; (4) when we have a good faith belief it is required by law, such as pursuant to a subpoena or other legal process; (5) when we have a
                good faith belief that doing so will help prevent imminent harm to someone.</Text>
              <Text style={{color: PRIMARY_GRAY, marginBottom: 20}}>If we are going to share your information in response to legal process, we’ll
                give you notice so you can challenge it (for example by seeking court intervention),
                unless we’re prohibited by law or believe doing so may endanger others or cause illegal conduct.
                We will object to legal requests for information about users of our services that we believe are improper.</Text>
            </View>
            <View>
              <Text style={{
                fontSize: 18,
                fontFamily: GROTESK_SEMIBOLD,
                color: PRIMARY_BLACK,
                marginVertical: 20,
                textTransform: 'uppercase'
              }}>
                Public Data
              </Text>
              <Text style={{color: PRIMARY_GRAY, marginBottom: 20}}>Search engines may index your Medium user profile page, public
                interactions (such as claps or highlights), and post pages, such that people may find these pages when
                searching against your name on services like Google, DuckDuckGo, or Bing. Users may also share links
                to your content on social media platforms such as Facebook or Twitter.</Text>
            </View>
            <View>
              <Text style={{
                fontSize: 18,
                fontFamily: GROTESK_SEMIBOLD,
                color: PRIMARY_BLACK,
                marginVertical: 20,
                textTransform: 'uppercase'
              }}>
                Contact Us
              </Text>
              <Text style={{color: PRIMARY_GRAY, marginBottom: 20}}>You may contact us by emailing us at <Text>contact@socialogue.com</Text></Text>
            </View>

            <View style={{flexDirection: 'row', alignItems: 'center', marginVertical: 30}}>
              <Image source={arrowLeft} style={{width: 25, height: 12, marginRight: 5}} />
              <Text
                style={{fontFamily: GROTESK_SEMIBOLD,
                  color: PRIMARY_BLACK,
                }}
              >
                Back to main
              </Text>
            </View>

          </View>
        </ScrollView>
      </View>
    );
  }
}
export default PrivacyPolicy;

