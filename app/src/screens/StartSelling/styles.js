import {StyleSheet, Text} from 'react-native';
import {GROTESK_SEMIBOLD, LIGHT_GRAY, PRIMARY_BLACK, PRIMARY_GRAY, SECONDARY_ORANGE} from "../../utils/constants";
import * as React from "react";

export default StyleSheet.create({
  container: {
    flex: 10
  },
  headerWrapper: {
    height: 60
  },
  bodyWrapper: {
    padding: 20
  },
  button: {
    width: 140,
    height: 35,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  },
  bottomWarpper: {
    borderTopWidth: 1
  },
});
