import * as React from 'react';
import {Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Header from "../../components/Header";import styles from './styles';

import Top from "../../components/Top";
import ShopSmallView from "../../components/ShopSmallView";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY, SECONDARY_ORANGE} from "../../utils/constants";
import {navigateToApplyShop, navigateToCart} from "../../navigator";

const photo = require('./images/photo.png');
const photo2 = require('./images/photo2.png');


class StartSelling extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.headerWrapper}>
            <Header componentId={this.props.componentId} />
          </View>
          <View style={styles.bodyWrapper}>
            <View style={{borderBottomWidth: 1, paddingBottom: 10}}>
              <Text style={{fontSize: 28, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK,}}>start selling<Text style={{fontSize: 40, color: SECONDARY_ORANGE}}>.</Text></Text>
            </View>
            <View style={{paddingVertical: 20}}>
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK, textTransform: 'uppercase'}}>selling information</Text>
              <Image source={photo} style={{width: '100%', height: 120, marginTop: 20}} />
            </View>
            <View>
              <Text style={{
                fontSize: 25,
                fontFamily: GROTESK_SEMIBOLD,
                color: PRIMARY_BLACK,
                marginVertical: 20,
                textTransform: 'lowercase'
              }}>
                sell on socialogue
              </Text>
              <Text style={{color: PRIMARY_GRAY, marginBottom: 20}}>Simply dummy text of the printing and typesetting industry.
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took
                a galley of type and scrambled it to make a type specimen book.</Text>
              <Text style={{color: PRIMARY_GRAY, marginBottom: 20}}>It has survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the
                release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                software like Aldus PageMaker including versions of Lorem Ipsum.</Text>
            </View>
            <View>
              <View style={{flexDirection: 'row', paddingVertical: 20, height: 130}}>
                <View style={{width: '50%'}} >
                  <Text style={{
                    fontSize: 25,
                    fontFamily: GROTESK_SEMIBOLD,
                    color: PRIMARY_BLACK,
                    textTransform: 'lowercase',
                    paddingTop: '39%',
                    height: '100%',
                  }}>
                    benefits
                  </Text>
                </View>
                <View style={{width: '50%'}} >
                  <Image source={photo2} style={{width: '100%', height: '100%'}} />
                </View>
              </View>
              <Text style={{color: PRIMARY_GRAY, marginBottom: 20}}>Simply dummy text of the printing and typesetting industry.
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took
                a galley of type and scrambled it to make a type specimen book.</Text>
            </View>
            <View>
              <View style={{ paddingVertical: 20}}>
                <View style={{width: '50%'}} >
                  <Text style={{
                    fontSize: 25,
                    fontFamily: GROTESK_SEMIBOLD,
                    color: PRIMARY_BLACK,
                    textTransform: 'lowercase'
                  }}>
                    principles
                  </Text>
                </View>
              </View>
              <Text style={{color: PRIMARY_GRAY, marginBottom: 20}}>It has survived not only five centuries, but also the
                leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including
                versions of Lorem Ipsum.</Text>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'flex-end', marginVertical: 30}}>
              <TouchableOpacity style={styles.button}
                                onPress={() => navigateToApplyShop(this.props.componentId)}

              >
                <Text style={styles.buttonText}>Apply for shop</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        <View style={styles.bottomWarpper}>
        </View>
      </View>
    );
  }
}
export default StartSelling;


