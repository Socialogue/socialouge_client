import {StyleSheet, Text} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY} from "../../utils/constants";
import * as React from "react";

export default StyleSheet.create({
  container: {
    flex: 100
  },
  headerWarpper: {
    height: 60
  },
  moreWarpper: {
    width: '55%',
    position: 'absolute',
    backgroundColor: '#fff',
    right: 0,
    top: 59
  },
  viewProfile: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderWidth: 1
  },
  profile: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
  },
  profileIcon: {
    width: 20,
    height: 20,
    marginRight: 10
  },
  cross: {
    width: 25,
    height: 25,
  },
  crossIcon: {
    paddingTop: 5,
    paddingRight: 5,
    paddingLeft: 15,
    paddingBottom: 15,
  },
  delete: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    borderLeftWidth: 1,
    borderRightWidth: 1
  },
  deleteIcon: {
    width: 20,
    height: 20,
    marginRight: 10
  },
  blockProfile: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    borderWidth: 1
  },
  blockProfileIcon: {
    width: 25,
    height: 20,
    marginRight: 10
  },
  container: {
    flex: 100,
  },
  idWrapper: {
    height: 60,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    padding: 15,
    position: 'relative'
  },
  idWrapperContainer: {
    flex: 100,
    flexDirection: 'row',
    alignItems: 'center'
  },
  idTitle: {
    fontSize: 14,
    color: '#000'
  },
  chatImg: {
    flex: 25,
    flexDirection: 'row',
    alignItems: 'center'
  },
  leftArrow: {
    width: 25,
    height: 10,
    marginRight: 5
  },
  id: {
    flex: 78,
    marginLeft: 15
  },
  activeStatus: {
    fontSize: 12,
    color: '#868686'
  },
  moreOption: {
    flex: 7,
    paddingTop: 20,
    paddingLeft: 20,
    paddingBottom: 20,
    paddingRight: 10
    // backgroundColor: 'green'
  },
  more: {
    width: 25,
    height: 10
  },
  conversationWrapper: {
    flex: 80,
    padding: 15,
  },
  conversationImg: {
    width: 30,
    height: 30,
    marginRight: 10
  },
  fromText: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
    marginTop: 10
  },
  textTime: {
    fontSize: 12,
    color: '#868686',
    marginBottom: 5
  },
  text: {
    color: '#000',
    fontSize: 12
  },
  textWrapper: {
    backgroundColor: '#EFEFEF',
    padding: 10,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderTopRightRadius: 20,
    marginRight: '30%'
  },
  toTextWrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginBottom: 10,
    marginTop: 10,
    backgroundColor: '#000',
    padding: 10,
    marginLeft: '30%',
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderTopLeftRadius: 20
  },
  toText: {
    color: '#fff',
    fontSize: 12
  },
  sendWrapper: {
    borderWidth: 1,
    paddingLeft: 15,
    paddingRight: 15,
    height: 60
  },
  sendContainer: {
    flex: 100,
    flexDirection: 'row',
    alignItems: 'center'
  },
  emojiView: {
    flex: 10
  },
  emoji: {
    width: 20,
    height: 20,
  },
  addFileView: {
    flex: 10
  },
  addFile: {
    width: 20,
    height: 20,
  },
  addPhotoView: {
    flex: 10
  },
  addPhoto: {
    width: 20,
    height: 20,
  },
  typeInput: {
    flex: 70,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rightArrow: {
    width: 20,
    height: 20
  },
  send: {
    flexDirection: 'row'
  },
  sendText: {
    color: '#000'
  }
});
