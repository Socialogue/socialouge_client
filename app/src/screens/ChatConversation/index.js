import * as React from 'react';
import {View, Text, Image, StyleSheet, TextInput, ScrollView, TouchableOpacity} from 'react-native';
import {Navigation} from "react-native-navigation";
import Header from "../../components/Header";
import {navigateToChatList, navigateToHome} from "../../navigator";
import styles from './styles';

const more = require('../../images/more.png');
const arrowLeft = require('../../images/arrowLeft.png');
const rightArrow = require('../../images/rightArrow.png');
const Ellipse45 = require('../../images/Ellipse45.png');
const emoji = require('../../images/emoji.png');
const addFile = require('../../images/addFile.png');
const addPhoto = require('../../images/addPhoto.png');
const profileIcon = require('../../images/profileIcon.png');
const deleteIcon = require('../../images/delete.png');
const blockProfile = require('../../images/blockProfile.png');
const cross = require('../../images/cross.png');

class ChatConversation extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isVisible: false
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerWarpper}>
          <Header componentId={this.props.componentId} />
        </View>
        <View style={styles.idWrapper}>
          <View style={styles.idWrapperContainer}>
            <View style={styles.chatImg}>

              <TouchableOpacity
                onPress={() => navigateToChatList(this.props.componentId)}
                >
                {/*<Header componentId={this.props.componentId}></Header>*/}

                <Image source={arrowLeft} style={styles.leftArrow} />

              </TouchableOpacity>
              {/*<Image source={leftArrow} style={styles.leftArrow} />*/}
              <Image source={Ellipse45} style={styles.img} />
            </View>
            <View style={styles.id}>
              <Text style={styles.idTitle}>NICO RUBIO</Text>
              <Text style={styles.activeStatus}>Active 3 min ago</Text>
            </View>
            <TouchableOpacity
              style={styles.moreOption}
              onPress={() => this.setState({isVisible: true})}
            >
              <Image source={more} style={styles.more}/>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.conversationWrapper}>
          <ScrollView style={styles.conversationContainer}>
            <View style={styles.fromText}>
              <Image source={Ellipse45} style={styles.conversationImg} />
              <View style={styles.textContainer}>
                <Text style={styles.textTime}>23 min ago</Text>
                <View style={styles.textWrapper}>
                  <Text style={styles.text}>Hello I'am sending you a free sample of my new jumper</Text>
                </View>
              </View>
            </View>
            <View style={styles.toText}>
              <View style={styles.toTextWrapper}>
                <Text style={styles.toText}>Hello I'am sending you a free sample of</Text>
              </View>
            </View>
          </ScrollView>
        </View>
        <View style={styles.sendWrapper}>
          <View style={styles.sendContainer}>
            <View style={styles.emojiView}>
              <Image source={emoji} style={styles.emoji} />
            </View>
            <View style={styles.addFileView}>
              <Image source={addFile} style={styles.addFile} />
            </View>
            <View style={styles.addPhotoView}>
              <Image source={addPhoto} style={styles.addPhoto} />
            </View>
            <View style={styles.typeInput}>
              {/*<View type={styles.typeContainer}>*/}
                <TextInput
                  style={{width: '75%'}}
                  placeholder="Type a message..."
                  multiline
                />
                <View style={styles.send}>
                  <Text style={styles.sendText}>Send</Text>
                  <Image source={rightArrow} style={styles.rightArrow} />
                </View>
              {/*</View>*/}
            </View>
          </View>
        </View>
        {this.state.isVisible &&
          <View style={styles.moreWarpper}>
            <View style={styles.viewProfile}>
              <View style={styles.profile}>
                <Image source={profileIcon} style={styles.profileIcon} />
                <Text style={styles.profileText}>View profile</Text>
              </View>
              <TouchableOpacity
                style={styles.crossIcon}
                onPress={() => this.setState({isVisible: false})}>
                <Image source={cross} style={styles.cross} />
              </TouchableOpacity>
            </View>
            <View style={styles.delete}>
              <Image source={deleteIcon} style={styles.deleteIcon} />
              <Text style={styles.profileText}>Delete conversation</Text>
            </View>
            <View style={styles.blockProfile}>
              <Image source={blockProfile} style={styles.blockProfileIcon} />
              <Text style={styles.profileText}>Block profile</Text>
            </View>
          </View>
        }
      </View>
    );
  }
}

export default ChatConversation;

