import * as React from 'react';
import {View, TouchableOpacity, Text, Image, StyleSheet, TextInput, ScrollView} from 'react-native';
import Header from "../../components/Header";
import {Navigation} from "react-native-navigation";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../utils/constants";
import PrimaryButton from "../../components/PrimaryButton";
import {navigateToHome, navigateToThankYou} from "../../navigator";

const ThankYouIcon = require('./images/ThankYouIcon.png');


class ThankYou extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  onPressButton = () => {
    this.setState({loading: true});
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerWarpper}>
          <View style={{height: 60}}>
            <Header componentId={this.props.componentId} />
          </View>
        </View>
        <View style={styles.bodyWarpper}>
          <View style={{
            alignItems: 'center',
            justifyContent: 'center',
            flex: 1
          }}>
            <Image source={ThankYouIcon} style={{width: 60, height: 60}} />
            <Text style={{
              fontSize: 24,
              marginVertical: 20,
              fontFamily: GROTESK_SEMIBOLD,
              color: PRIMARY_BLACK,
            }}>
              Thank you for your purchase!</Text>
            <Text style={{
              fontSize: 16,
              fontFamily: GROTESK_SEMIBOLD,
              color: PRIMARY_BLACK,
              textTransform: 'uppercase',
            }}>
              Order number: 546425</Text>
          </View>

        </View>
        <View style={styles.footerWrapper}>
          <View>
            <Text style={{
              fontSize: 15,
              fontFamily: GROTESK_SEMIBOLD,
              color: PRIMARY_BLACK,
              marginVertical: 30,
              marginHorizontal: '10%'
            }}>
              We’ll email you an order confirmation with order details.</Text>
            <PrimaryButton
              loading={this.props.loading}
              title='backtosocialogue'
              // onPress={this.props.onPressButton}/>
              onPress={() => navigateToHome(this.props.componentId)}/>
          </View>
        </View>
      </View>
    );
  }
}

export default ThankYou;

const styles = StyleSheet.create({
  container: {
    flex: 10
  },
  headerWarpper: {
    flex: 1
  },
  bodyWarpper: {
    flex: 6,
    padding: 20
  },
  footerWrapper: {
    flex: 3,
    padding: 20,
  }

});
