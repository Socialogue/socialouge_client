import {StyleSheet, Text} from 'react-native';
import {GROTESK_SEMIBOLD, LIGHT_GRAY, PRIMARY_BLACK, PRIMARY_GRAY} from "../../utils/constants";
import * as React from "react";

export default StyleSheet.create({
  container: {
    flex: 10,
    // backgroundColor: 'red'
  },
  headerWrapper: {
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    flex: 1,
    // backgroundColor: 'red'
  },
  bodyWrapper: {
    padding: 20,
    flex: 5.2,
    // backgroundColor: 'green'
  },
  footerWrapper: {
    padding: 20,
    flex: 3,
    // backgroundColor: 'blue'
  },

  lebelText: {
    fontSize: 14,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    textTransform: 'uppercase',
  },

  allOrder: {
    borderWidth: 1,
    padding: 10,
    borderColor: LIGHT_GRAY,
    marginVertical: 30
  },
  orderItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10
  },
  order: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  orderText: {
    fontSize: 12,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    textTransform: 'uppercase',
  },



  subText: {
    fontSize: 13,
    color: PRIMARY_BLACK,
    fontFamily: GROTESK_SEMIBOLD,
  },
});
