import * as React from 'react';
import {Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {GROTESK_SEMIBOLD, LIGHT_GRAY, PRIMARY_BLACK, PRIMARY_GRAY, SECONDARY_ORANGE} from "../../utils/constants";
import PrimaryButton from "../../components/PrimaryButton";
import {navigateToOrderingOverView, navigateToThankYou} from "../../navigator";
import styles from './styles';

import {Navigation} from "react-native-navigation";


const cross = require('./images/cross.png');
const arrowLeft = require('./images/arrowLeft.png');
const Group106 = require('./images/Group106.png');
const coat = require('./images/coat.png');


class OrderingOverView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  onPressButton = () => {
    this.setState({loading: true});
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerWrapper}>
          <View style={{flexDirection: 'row', alignItems: 'center', height: 60}}>
            <TouchableOpacity
              style={{flexDirection: 'row', alignItems: 'center', height: 60}}
              onPress={() => Navigation.pop(this.props.componentId)}>
              {/*<Header componentId={this.props.componentId}></Header>*/}

              <Image source={arrowLeft} style={{width: 25, height: 10, marginRight: 5}} />
              <Text style={styles.lebelText}>Delivery & PAyment</Text>

            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.bodyWrapper}>
          <Text style={{
            fontSize: 14,
            marginTop: 20,
            fontFamily: GROTESK_SEMIBOLD,
            color: PRIMARY_BLACK,
            textTransform: 'uppercase',}}>Ordering overview</Text>

          <View style={styles.allOrder}>
            <ScrollView>
              <View style={styles.orderItem}>
                <View style={styles.order}>
                  <Image source={cross} style={{width: 22, height: 22}}/>
                  <Image source={coat} style={{width: 34, height: 50, marginHorizontal: 10}}/>
                  <Text style={styles.orderText}>1 X </Text>
                  <Text style={styles.orderText}>limited edition coat</Text>
                </View>
                <View>
                  <Text style={styles.lebelText}>€ 240.00</Text>
                </View>
              </View>
              <View style={styles.orderItem}>
                <View style={styles.order}>
                  <Image source={cross} style={{width: 22, height: 22}}/>
                  <Image source={coat} style={{width: 34, height: 50, marginHorizontal: 10}}/>
                  <Text style={styles.orderText}>1 X </Text>
                  <Text style={styles.orderText}>WIDE SLEEVE JEANS</Text>
                </View>
                <View>
                  <Text style={styles.lebelText}>€ 240.00</Text>
                </View>
              </View>
              <View style={styles.orderItem}>
                <View style={styles.order}>
                  <Image source={cross} style={{width: 22, height: 22}}/>
                  <Image source={coat} style={{width: 34, height: 50, marginHorizontal: 10}}/>
                  <Text style={styles.orderText}>1 X </Text>
                  <Text style={styles.orderText}>WIDE SLEEVE JEANS</Text>
                </View>
                <View>
                  <Text style={styles.lebelText}>€ 240.00</Text>
                </View>
              </View>
              <View style={styles.orderItem}>
                <View style={styles.order}>
                  <Image source={cross} style={{width: 22, height: 22}}/>
                  <Image source={coat} style={{width: 34, height: 50, marginHorizontal: 10}}/>
                  <Text style={styles.orderText}>1 X </Text>
                  <Text style={styles.orderText}>WIDE SLEEVE JEANS</Text>
                </View>
                <View>
                  <Text style={styles.lebelText}>€ 240.00</Text>
                </View>
              </View>
              <View style={styles.orderItem}>
                <View style={styles.order}>
                  <Image source={cross} style={{width: 22, height: 22}}/>
                  <Image source={coat} style={{width: 34, height: 50, marginHorizontal: 10}}/>
                  <Text style={styles.orderText}>1 X </Text>
                  <Text style={styles.orderText}>WIDE SLEEVE JEANS</Text>
                </View>
                <View>
                  <Text style={styles.lebelText}>€ 240.00</Text>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
        <View style={styles.footerWrapper}>
        <View style={{
          borderWidth: 1,
          padding: 10,
          borderColor: LIGHT_GRAY
        }}>
          <View style={styles.orderPrice}>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 10}}>
              <Text style={styles.orderText}>Products price:</Text>
              <Text style={styles.lebelText}>€ 240.00</Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 10}}>
              <Text style={styles.orderText}>VAT:</Text>
              <Text style={styles.lebelText}>€ 40.00</Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 10}}>
              <Text style={styles.orderText}>Delivery:</Text>
              <Text style={styles.lebelText}>€ 50.00</Text>
            </View>
          </View>
        </View>
          <PrimaryButton
            loading={this.props.loading}
            title='submit'
            // onPress={this.props.onPressButton}/>
            onPress={() => navigateToThankYou(this.props.componentId)}/>
      </View>
      </View>
    );
  }
}
export default OrderingOverView;

