import {StyleSheet, Text} from 'react-native';
import {GROTESK_SEMIBOLD, LIGHT_GRAY, PRIMARY_BLACK, PRIMARY_GRAY, SECONDARY_ORANGE} from "../../utils/constants";
import * as React from "react";

export default StyleSheet.create({
  container: {
    padding: 20,
    flex: 100
  },
  headerWarpper: {
    flex: 20
  },
  bodyWarpper: {
    flex: 80,
    marginHorizontal: '8%'
  },
  headTop: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  backTo: {
    fontSize: 14,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  },
  ressetText: {
    fontSize: 17,
    marginBottom: 30,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  },
  title: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  },
  passInput: {
    paddingHorizontal: 20,
    borderLeftWidth: 1,
    borderColor: SECONDARY_ORANGE,
    marginBottom: 30
  },
  newPass: {
    borderBottomWidth: 1,
    marginBottom: 15
  },
  rePass: {
    borderBottomWidth: 1,
  },
  bottom: {
    alignItems: 'center',
    marginTop: 20,
  }
});
