import * as React from 'react';
import {Image, StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';
import {Navigation} from "react-native-navigation";
import styles from './styles';

import {GROTESK_SEMIBOLD, PRIMARY_BLACK, SECONDARY_ORANGE} from '../../utils/constants';
import PrimaryButton from "../../components/PrimaryButton";
import {navigateToLogin} from "../../navigator";

const leftArrow = require('.././../images/leftArrow.png');
const Socialoguelogo = require('.././../images/Socialoguelogo.png');



class SubmitEmail extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }
  onPressButton = () => {
    this.setState({loading: true});
}
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerWarpper}>
          <View style={styles.headTop}>
            <Image source={leftArrow} style={{width: 6, height: 10, marginRight: 10}} />
            <Text
              style={styles.backTo}
              onPress={() => navigateToLogin(this.props.componentId)}
            >Back to main
            </Text>
          </View>
        </View>
        <View style={styles.bodyWarpper}>
          <Image source={Socialoguelogo} style={{width: 100, height: 25, marginRight: 10}} />
          <Text style={styles.ressetText}>check your email/phone number<Text style={{fontSize: 35, color: SECONDARY_ORANGE}}>.</Text></Text>
          <View style={styles.passInput}>
            <Text style={styles.title}>EMAIL OR PHONE NUMBER</Text>
            <TextInput
              secureTextEntry={true}
              style={styles.newPass}
              placeholder="Email or Phone number"
            />
          </View>
          <View style={styles.resstWrapper}>
            <PrimaryButton
              loading={this.state.loading}
              title='submit'
              onPress={this.onPressButton}/>
          </View>
          <View style={styles.bottom}>
            <TouchableOpacity
              onPress={() => navigateToLogin(this.props.componentId)}
            >

              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK,}}>Back to Login</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default SubmitEmail;

