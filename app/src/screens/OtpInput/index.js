import * as React from 'react';
import {Text, View, Image, TouchableOpacity, ScrollView, StyleSheet, TextInput} from 'react-native';
import {Navigation} from "react-native-navigation";
import styles from './styles';

import OTPInputView from '@twotalltotems/react-native-otp-input';
import {COLOR_ORANGE, GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../utils/constants";
import { verifyOtp, resendOtp } from "core/actions/authActionCreators";
import { connect } from "react-redux";
import {
  selectOtpErrorMsg,
  selectOtpSendMsg,
  selectToken,
  selectUserToVerifyOtp
} from "core/selectors/user";
import PrimaryButton from "../../components/PrimaryButton";
import {navigateToHome, navigateToLogin} from "../../navigator";

const leftArrow = require('.././../images/leftArrow.png');
const Socialoguelogo = require('.././../images/Socialoguelogo.png');

class SubmitEmail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      otp: "",
      countdown: 30,
      resend: false,
    };
  }



  componentDidMount() {
    this.countdown();
  }

  countdown() {
    let now = 30;
    this.counter = setInterval(() => {
      const rem = 1;
      const t = now - rem;
      now = t;
      this.setState({ countdown: t });
      if (t === 0) {
        clearInterval(this.counter);
        this.setState({ resend: true });
      }
    }, 1000);
  }

  componentWillUnmount() {
    if (this.counter) clearInterval(this.counter);
  }

  componentDidUpdate = prevProps => {

    if (prevProps.otpSendMsg !== this.props.otpSendMsg) {
      if (this.props.otpSendMsg !== "") {
        alert(this.props.otpSendMsg);
        this.setState({loading:false});
      }
    }

    if (prevProps.loginSuccess !== this.props.loginSuccess) {
      if (this.props.loginSuccess) {
        navigateToHome(this.props.componentId);
      }
    }

    if (prevProps.otpErrorMsg !== this.props.otpErrorMsg) {
      if (this.props.otpErrorMsg !== "") {
        alert(this.props.otpErrorMsg);
        this.setState({loading:false,otp: ''});
      }
    }
  };

  handleOtpChange = code => {
    this.setState({
      otp: code
    });
  };

  onPressButton = async e => {
    if (this.state.otp.length < 5) {
      alert("Please Enter OTP Correctly");
      return;
    }

    this.setState({ loading: true });

    this.props.verifyOtp(
      this.state.otp,
      this.props.user.username,
      this.props.user.accountType
    );
    return true;
  };


  handleResendOtp = () => {
    this.props.resendOtp(this.props.user.username, this.props.user.accountType);
    this.setState({
      countdown: 30,
      resend: false
    });
    this.countdown();
  };


  render() {
    console.log(this.props);
    return (
      <View style={styles.container}>
        <View style={styles.headerWarpper}>
          <View style={styles.headTop}>
            <Image source={leftArrow} style={{width: 6, height: 10, marginRight: 10}} />
            <Text
              style={{fontSize: 14, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK,}}
              onPress={() => navigateToLogin(this.props.componentId)}
            >Back to main
            </Text>
          </View>
        </View>
        <View style={styles.bodyWarpper}>
          <Image source={Socialoguelogo} style={{width: 88, height: 22}} />
          <Text style={styles.ressetText}>verification<Text style={{fontSize: 40, color: '#FF6A00'}}>.</Text></Text>
          <Text style={{fontSize: 12, marginBottom: 30}}>Enter the verification code we send you on your email or mobile phone</Text>
          <View style={styles.passInput}>
            <OTPInputView
              style={{height: 50}}
              pinCount={5}
              code={this.state.otp}
              // onCodeChanged = {this.handleOtpChange}
              autoFocusOnLoad
              codeInputFieldStyle={styles.underlineStyleBase}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
              // onCodeFilled={this.handleOtpChange}
              onCodeChanged={this.handleOtpChange}
            />
          </View>
          {!this.state.resend ?
            <View style={{marginBottom: 20, color: '#000'}}>
              <Text style={{color: '#000', fontSize: 13}}>Didin't get sms? Resend in {this.state.countdown}</Text>
            </View> :
            <View style={{marginBottom: 20, color: '#000'}}>
              <Text style={{color: '#000', fontSize: 13}} onPress={this.handleResendOtp}>Resend code</Text>
            </View>
          }


          <View style={styles.resstWrapper}>
            {/*<TouchableOpacity style={styles.ressetButton}>*/}
            {/*  <Text style={styles.ressetButtonText}>SUBMIT</Text>*/}
            {/*</TouchableOpacity>*/}
            <PrimaryButton
              loading={this.state.loading}
              title='SUBMIT'
              onPress={this.onPressButton}/>
          </View>
          <View style={styles.bottom}>
            <Text
              style={{color: '#000'}}
              onPress={() => navigateToLogin(this.props.componentId)}
            >Back
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    user: selectUserToVerifyOtp(state),
    isLoggedIn: !!selectToken(state),
    auth: state.auth,
    otpSendMsg: selectOtpSendMsg(state),
    otpErrorMsg: selectOtpErrorMsg(state),
    loginSuccess: state.auth.loginSuccess,
  }),
  {
    verifyOtp,
    resendOtp
  }
)(SubmitEmail);


