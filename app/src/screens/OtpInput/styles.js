import {StyleSheet, Text} from 'react-native';
import {GROTESK_SEMIBOLD, LIGHT_GRAY, PRIMARY_BLACK, PRIMARY_GRAY, SECONDARY_ORANGE} from "../../utils/constants";
import * as React from "react";

export default StyleSheet.create({
  container: {
    padding: 20,
    flex: 100
  },
  headerWarpper: {
    flex: 20
  },
  bodyWarpper: {
    flex: 80,
    marginHorizontal: '10%'
  },
  headTop: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  ressetText: {
    fontSize: 35,
    color: '#000',
  },
  passInput: {
    paddingHorizontal: 20,
    marginBottom: 30
  },
  title: {
    color: '#000'
  },
  rePass: {
    borderBottomWidth: 1,
  },
  ressetButton: {
    backgroundColor: '#000',
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ressetButtonText: {
    color: '#fff',
  },
  bottom: {
    alignItems: 'center',
    marginTop: 20,
  },


  borderStyleBase: {
    width: 25,
    height: 45
  },

  borderStyleHighLighted: {
    borderColor: "#FF6A00",
  },

  underlineStyleBase: {
    width: 25,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
    borderColor: PRIMARY_BLACK,
    color: PRIMARY_BLACK
  },

  underlineStyleHighLighted: {
    borderColor: "#FF6A00",
  },
});
