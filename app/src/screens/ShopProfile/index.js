import * as React from 'react';
import {Text, View, Image, TouchableOpacity, ScrollView} from 'react-native';

import styles from './styles';
import Top from './component/Top';
import ProfileTab from './component/ProfileTab';
import {Navigation} from "react-native-navigation";
import {navigateToAppMenu, navigateToCart, navigateToChatList, navigateToUserProfile} from "../../navigator";

const arrowLeft = require('./images/arrowLeft.png');
const cart = require('./images/cart.png');
const notificationicon = require('./images/notificationicon.png');
const menubar = require('../../images/menubar.png');

class ShopProfile extends React.Component {

constructor(props) {
  super(props);
}

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerWarpper}>
          <View>
            <TouchableOpacity
              style={{paddingRight: 30, paddingVertical: 10}}
              onPress={() => Navigation.pop(this.props.componentId)}>
              {/*<Header componentId={this.props.componentId}></Header>*/}
              <Image source={arrowLeft} style={{width: 25, height: 15}} />
            </TouchableOpacity>
          </View>
          <View style={styles.iconPart}>

            <TouchableOpacity
              style={{paddingRight: 10, paddingVertical: 10, paddingLeft: 10}}>
              {/*<Header componentId={this.props.componentId}></Header>*/}
              <Image source={notificationicon} style={{width: 22, height: 22}} />
            </TouchableOpacity>
            <TouchableOpacity
              style={{paddingRight: 20, paddingVertical: 10}}
              onPress={() => navigateToAppMenu(this.props.componentId)}
            >
              {/*<Header componentId={this.props.componentId}></Header>*/}
              <Image source={menubar} style={{width: 25, height: 25}} />
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.bodyWarpper}>
          <ScrollView>
            <Top componentId={this.props.componentId}/>
            <ProfileTab />
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default ShopProfile;


