import * as React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";

import ProductsTab from '../component/ProductsTab';
import PostTab from '../component/PostTab';
import AboutTab from '../component/AboutTab';
import TermsTab from '../component/TermsTab';
import ReviewTab from '../component/ReviewTab';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../../utils/constants";

class ProfileTab extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      activetab: 'products'
    };
  }

  render() {

    const {activetab} = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.tabButton}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.setState({activetab: 'products'})}>
            <Text
              style={
                activetab === 'products'
                  ? styles.activeButtonText
                  : styles.inActiveButtonText
              }>
              products
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.setState({activetab: 'post'})}>
            <Text
              style={
                activetab === 'post'
                  ? styles.activeButtonText
                  : styles.inActiveButtonText
              }>
              post
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.setState({activetab: 'about'})}>
            <Text
              style={
                activetab === 'about'
                  ? styles.activeButtonText
                  : styles.inActiveButtonText
              }>
              about
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.lastButton}
            onPress={() => this.setState({activetab: 'review'})}>
            <Text
              style={
                activetab === 'review'
                  ? styles.activeButtonText
                  : styles.inActiveButtonText
              }>
              REVIEWS
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.lastButton}
            onPress={() => this.setState({activetab: 'terms'})}>
            <Text
              style={
                activetab === 'terms'
                  ? styles.activeButtonText
                  : styles.inActiveButtonText
              }>
              terms
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.tabContent}>
          {activetab === 'products' && <ProductsTab />}
          {activetab === 'post' && <PostTab />}
          {activetab === 'about' && <AboutTab />}
          {activetab === 'review' && <ReviewTab />}
          {activetab === 'terms' && <TermsTab />}
        </View>
      </View>
    );
  }
}

export default ProfileTab;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,

  },
  tabButton: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderBottomWidth: 1,
    marginBottom: 20,
  },

  activeButtonText: {
    textTransform: 'uppercase',
    color: '#000',
    borderBottomWidth: 3,
    paddingBottom: 5,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  inActiveButtonText: {
    textTransform: 'uppercase',
    color: '#868686',
  },
});
