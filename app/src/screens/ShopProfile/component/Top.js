import * as React from 'react';
import { Image, StyleSheet, Text, View, ScrollView, TouchableOpacity } from "react-native";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../../utils/constants";
import {navigateToShopProfileSetting, navigateToUserProfile, navigateToUserProfileSetting} from "../../../navigator";
import {selectIsLoggedIn, selectUser} from "core/selectors/user";
import {getFollowedStatus} from "core/selectors/profile";
import {addToFollow} from "core/actions/publicProfileActionCreator";
import {openChatWindow} from "core/messenger/messageActionCreators";
import {fetchConversations} from "core/messenger/actionCreators/conversationsActionCreators";
import { connect } from "react-redux";
import {errorAlert, successAlert} from "web/src/utils/alerts";
import {ROUTE_LOGIN} from "web/src/routes/constants";

const coverphoto = require('../images/coverphoto.png');
const message = require('../images/massegeicon.png');
const userAvater = require('../images/userAvater.png');

class Top extends React.Component {
  componentDidMount = () => {};
  componentDidUpdate = prevProps => {
    if (prevProps.isFollowed !== this.props.isFollowed) {
      if (this.props.isFollowed) {
        successAlert("Followed");
      }
    }
  };

  follow = userId => {
    console.log("userId  ", userId);
    this.props.fetchConversations(userId);
    // return;

    try {
      this.props.addToFollow(userId, this.props.match.params.id);
    } catch (err) {
      console.log(err);
      errorAlert("Something went wrong.");
    }
  };

  onClickMessage = userId => {
    if (!this.props.isLoggedIn) {
      this.props.history.push(ROUTE_LOGIN);
    } else {
      this.props.openChatWindow(
        this.props.match.params.id,
        this.props.user.name,
        this.props.user.profileImage
      );
    }
  };
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.cover}>
          <Image source={coverphoto} style={styles.coverphoto} />
          <Image source={userAvater} style={styles.userAvater} />
        </View>
        <View style={styles.nameProfile}>
          <View style={styles.name}>
            <Text style={styles.nameTitle}>JORA JOHANSON</Text>
            <Text style={styles.followersCount}>232 FOLLOWERS</Text>
            <TouchableOpacity
              style={{paddingVertical: 10}}
              onPress={() => navigateToShopProfileSetting(this.props.componentId)}>
              <Text style={styles.editProfile}>Edit Shop Profile</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.follow}>
            <TouchableOpacity
              style={styles.followButton}
              >
              <Text style={styles.followText}>Follow</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.messageButton}
            >
              <Text style={styles.messageText}>Message</Text>
              <Image source={message} style={styles.messageIcon} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    loggedInUser: selectUser(state),
    isLoggedIn: selectIsLoggedIn(state),
    isFollowed: getFollowedStatus(state)
  }),
  {
    addToFollow,
    openChatWindow,
    fetchConversations
  }
)(Top);

const styles = StyleSheet.create({
  container: {

  },
  cover: {
    flex: 30
  },
  userAvater: {
    marginLeft: 20,
    height: 80,
    width: 80,
    borderRadius: 37.5,
    position: 'absolute',
    bottom: '-30%',
    backgroundColor: '#fff'
  },
  nameProfile: {
    flexDirection: 'row',
    flex: 100,
    padding: 20,
    marginTop: 40
  },
  nameTitle: {
    fontSize: 30,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  name: {
    flex: 60
  },
  followersCount: {
    marginTop: 10,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  editProfile: {
    color: '#868686'
  },
  follow: {
    flex: 40
  },
  followButton: {
    borderWidth: 1,
    height: 33,
    width: 115,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20
  },
  messageButton: {
    flexDirection: 'row',
    borderWidth: 1,
    height: 33,
    width: 115,
    justifyContent: 'center',
    alignItems: 'center'
  },
  followText: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  messageText: {
    marginRight: 10,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  }

});
