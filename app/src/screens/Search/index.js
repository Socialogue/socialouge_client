import * as React from 'react';
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  offset,
  TouchableOpacity, Image, TextInput,
} from "react-native";
import Header from "../../components/Header";
import styles from './styles';

import BottomTab from "../../components/BottomTab";

const search = require('../../images/search.png');
const searchresultimage = require('../../images/searchresultimage.png');
const Ellipse45 = require('../../images/Ellipse45.png');



class SearchScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activetab: 'clothes',
    };
  }

  render() {
    const {activetab} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.headerWarpper}>
          <Header componentId={this.props.componentId} />
        </View>
        <View style={styles.tabContainer}>
          <View style={styles.tabButton}>
            <TouchableOpacity
              style={styles.clothesButton}
              onPress={() => this.setState({activetab: 'clothes'})}>
              <Text
                style={
                  activetab === 'clothes'
                    ? styles.activeClothesButtonText
                    : styles.inActiveClothesButtonText
                }>
                CLOTHES
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.shopsButton}
              onPress={() => this.setState({activetab: 'shops'})}>
              <Text
                style={
                  activetab === 'shops'
                    ? styles.activeShopsButtonText
                    : styles.inActiveShopsButtonText
                }>
                SHOPS
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.accountsButton}
              onPress={() => this.setState({activetab: 'accounts'})}>
              <Text
                style={
                  activetab === 'accounts'
                    ? styles.activeAccountsButtonText
                    : styles.inActiveAccountsButtonText
                }>
                ACCOUNTS
              </Text>
            </TouchableOpacity>
          </View>

          <View style={styles.tabContent}>
            {activetab === 'clothes' &&
            <ScrollView>
              <View style={styles.searchWarpper}>
                <Image source={search} style={{width: 22, height: 22}} />
                <View style={styles.searchInput}>
                  <TextInput
                    style={styles.searchBar}
                    placeholder="Search..."
                  />
                </View>
              </View>
              <View style={styles.searchResult}>
                <Image source={searchresultimage} style={searchresultimage} />
                <View style={styles.searchDetails}>
                  <View style={styles.DetailsName}>
                    <Text style={styles.name}>Classic</Text>
                    <Text style={styles.searchName}>shart</Text>
                  </View>
                  <View style={styles.productsCategory}>
                    <Text style={styles.category}>Cloths</Text>
                    <Text style={styles.dots}>{'\u2022'}</Text>
                    <Text style={styles.productsCondition}>Fevorite</Text>
                  </View>
                </View>
              </View>
              <View style={styles.searchResult}>
                <Image source={searchresultimage} style={searchresultimage} />
                <View style={styles.searchDetails}>
                  <View style={styles.DetailsName}>
                    <Text style={styles.name}>Classic</Text>
                    <Text style={styles.searchName}>shart</Text>
                  </View>
                  <View style={styles.productsCategory}>
                    <Text style={styles.category}>Cloths</Text>
                    <Text style={styles.dots}>{'\u2022'}</Text>
                    <Text style={styles.productsCondition}>Fevorite</Text>
                  </View>
                </View>
              </View>
            </ScrollView>
            }
            {activetab === 'shops' &&
            <ScrollView>
              <View style={styles.searchWarpper}>
                <Image source={search} style={{width: 22, height: 22}} />
                <View style={styles.searchInput}>
                  <TextInput
                    style={styles.searchBar}
                    placeholder="Search..."
                  />
                </View>
              </View>
            </ScrollView>
            }
            {activetab === 'accounts' &&
            <ScrollView>
              <View style={styles.searchWarpper}>
                <Image source={search} style={{width: 22, height: 22}} />
                <View style={styles.searchInput}>
                  <TextInput
                    style={styles.searchBar}
                    placeholder="Search..."
                  />
                </View>
              </View>
              <View style={styles.searchResult}>
                <Image source={Ellipse45} style={styles.reviewerIdIcon} />
                <View style={styles.accountsDetailsName}>
                  <Text style={styles.name}>LILY-ROSE FLOWERS</Text>
                  <Text style={styles.searchName}>SHIRT</Text>
                </View>
              </View>
              <View style={styles.searchResult}>
                <Image source={Ellipse45} style={styles.reviewerIdIcon} />
                <View style={styles.accountsDetailsName}>
                  <Text style={styles.name}>LILY-ROSE FLOWERS</Text>
                  <Text style={styles.searchName}>SHIRT</Text>
                </View>
              </View>
            </ScrollView>
            }
          </View>
        </View>
        <View style={styles.bottomWarpper}>
          {/*<BottomTab componentId={this.props.componentId}/>*/}
        </View>
      </View>
    );
  }
}

export default SearchScreen;


