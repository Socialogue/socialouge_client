import {StyleSheet, Text} from 'react-native';
import {GROTESK_SEMIBOLD, LIGHT_GRAY, PRIMARY_BLACK, PRIMARY_GRAY, SECONDARY_ORANGE} from "../../utils/constants";
import * as React from "react";

export default StyleSheet.create({
  container: {
    flex: 100,
  },
  headerWarpper: {
    height: 60
  },
  bottomWarpper: {
    borderTopWidth: 1
  },
  tabContainer: {
    flex: 90,
    padding: 20,
  },
  tabButton: {
    flexDirection: 'row',
  },
  clothesButton: {
    marginRight: 20,
  },
  shopsButton: {
    marginRight: 20
  },
  accountsButton: {
    marginRight: 20
  },
  activeClothesButtonText: {
    color: '#000',
    borderBottomWidth: 3,
    paddingBottom: 7
  },
  inActiveClothesButtonText: {
    color: '#868686',
  },
  activeShopsButtonText: {
    color: '#000',
    borderBottomWidth: 3,
    paddingBottom: 7
  },
  inActiveShopsButtonText: {
    color: '#868686',
  },
  activeAccountsButtonText: {
    color: '#000',
    borderBottomWidth: 3,
    paddingBottom: 7
  },
  inActiveAccountsButtonText: {
    color: '#868686',
  },
  searchWarpper: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 10,
    marginBottom: 20,
    marginTop: 10
  },
  searchInput: {
    borderBottomWidth: 1,
    width: '90%',
    marginLeft: 10,
  },
  searchResult: {
    marginTop: 10,
    flexDirection: 'row'
  },
  searchDetails: {
    marginLeft: 15
  },
  DetailsName: {
    flexDirection: 'row'
  },
  searchName: {
    marginLeft: 5,
    color: '#FF6A00'
  },
  productsCategory: {
    flexDirection: 'row',

  },
  category: {
    color: '#8E8E8E'
  },
  dots: {
    color: '#8E8E8E',
    marginLeft: 10,
    marginRight: 10
  },
  productsCondition: {
    color: '#8E8E8E'
  },
  accountsDetailsName: {
    marginLeft: 15,
    flexDirection: 'row',
    alignItems: 'center'
  }
});
