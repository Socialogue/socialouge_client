import {StyleSheet, Text} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY} from "../../utils/constants";
import * as React from "react";

export default StyleSheet.create({
  container: {
    flex: 10,
    paddingLeft: 20,
    paddingRight: 20,
  },
  headerWrapper: {
    height: 60,
    borderBottomWidth: 1,
    paddingBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  lebelText: {
    fontSize: 14,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    textTransform: 'uppercase',
    marginBottom: 10
  },
  subText: {
    fontSize: 13,
    color: PRIMARY_BLACK,
    fontFamily: GROTESK_SEMIBOLD,
  },
  button: {
    width: 100,
    height: 35,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  }
});
