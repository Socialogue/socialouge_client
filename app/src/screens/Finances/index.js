import * as React from 'react';
import {Image, ScrollView, StyleSheet, Text, TouchableOpacity, View, TextInput} from 'react-native';
import styles from './styles';

import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY, SECONDARY_ORANGE, LIGHT_GRAY} from "../../utils/constants";


const cross = require('./images/cross.png');
const downArrow = require('./images/downArrow.png');
const Group106 = require('./images/Group106.png');


class Finances extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.headerWrapper}>
            <Text style={{fontSize: 30, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK,}}>finances<Text style={{fontSize: 40, color: SECONDARY_ORANGE}}>.</Text></Text>
            <Image source={cross} style={{width: 22, height: 22, marginTop: 10}}/>
          </View>
          <View style={styles.bodyWrapper}>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: 20}}>
              <View>
                <Text style={styles.lebelText}>Ammount due april</Text>
                <Text style={styles.subText}>EUR 0.38 Due April</Text>
              </View>
              <View>
                <TouchableOpacity style={styles.button}>
                  <Text style={styles.buttonText}>Play now</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{borderWidth: 1, borderColor: LIGHT_GRAY}}>
              <View style={{margin: 20, paddingBottom: 10, borderBottomWidth: 1, borderColor: LIGHT_GRAY}}>
                <View>
                  <Text style={styles.lebelText}>Monthly statements</Text>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Text style={{fontFamily: GROTESK_SEMIBOLD}}>2021</Text>
                    <Image source={downArrow} style={{width: 8, height: 5, marginLeft: 5}} />
                  </View>
                </View>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginVertical: 10}}>
                <Text style={{fontSize: 14, color: PRIMARY_GRAY}}>Month</Text>
                <Text style={{fontSize: 14, color: PRIMARY_GRAY}}>Sales</Text>
                <Text style={{fontSize: 14, color: PRIMARY_GRAY}}>Fees & taxes</Text>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginVertical: 10}}>
                <Text style={styles.lebelText}>April 2021</Text>
                <Text style={styles.lebelText}>EUR 0.00</Text>
                <Text style={styles.lebelText}>EUR 0.00</Text>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginVertical: 10}}>
                <Text style={styles.lebelText}>April 2021</Text>
                <Text style={styles.lebelText}>EUR 0.00</Text>
                <Text style={styles.lebelText}>EUR 0.00</Text>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginVertical: 10}}>
                <Text style={styles.lebelText}>April 2021</Text>
                <Text style={styles.lebelText}>EUR 0.00</Text>
                <Text style={styles.lebelText}>EUR 0.00</Text>
              </View>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginVertical: 10}}>
                <Text style={styles.lebelText}>April 2021</Text>
                <Text style={styles.lebelText}>EUR 0.00</Text>
                <Text style={styles.lebelText}>EUR 0.00</Text>
              </View>
            </View>
            <View style={{alignItems: 'center', marginVertical: 30}}>
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>See more...</Text>
              <Image source={Group106} style={{width: 15, height: 20, marginTop: 10}}/>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
export default Finances;

