import * as React from 'react';
import {Text, View, TextInput, StyleSheet} from 'react-native';
import { selectUser } from "core/selectors/user";
import PostSimple from "../../../components/PostSimple";
import {connect} from "react-redux";
import {
  selectCount,
  selectLikedPosts,
  selectPostsLoading
} from "core/selectors/posts";
import { fetchLikedPosts, reactToPost } from "core/actions/postActionCreators";
import Product from '../component/Product';


class LikedTab extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      allPosts: [],
      hasloaded: false
    }
  }

  componentDidMount(){
    console.log("User _id jowel post "+ this.props.user._id);
      this.props.fetchLikedPosts(
        this.props.user._id,
        10,
        0
      );



  };

  componentDidUpdate(){
    // console.log(" jowel posts "+ JSON.stringify(this.props.posts));
    // if (this.props.likedPosts && this.state.allPosts.length == 0) {
    //   this.setState({
    //     hasloaded: true,
    //     allPosts: this.props.likedPosts
    //   });
    // }

    // if (JSON.stringify(prevProps.posts) !== JSON.stringify(this.props.posts)) {
    //   if (this.props.posts.length > 0) {
    //     this.setState({
    //       allPosts: this.state.allPosts.concat(this.props.posts)
    //     });
    //   }
    // }
  };

    render() {

      return (
        <View style={styles.container}>
        {
          this.props.likedPosts && this.props.likedPosts.map(post => (
            <Product key={post._id} post={post}/>
          ))
        }
          <Product />
        </View>
      );
    }
  }


export default connect(
  state => ({
    likedPosts: selectLikedPosts(state),
    total: selectCount(state),
    user: selectUser(state),
    postLoading: selectPostsLoading(state)
  }),
  {
    fetchLikedPosts,
    reactToPost
  }
)(LikedTab);

const styles = StyleSheet.create({
  container: {

  },
});
