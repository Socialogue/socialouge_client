import * as React from 'react';
import {Text, View, TextInput, StyleSheet, Image, TouchableOpacity} from 'react-native';

import {fetchPosts,fetchLikedPosts,fetchFavoritePosts} from "core/actions/postActionCreators";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY} from "../../../utils/constants";
import {Navigation} from "react-native-navigation";
const image = require('../../../images/images-2.png');
const rightArrow = require('../../../images/rightArrow.png');
import { connect } from "react-redux";
import { selectPosts } from "core/selectors/posts";


class OrdersTab extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.activeOrder}>
          <Text style={styles.orderTitle}>ACTIVE ORDERS (3)</Text>
          <View style={styles.card}>
            <View style={styles.title}>
              <Text style={styles.activeOrderTitle}>Product</Text>
              <Image source={image} style={{width: 50, height: 60}} />
            </View>
            <View style={styles.title}>
              <Text style={styles.activeOrderTitle}>Order name</Text>
              <View>
                <Text style={styles.orderName}>WIDE BRIM BUCKET HAT</Text>
                <Text style={styles.orderName}>by BUTIQ K</Text>
                <Text style={styles.orderNum}>Order number: 13-21323-00233</Text>
              </View>
            </View>
            <View style={styles.title}>
              <Text style={styles.activeOrderTitle}>Order date</Text>
              <Text style={styles.orderName}>Jan 03, 2021</Text>
            </View>
            <View style={styles.title}>
              <Text style={styles.activeOrderTitle}>Status</Text>
              <Text style={{
                fontSize: 12,
                color: PRIMARY_BLACK,
                fontFamily: GROTESK_SEMIBOLD,
                paddingBottom: 5,
                color: '#ff6a00'}}>IN PROCESS</Text>
            </View>
            <View style={styles.title}>
              <Text style={styles.activeOrderTitle}>Order total</Text>
              <Text style={styles.orderName}>€ 240.00</Text>
            </View>
            <View style={styles.button}>
              <Text></Text>
              <TouchableOpacity
                style={styles.contactButton}>
                <Text style={styles.buttonText}>Contact seller</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.completeOrder}>
          <Text style={styles.orderTitle}>COMPLETED ORDERS (3)</Text>
          <View style={styles.card}>
            <View style={styles.title}>
              <Text style={styles.activeOrderTitle}>Product</Text>
              <Image source={image} style={{width: 50, height: 60}} />
            </View>
            <View style={styles.title}>
              <Text style={styles.activeOrderTitle}>Order name</Text>
              <View>
                <Text style={styles.orderName}>WIDE BRIM BUCKET HAT</Text>
                <Text style={styles.orderName}>by BUTIQ K</Text>
                <Text style={styles.orderNum}>Order number: 13-21323-00233</Text>
              </View>
            </View>
            <View style={styles.title}>
              <Text style={styles.activeOrderTitle}>Order date</Text>
              <Text style={styles.orderName}>Jan 03, 2021</Text>
            </View>
            <View style={styles.title}>
              <Text style={styles.activeOrderTitle}>Status</Text>
              <Text style={{
                fontSize: 12,
                color: PRIMARY_BLACK,
                fontFamily: GROTESK_SEMIBOLD,
                paddingBottom: 5,
                color: 'green'}}>SHIPPED</Text>
            </View>
            <View style={styles.title}>
              <Text style={styles.activeOrderTitle}>Order total</Text>
              <Text style={styles.orderName}>€ 240.00</Text>
            </View>
            <View style={styles.comButton}>
              <TouchableOpacity
                style={styles.contactButton}>
                <Text style={styles.buttonText}>Return item</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.contactButton}>
                <Text style={styles.buttonText}>Contact seller</Text>
              </TouchableOpacity>
              <View style={styles.button}>
                <Text></Text>
                <TouchableOpacity
                  style={styles.leaveButton}>
                  <Text style={styles.buttonText}>Leave review </Text>
                  <Image source={rightArrow} style={{width: 25, height: 10}} />
                </TouchableOpacity>
              </View>
            </View>

          </View>
        </View>

      </View>
    );
  }
}

export default connect(
  state => ({
    posts: selectPosts(state)
  }),
  {
    fetchPosts
  }
)(OrdersTab);





const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  activeOrder: {
    paddingVertical: 20
  },
  orderTitle: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    textTransform: 'uppercase',
    marginBottom: 20
  },
  card: {
    padding: 20,
    borderTopWidth: 1,
    borderBottomWidth: 1
  },
  title: {
    paddingVertical: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  activeOrderTitle: {
    color: PRIMARY_BLACK,
    fontFamily: GROTESK_SEMIBOLD,
  },
  orderName: {
    fontSize: 12,
    color: PRIMARY_BLACK,
    fontFamily: GROTESK_SEMIBOLD,
    paddingBottom: 5
  },
  orderNum: {
    fontSize: 10,
    color: PRIMARY_BLACK,
    fontFamily: PRIMARY_GRAY
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  contactButton: {
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderWidth: 1,
    marginTop: 20
  },
  buttonText: {
    fontSize: 10,
    color: PRIMARY_BLACK,
    fontFamily: GROTESK_SEMIBOLD,
  },
  comButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  leaveButton: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    marginTop: 20
  }
});
