import * as React from 'react';
import {Image, StyleSheet, Text, View, TextInput} from 'react-native';

const search = require('../images/search.png');
const home = require('../images/homeicons.png');
const rightarrow = require('../images/arrowright.png');
const shop = require('../images/shopicons.png');
const sell = require('../images/sellicon.png');

class MenuWarpper extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.searchWarpper}>
          <Image source={search} style={search} />
          <View style={styles.searchInput}>
            <TextInput
              style={styles.searchBar}
              placeholder="Search..."
            />
          </View>
        </View>
        <View style={styles.menuWarpper}>
          <View style={styles.homeMenu}>
            <View style={styles.textIcon}>
              <Image source={home} style={styles.homeIcon} />
              <Text style={styles.homeText}>HOME</Text>
            </View>
            <Image source={rightarrow} style={styles.rightArrow} />
          </View>
          <View style={styles.shopMenu}>
            <View style={styles.textIcon}>
              <Image source={shop} style={styles.shopIcon} />
              <Text style={styles.shopText}>SHOP</Text>
            </View>
            <Image source={rightarrow} style={styles.rightArrow} />
          </View>
          <View style={styles.sellMenu}>
            <View style={styles.textIcon}>
              <Image source={sell} style={styles.sellIcon} />
              <Text style={styles.sellText}>SELLS</Text>
            </View>
            <Image source={rightarrow} style={styles.rightArrow} />
          </View>
        </View>

        <View style={styles.bottomWarpper}>
          <Text style={styles.logIn}>LOGIN</Text>
          <Text style={styles.signUp}>SIGNUP</Text>
        </View>
      </View>
    );
  }
}

export default MenuWarpper;

const styles = StyleSheet.create({
  container: {
    padding: 20,
    flex: 100
  },
  searchWarpper: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 10
  },
  search: {
    marginRight: 20,
  },
  searchInput: {
    borderBottomWidth: 1,
    width: '95%'
  },
  menuWarpper: {
    flex: 80,
  },
  homeMenu: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 30,
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#E5E5E5'
  },
  textIcon: {
    flexDirection: 'row',
  },
  homeIcon: {
    marginRight: 10
  },
  shopMenu: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#E5E5E5'
  },
  shopIcon: {
    marginRight: 10
  },
  sellMenu: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#E5E5E5'
  },
  sellIcon: {
    marginRight: 10
  },
  bottomWarpper: {
    flex: 10,
    flexDirection: 'row',
    alignItems: 'flex-end'
  },
  logIn: {
    marginRight: 20
  }

});
