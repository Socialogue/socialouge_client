import * as React from 'react';
import {Dimensions, Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Navigation} from "react-native-navigation";
import {navigateToChatConversation, navigateToProductPage} from "../../../navigator";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../../utils/constants";

const FirstPost = require('../../../images/images-2.png');
const Like = require('../../../images/Like.png');
const Favorite = require('../../../images/Favorite.png');


class Product extends React.Component {


  render() {

    // const {item} = this.props;

    return (
      <View style={styles.container}>
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => navigateToProductPage(this.props.componentId)}>
          <Image source={FirstPost} style={{width: '100%', height: 400}} />
        </TouchableOpacity>
        <Text style={styles.postTitle}>Name</Text>
          <Text style={styles.byTitle}>by BUTIQ K</Text>
          <View style={styles.bottomWarpper}>
            <View style={styles.priceBox}>
              <Text style={styles.price}>30 €</Text>
            </View>
            <View style={styles.reactBox}>
              <Image source={Like} style={{width: 20, height: 22, marginRight: 20}} />
              <Image source={Favorite} style={{width: 20, height: 22}} />
            </View>
          </View>
      </View>
    );
  }
}

export default Product;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  postTitle: {
    fontSize: 18,
    paddingTop: 10,
    paddingBottom: 10,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  byTitle: {
    fontSize: 16,
    paddingTop: 10,
    paddingBottom: 10,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  bottomWarpper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  price: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  reactBox: {
    flexDirection: 'row',
  },
});
