import * as React from 'react';
import moment from "moment";
import {Text, View, TextInput, StyleSheet} from 'react-native';
import { selectUser } from "core/selectors/user";
import PostSimple from "../../../components/PostSimple";
import { selectPosts, selectPostsLoading } from "core/selectors/posts";
import { fetchPosts } from "core/actions/postActionCreators";
import {connect} from "react-redux";
import Post from '../component/Post';

class PostTab extends React.Component {

constructor(props) {
  super(props);
  this.state = {
    allPosts: [],
    hasloaded: false
  }
}

componentDidMount(){
  console.log("User _id jowel post "+ this.props.user._id);
    this.props.fetchPosts(
      this.props.user._id,
      10,
      0
    );



};

componentDidUpdate(){
  console.log(" jowel posts "+ JSON.stringify(this.props.posts));
  // if (this.state.hasloaded == false) {
  //   this.setState({
  //     hasloaded: true,
  //     allPosts: this.props.posts
  //   });
  // }

  // if (JSON.stringify(prevProps.posts) !== JSON.stringify(this.props.posts)) {
  //   if (this.props.posts.length > 0) {
  //     this.setState({
  //       allPosts: this.state.allPosts.concat(this.props.posts)
  //     });
  //   }
  // }
};

  render() {

    return (
      <View style={styles.container}>
      {
        this.props.posts && this.props.posts.map(post => (
          <Post key={post._id} post={post}/>
        ))
      }
      </View>
    );
  }
}

export default connect(
  state => ({
    posts: selectPosts(state),
    postLoading: selectPostsLoading(state),
    user: selectUser(state)
  }),
  {
    fetchPosts
  }
)(PostTab);



const styles = StyleSheet.create({
  container: {

  },
});
