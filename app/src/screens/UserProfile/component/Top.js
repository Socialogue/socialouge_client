import * as React from 'react';
import { Image, StyleSheet, Text, View, ScrollView, TouchableOpacity } from "react-native";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../../utils/constants";
import {navigateToUserProfile, navigateToUserProfileSetting} from "../../../navigator";
import {selectIsLoggedIn, selectUser} from "core/selectors/user";
import {getFollowedStatus} from "core/selectors/profile";
import {addToFollow} from "core/actions/publicProfileActionCreator";
import {openChatWindow} from "core/messenger/messageActionCreators";
import {fetchConversations} from "core/messenger/actionCreators/conversationsActionCreators";
import { connect } from "react-redux";
import {errorAlert, successAlert} from "web/src/utils/alerts";
import {ROUTE_LOGIN} from "web/src/routes/constants";
import {getImageOriginalUrl} from "core/utils/helpers";

const coverphoto = require('../images/coverphoto.png');
const message = require('../images/massegeicon.png');
const userAvater = require('../images/userAvater.png');

class Top extends React.Component {
  constructor(props) {
    super(props);
  }


  componentDidMount = () => {};
  componentDidUpdate = prevProps => {
    if (prevProps.isFollowed !== this.props.isFollowed) {
      if (this.props.isFollowed) {
        successAlert("Followed");
      }
    }
  };

  follow = userId => {
    console.log("userId  ", userId);
    this.props.fetchConversations(userId);
    // return;

    try {
      this.props.addToFollow(userId, this.props.match.params.id);
    } catch (err) {
      console.log(err);
      errorAlert("Something went wrong.");
    }
  };

  onClickMessage = userId => {
    if (!this.props.isLoggedIn) {
      this.props.history.push(ROUTE_LOGIN);
    } else {
      this.props.openChatWindow(
        this.props.match.params.id,
        this.props.user.name,
        this.props.user.profileImage
      );
    }
  };
  render() {
    console.log('image', this.props.loggedInUser.coverImage);


    return (
      <View style={styles.container}>
        <View style={styles.cover}>
        <Image source= {{uri: getImageOriginalUrl(this.props.loggedInUser.coverImage)}} style={{width:'100%',height:150}}/>
          <Image source= {{uri: getImageOriginalUrl(this.props.loggedInUser.profileImage)}} style={styles.userAvater} />
        </View>
        <View style={styles.nameProfile}>
          <View style={styles.name}>
            <Text style={styles.nameTitle}>{this.props.loggedInUser.fullName}</Text>
            <Text style={styles.followersCount}>{this.props.loggedInUser.totalFollowers} FOLLOWERS</Text>
            <TouchableOpacity
              style={{paddingVertical: 10}}
              onPress={() =>navigateToUserProfileSetting(this.props.componentId)}>
              <Text style={styles.editProfile}>Edit Profile</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

// export default Top;
export default connect(
  state => ({
    loggedInUser: selectUser(state),
    isLoggedIn: selectIsLoggedIn(state),
    isFollowed: getFollowedStatus(state)
  }),
  {
    addToFollow,
    openChatWindow,
    fetchConversations
  }
)(Top);

const styles = StyleSheet.create({
  container: {

  },
  cover: {
    flex: 30
  },
  userAvater: {
    marginLeft: 20,
    height: 80,
    width: 80,
    borderRadius: 37.5,
    position: 'absolute',
    bottom: '-30%',
    backgroundColor: '#fff'
  },
  nameProfile: {
    flexDirection: 'row',
    flex: 100,
    padding: 20,
    marginTop: 40
  },
  nameTitle: {
    fontSize: 30,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  name: {
    flex: 60
  },
  followersCount: {
    marginTop: 10,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  editProfile: {
    color: '#868686'
  },

});
