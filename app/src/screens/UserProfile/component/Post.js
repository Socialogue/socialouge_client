import * as React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY} from "../../../utils/constants";

import TimeAgo from '../../../components/Time';
import { navigateToProductPage, navigateToPublicUserProfile } from '../../../navigator';
import moment from "moment";
import {getImageOriginalUrl} from "core/utils/helpers";
const Like = require('../../../images/Like.png');
const Comment = require('../../../images/Comment.png');
const postImage = require('../../../images/noImage.png');

class PostCommentReply extends React.Component {
  render() {
    const {post} = this.props;
    let imgSrc = postImage;
    if(post.images && post.images.length) {
      imgSrc = {uri: getImageOriginalUrl(post.images[0].url)};
    }
    console.log('image', post.images[0].url);
    return (
      <View style={styles.container}>
        <TouchableOpacity
          activeOpacity={.8}
          onPress={() =>navigateToProductPage(this.props.componentId)}>
          <Image source={imgSrc} style={styles.postImage} />
        </TouchableOpacity>
        <View style={styles.bottomWarpper}>
          <View style={styles.byTitle}>
            <View style={styles.nameTitle}>
              <Text style={styles.time}>{moment(post.createdAt).fromNow()}</Text>
            </View>
            <View style={styles.reactBox}>
              <Image source={Like} style={{width: 20, height: 22, marginRight: 10}} />
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>{post.totalLike}</Text>
              <Image source={Comment} style={{width: 20, height: 22, marginRight: 10, marginLeft: 10}} />
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>{post.totalComment}</Text>
            </View>
          </View>
        </View>
        <Text style={styles.desscription}>
          {post.content || post.postId.content}
        </Text>

      </View>
    );
  }
}

export default PostCommentReply;

const styles = StyleSheet.create({
  container: {
    marginBottom: 20
  },
  postImage: {
    width: '100%',
    height: 400,
  },
  byTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
  },
  time: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  },
  reactBox: {
    flexDirection: 'row',
    marginTop: 10,
  },
  like: {
    marginRight: 10,
  },
  comment: {
    marginRight: 10,
    marginLeft: 10,
  },
  desscription: {
    color: PRIMARY_GRAY,
  },
});
