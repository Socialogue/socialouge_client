import * as React from 'react';
import {Text, View, Image, StyleSheet} from 'react-native';


const largeimage = require('../images/largeimage.png');
const smallimage_1 = require('../images/smallimage-1.png');
const smallimage_2 = require('../images/smallimage-2.png');
const smallimage_3 = require('../images/smallimage-3.png');
const grouparrow = require('../images/grouparrow.png');
import {
  fetchFavoriteProducts,
  reactToProduct
} from "core/actions/productActionCreators";
import { fetchOutfits } from "core/actions/outfitsActionCreators";
import PostSimple from "../../../components/PostSimple";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../../utils/constants";

import { connect } from "react-redux";
import { selectPostsLoading } from "core/selectors/posts";

import { selectOutfitsData, selectOutfitsMsg } from "core/selectors/outfits";

import {
  selectFavoriteProducts,
  selectTotalFavorites
} from "core/selectors/products";

import { selectUser } from "core/selectors/user";



class FavoriteTab extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      allPosts: [],
      hasloaded: false
    }
  }
  
  componentDidMount(){
    console.log("User _id jowel post "+ this.props.user._id);
      this.props.fetchFavoriteProducts(
        this.props.user._id,
        10,
        0
      );
      this.props.fetchOutfits();
  };

  render() {
    return (
      <View style={styles.container}>
      
        <View style={styles.outfitWarpper}>
          <Text style={styles.outfitHeadTitle}>MY OUTFITS </Text>
          
          <View style={styles.largeImageView}>
            <Image source={largeimage} style={styles.largeImage} />
          </View>
          <View style={styles.smallImageView}>
            <Image source={smallimage_1} style={styles.smallimage} />
            <Image source={smallimage_2} style={styles.smallimage} />
            <Image source={smallimage_3} style={styles.smallimage} />
          </View>
          <View style={styles.outfitBottom}>
            <Text style={styles.outfitTitle}>Winter Outfit 2020</Text>
            <Text style={styles.outfitProduct}>14 Products</Text>
          </View>
          <View style={styles.showMore}>
            <Text style={styles.showMoreText}>Show more...</Text>
            <Image source={grouparrow} style={styles.grouparrow}/>
          </View>
          
        </View>
          
      
        <View style={styles.postWarpper}>
          <PostSimple />
          <View style={styles.showMore}>
            <Text style={styles.showMoreText}>Show more...</Text>
            <Image source={grouparrow} style={styles.grouparrow}/>
          </View>
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    favoriteProducts: selectFavoriteProducts(state),
    favoriteCount: selectTotalFavorites(state),
    user: selectUser(state),
    postLoading: selectPostsLoading(state),
    outfits: selectOutfitsData(state),
    outfitsErrorMsg: selectOutfitsMsg(state)
  }),
  {
    fetchFavoriteProducts,
    fetchOutfits,
    reactToProduct
  }
)(FavoriteTab);



const styles = StyleSheet.create({
  outfitWarpper: {
    borderBottomWidth: 1,
    paddingBottom: 30,
    marginBottom: 20
  },
  outfitHeadTitle: {
    fontSize: 18,
    marginBottom: 20,
    color: PRIMARY_BLACK,
    fontFamily: GROTESK_SEMIBOLD,
  },
  largeImage: {
    width: '100%',
    height: 300
  },
  smallImageView: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: 20,
    marginBottom: 30
  },
  smallimage: {
    width: '31%',
    height: 100
  },
  outfitBottom: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  outfitTitle: {
    fontSize: 18,
    marginBottom: 10,
    color: PRIMARY_BLACK,
    fontFamily: GROTESK_SEMIBOLD,
  },
  outfitProduct: {
    marginBottom: 20,
    color: PRIMARY_BLACK,
    fontFamily: GROTESK_SEMIBOLD,
  },
  showMore: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  showMoreText: {
    marginBottom: 20,
    color: PRIMARY_BLACK,
    fontFamily: GROTESK_SEMIBOLD,
  },
  grouparrow: {
    width: 15,
    height: 20,
    paddingBottom: 20
  },


});
