import * as React from 'react';
import { Image, StyleSheet, Text, View, ScrollView, TouchableOpacity } from "react-native";

import PostTab from '../component/PostTab';
import FavoriteTab from '../component/FavoriteTab';
import FollowingTab from '../component/FollowingTab';
import ReviewTab from '../component/ReviewTab';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../../utils/constants";

class ProfileTab extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      activetab: 'post'
    };
  }

  render() {

    const {activetab} = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.tabButton}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.setState({activetab: 'post'})}>
            <Text
              style={
                activetab === 'post'
                  ? styles.activeButtonText
                  : styles.inActiveButtonText
              }>
              POSTS
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.setState({activetab: 'favorite'})}>
            <Text
              style={
                activetab === 'favorite'
                  ? styles.activeButtonText
                  : styles.inActiveButtonText
              }>
              FAVORITES
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.setState({activetab: 'following'})}>
            <Text
              style={
                activetab === 'following'
                  ? styles.activeButtonText
                  : styles.inActiveButtonText
              }>
              FOLLOWING
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.lastButton}
            onPress={() => this.setState({activetab: 'review'})}>
            <Text
              style={
                activetab === 'review'
                  ? styles.activeButtonText
                  : styles.inActiveButtonText
              }>
              REVIEWS
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.tabContent}>
          {activetab === 'post' && <PostTab />}
          {activetab === 'favorite' && <FavoriteTab />}
          {activetab === 'following' && <FollowingTab />}
          {activetab === 'review' && <ReviewTab />}
        </View>
      </View>
    );
  }
}

export default ProfileTab;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,

  },
  tabButton: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderBottomWidth: 1,
    marginBottom: 20,
  },

  activeButtonText: {
    fontSize: 12,
    borderBottomWidth: 3,
    paddingBottom: 5,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  inActiveButtonText: {
    fontSize: 12,
    color: '#868686',
  },
});
