import * as React from 'react';
import {Text, View, TextInput, StyleSheet} from 'react-native';

import Post from "../component/Post";
import {fetchPosts,fetchLikedPosts,fetchFavoritePosts} from "core/actions/postActionCreators";
import {connect} from "react-redux";

class PostTab extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Post componentId={this.props.componentId} />
      </View>
    );
  }
}

export default connect((state) => {

}, {
  fetchPosts
})(PostTab);



const styles = StyleSheet.create({
  container: {

  },
});
