import * as React from 'react';
import {Text, View, StyleSheet, Image} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../../utils/constants";

const reviewicon = require('../images/reviewicon.png');
const reviewicondisable = require('../images/reviewicondisable.png');
const reviewimage = require('../images/reviewimage.png');
const grouparrow = require('../images/grouparrow.png');
const userAvater = require('../../../images/userAvater.png');



class RewiewTab extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.rewiewWarpper}>
          <Text style={styles.rewiewCount}>REWIEWS (33)</Text>
          <View style={styles.reviewsIcon}>
            <Image source={reviewicon} style={styles.reviewicon} />
            <Image source={reviewicon} style={styles.reviewicon} />
            <Image source={reviewicon} style={styles.reviewicon} />
            <Image source={reviewicon} style={styles.reviewicon} />
            <Image source={reviewicondisable} style={styles.reviewicon} />
          </View>
          <View style={styles.rewiewerId}>
            <Image source={userAvater} style={{marginRight: 10, height: 50, width: 50, borderRadius: 37.5}} />
            <View style={styles.rewiewerDetails}>
              <View style={styles.idName}>
                <Text style={styles.rewiewName}>UNIQ BUTIQ </Text>
                <Text style={styles.rewiewTime}>at 19 Jan 2018</Text>
              </View>
              <Text style={styles.desscription}>
                The passage is attributed to an unknown typesetter in the 15th
                century.
              </Text>
            </View>
          </View>
          <View style={styles.rewiewImage}>
            <Image source={reviewimage}style={styles.rewiewimage} />
          </View>
          <View style={styles.rewiewTitle}>
            <Text style={styles.rewiewTitleText}>WIDE BRIM BUCKET HAT</Text>
            <Text style={styles.rewiewSubText}>by BUTIQ K</Text>
          </View>
          <View style={styles.showMore}>
            <Text style={styles.showMoreText}>Show more...</Text>
            <Image source={grouparrow} style={styles.grouparrow} />
          </View>
        </View>
      </View>
    );
  }
}

export default RewiewTab;

const styles = StyleSheet.create({
  rewiewWarpper: {
    borderBottomWidth: 1,
    paddingBottom: 20,
    marginBottom: 20
  },
  rewiewCount: {
    color: PRIMARY_BLACK,
    fontFamily: GROTESK_SEMIBOLD,
  },
  reviewsIcon: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginBottom: 20
  },
  reviewicon: {
    marginRight: 5,
    width: 15,
    height: 15
  },
  rewiewerId: {
    flexDirection: 'row'
  },
  rewiewerDetails: {
    paddingRight: 20,
    paddingLeft: 20
  },
  idName: {
    flexDirection: 'row'
  },
  rewiewSubText: {
    color: PRIMARY_BLACK,
    fontFamily: GROTESK_SEMIBOLD,
  },
  rewiewName: {
    fontSize: 14,
    marginBottom: 10,
    marginRight: 10,
    color: PRIMARY_BLACK,
    fontFamily: GROTESK_SEMIBOLD,
  },
  rewiewTime: {
    fontSize: 12,
    color: '#BBBBBB'
  },
  desscription: {
    color: '#868686',
    marginBottom: 40
  },
  rewiewImage: {
    alignItems: 'center',
    marginBottom: 20
  },
  rewiewTitle: {
    alignItems: 'center',
  },
  rewiewTitleText: {
    marginBottom: 10,
    color: PRIMARY_BLACK,
    fontFamily: GROTESK_SEMIBOLD,
  },
  showMore: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
  },
  showMoreText: {
    marginBottom: 20,
    color: PRIMARY_BLACK,
    fontFamily: GROTESK_SEMIBOLD,
  },
  grouparrow: {
    paddingBottom: 20,
    width: 15,
    height: 20,
  },
});
