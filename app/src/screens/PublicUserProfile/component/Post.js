import * as React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY} from "../../../utils/constants";

import TimeAgo from '../../../components/Time';
import { navigateToProductPage, navigateToPublicUserProfile } from '../../../navigator';
import moment from "moment";
const post = require('../images/images-5.png');
const Like = require('../../../images/Like.png');
const Comment = require('../../../images/Comment.png');
const userAvater = require('../../../images/userAvater.png');

class Post extends React.Component {
  render() {
    const {post} = this.props;
    return (
      <View style={styles.container}>
        {/*<TouchableOpacity*/}
        {/*  activeOpacity={.7}*/}
        {/*  onPress={() =>navigateToProductPage(this.props.componentId)}>*/}
        {/*  <Image source={{url: post.images && post.images.length > 0 && post.images[0] ? post.images[0].url : ""}} style={styles.postCommentReply} />*/}
        {/*</TouchableOpacity>*/}
        {/*<View style={styles.bottomWarpper}>*/}
        {/*  <TouchableOpacity*/}
        {/*    activeOpacity={.7}*/}
        {/*    onPress={() =>navigateToPublicUserProfile(this.props.componentId)}>*/}
        {/*    <View style={styles.byTitle}>*/}
        {/*      <Image source={userAvater} style={{ width: 40, height: 40, borderRadius: 37.5 }} />*/}
        {/*      <View style={styles.nameTitle}>*/}
        {/*        <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>{post.postId && post.postId.userId && post.postId.userId.name   ? post.postId.userId.name : ""}</Text>*/}
        {/*        <Text>{moment(post.createdAt).fromNow()}</Text>*/}
        {/*      </View>*/}
        {/*    </View>*/}
        {/*  </TouchableOpacity>*/}
        {/*</View>*/}
        {/*<Text style={styles.desscription}>*/}
        {/*  {post.content || post.postId.content}*/}
        {/*</Text>*/}
        {/*<View style={styles.reactBox}>*/}
        {/*  <Image source={Like} style={{width: 20, height: 22, marginRight: 10}} />*/}
        {/*  <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>{post.totalLike} LIKES</Text>*/}
        {/*  <Image source={Comment} style={{width: 20, height: 22, marginRight: 10, marginLeft: 10}} />*/}
        {/*  <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>{post.totalComment} COMMENTS</Text>*/}
        {/*</View>*/}
      </View>
    );
  }
}

export default Post;

const styles = StyleSheet.create({
  container: {

  },
  postCommentReply: {
    width: '100%',

  },
  byTitle: {
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
  },
  nameTitle: {
    marginLeft: 10
  },

  reactBox: {
    flexDirection: 'row',
    marginTop: 10,
  },
  like: {
    marginRight: 10,
  },
  comment: {
    marginRight: 10,
    marginLeft: 10,
  },
  desscription: {
    color: PRIMARY_GRAY,
  },
});
