import * as React from 'react';
import {Text, View, Image, TouchableOpacity, ScrollView, StyleSheet} from 'react-native';
import {Navigation} from "react-native-navigation";
import {navigateToAppMenu, navigateToChatConversation} from "../../navigator";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../utils/constants;
import {connect} from "react-redux";
import {selectPosts} from "core/selectors/posts";
import {selectFeed, selectToken, selectViewType} from "core/selectors/user";
import {getPostProducts} from "core/actions/homeActionCreators";


const socialoguelogo = require('../../images/Socialoguelogo.png');
const cart = require('../../images/cart.png');
const menubar = require('../../images/menubar.png');

class Header extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerWarpper}>
          <View style={styles.logo}>
            <Image source={socialoguelogo} style={{width: 100, height: 25}} />
          </View>
          <View style={styles.iconPart}>
            {/*<Image source={search} style={{width: 22, height: 22}} />*/}
            {/*<Image source={message} style={{width: 22, height: 22, marginLeft: 20}} />*/}
            <Image source={cart} style={{width: 22, height: 22, marginLeft: 20}} />
            <Text style={styles.logInText}>LOGIN</Text>
            <TouchableOpacity
              onPress={() => navigateToAppMenu(this.props.componentId)}>

              <Image source={menubar} style={{width: 25, height: 25, marginLeft: 20}} />

            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    posts: selectPosts(state),
    isLoggedIn: !!selectToken(state),
    auth: state.auth,
    sortViewType: selectViewType(state),
    feed: selectFeed(state)
  }),
  {
    getPostProducts
  }
)(Header);

const styles = StyleSheet.create({
  container: {
    position: 'relative'
  },
  headerWarpper: {
    flex: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 20,
    width: '93%',
    position: 'relative'
  },
  iconPart: {
    flexDirection: 'row',
  },
  logInText: {
    marginLeft: 20,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },

});
