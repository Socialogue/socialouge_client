

import React, { Component } from 'react';
import {
  Platform,
  View,
  StyleSheet,
  Text,
  Image,
  TextInput,
  AppRegistry,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import {Navigation} from "react-native-navigation";
import {navigateToLogin, navigateToPlus} from "../../navigator";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../utils/constants";
import styles from './styles';

const cross = require('../../images/cross.png');
const Ellipse45 = require('../../images/Ellipse45.png');
const arrowLeft = require('../../images/arrowLeft.png');
const search = require('../../images/search.png');
const searchresultimage = require('../../images/searchresultimage.png');


export default class TagListScreen extends Component <{}> {

  constructor(props) {
    super(props);
    this.state = {
      activetab: 'yourItem',
    };
  }

  render() {
    const {activetab} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.headerWarpper}>
          <View style={styles.createPost}>
            <TouchableOpacity
              style={styles.createPost}
              onPress={() => navigateToPlus(this.props.componentId)}
            >
              {/*<Header componentId={this.props.componentId}></Header>*/}

              <Image source={arrowLeft} style={{width: 25, height: 25, marginRight: 5}}/>
              <Text style={{
                fontSize: 16,
                fontFamily: GROTESK_SEMIBOLD,
                color: PRIMARY_BLACK
              }}>CREATE A POST</Text>
            </TouchableOpacity>

          </View>
          <View style={styles.createPostCross}>
            <TouchableOpacity
              onPress={() => Navigation.pop(this.props.componentId)}>
              {/*<Header componentId={this.props.componentId}></Header>*/}
              <Image source={cross} style={{width: 25, height: 25}}/>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.bodyWarpper}>
          <View style={styles.tabContainer}>
            <View style={styles.tabButton}>
              <TouchableOpacity
                style={styles.yourItemButton}
                onPress={() => this.setState({activetab: 'yourItem'})}>
                <Text
                  style={
                    activetab === 'yourItem'
                      ? styles.activeClothesButtonText
                      : styles.inActiveClothesButtonText
                  }>
                  YOUR ITEM
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.shopsButton}
                onPress={() => this.setState({activetab: 'shops'})}>
                <Text
                  style={
                    activetab === 'shops'
                      ? styles.activeShopsButtonText
                      : styles.inActiveShopsButtonText
                  }>
                  SHOPS
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.socialogueButton}
                onPress={() => this.setState({activetab: 'socialogue'})}>
                <Text
                  style={
                    activetab === 'socialogue'
                      ? styles.activeAccountsButtonText
                      : styles.inActiveAccountsButtonText
                  }>
                  FROM SOCIALOGUE
                </Text>
              </TouchableOpacity>
            </View>

            <View style={styles.tabContent}>
              {activetab === 'yourItem' &&
              <ScrollView>
                <View style={styles.searchWarpper}>
                  <Image source={search} style={{width: 22, height: 22}} />
                  <View style={styles.searchInput}>
                    <TextInput
                      style={styles.searchBar}
                      placeholder="Search..."
                    />
                  </View>
                </View>
                <View style={styles.searchResult}>
                  <Image source={searchresultimage} style={searchresultimage} />
                  <View style={styles.searchDetails}>
                    <View style={styles.DetailsName}>
                      <Text style={styles.name}>Classic</Text>
                      <Text style={styles.searchName}>shart</Text>
                    </View>
                    <View style={styles.productsCategory}>
                      <Text style={styles.category}>Cloths</Text>
                      <Text style={styles.dots}>{'\u2022'}</Text>
                      <Text style={styles.productsCondition}>Fevorite</Text>
                    </View>
                  </View>
                </View>
                <View style={styles.searchResult}>
                  <Image source={searchresultimage} style={searchresultimage} />
                  <View style={styles.searchDetails}>
                    <View style={styles.DetailsName}>
                      <Text style={styles.name}>Classic</Text>
                      <Text style={styles.searchName}>shart</Text>
                    </View>
                    <View style={styles.productsCategory}>
                      <Text style={styles.category}>Cloths</Text>
                      <Text style={styles.dots}>{'\u2022'}</Text>
                      <Text style={styles.productsCondition}>Fevorite</Text>
                    </View>
                  </View>
                </View>
              </ScrollView>
              }
              {activetab === 'shops' &&
              <ScrollView>
                <View style={styles.searchWarpper}>
                  <Image source={search} style={{width: 22, height: 22}} />
                  <View style={styles.searchInput}>
                    <TextInput
                      style={styles.searchBar}
                      placeholder="Search..."
                    />
                  </View>
                </View>
              </ScrollView>
              }
              {activetab === 'socialogue' &&
              <ScrollView>
                <View style={styles.searchWarpper}>
                  <Image source={search} style={{width: 22, height: 22}} />
                  <View style={styles.searchInput}>
                    <TextInput
                      style={styles.searchBar}
                      placeholder="Search..."
                    />
                  </View>
                </View>
                <View style={styles.searchResult}>
                  <Image source={Ellipse45} style={styles.reviewerIdIcon} />
                  <View style={styles.accountsDetailsName}>
                    <Text style={styles.name}>LILY-ROSE FLOWERS</Text>
                    <Text style={styles.searchName}>SHIRT</Text>
                  </View>
                </View>
                <View style={styles.searchResult}>
                  <Image source={Ellipse45} style={styles.reviewerIdIcon} />
                  <View style={styles.accountsDetailsName}>
                    <Text style={styles.name}>LILY-ROSE FLOWERS</Text>
                    <Text style={styles.searchName}>SHIRT</Text>
                  </View>
                </View>
              </ScrollView>
              }
            </View>
          </View>
        </View>
        <View style={styles.bottomWarpper}>
          <View style={styles.cancelButton}>
            <TouchableOpacity style={styles.button}>
              <Text>CANCEL</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.postButton}>
            <TouchableOpacity style={styles.button}>
              <Text>POST</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

