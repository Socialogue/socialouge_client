import * as React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Navigation} from "react-native-navigation";
import {GROTESK_SEMIBOLD, LIGHT_GRAY, PRIMARY_BLACK, PRIMARY_GRAY, PRIMARY_WHITE} from "../../../utils/constants";
import PrimaryButton from "../../../components/PrimaryButton";
import {navigateToLogin, navigateToRegister, navigateToResetPassword} from "../../../navigator";

const google = require('../images/google.png');
const facebook = require('../images/facebook.png');

class Footer extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logInWrapper}>
          <PrimaryButton
            loading={this.props.loading}
            title='login'
            onPress={this.props.onPressButton}/>
        </View>
        <View style={styles.registerOr}>
          <TouchableOpacity
            onPress={() =>navigateToRegister(this.props.componentId)}>
            {/*<Header componentId={this.props.componentId}></Header>*/}
            <Text
              style={styles.register}
            >
              Don't have an account?
              <Text style={{fontSize: 15}}>
                {" "}Register
              </Text>
            </Text>
          </TouchableOpacity>
          <Text style={styles.or}>or</Text>
        </View>
        <View style={styles.linkContainer}>
          <View style={styles.linkWrapper}>
            <TouchableOpacity style={styles.googleButton}>
              <Image source={google} style={{width: 18, height: 18, marginRight: 10}} />
              <Text style={styles.googleButtonText}>Google</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.facebookButton}>
              <Image source={facebook} style={{width: 10, height: 18, marginRight: 10}} />
              <Text style={styles.facebookButtonText}>Facebook</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default Footer;

const styles = StyleSheet.create({
  container: {
    // flex: 100,
  },
  logInWrapper: {
    alignItems: 'center',
    marginRight: 20,
    marginLeft: 20,
    marginBottom: 20
  },
  logInButton: {
    backgroundColor: PRIMARY_BLACK,
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logInButtonText: {
    color: PRIMARY_WHITE,
    fontSize: 15,
    fontFamily: GROTESK_SEMIBOLD,
  },
  registerOr: {
    alignItems: 'center',
  },
  or: {
    color: PRIMARY_GRAY,
    marginTop: 20,
  },
  linkContainer: {
    alignItems: 'center',
  },
  linkWrapper: {
    flexDirection: 'row',
    marginTop: 20,
  },
  googleButton: {
    flexDirection: 'row',
    backgroundColor: LIGHT_GRAY,
    justifyContent: 'center',
    alignItems: 'center',
    width: 130,
    height: 50,
    marginRight: 20,
  },
  googleButtonText: {
    color: PRIMARY_GRAY,
  },
  facebookButton: {
    flexDirection: 'row',
    backgroundColor: '#3B5998',
    justifyContent: 'center',
    alignItems: 'center',
    width: 130,
    height: 50,
  },
  facebookButtonText: {
    color: PRIMARY_WHITE,
  },
  register: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  }
});
