import * as React from 'react';
import {Text, View, TextInput, StyleSheet, props, TouchableOpacity} from 'react-native';
import {GROTESK_SEMIBOLD} from "../../../utils/constants";
import {PRIMARY_BLACK} from '../../../utils/constants';
import {SECONDARY_ORANGE} from '../../../utils/constants';
import {PRIMARY_GRAY} from '../../../utils/constants';



import CheckBox from '@react-native-community/checkbox';
import {Navigation} from "react-native-navigation";
import {navigateToHome, navigateToResetPassword, navigateToSubmitEmail} from "../../../navigator";

class From extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
    };
  }

  toggleCheckbox = newValue => {
    this.setState({checked: newValue});
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.emailTitle}>EMAIL / PHONE NUMBER</Text>
        <TextInput
          style={styles.textInput}
          placeholder="Your Email or phone number"
          value={this.props.username}
          onChangeText={e => this.props.onChange('username', e)}
        />
        <Text style={styles.password}>PASSWORD</Text>
        <TextInput
          secureTextEntry={true}
          style={styles.textInput}
          placeholder="Your Password"
          value={this.props.password}
          onChangeText={e => this.props.onChange('password', e)}
        />
        <View style={styles.fromFooter}>
          <View style={styles.checkboxContainer}>
            <CheckBox
              style={styles.checkBox}
              value={this.state.checked}
              onValueChange={this.toggleCheckbox}
            />
            <Text style={styles.label}>Remember me</Text>
          </View>
          <TouchableOpacity
            onPress={() => navigateToSubmitEmail(this.props.componentId)}
            >
            <Text style={styles.forgot}>Forgot Password?</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default From;

const styles = StyleSheet.create({
  container: {
    paddingLeft: 20,
    paddingRight: 20,
    borderLeftWidth: 1,
    borderLeftColor: SECONDARY_ORANGE,
  },
  emailTitle: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  password: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  textInput: {
    color: PRIMARY_GRAY,
    borderBottomWidth: 1,
    marginTop: 20,
    marginBottom: 20,
  },
  fromFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  checkboxContainer: {
    flexDirection: 'row',
  },
  label: {
    marginTop: 5,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  forgot: {
    marginTop: 7,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
});
