import * as React from 'react';
import Footer from './component/Footer';
import From from './component/From';
import { connect } from "react-redux";

import {Text, View, Image, ScrollView, StyleSheet, TouchableOpacity} from 'react-native';



import styles from './styles';
import {Navigation} from "react-native-navigation";
import {
  selectLoginErrorMsg,
  selectNetworkError,
  selectOtpSendMsg,
  selectOtpVerifyStatus,
  selectToken,
  selectUser
} from "core/selectors/user";
import {login} from "core/actions/authActionCreators";
import {ACCOUNTYPE_EMAIL} from "core/constants/terms";
import {errorAlert, successAlert} from "web/src/utils/alerts";
import {ROUTE_HOME, ROUTE_VERIFY_OTP} from "web/src/routes/constants";
import {navigateToHome, startMainActivity} from "../../navigator";

const vector = require('./images/Vector.png');
const leftArrow = require('./images/leftArrow.png');
const rectangle = require('./images/Rectangle.png');

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      username: "",
      password: "",
      accountType: ACCOUNTYPE_EMAIL
    };
  }

  componentDidUpdate(prevProps) {

    if (this.props.isLoggedIn !== prevProps.isLoggedIn) {
      if (this.props.isLoggedIn) {
        // navigateToHome(this.props.componentId);
        startMainActivity();
      }
    }

    if (this.props.otpVerifyStatus !== prevProps.otpVerifyStatus) {
      if (this.props.otpVerifyStatus) {
        alert(this.props.otpSendMsg);
        // this.props.history.push(ROUTE_VERIFY_OTP);
      }
    }
    if (this.props.loginErrorMsg !== prevProps.loginErrorMsg) {
      if (this.props.loginErrorMsg !== "") {
        alert(this.props.loginErrorMsg);
        this.setState({
          loading: false
        });
      }
    }
  }

  onChangeInput = (key, value) => {
    this.setState( {
      [key]: value
    });
  }

  onPressButton = () => {
    if (this.state.username === "" || this.state.password === "") {
      alert("You should provide some credential!");
      return;
    }

    this.setState({
      loading: true
    });

    this.props.login(
      this.state.username,
      this.state.password,
      this.state.accountType
    );
    return true;

  }

  render() {
    const {username, password, loading} = this.state;
    return (
      <ScrollView>
      <View style={styles.container}>
        <View style={styles.headerWarpper}>
          <View>
            <TouchableOpacity
              style={styles.headTop}
              onPress={() => navigateToHome(this.props.componentId)}
            >
              <Image source={leftArrow} style={{width: 7, height: 12, marginRight: 5}} />
              <Text
                style={styles.backTo}
              >
                Back to main
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.headBottom}>
            <Image source={vector} style={{width: 25, height: 25, marginRight: 5}} />
            <Text style={styles.logIn}>Login</Text>
            <Image source={rectangle} style={styles.rectangle} />
          </View>
        </View>
        <View style={styles.bodyWarpper}>
          <From
            username={username}
            password={password}
            onChange={this.onChangeInput}
            componentId={this.props.componentId}/>
        </View>
        <View style={styles.footerWarpper}>
          <Footer
            loading={loading}
            onPressButton={this.onPressButton}
            componentId={this.props.componentId} />
        </View>
      </View>
      </ScrollView>
    );
  }
}

export default connect(
  state => ({
    user: selectUser(state),
    isLoggedIn: !!selectToken(state),
    auth: state.auth,
    networkError: selectNetworkError(state),
    otpSendMsg: selectOtpSendMsg(state),
    otpVerifyStatus: selectOtpVerifyStatus(state),
    loginErrorMsg: selectLoginErrorMsg(state)
  }),
  {
    login
  }
)(Login);



