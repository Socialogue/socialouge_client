import {StyleSheet} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from '../../utils/constants';


export default StyleSheet.create({
  container: {
    padding: 20,
    // marginBottom: 300
  },
  headerWrapper: {
    // marginVertical: 30
  },
  headTop: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  backTo: {
    fontSize: 14,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  },
  bodyWarpper: {
    // height: '50%',
    // justifyContent: 'center',
    // backgroundColor: 'green'
    marginTop: 30,
    marginBottom: 60
  },
  headBottom: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10
    // marginVertical: 30,
  },
  logIn: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    fontSize: 40,
    // marginBottom: ,
    marginRight: 3,
  },
  footerWarpper: {
    height: '30%'
    // justifyContent: 'flex-end',
    // backgroundColor: 'blue'
  },
});
