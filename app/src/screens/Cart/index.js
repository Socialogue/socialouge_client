import * as React from 'react';
import {Image, ScrollView, StyleSheet, Text, TouchableOpacity, View, TextInput} from 'react-native';

import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY, SECONDARY_ORANGE, LIGHT_GRAY, PRIMARY_WHITE} from "../../utils/constants";
import styles from './styles';


import NumericInput from 'react-native-numeric-input'
import PrimaryButton from "../../components/PrimaryButton";

import {navigateToCart, navigateToCheckOutAddress, navigateToSearch} from "../../navigator";
import {Navigation} from "react-native-navigation";



const cross = require('./images/cross.png');
const Info = require('./images/Info.png');
const coat = require('./images/coat.png');
const rightArrow = require('./images/rightArrow.png');


class Cart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }
  onPressButton = () => {
    this.setState({loading: true});
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerWrapper}>
          <Text style={{fontSize: 30, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK,}}>bag <Text style={{fontSize: 40, color: SECONDARY_ORANGE}}>.</Text></Text>
          <View style={{flexDirection: 'row', alignItems: 'center',}}>
            <Image source={Info} style={{width: 20, height: 20, marginTop: 10}}/>
            <TouchableOpacity
              onPress={() => Navigation.pop(this.props.componentId)}
            >
              <Image source={cross} style={{width: 22, height: 22, marginTop: 10, marginLeft: 10}}/>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.bodyWrapper}>
          <ScrollView>
            <View style={{paddingLeft: 20, paddingRight: 20}}>
              <View style={styles.cartItem}>
                <View style={{flexDirection: 'row'}}>
                  <View>
                    <Image source={coat} style={{width: 80, height: 116}}/>
                  </View>
                  <View style={{paddingHorizontal: 20}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', width: '80%'}}>
                      <View>
                        <Text style={styles.lebelText}>limited edition coat</Text>
                        <Text style={styles.subText}>by BUTIQ K</Text>
                      </View>
                      <View>
                        <Image source={cross} style={{width: 22, height: 22}}/>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '80%', marginTop: 40}}>
                      <NumericInput
                        type='plus-minus'
                        onChange={value => console.log(value)}
                        borderColor='#fff'
                      />

                      <Text style={styles.subText}>€ 240.00</Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.cartItem}>
                <View style={{flexDirection: 'row'}}>
                  <View>
                    <Image source={coat} style={{width: 80, height: 116}}/>
                  </View>
                  <View style={{paddingHorizontal: 20}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', width: '80%'}}>
                      <View>
                        <Text style={styles.lebelText}>limited edition coat</Text>
                        <Text style={styles.subText}>by BUTIQ K</Text>
                      </View>
                      <View>
                        <Image source={cross} style={{width: 22, height: 22}}/>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '80%', marginTop: 40}}>
                      <NumericInput
                        type='plus-minus'
                        onChange={value => console.log(value)}
                        borderColor='#fff'
                      />

                      <Text style={styles.subText}>€ 240.00</Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.cartItem}>
                <View style={{flexDirection: 'row'}}>
                  <View>
                    <Image source={coat} style={{width: 80, height: 116}}/>
                  </View>
                  <View style={{paddingHorizontal: 20}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', width: '80%'}}>
                      <View>
                        <Text style={styles.lebelText}>limited edition coat</Text>
                        <Text style={styles.subText}>by BUTIQ K</Text>
                      </View>
                      <View>
                        <Image source={cross} style={{width: 22, height: 22}}/>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '80%', marginTop: 40}}>
                      <NumericInput
                        type='plus-minus'
                        onChange={value => console.log(value)}
                        borderColor='#fff'
                      />

                      <Text style={styles.subText}>€ 240.00</Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.cartItem}>
                <View style={{flexDirection: 'row'}}>
                  <View>
                    <Image source={coat} style={{width: 80, height: 116}}/>
                  </View>
                  <View style={{paddingHorizontal: 20}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', width: '80%'}}>
                      <View>
                        <Text style={styles.lebelText}>limited edition coat</Text>
                        <Text style={styles.subText}>by BUTIQ K</Text>
                      </View>
                      <View>
                        <Image source={cross} style={{width: 22, height: 22}}/>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '80%', marginTop: 40}}>
                      <NumericInput
                        type='plus-minus'
                        onChange={value => console.log(value)}
                        borderColor='#fff'
                      />

                      <Text style={styles.subText}>€ 240.00</Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.cartItem}>
                <View style={{flexDirection: 'row'}}>
                  <View>
                    <Image source={coat} style={{width: 80, height: 116}}/>
                  </View>
                  <View style={{paddingHorizontal: 20}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', width: '80%'}}>
                      <View>
                        <Text style={styles.lebelText}>limited edition coat</Text>
                        <Text style={styles.subText}>by BUTIQ K</Text>
                      </View>
                      <View>
                        <Image source={cross} style={{width: 22, height: 22}}/>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '80%', marginTop: 40}}>
                      <NumericInput
                        type='plus-minus'
                        onChange={value => console.log(value)}
                        borderColor='#fff'
                      />

                      <Text style={styles.subText}>€ 240.00</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
        <View style={styles.footerWrapper}>
          <View style={{ padding: 5, borderWidth: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
            <View style={{flexDirection: 'row', borderBottomWidth: 1, alignItems: 'center', width: 120}}>
              <TextInput
                style={{width: 100}}
                placeholder="| Discount code"
              />
              <Image source={rightArrow} style={{width: 22, height: 22}}/>
            </View>
            <View>
              <Text style={{fontSize: 12, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK,}}>TOTAL <Text style={{fontSize: 16}}>€ 720.00</Text></Text>
            </View>
          </View>
          <PrimaryButton
            loading={this.props.loading}
            title='Proceed to checkout'
            // onPress={this.props.onPressButton}/>
            onPress={() => navigateToCheckOutAddress(this.props.componentId)}/>
        </View>
      </View>
    );
  }
}
export default Cart;

