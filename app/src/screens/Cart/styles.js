import {StyleSheet, Text} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY} from "../../utils/constants";
import * as React from "react";

export default StyleSheet.create({
  container: {
    flex: 10,
    // backgroundColor: 'red'

  },
  headerWrapper: {
    height: 60,
    borderBottomWidth: 1,
    paddingBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 20,
    marginRight: 20,

  },
  bodyWrapper: {
    flex: 7,
    // backgroundColor: 'green'
  },
  lebelText: {
    fontSize: 14,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    textTransform: 'uppercase',
    marginBottom: 10
  },
  subText: {
    fontSize: 13,
    color: PRIMARY_BLACK,
    fontFamily: GROTESK_SEMIBOLD,
  },
  cartItem: {
    paddingVertical: 20,
    borderBottomWidth: 1
  },
  footerWrapper: {
    height: 113,
    // backgroundColor: 'red'
    marginLeft: 20,
    marginRight: 20,
  },
});
