import * as React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Onboarding from 'react-native-onboarding-swiper';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../utils/constants";
import {connect} from "react-redux";
import {selectUser} from "core/selectors/user";
import {swiperShown} from "../../redux/actionCreators/appConfigActionCreators";
import {startLoginActivity} from "../../navigator";
import styles from './styles';

const rightArrow = require('./images/rightArrow.png');


class Swiper extends React.Component {

  onPressSkip = () => {
    this.props.swiperShown(true);
    startLoginActivity();
  }

  render() {
    return (
      <View style={styles.container}>
        <Onboarding
          bottomBarColor='#fff'
          showSkip={false}
          showNext={false}
          onDone={this.onPressSkip}
          pages={[
            {
              backgroundColor: '#fff',
              // image: <Image source={require('./images/circle.png')} />,
              title: 'unique eco-brands.',
              subtitle: '',
            },
            {
              backgroundColor: '#fff',
              // image: <Image source={require('./images/circle.png')} />,
              title: 'shop.',
              subtitle: '',
            },
            {
              backgroundColor: '#fff',
              // image: <Image source={require('./images/circle.png')} />,
              title: 'sell.',
              subtitle: '',
            },
            {
              backgroundColor: '#fff',
              // image: <Image source={require('./images/circle.png')} />,
              title: 'connect.',
              subtitle: '',
            }
          ]}
        />
        <View style={{
          position: 'absolute',
          top: 20,
          right: 20
        }}>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}
            onPress={this.onPressSkip}
          >
            <Text style={{
              fontFamily: GROTESK_SEMIBOLD,
              color: PRIMARY_BLACK,
              marginRight: 5
            }}>Skip</Text>
            <Image source={rightArrow} style={{width: 20, height: 15, marginRight: 5}} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}


export default connect(
  state => ({
    user: selectUser(state),
  }),
  {
    swiperShown
  }
)(Swiper);

