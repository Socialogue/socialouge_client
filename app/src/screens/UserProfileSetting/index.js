import * as React from 'react';
import {Text, View, Image, TouchableOpacity, ScrollView} from 'react-native';

import styles from './styles';
import ProfileTab from './component/ProfileTab';
import {Navigation} from "react-native-navigation";
import {navigateToAppMenu, navigateToCart} from "../../navigator";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../utils/constants";
import CountryPicker from 'react-native-country-picker-modal';
import {errorAlert, successAlert} from "web/src/utils/alerts";
import {getMonthIndex} from "core/utils/dummyDate";
import {selectPosts} from "core/selectors/posts";
import {selectErrorMsg, selectModalType, selectSuccessMsg} from "core/selectors/settings";
import {
  deleteAccount, getUserData, updateDeliveryData,
  updateEmailData,
  updatePasswordData,
  updatePersonalData
} from "core/actions/userSettingsActionCreators";
import { connect } from "react-redux";


const arrowLeft = require('./images/arrowLeft.png');
const cart = require('./images/cart.png');

const menubar = require('../../images/menubar.png');


class UserProfile extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <View style={styles.container}>
        <View style={styles.headerWarpper}>
          <View>
            <TouchableOpacity
              style={{ paddingVertical: 10, flexDirection: 'row', alignItems: 'center'}}
              onPress={() => Navigation.pop(this.props.componentId)}>

              <Image source={arrowLeft} style={{width: 25, height: 15}} />
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK, textTransform: 'uppercase'}}>User Profile</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.iconPart}>
            <TouchableOpacity
              style={{paddingRight: 20, paddingVertical: 10, paddingLeft: 10}}
              onPress={() =>navigateToCart(this.props.componentId)}>
              <Image source={cart} style={{width: 22, height: 22}} />
            </TouchableOpacity>
            <TouchableOpacity
              style={{paddingRight: 20, paddingVertical: 10}}
              onPress={() => navigateToAppMenu(this.props.componentId)}
            >
              {/*<Header componentId={this.props.componentId}></Header>*/}
              <Image source={menubar} style={{width: 25, height: 25}} />
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.bodyWarpper}>
          <ScrollView>
            <ProfileTab
              componentId={this.props.componentId} />
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default UserProfile;
