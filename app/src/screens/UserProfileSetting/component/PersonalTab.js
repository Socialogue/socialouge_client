import * as React from 'react';
import {Text, View, TextInput, StyleSheet, Image, TouchableOpacity, StatusBarIOS} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, SECONDARY_ORANGE} from "../../../utils/constants";
import DropDownPicker from "react-native-dropdown-picker";
import {Navigation} from "react-native-navigation";
import { Dropdown } from 'react-native-material-dropdown';
import CountryPicker from 'react-native-country-picker-modal';
import {connect} from "react-redux";
import {selectPersonalData} from "core/selectors/settings";
import {
  deleteAccount, getUserData, updateDeliveryData,
  updateEmailData,
  updatePasswordData,
  updatePersonalData
} from "core/actions/userSettingsActionCreators";
import SimpleInput from "../../../components/SimpleInput";


const arrowLeft = require('../images/arrowLeft.png');
const downArrow = require('../../../images/downArrow.png');


class PersonalTab extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      view: "personal",
      nickname: "",
      gender: "",
      fullName: "",
      email: "" ,
      country: "LT",
      isCountryVisible: false,
      phone: "",
      hasCalledUpdate: false
    };
  }

  onChangeInput = (key, value) => {
    this.setState({
      [key]: value
    });
  }

  onSelectCountry = (data) => {
    this.setState({country: data.cca2, countryName: data.name, isCountryVisible: false});
  }
  onClose = () => {
    this.setState({isCountryVisible: false});
  }

  //no need to call any api , already has from parent view UserProfile/index
  componentDidMount = () => {
    // this.props.getUserData();
    console.log("PersonalTab : "+JSON.stringify(this.props.personalInfo));
    if (!this.state.hasCalledUpdate) {

      const { personalInfo } = this.props;
      this.setState({
        hasCalledUpdate: true,
        nickname: personalInfo.nickName,
        gender: personalInfo.gender,
        fullName: personalInfo.fullName,
        email: personalInfo.email,
        country: personalInfo.country,
        phone: personalInfo.phone,
      });

    }
  };

// Save button action
  onSubmit = () => {
    // console.log(this.state)
    //

    // check all mandatory field
    if (this.state.nickname === "") {
      alert('please put all required information');
      return;
    }

    // submit the form by calling action creator
    try {
      const personal = {
        nickName: this.state.nickname,
        gender: this.state.gender,
        fullName: this.state.fullName,
        country: this.state.country
      };
      this.props.updatePersonalData(personal);
    } catch (e) {
      console.log("catch", e);
    }

  };

  gotPersonalData = (fullName, value) => {
    this.setState({
      [fullName]: value
    });
  };

  gotEmailData = (key, value) => {
    this.setState({
      [key]: value
    });
  };

  gotDeliveryData = (key, value) => {
    this.setState({
      [key]: value
    });
  };

  deleteAccount = () => {
    console.log("account delete.");
    this.props.deleteAccount();
  };

  emailOrPass = value => {
    this.setState({
      mailOrPass: value
    });
  };

  //check current gender
  checkMatchSex(current, option) {
    console.log(current === option);
    return current === option;
  }

  render() {
    console.log('full name', this.props.name)

     console.log(JSON.stringify(this.props.personalInfo));
    const {nickname, gender, fullName , country,  phone, email, isCountryVisible} = this.state;

    console.log("Gender: "+ gender);

    return (
      <View style={styles.container}>
        <Text style={styles.personalTitle}>
          PERSONAL settings</Text>
        <SimpleInput
          label="Nickname "
          placeholder="Nick Name"
          value={nickname}
          isRequired={true}
          onChange={ e => this.onChangeInput('nickname', e)}/>
        <View style={styles.content}>
          <View  style={styles.lebel}>
            <Text style={styles.lebelText}>Gender <Text style={{color: SECONDARY_ORANGE}}>*</Text></Text>
          </View>
          <View>
            <View style={{marginTop: 20}}></View>
            { gender !== "" &&
            <DropDownPicker
              style={styles.drop}
              value={gender}
              items={[
                {label: 'Male', value: 'Male', selected: this.checkMatchSex(gender,"Male")},
                {label: 'Female', value: 'Female', selected: this.checkMatchSex(gender,"Female")},
                {label: 'Others', value: 'Others', selected: this.checkMatchSex(gender,"Others")},
              ]}
              defaultIndex={0}
              showArrow={true}
              containerStyle={{height: 40}}
              onChangeItem={ item => this.setState({gender: item.value}) }
            />
            }

            { gender == "" &&
            <DropDownPicker
              style={styles.drop}
              value={gender}
              items={[
                {label: 'Male', value: 'Male',},
                {label: 'Female', value: 'Female', },
                {label: 'Others', value: 'Others',},
              ]}
              defaultIndex={0}
              showArrow={true}
              containerStyle={{height: 40}}
              onChangeItem={ item => this.setState({gender: item.value}) }
            />
            }

          </View>
        </View>
        <SimpleInput
          label="FULL Name "
          placeholder="Full Name"
          value={fullName}
          isRequired={true}
          onChange={ e => this.onChangeInput('fullName', e)}/>

        <View style={styles.content}>
          <View  style={styles.lebel}>
            <Text style={styles.lebelText}>country <Text style={{color: SECONDARY_ORANGE}}>*</Text></Text>
          </View>
          <View>
            <TouchableOpacity
              activeOpacity={1}
              style={{flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, paddingVertical: 10, paddingTop: 20}}
              onPress={() => this.setState({isCountryVisible: true})}
              >
              <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%'}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <CountryPicker
                    countryCode={country}
                    withFlag
                    onSelect={this.onSelectCountry}
                    visible={isCountryVisible}
                    onClose={this.onClose}
                  />
                  <Text style={{color: PRIMARY_BLACK}}>{country}</Text>
                </View>
                <View>
                  <Image source={downArrow} style={{width: 10, height: 6, marginRight: 10}} />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>

        <SimpleInput
          label="Phone number "
          placeholder="+ 370 23 234 234"
          value={phone}
          isRequired={true}
          isEditable={false}
          onChange={ e => this.onChangeInput('phone', e)}/>

        <SimpleInput
          label="Email address "
          placeholder="Name@domain.com"
          value={email}
          isRequired={true}
          isEditable={false}
          onChange={ e => this.onChangeInput('email', e)}/>

        <View style={{ paddingVertical: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
          <View>

          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity
              style={{ paddingVertical: 10, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 20}}
              onPress={() => Navigation.pop(this.props.componentId)}>
              <Image source={arrowLeft} style={{width: 25, height: 15, marginRight: 5}} />
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{width: 100, height: 30, borderWidth: 1, justifyContent: 'center', alignItems: 'center'}}
              onPress={this.onSubmit}>
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>Save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    personalInfo: selectPersonalData(state)
  }),
  {
    updatePersonalData,
    updateEmailData,
    updatePasswordData,
    deleteAccount,
    updateDeliveryData,
    getUserData
  }
)(PersonalTab);

const styles = StyleSheet.create({
  container: {
    // paddingVertical: 20
  },
  content: {
    marginVertical: 15
  },
  lebelText: {
    fontSize: 14,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    textTransform: 'uppercase'
  },
  input: {
    borderBottomWidth: 1,
    width: '100%'
  },
  personalTitle: {
    fontSize: 14,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    textTransform: 'uppercase',
    marginBottom: 10
  }
});
