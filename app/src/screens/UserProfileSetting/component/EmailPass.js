import * as React from 'react';
import {Text, View, TextInput, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../../utils/constants";
import SimpleInput from "../../../components/SimpleInput";
import {Navigation} from "react-native-navigation";
const arrowLeft = require('../images/arrowLeft.png');
import {connect} from "react-redux";
import {
  deleteAccount, getUserData, updateDeliveryData,
  updateEmailData,
  updatePasswordData,
  updatePersonalData
} from "core/actions/userSettingsActionCreators";

import {selectPersonalData} from "core/selectors/settings";

class EmailPass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      accountEmailAddress: "",
      newEmailAddress: "",
      oldPass: "",
      newPass:"",
      confirmNewPass: "",
    };
  }

componentDidMount() {
  console.log("EmailData: "+ JSON.stringify(this.props.emailData));
  this.setState({
    accountEmailAddress: this.props.emailData.email,
    oldPass: "",
    newPass:"",
    confirmNewPass: "",
  });
}

onChangeEmailAddress = () => {
  if (this.state.accountEmailAddress == "" || this.state.newEmailAddress == "") {
    alert("Please fill up the form");
    return;
  }

//console.log("email "+this.state.accountEmailAddress + " "+"newEmain "+this.state.newEmailAddress);

  try {
    const emailData = {
      email: this.state.accountEmailAddress,
      newEmail: this.state.newEmailAddress
    };
    this.props.updateEmailData(emailData);
  } catch (e) {
    console.log("catch", e);
  }
}



onSubmit = () => {

// if active tab is email and password(change password)...

    if (
      this.state.oldPass == "" ||
      this.state.newPass == "" ||
      this.state.confirmNewPass == ""
    ) {
      alert("Please fill up the form");
      return;
    }
console.log("pass "+ this.state.newPass + " "+ "confirm pass "+ this.state.confirmNewPass);
    if (this.state.newPass !== this.state.confirmNewPass) {
      alert("New password and confirm password not match!");
      return;
    }
    try {
      const passwordData = {
        currentPassword: this.state.oldPass,
        newPassword: this.state.newPass,
        confirmPassword: this.state.confirmNewPass
      };
      this.props.updatePasswordData(passwordData);
    } catch (e) {
      console.log("catch", e);
    }

};

onChangeInput = (key, value) => {
  this.setState({
    [key]: value
  });
}

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.emailTitle}>EMAIL & PASSWORD</Text>
        <SimpleInput
          label="ACCOUNT EMAIL ADDRESS "
          placeholder="example@email.com"
          value={this.state.accountEmailAddress}
          onChange={ e => this.onChangeInput('accountEmailAddress', e)}
        />
        <SimpleInput
          label="Enter New EMAIL ADDRESS "
          placeholder="example@email.com"
          value={this.state.newEmailAddress}
          onChange={ e => this.onChangeInput('newEmailAddress', e)}
        />
        <View style={{width:'100%',flexDirection: 'row-reverse'}}>
          <TouchableOpacity
              style={{ height: 30, borderWidth: 1, padding: 10,justifyContent: 'center', alignItems: 'center'}}
              onPress={this.onChangeEmailAddress}>
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>Update Email Address</Text>
            </TouchableOpacity>
        </View>
        <SimpleInput
          label="CHANGE PASSWORD"
          placeholder="OLD PASSWORD"
          value={this.state.oldPass}
          onChange={ e => this.onChangeInput('oldPass', e)}
        />


        <SimpleInput
          style={styles.input}
          placeholder="NEW PASSWORD"
          value={this.state.newPass}
          onChange={ e => this.onChangeInput('newPass', e)}
        />
        <SimpleInput
          style={styles.input}
          placeholder="REPEAT NEW PASSWORD"
          value={this.state.confirmNewPass}
          onChange={ e => this.onChangeInput('confirmNewPass', e)}
        />
        
        <View style={{ paddingVertical: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
          <View>

          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
           
            <TouchableOpacity
              style={{ height: 30, borderWidth: 1, padding: 10, justifyContent: 'center', alignItems: 'center'}}
              onPress={this.onSubmit}>
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>Update Password</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.deleteAcc}>DELETE ACCOUNT</Text>
          <Text style={styles.delete}>DELETE</Text>
        </View>
      </View>
    );
  }
}


export default connect(
  state => ({
    emailData: selectPersonalData(state)
  }),
  {
    updatePersonalData,
    updateEmailData,
    updatePasswordData,
    deleteAccount,
    updateDeliveryData,
    getUserData
  }
)(EmailPass);


const styles = StyleSheet.create({
  container: {

  },
  emailInputTitle: {
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  emailTitle: {
    fontSize: 14,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    textTransform: 'uppercase',
    marginBottom: 10
  },
  input: {
    borderBottomWidth: 1,
    marginBottom: 20
    // width: 200
  },
  deleteAcc: {
    fontSize: 14,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    textTransform: 'uppercase',
    marginVertical: 10
  },
  delete: {
    fontSize: 14,
    fontFamily: GROTESK_SEMIBOLD,
    color: 'red',
    textTransform: 'uppercase',
    marginVertical: 10
  }
});
