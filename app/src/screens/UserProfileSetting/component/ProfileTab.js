import * as React from 'react';
import { Image, StyleSheet, Text, View, ScrollView, TouchableOpacity } from "react-native";

import PersonalTab from '../component/PersonalTab';
import EmailPass from '../component/EmailPass';
import DeliveryTab from '../component/DeliveryTab';
import ReviewTab from '../../ProductPage/component/ReviewTab';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../../utils/constants";
import Header from "../../../components/Header";

class ProfileTab extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      activetab: 'a'
    };
  }

  render() {
    const {activetab} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.tabButton}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.setState({activetab: 'a'})}>
            <Text
              style={
                activetab === 'a'
                  ? styles.activeButtonText
                  : styles.inActiveButtonText
              }>
              PERSONAL
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.setState({activetab: 'b'})}>
            <Text
              style={
                activetab === 'b'
                  ? styles.activeButtonText
                  : styles.inActiveButtonText
              }>
              EMAIL & PASSWORD
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.setState({activetab: 'c'})}>
            <Text
              style={
                activetab === 'c'
                  ? styles.activeButtonText
                  : styles.inActiveButtonText
              }>
              DELIVERY
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.tabContent}>
          {activetab === 'a' &&
          <PersonalTab
            componentId={this.props.componentId} />
          }
          {activetab === 'b' && <EmailPass componentId={this.props.componentId} />}
          {activetab === 'c' && <DeliveryTab componentId={this.props.componentId} />}
        </View>
      </View>
    );
  }
}

export default ProfileTab;

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  tabButton: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderBottomWidth: 1,
    marginBottom: 20,
  },

  activeButtonText: {
    color: '#000',
    borderBottomWidth: 3,
    paddingBottom: 5,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  inActiveButtonText: {
    color: '#868686',
  },
});
