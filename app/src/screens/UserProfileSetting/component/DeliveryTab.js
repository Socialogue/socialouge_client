import * as React from 'react';
import {Text, View, TextInput, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, SECONDARY_ORANGE} from "../../../utils/constants";
import CheckBox from "@react-native-community/checkbox";
import SimpleInput from "../../../components/SimpleInput";
import CountryPicker from "react-native-country-picker-modal";
import {Navigation} from "react-native-navigation";

const downArrow = require('../../../images/downArrow.png');
const arrowLeft = require('../images/arrowLeft.png');
import {connect} from "react-redux";
import {
  deleteAccount, getUserData, updateDeliveryData,
  updateEmailData,
  updatePasswordData,
  updatePersonalData,
  addDeliveryData,
  getDeliveryData
} from "core/actions/userSettingsActionCreators";

import {selectDeliveryData} from "core/selectors/settings";

//{"softDelete":{"status":0},"role":1,"verified":true,"totalFollowers":0,"createdAt":"2021-05-06T21:45:41.333Z",
//"_id":"607c4f4509506b29103206bb",
//"name":"test","firstName":"Lukas",
//"lastName":"Paucek","username":"test1@email.com",
//"email":"test1@email.com",
//"password":"$2b$12$ixDhJistk89lzbDimdbyguPAelwH5AQ6MEExdYbAzPYzF3zeErY3m",
//"profileImage":"http://placeimg.com/640/480","coverImage":"http://placeimg.com/640/480",
//"accountType":1,"address":"","companyAddress":"","country":"LT",
//"gender":"Female","nickName":"Hh",
//"phone":"1234%","sureName":"","zipCode":"","delivery":[],"likedPost":[],"followers":[],"following":[],"shopFollowing":[],"favoriteProduct":[],"collections":[]}



class DeliveryTab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      sureName: "",
      address:"",
      zipCode: "",
      email:"",
      phone: "",
      country: "LT",
      billing: 0,
      isCountryVisible: false,
      hasloadedData: false,
      deliveryArrayIndex: -1,
      deliveryId: null

    };
    
  }

componentDidMount() {
  console.log("compnentDidMount calLED");
  this.props.getDeliveryData();
  // console.log("DeliveryData : "+ JSON.stringify(this.props.delivrData));
  // this.setState(
  //   {
  //     name: this.props.delivrData.name,
  //     sureName: this.props.delivrData.sureName,
  //     address: this.props.delivrData.address,
  //     zipCode: this.props.delivrData.zipCode,
  //     email: this.props.delivrData.email,
  //     phone: this.props.delivrData.phone,
  //     country: this.props.delivrData.country,
  //     isCountryVisible: false,
  //   }
  // );
}

componentDidUpdate() {
if(!this.state.hasloadedData) {
  this.setState({
    hasloadedData: true
  });
}


 // console.log("DeliveryData : "+ JSON.stringify(this.props.delivrData));

console.log("componentDidUpdate Called");

}

onChangeInput = (key, value) => {
  this.setState({
    [key]: value
  });
}

onSubmit = () => {
  if (
    this.state.name == "" ||
    this.state.sureName == "" ||
    this.state.country == "" ||
    this.state.address == "" ||
    this.state.zipCode == "" ||
    this.state.email == "" ||
    this.state.phone == ""
  ) {
    alert("Please fill up the form");
    return;
  }
  // const monthIndex = getMonthIndex(this.state.birthmonth);
  // const birthday = new Date(
  //   this.state.birthyear,
  //   monthIndex,
  //   this.state.birthday
  // );

  try {
    const delivery = {
      name: this.state.name,
      sureName: this.state.sureName,
      country: this.state.country,
      address: this.state.address,
      zipCode: this.state.zipCode,
      billing: this.state.billing,
      phone: this.state.phone,
      email: this.state.email,
      id:  this.state.deliveryId
    };
    console.log("deld: ", delivery);
    if (this.state.deliveryId != null) {
      this.props.updateDeliveryData(delivery);
    } else {
      this.props.addDeliveryData(delivery);
    }
  } catch (e) {
    console.log("catch", e);
  }

}


  onSelectCountry = (data) => {
    this.setState({country: data.cca2, countryName: data.name, isCountryVisible: false});
  }
  onClose = () => {
    this.setState({isCountryVisible: false});
  }

 deliveryAddressCheckBoxOnClick = (item,index) => {
  //console.log("ITMS Selected "+ index + "item :"+ JSON.stringify(item));
  if(this.state.deliveryArrayIndex === index) {
    this.setState({
      name: "",
      sureName: "",
      address: "",
      zipCode: "",
      email: "",
      phone: "",
      country: "",
      billing: "",
      isCountryVisible: false,
      deliveryArrayIndex: -1,
      hasloadedData: true,
      deliveryId: null
      //deliveryAddress: item
    });
    return;
  }
  this.setState({
    name: item.name,
    sureName: item.surname,
    address: item.address,
    zipCode: item.zipCode,
    email: item.email,
    phone: item.phone,
    country: item.country == 'UK' ? "GB" : item.country,
    billing: item.billing,
    isCountryVisible: false,
    deliveryArrayIndex: index,
    hasloadedData: true,
    deliveryId: item._id
    //deliveryAddress: item
  });
}



  render() {
    const {country, name, email,phone,sureName,zipCode,address,billing, isCountryVisible} = this.state;


console.log("Country "+country+"name "+name+"email "+email+"sureName "+sureName+"zipcode "+zipCode+"address "+address+"phone "+phone+"isCountryVisible "+isCountryVisible);



    return (
      <View style={styles.container}>
      {
          this.props.delivrData.map(
            (item,index) => (
              <View style={styles.checkboxContainer}>
          <CheckBox
          value={this.state.deliveryArrayIndex === index}
            style={styles.checkBox}
            
            onValueChange={(newValue) => this.deliveryAddressCheckBoxOnClick(item,index)}
          />
          <Text style={styles.billingTitle}>{item.address}</Text>
        </View>
            )
          )
        }
        <Text style={styles.deliveryTitle}>DELIVERY INFORMATION</Text>
        <View style={styles.checkboxContainer}>
          <CheckBox
          value={billing === 1}
            style={styles.checkBox}
            onValueChange={(newValue) => this.setState({billing: billing == 0 ? 1 : 0})}
          />
          <Text style={styles.billingTitle}>Use a billing address</Text>
        </View>
        
        <SimpleInput
          label="NAME"
          placeholder="Your name"
          value={name}
          onChange={ e => this.onChangeInput('name', e)}
        />
        <SimpleInput
          label="SURENAME"
          placeholder="Your surename"
          value={sureName}
          onChange={ e => this.onChangeInput('sureName', e)}
        />

        <View style={styles.content}>
          <View  style={styles.lebel}>
            <Text style={styles.deliveryTitle}>country</Text>
          </View>
          <View>
            <TouchableOpacity
              activeOpacity={1}
              style={{flexDirection: 'row', alignItems: 'center', borderBottomWidth: 1, paddingVertical: 10, paddingTop: 20}}
              onPress={() => this.setState({isCountryVisible: true})}
            >
              <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%'}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <CountryPicker
                    countryCode={country}
                    withFlag
                    onSelect={this.onSelectCountry}
                    visible={isCountryVisible}
                    onClose={this.onClose}
                  />
                  <Text style={{color: PRIMARY_BLACK}}>{country}</Text>
                </View>
                <View>
                  <Image source={downArrow} style={{width: 10, height: 6, marginRight: 10}} />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>

        <SimpleInput
          label="ADDRESS"
          placeholder="Your address"
          value={address}
          onChange={ e => this.onChangeInput('address', e)}
        />
        <SimpleInput
          label="ZIP CODE"
          placeholder="Your zip code"
          value={zipCode}
          onChange={ e => this.onChangeInput('zipCode', e)}
        />
         <SimpleInput
          label="YOUR PHONE NUMBER"
          placeholder="Your phone number"
          value={phone}
          onChange={ e => this.onChangeInput('phone', e)}
        /> 
        <SimpleInput
          label="YOUR EMAIL ADDRESS"
          placeholder="Your email address"
          value={email}
          onChange={ e => this.onChangeInput('email', e)}
        />

        <View style={{ paddingVertical: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
          <View>

          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity
              style={{ paddingVertical: 10, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 20}}
              onPress={() => Navigation.pop(this.props.componentId)}>
              <Image source={arrowLeft} style={{width: 25, height: 15, marginRight: 5}} />
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{width: 100, height: 30, borderWidth: 1, justifyContent: 'center', alignItems: 'center'}}
              onPress={this.onSubmit}>
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>Save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}


export default connect(
  state => ({
    delivrData: selectDeliveryData(state)
  }),
  {
    updatePersonalData,
    updateEmailData,
    updatePasswordData,
    deleteAccount,
    updateDeliveryData,
    getUserData,
    addDeliveryData,
    getDeliveryData
  }
)(DeliveryTab);


const styles = StyleSheet.create({
  container: {

  },
  deliveryTitle: {
    fontSize: 14,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    textTransform: 'uppercase',
    marginBottom: 10
  },
  checkboxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10
  },
  billingTitle: {
    fontSize: 14,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    textTransform: 'uppercase',

  },
  content: {
    marginVertical: 15
  },
});
