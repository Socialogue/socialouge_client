import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 100
  },
  headerWarpper: {
    // flex: 8,
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    // paddingRight: 20,
    paddingLeft: 15,
    borderBottomWidth: 1,
  },

  iconPart: {
    flexDirection: 'row',
  },
  bodyWarpper: {
    flex: 90
  },



});
