import * as React from 'react';
import {ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';
import Header from "../../components/Header";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, SECONDARY_ORANGE} from "../../utils/constants";
import DropDownPicker from "react-native-dropdown-picker";
import PhoneInput from "react-native-phone-number-input";
import {selectUser} from "core/selectors/user";
import {selectErrorMsg, selectSuccessMsg} from "core/selectors/shops";
import {applyForShop} from "core/actions/shopActionCreators";
import {connect} from "react-redux";
import {errorAlert, successAlert} from "web/src/utils/alerts";
import {ROUTE_HOME} from "web/src/routes/constants";
import SimpleInput from '../../components/SimpleInput';
import {Navigation} from "react-native-navigation";
import {navigateToCart, navigateToHome} from "../../navigator";

class ApplyShop extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shopName: "",
      itemForSell: "",
      countryCode: "",
      phone: "",
      companyName:"",
      companyId:"",
      companyVat:"",
      companyEmail:"",
      registeredCompanyAddress:"",
      socialMedia1: "",
      socialMedia2: "",
      whereDidYouHere:""
    };
  }

  componentDidMount = () =>{

  }

  onInputChange = (key, value) => {
    this.setState({
      [key]: value
    });
  };

  componentDidUpdate = prevProps => {
    if (prevProps.successMsg !== this.props.successMsg) {
      if (this.props.successMsg !== "") {
        alert(this.props.successMsg);
        navigateToHome(this.props.componentId);
      }
    }
    if (prevProps.errorMsg !== this.props.errorMsg) {
      if (this.props.errorMsg !== "") {
        alert(this.props.errorMsg);
      }
    }
  };

  validateEmail = (email) => {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
  }

  onSubmit = () => {
    if (
      this.state.companyEmail == "" ||
      this.state.companyId == "" ||
      this.state.companyName == "" ||
      this.state.countrycode == "" ||
      this.state.itemForSell == "" ||
      this.state.phone == "" ||
      this.state.registeredCompanyAddress == "" ||
      this.state.shopName == "" ||
      this.state.socialMedia1 == "" ||
      this.state.whereDidYouHere == "" ||
      this.state.userId == ""
    ) {
      alert("Please fill up form properly.");
      return;
    }

    if(!this.validateEmail(this.state.companyEmail)) {
      alert('PLease enter valid email address');
      return;
    }

    try {
      this.props.applyForShop(this.state);
    } catch (e) {
      console.log("catch", e);
    }
    return true;
  };
  render() {
    // console.log('apply sghop cinponentns props', this.props);
    const {
      shopName,
      itemForSell,
      phoneWithoutCode,
      companyId,
      companyName,
      companyEmail,
      companyVat, registeredCompanyAddress,
      socialMedia1, socialMedia2, whereDidYouHere} = this.state;
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.headerWrapper}>
            <Header componentId={this.props.componentId} />
          </View>
          <View style={styles.bodyWrapper}>
            <View style={{borderBottomWidth: 1, paddingBottom: 10}}>
              <Text style={{fontSize: 28, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK,}}>apply for a shop<Text style={{fontSize: 40, color: SECONDARY_ORANGE}}>.</Text></Text>
            </View>
            <Text style={{fontSize: 20, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK, marginVertical: 10}}>tell us a bit about your shop</Text>
            <SimpleInput
              label="your shop name"
              placeholder="Enter your shop name"
              value={shopName}
              isRequired={true}
              onChange={e => this.onInputChange('shopName', e)}/>
            <View style={styles.content}>
              <View  style={styles.lebel}>
                <Text  style={styles.lebelText}>what items do you sell<Text style={{color: SECONDARY_ORANGE}}>*</Text></Text>
              </View>
              <View style={{marginTop: 20}}>
                <DropDownPicker
                  style={styles.input}
                  items={[
                    {label: 'My own design', value: 'My own design'},
                    {label: 'Others design', value: 'Others design'},
                  ]}
                  defaultIndex={0}
                  showArrow={true}
                  containerStyle={{height: 40}}
                  value={itemForSell}
                  onChangeItem={ item => this.setState({itemForSell: item.value}) }
                />
              </View>
            </View>
            <View style={styles.content}>
              <View style={styles.lebel}>
                <Text style={styles.lebelText}>your contact number<Text style={{color: SECONDARY_ORANGE}}>*</Text></Text>
              </View>
              <View>
                <PhoneInput
                  // style={styles.input}
                  containerStyle={{
                    backgroundColor: 'none',
                    borderBottomWidth: 1,
                    marginBottom: 10,
                    height: 75,
                    marginVertical: 5,
                    width: '100%'
                  }}
                  defaultCode="LT"
                  value={phoneWithoutCode}
                  onChangeText={ (text) => this.onInputChange('phoneWithoutCode', text)}
                  onChangeFormattedText={(text) => this.onInputChange('phone', text)}
                />
              </View>
            </View>
            <SimpleInput
              label="company name"
              placeholder="Enter your company name"
              value={companyName}
              isRequired={true}
              onChange={e => this.onInputChange('companyName', e)}/>

            <SimpleInput
              label="company id "
              placeholder="Enter your company code"
              value={companyId}
              isRequired={true}
              // find={true}
              findName="Find code"
              onChange={ e => this.onInputChange('companyId', e)}/>

            <SimpleInput
              label="Company Email address"
              placeholder="Name@domain.com"
              value={companyEmail}
              isRequired={true}
              onChange={e => this.onInputChange('companyEmail', e)}/>
            <SimpleInput
              label="company Vat"
              placeholder="if available"
              value={companyVat}
              isRequired={true}
              onChange={ e => this.onInputChange('companyVat', e)}/>

            <SimpleInput
              label="Registered company address "
              placeholder="Little George St, Westminster, London"
              value={registeredCompanyAddress}
              isRequired={true}
              // find={true}
              findName="Find address"
              onChange={ e => this.onInputChange('registeredCompanyAddress', e)}/>
            <View style={styles.content}>
              <View style={styles.lebel}>
                <Text style={styles.lebelText}>social media<Text style={{color: SECONDARY_ORANGE}}>*</Text></Text>
              </View>
              <View>
                <TextInput
                  style={styles.input}
                  placeholder="Enter a link"
                  value={socialMedia1}
                  onChangeText={ e => this.onInputChange('socialMedia1', e)}
                />
                <TextInput
                  style={styles.input}
                  placeholder="Enter a link"
                  value={socialMedia2}
                  onChangeText={ e => this.onInputChange('socialMedia2', e)}
                />
              </View>
            </View>
            <View style={styles.content}>
              <View  style={styles.lebel}>
                <Text  style={styles.lebelText}>where did you hear about us <Text style={{color: SECONDARY_ORANGE}}>*</Text></Text>
              </View>
              <View style={{marginTop: 20}}>
                <DropDownPicker
                  style={styles.input}
                  items={[
                    {label: 'Google', value: 'Google'},
                    {label: 'Facebook', value: 'Facebook'},
                    {label: 'Linkedin', value: 'Linkedin'},
                  ]}
                  defaultIndex={0}
                  showArrow={true}
                  containerStyle={{height: 40}}
                  value={whereDidYouHere}
                  onChangeItem={ item => this.setState({whereDidYouHere: item.value}) }
                />
              </View>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'flex-end', marginVertical: 30}}>
              <TouchableOpacity style={styles.button} onPress={this.onSubmit}>
                <Text style={styles.buttonText}>Submit</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default connect(
  state => ({
    userId: selectUser(state),
    successMsg: selectSuccessMsg(state),
    errorMsg: selectErrorMsg(state)
  }),
  {
    applyForShop
  }
)(ApplyShop);

const styles = StyleSheet.create({
  container: {
    flex: 10
  },
  headerWrapper: {
    height: 60
  },
  bodyWrapper: {
    padding: 20
  },
  button: {
    width: 120,
    height: 35,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    // flexDirection: 'row',
    // alignItems: 'center',
    // justifyContent: 'space-between',
    marginVertical: 20
  },
  imageUpload: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 20
  },
  input: {
    borderBottomWidth: 1,
    // width: 200
  },
  lebel: {
    // width: '36%'
  },
  lebelText: {
    fontSize: 14,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    textTransform: 'uppercase'
  },
  buttonText: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  }

});
