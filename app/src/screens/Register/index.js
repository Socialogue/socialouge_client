import * as React from 'react';
import Footer from './component/Footer';
import Form from './component/Form';

import {
  authenticateByFacebook,
  authenticateByGoogle,
  register
} from "core/actions/authActionCreators";
import { connect } from "react-redux";

import {Text, View, Image, ScrollView} from 'react-native';

import styles from './styles';

import {
  selectLoginStatus,
  selectRegisterErrorMessage,
  selectRegisterProccessing,
  selectRegistrationStatus,
  selectToken
} from "core/selectors/user";
import {navigateToHome, navigateToOtpInput} from "../../navigator";

const vector = require('./images/Vector.png');
const dropdown = require('./images/leftArrow.png');
const rectangle = require('./images/Rectangle.png');

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      user: '',
      number: '',
      loading: false,
      numberWithCode: '',
      password: '',
      activeTab: 'phone',
      privacy: false
    };
  }

  componentDidUpdate(prevProps) {

    if (prevProps.registerErrorMsg !== this.props.registerErrorMsg) {
      if (this.props.registerErrorMsg != "") {
        alert(this.props.registerErrorMsg);
        this.setState({loading:false});
      }
    }
    if (prevProps.registerStatus !== this.props.registerStatus) {
      if (this.props.registerStatus) {
        // successAlert("Otp is sent");
        // this.props.history.push("/verify-otp");
        navigateToOtpInput(this.props.componentId);

      }
    }
  }

  onChangeInput = (key, value) => {
    this.setState({
      [key]: value
    });
  }

  onChangeTabEmail = () => {
    this.setState({activeTab: 'email'});
  };
  onChangeTabPhone = () => {
    this.setState({activeTab: 'phone'});
  };

  privacyChange = (newValue) => {
    this.setState({privacy: !this.state.privacy});
  };

  onPressButton = () => {

    if (this.state.activeTab === "email") {
      if (
        this.state.user == "" ||
        this.state.email == "" ||
        (this.state.password == "" || this.state.password.length < 5)
      ) {
        alert("Please fill up the inputs..!!");
        return;
      }
    }

    if (this.state.activeTab === "phone") {
      if (
        this.state.user == "" ||
        this.state.phone == "" ||
        (this.state.password == "" || this.state.password.length < 5)
      ) {
        alert("Please fill up the form inputs..!!");
        return;
      }
    }

    if (!this.state.privacy) {
      alert("Please accept privacy policy..!!");
      return;
    }

    this.setState({ loading: true });

    try {
      if (this.state.activeTab === "email") {
        this.props.register(
          this.state.user,
          this.state.email,
          this.state.password,
          1
        );
        // TODO "NEED TO EXPORT CONST FOR ACCOUTN TYPE"
      }
      if (this.state.activeTab === "phone") {
        this.props.register(
          this.state.user,
          this.state.phone,
          this.state.password,
          2
        );
      }
    } catch (err) {
      console.log("register form submit error: ", err);
    }
    // this.setState({ loading: false });
    return true;
  }

  render() {

    const {email, user, number, numberWithCode, password, activeTab, privacy, loading} = this.state;
    // console.log(this.props);
    return (
      <ScrollView>
        <View style={styles.container}>
        <View style={styles.headerWarpper}>
          <View style={styles.headTop}>
            <Image source={dropdown} style={{width: 7, height: 12, marginRight: 5}} />
            <Text
              style={styles.backTo}
              onPress={() => navigateToHome(this.props.componentId)}
            >Back to main
            </Text>
          </View>
          <View style={styles.headBottom}>
            <Image source={vector} style={{width: 25, height: 25, marginRight: 5}} />
            <Text style={styles.logIn}>register</Text>
            <Image source={rectangle} style={styles.rectangle} />
          </View>
        </View>
        <View style={styles.bodyWarpper}>
          <Form
            email={email}
            user={user}
            number={number}
            numberWithCode={numberWithCode}
            passowrd={password}
            privacy={privacy}
            activeTab={activeTab}
            onChange={this.onChangeInput}
            privacyChange={this.privacyChange}
            onChangeTabEmail={this.onChangeTabEmail}
            onChangeTabPhone={this.onChangeTabPhone}/>
        </View>
        <View style={styles.footerWarpper}>
          <Footer
            loading={loading}
            onPressButton={this.onPressButton}
            componentId={this.props.componentId} />
        </View>
      </View>
      </ScrollView>
    );
  }
}

export default connect(
  state => ({
    isLoggedIn: !!selectToken(state),
    loading: selectRegisterProccessing(state),
    registerStatus: selectRegistrationStatus(state),
    registerErrorMsg: selectRegisterErrorMessage(state),
    loginStatus: selectLoginStatus(state)
  }),
  {
    register,
    authenticateByFacebook,
    authenticateByGoogle
  }
)(Register);
