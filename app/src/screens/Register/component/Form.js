import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  props,
  value,
  setValue,
  TouchableOpacity,
  setFormattedValue,
} from 'react-native';
import {GROTESK_SEMIBOLD} from "../../../utils/constants";
import {PRIMARY_BLACK} from '../../../utils/constants';
import {SECONDARY_ORANGE} from '../../../utils/constants';
import {PRIMARY_GRAY} from '../../../utils/constants';

import CheckBox from '@react-native-community/checkbox';

import EmailTab from './EmailTab';
import PhoneTab from './PhoneTab';

class Form extends React.Component {
  render() {
    const {email, user, number, numberWithCode, password, privacy, activeTab, privacyChange} = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.tabContainer}>
          <View style={styles.tabButton}>
            <TouchableOpacity
              style={styles.emailButton}
              // onPress={() => this.setState({activeTab: 'email'})}>
              onPress={this.props.onChangeTabEmail}>
              <Text
                style={
                  activeTab === 'email'
                    ? styles.activeEmailButtonText
                    : styles.inActiveEmailButtonText
                }>
                EMAIL ADDRESS
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.phoneButton}
              // onPress={() => this.setState({activeTab: 'phone'})}>
              onPress={this.props.onChangeTabPhone}>
              <Text
                style={
                  activeTab === 'phone'
                    ? styles.activePhoneButtonText
                    : styles.inActivePhoneButtonText
                }>
                PHONE NUMBER
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.tabContent}>
            {activeTab === 'email' && <EmailTab email={email} user={user} password={password} onChange={this.props.onChange}/>}
            {activeTab === 'phone' && <PhoneTab user={user} number={number} numberWithCode={numberWithCode} password={password} onChange={this.props.onChange} />}
          </View>
        </View>
        <View style={styles.fromFooter}>
          <View style={styles.checkboxContainer}>
            <CheckBox
              style={styles.checkBox}
              value={privacy}
              onValueChange={privacyChange}
            />
            <Text style={styles.label}>
              Agree with Socialogue Privacy Policy
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

export default Form;

const styles = StyleSheet.create({
  container: {
    flex: 100,
    paddingLeft: 20,
    paddingRight: 20,
    borderLeftWidth: 1,
    borderLeftColor: SECONDARY_ORANGE,
  },
  tabContent: {

  },
  tabButton: {
    justifyContent: 'center',
    flexDirection: 'row',
    borderBottomWidth: 1,
    marginBottom: 20,
  },
  emailButton: {
    flex: 50,
    alignItems: 'center',
  },
  phoneButton: {
    flex: 50,
    alignItems: 'center',
  },
  activePhoneButtonText: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    borderBottomWidth: 3,
  },
  inActivePhoneButtonText: {
    color: PRIMARY_GRAY,
  },
  activeEmailButtonText: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    borderBottomWidth: 3,
  },
  inActiveEmailButtonText: {
    color: PRIMARY_GRAY,
  },
  fromFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10
  },
  checkboxContainer: {
    flexDirection: 'row',
  },
  label: {
    marginTop: 5,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  },
});
