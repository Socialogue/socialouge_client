import * as React from 'react';
import {Text, View, TextInput, StyleSheet} from 'react-native';
import {GROTESK_SEMIBOLD} from "../../../utils/constants";
import {PRIMARY_BLACK} from '../../../utils/constants';
import {PRIMARY_GRAY} from '../../../utils/constants';

class EmailTab extends React.Component {
  render() {
    return (
      <View style={styles.emailContentTab}>
        <Text style={styles.Title}>EMAIL</Text>
        <TextInput
          style={styles.textInput}
          placeholder="Your Email"
          value={this.props.email}
          onChangeText={ e => this.props.onChange('email', e)}
        />
        <Text style={styles.Title}>USERNAME</Text>
        <TextInput
          style={styles.textInput}
          placeholder="Your Username"
          value={this.props.user}
          onChangeText={ e => this.props.onChange('user', e)}
        />
        <Text style={styles.Title}>PASSWORD</Text>
        <TextInput style={styles.textInput}
                   secureTextEntry={true}
                   value={this.props.password}
                   onChangeText={ e => this.props.onChange('password', e)}
                   placeholder="Your Password" />
      </View>
    );
  }
}

export default EmailTab;

const styles = StyleSheet.create({
  textInput: {
    color: PRIMARY_GRAY,
    borderBottomWidth: 1,
    marginTop: 5,
    marginBottom: 15,
  },
  Title: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  }
});
