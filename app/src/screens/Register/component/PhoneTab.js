import * as React from 'react';
import {
  Text,
  View,
  TextInput,
  useRef,
  value,
  StyleSheet,
  setValue,
  useState,
} from 'react-native';

import PhoneInput from 'react-native-phone-number-input';

import {GROTESK_SEMIBOLD} from "../../../utils/constants";
import {PRIMARY_BLACK} from '../../../utils/constants';
import {PRIMARY_GRAY} from '../../../utils/constants';

const myCountryCodeList = ['BR', 'US', "AF", "AL"];
const RegionList = [
  'Africa',
  'Americas',
  'Antarctic',
  // 'Asia',
  'Europe',
  'Oceania'
]
class PhoneTab extends React.Component {

  render() {
    // const phoneInput = useRef < PhoneInput > null;
    return (
      <View style={styles.phoneContentTab}>
        <Text style={styles.Title}>USER NAME</Text>
        <TextInput
          style={styles.userInput}
          placeholder="Your user name"
          value={this.props.user}
          onChangeText={ e=> this.props.onChange('user', e)}
        />

        <Text style={styles.Title}>PHONE NUMBER</Text>
        {/* <PhoneInput
                    ref={phoneInput}
                    defaultValue={value}
                    onChangeText={(text) => {
                    setValue(text);
                    }}
                    onChangeFormattedText={(text) => {
                    setFormattedValue(text);
                    }}
                /> */}
        <PhoneInput
          style={styles.phoneInput}
          containerStyle={{
            backgroundColor: 'none',
            borderBottomWidth: 1,
            marginBottom: 10,
            height: 75,
            marginVertical: 5,

          }}
          defaultCode="LT"
          region={RegionList}
          // countryCodes={myCountryCodeList}
          value={this.props.number}
          onChangeText={(text) => {
            this.props.onChange('number', text)
          }}
          onChangeFormattedText={(text) => {
            this.props.onChange('numberWithCode', text)
          }}
        />
        <Text style={styles.Title}>PASSWORD</Text>
        <TextInput
          secureTextEntry={true}
          style={styles.textInput}
          placeholder="Your Password"
          value={this.props.password}
          onChangeText={ e => this.props.onChange('password', e)}
        />
      </View>
    );
  }
}

export default PhoneTab;

const styles = StyleSheet.create({
  textInput: {
    color: PRIMARY_GRAY,
    borderBottomWidth: 1,
    // marginTop: 5,
    marginBottom: 10,
  },
  userInput: {
    color: PRIMARY_GRAY,
    borderBottomWidth: 1,
    marginBottom: 10
  },
  Title: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  }
});
