import {StyleSheet} from 'react-native';
import {GROTESK_SEMIBOLD} from "../../utils/constants";
import {PRIMARY_BLACK} from '../../utils/constants';


export default StyleSheet.create({
  container: {
    flex: 100,
    padding: 20,
  },
  inner: {
    flex: 1,
    padding: 20,
  },

  headerWarpper: {
    flex: 10,
    // backgroundColor: 'red'
  },
  headTop: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  dropdown: {
    marginRight: 5,
  },
  backTo: {
    fontSize: 14,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  },
  bodyWarpper: {
    flex: 60,
    justifyContent: 'center',
    marginVertical: 20,
    // backgroundColor: 'green'
  },
  headBottom: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    marginTop: 10,
  },
  vector: {
    marginRight: 5,
  },
  logIn: {
    fontSize: 40,
    marginBottom: -10,
    marginRight: 3,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  },
  footerWarpper: {
    flex: 30,
    justifyContent: 'flex-end',
    // backgroundColor: 'blue'
  },
});
