import * as React from 'react';
import {Image, ScrollView, StyleSheet, Text, TextInput, View} from 'react-native';
import styles from './styles';

import Header from "../../components/Header";
import {
  GROTESK_SEMIBOLD,
  LIGHT_GRAY,
  PRIMARY_BLACK,
  PRIMARY_GRAY,
  PRIMARY_WHITE,
  SECONDARY_ORANGE
} from "../../utils/constants";
import PhoneInput from "react-native-phone-number-input";
import CheckBox from "@react-native-community/checkbox";

import RadioGroup, {Radio} from "react-native-radio-input";
import PrimaryButton from "../../components/PrimaryButton";
import {navigateToCheckOutAddress, navigateToOrderingOverView} from "../../navigator";
import BottomTab from "../../components/BottomTab";


const cross = require('./images/cross.png');
const Info = require('./images/Info.png');
const coat = require('./images/coat.png');
const rightArrow = require('./images/rightArrow.png');
const downArrow = require('./images/downArrow.png');


class CheckOutAddress extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      checked: false,
      content: {}
    };
  }
  onPressButton = () => {
    this.setState({loading: true});
  }
  getChecked = (value) => {
    // value = our checked value
    console.log(value)
  }

  toggleCheckbox = newValue => {
    this.setState({checked: newValue});
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerWrapper}>
          <Header componentId={this.props.componentId} />
        </View>
        <View style={styles.bodyWrapper}>
          <ScrollView contentContainerStyle={this.state.content}>
            <View style={{padding: 30}}>
              <Text style={styles.lebelText}>address</Text>
              <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <RadioGroup
                  getChecked={this.getChecked}
                  IconStyle={{height: 22, width: 22}}
                  labelStyle={{fontSize: 16, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK, marginLeft: 20}}
                >
                  <Radio iconName={"lens"} label={"Tyler Perry"} value={"A"}/>
                    <Text style={{fontSize: 12, color: PRIMARY_GRAY, marginLeft: '20%', marginBottom: 20}}>Little George St, Westminster London</Text>
                  <Radio iconName={"lens"} label={"Use other address"} value={"B"}/>
                </RadioGroup>
                <View>
                  <Text style={{color: PRIMARY_GRAY, fontFamily: GROTESK_SEMIBOLD, borderBottomWidth: 1}}>EDIT</Text>
                </View>
              </View>
              <View style={{paddingVertical: 30}}>
                <Text style={styles.lebelText}>Other Delivery information</Text>
                <View style={{marginVertical: 20}}>
                  <Text style={styles.lebelText}>Name <Text style={{color: SECONDARY_ORANGE}}>*</Text></Text>
                  <TextInput
                    style={styles.input}
                    placeholder="Tyler"
                  />
                </View>
                <View style={{marginVertical: 20}}>
                  <Text style={styles.lebelText}>Surename <Text style={{color: SECONDARY_ORANGE}}>*</Text></Text>
                  <TextInput
                    style={styles.input}
                    placeholder="Perry"
                  />
                </View>
                <View style={styles.content}>
                  <CheckBox
                    style={{marginRight: 10}}
                    value={this.state.checked}
                    onValueChange={this.toggleCheckbox}
                  />
                  <Text style={styles.lebel}>Show my name</Text>
                </View>
                <View style={{marginVertical: 20}}>
                  <Text style={styles.lebelText}>Date of birth <Text style={{color: SECONDARY_ORANGE}}>*</Text></Text>
                  <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View style={styles.dateTab}>
                      <Text>1993</Text>
                      <Image source={downArrow} style={{width: 10, height: 6, marginLeft: 10}} />
                      <View style={{width: 67, backgroundColor: PRIMARY_WHITE, position: 'absolute', top: 45, alignItems: 'center', borderWidth: 1}}>
                        <ScrollView style={{maxHeight: 200 }} >
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>1994</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>1994</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>1994</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>1994</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>1920</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>1920</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>1920</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>1920</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>1920</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>1920</Text>
                        </ScrollView>
                      </View>
                    </View>
                    <View style={styles.dateTab}>
                      <Text>January</Text>
                      <Image source={downArrow} style={{width: 10, height: 6, marginLeft: 10}} />
                      <View style={{width: 87, backgroundColor: PRIMARY_WHITE, position: 'absolute', top: 45, alignItems: 'center', borderWidth: 1}}>
                        <ScrollView style={{maxHeight: 200 }} >
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>January</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>January</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>January</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>January</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>January</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>January</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>January</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>January</Text>
                        </ScrollView>
                      </View>

                    </View>

                    <View style={styles.dateTab}>
                      <Text>21</Text>
                      <Image source={downArrow} style={{width: 10, height: 6, marginLeft: 10}} />
                      <View style={{width: 52, backgroundColor: PRIMARY_WHITE, position: 'absolute', top: 45, alignItems: 'center', borderWidth: 1}}>
                        <ScrollView style={{maxHeight: 200 }} >
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>21</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>21</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>21</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>21</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>21</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>21</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>21</Text>
                          <Text style={{borderBottomWidth: 1, borderColor: LIGHT_GRAY, padding: 10}}>21</Text>
                        </ScrollView>
                      </View>
                    </View>
                  </View>
                </View>
                <View style={{marginVertical: 20, marginTop: 200}}>
                  <Text style={styles.lebelText}>Country<Text style={{color: SECONDARY_ORANGE}}>*</Text></Text>
                  <PhoneInput
                    style={styles.phoneInput}
                    containerStyle={{
                      backgroundColor: 'none',
                      borderBottomWidth: 1,
                      marginBottom: 10,
                      height: 75,
                      marginVertical: 5,

                    }}
                    defaultCode="US"
                    // region={RegionList}
                    // countryCodes={myCountryCodeList}
                    value={this.props.number}
                    onChangeText={(text) => {
                      this.props.onChange('number', text)
                    }}
                    onChangeFormattedText={(text) => {
                      this.props.onChange('numberWithCode', text)
                    }}
                  />
                </View>
                <View style={{marginVertical: 20}}>
                  <Text style={styles.lebelText}>Address <Text style={{color: SECONDARY_ORANGE}}>*</Text></Text>
                  <View style={styles.typeInput}>
                    <TextInput
                      style={{width: '70%'}}
                      placeholder="Little George St, Westminster, London"
                      multiline
                    />
                    <View style={styles.send}>
                      <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK,}}>Find address</Text>
                    </View>
                  </View>
                </View>
                <View style={{marginVertical: 20}}>
                  <Text style={styles.lebelText}>Zip code <Text style={{color: SECONDARY_ORANGE}}>*</Text></Text>
                  <View style={styles.typeInput}>
                    <TextInput
                      style={{width: '70%'}}
                      placeholder="12412"
                      multiline
                    />
                    <View style={styles.send}>
                      <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK,}}>Find code</Text>
                    </View>
                  </View>
                </View>
                <View style={{marginVertical: 20}}>
                  <Text style={styles.lebelText}>Phone number <Text style={{color: SECONDARY_ORANGE}}>*</Text></Text>
                  <TextInput
                    style={styles.input}
                    placeholder="+ 370 23 234 234"
                    keyboardType="number-pad"
                  />
                </View>
                <View style={{marginVertical: 20}}>
                  <Text style={styles.lebelText}>Email address <Text style={{color: SECONDARY_ORANGE}}>*</Text></Text>
                  <TextInput
                    style={styles.input}
                    placeholder="Name@domain.com"
                  />
                </View>
                <View style={{marginVertical: 20}}>
                  <Text style={styles.lebelText}>Delivery <Text style={{color: SECONDARY_ORANGE}}>*</Text></Text>
                  <RadioGroup
                    getChecked={this.getChecked}
                    IconStyle={{height: 22, width: 22}}
                    labelStyle={{fontSize: 16, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK, marginLeft: 20}}
                  >
                    <Radio iconName={"lens"} label={"Standard delivery"} value={"A"}/>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                      <Text style={{fontSize: 12, color: PRIMARY_GRAY, marginLeft: '20%', marginBottom: 20}}>Delivery 3 - 5 working days</Text>
                      <Text style={styles.lebelText}>€ 2.99</Text>
                    </View>
                    <Radio iconName={"lens"} label={"FAST delivery"} value={"B"}/>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                      <Text style={{fontSize: 12, color: PRIMARY_GRAY, marginLeft: '20%', marginBottom: 20}}>Delivery 1 - 2 working days</Text>
                      <Text style={styles.lebelText}>€ 2.99</Text>
                    </View>
                    <Radio iconName={"lens"} label={"Pickup lockers"} value={"c"}/>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                      <Text style={{fontSize: 12, color: PRIMARY_GRAY, marginLeft: '20%', marginBottom: 20}}>Delivery 1 - 2 working days</Text>
                      <Image source={downArrow} style={{width: 10, height: 6, marginLeft: 10}} />
                    </View>
                  </RadioGroup>
                </View>
                <View style={{marginVertical: 20}}>
                  <Text style={styles.lebelText}>Payment <Text style={{color: SECONDARY_ORANGE}}>*</Text></Text>
                  <RadioGroup
                    getChecked={this.getChecked}
                    IconStyle={{height: 22, width: 22}}
                    labelStyle={{fontSize: 16, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_GRAY, marginLeft: 10}}
                    RadioGroupStyle={{flexDirection: "row", justifyContent: "space-between"}}
                  >
                    <Radio iconName={"lens"} label={"Paysera"} value={"A"}/>

                    <Radio iconName={"lens"} label={"Stripe"} value={"B"}/>


                  </RadioGroup>
                </View>
              </View>
              <PrimaryButton
                loading={this.props.loading}
                title='submit'
                // onPress={this.props.onPressButton}/>
                onPress={() => navigateToOrderingOverView(this.props.componentId)}/>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}
export default CheckOutAddress;

