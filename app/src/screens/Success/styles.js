import {StyleSheet, Text} from 'react-native';
import {GROTESK_SEMIBOLD, LIGHT_GRAY, PRIMARY_BLACK, PRIMARY_GRAY, SECONDARY_ORANGE} from "../../utils/constants";
import * as React from "react";

export default StyleSheet.create({
  container: {
    padding: 20,
    flex: 100
  },
  headerWarpper: {
    flex: 20
  },
  bodyWarpper: {
    flex: 80,
    marginHorizontal: '10%'
  },
  headTop: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  ressetText: {
    fontSize: 30,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    marginBottom: 5,
    marginTop: 10
  },
});
