import * as React from 'react';
import {Text, View, Image, TouchableOpacity, ScrollView, StyleSheet, TextInput} from 'react-native';
import {Navigation} from "react-native-navigation";
import Form from "../Register/component/Form";
import styles from './styles';

import {GROTESK_SEMIBOLD, PRIMARY_BLACK, SECONDARY_ORANGE, PRIMARY_GRAY} from '../../utils/constants';
import PrimaryButton from "../../components/PrimaryButton";
import {navigateToLogin} from "../../navigator";

const leftArrow = require('.././../images/leftArrow.png');
const Socialoguelogo = require('.././../images/Socialoguelogo.png');



class SubmitEmail extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  onPressButton = () => {
    this.setState({loading: true});
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerWarpper}>
          <View style={styles.headTop}>
            <Image source={leftArrow} style={{width: 6, height: 10, marginRight: 10}} />
            <Text
              style={{fontSize: 14, fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK,}}
              onPress={() => navigateToLogin(this.props.componentId)}

            >Back to main
            </Text>
          </View>
        </View>
        <View style={styles.bodyWarpper}>
          <Image source={Socialoguelogo} style={{width: 100, height: 25, marginRight: 10}} />
          <Text style={styles.ressetText}>success<Text style={{fontSize: 40, color: SECONDARY_ORANGE}}>.</Text></Text>
          <Text style={{fontSize: 12, marginBottom: 30, color: PRIMARY_GRAY}}>Your new password has been successfully saved.</Text>
          <View style={styles.resstWrapper}>
            <PrimaryButton
              loading={this.state.loading}
              title='login'
              onPress={this.onPressButton}/>
          </View>
        </View>
      </View>
    );
  }
}

export default SubmitEmail;

