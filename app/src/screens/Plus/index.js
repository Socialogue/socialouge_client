

import React, { Component } from 'react';
import {Platform, View, StyleSheet, Text, Image, TextInput, AppRegistry, TouchableOpacity} from 'react-native';
import {Navigation} from "react-native-navigation";
import {navigateToLogin, navigateToTagList} from "../../navigator";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK} from "../../utils/constants";
import BottomTab from "../../components/BottomTab";
import styles from './styles';
import Header from '../../components/Header';

const userAvater = require('../../images/userAvater.png');
const cross = require('../../images/cross.png');
const Tag = require('../../images/Tag.png');
const imaigIcons = require('../../images/imaigIcons.png');


export default class PlusScreen extends Component {

constructor(props) {
  super(props);
}

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerTopWarpper}>
          <Header componentId={this.props.componentId} />
        </View>
        <View style={styles.headerWarpper}>
          <View style={styles.createPost}>
            <Text style={{
              fontSize: 16,
              fontFamily: GROTESK_SEMIBOLD,
              color: PRIMARY_BLACK
            }}>CREATE A POST</Text>
          </View>
          {/*<View style={styles.createPostCross}>*/}
          {/*  <TouchableOpacity*/}
          {/*    onPress={() => Navigation.pop(this.props.componentId)}>*/}
          {/*    /!*<Header componentId={this.props.componentId}></Header>*!/*/}

          {/*    <Image source={cross} style={{width: 25, height: 25}}/>*/}
          {/*  </TouchableOpacity>*/}
          {/*</View>*/}
        </View>
        <View style={styles.bodyWarpper}>
          <View style={styles.bodyContainer}>
            <View style={styles.postId}>
              <Image source={userAvater} style={styles.userAvater} />
              <Text style={styles.createUser}>BEN ONWAR</Text>
            </View>
            <View style={styles.typeText}>
              <TextInput
                style={styles.textInput}
                placeholder="What's on your mind?"
                multiline
              />
            </View>
          </View>
        </View>
        <View style={styles.postWarpper}>
          <View style={styles.icon}>
            <Image source={imaigIcons} style={{width: 22, height: 22, marginRight: 15}} />

            <TouchableOpacity
              onPress={() =>navigateToTagList(this.props.componentId)}>
              {/*<Header componentId={this.props.componentId}></Header>*/}
              <Image source={Tag} style={{width: 22, height: 22}} />
            </TouchableOpacity>

          </View>
          <View style={styles.postButton}>
            <TouchableOpacity style={styles.button}>
              <Text style={styles.postButtonText}>POST</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.bottomWarpper}>
          {/*<BottomTab componentId={this.props.componentId}/>*/}
        </View>
      </View>
    );
  }
}

