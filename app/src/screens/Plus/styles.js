import {StyleSheet, Text} from 'react-native';
import {GROTESK_SEMIBOLD, LIGHT_GRAY, PRIMARY_BLACK, PRIMARY_GRAY, SECONDARY_ORANGE} from "../../utils/constants";
import * as React from "react";

export default StyleSheet.create({
  container: {
    flex: 100,
    // padding: 20
  },
  headerTopWarpper: {
    height: 60
  },
  headerWarpper: {
    flex: 6,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    paddingBottom: 5,
    marginHorizontal: 20,
    marginTop: 20
  },
  bodyWarpper: {
    flex: 90,
    margin: 20,
  },
  bottomWarpper: {
    borderTopWidth: 1
  },
  bodyContainer: {
    flex: 100
  },
  postId: {
    flex: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  createUser: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  },
  userAvater: {
     width: 40,
    height: 40,
    borderRadius: 37.5,
    marginRight: 10
  },
  typeText: {
    flex: 60
  },
  textInput: {
    padding: 10
  },
  postWarpper: {
    flex: 4,
    borderTopWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 10,
    margin: 20

  },
  icon: {
    flexDirection: 'row',
  },
  button: {
    borderWidth: 1,
    height: 30,
    width: 90,
    alignItems: 'center',
    justifyContent: 'center'
  },
  postButtonText: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  }
});
