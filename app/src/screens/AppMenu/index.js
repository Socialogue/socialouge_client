import * as React from 'react';
import {Alert, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View, Button} from "react-native";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY} from '../../utils/constants';

import styles from './styles';


import {Navigation} from "react-native-navigation";
import DropDownItem from "react-native-drop-down-item";
import {connect} from "react-redux";
import {selectToken, selectUser} from "core/selectors/user";
import {logOut} from "core/actions/authActionCreators";
import {
  navigateToApplyShop,
  navigateToCart,
  navigateToHome,
  navigateToLogin,
  navigateToRegister,
  ChangeNavigationToShop, ChangeNavigationToUser,
  navigateToShopProfile, navigateToStartSelling,
  navigateToUserProfile, startMainActivity, startShopActivity
} from "../../navigator";
import {selectShops} from "core/selectors/shops";
import {fetchShop} from "core/actions/shopActionCreators";
import {setActiveShop} from "core/actions/accountActionCreators";
import {selectActiveProfile, selectActiveShop} from "core/selectors/account";
import {ROUTE_CREATE_PRODUCT, ROUTE_LOGIN, ROUTE_SELLING_START} from "web/src/routes/constants";
import ShopDown from "./ShopDown";
import {getImageOriginalUrl} from "core/utils/helpers";



const socialoguelogo = require('../../images/Socialoguelogo.png');

const cart = require('../../images/cart.png');
const cross = require('../../images/cross.png');

const downArrow = require('../../images/downArrow.png');
const upArrow = require('../../images/upArrow.png');
const IC_ARR_DOWN = require('../../images/ic_arr_down.png');
const IC_ARR_UP = require('../../images/ic_arr_up.png');
const dashboard = require('../../images/dashboard.png');
const sellicon = require('../../images/sellicon.png');
const setting = require('../../images/settings.png');
const socialogue = require('../../images/socialogue.png');
const userAvater = require('../../images/userAvater.png');
const arrowLeft = require('../../images/arrowLeft.png');


const Favorite = require('../../images/Favorite.png');
const notificationicon = require('../../images/notificationicon.png');



class AppMenu extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      showManCategory: false,
      showWomanCategory: false,
      isArrow: false,
      shopArrow: false,
      allShops: []
    }
  }

  _categoryShow = (type) => {
    // console.log(type);
    if(type === 'man'){
      this.setState({showManCategory: !this.state.showManCategory})
      this.setState({showWomanCategory: false})
    }
    if(type === 'woman'){
      this.setState({showWomanCategory: !this.state.showWomanCategory})
      this.setState({showManCategory: false})
    }
  }

  _shopShow = () => {
    this.setState({isArrow: !this.state.isArrow});
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isLoggedIn !== this.props.isLoggedIn) {
      if (!this.props.isLoggedIn) {
        navigateToHome(this.props.componentId);

      }
    }
  }
  componentDidMount = () =>{
    this.props.fetchShop();
  }

  switchAccount = () => {
    const {allShops} = this.props;
    const length = allShops.length;
    if (length == 0) {
      /*this.props.history.push(ROUTE_SELLING_START);*/
      // switch to applyshop screen
    } else if (length === 1) {
      this.props.setActiveShop(allShops[0].shopName, allShops[0]);
      //startShopActivity();
      ChangeNavigationToShop();

    } else {
      this.props.setActiveShop(allShops[0].shopName, allShops[0]);
      //startShopActivity();
      // set all shops to dropdown
     ChangeNavigationToShop();
    }
  };


  switchToUserAccount = () => {
    this.props.setActiveShop("", {});
    //startMainActivity();
   ChangeNavigationToUser();
  };


  logOut = () => {
    Alert.alert(
      "Do You Want to Logout?",
      "",
      [
        {
          text: "Cancel",
          style: "cancel"
        },
        { text: "Logout", onPress: () => this.props.logOut() }
      ]
    );
  }

  renderForActiveShop = () => {
    const Arrow = this.showArrow();
    return (
      <View style={styles.userId}>
        <View style={styles.id}>
          <Image
            source={this.props.user.profileImage === undefined ? userAvater : {uri:getImageOriginalUrl(this.props.user.profileImage)}}
            style={{marginRight: 10, height: 40, width: 40, borderRadius: 37.5}} />
          <TouchableOpacity
            onPress={() =>navigateToShopProfile(this.props.componentId)}>
            <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK, fontSize: 16}}>
              {this.props.activeShop.shopName}
            </Text>
          </TouchableOpacity>

          <View>
            {Arrow}
          </View>
        </View>
        <View style={styles.switchUser}>
          <TouchableOpacity
            style={styles.switchButton}
            onPress={this.switchToUserAccount}
          >
            <Text style={styles.switchButtonText}>SWITCH USER</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderForActiveUser = () => {
    let SwitchView = null;
    if (this.props.allShops.length) {
      SwitchView = (
        <TouchableOpacity
          style={styles.switchButton}
          onPress={this.switchAccount}
        >
          <Text style={styles.switchButtonText}>SWITCH SHOP</Text>
        </TouchableOpacity>
      );
    }

    if (this.props.allShops.length === 0) {
      SwitchView = (
        <TouchableOpacity
          style={styles.switchButton}
          onPress={() => navigateToStartSelling(this.props.componentId)}
        >
          <Text style={styles.switchButtonText}>CREATE SHOP</Text>
        </TouchableOpacity>
      );
    }

    return (
      <View style={styles.userId}>
        <View style={styles.id}>
          <Image
            source={this.props.user.profileImage === undefined ? userAvater : {uri:getImageOriginalUrl(this.props.user.profileImage)}}
            style={{marginRight: 10, height: 40, width: 40, borderRadius: 37.5}} />
          <TouchableOpacity
            onPress={() =>navigateToUserProfile(this.props.componentId)}>
            <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK, fontSize: 16}}>
              {this.props.user.fullName}
            </Text>
          </TouchableOpacity>

        </View>
        <View style={styles.switchUser}>
          {SwitchView}
        </View>
      </View>
    )
  }

  renderUserNameRow = () => {
    const hasActiveShop = Object.entries(this.props.activeShop).length !== 0;

    if (hasActiveShop) {
      return this.renderForActiveShop();
    }

    return this.renderForActiveUser();
  }


  showArrow = () => {
    if (this.props.allShops.length > 1) {
      return (
        <View>
          <TouchableOpacity
            style={{padding: 15}}
            onPress={() => this._shopShow()}
          >
            {this.state.isArrow &&
              <Image source={upArrow} style={{width: 10, height: 6}} />
            }
            {!this.state.isArrow &&
              <Image source={downArrow} style={{width: 10, height: 6}} />
            }
          </TouchableOpacity>
        </View>
      );
    }

    return null;
  }

  setActiveShop = (shop) => {
    this.props.setActiveShop(shop.shopName, shop);
    this.setState({isArrow: false});
  }

  render() {
    // console.log('show shop', this.props.allShops.length);
    return (
      <View style={styles.container}>
        <View style={styles.headerWarpper}>
          <View style={styles.logo}>
            <TouchableOpacity
              onPress={() => navigateToHome(this.props.componentId)}
            >
              <Image source={socialoguelogo} style={{width: 100, height: 25}} />
            </TouchableOpacity>
          </View>
          <View style={styles.iconPart}>
            {this.props.isLoggedIn &&
            <TouchableOpacity
              style={{paddingVertical: 10, paddingHorizontal: 5}}
            >
              <Image source={notificationicon} style={{width: 22, height: 22}} />
            </TouchableOpacity>
            }
            {this.props.isLoggedIn &&
            <TouchableOpacity
              style={{paddingVertical: 10, paddingHorizontal: 5}}
            >
              <Image source={Favorite} style={{width: 22, height: 23, marginBottom: 5}} />
            </TouchableOpacity>
            }
            {this.props.isLoggedIn &&
            <TouchableOpacity
              style={{paddingVertical: 10, paddingHorizontal: 5}}
              onPress={() => navigateToCart(this.props.componentId)}
            >
              <Image source={cart} style={{width: 22, height: 22}} />
            </TouchableOpacity>
            }

            {/*<Text style={styles.logInText}>LOGIN</Text>*/}
            <TouchableOpacity
              style={{paddingRight: 20, paddingVertical: 10, paddingLeft: 10}}
              onPress={() => Navigation.pop(this.props.componentId)}>
              {/*<Header componentId={this.props.componentId}></Header>*/}
              <Image source={cross} style={{width: 25, height: 25}} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.bodyWarpper}>
          <View style={styles.category}>
            <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_GRAY}}>CATEGORY</Text>

            <TouchableOpacity
              style={{alignItems: 'center'}}
              onPress={() => this._categoryShow('man')}
            >
              <Text style={styles.manCategory}>MAN</Text>
              {this.state.showManCategory &&
              <Image source={upArrow} style={{width: 10, height: 6, marginTop: 5}} />
              }
              {!this.state.showManCategory &&
              <Image source={downArrow} style={{width: 10, height: 6, marginTop: 5}} />
              }
            </TouchableOpacity>
            <TouchableOpacity
              style={{alignItems: 'center'}}
              onPress={() => this._categoryShow('woman')}
            >
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>WOMAN</Text>
              {this.state.showWomanCategory &&
              <Image source={upArrow} style={{width: 10, height: 6, marginTop: 5}} />
              }
              {!this.state.showWomanCategory &&
              <Image source={downArrow} style={{width: 10, height: 6, marginTop: 5}} />
              }
            </TouchableOpacity>
          </View>
          <View>
            {this.state.showManCategory &&
            <View style={{height: 1000, borderTopWidth: 1}}>
              <View style={{padding: 20, flexDirection: 'row', alignItems: 'center'}}>
                <Image source={arrowLeft} style={{width: 25, height: 15, marginRight: 5}} />
                <Text style={{textTransform: 'uppercase', fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>Clothes</Text>
              </View>
              <Text style={styles.subCategory}>See all</Text>
              <Text style={styles.subCategory}>Coats & jackets</Text>
              <Text style={styles.subCategory}>Suits & blazers</Text>
              <Text style={styles.subCategory}>Pants</Text>
              <Text style={styles.subCategory}>Underwear</Text>
            </View>
            }
            {this.state.showWomanCategory &&
            <View style={{height: 1000, borderTopWidth: 1}}>
              <View style={{padding: 20, flexDirection: 'row', alignItems: 'center'}}>
                <Image source={arrowLeft} style={{width: 25, height: 15, marginRight: 5}} />
                <Text style={{textTransform: 'uppercase', fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>Woman</Text>
              </View>
            </View>
            }
          </View>
          {this.props.isLoggedIn &&
            this.renderUserNameRow()
          }
          <ScrollView style={{alignSelf: 'stretch', padding: 20}}>
            <View style={styles.downMenu}>
              <DropDownItem
              key={'SK1'}
                style={styles.dropDownItem}
                contentVisible={false}
                invisibleImage={IC_ARR_DOWN}
                visibleImage={IC_ARR_UP}
                useNativeDriver={false}
                header={
                  <View style={styles.menuHead}>
                    <Image source={sellicon} style={{width: 20, height: 20, marginRight: 20}} />
                    <Text style={styles.drpDownText}>SELL</Text>
                  </View>
                }>
                <View>
                  <Text>Content</Text>
                </View>
              </DropDownItem>
            </View>
            <View style={styles.downMenu}>
              <DropDownItem
                style={styles.dropDownItem}
                contentVisible={false}
                invisibleImage={IC_ARR_DOWN}
                visibleImage={IC_ARR_UP}
                useNativeDriver={false}
                header={
                  <View style={styles.menuHead}>
                    <Image source={dashboard} style={{width: 18, height: 18, marginRight: 20}} />
                    <Text style={styles.drpDownText}>DASHBOARD</Text>
                  </View>
                }>
                <View>
                  <Text>Content</Text>
                </View>
              </DropDownItem>
            </View>
            <View style={styles.downMenu}>
              <DropDownItem
              key={'SK3'}
                style={styles.dropDownItem}
                contentVisible={false}
                invisibleImage={IC_ARR_DOWN}
                visibleImage={IC_ARR_UP}
                useNativeDriver={false}
                header={
                  <View style={styles.menuHead}>
                    <Image source={setting} style={{width: 20, height: 20, marginRight: 20}} />
                    <Text style={styles.drpDownText}>SETTING</Text>
                  </View>
                }>
                <View>
                  <Text>Content</Text>
                </View>
              </DropDownItem>
            </View>
            <View style={styles.downMenu}>
              <DropDownItem
              key={'SK4'}
                style={styles.dropDownItem}
                contentVisible={false}
                invisibleImage={IC_ARR_DOWN}
                visibleImage={IC_ARR_UP}
                useNativeDriver={false}
                header={
                  <View style={styles.menuHead}>
                    <Image source={socialogue} style={{width: 20, height: 20, marginRight: 20}} />
                    <Text style={styles.drpDownText}>SOCIALOGUE</Text>
                  </View>
                }>
                <View>
                  <Text>Content</Text>
                </View>
              </DropDownItem>
            </View>
          </ScrollView>
        </View>

        <View style={styles.bottomWarpper}>
          <View style={styles.language}>
            <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>LANGUAGE</Text>
            <Image source={downArrow} style={{width: 10, height: 6, marginLeft: 10}} />
          </View>
          <View style={styles.loginSignup}>

            {!this.props.isLoggedIn &&
            <TouchableOpacity
              style={{paddingHorizontal: 10, paddingVertical: 10}}
              onPress={() =>navigateToLogin(this.props.componentId)}>
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>LOGIN</Text>
            </TouchableOpacity>
            }
            {!this.props.isLoggedIn &&
            <TouchableOpacity
              style={{paddingLeft: 10, paddingVertical: 10}}
              onPress={() =>navigateToRegister(this.props.componentId)}>
              <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>SIGNUP</Text>
            </TouchableOpacity>
            }
            {this.props.isLoggedIn &&
            <Text onPress={this.logOut}  style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>LOGOUT</Text>
            }
          </View>
        </View>

        {this.state.isArrow &&
        <View style={styles.shopDown}>
          <ScrollView>
            {this.props.allShops.length &&
            <>
              {this.props.allShops.map( item => {
                return (
                  <TouchableOpacity
                    activeOpacity={.8}
                    onPress={() => this.setActiveShop(item)}>
                    <View key={item.shopName} style={styles.shopDownItem}>
                      <Image
                        source={this.props.user.profileImage === undefined ? userAvater : {uri: getImageOriginalUrl(this.props.user.profileImage)}}
                        style={{marginRight: 10, height: 30, width: 30, borderRadius: 37.5}} />
                      <Text>{item.shopName}</Text>
                    </View>
                  </TouchableOpacity>
                )
              })}
            </>
            }
          </ScrollView>
        </View>
        }
      </View>
    );
  }
}

export default connect(
  state => ({
    auth: state.auth,
    user: selectUser(state),
    allShops: selectShops(state),
    isLoggedIn: !!selectToken(state),
    activeShop: selectActiveShop(state),
    activeProfile: selectActiveProfile(state),

  }),
  {
    logOut,
    fetchShop,
    setActiveShop,
  }
)(AppMenu);


