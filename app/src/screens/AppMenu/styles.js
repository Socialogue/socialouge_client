import {StyleSheet, Text} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY} from "../../utils/constants";
import * as React from "react";

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  headerWarpper: {
    // flex: 8,
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    // paddingRight: 20,
    paddingLeft: 20,
    borderBottomWidth: 1,
  },
  iconPart: {
    flexDirection: 'row',
    alignItems: 'center',

  },
  logInText: {
    marginLeft: 20
  },
  bodyWarpper: {
    flex: 80,
  },
  category: {
    flexDirection: 'row',
    padding: 20
  },
  switchButtonText: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
    textTransform: 'uppercase',
  },
  switchButton: {
    padding: 10,
  },
  manCategory: {
    marginRight: 15,
    marginLeft: 15,
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK
  },
  userId: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20
  },
  shopDown: {
    position: 'absolute',
    width: 180,
    height: 150,
    backgroundColor: '#fff',
    top: '26%',
    left: 20,
    borderWidth: 1,
  },
  shopDownItem: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderBottomWidth: 1
  },
  id: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  downMenu: {
    paddingTop: 15,
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderColor: '#E5E5E5',
  },
  drpDownText: {
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_BLACK,
  },
  menuHead: {
    flexDirection: 'row',
  },
  bottomWarpper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 8,
    alignItems: 'center',
    paddingRight: 20,
    paddingLeft: 20,
  },
  language: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  loginSignup: {
    flexDirection: 'row',
  },
  logIn: {
    marginRight: 10
  },
  // dropDownItem: {
  //   height: '20%'
  // },
  subCategory: {
    textTransform: 'uppercase',
    fontFamily: GROTESK_SEMIBOLD,
    color: PRIMARY_GRAY,
    paddingHorizontal: 20,
    paddingVertical: 10
  }
});
