import {Image, Text, TouchableOpacity, View} from "react-native";
import styles from "./styles";
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY} from "../../utils/constants";
import * as React from "react";


<View style={styles.category}>
  <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_GRAY}}>CATEGORY</Text>

  <TouchableOpacity
    style={{alignItems: 'center'}}
    onPress={() => this._categoryShow('man')}
  >
    <Text style={styles.manCategory}>MAN</Text>
    {this.state.showManCategory &&
    <Image source={upArrow} style={{width: 10, height: 6, marginTop: 5}} />
    }
    {!this.state.showManCategory &&
    <Image source={downArrow} style={{width: 10, height: 6, marginTop: 5}} />
    }
  </TouchableOpacity>
  <TouchableOpacity
    style={{alignItems: 'center'}}
    onPress={() => this._categoryShow('woman')}
  >
    <Text style={{fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>WOMAN</Text>
    {this.state.showWomanCategory &&
    <Image source={upArrow} style={{width: 10, height: 6, marginTop: 5}} />
    }
    {!this.state.showWomanCategory &&
    <Image source={downArrow} style={{width: 10, height: 6, marginTop: 5}} />
    }
  </TouchableOpacity>
</View>
<View>
  {this.state.showManCategory &&
  <View style={{height: 1000, borderTopWidth: 1}}>
    <View style={{padding: 20, flexDirection: 'row', alignItems: 'center'}}>
      <Image source={arrowLeft} style={{width: 25, height: 15, marginRight: 5}} />
      <Text style={{textTransform: 'uppercase', fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>Clothes</Text>
    </View>
    <Text style={styles.subCategory}>See all</Text>
    <Text style={styles.subCategory}>Coats & jackets</Text>
    <Text style={styles.subCategory}>Suits & blazers</Text>
    <Text style={styles.subCategory}>Pants</Text>
    <Text style={styles.subCategory}>Underwear</Text>
  </View>
  }
  {this.state.showWomanCategory &&
  <View style={{height: 1000, borderTopWidth: 1}}>
    <View style={{padding: 20, flexDirection: 'row', alignItems: 'center'}}>
      <Image source={arrowLeft} style={{width: 25, height: 15, marginRight: 5}} />
      <Text style={{textTransform: 'uppercase', fontFamily: GROTESK_SEMIBOLD, color: PRIMARY_BLACK}}>Woman</Text>
    </View>
  </View>
  }
</View>

import * as React from 'react';
import {Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY, PRIMARY_WHITE} from '../utils/constants';
const downArrow = require('../../images/downArrow.png');
const upArrow = require('../../images/upArrow.png');


class ShopDown extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      isVisible: false,
    }
  }
  _shopShow = () => {
    this.setState({isVisible: !this.state.isVisible});
  }
  render() {
    return (
      <View style={styles.container}>

      </View>
    );
  }
}

export default ShopDown;

const styles = StyleSheet.create({

});
