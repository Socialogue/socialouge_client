import * as React from 'react';
import {Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {GROTESK_SEMIBOLD, PRIMARY_BLACK, PRIMARY_GRAY, PRIMARY_WHITE} from '../utils/constants';
const downArrow = require('../../images/downArrow.png');
const upArrow = require('../../images/upArrow.png');


class ShopDown extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      isVisible: false,
    }
  }
  _shopShow = () => {
    this.setState({isVisible: !this.state.isVisible});
  }
  render() {
    return (
      <View style={styles.container}>

      </View>
    );
  }
}

export default ShopDown;

const styles = StyleSheet.create({
  shopDown: {
    position: 'absolute',
    width: 180,
    height: 200,
    backgroundColor: 'red',
    zIndex: 9999999,
    top: 50,
    left: -100,
    borderWidth: 1,
    padding: 10
  },
});
