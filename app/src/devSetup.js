import { YellowBox } from 'react-native';
import config from './config';

YellowBox.ignoreWarnings([
  'Warning: isMounted(...) is deprecated',
  'Module RCTImageLoader requires main queue setup',
  'VirtualizedLists should',
]);

if (config.DISABLE_YELLOW_BOX) {
  // $FlowFixMe
  console.disableYellowBox = true; // eslint-disable-line no-console
}

if (config.NETWORK_DEBUG) {
  /** @namespace global.originalXMLHttpRequest */
  global.XMLHttpRequest = global.originalXMLHttpRequest ?
    global.originalXMLHttpRequest : global.XMLHttpRequest;

  /** @namespace global.originalFormData */
  global.FormData = global.originalFormData ?
    global.originalFormData : global.FormData;
}

if (config.CONFIG_LOGGER) {
  const map = require('lodash/map');
  /* eslint-disable no-console */
  console.log('--------------app config--------------');
  map(config, (value, key) => console.log(`${key}: ${value}`));
  console.log('--------------app config--------------');
  /* eslint-enable no-console */
}
