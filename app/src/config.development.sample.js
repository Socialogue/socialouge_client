export default {
  API_URL: '',
  GUEST_SECRET: 'simpleguesttoken',
  SUPPORT_MAIL: '',
  FILE_SERVER_URL: '',
  DISABLE_YELLOW_BOX: false,
  NETWORK_DEBUG: true,
  REDUX_LOGGER: true,
  CONFIG_LOGGER: true,
  FB_APP_EVENTS: false,
  FLIPPER_DEBUGGER: true,
};
