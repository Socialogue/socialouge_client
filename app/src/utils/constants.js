export const PRIMARY_BLACK = "#000000";
export const PRIMARY_GRAY = "#868686";
export const SECONDARY_ORANGE = "#FF6A00";
export const LIGHT_GRAY = "#E8E8E8";
export const PRIMARY_WHITE = "#FFFFFF";

export const GROTESK_REGULAR = "hk-grotesk.regular";
export const GROTESK_SEMIBOLD = "hk-grotesk.semibold";
export const GROTESK_BOLD = "hk-grotesk.bold";

