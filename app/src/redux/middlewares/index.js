import axiosClientMiddleware from 'core/redux/axios/middleware';
import config from '../../config';
import createSagaMiddleware from 'redux-saga';
import injectTokenBeforeRequest from 'core/redux/middlewares/injectTokenBeforeRequest';

const sagaMiddleware = createSagaMiddleware();

const middlewares = [
  injectTokenBeforeRequest(config.GUEST_SECRET),
  axiosClientMiddleware,
  /**
   * sometimes saga middleware does not capture all dispatched actions,
   * if up in the middleware order, so putting it at last as a workaround
   */
  sagaMiddleware
];

/** Add development specific middlewares here */
if (process.env.NODE_ENV === 'development') {
  /** Log redux actions */
  if (config.REDUX_LOGGER) {
    middlewares.push(require('redux-logger').default);
  }
  if (config.FLIPPER_DEBUGGER) {
    createDebugger = require('redux-flipper').default;
    middlewares.push(createDebugger());
  }
}

/** Analytics should be required on production, testing etc. */
// if (config.REDUX_ANALYTICS) {
//   middlewares.push(require('./analyticsMiddleware').default);
// }


export { sagaMiddleware };
export default middlewares;
