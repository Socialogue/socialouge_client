import { combineReducers } from "redux";

import fileUploadReducer from "core/redux/reducers/fileUploadReducer";
import authReducer from "core/redux/reducers/authReducer";
import postReducer from "core/redux/reducers/postReducer";
import shopReducer from "core/redux/reducers/shopReducer";
import socketReducer from "core/messenger/socketReducer";
import productReducer from "core/redux/reducers/productReducer";
import cartReducer from "core/redux/reducers/cartReducer";
import homeReducer from "core/redux/reducers/homeReducer";
import publicProfileReducer from "core/redux/reducers/publicProfileReducer";
import messengerReducer from "core/messenger/messageReducer";
import userSettingsReducer from "core/redux/reducers/userSettingReducer";
import outfitsReducer from "core/redux/reducers/outfitsReducer";
import accountReducer from "core/redux/reducers/accountReducer";
import appConfig from "./appConfigReducer";

export default combineReducers({
  files: fileUploadReducer,
  auth: authReducer,
  posts: postReducer,
  shops: shopReducer,
  socket: socketReducer,
  products: productReducer,
  cart: cartReducer,
  home: homeReducer,
  publicProfile: publicProfileReducer,
  messenger: messengerReducer,
  settings: userSettingsReducer,
  outfits: outfitsReducer,
  account: accountReducer,
  appConfig: appConfig
});
