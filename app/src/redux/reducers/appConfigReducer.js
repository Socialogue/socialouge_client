import {REHYDRATE} from "redux-persist/constants";
import {ACTION_SWIPER_SHOWN} from "../actionTypes";

export const initialState = {
  swiperShown: false,
};

const appConfigReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case REHYDRATE: {
      const { appConfig } = payload;

      if (appConfig) {
        return {
          ...state,
          swiperShown: appConfig.swiperShown,
        };
      }

      return state;
    }

    case ACTION_SWIPER_SHOWN: {
      const { swiperShown } = payload;
      return {
        ...state,
        swiperShown
      };
    }

    default:
      return state;
  }
};

export default appConfigReducer;
