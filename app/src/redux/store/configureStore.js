import composeEnhancers from "core/redux/store/composeEnhancers";
import { applyMiddleware, createStore } from "redux";
import { autoRehydrate } from "redux-persist";
import middlewares, { sagaMiddleware } from "../middlewares";
import rootReducer from "../reducers/rootReducer";
import sagas from "../sagas";

export default function configureStore() {
  /** We need multiple com positions before applying middlewares */
  const enhancer = composeEnhancers(
    applyMiddleware(...middlewares),
    autoRehydrate()
  );

  const store = createStore(rootReducer, {}, enhancer);

  /** Enable hot reloading of reducers */
  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require("../reducers/rootReducer").default;
      store.replaceReducer(nextRootReducer);
    });
  }

  sagaMiddleware.run(sagas);

  return store;
}
