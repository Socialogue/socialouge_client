import {ACTION_SWIPER_SHOWN} from "../actionTypes";

export const swiperShown = (swiperShown) => ({
  type: ACTION_SWIPER_SHOWN,
  payload: {
    swiperShown
  }
});
