/**
 * @flow
 */
import { all } from 'redux-saga/effects';

import axiosSaga from 'core/redux/axios/saga';
import { selectToken } from 'core/selectors/user';
import { axiosClient, axiosClientConfig } from 'core/client';

import config from '../../config';

const baseUrl = config.API_URL;
const apiVersion = config.APP_VERSION;
const guestToken = config.GUEST_SECRET;

const client = axiosClient({
  baseUrl,
  apiVersion
});

const clientConfig = axiosClientConfig({
  guestToken,
  getTokenFromState: selectToken
});

// $FlowFixMe
export default function* rootSaga() {
  yield all([
    axiosSaga(client, clientConfig),
  ]);
}
