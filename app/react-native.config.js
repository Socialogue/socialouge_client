module.exports = {
  dependencies: {
    "react-native-config": {
      platforms: {
        ios: null
        // add more platform to disable auto-linking for them too
      }
    },
    "react-native-sentry": {
      platforms: {
        android: null
      }
    }
  }
}
