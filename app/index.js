/**
 * https://github.com/facebook/react-native
 * @flow
 */
import { LogBox } from 'react-native';

LogBox.ignoreLogs(['Warning: ...', 'componentWill']);
// eslint-disable-next-line no-unused-vars
import slapp from './src/App';
