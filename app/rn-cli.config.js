const path = require('path');
// eslint-disable-next-line import/no-extraneous-dependencies
const blacklist = require('metro-config/src/defaults/blacklist');

const cwd = path.resolve(__dirname);

module.exports = {
  watchFolders: [
    path.resolve(cwd, '..') // project directory
  ],
  resolver: {
    blacklistRE: blacklist([
      /cook-app\/.*/
    ])
  }
};

