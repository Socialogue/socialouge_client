FROM node:12

# Create app directory
WORKDIR /usr/src/app
COPY . .

# Install app dependencies
#COPY package.json ./
#COPY yarn.lock ./
#RUN rm yarn.lock

# If you are building your code for production
RUN npm install yarn -g
RUN yarn install

# EXPOSE 8080
# CMD [ "npm", "start" ]
