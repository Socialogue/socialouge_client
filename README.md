# Socialogue Monorepo

## Getting Started
This project is a monorepo, set up using yarn workspace. So using yarn instead of npm is required. You can learn about how workspace works [here.](https://yarnpkg.com/lang/en/docs/workspaces/)

## Install Dependencies
```
yarn install
```
A `yarn.lock` file is included to make sure we have the same dependency versions in any environment.

## Folder Structure
```
root
├── core (includes common functionalities for both mobile & web)
├── app (app for iOS & android)
├── packages (contains eslint config & web build script)
├── scripts (development scripts)
├── web (desktop website)
├── mobile-web (mobile website)
```

## Web Development
Copy & rename `web/src/config.development.sample.js` to `web/src/config.development.js`. Change variables as per your need.

To start working on the website, run
```
yarn web-start
```

To build for production, run
```
yarn web-build
```
> Build files will be available in `/build/web` directory.

To test server side rendering during development, run
```
yarn web-ssr
```
Before running this command, make sure you ran `yarn web-build` to generate the production build.

To analyze bundle size, run
```
yarn web-analyze
```

To start storybook, run
```
yarn web-storybook
```

Pre commit command `"precommit": "lint-staged",`
